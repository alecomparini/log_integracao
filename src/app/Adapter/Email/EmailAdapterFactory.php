<?php
namespace App\Adapter\Email;

use App\Core\Contracts\IFactory;

class EmailAdapterFactory implements IFactory {
    public static function create($type) {
        if ($type === 'phpmailer') {
            return new PhpMailerEmailAdapter();
        }

        return new MockEmailAdapter();
    }
}