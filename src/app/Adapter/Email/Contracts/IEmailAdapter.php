<?php
namespace App\Adapter\Email\Contracts;

interface IEmailAdapter {
    public function setFrom($email, $nome = null);
    public function addTo($email, $nome = null);
    public function send($throwException = false);
    public function setBody($body, $isHtml = true);
    public function setSubject($subject);
    public function addAttachment($path, $name = '');
}