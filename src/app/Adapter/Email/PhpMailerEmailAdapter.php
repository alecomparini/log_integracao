<?php
namespace App\Adapter\Email;

use App\Adapter\Email\Contracts\IEmailAdapter;
use App\Core\Config;
use App\Core\Logger;
use App\Exceptions\FrameworkException;
use PHPMailer;

class PhpMailerEmailAdapter implements IEmailAdapter {

    /** @var \PHPMailer */
    private $mailer;

    public function __construct() {
        $this->mailer = new PHPMailer();
        $this->configure();
    }

    public function setFrom($email, $nome = null) {
        $nome = !is_null($nome) ? $nome : $email;
        $this->mailer->setFrom($email, $nome);
        return $this;
    }

    public function addTo($email, $nome = '') {
        $this->mailer->addAddress($email, $nome);
        return $this;
    }

    public function send($throwException = true) {
        if (!$this->mailer->send()) {
            $mensagemErro = 'Mailer error: ' . $this->mailer->ErrorInfo;
            if ($throwException) {
                throw new FrameworkException($mensagemErro);
            }

            Logger::getInstance()->error($mensagemErro);
            return false;
        }
        return true;
    }

    public function setBody($body, $isHtml = true) {
        $this->mailer->isHTML($isHtml);
        $this->mailer->Body = $body;
        $this->mailer->AltBody = $body;
        return $this;
    }

    public function setSubject($subject) {
        $this->mailer->Subject = $subject;
        return $this;
    }

    public function addAttachment($path, $name = '') {
        $this->mailer->addAttachment($path, $name);
        return $this;
    }

    private function configure() {
        $this->mailer->isSMTP();
        $this->mailer->Host = Config::get('email.smtp.host');
        $this->mailer->Username = Config::get('email.smtp.user');
        $this->mailer->Password = Config::get('email.smtp.pass');
        $this->mailer->SMTPSecure = Config::get('email.smtp.auth');
        $this->mailer->Port = Config::get('email.smtp.port');
        $this->mailer->SMTPAuth = true;
        $this->mailer->CharSet = "UTF-8";

        $this->mailer->addReplyTo(Config::get('email.from.email'), Config::get('email.from.nome'));
        $this->setFrom(Config::get('email.from.email'), Config::get('email.from.nome'));
        return $this;
    }

}