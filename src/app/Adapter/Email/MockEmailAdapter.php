<?php
namespace App\Adapter\Email;

use App\Adapter\Email\Contracts\IEmailAdapter;
use App\Core\Config;
use App\Core\Logger;
use App\Exceptions\FrameworkException;
use PHPMailer;

class MockEmailAdapter implements IEmailAdapter {

    public function __construct() {

    }

    public function setFrom($email, $nome = null) {
        return $this;
    }

    public function addTo($email, $nome = '') {
        return $this;
    }

    public function send($throwException = true) {
        return true;
    }

    public function setBody($body, $isHtml = true) {
        return $this;
    }

    public function setSubject($subject) {
        return $this;
    }

    public function addAttachment($path, $name = '') {
        return $this;
    }
}