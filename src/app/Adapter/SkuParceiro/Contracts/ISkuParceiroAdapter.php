<?php
namespace App\Adapter\SkuParceiro\Contracts;

/**
 * Interface ISkuParceiroAdapter
 *
 * @package App\Adapter\SkuParceiro\Contracts
 */
interface ISkuParceiroAdapter {
    /**
     * @param int $parceiroId
     * @param string[] $codigos
     *
     * @return string[]
     */
    function getSkuParceiro($parceiroId, $codigos);
}