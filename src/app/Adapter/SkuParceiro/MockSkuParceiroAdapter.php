<?php
namespace App\Adapter\SkuParceiro;

use App\Adapter\SkuParceiro\Contracts\ISkuParceiroAdapter;
use App\BO\ParceiroConfiguracaoBO;
use App\BO\SkuParceiroBO;
use App\Core\Logger;
use App\Exceptions\BusinessException;
use App\Exceptions\DatabaseException;
use App\Library\VtexSDK;
use App\Model\Configuracao;

/**
 * Class MockSkuParceiroAdapter
 *
 * @package App\Adapter\SkuParceiro
 */
class MockSkuParceiroAdapter implements ISkuParceiroAdapter {

    /**
     * @param int       $parceiroId
     * @param \string[] $codigos
     *
     * @return \string[]
     * @throws \App\Exceptions\BusinessException
     */
    public function getSkuParceiro($parceiroId, $codigos) {
        $skus = array();

        foreach ($codigos as $codigo) {
            $skus[$codigo] = $codigo;
        }

        return $skus;
    }
}