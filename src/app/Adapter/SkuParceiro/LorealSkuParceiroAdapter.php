<?php
namespace App\Adapter\SkuParceiro;

use App\Adapter\SkuParceiro\Contracts\ISkuParceiroAdapter;
use App\BO\ParceiroConfiguracaoBO;
use App\BO\SkuParceiroBO;
use App\Core\Logger;
use App\Exceptions\BusinessException;
use App\Exceptions\DatabaseException;
use App\Library\VtexSDK;
use App\Model\Configuracao;

/**
 * Class LorealSkuParceiroAdapter
 *
 * @package App\Adapter\SkuParceiro
 */
class LorealSkuParceiroAdapter implements ISkuParceiroAdapter {

    /**
     * @param int       $parceiroId
     * @param \string[] $codigos
     *
     * @return \string[]
     * @throws \App\Exceptions\BusinessException
     */
    public function getSkuParceiro($parceiroId, $codigos) {
        $skuParceiroBO = new SkuParceiroBO();

        $skus = array();

        $vtexSDK = new VtexSDK($parceiroId);

        try {
            $skusParceiro = $skuParceiroBO->buscarSkuParceiro($parceiroId, $codigos);

            foreach ($skusParceiro as $skuParceiro) {
                $skus[$skuParceiro->codigo] = $skuParceiro->skuParceiro;
                $codigos = array_diff($codigos, array($skuParceiro->codigo));
            }
        } catch (DatabaseException $dbe) {
            Logger::getInstance()->exception($dbe);
        }

        // Busca só os que não foram encontrados no banco de dados
        foreach ($codigos as $codigo) {
            try {
                $skus[$codigo] = $vtexSDK->getIdSkuVtex($codigo);
                try {
                    $skuParceiroBO->inserirSkuParceiro($parceiroId, $codigo, $skus[$codigo]);
                } catch (DatabaseException $dbe) {
                    Logger::getInstance()->exception($dbe);
                }
            } catch (BusinessException $be) {
                Logger::getInstance()->exception($be);
                throw $be;
            }
        }

        return $skus;
    }
}