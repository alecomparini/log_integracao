<?php
namespace App\Adapter\SkuParceiro;

use App\Core\Contracts\IFactory;
use App\Exceptions\BusinessException;
use App\Model\Response;

/**
 * Class SkuParceiroAdapterFactory
 *
 * @package App\Adapter\SkuParceiro
 */
class SkuParceiroAdapterFactory implements IFactory {
    /**
     * @param $type
     *
     * @return \App\Adapter\SkuParceiro\LorealSkuParceiroAdapter
     * @throws \App\Exceptions\BusinessException
     */
    public static function create($type) {
        if ($type == 'loreal') {
            return new LorealSkuParceiroAdapter();
        } elseif ($type == 'mock') {
            return new MockSkuParceiroAdapter();
        }
        throw new BusinessException('Adapter ' . $type . ' not found.', Response::ERRO_CONSULTA_SKU_PARCEIRO);
    }
}