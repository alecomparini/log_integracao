<?php
namespace App\Adapter\EstoqueParceiro\Contracts;

/**
 * Interface IEstoqueParceiroAdapter
 *
 * @package App\Adapter\EstoqueParceiro
 */
interface IEstoqueParceiroAdapter {
    /**
     * Retorna o estoque dos produtos de um parceiro
     *
     * @param int      $parceiroId Código de Parceiro
     * @param string[] $codigos    Códigos dos produtos (SKU)
     * @param int      $pagina     Página
     *
     * @return array
     */
    function getEstoques($parceiroId, $codigos, $pagina);
}