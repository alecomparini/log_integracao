<?php
namespace App\Adapter\EstoqueParceiro;

use App\Adapter\EstoqueParceiro\Contracts\IEstoqueParceiroAdapter;
use App\BO\ParceiroConfiguracaoBO;
use App\BO\ProdutoBO;
use App\Core\Config;
use App\Core\Logger;
use App\Exceptions\BusinessException;
use App\Exceptions\DatabaseException;
use App\Library\VtexSDK;
use App\Model\Configuracao;
use App\Service\JobPoolExecutor;

/**
 * Class LorealEstoqueParceiroAdapter
 *
 * @package App\Adapter\EstoqueParceiro
 */
class LorealEstoqueParceiroAdapter implements IEstoqueParceiroAdapter {
    /**
     * Retorna o estoque dos produtos de um parceiro
     *
     * @param int      $parceiroId Código de Parceiro
     * @param string[] $codigos    Códigos dos produtos (SKU)
     * @param int      $pagina
     *
     * @return array
     * @throws \App\Exceptions\BusinessException
     * @throws \App\Exceptions\ValidacaoException
     */
    public function getEstoques($parceiroId, $codigos = array(), $pagina = 1) {
        $produtoBO = new ProdutoBO();

        $result = $produtoBO->getProdutoEstoqueParceiro($parceiroId, $codigos, $pagina);

        /** @var array $estoques */
        $estoques = $result['estoques'];
        $paginacao = $result['paginacao'];

        $codigos = array_keys($estoques);

        // Try/Catch para permitir que o relatório possa ser renderizado mesmo que
        // não tenha sido criado a tabela de cache no banco de dados ainda.
        try {
            $skusProduto = $produtoBO->buscaSkuParceiro($parceiroId, $codigos);
            foreach ($skusProduto as $skuProduto) {
                $estoques[$skuProduto->codigo]->skuParceiro = $skuProduto->skuParceiro;

                //Remove da lista de códigos para poder evitar ter que buscar na vtex
                $codigos = array_diff($codigos, array($skuProduto->codigo));
            }
        } catch (DatabaseException $dbe) {
            Logger::getInstance()->exception($dbe);
        }

        // Busca os SKUs que estão faltando
        $poolSize = Config::get('services.estoque-parceiro.poolsize', 50);
        $host = Config::get('services.estoque-parceiro.host', 'integracao.dev');
        $port = Config::get('services.estoque-parceiro.port', 80);
        $timeout = Config::get('services.estoque-parceiro.timeout', 1);
        $username = Config::get('services.estoque-parceiro.username', 'william');
        $password = Config::get('services.estoque-parceiro.password', 'william');
        $pool = new JobPoolExecutor($poolSize, $timeout, $host, $port, $username, $password);

        $configBO = new ParceiroConfiguracaoBO();
        $configEndpoint = $configBO->getConfiguracoes($parceiroId, Configuracao::ENDPOINT_BUSCA_SKU_PARCEIRO, true);

        $pool->setHttpMethod('POST');
        $pool->setIsJson(true);
        foreach ($codigos as $codigo) {
            $pool->addJob("/servicos/parceiro/sku/{$parceiroId}/{$codigo}", array(
                'endpoint' => $configEndpoint->valor
            ));
        }

        $pool->processAll();
        $respostas = $pool->getAllResponses();

        foreach ($respostas as $resposta) {
            $data = json_decode($resposta);
            if ($data->success) {
                $estoques[$data->data->codigo]->skuParceiro = $data->data->sku;
            } else {
                Logger::getInstance()->error("Falha ao consultar SKU ({$data->data->codigo}) - " . $data->mensagem);
            }
        }

        //Busca os estoques
        $codigos = array();
        foreach ($estoques as $estoque) {
            if (!empty($estoque->skuParceiro)) {
                $codigos[] = $estoque->skuParceiro;
            }
        }

        $vtex = new VtexSDK($parceiroId);
        try {
            $estoquesParceiro = $vtex->getStock($codigos);
        } catch (BusinessException $be) {
            Logger::getInstance()->exception($be);
            throw $be;
        }

        $estoquesComSkuParceiro = array_reduce($estoques, array($this, 'filtraSkuComEstoque'), array());

        foreach ($estoquesParceiro->items as $item) {
            $disponivel = 0;
            foreach ($item->wareHouses as $wareHouse) {
                $disponivel += ($wareHouse->quantity - $wareHouse->reserved);
            }

            $skuMV = $estoquesComSkuParceiro[$item->skuId]->codigo;
            $estoques[$skuMV]->quantidadeParceiro = $disponivel;
        }

        return array(
            'estoques' => $estoques,
            'paginacao' => $paginacao,
        );
    }

    private function filtraSkuComEstoque($acc, $item) {
        if (isset($item->skuParceiro)) {
            $acc[$item->skuParceiro] = $item;
        }

        return $acc;
    }
}