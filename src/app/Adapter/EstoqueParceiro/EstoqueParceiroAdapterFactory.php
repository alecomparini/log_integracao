<?php
namespace App\Adapter\EstoqueParceiro;

use App\Core\Contracts\IFactory;
use App\Exceptions\FrameworkException;

abstract class EstoqueParceiroAdapterFactory implements IFactory {
    /**
     * @param $type
     *
     * @return \App\Adapter\EstoqueParceiro\LorealEstoqueParceiroAdapter
     * @throws \App\Exceptions\FrameworkException
     */
    public static function create($type) {
        if ($type == 'loreal') {
            return new LorealEstoqueParceiroAdapter();
        } elseif ($type == 'mock') {
            return new MockEstoqueParceiroAdapter();
        }

        throw new FrameworkException('Adapter Type not found');
    }
}