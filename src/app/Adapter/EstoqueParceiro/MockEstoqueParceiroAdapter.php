<?php
namespace App\Adapter\EstoqueParceiro;

use App\Adapter\EstoqueParceiro\Contracts\IEstoqueParceiroAdapter;
use App\BO\ParceiroConfiguracaoBO;
use App\BO\ProdutoBO;
use App\Core\Config;
use App\Core\Logger;
use App\Exceptions\BusinessException;
use App\Exceptions\DatabaseException;
use App\Library\VtexSDK;
use App\Model\Configuracao;
use App\Service\JobPoolExecutor;

/**
 * Class LorealEstoqueParceiroAdapter
 *
 * @package App\Adapter\EstoqueParceiro
 */
class MockEstoqueParceiroAdapter implements IEstoqueParceiroAdapter {
    /**
     * Retorna o estoque dos produtos de um parceiro
     *
     * @param int      $parceiroId Código de Parceiro
     * @param string[] $codigos    Códigos dos produtos (SKU)
     * @param int      $pagina
     *
     * @return array
     * @throws \App\Exceptions\BusinessException
     * @throws \App\Exceptions\ValidacaoException
     */
    public function getEstoques($parceiroId, $codigos = array(), $pagina = 1) {
        $produtoBO = new ProdutoBO();

        $result = $produtoBO->getProdutoEstoqueParceiro($parceiroId, $codigos, $pagina);

        /** @var \App\Model\EstoqueParceiro[] $estoques */
        $estoques = $result['estoques'];
        $paginacao = $result['paginacao'];

        /** @var \App\Model\EstoqueParceiro $estoque */
        foreach ($estoques as $estoque) {
            $estoques[$estoque->codigo]->skuParceiro = $estoque->codigo;
            $estoques[$estoque->codigo]->quantidadeParceiro = mt_rand(-50, 50);
        }

        return array(
            'estoques' => $estoques,
            'paginacao' => $paginacao,
        );
    }

    private function filtraSkuComEstoque($acc, $item) {
        if (isset($item->skuParceiro)) {
            $acc[$item->skuParceiro] = $item;
        }

        return $acc;
    }
}