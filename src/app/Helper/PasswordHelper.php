<?php
namespace App\Helper;

use InvalidArgumentException;
use RuntimeException;

class PasswordHelper {
    const BLOWFISH = '2a';
    const SALT_MIN_SIZE = 15;

    private $algorith;
    private $cost;

    /**
     * PasswordHelper constructor.
     *
     * @param string $algorith
     * @param int    $cost
     */
    public function __construct($algorith = self::BLOWFISH, $cost = 8) {
        $this->algorith = $algorith;
        $this->cost = str_pad($cost, 2, '0', STR_PAD_LEFT);
    }

    public function hash($string) {
        $this->checkForCryptFunction();
        $salt = $this->generateRandomSalt();
        return crypt($string, $this->generateHashString($salt));
    }

    public function check($string, $hash) {
        $this->checkForCryptFunction();
        return crypt($string, $hash) === $hash;
    }

    private function checkForCryptFunction() {
        if (!function_exists('crypt')) {
            throw new RuntimeException('Função crypt não encontrada no sistema');
        }
        return true;
    }

    private function generateHashString($salt = '') {
        if ('' === $salt) {
            throw new InvalidArgumentException('O campo salt não deve ser vazio');
        }

        if (strlen($salt) < self::SALT_MIN_SIZE) {
            throw new InvalidArgumentException(sprintf('Por questões de segurança o campo salt deve conter ao menos %d caracteres', self::SALT_MIN_SIZE));
        }

        return sprintf('$%s$%02d$%s$', $this->algorith, $this->cost, $salt);
    }

    public function generateRandomSalt($size = 22) {
        if (!is_numeric($size)) {
            throw new InvalidArgumentException('Argumento size deve ser um inteiro');
        }

        if ($size < 1) {
            throw new InvalidArgumentException('Argumento size deve ser maior que zero');
        }

        $seed = uniqid(mt_rand(), true);
        $salt = str_replace('+', '.', base64_encode($seed));

        return substr($salt, 0, $size);
    }
}