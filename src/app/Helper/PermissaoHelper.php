<?php
namespace App\Helper;

use App\BO\ParceiroSitePermissaoBO;
use App\BO\TokenBO;
use App\Core\Config;
use App\DAO\ParceiroSitePermissaoDAO;
use App\DAO\ParceiroSiteDAO;
use App\DAO\InterfacePermissaoDAO;
use App\Exceptions\AuthorizationException;
use App\Exceptions\BusinessException;
use App\Exceptions\ValidacaoException;
use App\Model\Response;
use App\Service\Mailer;
use DateTime;

/**
 * Class PermissaoHelper
 * @package App\Helper
 * @author: Nobody
 */
class PermissaoHelper {
    /**
     * Checa permissão por IP de parceiro
     *
     * @param      $parceiroId
     * @param      $ip
     * @param null $permissoesParam
     *
     * @return bool
     * @throws \App\Exceptions\BusinessException
     * @throws \App\Exceptions\AuthorizationException
     * @throws \App\Exceptions\ValidacaoException;
     */
    public static function checaPermissao($parceiroId, $ip, &$permissoesParam = null) {
       
        $ipsLivres = explode(',', IPS_BYPASS);

        if (in_array($ip, $ipsLivres)) {
            return true;
        }

        // Regras de validação dos parametros
        $condicoes = array(
            "parceiroId" => array("required", "notempty", "int"),
            "ip" => array("required", "notempty", "str"),
        );

        // Parametros enviados no post
        $validacao = new Validacao($condicoes);

        $dados = new \stdClass();
        $dados->parceiroId = $parceiroId;
        $dados->ip = $ip;

        //Executa a validação e em caso de erro, joga exception
        if (!$validacao->executaValidacao($dados)) {
            throw new ValidacaoException($validacao->msg, 2);
        }

        $parceiroSiteDAO = new ParceiroSiteDAO();

        $parceiroSitePermissaoDAO = new ParceiroSitePermissaoDAO();
        $parceiroSite = $parceiroSiteDAO->getParceiroSiteById($parceiroId);
        if (null === $parceiroSite) {
            throw new BusinessException(sprintf("Parceiro não encontrado com a ID %d", $parceiroId), Response::CODIGO_PARCEIRO_INVALIDO);
        }
        $permissoes = $parceiroSitePermissaoDAO
            ->getPermissoesParceiroSiteIp($parceiroSite->parceiroSiteId
                , $parceiroSite->siteId
                , $ip);

        if (null !== $permissoesParam) {
            $permissoesParam = $permissoes;
        }

        if (count($permissoes) === 0) {
            $permissoes = array();
            $mailer = new Mailer();

            $destinatariosErros = Config::get('destinatarios_erros');
            foreach ($destinatariosErros as $destinatario) {
                $mailer->addTo($destinatario[0], $destinatario[1]);
            }
            $hoje = new DateTime('now');
            $mailer
                ->setSubject("Parceiro Bloqueado Integração")
                ->setBody("ParceiroId: {$parceiroId} - IP: {$ip} - Data: {$hoje->format('d/m/Y H:i:s')}")
                ->send();
            return false;
        }
        return true;
    }

    /**
     * Verifica se o parceiroId informado possui permissão de acesso na Interface solicitada
     * @param $parceiroId
     * @param $interfaceTipoId
     * @param null $permissao
     * @return bool
     * @throws \Exception
     */
    public static function checaPermissaoInterface($parceiroId, $interfaceTipoId, &$permissao = null) {
        // Regras de validação dos parametros
        $condicoes = array(
            "parceiroId" => array("notempty", "int", "required"),
            "interfaceTipo" => array("notempty", "required")
        );

        // Parametros enviados no post
        $validacao = new Validacao($condicoes);

        $dados = new \stdClass();
        $dados->parceiroId = $parceiroId;
        $dados->interfaceTipo = $interfaceTipoId;

        //Executa a validação e em caso de erro, joga exception
        if (!$validacao->executaValidacao($dados)) {
            throw new ValidacaoException($validacao->msg);
        }

        $parceiroSiteDAO = new ParceiroSiteDAO();
        $interfacePermissaoDAO = new InterfacePermissaoDAO();

        $parceiroSite = $parceiroSiteDAO->getParceiroSiteById($parceiroId);
        $permissaoInterface = $interfacePermissaoDAO
            ->getInterfacePermissaoByParceiroSiteId($parceiroSite->parceiroSiteId, $interfaceTipoId);

        if (null !== $permissao) {
            $permissao = $permissaoInterface;
        }

        return null !== $permissaoInterface;
    }

    public static function hasPermissaoIp($ip) {
        $parceiroSitePermissaoBO = new ParceiroSitePermissaoBO();
        return $parceiroSitePermissaoBO->verificaPermissaoIp($ip);
    }

    public static function checaPermissaoTokenEscopo($token, $escopo) {
        $tokenBO = new TokenBO();
        $token = $tokenBO->getToken($token);
        if (null === $token) {
            return false;
        }
        $tokenEscopo = $tokenBO->getTokenEscopo($token, $escopo);
        return null !== $tokenEscopo;
    }
}
