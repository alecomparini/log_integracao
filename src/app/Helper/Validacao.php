<?php

namespace App\Helper;

use App\Core\Config;

/**
 * Class Validacao
 * @package App\Helper
 */
class Validacao {
    public $condicoes;
    public $msg;
    public static $lastMsg;

    public function __construct($condicoes) {
        $this->condicoes = $condicoes;
    }
    
    public static function validar($valor, $tipo) {
        static::$lastMsg = "";
        
        if (!is_array($tipo)) {
            $tipo = array($tipo);
        }
        
        $regras = array('campo' => $tipo);
        $validacao = new static($regras);
        
        $obj = new \stdClass;
        $obj->campo = $valor;
        
        if (!$validacao->executaValidacao($obj)) {
            static::$lastMsg = $validacao->msg;
            return false;
        }
        return true;
    }

    /**
     * @param object|array $objeto
     * @return bool
     */
    public function executaValidacao($objeto) {

        if (!is_object($objeto) && !is_array($objeto)) {
            $this->msg = "Validações devem ser executadas em arrays ou objetos apenas.";
            return false;
        }

        foreach ($this->condicoes as $campo => $regras) {
            if (in_array("required", $regras)) {
                if (is_array($objeto)) {
                    if (!array_key_exists($campo, $objeto)) {
                        $this->msg = "Campo ({$campo}) e obrigatorio.";
                        return false;
                    }
                } else {
                    if (is_object($objeto) && !property_exists($objeto, $campo)) {
                        $this->msg = "Campo ({$campo}) e obrigatorio.";
                        return false;
                    }
                }
            }
        }

        foreach ($objeto as $key => $valor) {
            if (isset($this->condicoes[$key])) {
                foreach ($this->condicoes[$key] as $index => $condicao) {

                    if ((in_array('pj', $this->condicoes[$key]) && ($objeto->clienteTipo == 'pf')) ||
                        (in_array('pf', $this->condicoes[$key]) && ($objeto->clienteTipo == 'pj'))) {
                        break;
                    }

                    $c = is_array($condicao) ? $index : $condicao;

                    switch ($c) {
                        case 'int':
                            if (is_array($condicao)) {
                                if (isset($condicao['min']) && strlen($valor) < $condicao['min']) {
                                    $this->msg = "Campo (" . $key . ") nao atende o valor correto esperado";
                                    return false;
                                }

                                if (isset($condicao['max']) && strlen($valor) > $condicao['max']) {
                                    $this->msg = "Campo (" . $key . ") nao atende o valor correto esperado";
                                    return false;
                                }
                                if (isset($condicao['ramoAtividade'])) {
                                    $ramos = array('0', '1', '2', '3', '4', '5', '6', '7', '8');

                                    if (!in_array($valor, $ramos)) {
                                        $this->msg = "Campo (" . $key . ") nao atende o valor correto esperado";
                                        return false;
                                    }
                                }
                            }
                            if (!is_numeric($valor)) {
                                $this->msg = "Campo (" . $key . ") deve ser numero inteiro.";
                                return false;
                            }
                            break;

                        case 'float':
                            if (is_array($condicao)) {
                                if (isset($condicao['min']) && $valor < $condicao['min']) {
                                    $this->msg = "Campo (" . $key . ") nao atende o valor correto esperado";
                                    return false;
                                }

                                if (isset($condicao['max']) && $valor > $condicao['max']) {
                                    $this->msg = "Campo (" . $key . ") nao atende o valor correto esperado";
                                    return false;
                                }

                                if (isset($condicao['notEmptyFloat']) && $valor <= 0) {
                                    $this->msg = "Campo (" . $key . ") nao pode ser 0";
                                    return false;
                                }

                                if (isset($condicao['notNegativeFloat']) && $valor < 0) {
                                    $this->msg = "Campo (" . $key . ") nao pode ser negativo";
                                    return false;
                                }
                            }
                            if (!is_numeric($valor)) {
                                $this->msg = "Campo (" . $key . ") deve ser numero float";
                                return false;
                            }
                            break;

                        case 'notnull':
                            if (is_null($valor)) {
                                $this->msg = "Campo (" . $key . ") nao deve ser nulo";
                                return false;
                            }
                            break;

                        case 'notempty':
                            if (empty($valor)) {
                                $this->msg = "Campo (" . $key . ") nao deve ser vazio";
                                return false;
                            }
                            break;

                        case 'notemptynorwhitespaces':
                            $tmp = trim($valor);
                            if (empty($tmp)) {
                                $this->msg = "Campo (" . $key . ") nao deve ser vazio";
                                return false;
                            }
                            break;

                        case 'str':
                            if (is_array($condicao)) {
                                if (!(isset($condicao['acceptNull']) && empty($valor))) {
                                    if (isset($condicao['min']) && strlen(utf8_decode($valor)) < $condicao['min']) {
                                        $this->msg = "Campo (" . $key . ") esta menor que o valor minimo";
                                        return false;
                                    }
                                    if (isset($condicao['max']) && strlen(utf8_decode($valor)) > $condicao['max']) {

                                        $this->msg = "Campo (" . $key . ") esta maior que o valor maximo";
                                        return false;
                                    }
                                }
                                if (isset($condicao['tipocliente']) && (!(($valor == 'pf') || ($valor == 'pj')))) {
                                    $this->msg = "Campo (" . $key . ") deve ser pf ou pj";
                                    return false;
                                }
                                if (isset($condicao['sexo']) && (!(($valor == 'M') || ($valor == 'F')))) {
                                    $this->msg = "Campo (" . $key . ") deve ser M ou F";
                                    return false;
                                }
                                if (isset($condicao['tipoEndCobranca']) && (!(($valor == 'Residencial') || ($valor == 'Comercial')))) {
                                    $this->msg = "Campo (" . $key . ") deve ser Residencial ou Comercial";
                                    return false;
                                }
                                if (isset($condicao['VendedorToken']) && $valor != '') {
                                    $dadosVendedor = Utils::encryptDecrypt('decrypt', $valor);
                                    $dadosVendedor = explode("|", $dadosVendedor);

                                    if (count($dadosVendedor) <= 1) {
                                        $this->msg = "Campo (" . $key . ") invalido";
                                        return false;
                                    }
                                }
                                if (isset($condicao['TiposAceitos']) && !in_array($valor, $condicao['TiposAceitos'])) {
                                    $this->msg = "Campo ({$key}) deve ser um dos seguintes: " . implode(", ", $condicao['TiposAceitos']);
                                    return false;
                                }
                            }

                            if (!is_string($valor)) {
                                $this->msg = "Campo (" . $key . ") deve ser string";
                                return false;
                            }
                            break;

                        case 'bool':
                            if ($valor != 1 && $valor != 0) {
                                $this->msg = "Campo (" . $key . ") deve ser booleano";
                                return false;
                            }
                            break;

                        case 'inscEstadual':
                            if (1 === (int)$objeto->clientePjIsento) {
                                $objeto->clientePjInscEstadual = "0";
                            }
                            break;

                        case 'date':
                            if (!$this->checaData($valor)) {
                                $this->msg = "Campo (" . $key . ") esta com a data no formato incorreto";
                                return false;
                            }
                            break;

                        case 'array':
                            if (!is_array($valor)) {
                                $this->msg = "Campo ({$key}) deve ser um array (lista)";
                                return false;
                            }

                            if (is_array($condicao)) {

                                if (isset($condicao['permitido']) && is_array($condicao['permitido'])) {
                                    $permitido = $condicao['permitido'];
                                    foreach ($valor as $field) {
                                        if (!in_array($field, $permitido)) {
                                            $this->msg = sprintf('Campo (%s) deve ser um dos seguintes valores (%s). Valor informado foi (%s)', $key, implode(', ', $permitido), $field);
                                            return false;
                                        }
                                    }
                                }

                                if (isset($condicao['Classe'])) {
                                    list($classe) = array_reverse(explode('\\', $condicao['Classe']));
                                    foreach ($valor as $item) {
                                        if (!is_object($item) || get_class($item) != $condicao['Classe']) {
                                            $this->msg = "Campo ({$key}) deve ser um array de objetos do tipo " . $classe;
                                            return false;
                                        }
                                    }
                                }

                                // Valida mínimo e máximo
                                if (isset($condicao['min']) && sizeof($valor) < intval($condicao['min'])) {
                                    $this->msg = sprintf("Tamanho do campo (%s) deve ser maior ou igual a %d. Tamanho informado foi %d.", $key, $condicao['min'], sizeof($valor));
                                    return false;
                                }

                                if (isset($condicao['max']) && sizeof($valor) > intval($condicao['max'])) {
                                    $this->msg = sprintf("Tamanho do campo (%s) deve ser menor ou igual a %d. Tamanho informado foi %d.", $key, $condicao['max'], sizeof($valor));
                                    return false;
                                }

                                if (isset($condicao['equals']) && sizeof($valor) != intval($condicao['equals'])) {
                                    $this->msg = sprintf("Tamanho do campo (%s) deve ser igual a %d. Tamanho informado foi %d.", $key, $condicao['equals'], sizeof($valor));
                                    return false;
                                }

                            }

                            break;

                        case 'int_array':
                            if (!is_array($valor)) {
                                $this->msg = "Campo ({$key}) deve ser um array (lista)";
                                return false;
                            } else {
                                if (in_array(false, array_map(array('\App\Helper\Validacao', "isIntArrayCallback"), $valor))) {
                                    $this->msg = "Campo ({$key}) deve ser um array (lista) contendo apenas inteiros";
                                    return false;
                                }
                            }
                            break;
                        case 'null':
                            if (!is_null($valor)) {
                                $this->msg = "Campo ({$key}) deve ser nulo";
                                return false;
                            }
                            break;

                        //Caso de PedidoProduto
                        case 'PP':
                            if (is_array($objeto->pedidoProduto)) {
                                $valorDescontoTotal = 0;
                                foreach ($objeto->pedidoProduto as $produto) {
                                    if ($produto->codigoProduto <= 0 || empty($produto->codigoProduto)) {
                                        $this->msg = "Campo (" . $key . ") o codigo do produto esta invalido";
                                        return false;
                                    } else if ($produto->quantidade <= 0) {
                                        $this->msg = "Campo (" . $key . ") esta com a quantidade incorreta";
                                        return false;
                                    } else if ($produto->valorUnitario <= 0) {
                                        $this->msg = "Campo (" . $key . ") esta com o valor unitario incorreto";
                                        return false;
                                    } else if ($produto->valorTotal <= 0) {
                                        $this->msg = "Campo (" . $key . ") esta com o valor total incorreto";
                                        return false;
                                    } else if ($produto->tipo != '1') {
                                        $this->msg = "Campo (" . $key . ") nao esta com o codigo de produto simples ";
                                        return false;
                                    } else if (empty($produto->prazoEntregaCombinado) || $produto->prazoEntregaCombinado < 3) {
                                        $this->msg = "Campo (" . $key . ") esta com prazo de entrega combinado menor que 3 ";
                                        return false;
                                    } else if ($produto->valorDesconto < 0) {
                                        $this->msg = "Campo (" . $key . ") esta com valor negativo ";
                                        return false;
                                    } else if ($produto->valorFrete < 0) {
                                        $this->msg = "Campo (" . $key . ") do produto '" . $produto->codigoProduto . "' esta com valor negativo ";
                                        return false;
                                    }

                                    $valorDescontoTotal += $produto->valorDesconto * $produto->quantidade;
                                }
                            }

                            break;

                        //Caso de Pagamento
                        case 'PAG':
                            if (is_array($objeto->pagamento)) {
                                $bandeiras = array('1', '2', '3', '4', '6', '13');
                                foreach ($objeto->pagamento as $pagamento) {

                                    //cartao
                                    if ($pagamento->meioPagamentoId == '1') {

                                        $cartaoValidade = explode('/', $pagamento->cartaoValidade);
                                        list($mesValidade, $anoValidade) = $cartaoValidade;
                                        if (!in_array($pagamento->bandeiraId, $bandeiras)) {
                                            $this->msg = "Campo (" . $key . ") contem uma bandeira incorreta";
                                            return false;
                                        } else if (($pagamento->parcelas < 1) || ($pagamento->parcelas > 12)) {
                                            $this->msg = "Campo (" . $key . ") esta com o parcelamento incorreto";
                                            return false;
                                        } else if (strlen($pagamento->cartaoNumero < 6) || (strlen($pagamento->cartaoNumero) > 19)) {
                                            $this->msg = "Campo (" . $key . ") esta com o numero de cartao incorreto";
                                            return false;
                                        } else if (count($cartaoValidade) != 2) {
                                            $this->msg = "Campo (" . $key . ") esta com a data incorreta ";
                                            return false;
                                        } else if ($mesValidade > 12 || intval(date('Ym')) > intval(sprintf('%s%s', $anoValidade, $mesValidade))) {
                                            $this->msg = "Campo (" . $key . ") esta com a validade de cartao incorreta ";
                                            return false;
                                        } else if (strlen($pagamento->cartaoCodSeguranca) < 1 || (strlen($pagamento->cartaoCodSeguranca) > 5)) {
                                            $this->msg = "Campo (" . $key . ") esta com o codigo de seguranca incorreto";
                                            return false;
                                        } else if ((strlen($pagamento->cartaoNomePortador) < 3) || (strlen(utf8_decode($pagamento->cartaoNomePortador)) > 30)) {
                                            $this->msg = "Campo (" . $key . ") Nome do portador do cartao tem que ter 3 a 30 caracteres";
                                            return false;
                                        }
                                        // boleto
                                    } else if ($pagamento->meioPagamentoId == '2') {

                                        // Dinheiro / Boleto Pos Pago
                                    } else if ($pagamento->meioPagamentoId == '7') {

                                        if (($pagamento->parcelas < 1) || ($pagamento->parcelas > 12)) {
                                            $this->msg = "Campo (parcelas) esta com o parcelamento incorreto";
                                            return false;
                                        } else if (!isset($pagamento->valorPago) || $pagamento->valorPago == '') {
                                            $this->msg = "Campo (valorPago) não pode ser vazio";
                                            return false;
                                        } else if (!isset($pagamento->sequencial) || $pagamento->sequencial == '') {
                                            $this->msg = "Campo (sequencial) não pode ser vazio";
                                        }
                                    } else if ($pagamento->meioPagamentoId == '8' && empty($pagamento->pontos)) {
                                            $this->msg = "Campo (" . $key . ") nao foi informado os pontos para o tipo de pagamento 8";
                                            return false;
                                    } elseif (!in_array($pagamento->meioPagamentoId, Config::get('pedidos.meios_pagamentos_aceitos'))) {
                                        $this->msg = "Campo (" . $key . ") contem um meio de pagamento invalido ";
                                        return false;
                                        // Meio de pagamento novo: Cartão PAGO
                                    } else if ($pagamento->meioPagamentoId == '9') {
                                        $teste = $this->validaCartaoPago($pagamento, $bandeiras);

                                        if (!$teste) {
                                            return false;
                                        }

                                    } elseif (!in_array($pagamento->meioPagamentoId, Config::get('pedidos.meios_pagamentos_aceitos'))) {
                                        $this->msg = "Campo (" . $key . ") contem um meio de pagamento invalido ";
                                        return false;
                                    }

                                    if (($pagamento->valorPago) <= 0 && (float)$pagamento->pontos <= 0) {
                                        $this->msg = "Campo (" . $key . ") esta com o valor pago incorreto ";
                                        return false;
                                    }
                                }
                            }
                            break;

                        case 'cpf':
                            // Array de CPFs inválidos
                            $cpfsInvalidos = array(
                                '00000000000',
                                '11111111111',
                                '22222222222',
                                '33333333333',
                                '44444444444',
                                '55555555555',
                                '66666666666',
                                '77777777777',
                                '88888888888',
                                '99999999999',
                            );
                            
                            // Elimina possivel mascara
                            $cpf = preg_replace('/[^0-9]/', '', $valor);
                            $cpf = str_pad($cpf, 11, '0', STR_PAD_LEFT);

                            // Verifica se o numero de digitos informados é igual a 11
                            if (strlen($cpf) != 11 || in_array($cpf, $cpfsInvalidos)) {
                                $this->msg = "Campo ({$key}) não é um CPF válido.";
                                return false;
                            } else {
                                for ($t = 9; $t < 11; $t++) {
                                    for ($d = 0, $c = 0; $c < $t; $c++) {
                                        $d += $cpf{$c} * (($t + 1) - $c);
                                    }
                                    $d = ((10 * $d) % 11) % 10;
                                    if ($cpf{$c} != $d) {
                                        $this->msg = "Campo ({$key}) não é um CPF válido.";
                                        return false;
                                    }
                                }
                            }
                            break;

                        case 'cnpj':
                            // Array de CNPJs inválidos
                            $cnpjsInvalidos = array(
                                '00000000000000',
                                '11111111111111',
                                '22222222222222',
                                '33333333333333',
                                '44444444444444',
                                '55555555555555',
                                '66666666666666',
                                '77777777777777',
                                '88888888888888',
                                '99999999999999',
                            );

                            $cnpj = preg_replace('/[^0-9]/', '', $valor);
                            $cnpj = str_pad($cnpj, 14, '0', STR_PAD_LEFT);

                            if (strlen($cnpj) != 14 || in_array($cnpj, $cnpjsInvalidos)) {
                                $this->msg = "Campo ({$key}) não é um CNPJ válido.";
                                return false;
                            } else {
                                $tamanho = strlen($cnpj) - 2;
                                $numeros = substr($cnpj, 0, $tamanho);
                                $digitos = substr($cnpj, $tamanho);
                                $soma = 0;
                                $pos = $tamanho - 7;
                                for ($i = $tamanho; $i >= 1; $i--) {
                                    $soma += $numeros[$tamanho - $i] * $pos;
                                    $pos = $pos - 1;
                                    if ($pos < 2) {
                                        $pos = 9;
                                    }
                                }
                                $resultado = $soma % 11 < 2 ? 0 : 11 - $soma % 11;
                                if ($resultado != $digitos[0]) {
                                    $this->msg = "Campo ({$key}) não é um CNPJ válido.";
                                    return false;
                                }

                                $tamanho = $tamanho + 1;
                                $numeros = substr($cnpj, 0, $tamanho);
                                $soma = 0;
                                $pos = $tamanho - 7;
                                for ($i = $tamanho; $i >= 1; $i--) {
                                    $soma += $numeros[$tamanho - $i] * $pos;
                                    $pos = $pos - 1;
                                    if ($pos < 2) {
                                        $pos = 9;
                                    }
                                }
                                $resultado = $soma % 11 < 2 ? 0 : 11 - $soma % 11;
                                if ($resultado != $digitos[1]) {
                                    $this->msg = "Campo ({$key}) não é um CNPJ válido.";
                                    return false;
                                }
                            }
                            break;

                        case 'email':
                            //$regex = "/^(?=[A-Z0-9][A-Z0-9@._%+-]{5,253}+$)[A-Z0-9._%+-]{1,64}+@(?:(?=[A-Z0-9-]{1,63}+\\.)[A-Z0-9]++(?:-[A-Z0-9]++)*+\\.){1,8}+[A-Z]{2,63}+$/i";
                            $email = filter_var($valor, FILTER_VALIDATE_EMAIL);
                            if (empty($email)) {
                                $this->msg = "Campo ({$key}) não contém um e-mail válido";
                                return false;
                            }
                            break;

                        case 'ip':
                            $regex = '/^(?:\d{1,3}\.){3}\d{1,3}$/i';
                            if (!preg_match($regex, trim($valor))) {
                                $this->msg = sprintf("Campo (%s) deve conter um IP no formato válido.", $key);
                                return false;
                            }
                            break;
                        case 'tiposaceitos':

                            $valido = false;
                            foreach ($condicao as $innerKey => $value) {
                                if (is_numeric($innerKey)) {
                                    $valido = static::validar($valor, $value);
                                } else {
                                    $valido = static::validar($valor, array($innerKey => $value));
                                }
                                if ($valido) {
                                    break;
                                }
                            }
                            $tipos = array();
                            foreach ($condicao as $innerKey => $innerValue) {
                                if (is_numeric($innerKey)) {
                                    $tipos[] = $innerValue;
                                } else {
                                    $tipos[] = $innerKey;
                                }
                            }
                            if (!$valido) {
                                $this->msg = sprintf("Campo ({$key}) deve ser do(s) tipo(s) (%s).", implode(", ", $tipos));
                                return $valido;
                            }
                        break;
                        case 'cep':
                            $cep = preg_replace('/\D/i', '', $valor);

                            if(strlen($cep) != 8) {
                                $this->msg = "Campo (" . $key . ") nao atende o valor correto esperado - (Cep: " . $valor . ")";
                                return false;
                            }

                        break;

                    }
                }
            }
        }

        return true;
    }

    private static function isIntArrayCallback($item) {
        return is_numeric($item);
    }

    private function checaData($data) {
        $arrayData = explode('-', $data);
        $resultado = true;
        if (strlen($data) != 10 || count($arrayData) != 3) {
            $resultado = false;
        } else {
            $resultado = checkdate($arrayData[1], $arrayData[2], $arrayData[0]);
        }
        return $resultado;
    }

    /**
     * Método de validação para meio de pagamento 9: Cartão Pago, que requer os seguintes campos:
     *
     * bandeiraId, parcelas, cartaoNumeroResumido, cartaoValidade, cartaoCodSeguranca, cartaoNomePortador, nsu,
     * autorizadaoId, dataAutorizacao, dataCaptura, adquirente
     *
     * @param $pagamento
     * @param $bandeiras
     * @return bool
     */
    private function validaCartaoPago($pagamento, $bandeiras) {
        $result = true;

        if (isset($pagamento->dataAutorizacao) || $pagamento->dataAutorizacao != '') {
            $data = \DateTime::createFromFormat('Y-m-d H:i:s', $pagamento->dataAutorizacao);
            $dateInfo = \DateTime::getLastErrors();

            if (!$data instanceof \DateTime || $dateInfo['warning_count'] > 0 || $dateInfo['error_count'] > 0) {
                $this->msg = "Campo (dataAutorizacao) deve conter data válida: aaaa-mm-dd hh:mm:ss";
                $result = false;
            }

            $now = strtotime(date('Y-m-d H:i:s'));
            $time = strtotime($data->format('Y-m-d H:i:s'));

            if ($time > $now) {
                $this->msg = "Campo (dataAutorizacao) não pode ser uma data futura";
                $result = false;
            }
        } else {
            $this->msg = "Campo (dataAutorizacao) não pode ser vazio";
            $result = false;
        }

        if (!$result) {
            return $result;
        }

        $cartaoValidade = explode('/', $pagamento->cartaoValidade);
        list($mesValidade, $anoValidade) = $cartaoValidade;

        if (!in_array($pagamento->bandeiraId, $bandeiras)) {
            $this->msg = "Campo (bandeiraId) contem uma bandeira incorreta";
            $result = false;
        } else if (($pagamento->parcelas < 1) || ($pagamento->parcelas > 12)) {
            $this->msg = "Campo (parcelas) esta com o parcelamento incorreto";
            $result = false;
        } else if (strlen($pagamento->cartaoNumero < 6) || (strlen($pagamento->cartaoNumero) > 19)) {
            $this->msg = "Campo (cartaoNumero) esta com o numero de cartao incorreto";
            $result = false;
        } else if (count($cartaoValidade) != 2) {
            $this->msg = "Campo (cartaoValidade) esta com a data incorreta ";
            $result = false;
        } else if ($mesValidade > 12 || intval(date('Ym')) > intval(sprintf('%s%s', $anoValidade, $mesValidade))) {
            $this->msg = "Campo (cartaoValidade) esta com a validade de cartao incorreta ";
            $result = false;
        } else if ((strlen($pagamento->cartaoNomePortador) < 3) || (strlen(utf8_decode($pagamento->cartaoNomePortador)) > 30)) {
            $this->msg = "Campo (cartaoNomePortador) Nome do portador do cartao tem que ter 3 a 30 caracteres";
            $result = false;
        } else if (!isset($pagamento->nsu) || $pagamento->nsu == '') {
            $this->msg = "Campo (nsu) é obrigatório para o meio de pagamento Cartão Pago";
            $result = false;
        } else if (!isset($pagamento->autorizacaoId) || $pagamento->autorizacaoId == '') {
            $this->msg = "Campo (autorizacaoId) é obrigatório para o meio de pagamento Cartão Pago";
            $result = false;
        } else if (!isset($pagamento->adquirente) || $pagamento->adquirente == '' || !is_numeric($pagamento->adquirente)) {
            $this->msg = "Campo (adquirente) é obrigatório para o meio de pagamento Cartão Pago";
            $result = false;
        }

        return $result;
    }
}
