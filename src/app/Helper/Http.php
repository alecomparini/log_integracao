<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 15-02-2016
 * Time: 16:11
 */

namespace App\Helper;

/**
 * Realiza operações wrapper HTTP
 * Class Http
 * @package App\Helper
 */
class Http {

    const OK = 200;
    const NOT_FOUND = 404;
    const ERRO_PRECONDICAO = 412;
    
    public $responseCode;
    public $body;
    public $contentType;

    /**
     * @param $url
     * @return \App\Helper\Http
     */
    public static function get($url) {
        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'GET');
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        $response = curl_exec($ch);
        $httpStatus = curl_getinfo($ch);

        $obj = new static();
        $obj->responseCode = $httpStatus['http_code'];
        $obj->contentType = $httpStatus['content_type'];
        $obj->body = $response;
        return $obj;
    }

    /**
     * Realiza uma chamada post
     * @param $url
     * @param $fields
     * @param bool $isJson
     * @return \App\Helper\Http
     */
    public static function post($url, $fields, $isJson = false, $headers = array()) {
        $postData = $isJson ? json_encode($fields, JSON_NUMERIC_CHECK) : http_build_query($fields);

        $ch = curl_init($url);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, 'POST');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);

        if ($isJson) {
            $headers = array_merge($headers, array(
                'Content-Type: application/json',
                'Accept: application/json',
                'Content-Lenght: ' . strlen($postData)
            ));
        }

        if (sizeof($headers) > 0) {
            curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
        }

        $response = curl_exec($ch);
        $httpStatus = curl_getinfo($ch);

        $obj = new static();
        $obj->responseCode = $httpStatus['http_code'];
        $obj->contentType = $httpStatus['content_type'];
        $obj->body = $response;

        return $obj;
    }
}