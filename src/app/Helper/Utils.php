<?php
namespace App\Helper;

use App\Model\Model;

/**
 * Class Utils
 *
 * Métodos utilitários para o sistema
 *
 * @package App\Helper
 */
class Utils {

    /**
     * Limpa a string de acentuações e caracteres especiais
     *
     * @param $string
     * @return mixed|string
     */
    public static function limpaString($string) {
        $na = array(
            "'[ÁÀÃÄÂ]'" => "A",
            "'[àáâãä]'" => "a",
            "'[ÉÈÊË]'" => "E",
            "'[éèêë]'" => "e",
            "'[ÌÍîÏ]'" => "I",
            "'[íìîï]'" => "i",
            "'[ÓÒÔÕÖ]'" => "O",
            "'[óòõôö]'" => "o",
            "'[ÚÙÛÜ]'" => "U",
            "'[úùûü]'" => "u",
            "'ç'" => "c",
            "'Ç'" => "C"
        );

        $a = array();
        foreach ($na as $k => $val) {
            $a[mb_convert_encoding($k, 'ISO-8859-1', 'UTF-8')] = $val;
        }

        $string = preg_replace(array_keys($a), array_values($a), $string);

        $notAcceptableCharactersRegex = '#[^-a-zA-Z0-9_ ]#';

        $string = preg_replace($notAcceptableCharactersRegex, '', $string);
        $string = trim($string);
        return preg_replace('#[-_ ]+#', '-', $string);
    }

    public static function removeEspeciais($str, $replace = '') {
        $regex = '/[^a-zA-ZÀ-ÿ\s]/';
        return preg_replace($regex, $replace, $str);
    }

    /**
     * Converte qualquer coisa pra json convertendo encoding pra UTF-8
     *
     * @param mixed $param
     * @param int|null $flags
     * @return string
     */
    public static function jsonEncode($param, $flags = null) {
        $toUtf8 = static::utf8Convert($param);
        if (!is_null($flags)) {
            return json_encode($toUtf8, $flags);
        }
        return json_encode($toUtf8);
    }

    /**
     * Encoda de maneira eficiente array ou objeto para JSON
     *
     * @param $param
     * @return mixed
     */
    public static function utf8Convert($param) {
        if (is_object($param)) {
            if ($param instanceof Model) {
                $param = $param->jsonSerialize();
            }
            $properties = array_keys(get_object_vars($param));

            foreach ($properties as $prop) {

                $value = $param->$prop;
                if (is_array($value) || is_object($value)) {
                    $value = static::utf8Convert($value);
                } else {
                    if (mb_detect_encoding($value, 'UTF-8', true) === false) {
                        $value = mb_convert_encoding($value, 'UTF-8', 'ISO-8859-1');
                    }
                }

                $param->$prop = $value;
            }

        } elseif (is_array($param)) {

            foreach ($param as $key => $value) {
                if (is_array($value) || is_object($value)) {
                    $param[$key] = static::utf8Convert($value);
                } else {
                    if (!is_bool($value) && !mb_detect_encoding($value, 'UTF-8', true)) {
                        $param[$key] = mb_convert_encoding($value, 'UTF-8', 'ISO-8859-1');
                    }
                }
            }

        } else {
            if (!is_bool($param) && mb_detect_encoding($param, 'ISO-8859-1, UTF-8') == 'ISO-8859-1') {
                $param = mb_convert_encoding($param, 'UTF-8', 'ISO-8859-1');
            }
        }

        return $param;
    }

    /**
     * Criptografa a senha
     * @param $senha
     * @return string
     */
    public static function criptSenha($senha) {
        return md5(substr(md5($senha), 6));
    }

    /**
     * Criptografa senhas para o B2C
     * @param $senha
     * @return string
     */
    public static function criptSenhaB2C($senha) {
        $chave = "A0334mJS93m3jfb&SFJF#$&*#,DH";
        return md5($senha . $chave);
    }

    /**
     * Encripta ou decrypta alguma string baseada em uma chave padrão
     * @param $action
     * @param $string
     * @return bool|string
     */
    public static function encryptDecrypt($action, $string) {
        $output = false;

        $key = 'sA*(DH';

        // initialization vector
        $iv = md5(md5($key));

        if ($action == 'encrypt') {
            $output = mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($key), $string, MCRYPT_MODE_CBC, $iv);
            $output = base64_encode($output);
        } else if ($action == 'decrypt') {
            $output = mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($key), base64_decode($string), MCRYPT_MODE_CBC, $iv);
            $output = rtrim($output, "");
        }
        return $output;
    }

    /**
     * Formata o número recebido em formato de string para formato de moeda em Real
     * @param float $number
     * @return string
     */
    public static function formataMoeda($number) {
        return number_format($number, 2, ",", ".");
    }

    /**
     * Recebe uma string formatada em moeda R$ e retorna valor numérico respectivo em double
     *
     * @param $string
     * @return float
     */
    public static function formataNumero($string) {
        $valor = str_replace('.', '', $string);
        $valor = str_replace(',', '.', $valor);

        return (double)$valor;
    }

    /**
     * Recebe um float e arredonda ele evitando o problema de float precision da linguagem
     * Sempre utilizar antes de fazer operações matemáticas com float
     * @param $float
     * @return float
     */
    public static function convertMoeda($float) {
        return round($float * 100) / 100;
    }

    /**
     * Adicionar x dias úteis para uma determinada data
     * @param $strData
     * @param int $intQtdDiasSomar
     * @return string
     */
    public static function adicionarDiasUteis($strData, $intQtdDiasSomar = 7) {
        // Caso seja informado uma data do MySQL do tipo DATETIME - aaaa-mm-dd 00:00:00
        // Transforma para DATE - aaaa-mm-dd
        $strData = substr($strData, 0, 10);

        // Se a data estiver no formato brasileiro: dd/mm/aaaa
        // Converte-a para o padrão americano: aaaa-mm-dd
        if (preg_match("@/@", $strData) == 1) {
            $strData = implode("-", array_reverse(explode("/", $strData)));
        }
        $arrayData = explode('-', $strData);
        $countDays = 0;
        $intQtdDiasUteis = 0;
        while ($intQtdDiasUteis < $intQtdDiasSomar) {
            $countDays++;
            if (($diasDaSemana = gmdate('w', strtotime('+' . $countDays . ' day', mktime(0, 0, 0, $arrayData[1], $arrayData[2], $arrayData[0])))) != '0' && $diasDaSemana != '6') {
                $intQtdDiasUteis++;
            }
        }

        return gmdate('Y-m-d', strtotime('+' . $countDays . ' day', strtotime($strData)));
    }

    /**
     * Converte, recursivamente, todas as propriedades com o nome iniciando com minúscula
     * @param $obj
     * @return array
     */
    public static function lcfirstRecursivo($obj) {
        if (is_object($obj)) {
            if ($obj instanceof Model) {
                $obj = $obj->jsonSerialize();
            }
            $tmp = new \stdClass();
        } else {
            $tmp = array();
        }
        foreach ($obj as $p => $v) {
            if (!is_array($v) && !is_object($v)) {
                if (is_array($obj)) {
                    $tmp[lcfirst($p)] = $v;
                } else {
                    if (is_object($obj)) {
                        $tmp->{lcfirst($p)} = $v;
                    }
                }
            } else {
                if (is_array($obj)) {
                    $tmp[lcfirst($p)] = static::lcfirstRecursivo($v);
                } else {
                    if (is_object($obj)) {
                        $tmp->{lcfirst($p)} = static::lcfirstRecursivo($v);
                    }
                }
            }
        }
        return $tmp;
    }

    /**
     * Converte, recursivamente, todas as propriedades nome o nome iniciando com maiúscula
     * @param $obj
     * @return array|\stdClass
     */
    public static function ucfirstRecursivo($obj) {
        if (is_object($obj)) {
            $tmp = new \stdClass();
        } else {
            $tmp = array();
        }
        foreach ($obj as $p => $v) {
            if (!is_array($v) && !is_object($v)) {
                if (is_array($obj)) {
                    $tmp[ucfirst($p)] = $v;
                } else {
                    if (is_object($obj)) {
                        $tmp->{ucfirst($p)} = $v;
                    }
                }
            } else {
                if (is_array($obj)) {
                    $tmp[ucfirst($p)] = static::ucfirstRecursivo($v);
                } else {
                    if (is_object($obj)) {
                        $tmp->{ucfirst($p)} = static::ucfirstRecursivo($v);
                    }
                }
            }
        }
        return $tmp;
    }

    public static function camelToSnake($str) {
        $tmp = lcfirst($str);
        return strtolower(preg_replace('/(?<=.)([A-Z])/m', '_$1', $tmp));
    }

    /**
     * Dado uma string de entrada contendo um valor numérico (ou não),
     * é removido todos os caracteres que não são números
     * @param string $str
     * @return string
     */
    public static function soNumeros($str) {
        return preg_replace('/\D/i', '', $str);
    }

    public static function aplicaSerializacao($item) {
        if (is_object($item) && method_exists($item, 'jsonSerialize')) {
            return $item->jsonSerialize();
        }
        return $item;
    }

    public static function sanitizeString($sql) {
        return str_replace("'", '', $sql);
    }

    /**
     * Faz o que o nome da função diz, converte minutos em segundos
     * @param int $minutes
     * @return int
     */
    public static function secondsFromMinutes($minutes) {
        return $minutes * 60;
    }
    public static function secondsFromHours($hours) {
        return $hours * 60 * 60;
    }
    public static function secondsFromTime($time) {
        $regex = '/^(?:(\d*)\s)?([0-1][\d]|2[0-4]):([0-5][\d]):([0-5][\d])$/mi';
        if (preg_match($regex, $time, $matches)) {
            echo "<pre>";
            print_r($matches);
            exit;
        }
        return 0;
    }

    /**
     * Pega o último pedaço de uma string baseado em um delimitador
     * 
     * @param string $delimitador
     * @param string $texto
     *
     * @return string
     */
    public static function recuperaUltimoPedacoTextoSeparador($delimitador, $texto) {
        $temp = explode($delimitador, $texto);
        return end($temp);
    }
    
    /**
     * Retorna o diretório temporário do sistema
     * @return string
     */
    public static function getTempDir() {
    	return sys_get_temp_dir();
    }
    
}