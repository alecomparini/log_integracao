<?php
use App\Core\Router;

$router = Router::getInstance();

$router->get("/", "IndexController::index");
$router->post("/InterfaceTracking/Consulta", 'Rest\V1\TrackingController::consulta');
$router->get("/ws/{class}", 'Soap\V1\WsdlController::index', array("_params" => array("wsdl" => true)));
$router->post("/ws/{class}", 'Soap\V1\WsdlController::index');

//Rotas para REST V2
$router->get("/v2/produto", 'Rest\V2\ProdutoController::consulta');
$router->post("/v2/pedido", 'Rest\V2\PedidoController::criar');
$router->get('/v2/vendedor', 'Rest\V2\VendedorController::index');
$router->get('/v2/estoque', 'Rest\V2\ProdutoController::getEstoquePlus');
$router->get('/v2/estoqueRetail', 'Rest\V2\ProdutoController::getEstoqueRetail');
$router->get('/v2/interface', 'Rest\V2\InterfaceController::setInterfaceConfirmacao');
$router->get('/v2/dados', 'Rest\V2\DadosController::getDados');
$router->post('/v2/dados', 'Rest\V2\DadosController::confirmar');
$router->get('/v2/parcelamento', 'Rest\V2\DadosController::parcelamento');
$router->get('/v2/tracking', 'Rest\V2\TrackingController::getTracking');
$router->get('/v2/frete', 'Rest\V2\FreteController::consulta');
$router->post('/v2/statusPagamento', 'Rest\V2\PagamentoController::setStatusPagamento');

$router->get('/v2/bandeira', 'Rest\V2\DadosController::bandeira');
$router->get('/v2/bandeira/{id}', 'Rest\V2\DadosController::bandeira');
$router->get('/v2/pedido_parceiro', 'Rest\V2\PedidoParceiroController::index');
$router->get('/v2/campanha', 'Rest\V2\ProdutoController::getCampanha');

//Rotas site
$router->get('/manual', 'ManualController::v3');
$router->get('/manual/v1', 'ManualController::v1');
$router->get('/v1', 'ManualController::v1');

$router->get('/manual/v2', 'ManualController::v2');
$router->get('/v2', 'ManualController::v2');

$router->get('/manual/v3', 'ManualController::v3');
$router->get('/v3', 'ManualController::v3');

$router->get('/softbox/test', 'TestController::index');
$router->get('/reports/estoque', 'ReportsController::estoque');


//Rotas serviços
$router->post('/servicos/notificacao', 'Rest\Servicos\ServicosController::setNotificacao');
$router->post('/servicos/notificacao_estorno_pontos', 'Rest\Servicos\ServicosController::setNotificacaoEstornoPontos');
$router->get('/servicos/notificar_estorno_pontos', 'Rest\Servicos\ServicosController::notificarEstornoPontos');
$router->post('/servicos/enviarNotificacao', 'Rest\Servicos\ServicosController::enviarNotificacao');
$router->get('/servicos/notificar', 'Rest\Servicos\ServicosController::notificar');
$router->get('/servicos/notificar2', 'Rest\Servicos\ServicosController::notificar2');
$router->get('/servicos/notificarFilaRapida', 'Rest\Servicos\ServicosController::notificarFilaRapida');
$router->get('/servicos/notificarFilaLenta', 'Rest\Servicos\ServicosController::notificarFilaLenta');
$router->get('/servicos/processarPrePedido', 'Rest\Servicos\ServicosController::processarPrePedidos');
$router->get('/servicos/processarPrePedido/{dadosImportacaoId}', 'Rest\Servicos\ServicosController::processarPrePedido');
$router->get('/servicos/cancelarPrePedido', 'Rest\Servicos\ServicosController::cancelarPrePedidos');
$router->get('/servicos/cancelarPrePedido/{dadosImportacaoId}', 'Rest\Servicos\ServicosController::cancelarPrePedido');

$router->post('/servicos/parceiro/sku/{parceiroId}/{codigo}', 'Rest\Servicos\ParceiroController::getSkuParceiro');

$router->get('/estatisticas/notificacoes', 'Rest\EstatisticasController::index');
$router->get('/estatisticas/notificacoes/restante', 'Rest\EstatisticasController::notificacoesRestante');
$router->get('/estatisticas/notificacoes/erro', 'Rest\EstatisticasController::notificacoesErro');
$router->get('/servicos/notificar_tracking', 'Rest\Servicos\ServicosController::notificarTracking');
$router->post('/servicos/notificacao_tracking', 'Rest\Servicos\ServicosController::setNotificacaoTracking');

// Rotas tarefas
$router->get('/servicos/tarefas', 'TarefasController::index');
$router->get('/servicos/tarefas/iniciar', 'TarefasController::iniciar');
$router->get('/servicos/tarefas/executar/{id}', 'TarefasController::executar', array('_params' => array('forcado' => false)));
$router->get('/servicos/tarefas/executar/{id}/forcado', 'TarefasController::executar', array('_params' => array('forcado' => true)));

$router->get('/login', 'LoginController::index');
$router->get('/access_token', 'LoginController::accessToken');
$router->delete('/invalidar_access_token', 'LoginController::invalidarAccessToken');
$router->get('/v3/access_token', 'LoginController::accessToken');
$router->delete('/v3/invalidar_access_token', 'LoginController::invalidarAccessToken');

$router->get('/softbox/sleep/{delay}', 'TestController::sleep');

/* Exemplos de rotas com middlewares */
function closureAuth(Router $router) {
    $router->get('/closure', 'TestController::closure');
    $router->get('/no-closure', 'TestController::closureOff');
}
$router->group(array('middleware' => array('auth')), 'closureAuth');

$router->get('/asset/{module}/{type}/{file}', 'AssetController::getAsset');

function loggedInAuth(Router $router) {
    $router->get('/v2/tarefas', 'Rest\V2\TarefasController::index');

    //Rotas para REST V3 CORRIGIDAS
    $router->get('/v3/frete', 'Rest\V3\FreteController::consulta');
    $router->get('/v3/parcelamento', 'Rest\V3\DadosController::parcelamento');
    $router->get('/v3/vendedor', 'Rest\V3\VendedorController::index');

    //NEED FIX
    $router->get("/v3/produto", 'Rest\V3\ProdutoController::consulta');
    $router->post("/v3/pedido", 'Rest\V3\PedidoController::criar');
    $router->get('/v3/estoque', 'Rest\V3\ProdutoController::getEstoquePlus');
    $router->get('/v3/estoqueRetail', 'Rest\V3\ProdutoController::getEstoqueRetail');
    $router->get('/v3/interface', 'Rest\V3\InterfaceController::setInterfaceConfirmacao');
    $router->get('/v3/dados', 'Rest\V3\DadosController::getDados');
    $router->post('/v3/dados', 'Rest\V3\DadosController::confirmar');
    $router->get('/v3/tracking', 'Rest\V3\TrackingController::getTracking');

    $router->get('/v3/bandeira', 'Rest\V2\DadosController::bandeira');
    $router->get('/v3/bandeira/{id}', 'Rest\V2\DadosController::bandeira');
    $router->get('/v3/pedido_parceiro', 'Rest\V2\PedidoParceiroController::index');
    $router->get('/v3/campanha', 'Rest\V2\ProdutoController::getCampanha');

    $router->post('/access_token', 'LoginController::criarTokenEstendido');

    $router->get('/v3/buscarPedidosComErro', 'Rest\Servicos\ServicosController::buscarPedidosComErro');
}
$router->group(array('middleware' => array('loggedIn')), 'loggedInAuth');
$router->get('/admin', 'Admin\DashboardController::index');
$router->get('/admin/tarefas', 'Admin\DashboardController::tarefas');
$router->get('/admin/pedido/erro', 'Admin\DashboardController::pedidosErro');