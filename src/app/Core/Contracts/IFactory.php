<?php
namespace App\Core\Contracts;

interface IFactory {
    public static function create($type);
}