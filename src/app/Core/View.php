<?php
namespace App\Core;

/**
 * Class View
 * @package App\Core
 */
class View {
    private $template;
    private $engine;
    private $dados;
    
    public function __construct() {
        $loader = new \Twig_Loader_Filesystem(APP_DIR . 'views');
        $twig = new \Twig_Environment($loader);
        $this->engine = $twig;
    }
    
    public function setTemplate($file) {
        $this->template = $file;
    }
    
    public function setDados($dados) {
        $this->dados = $dados;
    }
    
    public function set($chave, $valor) {
        $this->dados[$chave] = $valor;
    }
    
    public function delete($chave) {
        if (array_key_exists($chave, $this->dados)) {
            unset($this->dados[$chave]);
        }
    }
    
    public function render() {
        return $this->engine->render($this->template, $this->dados);
    }
    
    public function show($file, $viewData = array()) {
        $this->setTemplate($file);
        $this->setDados($viewData);
        return $this->render();
    }
}