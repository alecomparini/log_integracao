<?php
namespace App\Core;

use App\Core\Exception\PhpRuntimeException;
use App\Exceptions\DatabaseException;
use Mysqli;

/**
 * Classe responsável por encapsular e mascarar como
 * são feitas as chamadas ao banco de dados
 */
class Database {

    public static $hasConnection = false;

    const STRING = 'string';
    const INT = 'int';
    const FLOAT = 'float';

    const TYPE_MASTER = 'master';
    const TYPE_SLAVE  = 'slave';

    /** @var MySqli */
    private $linkMaster;

    /** @var MySqli */
    private $linkSlave;

    /** @var string */
    private $hostMaster;

    /** @var string */
    private $portMaster;

    /** @var string */
    private $userMaster;

    /** @var string */
    private $passMaster;

    /** @var string */
    private $baseMaster;

    /** @var string */
    private $hostSlave;

    /** @var string */
    private $portSlave;

    /** @var string */
    private $userSlave;

    /** @var string */
    private $passSlave;

    /** @var string */
    private $baseSlave;

    /** @var string */
    private $sqlPath;

    /** @var string */
    protected $lastQuery = null;

    /** @var null|boolean Keep track of the last commit success or failure. null if no previous commit */
    private $lastCommitStatus = null;

    /** @var bool Keep track if the last commit was really executed on the database or was just a virtual commit */
    private $isRealCommit = false;

    /** @var Database */
    private static $instance;

    private $hasOpenTransaction = false;
    private $qtdStartTransaction = 0;

    /**
     * Construtor padrão
     * Cria as instâncias master e slave do banco de dados
     *
     * @throws \App\Exceptions\DatabaseException
     */
    private function __construct() {
        $this->hostMaster = Config::get('db.master.host');
        $this->portMaster = Config::get('db.master.port');
        $this->userMaster = Config::get('db.master.user');
        $this->passMaster = Config::get('db.master.pass');
        $this->baseMaster = Config::get('db.master.base');

        $this->hostSlave = Config::get('db.slave.host');
        $this->portSlave = Config::get('db.slave.port');
        $this->userSlave = Config::get('db.slave.user');
        $this->passSlave = Config::get('db.slave.pass');
        $this->baseSlave = Config::get('db.slave.base');
    }

    /**
     * Obtem um conexao com o banco de leitura.
     *
     * @throws DatabaseException
     *
     * @return \App\Core\MySqli Conexao.
     */
    private function getLinkSlave() {
        if (!empty($this->linkSlave)) {
            return $this->linkSlave;
        }

        try {
            $this->linkSlave = new Mysqli($this->hostSlave, $this->userSlave, $this->passSlave, $this->baseSlave, $this->portSlave);
        } catch (PhpRuntimeException $e) {
            throw new DatabaseException('Não foi possível se conectar ao banco de dados: SL :: ' . $e->getMessage());
        }

        if ($this->linkSlave->connect_errno) {
            throw new DatabaseException('Não foi possível se conectar ao banco de dados: SL');
        }

        return $this->linkSlave;
    }

    /**
     * Obtem uma conexao com o banco de escrita.
     *
     * @throws DatabaseException
     *
     * @return \App\Core\MySqli Conexao.
     */
    private function getLinkMaster() {
        if (!empty($this->linkMaster)) {
            return $this->linkMaster;
        }

        try {
            $this->linkMaster = new MySqli($this->hostMaster, $this->userMaster, $this->passMaster, $this->baseMaster, $this->portMaster);
        } catch (PhpRuntimeException $e) {
            throw new DatabaseException('Não foi possível se conectar ao banco de dados: MS :: ' . $e->getMessage());
        }

        if ($this->linkMaster->connect_errno) {
            throw new DatabaseException('Não foi possível se conectar ao banco de dados: MS');
        }

        return $this->linkMaster;
    }

    //private function __destruct() {
    //    $this->disconnect();
    //}

    /**
     * Get the current instance of the database
     *
     * @return Database
     * @throws \App\Exceptions\DatabaseException
     */
    public static function getInstance() {
        if (null === static::$instance) {
            static::$hasConnection = true;
            static::$instance = new static();
        }
        return static::$instance;
    }

    /**
     * Realiza a conexão com o banco
     * @param  string $type Tipo de conexão. master ou slave
     * @return Database Retorna uma instância dele mesmo para method chainning
     * @throws DatabaseException
     */
    private function connect($type = self::TYPE_MASTER) {
        if ($type == static::TYPE_MASTER && !$this->getLinkMaster()->ping()) {
            if ($this->hasOpenTransaction) {
                $ex = new DatabaseException('Connection lost inside transaction');
            } else {
                $this->getLinkMaster()->connect();
            }
        }

        if ($type == static::TYPE_SLAVE && !$this->getLinkSlave()->ping()) {
            $this->getLinkSlave()->connect();
        }

        if (isset($ex)) {
            $this->rollback();
            throw $ex;
        }
        return $this;
    }

    /**
     * Método responsável por fechar a conexao
     * @param  string $type Indica se a conexão é master ou slave
     * @return \App\Core\Database       Instância de si mesmo para method chainning
     */
    public function disconnect($type = null) {
        if ((empty($type) || $type == static::TYPE_SLAVE) && $this->linkSlave instanceof Mysqli) {
            $this->getLinkSlave()->close();
        }

        if ((empty($type) || $type == static::TYPE_MASTER) && $this->linkMaster instanceof Mysqli) {
            $this->getLinkMaster()->close();
        }
        return $this;
    }

    /**
     * Método por tratar sql injection
     * @param  string $sql Consulta SQL a ser sanitizada
     * @return string      A consulta sql sanitizada
     */
    private function antiInjection($sql) {
        return str_ireplace("\t", " ", trim($sql));
    }

    /**
     * Método que pega uma query e faz o bid dos parãmetros nela
     * @param  string $sql A consulta a ser bindada
     * @param  array $dados Dados para bindar
     * @param  array $dataType Tipos de dados relacionados aos dados
     * @return string        SQL bindada
     */
    public function prepare($sql, $dados = array(), $dataType = array()) {
        return $sql . "/*" . sizeof($dados) . sizeof($dataType) . "*/";
    }

    /**
     * Responsável por buscar um ou mais, objetos no banco
     * after
     * @param  string $sql Consulta a ser executada
     * @param  bool $single Define se vai buscar 1 único objeto ou uma coleção
     * @param $forceSelectOnMaster
     * @return mixed Resultado da consulta
     * @throws DatabaseException
     */
    private function fetch($sql, $single = false, $forceSelectOnMaster = false) {
        if ($this->hasOpenTransaction || (is_bool($forceSelectOnMaster) && $forceSelectOnMaster)) {
            $this->connect(static::TYPE_MASTER);
            $link = $this->getLinkMaster();
        } else {
            $this->connect(static::TYPE_SLAVE);
            $link = $this->getLinkSlave();
        }

        $query = $link->query($this->antiInjection($sql));
        if ($query === false) {
            $mysqlError = $link->error;
            $mysqlErrNo = $link->errno;

            Logger::getInstance()->warning('Erro SQL: ' . $sql);
            throw new DatabaseException($mysqlError, $mysqlErrNo);
        }

        $retorno = null;
        if ($single) {
            $retorno = $query->fetch_object();
        } else {
            $retorno = array();
            while ($r = $query->fetch_object()) {
                $retorno[] = $r;
            }
        }
        return $retorno;
    }

    /**
     * Busca 1 objeto no banco
     * @param  string $sql SQl a ser executada
     * @param $forceSelectOnMaster
     * @return null|\stdClass Objeto buscado
     * @throws DatabaseException
     */
    public function get($sql, $forceSelectOnMaster) {
        return $this->fetch($sql, true, $forceSelectOnMaster);
    }

    /**
     * Recupera uma coleção de objetos do banco
     * @param  string $sql SQl a ser executada
     * @param $forceSelectOnMaster
     * @return array Objeto buscado
     * @throws DatabaseException
     */
    public function getAll($sql, $forceSelectOnMaster) {
        return $this->fetch($sql, false, $forceSelectOnMaster);
    }

    /**
     * Insere um objeto no banco
     * @param  string $sql Consulta a ser executada
     * @return int retorna a ID do objeto inserido
     * @throws DatabaseException
     */
    public function insert($sql) {
        $query = $this->getLinkMaster()->query($this->antiInjection($sql));

        if ($query === false) {
            $mysqlError = $this->getLinkMaster()->error;
            $mysqlErrNo = $this->getLinkMaster()->errno;
            throw new DatabaseException($mysqlError, $mysqlErrNo);
        }

        return $this->getLinkMaster()->insert_id;
    }

    /**
     * Executa uma SQL no banco
     * @param  string $sql Consulta a ser executada
     * @return bool se a query foi executada com sucesso
     * @throws DatabaseException
     */
    public function execute($sql) {
        $query = $this->getLinkMaster()->query($this->antiInjection($sql));

        if ($query === false) {
            $mysqlError = $this->getLinkMaster()->error;
            $mysqlErrNo = $this->getLinkMaster()->errno;
            Logger::getInstance()->error('Erro na SQL', array('query' => $sql));
            throw new DatabaseException($mysqlError, $mysqlErrNo);
        }
        return $this->getLinkMaster()->affected_rows;
    }

    public function isOnTransaction() {
        return $this->hasOpenTransaction;
    }

    /**
     * Inicia uma transação
     * @return \App\Core\Database Retorna uma instância dele mesmo para method chainning
     */
    public function beginTransaction() {
        if ($this->hasOpenTransaction) {
            $this->qtdStartTransaction++;
        } else {
            $this->connect(static::TYPE_MASTER);

            $this->getLinkMaster()->autocommit(false);

            $this->hasOpenTransaction = true;
            $this->qtdStartTransaction = 1;
        }
        return $this;
    }

    /**
     * Commita uma transação
     * @return \App\Core\Database Retorna uma instância dele mesmo para method chainning
     */
    public function commit() {
        if ($this->hasOpenTransaction) {
            $this->qtdStartTransaction--;

            if ($this->qtdStartTransaction == 0) {
                $response = $this->getLinkMaster()->commit();
                $this->lastCommitStatus = $response;
                $this->isRealCommit = true;
                $this->hasOpenTransaction = false;
                $this->getLinkMaster()->autocommit(true);
            } else {
                $this->isRealCommit = false;
            }
        }
        return $this;
    }

    /**
     * Rollbacka uma transação
     * @return \App\Core\Database Retorna uma instância dele mesmo para method chainning
     */
    public function rollback() {
        if ($this->hasOpenTransaction) {

            $this->getLinkMaster()->rollback();
            $this->getLinkMaster()->autocommit(true);

            $this->qtdStartTransaction = 0;
            $this->hasOpenTransaction = false;
        }
        return $this;
    }

    /**
     * Define o diretório raiz onde serão executadas as consultas
     * @param string $path Caminho raiz dos consultas
     * @return Database Retorna uma instância dele mesmo para method chainning
     * @throws DatabaseException
     */
    public function setSqlPath($path) {
        if (!is_dir($path)) {
            throw new DatabaseException("Caminho informado nao e um diretorio.");
        }
        $path .= substr($path, -1, 1) != DS ? DS : "";
        $this->sqlPath = $path;
        return $this;
    }

    /**
     * Recupera uma consulta sql após validação se o mesmo existe
     * @param  string $file Caminho/nome da consulta
     * @param array $params
     * @return string Consulta SQL
     * @throws DatabaseException
     */
    public function getSqlFile($file, $params = array()) {
        if (empty($file)) {
            throw new DatabaseException("O arquivo SQL e obrigatorio.");
        }

        $filePath = $this->sqlPath . $file . ".php";
        if (!file_exists($filePath)) {
            throw new DatabaseException("Arquivo SQL nao encontrado - $filePath", 5);
        }

        $sql = require $filePath;

        if (empty($sql)) {
            throw new DatabaseException("Arquivo SQL retorna uma query vazia.", 6);
        }

        return $sql;
    }

    /**
     * Recupera uma consulta montada dado uma lista de parametros e o nome da query
     * @param  string $sqlFile Arquivo da query
     * @param  array $params Lista de parâmetros a ser bindado
     * @param array $bindTipos
     * @return string Consulta SQL completa
     * @throws DatabaseException
     */
    public function getSql($sqlFile, array $params = array(), $bindTipos = array()) {
        $sql = $this->getSqlFile($sqlFile, $params);

        Logger::getInstance()->logSql($sqlFile, $sql, $params);

        foreach ($params as $param => $value) {

            // Verifica se tem bind forçado
            if (array_key_exists($param, $bindTipos)) {
                $tipoBind = $bindTipos[$param];

                switch ($tipoBind) {
                    case Database::FLOAT:
                        $value = floatval($value);
                        $sql = preg_replace("/:\\b" . $param . "\\b/i", $value, $sql);
                        break;
                    default:
                    case Database::STRING:
                        if (!is_array($value)) {
                            $value = utf8_decode($value);
                            $sql = preg_replace("/:\\b" . $param . "\\b/i", sprintf('"%s"', $this->getLinkMaster()->real_escape_string(addslashes(addslashes($value)))), $sql);
                        } else {
                            foreach ($value as &$val) {
                                $val = "'" . $this->escapeString(utf8_decode($val)) . "'";
                            }
                            $sqlIn = sprintf("(%s)", implode(", ", $value));
                            $sql = preg_replace("/:\\b" . $param . "\\b/i", $sqlIn, $sql);
                        }
                        break;
                }

            } else {

                if (is_array($value)) {
                    foreach ($value as &$val) {
                        if (!is_numeric($val)) {
                            $val = "'" . $this->escapeString(utf8_decode($val)) . "'";
                        }
                    }
                    $sqlIn = sprintf("(%s)", implode(", ", $value));
                    $sql = preg_replace("/:\\b" . $param . "\\b/i", $sqlIn, $sql);
                } elseif (is_null($value)) {
                    $sql = preg_replace("/:\\b" . $param . "\\b/i", "NULL", $sql);
                } elseif (is_numeric($value)) {
                    $sql = preg_replace("/:\\b" . $param . "\\b/i", $value, $sql);
                } elseif (is_string($value)) {
                    $value = $this->escapeString(utf8_decode($value));
                    $sql = preg_replace("/:\\b" . $param . "\\b/i", sprintf('"%s"', $value), $sql);
                }
            }
        }

        return sprintf("/* %s */\n%s", $sqlFile, $sql);
    }

    /**
     * @return null|boolean Return if the last commit have been executed correctly or null if there's no previous commit
     */
    public function getLastCommitStatus() {
        return $this->lastCommitStatus;
    }

    /**
     * Verifies that the last commit was executed on the database or was a virtual commit (checkpoint commit)
     */
    public function isRealCommit() {
        return $this->isRealCommit;
    }

    private function escapeString($str) {
        $str = addcslashes($str, '"\\/');
        return addcslashes($str, '\\');
    }
}
