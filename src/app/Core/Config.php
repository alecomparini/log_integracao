<?php

namespace App\Core;

use App\Exceptions\FrameworkException;

/**
 * Class Config
 * @package App\Core
 */
class Config {
    protected static $config;
    protected static $env = '';

    /**
     *
     */
    public static function init() {
        static::$config = require APP_DIR . 'config.php';
        static::$env = static::$config['environment'];
    }

    /**
     * @param $env
     */
    public static function setEnv($env) {
        static::$env = $env;
    }

	/**
     * @param string $path
     * @param null $default the default value to return if not found
     * @return mixed
     */
    public static function get($path = '', $default = null) {
        $result = static::find(sprintf('%s.%s', static::$env, $path), $default);
        if ($default === $result) {
            $result = static::find(sprintf('%s.%s', 'default', $path), $default);
            if ($default === $result) {
                $result = static::find(sprintf('%s', $path), $default);
            }
        }
        return $result;
    }

    /**
     * @param string $path
     * @param null $default
     * @return null
     */
    public static function find($path = '', $default = null) {
        $root = static::$config;
        $paths = explode('.', $path);

        if (!empty($path) && sizeof($paths) > 0) {
            foreach ($paths as $p) {
                if (array_key_exists($p, $root)) {
                    $root = $root[$p];
                } else {
                    return $default;
                }
            }
        }

        return $root;
    }
}

Config::init();