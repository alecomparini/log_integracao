<?php

namespace App\Core\Controller;

use App\Model\Response;

/**
 * Class RestController
 * @package App\Core\Controller
 */
class RestController extends Controller {

    /** @var int */
    private $httpCode = 200;

    private $responseMessage = "";
    private $responseCode = 0;
    private $responseData = "";
    private $responseSuccess = false;

    /**
     * Médodo set do código http
     * @param $httpCode
     * @return $this
     */
    public function setHttpCode($httpCode) {
        if (!is_numeric($httpCode)) {
            throw new \InvalidArgumentException('Http code deve ser numérico');
        }
        if ($httpCode < 0) {
            throw new \InvalidArgumentException('Http code deve ser positivo');
        }
        $this->httpCode = $httpCode;

        return $this;
    }

    /**
     * @param string $responseMessage
     * @return $this
     */
    public function setResponseMessage($responseMessage) {
        $this->responseMessage = $responseMessage;
        return $this;
    }


    /**
     * @param int $responseCode
     * @return $this
     */
    public function setResponseCode($responseCode) {
        if (!is_numeric($responseCode)) {
            throw new \InvalidArgumentException("Código de resposta deve ser numérico");
        }
        $this->responseCode = $responseCode;
        return $this;
    }

    /**
     * @param mixed $responseData
     * @return $this
     */
    public function setResponseData($responseData) {
        $this->responseData = $responseData;
        return $this;
    }

    /**
     * @param boolean $responseSuccess
     * @return $this
     */
    public function setResponseSuccess($responseSuccess) {
        if (!is_bool($responseSuccess)) {
            throw new \InvalidArgumentException("Sucesso da resposta deve ser booleano");
        }
        $this->responseSuccess = $responseSuccess;
        return $this;
    }

    /**
     * Retorna o response em formato json
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getResponse() {
        $response = new Response($this->responseMessage, $this->responseCode, $this->responseData, $this->responseSuccess);
        return $this->json($response, $this->httpCode);
    }

    protected function startsWith($str) {
        $requestUrl = $this->request->getRequestUri();
        $strSize = strlen($str);
        return substr($requestUrl, 0, $strSize) == $str;
    }
}
