<?php

namespace App\Core\Controller;

use SoapFault;
use Zend\Soap\Server as SoapServer;
use Zend\Soap\AutoDiscover;
use Zend\Soap\Wsdl\ComplexTypeStrategy\ArrayOfTypeSequence;

/**
 * Class SoapController
 * @package App\Core\Controller
 */
class SoapController extends Controller {

    /**
     * Realiza o processamento de uma chamada SOAP de acordo com o wsdl
     * @param bool $wsdl
     * @return \Symfony\Component\HttpFoundation\Response
     */
    protected function serve($wsdl = false) {
        $soapClass = "\\App\\Soap\\" . $this->soapClass;
        $result = null;

        //Se a classe para este servico não existe, retorna erro
        if (!class_exists($soapClass)) {
            return "<?xml version='1.0' ?><root><success>false</success><data></data></root>";
        }

        //Define o arquivo absoluto do WSDL
        $class = get_class(new $soapClass());
        $wsdlFile = ROOT_DIR . "wsdl_files" . DS . str_replace('\\', '_', $class) . ".wsdl";

        if ($wsdl) {
            if (!isset($_GET["force"]) && file_exists($wsdlFile)) {
                $response = file_get_contents($wsdlFile);
            } else {
                $autoDiscover = new AutoDiscover(new ArrayOfTypeSequence());
                $autoDiscover->setClass($class)
                    ->setUri($this->getRequest()->getUri());
                $wsdl = $autoDiscover->generate();
                $wsdl->dump($wsdlFile);
                $response = $wsdl->toXML();
            }
            $result = $response;
        } else {
            $soapServer = new SoapServer(null, array(
                "soap_version" => SOAP_1_2,
                "wsdl" => $wsdlFile
            ));
            $soapServer->setClass($class);
            $soapServer->setReturnResponse(true);
            $soapResponse = $soapServer->handle();
            if ($soapResponse instanceof SoapFault) {
                /** @var $soapResponse \SoapFault */
                $soapServer->getSoap()->fault($soapResponse->faultcode, $soapResponse->getMessage());
                return null;
            }
            $result = $soapResponse;
        }

        return $result;
    }
}
