<?php
namespace App\Core\Controller;

use App\Core\View;
use App\DAO\ParceiroSiteDAO;
use App\DAO\ParceiroSitePermissaoDAO;
use App\Exceptions\FrameworkException;
use App\Helper\Utils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class Controller
 *
 * @package App\Core\Controller
 */
abstract class Controller {
    /** @var  \Symfony\Component\HttpFoundation\Request */
    protected $request;
    protected $body;
    protected $rawBody;
    protected $permittedContentTypes = array();

    /**
     * Define o tipo de dado de um request de acordo com o seu Content-type.
     * Controller constructor.
     *
     * @param Request $request
     *
     * @throws FrameworkException
     */
    public function __construct(Request $request) {

        // Define os tipos que aceitamos payloads
        $this->permittedContentTypes = array(
            "application/x-www-form-urlencoded",
            "multipart/form-data",
            "application/json",
            "application/xml",
            "text/xml"
        );

        //Obtém informação sobre o content-type requisitado
        $this->setRequest($request);
        $headers = explode(";", $this->request->headers->get("CONTENT_TYPE"));
        $contentType = $headers[0];

        //Verifica se é um content-type permitido e define o tipo de dado
        $raw = file_get_contents("php://input");
        $this->rawBody = $raw;
        if (in_array($contentType, $this->permittedContentTypes, false)) {
            switch ($contentType) {
                case "application/x-www-form-urlencoded":
                case "multipart/form-data":
                    $this->body = $_POST;
                    break;
                case "application/json":
                    $json = json_decode($raw);
                    if (json_last_error() !== JSON_ERROR_NONE) {
                        throw new FrameworkException('', 24);
                    }
                    $this->body = null !== $json ? $json : array();

                    $decodeAsArray = json_decode($raw, true);
                    $this->request->request->replace(is_array($decodeAsArray) ? $decodeAsArray : array());
                    break;
                case "application/xml":
                case "text/xml":
                    $this->body = array("nao" => "implementado");
                    break;
            }
        }
    }

    /**
     * Get the raw body request
     *
     * @return mixed
     */
    public function getRawBody() {
        return $this->rawBody;
    }

    /**
     * Método set para $request
     *
     * @param Request $request
     */
    public function setRequest(Request $request) {
        $this->request = $request;
    }

    /**
     * Método get para $request
     *
     * @return Request
     */
    public function getRequest() {
        return $this->request;
    }

    /**
     * Obtém um parâmetro da query
     *
     * @param $name
     *
     * @return mixed
     * @throws \InvalidArgumentException
     */
    protected function get($name) {
        return $this->request->query->get($name);
    }

    /**
     * Obtém um parâmetro do payload
     *
     * @param $name
     *
     * @return null
     */
    protected function getParam($name) {
        if (isset($this->body->{$name})) {
            return $this->body->{$name};
        }

        return null;
    }

    /**
     * Retorna de forma padrozinada uma saída para o Browser
     *
     * @param  \stdClass|array $dados      Dados para serem convertidos
     * @param  int             $httpStatus Código http a ser retornado
     * @param  array           $headers    Headers adicionais (ou sobrescritos)
     *
     * @return \Symfony\Component\HttpFoundation\Response        Response do symfony
     * @throws \InvalidArgumentException
     */
    public function json($dados = array(), $httpStatus = 200, array $headers = array()) {
        $headers = array_merge(array('content-type' => 'application/json; charset=utf-8'), $headers);
        $json = Utils::jsonEncode($dados);

        return new Response($json, $httpStatus, $headers);
    }

    /**
     * Retorna XML
     *
     * @param       $dados
     * @param int   $httpStatus
     * @param array $headers
     *
     * @return Response
     * @throws \InvalidArgumentException
     */
    public function xml($dados, $httpStatus = 200, array $headers = array()) {
        $headers = array_merge(array('content-type' => 'text/xml; charset=UTF-8'), $headers);

        return new Response($dados, $httpStatus, $headers);
    }

    /**
     * Retorna um text/plain com UTF-8
     *
     * @param       $string
     * @param int   $httpStatus
     * @param array $headers
     *
     * @return Response
     * @throws \InvalidArgumentException
     */
    public function text($string, $httpStatus = 200, array $headers = array()) {
        $headers = array_merge(array('content-type' => 'text/plain; charset=utf-8'), $headers);

        return new Response($string, $httpStatus, $headers);
    }

    /**
     * Retorna uma view
     *
     * @param       $template
     * @param array $dados
     *
     * @return View
     */
    public function view($template, $dados = array()) {
        $dados = array_merge($dados, array(
            'httpHost' => $this->request->getSchemeAndHttpHost()
        ));
        $dados = Utils::utf8Convert($dados);
        $template = str_replace('.', '/', $template) . '.html';

        $view = new View();
        $view->setTemplate($template);
        $view->setDados($dados);

        return $view;
    }

    /**
     * Checa a permissão de um parceiro para um determinado site
     *
     * @param $parceiroId
     *
     * @return bool
     */
    protected function checaPermissao($parceiroId) {
        $parceiroSiteDAO = new ParceiroSiteDAO();
        $parceiroSitePermissaoDAO = new ParceiroSitePermissaoDAO();
        $parceiroSite = $parceiroSiteDAO->getParceiroSiteById($parceiroId);
        $permissoes = $parceiroSitePermissaoDAO
            ->getPermissoes($parceiroSite->parceiroId,
                $parceiroSite->siteId,
                $this->request->getClientIp());

        return count($permissoes) > 0;
    }

    /**
     * Verifica se uma requisição é json ou não
     *
     * @return bool
     */
    protected function isJson() {
        return -1 !== stripos($this->request->headers->get('content-type'), 'application/json');
    }

    protected function forceHttps() {
        if (!$this->request->isSecure()) {
            $url = substr($this->request->getUri(), 4);
            header('Location: ' . 'https' . $url);
            exit;
        }
    }
}
