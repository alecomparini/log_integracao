<?php

namespace App\Core;

use App\Helper\Utils;
use Exception;
use Monolog\Formatter\LineFormatter;
use Monolog\Handler\StreamHandler;

/**
 * Class Logger
 *
 * @package App\Core
 */
class Logger {
    private static $instance = array();
    /**
     * @var \Monolog\Logger
     */
    private $frameworkLog;
    /**
     * @var \Monolog\Logger
     */
    private $errorLog;
    private $logAtivo = false;
    private $errorLogAtivo = false;
    private $instanceName = '';

    protected static $requestId = '';
    protected static $context = array();

    protected function __construct($instanceName = 'default') {
        $this->instanceName = $instanceName;
        $this->configure($instanceName);
    }

    private function configure($instanceName) {
        if ($instanceName === 'default') {
            $this->logAtivo = Config::get('logging.enabled');
            $this->errorLogAtivo = Config::get('logging.errors');

            $lineFormatter = new LineFormatter(null, null, true);

            if ($this->errorLogAtivo) {
                $this->errorLog = new \Monolog\Logger('error');

                $agora = new \DateTime('now');
                $ano = $agora->format("Y");
                $mes = $agora->format("m");
                $dia = $agora->format("d");
                $hora = $agora->format("H");

                $errorFile = sprintf("%s%s/%s/%s/error.%s.log", LOG_DIR, $ano, $mes, $dia, $hora);
                $frameworkFilePath = sprintf("%s%s/%s/%s/", LOG_DIR, $ano, $mes, $dia);

                //Create/check year folder
                if (!is_dir($frameworkFilePath)) {
                    //Tenta criar pasta
                    try {
                        mkdir($frameworkFilePath, 0777, true);
                    } catch (\Exception $e) {
                        $errorFile = sprintf("%s/error.%s%s%s%s.log", sys_get_temp_dir(), $ano, $mes, $dia, $hora);
                    }
                }

                $errorStream = new StreamHandler($errorFile);
                $errorStream->setFormatter($lineFormatter);
                $this->errorLog->pushHandler($errorStream);
            }

            if ($this->logAtivo) {
                $this->frameworkLog = new \Monolog\Logger('framework');

                $agora = new \DateTime('now');
                $ano = $agora->format("Y");
                $mes = $agora->format("m");
                $dia = $agora->format("d");
                $hora = $agora->format("H");

                $frameworkFile = sprintf("%s%s/%s/%s/framework.%s.log", LOG_DIR, $ano, $mes, $dia, $hora);
                $frameworkFilePath = sprintf("%s%s/%s/%s/", LOG_DIR, $ano, $mes, $dia);

                //Create/check year folder
                if (!is_dir($frameworkFilePath)) {
                    //Tenta criar pasta
                    try {
                        mkdir($frameworkFilePath, 0777, true);
                    } catch (\Exception $e) {
                        $frameworkFile = sprintf("%s/framework.%s%s%s%s.log", sys_get_temp_dir(), $ano, $mes, $dia, $hora);
                    }
                }

                $frameworkStream = new StreamHandler($frameworkFile);
                $frameworkStream->setFormatter($lineFormatter);
                $this->frameworkLog->pushHandler($frameworkStream);
            }
        }
        if (0 === strpos($instanceName, 'api')) {
            $this->logAtivo = true;
            $this->errorLogAtivo = true;
            $lineFormatter = new LineFormatter(null, null, true);
            $this->frameworkLog = new \Monolog\Logger('api');
            $this->errorLog = $this->frameworkLog;

            $agora = new \DateTime('now');
            $ano = $agora->format("Y");
            $mes = $agora->format("m");
            $dia = $agora->format("d");
            $hora = $agora->format("H");

            $frameworkFile = sprintf("%s%s/%s/{$this->instanceName}.%s.log", LOG_DIR, $ano, $mes, $dia);
            $frameworkFilePath = sprintf("%s%s/%s/", LOG_DIR, $ano, $mes);

            //Create/check year folder
            if (!is_dir($frameworkFilePath)) {
                //Tenta criar pasta
                try {
                    mkdir($frameworkFilePath, 0777, true);
                } catch (\Exception $e) {
                    $frameworkFile = sprintf("%s/{$this->instanceName}.%s%s%s.log", sys_get_temp_dir(), $ano, $mes, $dia);
                }
            }

            $frameworkStream = new StreamHandler($frameworkFile);
            $frameworkStream->setFormatter($lineFormatter);
            $this->frameworkLog->pushHandler($frameworkStream);
        }
    }

    public function logSql($file, $sql, $params) {
        if ($this->logAtivo && Config::get('logging.queries', false)) {
            $log = "File: " . $file . PHP_EOL . PHP_EOL;
            $log .= "SQL: " . $sql . PHP_EOL . PHP_EOL;
            $log .= "Params: " . print_r($params, true) . PHP_EOL;
            try {
                $this->frameworkLog->addInfo($log);
            } catch (Exception $e) {
                return $e->getCode();
            }
        }
    }

    public function warning($msg) {
        if ($this->logAtivo) {
            try {
                $this->frameworkLog->addWarning($msg);
            } catch (Exception $e) {
                return $e->getCode();
            }
        }
    }

    public function error($msg, $contextData = array()) {
        if ($this->errorLogAtivo) {
            try {
                if (!empty(static::$requestId)) {
                    $contextData = array_merge($contextData, array('requestId' => static::$requestId));
                }
                $this->errorLog->addError($msg, $contextData);
            } catch (Exception $e) {
                return $e->getCode();
            }
        }
    }

    public function exception(Exception $e, $contextData = array()) {
        $nomeExcecao = get_class($e);
        $mensagem = $e->getMessage();

        $arquivo = Utils::recuperaUltimoPedacoTextoSeparador(DS, $e->getFile());
        $linha = $e->getLine();

        // Recupera os últimos 3 calls
        $trace = $e->getTrace();

        $callsSequence = array();
        // Call sequence
        foreach ($trace as $call) {
            if (isset($call['file']) && isset($call['line'])) {
                $callsSequence[] = Utils::recuperaUltimoPedacoTextoSeparador(DS, $call['file']) . '(' . $call['line'] . ')';
            }
        }
        array_unshift($callsSequence, $arquivo . '(' . $linha . ')');

        // Monta string call
        $errorString = $nomeExcecao . ' ' . implode(" => ", $callsSequence) . ' :: ' . $mensagem;

        $this->error($errorString, $contextData);
    }

    public function log($msg, $context = array()) {
        if ($this->logAtivo) {
            try {
                $this->frameworkLog->addInfo($msg, $context);
            } catch (Exception $e) {
                return $e->getCode();
            }
        }
    }

    public function alert($msg) {
        if ($this->logAtivo) {
            try {
                $this->frameworkLog->addAlert($msg);
            } catch (Exception $e) {
                return $e->getCode();
            }
        }
    }

    /**
     * @return self
     */
    public static function getInstance($instanceName = 'default') {
        if (is_null(static::$instance[$instanceName])) {
            static::$instance[$instanceName] = new static($instanceName);
        }

        return static::$instance[$instanceName];
    }

    /**
     * Define o identificar do log
     * @param string $requestId
     */
    public static function setRequestId($requestId) {
        static::$requestId = $requestId;
    }

    private function __clone() {

    }

    private function __wakeup() {

    }

    /* Metodo que retorna os dados Contexto de forma estruturada para ser gravado no Log.
     *
     * */
    public static function getContextData($classMetodo , $request, $response ) {
        $class =  substr($classMetodo,strrpos($classMetodo,'\\')+1,strpos($classMetodo,':')-strrpos($classMetodo,'\\')-1);
        $metodo = substr($classMetodo,strpos($classMetodo,'::')+2);
        $contextData['Objeto'] = array ('Classe' =>$class , 'Metodo' => $metodo);
        $contextData['Request'] =  $request;
        $contextData['Response'] = array("Retorno" => str_replace(array("\t", "\n" ), '',$response));

        return static::$context = $contextData;
    }

}