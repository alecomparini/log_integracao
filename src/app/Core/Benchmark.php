<?php
namespace App\Core;

/**
 * Classe utilizada para gerar benchmarks
 * @package App\Core
 */
abstract class Benchmark {

    /**
     * Array que armazena os tempos dos benchmarks em execução
     * @var array
     */
    private static $times = array();

    /**
     * Flag que armazena se o benchmark será executado ou não
     * @var bool
     */
    private static $benchmarkEnabled = false;

    /**
     * Como se fosse um método construtor, use as coisas de inicialização aqui
     */
    public static function init() {
        static $started = false;
        if (!$started) {
            $started = !$started;

            static::$times = array();
            static::$benchmarkEnabled = Config::get('benchmark.enabled');
            if (is_null(static::$benchmarkEnabled)) {
                static::$benchmarkEnabled = false;
            }
        }
    }

    /**
     * Método que inicia um ou mais benchmarks com seus respectivos labels
     * @param string[] $labels
     */
    public static function start($labels) {
        if (!is_array($labels)) {
            $labels = array($labels);
        }
        $now = microtime(true);
        foreach ($labels as $label) {
            static::$times[$label] = $now;
        }
    }

    /**
     * Método que encerra a execução de um benchmark dado um específico label
     * @param $label
     * @param bool $return
     */
    public static function end($label, $return = false) {
        if (!isset(static::$times[$label])) {
            return;
        }

        $totalTime = microtime(true) - static::$times[$label];
        unset(static::$times[$label]);
        if (!$return) {
            echo vsprintf('%s: %s seconds', $label, $totalTime);
        } else {
            return $totalTime;
        }
    }

    /**
     * Retorna todos os labels que estão ativos no momento.
     * @return string[]
     */
    public static function getLabels() {
        return array_keys(static::$times);
    }

    /**
     * Método responasável por limpar todos os labels da classe de benchmark
     */
    public static function purgeAll() {
        static::$times = array();
    }

    /**
     * Método responsável por avisar se o benchmark será executado ou não
     * @return bool
     */
    public static function isEnabled() {
        return static::$benchmarkEnabled;
    }
}

Benchmark::init();