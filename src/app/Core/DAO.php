<?php
namespace App\Core;

use App\Core\Exception\PhpRuntimeException;
use InvalidArgumentException;

/**
 * Classe abstrata de acesso de dados
 */
abstract class DAO {
    /**
     * Instancia de \App\Core\Database
     * @var instance
     */
    protected $db;
    private $mapping;
    private $debug;
    private $cache;
    private $cacheTTL;
    private $forceSelectOnMaster = false;

    public function __construct() {

        $this->db = Database::getInstance();
        $this->db->setSqlPath(APP_DIR . "Service" . DS ."Query" . DS);

        $this->setMap(true);

        $cacheQuery = Config::get('caching.cache_query', false);
        $cacheTTL = Config::get('caching.cache_lifetime', 600);

        if (!is_bool($cacheQuery)) {
            throw new InvalidArgumentException("Variavel de ambiente 'caching.cache_query' deve ser definida como booleana");
        }

        $this->setCache($cacheQuery);
        $this->setCacheTTL($cacheTTL);
        $this->setDebug(false);
    }

    /**
     * @return boolean
     */
    public function isForceSelectOnMaster() {
        return $this->forceSelectOnMaster;
    }

    /**
     * @param boolean $forceSelectOnMaster
     *
     * @return $this
     * @throws \InvalidArgumentException
     * @throws PhpRuntimeException
     */
    public function forceSelectOnMaster($forceSelectOnMaster) {
        if (!is_bool($forceSelectOnMaster)) {
            throw new InvalidArgumentException('Parâmetro deve ser booleano');
        }
        $this->forceSelectOnMaster = $forceSelectOnMaster;
        return $this;
    }

    /**
     * Define se vai mapear em um modelo (true) ou se vai retornar stdClass (false)
     * @method  setMap
     * @param bool $mapping
     * @return bool $mapping   Status do Mapping
     */
    public function setMap($mapping = true) {
        if (!is_bool($mapping)) {
            throw new InvalidArgumentException("Variavel map deve ser booleana");
        }
        $this->mapping = $mapping;
        return $this->mapping;
    }

    /**
     * Define se vai cachear ou não
     * @method setCache
     * @param bool $cache cacheia ou não
     */
    public function setCache($cache = true) {
        if (!is_bool($cache)) {
            throw new InvalidArgumentException("Variável cache deve ser booleana");
        }
        $this->cache = $cache;
    }

    /**
     * Define tempo de vida de cache
     * @method setCacheTTL
     * @param int $cacheTTL tempo de vida em segundos
     */
    public function setCacheTTL($cacheTTL = 600) {
        if (!is_int($cacheTTL)) {
            throw new InvalidArgumentException("Variável cacheTTL deve ser booleana");
        }
        $this->cacheTTL = $cacheTTL;
    }

    /**
     * Define se ativa debug ou não
     * @method  setMap
     * @param   bool        $debugging ativa debug
     * @return  instance
     */
    public function setDebug($debugging = true) {
        if (!is_bool($debugging)) {
            throw new InvalidArgumentException("Variavel debugging deve ser booleana");
        }
        $this->debug = $debugging;
        return $this;
    }

    /**
     * Acessa o banco atraves de um SQL versionado e retorna ou nao um map de um model
     * Retorna uma única coleção
     * @method get
     * @param  string $sql Caminho da Service
     * @param  array $params Parametros para bindar na query
     * @param array $bindTipos
     * @return instance|stdClass
     */
    public function get($sql, array $params = array(), $bindTipos = array()) {
        $innerSql = $this->db->getSql($sql, $params, $bindTipos);

        $key = md5($innerSql);

        $retorno = Cache::get($key);
        if ($retorno === false) {
            $retorno = $this->getQuery($innerSql);
            if ($this->cache) {
                Cache::set($key, $retorno);
            }
        }
        return $retorno;
    }

    public function execute($sql, array $params = array(), $bindTipos = array()) {
        $innerSql = $this->db->getSql($sql, $params, $bindTipos);
        if ($this->debug) {
            print_r($innerSql);
        }
        return $this->db->execute($innerSql);
    }

    public function insert($sql, array $params = array(), $bindTipos = array()) {
        $innerSql = $this->db->getSql($sql, $params, $bindTipos);
        if ($this->debug) {
            print_r($innerSql);
        }

        return $this->db->insert($innerSql);
    }

    public function beginTransaction() {
        return $this->db->beginTransaction();
    }

    public function rollback() {
        return $this->db->rollback();
    }

    public function commit() {
        return $this->db->commit();
    }

    /**
     * Verifies if the last commit has really been executed on the database
     * @return bool
     */
    public function isCommited() {
        return $this->db->isRealCommit() && !!$this->db->getLastCommitStatus();
    }

    /**
     * Acessa o banco atraves de um SQL e retorna ou nao um map de um model
     * Retorna uma única coleção
     * @method raw
     * @param  string   $sql Query de um SQL
     * @return stdClass|instance
     */
    public function raw($sql) {
        Logger::getInstance()->logSql('raw', $sql, array());
        return $this->getQuery($sql);
    }

    public function rawExec($sql) {
        Logger::getInstance()->logSql('rawExec', $sql, array());
        return $this->db->execute($sql);
    }

    /**
     * [getQuery description]
     * @method getQuery
     * @param  [type]   $sql [description]
     * @return instance|null|\stdClass [type]        [description]
     */
    private function getQuery($sql) {
        if ($this->debug) {
            print_r($sql);
        }

        $result = $this->db->get($sql, $this->forceSelectOnMaster);

        if (!is_null($result) && $this->mapping && property_exists($this, "model")) {
            $modelFullName = "\\App\\Model\\" . $this->model;
            $model = new $modelFullName;
            $columnMap = array();
            if (method_exists($model, "columnMap")) {
                $columnMap = $model->columnMap();
            }
            $result = $this->mapColumns($result, $model, $columnMap);
        }

        return $result;
    }

    /**
     * Acessa o banco atraves de um SQL e retorna ou nao um map de um model
     * Retorna um vetor de coleções
     * @method getAll
     * @param  string $sql Caminho da Service/Query de um SQL
     * @param  array $params Parametros para bindar na query
     * @param array $bindTipos
     * @return array
     */
    public function getAll($sql, $params = array(), $bindTipos = array()) {
        $innerSql = $this->db->getSql($sql, $params, $bindTipos);

        if ($this->debug) {
            print_r($innerSql);
        }

        $key = md5($innerSql);
        $results = Cache::get($key);
        if ($results === false) {

            $results = $this->db->getAll($innerSql, $this->forceSelectOnMaster);

            if ($this->mapping && property_exists($this, "model")) {
                $modelFullName = "\\App\\Model\\" . $this->model;
                $retorno = array();
                foreach ($results as $result) {
                    $model = new $modelFullName;
                    $columnMap = array();
                    if (method_exists($model, "columnMap")) {
                        $columnMap = $model->columnMap();
                    }
                    $retorno[] = $this->mapColumns($result, $model, $columnMap);
                }
                $results = $retorno;
            }

            if ($this->cache) {
                Cache::set($key, $results);
            }
        }

        return $results;
    }

    /**
     * Retorna a SQL renderizada
     *
     * @author Cristiano Gomes
     *
     * @param string $sql
     * @param array $params
     * @param array $bindTipos
     * @return string
     */
    protected function getSql($sql, $params = array(), $bindTipos = array()) {
        return $this->db->getSql($sql, $params, $bindTipos);
    }

    /**
     * Mapeia as colunas retornadas do BD a um model
     * @method mapCollumns
     * @param  \stdClass     $result Retorno do BD
     * @param  instance      $model  Instancia do Model a ser mapeado
     * @param  array         $map    De/Para Colunas do banco => Objeto Model
     * @return instance              Instancia do Model
     */
    private function mapColumns($result, $model, array $map) {
        $properties = get_object_vars($result);
        foreach ($properties as $property => $value) {

            if (array_key_exists($property, $map)) {
                $model->{$map[$property]} = $value;
            } else {
                $model->{$property} = $value;
            }
        }
        return $model;
    }
}
