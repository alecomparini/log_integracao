<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 12/01/2016
 * Time: 09:29
 */

namespace App\Core\Exception;

/**
 * Class PhpRuntimeException
 * @package App\Core\Exception
 */
class PhpRuntimeException extends \Exception {

    /**
     * Realiza log das exceções em tempo de execução
     * @param $code
     * @param $string
     * @param $file
     * @param $line
     * @param $context
     * @throws static
     */
    public static function handle($code, $string, $file, $line, $context) {
        if ($code != E_NOTICE) {
            $internalFile = explode(DIRECTORY_SEPARATOR, $file);
            $filename = end($internalFile);
            $phpRuntimeException = new static(sprintf('[%s:%s] %s', $filename, $line, $string), $code);
            $phpRuntimeException->file = $file;
            $phpRuntimeException->line = $line;
            throw $phpRuntimeException;
        }
    }

    /**
     * Registra execeções em tempo de execução
     */
    public static function registerPhpRuntimeExceptions() {
        set_error_handler('\App\Core\Exception\PhpRuntimeException::handle');
    }
}