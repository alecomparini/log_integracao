<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 12/01/2016
 * Time: 09:27
 */

namespace App\Core\Exception;

/**
 * Class FrameworkException
 * @package App\Core\Exception
 */
class FrameworkException extends \Exception {

}