<?php

namespace App\Core;

use App\BO\DBLockBO;
use App\Controller\ErrorController;
use App\Core\Exception\PhpRuntimeException;
use App\Exceptions\AuthenticationException;
use App\Exceptions\AuthorizationException;
use App\Exceptions\BusinessException;
use App\Exceptions\DatabaseException;
use App\Exceptions\FrameworkException;
use App\Exceptions\PageNotFoundException;
use App\Model\Response as JsonResponse;
use Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class principal da aplicação
 * Instancia um router, faz o match da rota
 * Inicia o contrller, injeta o HttpRequest
 * Trata o retorno e exibe na tela a saída
 */
class Framework {

    protected static $requestId = null;

    /**
     * Método responsável por remover as / adicionadas no final da URL
     * exemplo: http://www.integracao.dev/ws/Consulta/?wsdl vira http://www.integracao.dev/ws/Consulta?wsdl
     * Isso foi necessário para não ter que criar rota com e sem barra no final, duplicando desnecessáriamente
     * o tamanho do arquivo e dificultando a leitura por causa de rotas duplicadas.
     */
    public static function stripTraillingSlash() {
        $urlParts = explode("?", $_SERVER["REQUEST_URI"]);
        if ($urlParts[0] != "/") {
            $urlParts[0] = substr($urlParts[0], -1, 1) == "/" ? substr($urlParts[0], 0, -1) : $urlParts[0];
            $_SERVER["REQUEST_URI"] = implode("?", $urlParts);
        }
    }

    /**
     * Método principal do framework, desde a inicialização
     * a exibicação dos dados
     * Pode ser considerado uma espécie de bootstra da aplicação
     * @throws FrameworkException
     */
    public function run() {
        try {
            //Define o request ID do Log
            Logger::setRequestId(static::getRequestId());

            //Registra o handler de erros para tenter converter para exceptions
            PhpRuntimeException::registerPhpRuntimeExceptions();

            //Remove a / do final da URL pra parar de dar biziu com as ROTAS
            static::stripTraillingSlash();

            //Cria a variavel de request do symfony
            $request = Request::createFromGlobals();

            $routerParams = Router::getParams();

            // Trata os middlewares da rota
            $this->handleMiddlewares($routerParams, $request);


            //Identifica controlador e método e instancia
            $controller = $routerParams["_controller"];
            $method = $routerParams["_method"];

            $controllerFullName = "\\App\\Controller\\" . $controller;

            if (!method_exists($controllerFullName, $method)) {
                throw new FrameworkException("Action {$method} in controller {$controllerFullName} was not found");
            }

            //Limpa a lista de parametros do roteador
            $controllerParams = array();
            foreach ($routerParams as $paramName => $paramValue) {
                if (substr($paramName, 0, 1) != "_") {
                    $controllerParams[$paramName] = $paramValue;
                }
            }

            //Unifica a lista de parametros de URL com os parametros forçcados na rota
            if (isset($routerParams["_params"]) && is_array($routerParams["_params"])) {
                foreach ($routerParams["_params"] as $paramName => $paramValue) {
                    $controllerParams[$paramName] = $paramValue;
                }
            }

            //Instancia o controlador e injeta o request
            try {
                $controller = new $controllerFullName($request);
            } catch (Exception $e) {
                Logger::getInstance()->exception($e);
                $resposta = new JsonResponse($e->getMessage(), $e->getCode(), array(), false);
                $response = new Response(json_encode($resposta), 412, array("content-type" => "application/json"));
                return $response->send();
            }

            try {
                //Injeta e invoka o método com os parametros obtidos do roteador
                $reflectionMethod = new \ReflectionMethod($controllerFullName, $method);
                $response = $reflectionMethod->invokeArgs($controller, $controllerParams);

            } catch (Exception $e) {
                Logger::getInstance()->exception($e);
                if (Config::get('environment') == 'dev') {
                    $txt = $e->getMessage() . PHP_EOL . PHP_EOL . $e->getTraceAsString();
                    $response = new Response($txt, 200, array("content-type" => "text/plain"));
                } else {
                    throw $e;
                }
            }
        } catch (Exception $e) {
            Logger::getInstance()->error('IP: ' . $request->getClientIp());
            Logger::getInstance()->exception($e);
            $response = $this->handleException($e, $request);
        }

        //Trata a resposta
        //Tem que ser melhorado (E MUITO)
        if (!is_null($response)) {
            if (!$response instanceof Response) {
                if (is_string($response)) {
                    $response = new Response($response, 200);
                } elseif (is_array($response) || $response instanceof JsonResponse) {
                    $response = new Response(json_encode($response), 200, array("content-type" => "application/json"));
                } elseif ($response instanceof View) {
                    $response = new Response($response->render(), 200);
                } else {
                    //gerar o 404 aqui tbm
                    $response = new Response("<h1>Not Found</h1>", 404);
                }
            }

            // HACK: fix encoding to UTF-8
            $contentType = $response->headers->get('content-type');
            if (is_null($contentType)) {
                $response->headers->set('content-type', 'text/html; charset=UTF-8');
            } else if (stripos($contentType, 'charset') === false) {
                $response->headers->set('Content-type', $contentType . '; charset=UTF-8');
            } else {
                // Checa se já é UTF-8
                if (!preg_match('/charset=utf\-8/i', $contentType)) {
                    $contentType = preg_replace('/charset\s*=\s*([^\s]+)/i', $contentType, 'charset=UTF-8');
                    $response->headers->set('Content-type', $contentType);
                }
            }

            try {
                $response = $this->beforeFinish($request, $response);
            } catch (Exception $e) {
                Logger::getInstance()->error('IP: ' . $request->getClientIp());
                Logger::getInstance()->exception($e);
                $response = $this->handleException($e, $request);
                if ($response instanceof View) {
                    $response = new Response($response->render(), 200);
                }
            }

            $response->send();
        }
    }

    /**
     * Execute actions before the framework finish
     *
     * @param \Symfony\Component\HttpFoundation\Request $request
     * @param \Symfony\Component\HttpFoundation\Response $response
     * @return Response
     */
    private function beforeFinish($request, $response) {

        if (Database::$hasConnection) {
            DBLockBO::releaseAll();
            /* If has open transaction, we rollback all the things */
            $db = Database::getInstance();
            if ($db->isOnTransaction()) {
                $db->rollback();
            }
            $db->disconnect();
        }

        return $response;
    }

    /**
     * @param $routerParams
     * @param $request
     */
    private function handleMiddlewares($routerParams, $request) {
        if (array_key_exists('_middleware', $routerParams)) {
            $middlewaresList = array();
            foreach ($routerParams['_middleware'] as $middlewareGroup) {
                $configMiddleware = Config::get('middleware');
                if (isset($configMiddleware[$middlewareGroup])) {
                    $middlewaresList = array_merge($middlewaresList, $configMiddleware[$middlewareGroup]);
                }
            }

            $middlewaresList = array_reverse($middlewaresList);
            $middlewareObjects = array();
            $lastMiddleware = null;

            //Check if all the middlewares implements the interface
            foreach ($middlewaresList as $middleware) {
                $md = new $middleware($lastMiddleware);
                $middlewareObjects[] = $md;
                $lastMiddleware = $md;
            }

            $middlewareObjects = array_reverse($middlewareObjects);
            if (isset($middlewareObjects[0])) {
                $middlewareObjects[0]->handle($request);
            }
        }
    }

    private function handleException(Exception $e, Request $request) {
        $controller = new ErrorController($request);
        $response = $controller->notFound();

        if ($e instanceof DatabaseException) {
            $controller = new ErrorController($request);
            $response = $controller->databaseError($e);

            try {
                Database::getInstance()->rollback();
            } catch (Exception $e) {
                Logger::getInstance()->exception($e);
            }
        }

        if ($e instanceof BusinessException) {
            $controller = new ErrorController($request);
            $response = $controller->businessError($e);
        }

        if ($e instanceof PhpRuntimeException) {
            $controller = new ErrorController($request);
            $response = $controller->runtimeError($e);
        }

        if ($e instanceof AuthorizationException) {
            $controller = new ErrorController($request);
            $response = $controller->authorizationError($e);
        }

        if ($e instanceof AuthenticationException) {
            $controller = new ErrorController($request);
            $response = $controller->authenticationError($e);
        }

        if ($e instanceof PageNotFoundException) {
            $controller = new ErrorController($request);
            $response = $controller->pageNotFound($e);
        }

        return $response;
    }

    /**
     * Recupera um hash único para cada execução do framework
     * Utilizado na geração dos logs para identificação
     *
     * @return string
     */
    public static function getRequestId() {
        if (null === static::$requestId) {
            static::$requestId = md5(uniqid('fwint', true) . microtime(true));
        }
        return static::$requestId;
    }
}