<?php

namespace App\Core;

/**
 * Class Cache
 * @package App\Core
 */
class Cache {

    /** @var  \Memcached $engine */
    private static $engine;

    /**
     * Configura dados de cache e o inicia
     * @return Cache
     * @throws \ErrorException
     */
    public static function init() {
        if (class_exists("Memcached")) {
            $cacheServer = Config::get('caching.cache_server.ip');
            $cacheServerPort = Config::get('caching.cache_server.port');

            if (!$cacheServer || !$cacheServerPort) {
                throw new \ErrorException('Falha na recuperação das variáveis de ambiente CACHE_SERVER e CACHE_SERVER_PORT.');
            }

            self::$engine = new \Memcached();
            self::$engine->setOption(\Memcached::OPT_COMPRESSION, false);
            self::$engine->setOption(\Memcached::OPT_DISTRIBUTION, \Memcached::DISTRIBUTION_CONSISTENT);
            self::$engine->addServer($cacheServer, $cacheServerPort);
        }

        return static::$engine;
    }

    /**
     * Gera a chave do cache
     * @param $str
     * @return string
     */
    public static function generateKey($str) {
        return md5($str);
    }

    /**
     * Obtém o cache de acordo com sua chave
     * @param $key
     * @return bool
     */
    public static function get($key) {
        if (!is_null(self::$engine)) {
            return self::$engine->get($key);
        }
        return false;
    }

    /**
     * Grava/atualiza o cache
     * @param $key
     * @param $value
     * @param int $ttl
     * @return bool
     */
    public static function set($key, $value, $ttl = 600) {
        if (!is_null(self::$engine)) {
            return self::$engine->set($key, $value, $ttl);
        }
        return false;
    }

    /**
     * Remove um determinado item do cache
     *
     * @param $key
     * @return bool
     */
    public static function delete($key) {
        if (!is_null(self::$engine)) {
            return self::$engine->delete($key);
        }
    }

}
