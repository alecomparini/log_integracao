<?php
namespace App\Library;

use App\BO\ParceiroConfiguracaoBO;
use App\BO\ParceiroSiteBO;
use App\Exceptions\BusinessException;
use App\Helper\Http;
use App\Model\Configuracao;
use App\Model\Response;

class VtexSDK {

    /**
     * @var \App\Model\Parceiro
     */
    private $parceiro;

    /**
     * VtexSDK constructor.
     *
     * @param $parceiroId
     *
     * @throws \App\Exceptions\BusinessException
     * @throws \Exception
     */
    public function __construct($parceiroId) {
        $parceiroBO = new ParceiroSiteBO();
        $parceiro = $parceiroBO->getParceiroSiteById($parceiroId);

        if (is_null($parceiro)) {
            throw new BusinessException("Parceiro não encontrado com a ID {$parceiroId}");
        }
        $this->parceiro = $parceiro;
    }

    /**
     * @param $idSkuMv
     *
     * @return string
     * @throws \App\Exceptions\BusinessException
     */
    public function getIdSkuVtex($idSkuMv) {
        $configuracaoBO = new ParceiroConfiguracaoBO();
        $configuracao = $configuracaoBO->getConfiguracoes($this->parceiro->parceiroId, Configuracao::ENDPOINT_BUSCA_SKU_PARCEIRO, true);
        $response = Http::get($configuracao->valor . $idSkuMv);

        if ($response->responseCode == Http::OK) {
            return trim($response->body, '"');
        } else if ($response->responseCode == Http::NOT_FOUND) {
            $data = json_decode($response->body);
            if (json_last_error() == JSON_ERROR_NONE) {
                throw new BusinessException("Exceção VTEX: " . $data->error->message . " [{$idSkuMv}]", Response::ERRO_CONSULTA_SKU_PARCEIRO);
            } else {
                throw new BusinessException('Falha do decodificar JSON: Consulta SKU VTEX', Response::ERRO_CONSULTA_SKU_PARCEIRO);
            }
        }
        throw new BusinessException('HTTP code not 200 or 404: ' . $response->body, Response::ERRO_CONSULTA_SKU_PARCEIRO);
    }

    /**
     * @param $idsVtex
     *
     * @return array|mixed
     * @throws \App\Exceptions\BusinessException
     */
    public function getStock($idsVtex) {
        $configuracaoBO = new ParceiroConfiguracaoBO();

        $configAppToken = $configuracaoBO->getConfiguracoes($this->parceiro->parceiroId, Configuracao::BUSCA_ESTOQUE_APP_TOKEN, true);
        $configAppKey = $configuracaoBO->getConfiguracoes($this->parceiro->parceiroId, Configuracao::BUSCA_ESTOQUE_APP_KEY, true);
        $configUrlBuscaEstoque = $configuracaoBO->getConfiguracoes($this->parceiro->parceiroId, Configuracao::ENDPOINT_BUSCA_ESTOQUE_PARCEIRO, true);

        $headers = array(
            "x-vtex-api-appToken: " . $configAppToken->valor,
            "x-vtex-api-appKey: " . $configAppKey->valor,
        );

        $payload = is_array($idsVtex) ? $idsVtex : array($idsVtex);
        $response = Http::post($configUrlBuscaEstoque->valor, $payload, true, $headers);
        if ($response->responseCode == Http::OK) {
            return json_decode($response->body);
        } else {
            $response = json_decode($response->body);
            throw new BusinessException($response->error->message);
        }
        return array();
    }
}