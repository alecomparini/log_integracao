<?php
namespace App\Exceptions;

/**
 * Class ValidacaoAttachmentsException
 *
 * Excessões de validação de Attachments
 *
 * @package App\Exceptions
 * @author Cristiano Gomes <cristianogomes@softbox.com.br>
 */
class ValidacaoAttachmentsException extends \Exception{}
