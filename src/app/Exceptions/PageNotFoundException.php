<?php
namespace App\Exceptions;

use Exception;

class PageNotFoundException extends Exception {

    /**
     * PageNotFoundException constructor.
     * @param string $message
     * @param Exception $previous
     */
    public function __construct($message, Exception $previous = null) {
        parent::__construct($message, 404, $previous);
    }
}