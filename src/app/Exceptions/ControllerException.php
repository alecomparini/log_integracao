<?php

namespace App\Exceptions;

use Exception;

/**
 * Classe que captura as exceções da camada de controller
 * Class ControllerException
 * @package App\Exceptions
 */
class ControllerException extends Exception {

}