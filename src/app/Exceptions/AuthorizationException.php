<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29-02-2016
 * Time: 15:23
 */

namespace App\Exceptions;

use App\Model\Response;
use Exception;

/**
 * Classe utilizada para enviar exceçoes de permissão negada
 *
 * @package App\Exceptions
 */
class AuthorizationException extends Exception {

    /**
     * AuthorizationException constructor.
     * @param string $message
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct($message = "", $code = null, Exception $previous = null) {
        if (!is_int($code)) {
            $code = Response::PERMISSAO_NEGADA;
        }
        parent::__construct($message, $code, $previous);
    }
}