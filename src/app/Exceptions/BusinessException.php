<?php

namespace App\Exceptions;

use Exception;

/**
 * Classe que captura as exceções de regras de negócio da aplicação.
 * Class BusinessException
 * @package App\Exceptions
 */
class BusinessException extends Exception {

}