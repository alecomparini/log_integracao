<?php
namespace App\Exceptions;

use App\Model\Response;
use Exception;

/**
 * Class ValidacaoException
 *
 * Excessões de validação
 *
 * @package App\Exceptions
 * @author Cristiano Gomes <cristianogomes@softbox.com.br>
 */
class ValidacaoException extends Exception {
    /**
     * AuthorizationException constructor.
     * @param string $message
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct($message = "", $code = null, Exception $previous = null) {
        if (!is_int($code)) {
            $code = Response::ERRO_VALIDACAO;
        }
        parent::__construct($message, $code, $previous);
    }
}
