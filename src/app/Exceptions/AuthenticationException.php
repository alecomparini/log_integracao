<?php
namespace App\Exceptions;

use App\Model\Response;
use Exception;

/**
 * Classe utilizada para enviar exceçoes de permissão negada
 *
 * @package App\Exceptions
 */
class AuthenticationException extends Exception {

    /**
     * AuthorizationException constructor.
     * @param string $message
     * @param int $code
     * @param Exception|null $previous
     */
    public function __construct($message = "", $code = null, Exception $previous = null) {
        if (!is_int($code)) {
            $code = Response::NOT_AUTHENTICATED;
        }
        parent::__construct($message, $code, $previous);
    }
}