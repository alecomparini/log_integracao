<?php

namespace App\DAO;

use App\Helper\Utils;

/**
 * Class CategoriaDAO
 * @package App\DAO
 */
class CategoriaDAO extends BaseDAO {

    protected $model = 'Categoria';
    
    /**
     * Busca uma categoria e toda sub-árvore de categorias filhos dela
     * @param int[] $categoriaId ID's da busca inicial
     * @return \App\Model\Categoria[] As categorias solicitadas e todas as suas filhas
     */
    public function buscarCategoriaEFilhos(array $categoriaId) {
        $params = array(
            'categoriaId' => $categoriaId
        );
        $this->setCache(true);
        $this->setCacheTTL(Utils::secondsFromMinutes(5));
        return $this->getAll('Categoria/QryBuscaCategoriaESubCategorias', $params);
    }
    
    public function buscarSubCategorias(array $categorias) {
        $params = array(
            'categorias' => $categorias
        );
        $this->setMap(false);
        $this->setCache(true);
        $this->setCacheTTL(Utils::secondsFromMinutes(5));
        return $this->getAll('Categoria/QryBuscaSubCategoria', $params);
    }
    
    public function buscarCategorias(array $categorias) {
        $params = array(
            'categorias' => $categorias
        );
        $this->setMap(false);
        $this->setCache(true);
        $this->setCacheTTL(Utils::secondsFromMinutes(5));
        return $this->getAll('Categoria/QryBuscaCategoria', $params);
    }
}