<?php
namespace App\DAO;

/**
 * Class GarantiaDAO
 *
 * Efetua buscas relacionadas a garantia dos produtos
 *
 * @package App\DAO
 * @author Cristiano Gomes <cristianogomes@softbox.com.br>
 */
class GarantiaDAO extends BaseDAO {
    /**
     * Retorna as garantias de um produto em um determinado site de acordo com suas características e preçoPor
     * @param $produtoId
     * @param $siteId
     * @param $categorias
     * @param $precoPor
     * @return array
     */
    public function getGarantias($produtoId, $siteId, $categorias, $precoPor) {
        $params = array(
            'produtoId' => $produtoId,
            'siteId' => $siteId,
            'categorias' => $categorias,
            'precoPor' => $precoPor
        );
        return $this->getAll('Garantia/QryGetGarantias', $params);
    }

    /**
     * Confirma a garantia de um produto de acordo com seu id e o código de garantia
     * @param $produtoId
     * @param $garantiaCodigo
     * @return mixed
     */
    public function confirmaGarantia($produtoId, $garantiaCodigo) {
        $this->setMap(false);

        $params = array(
            'produtoId' => $produtoId,
            'codigo' => $garantiaCodigo
        );

        return $this->get('Garantia/QryConfirmaGarantia', $params);
    }

}