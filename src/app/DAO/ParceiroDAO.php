<?php
namespace App\DAO;

/**
 * Class ParceiroDAO
 * @package App\DAO
 */
class ParceiroDAO extends BaseDAO {
    protected $model = 'Parceiro';
    protected $entity = 'Parceiro';
    protected $pks = 'ParceiroId';

    /**
     * Busca os parceiros
     * @return \App\Model\Parceiro[]
     */
    public function getParceiroAtivo() {
        return $this->getAll("Parceiro/QryGetParceirosAtivos");
    }

}

