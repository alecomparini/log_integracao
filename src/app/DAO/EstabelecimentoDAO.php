<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 25-01-2016
 * Time: 15:14
 */

namespace App\DAO;

class EstabelecimentoDAO extends BaseDAO {

    protected $model = 'Estabelecimento';
    protected $entity = 'Estabelecimento';
    protected $pks = array('EstabelecimentoId');

    /**
     * @return \App\Model\Estabelecimento
     */
    public function getEstabelecimentoPrioritario() {
        return $this->get('Estabelecimento/QryGetEstabelecimentoPrioritario');
    }
}