<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 06/01/2016
 * Time: 10:47
 */

namespace App\DAO;

/**
 * Class ClientePfDAO
 * @package App\DAO
 */
class ClientePfDAO extends BaseDAO {

    protected $model = 'ClientePf';
    protected $entity = 'ClientePf';
    protected $pks = array('ClientePfId');

    /**
     * @param $email
     * @param $cpf
     * @param $siteId
     * @return bool
     */
    public function emailEmUso($email, $cpf, $siteId) {
        $params = array(
            'email' => $email,
            'cpf' => $cpf,
            'siteId' => $siteId
        );
        $query = "Cliente/QryEmailEmUsoPf";
        $result = $this->get($query, $params);
        return !is_null($result);
    }

}