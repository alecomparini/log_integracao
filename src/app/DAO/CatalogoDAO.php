<?php

namespace App\DAO;

/**
 * Description of CatalogoDAO
 *
 * @author suportesoftbox
 */
class CatalogoDAO extends BaseDAO {
    protected $model = 'Catalogo';
    protected $entity = 'Catalogo';
    protected $pks = array('CatalogoId');
    
    public function getCampanhaByParceiro($parceiroId) {
        $params = array(
           "parceiroId" => $parceiroId
        );
        return $this->get("Catalogo/QryGetCatalogoByParceiro", $params);
    }
}
