<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29-02-2016
 * Time: 16:03
 */

namespace App\DAO;

use App\Model\Token;

class TokenDAO extends BaseDAO {
    public $model = 'Token';
    public $entity = 'Token';
    public $pks = array('TokenId');

    /**
     * Busca dados de um token dado uma string de token
     * @param $token
     * @return \App\Model\Token
     */
    public function getToken($token) {
        return $this->get('Token/QryGetToken', array(
            'token' => $token
        ));
    }

    /**
     * @param Token $token
     * @param null $escopo
     * @return \App\Model\TokenEscopo|\App\Model\TokenEscopo[]
     */
    public function getTokenEscopo(Token $token, $escopo = null) {
        $this->model = 'TokenEscopo';
        $params = array(
            'token' => $token->token,
            'escopo' => $escopo
        );
        $escopos = $this->getAll('Token/QryGetEscopos', $params);
        if (!is_null($escopo)) {
            return $escopos[0];
        }
        return $escopos;
    }
}