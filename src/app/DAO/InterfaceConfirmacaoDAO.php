<?php

namespace App\DAO;

/**
 * Class InterfaceConfirmacaoDAO
 * @package App\DAO
 */
class InterfaceConfirmacaoDAO extends BaseDAO {
    
    public function getInterfaceConfirmacao($parceiroId, $siteId, $interfaceTipo) {
        $params = array(
            "parceiroId" => $parceiroId,
            "siteId" => $siteId,
            "interfaceTipo" => $interfaceTipo
        );
        
        $query = 'InterfaceConfirmacao/QryGetInterfaceConfirmacao';
        $this->setMap(false);
        return $this->getAll($query, $params);
    }

    public function updateInterfaceConfirmacao($interfaces, $tipo) {
        $params = array(
            'interfaces' => $interfaces
        );

        $query = 'InterfaceConfirmacao' . DS . 'QryUpdateEnviada2';
        if ($tipo == 2) {
            $query = 'InterfaceConfirmacao' . DS . 'QryUpdateEnviada0';
        }

        try {
            return $this->execute($query, $params);
        } catch (\Exception $e) {
            return false;
        }
    }
    
}