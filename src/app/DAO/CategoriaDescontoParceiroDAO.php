<?php
namespace App\DAO;

/**
 * @author: William Okano <williamokano@gmail.com>
 * @createdAt: 20-04-2016 17:40
 * @package: App\DAO
 */
class CategoriaDescontoParceiroDAO extends BaseDAO {

    protected $model = 'CategoriaDescontoParceiro';

    /**
     * Método que busca por categorias dados um devido parceiro ou uma lista de categorias
     *
     * @author William Okano <williamokano@softbox.com.br>
     * @param $categorias
     * @param $parceiroId
     * @return \App\Model\CategoriaDescontoParceiro[]
     */
    public function getDescontoCategoriaParceiro($parceiroId, array $categorias = array()) {
        $params = array(
            'categoriasId' => $categorias,
            'parceiroId' => $parceiroId
        );

        return $this->getAll('Categoria/QryBuscaDescontoCategoriaParceiro', $params);
    }
}