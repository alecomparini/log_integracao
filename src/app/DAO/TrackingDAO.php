<?php
namespace App\DAO;

use App\Core\Database;

/**
 * Rastreia pedido de parceiro
 * Class TrackingDAO
 * @package App\DAO
 */
class TrackingDAO extends BaseDAO {

    protected $model = 'Tracking';
    protected $entity = 'InterfaceTracking';
    protected $pks = 'PedidoId';

    /**
     * @param array $pedidoId   PedidoId a ser rastreado
     * @param int $parceiroId   ParceiroId do pedido
     * @param int $siteId       SiteId do pedido
     * @return array
     */
    public function find(array $pedidoId, $parceiroId, $siteId) {
        $params = array(
            "sqlIn" => $pedidoId,
            "parceiroId" => $parceiroId,
            "siteId" => $siteId
        );

        $tipos = array(
            'sqlIn' => Database::STRING
        );

        return parent::getAll("Tracking/QryGetAll", $params, $tipos);
    }
    
}
