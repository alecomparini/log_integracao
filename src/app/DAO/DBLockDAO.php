<?php

namespace App\DAO;

/**
 * Class DBLockDAO
 * @package App\DAO
 */
class DBLockDAO extends BaseDAO {

    /** @var string */
    protected $entity = 'DBLock';

    /**
     * @param $label
     * @param int $timeout
     * @return \App\Model\DBLock
     */
    public function getLock($label, $timeout = 5) {
        $params = array(
            'nome_lock' => $label,
            'timeout' => $timeout
        );
        return $this->get('QryGetLock', $params);
    }

    /**
     * @param $label
     * @param int $timeout
     * @return \App\Model\DBLock
     */
    public function releaseLock($label) {
        $params = array(
            'nome_lock' => $label
        );
        return $this->get('QryReleaseLock', $params);
    }
}