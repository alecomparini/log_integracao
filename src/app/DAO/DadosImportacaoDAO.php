<?php
namespace App\DAO;

use App\Core\Database;
use App\Model\Pagamento;
use App\Model\Pedido;
use App\Model\PedidoProduto;
use App\Model\ParceiroSite;

/**
 * Class DadosImportacaoDAO
 * @package App\DAO
 */
class DadosImportacaoDAO extends BaseDAO {

    protected $model = 'DadosImportacao';
    protected $entity = 'DadosImportacao';
    protected $pks = array('DadosImportacaoId');

    /**
     * Verifica se já foi registrado o pedido do parceiro
     *
     * @param string $parceiroPedidoId
     * @param int $parceiroSiteId
     * @return bool
     */
    public function isImportado($parceiroPedidoId, $parceiroSiteId) {
        $params = array(
            "parceiroPedidoId" => $parceiroPedidoId,
            "parceiroSiteId" => $parceiroSiteId
        );
        $tipos = array(
            'parceiroPedidoId' => Database::STRING
        );
        $query = "DadosImportacao/QryGetDadosImportacaoByPedidoParceiroId";
        $dados = $this->getAll($query, $params, $tipos);

        return is_array($dados) && sizeof($dados) > 0;
    }

    /**
     * Salva um DadosImportacao e retorna a ID da PK recém inserida
     * @param Pedido $pedido
     * @return int
     */
    public function salvarPedido(Pedido $pedido, ParceiroSite $parceiroSite) {
        $params = array(
            'clienteTipo' => $pedido->clienteTipo,
            'clientePfNome' => $pedido->clientePfNome,
            'clientePfSobrenome' => $pedido->clientePfSobrenome,
            'clientePfCpf' => $pedido->clientePfCPF,
            'clientePfDataNascimento' => $pedido->clientePfDataNascimento,
            'clientePfSexo' => $pedido->clientePfSexo,

            'clientePjRazaoSocial' => $pedido->clientePjRazaoSocial,
            'clientePjNomeFantasia' => $pedido->clientePjNomeFantasia,
            'clientePjCNPJ' => $pedido->clientePjCNPJ,
            'clientePjIsento' => $pedido->clientePjIsento,
            'clientePjInscEstadual' => $pedido->clientePjInscEstadual,
            'clientePjTelefone' => $pedido->clientePjTelefone,
            'clientePjSite' => $pedido->clientePjSite,
            'clientePjRamoAtividade' => $pedido->clientePjRamoAtividade,

            'clienteEmail' => $pedido->clienteEmail,
            'siteId' => $pedido->siteId,
            'endCobrancaTipo' => $pedido->endCobrancaTipo,
            'endCobrancaDestinatario' => $pedido->endCobrancaDestinatario,
            'endCobrancaCep' => str_pad((string)$pedido->endCobrancaCep, 8, "0", STR_PAD_LEFT),
            'endCobrancaEndereco' => $pedido->endCobrancaEndereco,
            'endCobrancaNumero' => $pedido->endCobrancaNumero,
            'endCobrancaComplemento' => $pedido->endCobrancaComplemento,
            'endCobrancaBairro' => $pedido->endCobrancaBairro,
            'endCobrancaCidade' => $pedido->endCobrancaCidade,
            'endCobrancaEstado' => $pedido->endCobrancaEstado,
            'endCobrancaTelefone1' => $pedido->endCobrancaTelefone1,
            'endCobrancaTelefone2' => $pedido->endCobrancaTelefone2,
            'endCobrancaCelular' => $pedido->endCobrancaCelular,
            'endCobrancaReferencia' => $pedido->endCobrancaReferencia,
            'endEntregaTipo' => $pedido->endEntregaTipo,
            'endEntregaDestinatario' => $pedido->endEntregaDestinatario,
            'endEntregaCep' => str_pad((string)$pedido->endEntregaCep, 8, "0", STR_PAD_LEFT),
            'endEntregaEndereco' => $pedido->endEntregaEndereco,
            'endEntregaNumero' => $pedido->endEntregaNumero,
            'endEntregaComplemento' => $pedido->endEntregaComplemento,
            'endEntregaBairro' => $pedido->endEntregaBairro,
            'endEntregaCidade' => $pedido->endEntregaCidade,
            'endEntregaEstado' => $pedido->endEntregaEstado,
            'endEntregaTelefone1' => $pedido->endEntregaTelefone1,
            'endEntregaTelefone2' => $pedido->endEntregaTelefone2,
            'endEntregaCelular' => $pedido->endEntregaCelular,
            'endEntregaReferencia' => $pedido->endEntregaReferencia,
            'valorTotal' => $pedido->valorTotal,
            'valorTotalFrete' => $pedido->valorTotalFrete,
            'parceiroIp' => $pedido->parceiroIp,
            'valorTotalProdutos' => $pedido->valorTotalProdutos,
            'valorJuros' => $pedido->valorJuros,
            'valorFreteSemPromocao' => $pedido->valorFreteSemPromocao,
            'parceiroPedidoId' => $pedido->parceiroPedidoId,
            'parceiroSiteId' => $pedido->parceiroSiteId,
            'dataEntrega' => $pedido->dataEntrega,
            'turnoEntrega' => $pedido->turnoEntrega,
            'valorEntrega' => $pedido->valorEntrega,
            'vendedorId' => $pedido->vendedorId,
            'grupoVendedorId' => $pedido->grupoVendedorId,
            'integradorId' => $pedido->integradorId,
            'tipoIntegrador' => $parceiroSite->tipoIntegrador,
            'campanhaId' => $pedido->campanhaId,
            'prazoCd' => $pedido->prazoCd,
        );
        $query = "DadosImportacao/QryInsereDadosImportacaoSetProduto";

        return $this->insert($query, $params,array(
            "endEntregaCep"  => Database::STRING,
            "endCobrancaCep" => Database::STRING
        ));
    }

    /**
     * Salva um pedido produto e retorna a ID da PK recém inserida
     * @param $dadosImportacaoId
     * @param PedidoProduto $pedidoProduto
     * @return int
     */
    public function salvarPedidoProduto($dadosImportacaoId, PedidoProduto $pedidoProduto) {
        $params = array(
            'dadosImportacaoId' => $dadosImportacaoId,
            'produtoCodigo' => $pedidoProduto->codigoProduto,
            'quantidade' => (int)$pedidoProduto->quantidade,
            'valorUnitario' => (float)$pedidoProduto->valorUnitario,
            'valorTotal' => (float)$pedidoProduto->valorTotal,
            'valorFrete' => (float)$pedidoProduto->valorFrete,
            'tipo' => (int)$pedidoProduto->tipo,
            'sequencial' => (int)$pedidoProduto->sequencial,
            'valorDesconto' => (float)$pedidoProduto->valorDesconto,
            'produtoId' => $pedidoProduto->produtoId,
            'valorCustoEntrada' => (float)$pedidoProduto->valorCustoEntrada,
            'prazoEntregaCombinado' => $pedidoProduto->prazoEntregaCombinado
        );
        $query = "PedidoProduto/QryInserirPedidoProduto";
        $types = array(
            'produtoCodigo' => \App\Core\Database::STRING
        );
        return $this->insert($query, $params, $types);
    }

    public function salvarPedidoPagamento($dadosImportacaoId, Pagamento $pagamento) {
        $params = array(
            'dadosIntegracaoId' => $dadosImportacaoId,
            'meioPagamentoId' => $pagamento->meioPagamentoId,
            'valorPago' => $pagamento->valorPago,
            'parcelas' => $pagamento->parcelas,
            'sequencial' => $pagamento->sequencial,
            'bandeiraId' => $pagamento->bandeiraId,
            'percentualJuros' => $pagamento->percentualJuros,
            'valorJuros' => $pagamento->valorJuros,
            'cartaoNumero' => $pagamento->cartaoNumero,
            'cartaoValidade' => $pagamento->cartaoValidade,
            'cartaoCodSeguranca' => $pagamento->cartaoCodSeguranca,
            'cartaoNomePortador' => $pagamento->cartaoNomePortador,
            'boletoUrl' => $pagamento->boletoUrl,
            'dataVencimento' => $pagamento->dataVencimentoBoleto,
            'codigoVale' => $pagamento->codigoVale,
            'NSU' => $pagamento->nsu,
            'autorizacaoId' => $pagamento->autorizacaoId,
            'dataAutorizacao' => $pagamento->dataAutorizacao,
            'dataCaptura' => $pagamento->dataCaptura,
            'adquirente' => $pagamento->adquirente,
            'pontos'    => $pagamento->pontos
        );
        $query = "DadosImportacao/QryInserirPedidoPagamento";
        $bindTipos = array(
            'cartaoNumero' => Database::STRING,
            'cartaoCodSeguranca' => Database::STRING,
        );
        return $this->insert($query, $params, $bindTipos);
    }
}
