<?php
namespace App\DAO;

use App\BO\ParceiroSitePermissaoBO;
use App\Core\Database;

/**
 * Class ProdutoEstoqueDAO
 * @package App\DAO
 */
class ProdutoEstoqueDAO extends BaseDAO {

    protected $model = 'ProdutoEstoque';
    protected $entity = null;
    protected $pks = null;

    /**
     * Retorna os estoques dos produtos listados
     *
     * @param $parceiroId
     * @param $siteId
     * @param null $produtoCodigo
     * @return array
     */
    public function getEstoques($parceiroId, $siteId, $produtoCodigo = null, $ignoraStatus=false) {
        $this->setMap(false);
        $params = array(
            "parceiroId" => $parceiroId,
            "siteId" => $siteId
        );
        $query = (!$ignoraStatus) ? "QryGetEstoqueParceiro" : "QryGetEstoqueParceiroIgnoraStatus";

        if (!is_null($produtoCodigo)) {
            if (!is_array($produtoCodigo)) {
                $produtoCodigo = array($produtoCodigo);
            }
            if (sizeof($produtoCodigo) > 0) {
                $params["produtoCodigo"] = $produtoCodigo;
                $query = (!$ignoraStatus) ? "QryGetEstoqueProdutoParceiro" : "QryGetEstoqueProdutoParceiroIgnoraStatus";
            }
        }
        $types = array(
            'produtoCodigo' => \App\Core\Database::STRING
        );
        return $this->getAll("ProdutoEstoque/" . $query, $params, $types);
    }

    /**
     * GetEstoquePlus
     *
     * Retorna os estoques dos produtos listados, ou de todos os produtos liberados para o parceiro considerando
     * o estoque dos Combos pelos seus FILHOS
     *
     * @param integer $siteId
     * @param array $produtoCodigo
     * @param int $tamanhoPagina
     * @param $parceiroId
     * @return array
     */
    public function getEstoquePlus($siteId, array $produtoCodigo, $tamanhoPagina = 100, $parceiroId) {
        $parceiroPermissaoBO = new ParceiroSitePermissaoBO();
        $dadosPermissao = $parceiroPermissaoBO->getDadosPermissao($parceiroId, $siteId);

        if (!$dadosPermissao['todos']) {
            if ($dadosPermissao['campanha'] == 0) {
                if (sizeof($dadosPermissao['categorias']) == 0 && sizeof($dadosPermissao['fabricantes']) == 0) {
                    return array();
                }
            } else {
                $parceiroDAO = new ParceiroSiteDAO();
                $parceiroCampanha = $parceiroDAO->getParceiroSiteCampanhaAtiva($parceiroId, (array) $produtoCodigo);
                if (count($parceiroCampanha) > 0) {
                    $produtoCodigo = $parceiroCampanha;
                } else {
                    return array();
                }
            }
        }

        $params = array_merge($dadosPermissao, array(
            "produtoCodigo" => $produtoCodigo,
            "siteId" => $siteId,
            "tamanhoPagina" => $tamanhoPagina
        ));

        $tipos = array(
            'produtoCodigo' => Database::STRING
        );

        $query = is_null($produtoCodigo) ? 'QryGetEstoqueParceiroPlus' : 'QryGetEstoqueProdutoPlus';

        $this->setMap(false);

        return $this->getAll("ProdutoEstoque/" . $query, $params, $tipos);
    }

    /**
     * GetEstoqueRetail
     *
     * Retorna os estoques dos produtos listados de acordo com o código do Retail
     *
     * @param array $produtoCodigoRetail 
     * @return array
     */
    public function getEstoqueRetail($codigoRetail) {

        $params = array(
            "codigoRetail" => $codigoRetail
        );

        $tipos = array(
            'codigoRetail' => Database::INT
        );

        $query = 'QryGetEstoqueProdutoRetail';

        $this->setMap(false);

        $this->setCache(true);

        $this->setCacheTTL(3600);

        return $this->getAll("ProdutoEstoque/" . $query, $params, $tipos);
    }
}
