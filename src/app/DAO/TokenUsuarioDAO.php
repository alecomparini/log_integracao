<?php
namespace App\DAO;

use App\Core\Config;
use App\Model\V2\TokenUsuario;
use App\Model\V2\Usuario;
use DateTime;
use Firebase\JWT\JWT;

class TokenUsuarioDAO extends BaseDAO {
    protected $model = 'V2\TokenUsuario';

    /**
     * @param $token
     *
     * @return \App\Model\V2\TokenUsuario
     */
    public function buscarTokenUsuario($token) {
        $params = array(
            'token' => $token,
        );

        return $this->get('Token/QryBuscarTokenUsuario', $params);
    }

    /**
     * @param \App\Model\V2\Usuario $usuario
     * @param int                   $validadeEmSegundos
     *
     * @return \App\Model\V2\TokenUsuario
     * @internal param $idUsuario
     */
    public function criarTokenUsuario(Usuario $usuario, $validadeEmSegundos = 3600) {
        $agora = null;
        if ($validadeEmSegundos > 0) {
            $agora = new DateTime('now');
            $agora->add(new \DateInterval("PT{$validadeEmSegundos}S"));
        }

        $key = Config::get('jwt_key');
        $payload = array(
            'usuarioId' => $usuario->usuarioId,
            'iss' => time()
        );

        $params = array(
            'usuarioId' => $usuario->usuarioId,
            'parceiroId' => $usuario->parceiroId,
            'dataValidade' => null,
        );

        if (null !== $agora) {
            $params['dataValidade'] = $agora->format('Y-m-d H:i:s');
            $payload['exp'] = $agora->getTimestamp();
        }

        $token = JWT::encode($payload, $key);
        $params['token'] = $token;

        $id = $this->insert('Token/QryCriarTokenUsuario', $params);

        $tokenUsuario = new TokenUsuario();
        $tokenUsuario->tokenUsuarioId = $id;
        $tokenUsuario->token = $token;
        $tokenUsuario->usuarioId = $usuario->usuarioId;
        $tokenUsuario->dataValidade = null === $agora ? null : $agora->format('Y-m-d H:i:s');
        $tokenUsuario->dataCriacao = date('Y-m-d H:i:s');

        return $tokenUsuario;
    }

    public function removerToken($token) {
        return $this->execute('Token/QryRemoverToken', array('token' => $token));
    }
}