<?php
namespace App\DAO;

use App\Core\Database;
use App\BO\ParceiroSiteBO;
use App\Exceptions\BusinessException;
use App\Exceptions\DatabaseException;
use App\Model\Pagamento;
use App\Model\ParceiroSite;
use App\Model\Pedido;
use App\Model\PedidoAttachment;
use App\Model\PedidoGarantia;
use App\Model\PedidoProduto;
use App\Model\PedidoSeguro;
use App\Model\Response;
use App\Model\V2\StatusExportacaoPedido;

/**
 * Classe responsável por gerenciar todas a persistência de dados
 * dos pedidos gerados na integração. Como todo DAO, esta classe não
 * é responsável por fazer validações de negócio, cabendo a ela
 * realizar operações de persistência, busca e outros tipos de manipulação
 * de dados nos objetos utilizados para se gerar um pedido.
 *
 * @package App\DAO
 */
class PedidoDAO extends BaseDAO {
    protected $model = 'Pedido';

    /**
     * Método responsável por persistir um pedido no banco de dados.
     * Ele realiza a persistência de um objeto pedido completo, salvando o
     * cabeçalho, seus pagamentos e seus produtos.
     *
     * Todos os métodos podem ser utilizados individualmente caso necessário
     *
     * @param \App\Model\Pedido $pedido
     * @param bool              $prePedido
     *
     * @return bool
     * @throws \App\Exceptions\DatabaseException
     */
    public function salvarPedido(Pedido $pedido, $prePedido = false) {
        $dadosImportacaoId = $this->salvarCabecalho($pedido, $prePedido);
        $this->salvarPagamentos($dadosImportacaoId, $pedido);
        $this->salvarProdutos($dadosImportacaoId, $pedido);
    }

    /**
     * Salva o cabeçalho de um pedido.
     * Entenda por cabeçalho todos os atributos da classe pedido.
     *
     * @table riel_integracao.DadosImportacao
     *
     * @param \App\Model\Pedido $pedido
     * @param bool              $prePedido
     *
     * @return int
     */
    public function salvarCabecalho(Pedido $pedido, $prePedido = false) {
        $parceiroSiteDAO = new ParceiroSiteDAO();
        $parceiro = $parceiroSiteDAO->getParceiroSiteById($pedido->parceiroId);

        $params = array(
            'email' => null !== $pedido->clienteEmail ? $pedido->clienteEmail : '',
            'dataCadastro' => date('Y-m-d H:i:s'),
            'tipo' => null !== $pedido->clienteTipo ? $pedido->clienteTipo : '',
            'siteId' => $pedido->siteId,
            'nome' => null !== $pedido->clientePfNome ? $pedido->clientePfNome : '',
            'sobrenome' => null !== $pedido->clientePfSobrenome ? $pedido->clientePfSobrenome : '',
            'cpf' => null !== $pedido->clientePfCPF ? $pedido->clientePfCPF : '',
            'dataNascimento' => null !== $pedido->clientePfDataNascimento ? $pedido->clientePfDataNascimento : '',
            'sexo' => null !== $pedido->clientePfSexo ? $pedido->clientePfSexo : '',
            'razaoSocial' => null !== $pedido->clientePjRazaoSocial ? $pedido->clientePjRazaoSocial : '',
            'nomeFantasia' => null !== $pedido->clientePjNomeFantasia ? $pedido->clientePjNomeFantasia : '',
            'cnpj' => null !== $pedido->clientePjCNPJ ? $pedido->clientePjCNPJ : '',
            'inscricaoEstadual' => $pedido->clientePjInscEstadual,
            'isento' => null !== $pedido->clientePjIsento ? $pedido->clientePjIsento : '0',
            'telefone' => $pedido->clientePjTelefone,
            'site' => $pedido->clientePjSite,
            'ramoAtividade' => null !== $pedido->clientePjRamoAtividade ? $pedido->clientePjRamoAtividade : 0,
            'cobrancaTipo' => null !== $pedido->endCobrancaTipo ? $pedido->endCobrancaTipo : '',
            'cobrancaDestinatario' => null !== $pedido->endCobrancaDestinatario ? $pedido->endCobrancaDestinatario : '',
            'cobrancaCep' => null !== $pedido->endCobrancaCep ? $pedido->endCobrancaCep : '',
            'cobrancaEndereco' => null !== $pedido->endCobrancaEndereco ? $pedido->endCobrancaEndereco : '',
            'cobrancaNumero' => $pedido->endCobrancaNumero,
            'cobrancaComplemento' => $pedido->endCobrancaComplemento,
            'cobrancaBairro' => null !== $pedido->endCobrancaBairro ? $pedido->endCobrancaBairro : '',
            'cobrancaCidade' => null !== $pedido->endCobrancaCidade ? $pedido->endCobrancaCidade : '',
            'cobrancaEstado' => null !== $pedido->endCobrancaEstado ? $pedido->endCobrancaEstado : '',
            'cobrancaTelefone1' => null !== $pedido->endCobrancaTelefone1 ? $pedido->endCobrancaTelefone1 : '',
            'cobrancaTelefone2' => $pedido->endCobrancaTelefone2,
            'cobrancaCelular' => $pedido->endCobrancaCelular,
            'cobrancaReferencia' => $pedido->endCobrancaReferencia,
            'entregaTipo' => null !== $pedido->endEntregaTipo ? $pedido->endEntregaTipo : '',
            'entregaDestinatario' => null !== $pedido->endEntregaDestinatario ? $pedido->endEntregaDestinatario : '',
            'entregaCep' => null !== $pedido->endEntregaCep ? $pedido->endEntregaCep : '',
            'entregaEndereco' => null !== $pedido->endEntregaEndereco ? $pedido->endEntregaEndereco : '',
            'entregaNumero' => $pedido->endEntregaNumero,
            'entregaComplemento' => $pedido->endEntregaComplemento,
            'entregaBairro' => null !== $pedido->endEntregaBairro ? $pedido->endEntregaBairro : '',
            'entregaCidade' => null !== $pedido->endEntregaCidade ? $pedido->endEntregaCidade : '',
            'entregaEstado' => null !== $pedido->endEntregaEstado ? $pedido->endEntregaEstado : '',
            'entregaTelefone1' => null !== $pedido->endEntregaTelefone1 ? $pedido->endEntregaTelefone1 : '',
            'entregaTelefone2' => $pedido->endEntregaTelefone2,
            'entregaCelular' => $pedido->endEntregaCelular,
            'entregaReferencia' => $pedido->endEntregaReferencia,
            'cupomDescontoId' => null,
            'valorTotal' => null !== $pedido->valorTotal ? $pedido->valorTotal : '',
            'valorTotalFrete' => $pedido->valorTotalFrete,
            'valorCupomDesconto' => null,
            'valorCupomDescontoFrete' => null,
            'ip' => $pedido->parceiroIp,
            'statusExportacao' => $prePedido ? 4 : 0,
            'valorTotalProdutos' => $pedido->valorTotalProdutos,
            'valorDespesas' => 0,
            'valorJuros' => $pedido->valorJuros,
            'valorFreteSemPromocao' => $pedido->valorFreteSemPromocao,
            'parceiroPedidoId' => null !== $pedido->parceiroPedidoId ? $pedido->parceiroPedidoId : '',
            'producaoPedidoId' => null,
            'parceiroSiteId' => $parceiro->parceiroSiteId,
            'dataEntrega' => null !== $pedido->dataEntrega ? $pedido->dataEntrega : '0000-00-00',
            'turnoEntrega' => null !== $pedido->turnoEntrega ? $pedido->turnoEntrega : '',
            'valorEntrega' => null !== $pedido->valorEntrega ? $pedido->valorEntrega : '',
            'vendedorId' => null,
            'grupoVendedorId' => null,
            'integradorId' => $pedido->integradorId,
            'tipoIntegrador' => null,
            'campanhaId' => $pedido->campanhaId,
        );

        return $this->insert('DadosImportacao/QryInserirDadosImportacao', $params);
    }

    /**
     * Realizar a persistência de todos os meios de pagamento informados no pedido.
     * Apenas um wrapper para o salvarPagamento, onde ao invés do pedido é recebido
     * uma instância de Pagamento.
     *
     * @table riel_integracao.Pagamento
     *
     * @param                   $dadosImportacaoId ID gerada pelo cabeçalho
     * @param \App\Model\Pedido $pedido
     *
     * @return $this Retorna o mesmo objeto para interface fluente
     */
    public function salvarPagamentos($dadosImportacaoId, Pedido $pedido) {
        foreach ($pedido->pagamento as $pagamento) {

            /* FIX DATA CAPTURA É IGUAL DATA AUTORIZAÇÃO VISTO QUE ELES NÃO TEM A DATA DE CAPTURA */
            if (Pagamento::CARTAO_PAGO === (int)$pagamento->meioPagamentoId && $pagamento->dataCaptura === '') {
                $pagamento->dataCaptura = $pagamento->dataAutorizacao;
            }
            $this->salvarPagamento($dadosImportacaoId, $pagamento);
        }

        return $this;
    }

    /**
     * Realizar a persistência de um único objeto pagamento.
     * Não é responsável por fazer validações.
     *
     * @table riel_integracao.Pagamento
     *
     * @param                      $dadosImportacaoId  ID gerada pelo cabeçalho
     * @param \App\Model\Pagamento $pagamento
     *
     * @return int
     */
    public function salvarPagamento($dadosImportacaoId, Pagamento $pagamento) {
        $params = array(
            'dadosIntegracaoId' => $dadosImportacaoId,
            'meioPagamentoId' => $pagamento->meioPagamentoId,
            'bandeiraId' => $pagamento->bandeiraId,
            'valorPago' => $pagamento->valorPago,
            'parcelas' => $pagamento->parcelas,
            'percentualJuros' => $pagamento->percentualJuros,
            'valorJuros' => $pagamento->valorJuros,
            'cartaoNumero' => $pagamento->cartaoNumero,
            'cartaoValidade' => $pagamento->cartaoValidade,
            'cartaoCodSeguranca' => $pagamento->cartaoCodSeguranca,
            'cartaoNomePortador' => $pagamento->cartaoNomePortador,
            'boletoUrl' => $pagamento->boletoUrl,
            'dataVencimento' => $pagamento->dataVencimentoBoleto,
            'codigoVale' => $pagamento->codigoVale,
            'sequencial' => $pagamento->sequencial,
            'pontos' => $pagamento->pontos,
            'NSU' => $pagamento->nsu,
            'autorizacaoId' => $pagamento->autorizacaoId,
            'dataAutorizacao' => $pagamento->dataAutorizacao,
            'dataCaptura' => $pagamento->dataCaptura,
            'adquirente' => $pagamento->adquirente,
        );

        return $this->insert('DadosImportacao/QryInserirPedidoPagamento', $params);
    }

    /**
     * @param                   $dadosImportacaoId
     * @param \App\Model\Pedido $pedido
     */
    public function salvarProdutos($dadosImportacaoId, Pedido $pedido) {
        $produtoDAO = new ProdutoDAO();
        foreach ($pedido->pedidoProduto as $pedidoProduto) {
            // Atualiza o valor do custo de entrada antes de salvar
            $pedidoProduto->valorCustoEntrada = $produtoDAO->getProdutoCustoEntrada($pedidoProduto->codigoProduto);

            $this->salvarProduto($dadosImportacaoId, $pedidoProduto);
        }
    }

    /**
     * @param                          $dadosImportacaoId
     * @param \App\Model\PedidoProduto $pedidoProduto
     *
     * @return int
     */
    public function salvarProduto($dadosImportacaoId, PedidoProduto $pedidoProduto) {
        $params = array(
            'dadosImportacaoId' => $dadosImportacaoId,
            'produtoId' => null !== $pedidoProduto->produtoId ? $pedidoProduto->produtoId : 0,
            'produtoCodigo' => $pedidoProduto->codigoProduto,
            'quantidade' => $pedidoProduto->quantidade,
            'valorUnitario' => $pedidoProduto->valorUnitario,
            'valorTotal' => $pedidoProduto->valorTotal,
            'valorFrete' => $pedidoProduto->valorFrete,
            'tipo' => $pedidoProduto->tipo,
            'prazoEntregaCombinado' => $pedidoProduto->prazoEntregaCombinado,
            'sequencial' => $pedidoProduto->sequencial,
            'valorDesconto' => $pedidoProduto->valorDesconto,
            'valorCustoEntrada' => $pedidoProduto->valorCustoEntrada,
        );
        $pedidoProdutoId = $this->insert('DadosImportacao/QryInserirPedidoProduto', $params);

        if (!empty($pedidoProduto->garantia)) {
            $this->salvarGarantia($dadosImportacaoId, $pedidoProdutoId, $pedidoProduto->garantia);
        }

        if (!empty($pedidoProduto->seguro)) {
            $this->salvarSeguro($dadosImportacaoId, $pedidoProdutoId, $pedidoProduto->seguro);
        }

        if (!empty($pedidoProduto->attachments)) {
            $this->salvarAttachments($dadosImportacaoId, $pedidoProdutoId, $pedidoProduto);
        }

        return $pedidoProdutoId;
    }

    /**
     * @param                           $dadosImportacaoId
     * @param                           $pedidoProdutoId
     * @param \App\Model\PedidoGarantia $garantia
     *
     * @return int
     */
    public function salvarGarantia($dadosImportacaoId, $pedidoProdutoId, PedidoGarantia $garantia) {
        $params = array(
            'dadosImportacaoId' => $dadosImportacaoId,
            'pedidoProdutoId' => $pedidoProdutoId,
            'garantiaCodigo' => $garantia->codigo,
            'quantidade' => $garantia->quantidade,
            'prazo' => $garantia->prazo,
            'valorVenda' => $garantia->valorVenda,
        );
        $pedidoGarantiaId = $this->insert('DadosImportacao/QryInserirPedidoProdutoGarantia', $params);
        $garantia->produtoGarantiaId = $pedidoGarantiaId;

        return $pedidoGarantiaId;
    }

    /**
     * @param                         $dadosImportacaoId
     * @param                         $pedidoProdutoId
     * @param \App\Model\PedidoSeguro $seguro
     *
     * @return int
     */
    public function salvarSeguro($dadosImportacaoId, $pedidoProdutoId, PedidoSeguro $seguro) {
        $params = array(
            'dadosImportacaoId' => $dadosImportacaoId,
            'pedidoProdutoId' => $pedidoProdutoId,
            'seguroTabelaId' => $seguro->seguroTabelaId,
            'quantidade' => $seguro->quantidade,
            'valorVenda' => $seguro->valorVenda,
            'familia' => $seguro->familia,
        );
        $pedidoSeguroId = $this->insert('DadosImportacao/QryInserirPedidoProdutoSeguro', $params);
        $seguro->pedidoSeguroId = $pedidoSeguroId;

        return $pedidoSeguroId;
    }

    /**
     * @param                          $dadosImportacaoId
     * @param                          $pedidoProdutoId
     * @param \App\Model\PedidoProduto $pedidoProduto
     */
    public function salvarAttachments($dadosImportacaoId, $pedidoProdutoId, PedidoProduto $pedidoProduto) {
        foreach ($pedidoProduto->attachments as $attachment) {
            $this->salvarAttachment($dadosImportacaoId, $pedidoProdutoId, $attachment);
        }
    }

    /**
     * @param                             $dadosImportacaoId
     * @param                             $pedidoProdutoId
     * @param \App\Model\PedidoAttachment $attachment
     *
     * @return int
     */
    public function salvarAttachment($dadosImportacaoId, $pedidoProdutoId, PedidoAttachment $attachment) {
        $params = array(
            'dadosImportacaoId' => $dadosImportacaoId,
            'pedidoProdutoId' => $pedidoProdutoId,
            'tipo' => $attachment->type,
            'rotulo' => $attachment->label,
            'dataCriacao' => date('Y-m-d H:i:s'),
        );
        $pedidoAttachmentId = $this->insert('DadosImportacao/QryInserirPedidoProdutoAttachment', $params);
        $attachment->pedidoAttachmentId = $pedidoAttachmentId;

        return $pedidoAttachmentId;
    }

    /**
     * @return \App\Model\Pedido[] Pré-pedidos
     */
    public function buscarPrePedidos() {
        return $this->buscarPedidoPorStatusExportacao(StatusExportacaoPedido::PRE_PEDIDO);
    }

    /**
     * @return \App\Model\Pedido[]
     */
    public function buscarPedidosErroExportacao() {
        return $this->buscarPedidoPorStatusExportacao(StatusExportacaoPedido::PRE_PEDIDO_ERRO);
    }

    /**
     * @param $dadosImportacaoId
     *
     * @return \App\Model\Pedido
     */
    public function buscarPedidoPorDadosImportacaoId($dadosImportacaoId) {
        $params = array('dadosImportacaoId' => $dadosImportacaoId);
        $pedido = $this->get('DadosImportacao/QryBuscarPedidoPorDadosImportacaoId', $params);

        if ($pedido !== null) {
            $pedidoPagamentoDAO = new PedidoPagamentoDAO();
            $pedidoProdutoDAO = new PedidoProdutoDAO();

            $pedido->pagamento = $pedidoPagamentoDAO->buscarPagamentosPedido($pedido);
            $pedido->pedidoProduto = $pedidoProdutoDAO->buscarPedidoProdutoPorImportacaoId($pedido->dadosImportacaoId);
        }

        return $pedido;
    }

    /**
     * @param $statusExportacao
     *
     * @return \App\Model\Pedido[]
     */
    public function buscarPedidoPorStatusExportacao($statusExportacao) {
        $pedidoDAO = new PedidoDAO();
        $pedidoPagamentoDAO = new PedidoPagamentoDAO();
        $pedidoProdutoDAO = new PedidoProdutoDAO();

        // Busca cabeçalho do Pedido
        $pedidos = $pedidoDAO->buscarDadosImportacaoPorStatusExportacao($statusExportacao);

        /** @var \App\Model\Pedido[] $pedidos */
        foreach ($pedidos as $pedido) {
            $pedido->pagamento = $pedidoPagamentoDAO->buscarPagamentosPedido($pedido);
            $pedido->pedidoProduto = $pedidoProdutoDAO->buscarPedidoProdutoPorImportacaoId($pedido->dadosImportacaoId);
        }

        return $pedidos;
    }

    /**
     * @param $parceiroSiteId
     * @param $dataCorte
     *
     * @return \App\Model\Pedido[]
     */
    public function buscarPedidosPassivelCancelamento($parceiroSiteId, $dataCorte) {
        $params = array(
            'statusExportacao' => StatusExportacaoPedido::PRE_PEDIDO_ERRO,
            'dataCorte' => $dataCorte,
            'parceiroSiteId' => $parceiroSiteId,
        );

        return $this->getAll('DadosImportacao/QryBuscarPedidosPassivelCancelamento', $params);
    }

    /**
     * @param $parceiroPedidoId
     * @param $parceiroId
     *
     * @return \App\Model\Pedido[]
     */
    public function buscarPedidosPorParceiroPedidoId($parceiroPedidoId, $parceiroId) {
        $parceiroSiteBO = new ParceiroSiteBO();
        $parceiro = $parceiroSiteBO->getParceiroSiteById($parceiroId);

        $pedidos = $this->getAll('DadosImportacao/QryBuscarPedidosPorParceiroPedidoId', array(
            'parceiroSiteId' => $parceiro->parceiroSiteId,
            'parceiroPedidoId' => $parceiroPedidoId,
        ),array(
            'parceiroPedidoId' => Database::STRING
        ));

        $pedidoPagamentoDAO = new PedidoPagamentoDAO();
        $pedidoProdutoDAO = new PedidoProdutoDAO();
        foreach ($pedidos as $pedido) {
            $pedido->pagamento = $pedidoPagamentoDAO->buscarPagamentosPedido($pedido);
            $pedido->pedidoProduto = $pedidoProdutoDAO->buscarPedidoProdutoPorImportacaoId($pedido->dadosImportacaoId);
        }

        return $pedidos;
    }

    /**
     * @param $statusExportacao
     *
     * @return \App\Model\Pedido[]
     */
    public function buscarDadosImportacaoPorStatusExportacao($statusExportacao) {
        // Backup the old model value
        $oldModel = $this->model;
        $this->model = 'Pedido';

        $params = array('statusExportacao' => $statusExportacao);

        /** @var \App\Model\Pedido[] $response */
        $response = $this->getAll('DadosImportacao/QryBuscarDadosImportacaoPorStatusExportacao', $params);

        // Restore the model to his original state
        $this->model = $oldModel;

        return $response;
    }

    /**
     * @param \App\Model\Pedido $pedido
     * @param                   $statusExportacao
     *
     * @return bool
     */
    public function defineStatusExportacao(Pedido $pedido, $statusExportacao) {
        $params = array(
            'dadosImportacaoId' => $pedido->dadosImportacaoId,
            'statusExportacao' => $statusExportacao,
        );

        return $this->execute('DadosImportacao/QryDefineStatusExportacao', $params);
    }

    /**
     * @param \App\Model\Pedido $pedido
     * @param                   $mensagem
     *
     * @return bool
     */
    public function defineErroProcessamento(Pedido $pedido, $mensagem) {
        $erroProcessamento = $this->buscarErroProcessamento($pedido);
        $params = array(
            'dadosImportacaoId' => $pedido->dadosImportacaoId,
            'mensagem' => $mensagem,
            'dataErro' => date('Y-m-d H:i:s'),
        );

        if (null !== $erroProcessamento) {
            $params['dadosImportacaoErroId'] = $erroProcessamento->dadosImportacaoErroId;
        }

        return $this->execute('DadosImportacao/QryDefineErroProcessamento', $params);
    }

    /**
     * @param \App\Model\Pedido $pedido
     *
     * @return mixed
     */
    public function buscarErroProcessamento(Pedido $pedido) {
        $params = array(
            'dadosImportacaoId' => $pedido->dadosImportacaoId
        );

        return $this->get('DadosImportacao/QryBuscarErroProcessamento', $params);
    }

    public function defineMotivoCancelamento($dadosImportacaoId, $motivoCancelamento, $usuarioIdCancelamento) {
        $params = array(
            'dadosImportacaoId' => $dadosImportacaoId,
            'motivoCancelamento' => $motivoCancelamento,
            'usuarioIdCancelamento' => $usuarioIdCancelamento,
        );

        return $this->execute('DadosImportacao/QryDefineMotivoCancelamento', $params);
    }
}