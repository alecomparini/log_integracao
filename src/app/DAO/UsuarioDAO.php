<?php
namespace App\DAO;

class UsuarioDAO extends BaseDAO {
    protected $model = 'V2\Usuario';

    /**
     * @param $email
     *
     * @return \App\Model\V2\Usuario
     */
    public function buscarPorEmail($email) {
        $params = array('email' => $email);

        return $this->get('Usuario/QryBuscarPorEmail', $params);
    }

    /**
     * @param int $usuarioId
     *
     * @return \App\Model\V2\Usuario
     */
    public function buscarPorId($usuarioId) {
        $params = array('usuarioId' => $usuarioId);

        return $this->get('Usuario/QryBuscarPorId', $params);
    }
}