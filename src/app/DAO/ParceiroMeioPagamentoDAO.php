<?php

namespace App\DAO;

/**
 * DAO da entity ParceiroMeioPagamento
 *
 * @author Ítallo Costa <itallocosta@softbox.com.br>
 */
class ParceiroMeioPagamentoDAO extends BaseDAO {
    protected $model = 'ParceiroMeioPagamento';
    protected $entity = 'Parceiro_MeioPagamento';
    protected $pks = array('ParceiroId','MeioPagamentoId');
    
    
    public function getMeiosPagamentoByParceiro($parceiroId, $meioPagamentoId=null) {
        $params = array(
            "parceiroId" => $parceiroId,
            "meioPagamentoId" => $meioPagamentoId
        );
        $this->setCache(true);
        return $this->get("ParceiroMeioPagamento/QryMeioPagamentoByParceiro", $params);
    }
    
}
