<?php

namespace App\DAO;

/**
 * Dao da classe CatalalogoCategoria
 *
 * @author Ítallo Costa <itallocosta@softbox.com.br>
 */
class CatalogoCategoriaDAO extends BaseDAO {
    protected $model = 'CatalogoCategoria';
    protected $entity = 'Catalogo_Categoria';
    protected $pks = array('CatalogoId');
    
    public function getCatalogoCategoriaByCatalogo($catalogoId) {
        $params = array(
            'catalogoId' => $catalogoId
        );
        return $this->getAll("CatalogoCategoria/QryGetCatalogoCategoriaByCatalogo", $params);
    }
    
    public function getCampanhaByParceiro($parceiroId) {
        $params = array(
           "parceiroId" => $parceiroId
        );
        return $this->get("Catalogo/QryGetCatalogoByParceiro", $params);
    }

    public function getCatalogoCategoriaByParceiro($parceiroId) {
	$params = array(
           "parceiroId" => $parceiroId
        );
        return $this->getAll("CatalogoCategoria/QryGetCatalogoCategoriaByParceiro", $params);
    }

}