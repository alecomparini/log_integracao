<?php

namespace App\DAO;

use App\Model\Configuracao;
use App\BO\ParceiroConfiguracaoBO;
use App\Model\Response;
use App\Exceptions\ValidacaoAttachmentsException;

/**
 * Class AttachmentDao
 * @package App\DAO
 */
class AttachmentDao extends BaseDAO{
    protected $model = 'PedidoAttachment';
    protected $entity = 'Pedido_Attachment';

    /**
     * Persiste os attachments
     *
     * @param $dadosImportacaoId
     * @param $pedidoProdutoId
     * @param $attachment
     *
     * @return int
     */
    public function insertAttachment($dadosImportacaoId, $pedidoProdutoId, $attachment) {
        $params = array(
            'dadosImportacaoId' => $dadosImportacaoId,
            'pedidoProdutoId' => $pedidoProdutoId,
            'tipo' => $attachment->type,
            'rotulo' => $attachment->label
        );

        return $this->insert('Attachment/QryInsertAttachment', $params);
    }

    /**
     * Valida se o item do pedido deve conter attachments
     *
     * @param array $dadosItem
     * @param int $parcieroId
     * @throws ValidacaoAttachmentsException
     */
    public function validarItemAttachmentsObrigatorio($dadosItem, $parcieroId) {
        $parceiroConfigBO = new ParceiroConfiguracaoBO();
        $att = $parceiroConfigBO->getConfiguracoes((int)$parcieroId, Configuracao::ITENS_PARCEIRO_ATTACH_OBRIGATORIO, true);

        if (empty($att)) {
            return;
        }

        $itensAttachObrigatorio = array_flip(explode(";", $att->valor));

        if (isset($itensAttachObrigatorio[$dadosItem->codigoProduto])) {
            throw new ValidacaoAttachmentsException($dadosItem->codigoProduto, Response::ITEM_ATTACHMENTS_OBRIGATORIO);
        }
    }
}
