<?php
namespace App\DAO;

/**
 * Class SeguroDAO
 *
 * Efetua buscas relacionadas aos seguros
 *
 * @package App\DAO
 * @author Cristiano Gomes <cristianogomes@softbox.com.br>
 */
class SeguroDAO extends BaseDAO {
    protected $model = 'SeguroTabela';

    /**
     * Busca seguro elegível segundo lógica presente nos sites MV2
     * @param $precoPor
     * @param array $categoriasIds
     * @return \App\Core\instance|\App\Core\stdClass
     */
    public function getSeguroElegivel($precoPor, $categoriasIds) {

        if (is_numeric($categoriasIds)) {
            $categoriasIds = array($categoriasIds);
        }

        $params = array(
            "categoriasId" => $categoriasIds,
            'precoPor' => $precoPor
        );

        return $this->get("Seguro/QryGetSeguroElegivel", $params);
    }
}
