<?php
namespace App\DAO;

use App\Core\Database;

/**
 * Realiza operações em pedidos de parceiros
 * Class PedidoParceiroDAO
 * @package App\DAO
 */
class PedidoParceiroDAO extends BaseDAO {

    /**
     * @param int $parceidoId ID do parceiro
     * @param int[] $pedidosParceiroId ID's enviadas pelo parceiro
     * @return \App\Model\PedidoParceiro[] Pedidos parceiros encontrado
     */
    public function getPedidosParceiro($parceidoId, array $pedidosParceiroId) {
        $params = array(
            'pedidoParceiro' => $pedidosParceiroId,
            'parceiroId' => $parceidoId
        );
        $tipos = array(
            'pedidoParceiro' => Database::STRING
        );
        $oldForceSelectOnMaster = $this->isForceSelectOnMaster();
        $this->forceSelectOnMaster(true);
        $response = $this->getAll('Pedido/QryGetByPedidoParceiro', $params, $tipos);
        $this->forceSelectOnMaster($oldForceSelectOnMaster);
        return $response;
    }

    /**
     * Recebe uma lista de pedidoId's e retorna informa��es sobre o pedido e parceiro
     *
     * @param $pedidosIds
     * @return array
     */
    public function getDadosPedidoById($pedidosIds) {
        $params = array(
            'pedidoIds' => $pedidosIds
        );

        return $this->getAll('Pedido/QryGetDadosPedidoById', $params);
    }

}
