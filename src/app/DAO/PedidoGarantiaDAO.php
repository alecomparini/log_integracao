<?php
namespace App\DAO;

use App\Model\PedidoGarantia;

/**
 * Class GarantiaDAO
 *
 * Efetua buscas relacionadas a garantia dos produtos
 *
 * @package App\DAO
 * @author Cristiano Gomes <cristianogomes@softbox.com.br>
 */
class PedidoGarantiaDAO extends BaseDAO {

    /**
     * Registra a garantia do pedido
     * @param PedidoGarantia $garantia
     * @return int
     */
    public function setPedidoGarantia(PedidoGarantia $garantia) {
        $this->setMap(false);

        $params = array(
            'pedidoProdutoId'   => $garantia->pedidoProdutoId,
            'dadosImportacaoId' => $garantia->dadosImportacaoId,
            'garantiaCodigo'    => $garantia->codigo,
            'valorVenda'        => $garantia->valorVenda,
            'prazo'             => $garantia->prazo,
            'quantidade'        => $garantia->quantidade
        );

        return $this->insert('PedidoGarantia/QryInsertPedidoGarantia', $params);
    }
}