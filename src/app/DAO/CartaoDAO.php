<?php
namespace App\DAO;

/**
 * Class CartaoDAO
 *
 * Efetua as buscas relacionadas a Cartão no banco de dados
 *
 * @package App\DAO
 */
class CartaoDAO extends BaseDAO {
    public function getFatoresCartao() {
        $this->setMap(false);
        return $this->getAll("Cartao/QryGetFatorCartao", array());
    }
}
