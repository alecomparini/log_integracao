<?php
namespace App\DAO;

/**
 * Class ParceiroSiteDAO
 *
 * @package App\DAO
 */
class ParceiroSiteDAO extends BaseDAO {
    protected $model = 'ParceiroSite';
    protected $entity = 'Parceiro_Site';
    protected $pks = 'ParceiroSiteId';

    /**
     * Retorna o parceiro site de um parceiro do site
     *
     * @param $parceiroId
     * @param $siteId
     *
     * @return \App\Core\instance|\App\Core\stdClass
     */
    public function getParceiroSite($parceiroId, $siteId) {
        $params = array(
            "parceiro_id" => $parceiroId,
            "site_id" => $siteId
        );

        return $this->get("ParceiroSite/QryGetParceiroSite", $params);
    }

    /**
     * Retorna um parceiro site buscando pela ID.
     * Não utilizei o método base pois essa objeto estende outro
     *
     * @param  int $parceiroId ID do parceiro
     *
     * @return \App\Model\ParceiroSite|null
     */
    public function getParceiroSiteById($parceiroId) {
        $params = array(
            "parceiroId" => $parceiroId
        );
        $this->setCache(true);
        return $this->get("ParceiroSite/QryGetParceiroSiteById", $params);
    }

    /**
     * Retorna os IDs dos produtos do catálogo da campanha ativa a qual o parceiro está vinculado
     *
     * @param  int  $parceiroId
     * @param array $produtoCodigo
     *
     * @return mixed
     */
    public function getParceiroSiteCampanhaAtiva($parceiroId, $produtoCodigo = array()) {
        $params = array(
            "parceiroId" => $parceiroId,
            "produtoCodigo" => $produtoCodigo
        );
        $arrayRetorno = array();
        $dados = $this->getAll("ParceiroSite/QryGetParceiroSiteCampanhaAtiva", $params);
        if (empty($dados)) {
            return (array)$dados;
        } else {
            foreach ($dados as $item) {
                $arrayRetorno[] = $item->Codigo;
            }

            return $arrayRetorno;
        }
    }

    /**
     * Busca todos os parceiros
     *
     * @return \App\Model\ParceiroSite[]
     */
    public function buscarTodosParceiros() {
        return $this->getAll('ParceiroSite/QryGetAll');
    }
}
