<?php

namespace App\DAO;

/**
 * Class NotificacaoDAO
 *
 * @package App\DAO
 * @author Cristiano Gomes <cristianogomes@softbox.com.br>
 */
class NotificacaoDAO extends BaseDAO {

    public function setNotificacao($notificacao, $tipoNotificacao) {
        $tipoNotificacao = (int)$tipoNotificacao;
        $estoque = 0;
        $preco = 0;

        if ($tipoNotificacao === 1) {
            $estoque = 1;
        } elseif ($tipoNotificacao === 2) {
            $preco = 1;
        }

        $notificacao['tipoNotificacao'] = $tipoNotificacao;
        $notificacao['preco'] = $preco;
        $notificacao['estoque'] = $estoque;

        return $this->insert('Notificacao/QryInsertNotificacao', $notificacao);
    }

    /**
     * Busca a quantidade de envios faltantes
     *
     * @param bool $groupByParceiro Se enviado, agrupa por paceiro
     * @return \stdClass
     */
    public function getTotalNotificacoes($groupByParceiro = false) {
        $this->setMap(false);
        $this->setCache(false);
        $metodo = 'get';
        if ($groupByParceiro) {
            $metodo = 'getAll';
        }
        return $this->$metodo('Notificacao/QryGetTotalNotificacoes', array('groupByParceiro' => $groupByParceiro));
    }

    /**
     * Busca a quantidade de envios faltantes
     *
     * @param bool $groupByParceiro Se enviado, agrupa por paceiro
     * @return \stdClass
     */
    public function getTotalNotificacoesErro() {
        $this->setMap(false);
        $this->setCache(false);
        return $this->get('Notificacao/QryGetTotalNotificacoesErro');
    }

    public function deletaNotificacaoParceiroTentativa($parceiroId, $tentativas) {
        $params = array(
            'parceiroId' => $parceiroId,
            'tentativas' => $tentativas
        );
        return $this->execute('ParceiroSite/QryDeletaNotificacaoParceiroIdTentativas', $params);
    }
}
