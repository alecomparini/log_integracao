<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 06/01/2016
 * Time: 10:47
 */

namespace App\DAO;

/**
 * Class ClientePjDAO
 * @package App\DAO
 */
class ClientePjDAO extends BaseDAO {

    protected $model = 'ClientePj';
    protected $entity = 'ClientePj';
    protected $pks = array('ClientePfId');

    /**
     * @param $email
     * @param $cnpj
     * @param $siteId
     * @return bool
     */
    public function emailEmUso($email, $cnpj, $siteId) {
        $params = array(
            'email' => $email,
            'cnpj' => $cnpj,
            'siteId' => $siteId
        );
        $query = "Cliente/QryEmailEmUsoPj";
        $result = $this->get($query, $params);
        return !is_null($result);
    }

}