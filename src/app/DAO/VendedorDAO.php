<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 31/12/2015
 * Time: 10:44
 */

namespace App\DAO;

/**
 * Class VendedorDAO
 * @package App\DAO
 */
class VendedorDAO extends BaseDAO {
    protected $model = 'Vendedor';
    protected $entity = 'OperadorB2C';
    protected $pks = array('OperadorB2CId');

    /**
     * Retorna dados do vendedor de acordo com o email
     * @param $email
     * @return \App\Model\Vendedor
     */
    public function getVendedorByEmail($email) {
        $params = array(
            'email' => $email
        );

        return $this->get('Vendedor/QryGetVendedorByEmail', $params);
    }
}