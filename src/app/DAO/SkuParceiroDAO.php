<?php
namespace App\DAO;

use App\Core\Database;

class SkuParceiroDAO extends BaseDAO {

    /**
     * Busca as equivalencias de SKU no sistema dos parceiros
     *
     * @param int       $parceiroId
     * @param string[]  $codigos
     *
     * @return \App\Model\SkuParceiro[]
     */
    public function buscarSkuParceiro($parceiroId, array $codigos) {
        $query = "Produto/QryBuscaSkuParceiro";
        $params = array("codigos" => $codigos, "parceiroId" => $parceiroId);
        $tipos = array("codigos" => Database::STRING);

        return $this->getAll($query, $params, $tipos);
    }

    /**
     * Insere no nosso banco para evitar chamar o ENDPOINT
     * do parceiro para buscar o SKU no sistema do Parceiro
     *
     * @param int       $parceiroId
     * @param string    $codigo
     * @param string    $skuParceiro
     *
     * @return int Last Inserted ID
     */
    public function inserirSkuParceiro($parceiroId, $codigo, $skuParceiro) {
        $query = "Produto/QryInserirSkuParceiro";
        $params = array(
            "parceiroId" => $parceiroId,
            "codigo" => $codigo,
            "skuParceiro" => $skuParceiro
        );
        $tipos = array(
            "parceiroId" => Database::INT,
            "codigo" => Database::STRING,
            "skuParceiro" => Database::STRING
        );
        return $this->insert($query, $params, $tipos);
    }
}