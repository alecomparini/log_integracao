<?php
namespace App\DAO;

use App\Model\PedidoSeguro;

/**
 * Class PedidoSeguroDAO
 *
 * Efetua buscas relacionadas ao seguro dos produtos
 *
 * @package App\DAO
 * @author Cristiano Gomes <cristianogomes@softbox.com.br>
 */
class PedidoSeguroDAO extends BaseDAO {
    /**
     * Registra o seguro do pedido
     * @param PedidoSeguro $seguro
     * @return int
     */
    public function setPedidoSeguro(PedidoSeguro $seguro) {
        $this->setMap(false);

        $params = array(
            'pedidoProdutoId'   => $seguro->pedidoProdutoId,
            'dadosImportacaoId' => $seguro->dadosImportacaoId,
            'SeguroTabelaId'    => $seguro->seguroTabelaId,
            'valorVenda'        => $seguro->valorVenda,
            'quantidade'        => $seguro->quantidade,
            'familia'           => $seguro->familia
        );

        return $this->insert('PedidoSeguro/QryInsertPedidoSeguro', $params);
    }
}
