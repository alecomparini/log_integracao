<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 05-02-2016
 * Time: 17:40
 */

namespace App\DAO;


class FatorCartaoDAO extends BaseDAO {

    protected $model = 'FatorCartao';
    protected $entity = 'FatorCartao';
    protected $pks = array('FatorCartaoId');

    /**
     * Busca os fatores de uma respectiva Bandeira
     * @param $bandeiraId
     * @return \App\Model\FatorCartao[]
     */
    public function getFatores($bandeiraId) {
        $params = array(
            'bandeiraId' => $bandeiraId
        );
        return $this->getAll('FatorCartao/QryGetFatorCartaoByBandeira', $params);
    }
}