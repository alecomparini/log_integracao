<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 06/01/2016
 * Time: 14:56
 */

namespace App\DAO;

/**
 * Class CategoriaSiteDAO
 * @package App\DAO
 */
class CategoriaSiteDAO extends CategoriaDAO {
    protected $model = 'CategoriaSite';
    protected $entity = 'Categoria_Site';
    protected $pks = array('CategoriaId');

    /**
     * Retorna as categorias dos filhos de acordo com a categoria do produto pai para um determinado site
     * @param $idCategoriaPai
     * @param $siteId
     * @return array
     */
    public function buscaCategoriasFilho(array $idCategoriaPai, $siteId) {
        $params = array(
            'idCategoriaPai' => $idCategoriaPai,
            'siteId' => $siteId
        );
        if (count($idCategoriaPai) == 0) {
            return array();
        }
        $query = 'Categoria/QryBuscaCategoriaFilhos';
        return $this->getAll($query, $params);
    }

}