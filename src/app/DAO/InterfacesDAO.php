<?php
namespace App\DAO;

use App\Model\Response;

/**
 * Class InterfacesDAO
 * @package App\DAO
 */
class InterfacesDAO extends BaseDAO {
    protected $model = 'InterfaceModel';
    protected $entity = null;
    protected $pks = null;

    public function checkDadosInterface($parceiroId, $siteId, $interface) {
        $parceiroSiteDAO = new ParceiroSiteDAO;
        $parceiroSite = $parceiroSiteDAO->getParceiroSite($parceiroId, $siteId);
        $parceiroSiteId = $parceiroSite->parceiroSiteId;

        $enviado = $this->getQuantidadeEnviados($parceiroSiteId, $interface);
        if ($enviado->Quantidade > 0) {
            throw new \Exception("Erro: Dados enviados, aguardando confirmacao", Response::DADOS_INTERFACE_ENVIADO);
        } else {
            $estoque = $this->getDadosInterface($parceiroSiteId, $interface);

            if (sizeof($estoque) > 0) {
                //Atualiza dados envio
                $this->atualizaDadosEnvio($estoque);
            }

            return $estoque;
        }
    }

    private function atualizaDadosEnvio($estoque) {
        $interfaceIds = array();
        foreach ($estoque as $item) {
            $interfaceIds[] = $item->interfaceId;
        }

        $params = array(
            "interfaceIds" => $interfaceIds
        );

        return $this->execute("Interface/QryUpdateDadosEnvio", $params);
    }

    public function getQuantidadeEnviados($parceiroSiteId, $interfaceTipoId) {
        $params = array(
            "parceiroSiteId" => $parceiroSiteId,
            "interfaceTipoId" => $interfaceTipoId
        );

        return $this->get("Interface/QryCheckEnviadas", $params);
    }

    public function getDadosInterface($parceiroSiteId, $interface) {
        $sql = "Interface/QryGetInterfacesEnviar";

        $params = array(
            "parceiroSiteId" => $parceiroSiteId,
            "interfaceTipoId" => $interface
        );
        return $this->getAll($sql, $params);
    }

}
