<?php
namespace App\DAO;

use App\Core\Logger;
use App\Exceptions\DatabaseException;
use App\Model\V2\AgendamentoTarefa;

class AgendamentoTarefaDAO extends BaseDAO {
    protected $model = 'V2\AgendamentoTarefa';

    /**
     * @return AgendamentoTarefa[]
     */
    public function getAtivos() {
        return $this->getAll("Tarefa/QryGetAgendamentoAtivo");
    }

    /**
     * Busca um agendamento por ID
     *
     * @param $id
     *
     * @return AgendamentoTarefa
     */
    public function buscarPorIdAgendamento($id) {
        return $this->get('Tarefa/QryGetAgendamentoById', array('agendamentoTarefaId' => $id));
    }

    /**
     * Insere um registro de log de execução
     *
     * @param $id
     * @param $httpCode
     * @param $dataInicioExecucao
     * @param $dataTerminoExecucao
     * @param $body
     * @param $headers
     *
     * @return int
     * @internal param $dataInicio
     */
    public function salvarLogExecucao($id, $httpCode, $dataInicioExecucao, $dataTerminoExecucao, $body, $headers) {
        $params = array(
            'agendamentoTarefaId' => $id,
            'httpStatusCode' => $httpCode,
            'dataInicioExecucao' => $dataInicioExecucao,
            'dataTerminoExecucao' => $dataTerminoExecucao,
            'respostaHttp' => $body,
            'cabecalhosHttp' => $headers,
        );

        return $this->insert('Tarefa/QryInserirLog', $params);
    }

    public function atualizarDataUltimaExecucao(AgendamentoTarefa $agendamentoTarefa) {

        // Faz backup da flag de seleção no master pra poder voltar ao fim da execução
        $forceOnMasterOldValue = $this->isForceSelectOnMaster();
        $this->forceSelectOnMaster(true);

        $params = array(
            'agendamentoTarefaId' => $agendamentoTarefa->agendamentoTarefaId,
            'tarefaId' => $agendamentoTarefa->tarefaId,
            'dataUltimaExecucao' => date('Y-m-d H:i:s'),
        );
        try {
            $this->beginTransaction();
            $this->execute('Tarefa/QryAtualizarDataUltimaExecucaoAgendamento', $params);
            $this->execute('Tarefa/QryAtualizarDataUltimaExecucaoTarefa', $params);
            $this->commit();
        } catch (DatabaseException $dbe) {
            $this->rollback();
            Logger::getInstance()->exception($dbe);
            throw $dbe;
        }

        // Volta o valor antigo
        $this->forceSelectOnMaster($forceOnMasterOldValue);

        return true;
    }
}