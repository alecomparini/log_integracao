<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 05-02-2016
 * Time: 17:06
 */

namespace App\DAO;


class BandeiraDAO extends BaseDAO {
    protected $model = 'Bandeira';
    protected $entity = 'Bandeira';
    protected $pks = array('BandeiraId');

    /**
     * Retorna todas as bandeiras ativas
     * @return \App\Model\Bandeira[]
     */
    public function all() {
        return $this->getAll('Bandeira/QryGetAll');
    }

    /**
     * Retorna as bandeiras ativas com o(s) ID(s) informados
     * @param array|int $bandeiraId ID(s) das bandeiras a serem buscadas
     * @return \App\Model\Bandeira[]
     */
    public function findById($bandeiraId) {
        //Hack para buscar sempre com IN
        if (!is_array($bandeiraId)) {
            $bandeiraId = array($bandeiraId);
        }

        return $this->getAll('Bandeira/QryFindBandeira', array('bandeiraId' => $bandeiraId));
    }
}