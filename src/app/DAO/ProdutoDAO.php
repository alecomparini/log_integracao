<?php
namespace App\DAO;

use App\BO\CampanhaBO;
use App\BO\ParceiroSiteBO;
use App\BO\ParceiroSitePermissaoBO;
use App\Core\Config;
use App\Core\Database;
use App\Core\Logger;
use App\Exceptions\DatabaseException;
use App\Model\PedidoProduto;
use App\Model\V2\DadosPaginacao;
use App\Model\V2\ProdutosPaginado;
use InvalidArgumentException;

/**
 * Classe responsável por buscas relacionadas a produtos
 *
 * @author Cristiano Gomes <cristianogomes@softbox.com.br>
 */
class ProdutoDAO extends BaseDAO {

    protected $model = 'Produto';

    /**
     * Verifica no banco de dados e retorna a lista de produtos válidos recebidos pelo request
     *
     * @param array $produtoCodigo
     * @return mixed
     */
    public function getCodProdutos($produtoCodigo) {
        $params = array(
            "codigos" => $produtoCodigo
        );
        $types = array(
            'codigos' => Database::STRING
        );
        return $this->getAll("Produto/QryGetCodProdutos", $params, $types);
    }

    /**
     * Verifica no banco de dados e retorna a lista de produtos válidos recebidos pelo request
     *
     * @param array $produtoCodigoRetail
     * @return mixed
     */
    public function getCodProdutosRetail($produtoCodigoRetail) {
        $params = array(
            "codigosRetail" => $produtoCodigoRetail
        );
        $types = array(
            'codigosRetail' => Database::STRING
        );
        return $this->getAll("Produto/QryGetCodProdutosRetail", $params, $types);
    }

    /**
     * Retorna a lista de produtos
     * @param $siteId
     * @param $produtosId
     * @param null $dadosPaginacao
     * @param $parceiroId
     * @param array $produtoStatus
     * @return array
     */
    public function getProdutosFull($siteId, $produtosId, $dadosPaginacao = null, $parceiroId = null, array $produtoStatus = array(0,1,2)) {
        //$this->beginTransaction();
        try {
            $parceiroPermissaoBO = new ParceiroSitePermissaoBO();
            $dadosPermissao = $parceiroPermissaoBO->getDadosPermissao($parceiroId, $siteId);

            $params = $dadosPermissao;
            $params['siteId'] = $siteId;
            $params['produtosId'] = $produtosId;
            $params['status'] = $produtoStatus;
            if (is_array($dadosPaginacao) && count($dadosPaginacao) > 0) {
                $params['limite'] = $dadosPaginacao['limite'];
                $params['offsete'] = $dadosPaginacao['offset'];
                $response = $this->getAll("Produto/QryGetProdutosFullPaginado", $params);
            } else {
                $response = $this->getAll("Produto/QryGetProdutosFull", $params);
            }
            //$this->commit();
        } catch (DatabaseException $dbe) {
            $response = array();
            Logger::getInstance()->alert($dbe->getMessage());
            //$this->rollback();
        }
        return $response;
    }

    /**
     * Retorna a lista de produtos
     * @param int $siteId
     * @param int $produtosId
     * @param $produtoPai
     * @param array|null $dadosPaginacao
     * @param array $produtoStatus
     * @return array
     */
    public function getProdutosFullCombo($siteId, $produtosId, $produtoPai, $dadosPaginacao = null, array $produtoStatus = array(0,1,2)) {
        $params = array(
            'siteId' => $siteId,
            'produtosId' => $produtosId,
            'produtoPai' => $produtoPai,
            'status' => $produtoStatus
        );

        if (is_array($dadosPaginacao) && count($dadosPaginacao) > 0) {
            $params['limite'] = $dadosPaginacao['limite'];
            $params['offsete'] = $dadosPaginacao['offset'];
        }

        return $this->getAll("Produto/QryGetProdutosFullCombo", $params);
    }

    public function getComboFilhos($codCombo, $siteId) {
        $params = array(
            'codComboPai' => $codCombo,
            'siteId' => $siteId
        );
        return $this->getAll("Produto/QryGetComboFilhos", $params);
    }

    /**
     * Busca todos os produtos de determinado Site filtrando apenas pelo status = 1
     *
     * @param integer $siteId
     * @return array
     */
    public function getProdutosAll($siteId) {
        $params = array(
            'siteId' => $siteId
        );
        return $this->getAll("Produto/QryGetProdutosAll", $params);
    }

    /**
     * Busca informações sobre um determinado produto
     *
     * @param $siteId
     * @param $produtoId
     * @return array
     */
    public function getProdutos($siteId, $produtoId) {
        $params = array(
            'siteId' => $siteId,
            'produtoId' => $produtoId
        );
        return $this->getAll("Produto/QryGetProduto", $params);
    }

    public function getProdutosChanged($siteId, $dataAlteracao) {
        $params = array(
            'siteId' => $siteId,
            'dataAlteracao' => $dataAlteracao
        );
        return $this->getAll("Produto/QryGetProdutosChanged", $params);
    }

    /**
     * Busca algumas informações adicionais sobre o produto
     *
     * @param $produtoId
     * @param $siteId
     * @return \stdClass
     */
    public function getProdutoInfo($produtoId, $siteId) {
        $params = array(
            'produtoId' => $produtoId,
            'siteId' => $siteId
        );
        return $this->get("Produto/QryGetProdutoInfo", $params);
    }

    /**
     * Busca produtos de acordo com as regras estabelecidas de exceção e permissão.
     * @param $produtoCodigo
     * @param $parceiroId
     * @return array
     */
    public function buscaProdutoPermissao($produtoCodigo, $parceiroId) {
        $parceiroSiteBO = new ParceiroSiteBO();
        $campanhaBO = new CampanhaBO();
        $parceiro = $parceiroSiteBO->getParceiroSiteById($parceiroId);
        $produtos = array();
                
        $parceiroPermissaoBO = new ParceiroSitePermissaoBO();
        $dadosPermissao = $parceiroPermissaoBO->getDadosPermissao($parceiroId, $parceiro->siteBase);

        if (!$dadosPermissao['todos'] && sizeof($dadosPermissao['categorias']) == 0 && sizeof($dadosPermissao['fabricantes']) == 0 && !$dadosPermissao["campanha"]) {
            return array();
        }
                
        if (!empty($dadosPermissao["campanha"])) {
            $produtos = $campanhaBO->getCampanhaByProduto($dadosPermissao["campanha"], $produtoCodigo, $parceiro->siteBase);
        } else if ((sizeof($dadosPermissao['categorias']) > 0 || sizeof($dadosPermissao['fabricantes']) > 0 || $dadosPermissao["todos"]) && empty($dadosPermissao["campanha"])) {
            $params = array_merge($dadosPermissao, array(
                    'produtoCodigo' => $produtoCodigo,
                    'siteId' => $parceiro->siteBase,
                )
            );

            $tipos = array(
                'produtoCodigo' => Database::STRING
            );
            
            $this->setMap(false);
            $query = "Produto/QryGetProdutoPermissao";
            $produtos = $this->getAll($query, $params, $tipos);
        }
        return $produtos;
    }

    /**
     * Retorna um stdClass contendo ProdutoId e a quantidade disponível em estoque.
     * @param string $codigo
     * @return \App\Core\instance|\App\Core\stdClass
     */
    public function getProdutoEstoque($codigo) {
        $params = array(
            'codigo' => $codigo
        );
        $types = array(
            'codigo' => \App\Core\Database::STRING
        );
        $this->setMap(false);
        $query = "Produto/QryBuscaProdutoEstoque";
        return $this->get($query, $params, $types);
    }

    /**
     * Busca o ValorCustoEntrada de um produto dado um código
     * @param string $codigo
     * @return float
     */
    public function getProdutoCustoEntrada($codigo) {
        $params = array(
            'codigo' => $codigo
        );
        $types = array(
            'codigo' => \App\Core\Database::STRING
        );
        $result = $this->get("Produto/QryBuscaCustoEntrada", $params, $types);
        if (is_null($result)) {
            return 0.00;
        }
        return (float)$result->ValorCustoEntrada;
    }

    /**
     * Recebe um conjunto de produtosId's e retorna seus respectivos códigos
     * Utilizado para quando o sistema é invocado a partir dos produtos MV.COM que passam
     * produtoId ao invés de Código
     * @param $produtosCodigos
     * @return array
     */
    public function getCodsById($produtosCodigos) {
        $this->setMap(false);
        $params = array(
            'produtosCodigos' => $produtosCodigos
        );
        $types = array(
            'produtosCodigos' => \App\Core\Database::STRING
        );
        return $this->getAll("Produto/QryGetCodsById", $params, $types);
    }

    /**
     * Retorna o total de produtos disponíveis para o parceiro com base em seu siteId
     * @param $siteId
     * @param $parceiroId
     * @return \App\Core\instance|\App\Core\stdClass
     */
    public function getTotalProdutos($siteId, $parceiroId, array $produtoStatus = array(0,1,2)) {
        $parceiroPermissaoBO = new ParceiroSitePermissaoBO();
        $dadosPermissao = $parceiroPermissaoBO->getDadosPermissao($parceiroId, $siteId);

        if (!$dadosPermissao['todos'] && sizeof($dadosPermissao['categorias']) == 0 && sizeof($dadosPermissao['fabricantes']) == 0 && !$dadosPermissao["campanha"]) {
            return (object)array(
                'total' => 0
            );
        }

        $params = $dadosPermissao;
        $params['status'] = $produtoStatus;

        $this->setMap(false);
        $this->setCache(true);
        $this->setCacheTTL(1200);
        $params['siteId'] = $siteId;

        return $this->get("Produto/QryGetTotalProdutos", $params);
    }

    /**
     * @param     $parceiroId
     * @param int $pagina
     *
     * @return \App\Model\V2\ProdutosPaginado
     * @throws \App\Exceptions\ValidacaoException
     * @throws \Exception
     * @throws \InvalidArgumentException
     */
    public function buscarPaginado($parceiroId, $pagina = 1, array $produtoStatus = array(0,1,2)) {

        // Levanta as regras
        $params = $this->buscarPermissoes($parceiroId);

        // Busca o parceiro
        $parceiroBO = new ParceiroSiteBO();
        $parceiro = $parceiroBO->getParceiroSiteById($parceiroId);

        $params = array_merge(array(
            'emLinha' => 1,
            'status' => $produtoStatus
        ), $params);

        $itensPorPagina = Config::get('produtos.produtos-por-pagina', 100);
        $totalItens = $this->contar($parceiro->siteBase, $params);
        $dadosPaginacao = new DadosPaginacao($totalItens, $itensPorPagina);

        // Garante que a página não é negativa ou 0
        if ($pagina < 1 || $pagina > $dadosPaginacao->totalPaginas) {
            throw new InvalidArgumentException('A página informada não existe.');
        }

        $paramsPaginacao = array_merge(
            $params,
            array(
                'limit' => $dadosPaginacao->getLimit(),
                'offset' => $dadosPaginacao->getOffsetPagina($pagina)
            )
        );

        $tipos = array('limit' => Database::INT, 'offset' => Database::INT);

        $produtos = $this->buscar($parceiro->siteBase, $paramsPaginacao, $tipos);

        $produtosPaginado = new ProdutosPaginado();
        $produtosPaginado->produtos = $produtos;
        $produtosPaginado->dadosPaginacao = array(
            'totalProdutos' => $totalItens,
            'produtosPorPagina' => $itensPorPagina,
            'totalPaginas' => $dadosPaginacao->totalPaginas,
            'paginaAtual' => $pagina
        );

        return $produtosPaginado;
    }

    /**
     * @param int   $parceiroId
     * @param array $codigos
     *
     * @return \App\Model\V2\Produto[]
     * @throws \Exception
     */
    public function buscarPorCodigo($parceiroId, array $codigos = array()) {
        // Levanta as regras
        $params = $this->buscarPermissoes($parceiroId);

        // Busca o parceiro
        $parceiroBO = new ParceiroSiteBO();
        $parceiro = $parceiroBO->getParceiroSiteById($parceiroId);
        $params = array_merge(array(
            'emLinha' => 1,
            'codigo' => $codigos
        ), $params);
        $tipos = array('codigo' => Database::STRING);

        return $this->buscar($parceiro->siteBase, $params, $tipos);
    }

    private function buscarPermissoes($parceiroId) {
        $parceiroBO = new ParceiroSiteBO();
        $parceiro = $parceiroBO->getParceiroSiteById($parceiroId);

        $parceiroPermissaoBO = new ParceiroSitePermissaoBO();
        return $parceiroPermissaoBO->getDadosPermissao($parceiroId, $parceiro->siteBase);
    }

    /**
     * Busca produtos de acordo com a query informada
     *
     * @param int   $siteId
     * @param array $params
     * @param array $types
     *
     * @return \App\Model\V2\Produto[]
     */
    public function buscar($siteId, array $params = array(), array $types = array()) {
        $oldModel = $this->model;
        $this->model = "V2\\Produto";
        $params = array_merge(array(
            'siteId' => $siteId
        ), $params);

        $types = array_merge(array('siteId' => Database::INT), $types);

        $query = "Produto/V2/QryGetProdutoFull";
        $response = $this->getAll($query, $params, $types);
        $this->model = $oldModel;
        return $response;
    }

    /**
     * Conta produtos de acordo com a query informada
     *
     * @param int   $siteId
     * @param array $params
     * @param array $types
     *
     * @return int
     */
    public function contar($siteId, array $params = array(), array $types = array()) {
        $params = array_merge(array(
            'siteId' => $siteId
        ), $params);

        $types = array_merge(array(
            'siteId' => Database::INT
        ), $types);

        $query = "Produto/V2/QryCountProdutoFull";
        $this->setMap(false);
        $this->setCache(true);
        $this->setCacheTTL(1200);

        /** @var \stdClass $retorno */
        $retorno = $this->get($query, $params, $types);

        $this->setCache(false);
        $this->setMap(true);

        return $retorno->total;
    }

    /**
     * @param                          $dadosImportacaoId
     * @param \App\Model\PedidoProduto $pedidoProduto
     * @param                          $produtoId
     *
     * @return bool
     */
    public function atualizarPedidoProdutoProdutoId($dadosImportacaoId, PedidoProduto $pedidoProduto, $produtoId) {
        $params = array(
            'dadosImportacaoId' => $dadosImportacaoId,
            'pedidoProdutoId' => $pedidoProduto->pedidoProdutoId,
            'produtoId' => $produtoId,
        );

        return $this->execute('DadosImportacao/QryAtualizarPedidoProdutoProdutoId', $params);
    }

    /**
     * Busca o nome do produto de acordo com o informado pelo parceiro na integração
     *
     * @param $codProduto código do produto enviado pelo parceiro
     * @return array
     */
    public function buscarNomeProdutoSitePorCodigo($codProduto) {
        $this->setMap(false);
        $params = array(
            'codigo' => $codProduto
        );
        $types = array(
            'codigo' => \App\Core\Database::STRING
        );
        return $this->getAll("Produto/QryBuscarNomeProduto", $params, $types);
    }
}
