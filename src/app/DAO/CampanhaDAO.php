<?php
namespace App\DAO;


/**
 * Classe DAO representando a entidade Campanha
 *
 * @author Itallo Costa <itallocosta@softbox.com.br>
 */
class CampanhaDAO extends BaseDAO {
    protected $model = 'Campanha';
    protected $entity = 'Campanha';
    protected $pks = array('CampanhaId');
    
    public function getCampanhaByProduto($campanhaId, $siteBase, $produtoCodigo) {
        $params = array(
            "campanhaId" => $campanhaId,
            "siteId" => $siteBase,
            "produtoCodigo" => $produtoCodigo
            
        );
        
        $this->setMap(false);
        return $this->get("Campanha/QryGetProdutosCampanha", $params);
    }
    
    public function getCampanhaByParceiro($parceiroId) {
        $params = array(
           "parceiroId" => $parceiroId
        );
        
        return $this->get("Campanha/QryGetCampanhaByParceiro", $params);
    }

}
