<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 06/01/2016
 * Time: 10:47
 */

namespace App\DAO;

/**
 * Class ClienteDAO
 * @package App\DAO
 */
class ClienteDAO extends BaseDAO {

    protected $model = 'Cliente';
    protected $entity = 'Cliente';
    protected $pks = array('ClienteId');

}