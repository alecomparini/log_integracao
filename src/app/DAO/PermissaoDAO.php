<?php
namespace App\DAO;

/**
 * Class PermissaoDAO
 * @package App\DAO
 */
class PermissaoDAO extends BaseDAO {
    protected $model = 'Tracking';
    protected $entity = 'InterfaceTracking';
    protected $pks = 'PedidoId';
}
