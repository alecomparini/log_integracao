<?php

namespace App\DAO;

/**
 * Class InterfacePermissaoDAO
 * @package App\DAO
 */
class InterfacePermissaoDAO extends BaseDAO {
    protected $model = 'InterfacePermissao';
    protected $entity = null;
    protected $pks = null;
    
    public function getInterfacePermissaoByParceiroSiteId($parceiroSiteId, $interfaceTipo) {
        $params = array(
            "parceiroSiteId" => $parceiroSiteId,
            "interfaceTipo" => $interfaceTipo
        );
        
        $sql = "InterfacePermissao/QryGetInterfacePermissaoByParceiroInterface";
        
        return $this->get($sql, $params);
    }
    
}