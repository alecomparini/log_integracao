<?php
namespace App\DAO;

/**
 * Class ProdutoSiteDAO
 * @package App\DAO
 */
class ProdutoSiteDAO extends BaseDAO {

    protected $model = 'ProdutoSite';

    /**
     * Retorna o preço por dos produtos para um determinado parceiro e códigos dos produtos
     * @param $parceiroId
     * @param $codigos
     * @return \App\Core\instance|\App\Core\stdClass
     */
    public function getProdutoSite($parceiroId, $codigos) {
        $params = array(
            "parceiro_id" => $parceiroId,
            "codigos" => $codigos
        );
        $types = array(
            'codigos' => \App\Core\Database::STRING
        );
        return $this->get("ProdutoSite/QryGetProdutoSite", $params, $types);
    }

}
