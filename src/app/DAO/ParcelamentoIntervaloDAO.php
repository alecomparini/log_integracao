<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 05-02-2016
 * Time: 15:07
 */

namespace App\DAO;

class ParcelamentoIntervaloDAO extends BaseDAO {
    protected $model = 'ParcelamentoIntervalo';
    protected $entity = 'ParcelamentoIntervalo';
    protected $pks = array('ParcelamentoIntervaloId');

    public function getParcelamentoIntervalo($valor) {
        $params = array('valor' => $valor);
        return $this->get('Parcelamento/QryGetParcelamentoIntervalo', $params);
    }

}