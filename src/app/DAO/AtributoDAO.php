<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 17-03-2016
 * Time: 17:35
 */

namespace App\DAO;

/**
 * Classe responsável por fazer a interface com o banco de dados
 * e retornar os atributos de acordo com o solicitado
 * @package App\DAO
 */
class AtributoDAO extends BaseDAO {
    protected $entity = null;
    protected $model = 'AtributoCorTamanho';
    protected $pks = array();

    /**
     * Busca, se houver, os atributos de cor e tamanho do produto
     * @param int[] $produtosId ID's dos produtos a serem consultados
     * @return \App\Model\AtributoCorTamanho[]
     */
    public function getAtributosCorTamanhoById(array $produtosId) {
        $this->setCorTamanhoEntity();
        if (empty($produtosId)) {
            return array();
        }

        $params = array('produtosId' => $produtosId);
        return $this->getAll('Atributo/QryGetAtributoCorTamanho', $params);
    }

    /**
     * Busca, se houver, os atributos de substituicao do produto
     * @param int[] $produtosId
     * @param $siteId
     * @return \App\Model\AtributoSubstituicao[]
     */
    public function getAtributosSubstituicaoById(array $produtosId, $siteId) {
        $this->setSubstituicaoEntity();
        $params = array('produtosId' => $produtosId, 'siteId' => $siteId);
        return $this->getAll('Atributo/QryGetAtributoSubstituicao', $params);
    }

    /**
     * Método responsável por setar a saída como atributo de cor e tamanho
     * Este método pode ser encadeado
     * @return self
     */
    protected function setCorTamanhoEntity() {
        $this->model = 'AtributoCorTamanho';
        return $this;
    }

    /**
     * Método responsável por setar a saída como atributo substituição
     * Este método pode ser encadeado
     * @return self
     */
    public function setSubstituicaoEntity() {
        $this->model = 'AtributoSubstituicao';
        return $this;
    }
}