<?php

namespace App\DAO;

/**
 * Class VariavelDAO
 *
 * Responsável por buscas relativas as Variaveis
 *
 * @package App\DAO
 * @author Cristiano Gomes <cristianogomes@softbox.com.br>
 */
class VariavelDAO extends BaseDAO {
    /**
     * Retorna um ID de produto fake para o ARC aceitar o seguro como
     *
     * @param $siteId
     * @return \stdClass
     */
    public function getProdutoSeguroRouboId($siteId) {
        $this->setMap(false);
        return $this->get("Variavel/QryGetProdutoSeguroRouboId", array('siteId' => $siteId));
    }
}
