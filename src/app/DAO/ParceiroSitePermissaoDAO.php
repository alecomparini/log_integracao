<?php
namespace App\DAO;

/**
 * Class ParceiroSitePermissaoDAO
 * @package App\DAO
 */
class ParceiroSitePermissaoDAO extends BaseDAO {
    
    protected $model = 'ParceiroSitePermissao';
    protected $entity = 'Parceiro_Site_Permissao';
    protected $pks = 'ParceiroSitePermissaoId';
    
    public function getPermissoesParceiroSiteIp($parceiroSiteId, $siteId, $ip) {
        $params = array(
            "parceiroSiteId" => $parceiroSiteId,
            "siteId" => $siteId,
            "ip" => $ip
        );

        return $this->getAll("ParceiroSitePermissao/QryGetPermissaoByParceiroSiteIp", $params);
    }

    public function getPermissoesByIp($ip) {
        $params = array(
            'ip' => $ip,
        );

        return $this->getAll("ParceiroSitePermissao/QryGetPermissaoByIp", $params);
    }

    public function getPermissoes($parceiroId, $siteBase, $tipoPermissao) {
        $params = array(
            'parceiroId' => $parceiroId,
            'siteBase' => $siteBase,
            'permissaoTipoId' => $tipoPermissao,
        );
        $this->setCache(true);
        $this->setCacheTTL(600);
        return $this->getAll("ParceiroSitePermissao/QryGetPermissoes", $params);
    }



}
