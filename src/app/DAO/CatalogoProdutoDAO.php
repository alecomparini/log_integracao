<?php

namespace App\DAO;

/**
 * Dao da classe CatalogoProduto
 *
 * @author Ítallo Costa <itallocosta@softbox.com.br>
 */
class CatalogoProdutoDAO extends BaseDAO {
    protected $model = 'CatalogoProduto';
    protected $entity = 'Catalogo_Produto';
    protected $pks = array('CatalogoId');
    
    /**
     * Lista produtos do catálogo com sua respectiva configuração de desconto
     * @param type $catalogoId
     * @param array $produtos
     * @param type $parceiroId
     * @param type $siteBase
     * @return null|array
     */
    public function getProdutoCatalogoComDesconto($catalogoId, array $produtos, $parceiroId, $siteBase) {
        $params = array(
            'catalogoId' => $catalogoId,
            'produtos'   => $produtos,
            'parceiroId' => $parceiroId,
            'siteId'     => $siteBase
        );
        $retorno = $this->getAll("CatalogoProduto/QryGetCatalogoProduto", $params);
        if (is_null($retorno)) {
            return array();
        } else {
            $novoArray = array();
            foreach ($retorno as $produto) {
                $novoArray[$produto->produtoId] = $produto;                
            }
            return $novoArray;
        }
    }
}