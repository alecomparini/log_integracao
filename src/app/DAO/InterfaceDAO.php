<?php
namespace App\DAO;

/**
 * Class InterfaceDAO
 * @package App\DAO
 */
class InterfaceDAO extends BaseDAO {
    
    protected $model = 'Interface';
    protected $entity = 'Interface';
    protected $pks = array('InterfaceId');
    
    public function getInterfacesEnviar($parceiroSiteId, $interfaceTipoId) {
        $params = array(
            "parceiroSiteId" => $parceiroSiteId,
            "interfaceTipoId" => $interfaceTipoId
        );
        $sql = "Interface/QryGetInterfacesEnviar";
        return $this->getAll($sql, $params);
    }
    
}
