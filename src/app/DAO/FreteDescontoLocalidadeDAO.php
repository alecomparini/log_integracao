<?php

namespace App\DAO;

/**
 * Description of FreteDescontoLocalidadeDAO
 *
 * @author suportesoftbox
 */
class FreteDescontoLocalidadeDAO extends BaseDAO {
    
    protected $model = 'FreteDescontoLocalidade';
    protected $entity = 'FreteDesconto_Localidade';
    protected $pks = array('FreteDescontoLocalidadeId');
    
    /**
     * 
     * @param integer $parceiroId
     * @param string $cep
     * @return array
     */
    public function getDescontoFrete($parceiroId, $cep) {
        $params = array(
            'parceiroId' => $parceiroId,
            'cep' => $cep
        );

        return $this->get("FreteDescontoLocalidade/QryFreteDescontoLocalidadeByParceiroAndCep", $params);
    }
}
