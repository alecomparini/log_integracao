<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 15-02-2016
 * Time: 15:50
 */

namespace App\DAO;


class NotificacaoParceiroDAO extends BaseDAO {
    protected $model = 'NotificacaoParceiro';
    protected $entity = 'NotificacaoParceiro';
    protected $pks = array('ParceiroId', 'ProdutoCodigo');

    /**
     * @param int $limiteTentativas
     * @param int $limitOffset
     * @return \App\Model\NotificacaoParceiro[]
     */
    public function getNotificacoes($limiteTentativas = 5, $limitOffset = 500) {
        $query = 'Parceiro/QryGetNotificacaoParceiro';
        $params = array(
            'limite' => $limiteTentativas,
            'limitOffset' => $limitOffset
        );
        $this->setCache(false);
        return $this->getAll($query, $params);
    }

    public function getNotificacoesParceiro($parceiroId, $limiteTentativas = 5, $limitOffset = 50) {
        $query = 'Parceiro/QryGetNotificacoesPorParceiro';
        $params = array(
            'limite' => $limiteTentativas,
            'limitOffset' => $limitOffset,
            'parceiroId' => $parceiroId,
        );
        $this->setCache(false);
        return $this->getAll($query, $params);
    }

    /**
     * Busca as notificações relacionadas a tracking
     *
     * @param int $limiteTentativas
     * @return array
     */
    public function getNotificacoesTracking($limiteTentativas = 5) {
        $query = 'Parceiro/QryNotificacaoParceiroTracking';
        $params = array(
            'limite' => $limiteTentativas
        );
        $this->setCache(false);
        return $this->getAll($query, $params);
    }

    public function deleteTracking($parceiroId, $pedidoParceiroId) {
        $query = 'Parceiro/deleteNotificacaoParceiroTracking';
        $params = array(
            'parceiroId' => $parceiroId,
            'pedidoParceiroId' => $pedidoParceiroId
        );

        $this->execute($query, $params);
    }

    public function getNotificacoesExpiradasParceiro($parceiroId, $limiteTentativas = 5, $limitOffset = 50, $dataCorte = null) {
        $query = 'Parceiro/QryGetNotificacoesExpiradasPorParceiro';
        $params = array(
            'limite' => $limiteTentativas,
            'limitOffset' => $limitOffset,
            'parceiroId' => $parceiroId,
            'dataCorte' => $dataCorte,
        );
        $this->setCache(false);
        return $this->getAll($query, $params);
    }
}
