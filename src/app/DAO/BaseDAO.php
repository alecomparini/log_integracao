<?php

namespace App\DAO;

use App\Core\DAO;
use App\Core\Memcache;
use App\Exceptions\DatabaseException;

/**
 * Class BaseDAO
 * @package App\DAO
 */
abstract class BaseDAO extends DAO {

    private $tiposAceitos = array('boolean', 'integer', 'double', 'string', 'NULL');

    public function getById($id) {
        if (!is_array($id)) {
            $id = array($id);
        }

        $whereA = array();
        $pks = is_array($this->pks) ? $this->pks : array($this->pks);
        foreach ($pks as $field => $pk) {
            $whereA[] = sprintf("%s = %s", $pk, $id[$field]);
        }
        $where = implode(" AND ", $whereA);
        $stmt = sprintf("SELECT * FROM " . DB_INTEGRACAO . ".%s WHERE %s ", $this->entity, $where);
        return parent::raw($stmt);
    }

    public function delete($id) {
        $where = $this->getWherePk($id);
        $sql = "DELETE FROM " . DB_INTEGRACAO . ".{$this->entity} WHERE {$where}";
        return $this->rawExec($sql);
    }

    public function update($id, $dados) {
        if (!is_object($dados) && !is_array($dados)) {
            throw new DatabaseException('Os dados a serem atualizados devem ser um array ou um objecto');
        }

        $where = $this->getWherePk($id);
        $data = array();
        foreach ((array)$dados as $key => $value) {
            if (in_array(gettype($value), $this->tiposAceitos)) {
                $data[$key] = $value;
            }
        }

        $updateSet = array();
        foreach ($data as $field => $value) {
            $updateSet[] = sprintf("%s = '%s'", $field, $value);
        }
        $updateSet = implode(', ', $updateSet);
        $sql = "UPDATE {$this->entity} SET {$updateSet} WHERE {$where}";
        return $this->rawExec($sql);
    }

    private function getWherePk($id) {
        if (count($this->pks) === 1) {
            $where = sprintf("%s = '%s'", $this->pks[0], $id);
        } else {
            foreach ($id as $k => $v) {
                $whereA[] = sprintf("%s = '%s'", $k, $v);
            }
            $where = implode(' AND ', $whereA);
        }
        return $where;
    }
}
