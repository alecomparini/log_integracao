<?php

namespace App\DAO;

/**
 * Description of LocalidadeDAO
 *
 * @author suportesoftbox
 */
class LocalidadeDAO extends BaseDAO {
    protected $model = 'Localidade';
    protected $entity = '';
    protected $pks = array('LocalidadeId');
    
    public function getLocalidadeBySigla(array $sigla) {
        $params = array('sigla' => $sigla);
        return $this->getAll("Localidade/QryGetLocalidadeBySigla", $params);
    }
    
    public function getLocalidadeByCep($cep) {
        $params = array('cep' => $cep);
        return $this->getAll("Localidade/QryGetLocalidadeByCep", $param);
    }
}
