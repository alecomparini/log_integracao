<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 15-02-2016
 * Time: 11:55
 */

namespace App\DAO;


class ParceiroConfiguracaoDAO extends BaseDAO {
    protected $model = 'ParceiroConfiguracao';
    protected $entity = 'Parceiro_Configuracao';
    protected $pks = array('ParceiroId', 'ConfiguracaoId');

    /**
     * @param $parceiroId
     * @param array $configuracoes
     * @return \App\Model\ParceiroConfiguracao[]
     */
    public function getConfiguracoes($parceiroId, array $configuracoes = array()) {
        $params = array(
            'parceiroId' => $parceiroId,
            'configuracoes' => $configuracoes
        );
        $query = 'Parceiro/QryGetParceiroConfiguracao';
        return $this->getAll($query, $params);
    }
}