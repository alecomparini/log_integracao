<?php
namespace App\DAO;

/**
 * Description of NotificacaoEstornoPontosDAO
 *
 * @author suportesoftbox
 */
class NotificacaoEstornoPontosDAO extends BaseDAO {
    protected $model = 'NotificacaoEstornoPontos';
    protected $entity = 'NotificacaoEstornoPonto';
    protected $pks = array('ParceiroId','ParceiroPedidoId','ClienteDocumento');
        
    /**
     * @param int $limiteTentativas
     * @param int $limitOffset
     * @return \App\Model\NotificacaoParceiro[]
     */
    public function getNotificacoesEstornoPontos($limiteTentativas = 5, $limitOffset = 500) {
        $query = 'Parceiro/QryGetNotificacaoEstornoPontos';
        $params = array(
            'limite' => $limiteTentativas,
            'limitOffset' => $limitOffset
        );
        $this->setCache(false);
        return $this->getAll($query, $params);
    }
    
    public function deleteEstornoPonto($parceiroId, $pedidoParceiroId, $clienteDocumento) {
        $query = "Parceiro/QryDeleteNotificacaoEstornoPontos";
        $params = array(
            'parceiroId' => $parceiroId,
            'pedidoParceiroId' => $pedidoParceiroId,
            'clienteDocumento' => $clienteDocumento
        );
        return $this->execute($query, $params);
    }
    
    public function setNotificacaoEstornoPontos($pedidoParceiroId, $parceiroId, $clienteDocumento, $pontos) {
        $params = array(
            'pedidoParceiroId' => $pedidoParceiroId,
            'parceiroId' => $parceiroId,
            'clienteDocumento' => $clienteDocumento,
            'pontos' => $pontos
        );
       
        return $this->insert('Notificacao/QryInsertNotificacaoEstornoPonto', $params);
    }
    
}
