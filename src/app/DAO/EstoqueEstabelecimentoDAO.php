<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 25-01-2016
 * Time: 15:10
 */

namespace App\DAO;


use App\Model\PedidoProduto;

class EstoqueEstabelecimentoDAO extends BaseDAO {

    protected $model = 'EstoqueEstabelecimento';
    protected $entity = 'Estoque_Estabelecimento';
    protected $pks = array('ProdutoId', 'EstabelecimentoId');

    /**
     * @param null $produtoId
     * @param null $estabelecimentoId
     * @return \App\Model\EstoqueEstabelecimento[]
     */
    public function getEstoqueEstabelecimento($produtoId = null, $estabelecimentoId = null) {
        $params = array(
            'produtoId' => $produtoId,
            'estabelecimentoId' => $estabelecimentoId
        );
        return $this->getAll('Estoque/QryGetEstoqueEstabelecimento', $params);
    }

    /**
     * @param \App\Model\PedidoProduto $pedidoProduto
     * @param int $estabelecimentoId
     * @return int
     */
    public function setReservaEstoqueProduto(PedidoProduto $pedidoProduto, $estabelecimentoId) {
        $params = array(
            'qtdReserva' => $pedidoProduto->quantidade,
            'produtoId' => $pedidoProduto->produtoId,
            'estabelecimentoId' => $estabelecimentoId
        );
        return $this->execute('Estoque/QrySetReservaEstoqueProduto', $params);
    }
}