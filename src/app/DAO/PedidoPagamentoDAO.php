<?php
namespace App\DAO;

use App\Model\Pedido;

class PedidoPagamentoDAO extends BaseDAO {

    protected $model = 'Pagamento';

    /**
     * @param \App\Model\Pedido $pedido
     *
     * @return \App\Model\Pagamento[]
     */
    public function buscarPagamentosPedido(Pedido $pedido) {
        $params = array('dadosImportacaoId' => $pedido->dadosImportacaoId);
        $pagamentos = $this->getAll('DadosImportacao/QryBuscarPagamentosPedido', $params);
        $pedido->pagamento = $pagamentos;
        return $pagamentos;
    }
}