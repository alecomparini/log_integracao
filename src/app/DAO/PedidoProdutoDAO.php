<?php
namespace App\DAO;

use App\Model\PedidoProduto;

class PedidoProdutoDAO extends BaseDAO {
    protected $model = 'PedidoProduto';

    /**
     * @param $dadosImportacaoId
     *
     * @return \App\Model\PedidoProduto[]
     */
    public function buscarPedidoProdutoPorImportacaoId($dadosImportacaoId) {
        $params = array('dadosImportacaoId' => $dadosImportacaoId);
        $pedidoProdutos = $this->getAll('DadosImportacao/QryBuscarPedidoProdutoPorImportacaoId', $params);

        /** @var \App\Model\PedidoProduto[] $pedidoProdutos */
        foreach ($pedidoProdutos as $pedidoProduto) {
            $pedidoProduto->seguro = $this->buscarSeguroPedidoProduto($pedidoProduto);
            $pedidoProduto->garantia = $this->buscarGarantiaPedidoProduto($pedidoProduto);
            $pedidoProduto->attachments = $this->buscarAttachmentsPedidoProduto($pedidoProduto);
        }

        return $pedidoProdutos;
    }

    private function buscarSeguroPedidoProduto(PedidoProduto $pedidoProduto) {
        // Backup the DAO model
        $oldModel = $this->model;
        $this->model = 'PedidoSeguro';

        $params = array(
            'dadosImportacaoId' => $pedidoProduto->dadosImportacaoId,
            'pedidoProdutoId' => $pedidoProduto->pedidoProdutoId,
        );
        $response = $this->get('DadosImportacao/QryBuscarSeguroPedidoProduto', $params);
        $pedidoProduto->seguro = $response;

        // Restore the old model
        $this->model = $oldModel;

        return $response;
    }

    private function buscarGarantiaPedidoProduto(PedidoProduto $pedidoProduto) {
        // Backup the DAO model
        $oldModel = $this->model;
        $this->model = 'PedidoGarantia';

        $params = array(
            'dadosImportacaoId' => $pedidoProduto->dadosImportacaoId,
            'pedidoProdutoId' => $pedidoProduto->pedidoProdutoId,
        );
        $response = $this->get('DadosImportacao/QryBuscarGarantiaPedidoProduto', $params);
        $pedidoProduto->garantia = $response;

        // Restore the old model
        $this->model = $oldModel;

        return $response;
    }

    private function buscarAttachmentsPedidoProduto(PedidoProduto $pedidoProduto) {
        // Backup the DAO model
        $oldModel = $this->model;
        $this->model = 'PedidoAttachment';

        $params = array(
            'dadosImportacaoId' => $pedidoProduto->dadosImportacaoId,
            'pedidoProdutoId' => $pedidoProduto->pedidoProdutoId,
        );
        $response = $this->getAll('DadosImportacao/QryBuscarAttachmentsPedidoProduto', $params);
        $pedidoProduto->attachments = $response;

        // Restore the old model
        $this->model = $oldModel;

        return $response;
    }
}