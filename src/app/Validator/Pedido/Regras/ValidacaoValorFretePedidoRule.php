<?php
namespace App\Validator\Pedido\Regras;

use App\Exceptions\BusinessException;
use App\Model\Pedido;
use App\Model\Response;

class ValidacaoValorFretePedidoRule implements IPedidoValidatorRule {
    /**
     * @param \App\Model\Pedido $pedido
     *
     * @return bool
     * @throws \App\Exceptions\BusinessException
     */
    public function validar(Pedido $pedido) {
        $valorTotalFrete = 0;
        foreach ($pedido->pedidoProduto as $pedidoProduto) {
            $valorTotalFrete += $pedidoProduto->valorFrete;
        }

        $valorTotalFreteComputado = round($valorTotalFrete * 100);
        $valorTotalFreteEnviado = round($pedido->valorTotalFrete * 100);

        if ($valorTotalFreteEnviado !== $valorTotalFreteComputado) {
            $msg = "Erro: Valor Total Frete difere do calculado por produto.'";
            $msg .= "Valor Frete Pedido: R$ {$pedido->valorTotalFrete} Valor Frete Calculado (Soma Frete Produtos): {$valorTotalFrete}";
            throw new BusinessException($msg, 25);
        }

        return true;
    }
}