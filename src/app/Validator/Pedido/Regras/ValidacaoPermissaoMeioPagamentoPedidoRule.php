<?php
namespace App\Validator\Pedido\Regras;

use App\BO\ParceiroSiteBO;
use App\BO\PedidoBO;
use App\DAO\ParceiroSiteDAO;
use App\Exceptions\BusinessException;
use App\Model\Pagamento;
use App\Model\Pedido;
use App\Model\Response;

class ValidacaoPermissaoMeioPagamentoPedidoRule implements IPedidoValidatorRule {
    /**
     * @param \App\Model\Pedido $pedido
     *
     * @return bool
     * @throws \App\Exceptions\BusinessException
     * @throws \App\Exceptions\ValidacaoException
     * @throws \Exception
     */
    public function validar(Pedido $pedido) {
        $parceiroSiteBO = new ParceiroSiteBO();
        $parceiroSite = $parceiroSiteBO->getParceiroSiteById($pedido->parceiroId);
        $pedidoBO = new PedidoBO();
        $pedidoBO->iterarMeioPagamento($pedido, $parceiroSite);
        return true;
    }
}