<?php
namespace App\Validator\Pedido\Regras;

use App\BO\DadosImportacaoBO;
use App\Exceptions\BusinessException;
use App\Model\Pedido;
use App\Model\Response;

class ValidacaoImportacaoDuplicadaPedidoRule implements IPedidoValidatorRule {
    public function validar(Pedido $pedido) {
        //Verifica se já não existe no dados importação
        $dadosImportacaoBO = new DadosImportacaoBO();
        if ($dadosImportacaoBO->isImportado($pedido->parceiroPedidoId, $pedido->parceiroId)) {
            throw new BusinessException("Pedido {$pedido->parceiroPedidoId} já foi importado.", Response::PEDIDO_PARCEIRO_JA_IMPORTADO);
        }

        return true;
    }
}