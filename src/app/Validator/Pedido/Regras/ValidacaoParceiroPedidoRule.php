<?php
namespace App\Validator\Pedido\Regras;

use App\BO\ParceiroSiteBO;
use App\Exceptions\BusinessException;
use App\Model\Pedido;

class ValidacaoParceiroPedidoRule implements IPedidoValidatorRule {
    /**
     * @param \App\Model\Pedido $pedido
     *
     * @return bool
     * @throws \App\Exceptions\BusinessException
     * @throws \App\Exceptions\ValidacaoException
     * @throws \Exception
     */
    public function validar(Pedido $pedido) {
        //Verifica se o parceiroId e SiteId informado batem
        $parceiroSiteBO = new ParceiroSiteBO();
        $parceiroSite = $parceiroSiteBO->getParceiroSite($pedido->parceiroId, $pedido->siteId);
        if (null === $parceiroSite) {
            throw new BusinessException('Erro: Dados de acesso do parceiro invalido - Verificar SiteId e ParceiroId se estao corretos', 1);
        }

        return true;
    }
}