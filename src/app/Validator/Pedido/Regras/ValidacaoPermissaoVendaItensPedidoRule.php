<?php
namespace App\Validator\Pedido\Regras;

use App\BO\ParceiroSitePermissaoBO;
use App\Exceptions\BusinessException;
use App\Model\Pedido;
use App\Model\Response;

class ValidacaoPermissaoVendaItensPedidoRule implements IPedidoValidatorRule {
    /**
     * @param \App\Model\Pedido $pedido
     *
     * @return bool
     * @throws \App\Exceptions\BusinessException
     * @throws \App\Exceptions\ValidacaoException
     * @throws \Exception
     */
    public function validar(Pedido $pedido) {
        $permissoesBO = new ParceiroSitePermissaoBO();

        foreach ($pedido->pedidoProduto as $pedidoProduto) {

            // Verifica se eu posso vender o produto
            if (!$permissoesBO->verificaPermissaoProduto($pedidoProduto->codigoProduto, $pedido->parceiroId)) {
                throw new BusinessException("Erro: Produto nao cadastrado ou nao esta liberado para venda pela integracao do sistema", Response::PRODUTO_SEM_PERMISSAO);
            }
        }

        return true;
    }
}