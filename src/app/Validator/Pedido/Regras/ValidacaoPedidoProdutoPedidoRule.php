<?php
namespace App\Validator\Pedido\Regras;

use App\BO\ParceiroConfiguracaoBO;
use App\Exceptions\BusinessException;
use App\Exceptions\ValidacaoException;
use App\Helper\Validacao;
use App\Model\Pedido;
use App\Model\Response;
use App\Model\Configuracao;

class ValidacaoPedidoProdutoPedidoRule implements IPedidoValidatorRule {
    /**
     * @param \App\Model\Pedido $pedido
     *
     * @return bool
     * @throws \App\Exceptions\BusinessException
     * @throws \App\Exceptions\ValidacaoException
     * @throws \Exception
     */
    public function validar(Pedido $pedido) {
        $sequencial = 1;

        /* Valida os pedidos enviados */
        $regras = array(
            'quantidade' => array('required', 'notempty', 'int'),
            'valorUnitario' => array('required', 'float'),
            'valorTotal' => array('required', 'float'),
        );

        $validacao = new Validacao($regras);
        $confBO = new ParceiroConfiguracaoBO();
        $qtdMaxAtt = (int)$confBO->getConfiguracoes((int)$pedido->parceiroId, Configuracao::LIMITE_MAXIMO_ATTACHMENTS, true)->valor;
        foreach ($pedido->pedidoProduto as $pedidoProduto) {
            if (!$validacao->executaValidacao($pedidoProduto)) {
                throw new ValidacaoException($validacao->msg, Response::ERRO_VALIDACAO);
            }

            if ($sequencial !== (int)$pedidoProduto->sequencial) {
                throw new BusinessException("Erro de Sequencial do item do pedido SKU ({$pedidoProduto->codigoProduto});", Response::SEQUENCIAL_ITEM_ERRADO);
            }

            if (!empty($pedidoProduto->attachments) && (count($pedidoProduto->attachments) > $qtdMaxAtt)) {
            	throw new BusinessException("Erro de Attachments do item do pedido SKU ({$pedidoProduto->codigoProduto}) a quantidade de attachments por item não pode ultrapassar ".$qtdMaxAtt." personalizações;", Response::QUANTIDADE_ATTACHMENT_INVALIDA);
            }

            $sequencial++;
        }

        return true;
    }
}