<?php
namespace App\Validator\Pedido\Regras;

use App\Exceptions\BusinessException;
use App\Model\Pedido;
use App\Model\Response;

class ValidacaoValorPagamentoPedidoRule implements IPedidoValidatorRule {
    /**
     * @param \App\Model\Pedido $pedido
     *
     * @return bool
     * @throws \App\Exceptions\BusinessException
     */
    public function validar(Pedido $pedido) {
        $valorTotalPago = 0;

        foreach ($pedido->pagamento as $pagamento) {
            $valorTotalPago += $pagamento->valorPago;
        }

        $valorPagoComputado = round($valorTotalPago * 100);
        $valorTotal = round($pedido->valorTotal * 100);

        if ($valorPagoComputado !== $valorTotal) {
            throw new BusinessException("Total valor pago não conferer com o valor do Pedido. Valor Informado: R$ {$valorTotalPago}. Valor do Pedido R$ {$pedido->valorTotal}", 13);
        }

        return true;
    }
}