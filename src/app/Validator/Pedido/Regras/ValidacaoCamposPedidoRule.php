<?php
namespace App\Validator\Pedido\Regras;

use App\Exceptions\ValidacaoException;
use App\Helper\Validacao;
use App\Model\Pedido;
use App\Model\Response;

class ValidacaoCamposPedidoRule implements IPedidoValidatorRule {
    /**
     * @param \App\Model\Pedido $pedido
     *
     * @return bool
     * @throws \App\Exceptions\ValidacaoException
     */
    public function validar(Pedido $pedido) {
        $regras = array(
            'parceiroId' => array('notempty', 'int'),
            'siteId' => array('notempty', 'int'),
            'parceiroIp' => array('notemptynorwhitespaces', 'str' => array('min' => '7', 'max' => '15')),
            'clienteEmail' => array('notemptynorwhitespaces', 'str' => array('min' => '5', 'max' => '60')),
            'clienteTipo' => array('str' => array('tipocliente' => true)),
            'clientePfNome' => array('pf', 'notemptynorwhitespaces', 'str' => array('min' => '2', 'max' => '30')),
            'clientePfSobrenome' => array('pf', 'notemptynorwhitespaces', 'str' => array('min' => '2', 'max' => '30')),
            'clientePfCPF' => array('pf', 'notemptynorwhitespaces', 'str' => array('max' => '11', 'min' => '11')),
            'clientePfDataNascimento' => array('pf', 'str' => array('max' => '10', 'min' => '10'), 'date'),
            'clientePfSexo' => array('pf', 'str' => array('sexo' => true)),
            'clientePjRazaoSocial' => array('pj', 'notempty', 'str' => array('min' => '5', 'max' => '100')),
            'clientePjNomeFantasia' => array('pj', 'notempty', 'str' => array('min' => '5', 'max' => '100')),
            'clientePjCNPJ' => array('pj', 'notempty', 'str' => array('max' => '18', 'min' => '11')),
            'clientePjIsento' => array('pj', 'bool'),
            'clientePjInscEstadual' => array('pj', 'inscEstadual', 'int' => array('max' => '14')),
            'clientePjTelefone' => array('pj', 'notnull', 'notempty', 'str' => array('max' => '14', 'min' => '10')),
            'clientePjRamoAtividade' => array('pj', 'int' => array('ramoAtividade' => true)),
            'endCobrancaTipo' => array('str' => array('tipoEndCobranca' => true)),
            'endCobrancaDestinatario' => array('notemptynorwhitespaces', 'str' => array('max' => '100')),
            'endCobrancaCep' => array('cep', 'str' => array('max' => '8', 'min' => '8')),
            'endCobrancaEndereco' => array('notemptynorwhitespaces', 'str' => array('min' => '3', 'max' => '100')),
            'endCobrancaNumero' => array('int'),
            'endCobrancaComplemento' => array('str' => array('max' => '46')),
            'endCobrancaBairro' => array('notemptynorwhitespaces', 'str' => array('min' => '2', 'max' => '50')),
            'endCobrancaCidade' => array('notemptynorwhitespaces', 'str' => array('min' => '2', 'max' => '50')),
            'endCobrancaEstado' => array('notemptynorwhitespaces', 'str' => array('max' => '2', 'min' => '2')),
            'endCobrancaTelefone1' => array('notemptynorwhitespaces', 'str' => array('max' => '13', 'min' => '9')),
            'endCobrancaTelefone2' => array('str' => array('max' => '13', 'min' => '9', 'acceptNull' => true)),
            'endCobrancaCelular' => array('str' => array('max' => '13', 'min' => '9', 'acceptNull' => true)),
            'endCobrancaReferencia' => array('tiposaceitos' => array('str', 'null')),
            'endEntregaTipo' => array('notemptynorwhitespaces', 'str' => array('tipoEndCobranca' => true)),
            'endEntregaDestinatario' => array('notemptynorwhitespaces', 'str' => array('min' => '3', 'max' => '100')),
            'endEntregaCep' => array('cep', 'str' => array('max' => '8', 'min' => '8')),
            'endEntregaEndereco' => array('notemptynorwhitespaces', 'str' => array('min' => '3', 'max' => '100')),
            'endEntregaNumero' => array('int'),
            'endEntregaComplemento' => array('str' => array('max' => '45')),
            'endEntregaBairro' => array('notemptynorwhitespaces', 'str' => array('min' => '2', 'max' => '50')),
            'endEntregaCidade' => array('notemptynorwhitespaces', 'str' => array('min' => '2', 'max' => '50')),
            'endEntregaEstado' => array('notemptynorwhitespaces', 'str' => array('max' => '2', 'min' => '2')),
            'endEntregaTelefone1' => array('notemptynorwhitespaces', 'str' => array('max' => '13', 'min' => '9')),
            'endEntregaTelefone2' => array('str' => array('max' => '13', 'min' => '9', 'acceptNull' => true)),
            'endEntregaCelular' => array('str' => array('max' => '13', 'min' => '9', 'acceptNull' => true)),
            'endEntregaReferencia' => array('tiposaceitos' => array('str', 'null')),
            'parceiroPedidoId' => array('notemptynorwhitespaces', 'str' => array('min' => '1')),
            'valorTotal' => array('notempty', 'float' => array('notEmptyFloat' => true)),
            'valorTotalFrete' => array('float' => array('notNegativeFloat' => true)),
            'valorTotalProdutos' => array('notempty', 'float' => array('notEmptyFloat' => true)),
            'dataEntrega' => array('tiposaceitos' => array('null', 'str' => array('max' => '10', 'min' => '10'))),
            'turnoEntrega' => array('int'),
            'valorEntrega' => array('required', 'float'),
            'vendedorToken' => array('tiposaceitos' => array('str' => array('VendedorToken' => true), 'null')),
            'prazoCd' => array('tiposaceitos' => array('int', 'null', 'empty')),
        );

        // Se isento, não verifica
        if (1 === (int)$pedido->clientePjIsento) {
            $pedido->clientePjInscEstadual = 0;
        }

        if (empty($pedido->dataEntrega)) {
            $pedido->dataEntrega = '';
            unset($regras['dataEntrega']);
        }

        if (empty($pedido->turnoEntrega)) {
            $pedido->turnoEntrega = '';
            unset($regras['turnoEntrega']);
        }

        if (empty($pedido->valorEntrega)) {
            $pedido->valorEntrega = '';
            unset($regras['valorEntrega']);
        }

        if (empty($pedido->clientePjIsento)) {
            $pedido->clientePjIsento = 0;
        }

        $cepEntrega = (int)$pedido->endEntregaCep;
        if (empty($cepEntrega)) {
            $pedido->endEntregaCep = "";
        }

        $cepCobranca = (int)$pedido->endCobrancaCep;
        if (empty($cepCobranca)) {
            $pedido->endCobrancaCep = "";
        }

        $validacao = new Validacao($regras);
        if (!$validacao->executaValidacao($pedido)) {
            throw new ValidacaoException($validacao->msg, Response::ERRO_VALIDACAO);
        }

        //Valida o documento (CPF ou CNPJ)
        $documento = $pedido->clienteTipo === 'pf' ? $pedido->clientePfCPF : $pedido->clientePjCNPJ;
        $tipoValidacao = $pedido->clienteTipo === 'pf' ? 'cpf' : 'cnpj';
        if (!Validacao::validar($documento, $tipoValidacao)) {
            throw new ValidacaoException(Validacao::$lastMsg, 2);
        }

        return true;
    }
}