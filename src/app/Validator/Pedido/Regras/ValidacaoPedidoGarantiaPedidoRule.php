<?php
namespace App\Validator\Pedido\Regras;

use App\DAO\GarantiaDAO;
use App\DAO\ProdutoDAO;
use App\Exceptions\BusinessException;
use App\Helper\Validacao;
use App\Model\Pedido;
use App\Model\PedidoGarantia;
use App\Model\Response;

class ValidacaoPedidoGarantiaPedidoRule implements IPedidoValidatorRule {
    /**
     * @param \App\Model\Pedido $pedido
     *
     * @return bool
     * @throws \App\Exceptions\BusinessException
     * @throws \App\Exceptions\ValidacaoException
     * @throws \Exception
     */
    public function validar(Pedido $pedido) {
        /* $Validacao de garantia */
        $regrasGarantia = array(
            'codigo' => array('str', 'required', 'notempty'),
            'prazo' => array('int', 'required'),
            'valorVenda' => array('float', 'required'),
            'quantidade' => array('int', 'required', 'notempty'),
        );
        $validadorGarantia = new Validacao($regrasGarantia);;

        foreach ($pedido->pedidoProduto as $pedidoProduto) {
            if (null !== $pedidoProduto->garantia) {
                $garantia = $pedidoProduto->garantia;

                if ($pedidoProduto->garantia instanceof PedidoGarantia) {
                    if (!$validadorGarantia->executaValidacao($pedidoProduto->garantia)) {
                        throw new BusinessException($validadorGarantia->msg, Response::ERRO_VALIDACAO);
                    }
                } else {
                    throw new BusinessException("Erro: campo Garantia não é uma instância de PedidoGarantia", 51);
                }

                $produtoDAO = new ProdutoDAO();
                $produtosId = $produtoDAO->getCodProdutos(array($pedidoProduto->codigoProduto));
                if (count($produtosId) === 0) {
                    throw new BusinessException("Código de produto inválido.", Response::ERRO_VALIDACAO);
                }
                $garantiaDAO = new GarantiaDAO();

                $conf = $garantiaDAO->confirmaGarantia($produtosId[0]->produtoId, $garantia->codigo);

                if (!$conf->confirmacao) {
                    throw new BusinessException("Erro: Garantia não disponível para o produto ({$pedidoProduto->codigoProduto})", 28);
                }
            }
        }

        return true;
    }
}