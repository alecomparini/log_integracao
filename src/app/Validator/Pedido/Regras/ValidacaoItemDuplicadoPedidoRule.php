<?php
namespace App\Validator\Pedido\Regras;

use App\Exceptions\BusinessException;
use App\Model\Pedido;
use App\Model\Response;

class ValidacaoItemDuplicadoPedidoRule implements IPedidoValidatorRule {
    /**
     * @param \App\Model\Pedido $pedido
     *
     * @return bool
     * @throws \App\Exceptions\BusinessException
     */
    public function validar(Pedido $pedido) {
        $codigosEnviados = array();

        foreach ($pedido->pedidoProduto as $pedidoProduto) {
            if (in_array($pedidoProduto->codigoProduto, $codigosEnviados, true)) {
                throw new BusinessException("Produto duplicado no corpo do pedido. Sequencial: {$pedidoProduto->sequencial} Código: {$pedidoProduto->codigoProduto}", Response::PRODUTO_DUPLICADO);
            }
            $codigosEnviados[] = $pedidoProduto->codigoProduto;
        }

        return true;
    }
}