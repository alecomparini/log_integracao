<?php
namespace App\Validator\Pedido\Regras;

use App\Core\Contracts\IFactory;
use App\Model\Response;
use InvalidArgumentException;

class PedidoValidatorRuleFactory implements IFactory {
    /**
     * @param string $type
     *
     * @return \App\Validator\Pedido\Regras\IPedidoValidatorRule
     * @throws InvalidArgumentException
     */
    public static function create($type) {
        $className = sprintf('\%s\Validacao%sPedidoRule', __NAMESPACE__, ucfirst($type));
        if (class_exists($className)) {
            return new $className();
        }
        throw new InvalidArgumentException("Regra de validação ({$type}) não encontrada.", Response::ERRO);
    }
}