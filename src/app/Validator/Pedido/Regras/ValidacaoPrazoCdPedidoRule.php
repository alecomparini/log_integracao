<?php
namespace App\Validator\Pedido\Regras;

use App\Exceptions\ValidacaoException;
use App\Helper\Validacao;
use App\Exceptions\BusinessException;
use App\BO\ParceiroConfiguracaoBO;
use App\Model\Configuracao;
use App\Model\Pedido;
use App\Model\Response;

class ValidacaoPrazoCdPedidoRule implements IPedidoValidatorRule {
    /**
     * @param \App\Model\Pedido $pedido
     *
     * @return bool
     * @throws \App\Exceptions\ValidacaoException
     */
    public function validar(Pedido $pedido) {

        //valida se o parceiro é obrigado a informar o prazo cd, caso não seja obrigada e valor seja null ou 0 não é necessário validar o campo
        $confBO = new ParceiroConfiguracaoBO();
        if ($confBO->getConfiguracoes((int)$pedido->parceiroId, Configuracao::PRAZO_CD_OBRIGATORIO_PARCEIRO, true)->valor && empty($pedido->prazoCd)) {
            throw new BusinessException("O campo prazoCd é obrigatório para o pedido ({$pedido->parceiroPedidoId})", Response::PRAZO_CD_OBRIGATORIO);
        } elseif (empty($pedido->prazoCd)) {
            return true;
        }

        if (($pedido->prazoCd <= 0) || ($pedido->prazoCd > 30)) {
            throw new BusinessException("O campo prazoCd ({$pedido->prazoCd}) deve ser maior que 0 e menor igual a 30 dias para o pedido [{$pedido->parceiroPedidoId}]", Response::PRAZO_CD_VALIDO);
        }

    }
}