<?php
namespace App\Validator\Pedido\Regras;

use App\Model\Pedido;

interface IPedidoValidatorRule {
    public function validar(Pedido $pedido);
}