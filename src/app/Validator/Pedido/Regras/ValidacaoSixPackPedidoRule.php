<?php
namespace App\Validator\Pedido\Regras;

use App\BO\ParceiroConfiguracaoBO;
use App\BO\ParceiroSiteBO;
use App\Exceptions\BusinessException;
use App\Exceptions\ValidacaoAttachmentsException;
use App\Model\Configuracao;
use App\Model\Pedido;
use App\Model\Response;

class ValidacaoSixPackPedidoRule implements IPedidoValidatorRule {
    /**
     * @param \App\Model\Pedido $pedido
     *
     * @return bool
     * @throws \App\Exceptions\ValidacaoAttachmentsException
     * @throws \App\Exceptions\BusinessException
     * @throws \App\Exceptions\ValidacaoException
     * @throws \Exception
     */
    public function validar(Pedido $pedido) {
        $parceiroConfigBO = new ParceiroConfiguracaoBO();
        $aceitaAttachmentsConfig = $parceiroConfigBO->getConfiguracoes($pedido->parceiroId, Configuracao::ACEITA_ATTACHMENTS, true);

        if ($aceitaAttachmentsConfig->valor) {
            $codigoSixPackConfig = $parceiroConfigBO->getConfiguracoes($pedido->parceiroId, Configuracao::ATTACHMENT_SIX_PACK, true);
            $codigoSixPack = $codigoSixPackConfig->valor;

            foreach ($pedido->pedidoProduto as $pedidoProduto) {
                if (null !== $pedidoProduto->attachments && is_array($pedidoProduto->attachments) && count($pedidoProduto->attachments) > 0) {
                    if ($pedidoProduto->codigoProduto === $codigoSixPack) {
                        $totalAttachments = $pedidoProduto->quantidade * 6;

                        if (count($pedidoProduto->attachments) !== $totalAttachments) {
                            // Portado do setPedido. FIX: Exception sem mensagem!
                            throw new ValidacaoAttachmentsException('Quantidade de attachments não corresponde ao total de produtos (SIXPACK)', Response::ATT_CONTAGEM_ATT_SIXPACK_ERRADA);
                        }
                    } else {
                        if (count($pedidoProduto->attachments) !== (int)$pedidoProduto->quantidade) {
                            throw new ValidacaoAttachmentsException('Quantidade de attachments não corresponde ao total de produtos', Response::ATT_QUANTIDADE_NAO_CONFERE);
                        }
                    }

                    foreach ($pedidoProduto->attachments as $attachment) {
                        if (null === $attachment->type || !is_numeric($attachment->type) || null === $attachment->label) {
                            throw new ValidacaoAttachmentsException('Objeto attachment incompleto', Response::ATT_OBJETO_INCOMPLETO);
                        }

                        if (null === $attachment->type || null === $attachment->label) {
                            throw new ValidacaoAttachmentsException('Attachment: varáveis vazias', Response::ATT_VARIAVEIS_VAZIAS);
                        }
                    }
                }
            }
        } else {
            //se não houver configuração para attachments para o parceiro, garante que o atributo não passará daqui
            foreach ($pedido->pedidoProduto as $produto) {
                if (null !== $produto->attachments) {
                    unset($produto->attachments);
                }
            }
        }

        return true;
    }
}