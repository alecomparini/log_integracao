<?php
namespace App\Validator\Pedido\Regras;

use App\DAO\ProdutoDAO;
use App\Exceptions\BusinessException;
use App\Model\Pedido;
use App\Model\Response;

class ValidacaoEstoquePedidoRule implements IPedidoValidatorRule {
    /**
     * @param \App\Model\Pedido $pedido
     *
     * @return bool
     * @throws \App\Exceptions\BusinessException
     * @throws \App\Exceptions\ValidacaoException
     * @throws \Exception
     */
    public function validar(Pedido $pedido) {
        //Busca produto ID e Código
        $produtoDAO = new ProdutoDAO();

        foreach ($pedido->pedidoProduto as $pedidoProduto) {
            $produtoIdEstoque = $produtoDAO->getProdutoEstoque($pedidoProduto->codigoProduto);

            if ((int)$produtoIdEstoque->Disponivel < (int)$pedidoProduto->quantidade) {
                $nomeProduto = $produtoDAO->buscarNomeProdutoSitePorCodigo((int)$pedidoProduto->codigoProduto);
                throw new BusinessException("Erro: Estoque indisponivel - Nao existe estoque disponivel do item "
                    .$pedidoProduto->codigoProduto." - ".$nomeProduto[0]->Nome." para venda no ARC", Response::ERRO_VALIDACAO);

            }
        }

        return true;
    }
}