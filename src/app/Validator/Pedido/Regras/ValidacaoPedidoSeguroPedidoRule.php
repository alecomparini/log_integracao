<?php
namespace App\Validator\Pedido\Regras;

use App\BO\ParceiroSiteBO;
use App\DAO\ProdutoDAO;
use App\DAO\SeguroDAO;
use App\Exceptions\BusinessException;
use App\Helper\Validacao;
use App\Model\Pedido;
use App\Model\PedidoSeguro;
use App\Model\Response;

class ValidacaoPedidoSeguroPedidoRule implements IPedidoValidatorRule {
    /**
     * @param \App\Model\Pedido $pedido
     *
     * @return bool
     * @throws \App\Exceptions\BusinessException
     * @throws \App\Exceptions\ValidacaoException
     * @throws \Exception
     */
    public function validar(Pedido $pedido) {
        /* Validaçao de seguro */
        $regrasSeguro = array(
            'seguroTabelaId' => array('int', 'required', 'notempty'),
            'valorVenda' => array('float', 'required'),
            'familia' => array('int', 'required'),
            'quantidade' => array('int', 'required', 'notempty'),
        );
        $validadorSeguro = new Validacao($regrasSeguro);

        foreach ($pedido->pedidoProduto as $pedidoProduto) {
            if (null !== $pedidoProduto->seguro) {
                $seguro = $pedidoProduto->seguro;
                if ($pedidoProduto->seguro instanceof PedidoSeguro) {
                    if (!$validadorSeguro->executaValidacao($pedidoProduto->seguro)) {
                        throw new BusinessException($validadorSeguro->msg, Response::ERRO_VALIDACAO);
                    }
                } else {
                    throw new BusinessException("Erro: campo Seguro não é uma instância de PedidoSeguro", 50);
                }

                $parceiroSiteBO = new ParceiroSiteBO();
                $parceiroSite = $parceiroSiteBO->getParceiroSiteById($pedido->parceiroId);

                $produtoDAO = new ProdutoDAO();
                $produtosId = $produtoDAO->getCodProdutos(array($pedidoProduto->codigoProduto));
                if (count($produtosId) === 0) {
                    throw new BusinessException("Código de produto inválido.", Response::ERRO_VALIDACAO);
                }

                $produtoInfo = $produtoDAO->getProdutoInfo($produtosId[0]->produtoId, $parceiroSite->siteBase);

                $seguroDao = new SeguroDAO();
                $categorias = explode(',', $produtoInfo->Categorias);
                $conf = $seguroDao->getSeguroElegivel($produtoInfo->PrecoPor, $categorias);

                if ((int)$conf->Familia !== (int)$seguro->familia) {
                    throw new BusinessException("Erro: Família inválida para o seguro do produto ({$pedidoProduto->codigoProduto})", 29);
                }
                if ((int)$conf->SeguroTabelaId !== (int)$seguro->seguroTabelaId) {
                    throw new BusinessException("Erro: Seguro não disponível para o produto ({$pedidoProduto->codigoProduto})", 29);
                }
            }
        }

        return true;
    }
}