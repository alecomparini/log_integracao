<?php
namespace App\Validator\Pedido\Regras;

use App\BO\ClienteBO;
use App\Exceptions\BusinessException;
use App\Model\Pedido;

class ValidacaoEmailEmUsoPedidoRule implements IPedidoValidatorRule {
    /**
     * @param \App\Model\Pedido $pedido
     *
     * @return bool
     * @throws \App\Exceptions\BusinessException
     * @throws \App\Exceptions\ValidacaoException
     * @throws \Exception
     */
    public function validar(Pedido $pedido) {
        $documento = $pedido->clienteTipo === 'pf' ? $pedido->clientePfCPF : $pedido->clientePjCNPJ;
        //Verifica se não está em uso o e-mail
        $clienteBO = new ClienteBO();
        if ($clienteBO->emailEmUso($pedido->clienteEmail, $documento, $pedido->clienteTipo, $pedido->siteId)) {
            throw new BusinessException("Erro: Email enviado no pedido esta cadastrado para outro CNPJ/CPF", 22);
        }
        return true;
    }
}