<?php
namespace App\Validator\Pedido\Regras;

use App\Exceptions\BusinessException;
use App\Model\Pedido;
use App\Model\Response;

class ValidacaoValorProdutosPedidoRule implements IPedidoValidatorRule {
    /**
     * @param \App\Model\Pedido $pedido
     *
     * @return bool
     * @throws \App\Exceptions\BusinessException
     */
    public function validar(Pedido $pedido) {
        $totalProdutos = 0;

        foreach ($pedido->pedidoProduto as $pedidoProduto) {
            $valorProduto = $pedidoProduto->valorUnitario * $pedidoProduto->quantidade;

            // Adiciono o valor do seguro
            if (null !== $pedidoProduto->seguro) {
                $valorProduto += $pedidoProduto->seguro->valorVenda * $pedidoProduto->seguro->quantidade;
            }

            // Adiciono o valor da garantia
            if (null !== $pedidoProduto->garantia) {
                $valorProduto += $pedidoProduto->garantia->valorVenda * $pedidoProduto->garantia->quantidade;
            }

            // Verifico se o valor computador é igual ao enviado.
            $valorProdutoComputado = round($valorProduto * 100);
            $valorProdutoEnviado = round($pedidoProduto->valorTotal * 100);
            if ($valorProdutoEnviado !== $valorProdutoComputado) {
                throw new BusinessException("Erro: Valor total do produto inconsistente - Valor total do produto diferente da quantidade X o valor unitario valorTotal computador: {$valorProduto} valorTotal enviado: {$pedidoProduto->valorTotal}. SKU: ({$pedidoProduto->codigoProduto})", 7);
            }

            $totalProdutos += $valorProduto;
        }

        $totalProdutosComputado = round($totalProdutos * 100);
        $totalProdutosEnviado = round($pedido->valorTotalProdutos * 100);

        if ($totalProdutosComputado !== $totalProdutosEnviado) {
            throw new BusinessException("Erro: Valor da soma de todos os produtos ({$totalProdutos}) diverge do valor total de produtos enviado [{$pedido->valorTotalProdutos}]. SKU: [{$pedidoProduto->codigoProduto}]", 7);
        }

        return true;
    }
}