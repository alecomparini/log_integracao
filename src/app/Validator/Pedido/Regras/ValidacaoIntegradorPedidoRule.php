<?php
namespace App\Validator\Pedido\Regras;

use App\BO\ParceiroSiteBO;
use App\Exceptions\BusinessException;
use App\Model\Pedido;

class ValidacaoIntegradorPedidoRule implements IPedidoValidatorRule {
    /**
     * @param \App\Model\Pedido $pedido
     *
     * @return bool
     * @throws \App\Exceptions\BusinessException
     * @throws \App\Exceptions\ValidacaoException
     * @throws \Exception
     */
    public function validar(Pedido $pedido) {
        //Verifica se o parceiroId e SiteId informado batem
        $parceiroSiteBO = new ParceiroSiteBO();
        $parceiroSite = $parceiroSiteBO->getParceiroSite($pedido->parceiroId, $pedido->siteId);
        if (null === $parceiroSite) {
            throw new BusinessException('Erro: Dados de acesso do parceiro inválido - Verificar SiteId e ParceiroId se estao corretos', 1);
        }

        if ((!empty($pedido->integradorId) || (is_numeric($pedido->integradorId) && 0 === (int)$pedido->integradorId)) && !in_array($pedido->integradorId, $parceiroSite->integradores, false)) {
            throw new BusinessException('Erro: Dados de acesso do parceiro inválido - Verificar se o Integrador está vinculado ao parceiro corretamente.', 1);
        }

        return true;
    }
}