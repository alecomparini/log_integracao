<?php
namespace App\Validator\Pedido\Regras;

use App\Exceptions\BusinessException;
use App\Exceptions\ValidacaoException;
use App\Helper\Validacao;
use App\Model\Pagamento;
use App\Model\Pedido;
use App\Model\Response;

class ValidacaoPedidoPagamentoPedidoRule implements IPedidoValidatorRule {
    /**
     * @param \App\Model\Pedido $pedido
     *
     * @return bool
     * @throws \App\Exceptions\BusinessException
     * @throws \App\Exceptions\ValidacaoException
     * @throws \Exception
     */
    public function validar(Pedido $pedido) {
        $sequencial = 1;
        /* Valida os pagamentos enviados */
        $regraValidacaoPagamento = array(
            'meioPagamentoId' => array('required', 'notempty', 'int'),
            'bandeiraId' => array('required', 'notempty', 'int'),
            'parcelas' => array('required', 'notempty', 'int'),
            'cartaoNumero' => array('required', 'notempty', 'str'),
            'cartaoValidade' => array('required', 'notempty', 'str'),
            'cartaoCodSeguranca' => array('required', 'notempty', 'str'),
            'cartaoNomePortador' => array('required', 'notempty', 'str')
        );

        foreach ($pedido->pagamento as $pagamento) {
            switch ((int)$pagamento->meioPagamentoId) {
                case Pagamento::BOLETO_BANCARIO:
                case Pagamento::BOLETO_TERCEIROS:
                case Pagamento::BOLETO_PAYU:
                    unset($regraValidacaoPagamento['bandeiraId'], $regraValidacaoPagamento['cartaoNumero'], $regraValidacaoPagamento['cartaoNomePortador'], $regraValidacaoPagamento['cartaoCodSeguranca'], $regraValidacaoPagamento['cartaoValidade']);
                    $regraValidacaoPagamento['dataVencimentoBoleto'] = array('date', 'tiposaceitos' => array('str' => array('max' => '10', 'min' => '10')));
                    break;
                case Pagamento::BOLETO_POS_PAGO:
                case Pagamento::PONTOS:
                    unset($regraValidacaoPagamento['bandeiraId'], $regraValidacaoPagamento['cartaoNumero'], $regraValidacaoPagamento['cartaoNomePortador'], $regraValidacaoPagamento['cartaoCodSeguranca'], $regraValidacaoPagamento['cartaoValidade']);
                    break;
            }
            $validadorPagamento = new Validacao($regraValidacaoPagamento);
            if (!$validadorPagamento->executaValidacao($pagamento)) {
                throw new ValidacaoException($validadorPagamento->msg, Response::ERRO_VALIDACAO);
            }

            if ($sequencial !== (int)$pagamento->sequencial) {
                throw new BusinessException("Sequencial do Pagamento invalido - Verificar o valor de sequencial enviado", Response::ERRO_VALIDACAO);
            }

            $sequencial++;
        }

        return true;
    }
}