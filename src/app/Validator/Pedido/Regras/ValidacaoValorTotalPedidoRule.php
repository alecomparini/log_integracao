<?php
namespace App\Validator\Pedido\Regras;

use App\Exceptions\BusinessException;
use App\Model\Pedido;
use App\Model\Response;

class ValidacaoValorTotalPedidoRule implements IPedidoValidatorRule {
    /**
     * @param \App\Model\Pedido $pedido
     *
     * @return bool
     * @throws \App\Exceptions\BusinessException
     */
    public function validar(Pedido $pedido) {
        $valorDesconto = 0;

        foreach ($pedido->pedidoProduto as $pedidoProduto) {
            $valorDesconto += $pedidoProduto->valorDesconto * $pedidoProduto->quantidade;
        }

        $total = $pedido->valorTotalProdutos + $pedido->valorTotalFrete + $pedido->valorJuros - $valorDesconto;

        // Arredonda os valores para evitar problema de precisão
        $totalComputadoArredondado = round($total * 100);
        $totalEnviadoArredondado = round($pedido->valorTotal * 100);

        if ($totalEnviadoArredondado !== $totalComputadoArredondado) {
            $msg = "Erro: Valor Total do Pedido difere do Valor Total Calculado.";
            $msg .= " Valor Total Pedido: {$pedido->valorTotal} Valor Total Calculado: {$total}.";
            throw new BusinessException($msg, Response::ERRO_VALOR_PEDIDO_DIFERENTE);
        }

        return true;
    }
}