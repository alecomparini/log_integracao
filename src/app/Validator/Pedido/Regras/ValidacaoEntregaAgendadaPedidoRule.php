<?php
namespace App\Validator\Pedido\Regras;

use App\Exceptions\BusinessException;
use App\Helper\Utils;
use App\Model\Pedido;
use App\Model\Response;

class ValidacaoEntregaAgendadaPedidoRule implements IPedidoValidatorRule {
    /**
     * @param \App\Model\Pedido $pedido
     *
     * @return bool
     * @throws \App\Exceptions\BusinessException
     */
    public function validar(Pedido $pedido) {
        // De acordo com a regra no setPedido, prazo de entrega combinado é o do último produto
        $ultimoProduto = end($pedido->pedidoProduto);
        $prazoEntregaCombinado = $ultimoProduto->prazoEntregaCombinado;


        /* Valida entrega agendada */
        if (null !== $pedido->dataEntrega && "" !== $pedido->dataEntrega && $pedido->dataEntrega !== '0000-00-00') {
            $prazoConvencional = $prazoEntregaCombinado;
            $gorduraEntregaAgendada = 20;
            $addDiasUteis = 7;

            $prazoTotalInicial = $gorduraEntregaAgendada;
            $prazoTotalFinal = $prazoConvencional + $gorduraEntregaAgendada;
            $dataInicial = date('Y-m-d', strtotime("+" . $prazoTotalInicial . "days"));
            $dataFinal = date('Y-m-d', strtotime("+" . $prazoTotalFinal . "days"));
            $dataFinalDiasUteis = Utils::adicionarDiasUteis($dataFinal, $addDiasUteis);
            $dataAtual = date('Y-m-d');

            $dAtual = strtotime($dataAtual);
            $dEntrega = strtotime($pedido->dataEntrega);
            $dInicial = strtotime($dataInicial);
            $dFinal = strtotime($dataFinalDiasUteis);

            $sub1 = ($dEntrega - $dAtual) / 86400;
            $sub2 = ($dInicial - $dAtual) / 86400;
            $sub3 = ($dFinal - $dAtual) / 86400;

            if ($sub1 < $sub2 || $sub1 > $sub3) {
                throw new BusinessException("Erro: A data da Entrega Agendada esta fora do prazo aceito. Agendamento deve ser feito entre {$dataInicial} e {$dataFinalDiasUteis}", Response::DATA_ENTREGA_AGENDADA_ERRADA);
            }

            if ($pedido->turnoEntrega < 1 || $pedido->turnoEntrega > 3) {
                throw new BusinessException("Erro: O Turno da Entrega esta invalido", 27);
            }

            return true;
        }
    }
    
    public function dataValida(Pedido $pedido) {
    	
    	if ($pedido->dataEntrega === '0000-00-00' || empty($pedido->dataEntrega)) {
    		return true;
    	}
    	
    	$dataAtual = date('Y-m-d');
    	
    	//Valida se a $pedido->dataEntrega é menor ou igual ao dia de hoje
    	if (strtotime($pedido->dataEntrega) <= strtotime($dataAtual)) {
    		throw new BusinessException("Erro: A data da Entrega Agendada é menor ou igual a data atual.");
    	}
    	                                                  
    }
}