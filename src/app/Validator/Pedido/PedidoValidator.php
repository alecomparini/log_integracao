<?php
namespace App\Validator\Pedido;

use App\Model\Pedido;
use App\Model\Response;
use App\Validator\Pedido\Regras\IPedidoValidatorRule;
use InvalidArgumentException;

/**
 * Class responsável por validar um pedido de acordo com as regras injetadas.
 *
 * @package App\Validator\Pedido
 */
class PedidoValidator {

    /** @var \App\Validator\Pedido\Regras\IPedidoValidatorRule[] */
    private $regras = array();

    public function __construct(array $regras = array()) {
        $this->validarListaRegras($regras);
        $this->regras = $regras;
    }

    public function adicionarRegra(IPedidoValidatorRule $regra) {
        $this->regras[] = $regra;
    }

    public function adicionarListaRegras(array $regras) {
        $this->validarListaRegras($regras);
        foreach ($regras as $regra) {
            $this->adicionarRegra($regra);
        }
    }

    public function executarValidacao(Pedido $pedido) {
        foreach ($this->regras as $regra) {
            $regra->validar($pedido);
        }
        return true;
    }

    private function validarListaRegras(array $regras) {
        foreach ($regras as $regra) {
            if (!$regra instanceof IPedidoValidatorRule) {
                throw new InvalidArgumentException('O parâmetro informado não é uma lista de IPedidoValidatorRule', Response::ERRO_VALIDACAO);
            }
        }
    }
}