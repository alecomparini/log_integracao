<?php
namespace App\Model;

/**
 * Class Parceiro
 * @package App\Model
 */
class Parceiro extends Model {
    /** @var int */
    public $parceiroId;
    
    /** @var string */
    public $nome;
    
    /** @var int */
    public $status;

    /** @var int */
    public $tipoParceiro;
    
    /** @var string */
    public $tokenAutorizacao;
    
    public function columnMap() {
        return array(
            "ParceiroId" => "parceiroId",
            "Nome" => "nome",
            "Status" => "status",
            "TipoParceiro" => "tipoParceiro",
            "TokenAutorizacao" => 'tokenAutorizacao'
        );
    }
}