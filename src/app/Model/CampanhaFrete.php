<?php

namespace App\Model;

/**
 * Model Campanha_Frete
 *
 * @author suportesoftbox
 */
class CampanhaFrete extends Model {
    
    /** @var int */
    public $campanhaId;
    
    /** @var string */
    public $localidade;
    
    /** @var fload */
    public $valorFrete;
    
    public function columnMap() {
        return array(
            'CatalogoId' => 'catalogoId',
            'Nome' => 'nome',
            'Status' => 'status',
            'ValorFrete' => 'valorFrete'
        );
    }
}
