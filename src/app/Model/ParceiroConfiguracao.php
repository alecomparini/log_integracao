<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 15-02-2016
 * Time: 11:43
 */

namespace App\Model;

class ParceiroConfiguracao {

    /** @var int */
    public $parceiroId;

    /** @var int */
    public $configuracaoId;

    /** @var string */
    public $chave;

    /** @var mixed|string */
    public $valor;

    /** @var string */
    public $tipo;

    public function columnMap() {
        return array(
            'ParceiroId' => 'parceiroId',
            'ConfiguracaoId' => 'configuracaoId',
            'Chave' => 'chave',
            'Valor' => 'valor',
            'Tipo' => 'tipo',
        );
    }
}