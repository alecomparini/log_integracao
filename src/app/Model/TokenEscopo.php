<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29-02-2016
 * Time: 15:50
 */

namespace App\Model;


class TokenEscopo extends Token {
    /** @var int */
    public $tokenEscopoId;

    /** @var int */
    public $tokenId;

    /** @var string */
    public $escopo;

    public function columnMap() {
        $parent = parent::columnMap();
        return array_merge(array(
            'TokenEscopoId' => 'tokenEscopoId',
            'TokenId' => 'tokenId',
            'Escopo' => 'escopo',
        ), $parent);
    }
}