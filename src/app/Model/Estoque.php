<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 25-01-2016
 * Time: 14:59
 */

namespace App\Model;


class Estoque extends Model {
    /** @var int */
    public $produtoId;

    /** @var int */
    public $total;

    /** @var int */
    public $reservado;

    /** @var int */
    public $disponivel;

    /** @var int */
    public $preVenda;

    /** @var int */
    public $virtual;

    /** @var string */
    public $dataUltimaImportacao;

    /** @var int */
    public $prazoDisponibilidade;

    /** @var int */
    public $tipo;

    /** @var int */
    public $fisico;

    /** @var int */
    public $jit;

    /**
     * Mapeamento
     */
    public function columnMap() {
        return array (
            'ProdutoId' => 'produtoId',
            'Total' => 'total',
            'Reservado' => 'reservado',
            'Disponivel' => 'disponivel',
            'PreVenda' => 'preVenda',
            'Virtual' => 'virtual',
            'DataUltimaImportacao' => 'dataUltimaImportacao',
            'PrazoDisponibilidade' => 'prazoDisponibilidade',
            'Tipo' => 'tipo',
            'Fisico' => 'fisico',
            'Jit' => 'jit',
        );
    }

}