<?php
namespace App\Model;

/**
 * Class ProdutoSite
 * @package App\Model
 */
class ProdutoSite extends Produto {
    /** @var int */
    public $produtoId;

    /** @var int */
    public $siteId;

    /** @var float */
    public $precoDe;

    /** @var float */
    public $precoPor;

    /** @var int */
    public $parcelamentoMaximo;

    /** @var int */
    public $status;

    /** @var string */
    public $dataAlteracao;

    /** @var int */
    public $usuarioAlteracao;

    /** @var float */
    public $descontoAVista;

    /** @var int */
    public $urlEspecialId;

    /** @var int */
    public $disponivel;

    /** @var int */
    public $atualizaDados;

    /** @var int */
    public $excecaoLimpeza;

    public function columnMap() {
        return array(
            "ProdutoId"                 => "produtoId",
            "SiteId"                    => "siteId",
            "PrecoDe"                   => "precoDe",
            "PrecoPor"                  => "precoPor",
            "ParcelamentoMaximo"        => "parcelamentoMaximo",
            "Status"                    => "status",
            "DataAlteracao"             => "dataAlteracao",
            "UsuarioAlteracao"          => "usuarioAlteracao",
            "DescontoAVista"            => "DescontoAVista",
            "UrlEspecialId"             => "urlEspecialId",
            "Disponivel"                => "disponivel",
            "AtualizaDados"             => "atualizaDados",
            "ExcecaoLimpeza"            => "excecaoLimpeza",
        );
    }
}
