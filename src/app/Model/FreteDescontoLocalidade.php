<?php

namespace App\Model;

/**
 * Description of FreteDescontoLocalidade
 *
 * @author suportesoftbox
 */
class FreteDescontoLocalidade extends FreteDesconto {
    
    public $freteDescontoLocalidadeId;
    
    public $freteDescontoId;
    
    public $localidade;
    
    public $valorFrete;
    
    public function columnMap() {
        return array_merge(
                parent::columnMap(),
                array(
                    'FreteDescontoLocalidadeId' => 'freteDescontoLocalidadeId',
                    'FreteDescontoId' => 'freteDescontoId',
                    'Localidade' => 'localidade',
                    'ValorFrete' => 'valorFrete'
                )
            );
    }
}
