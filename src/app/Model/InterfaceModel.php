<?php

namespace App\Model;

/**
 * Class InterfaceModel
 * @package App\Model
 * @author Cristiano Gomes <cristianogomes@softabox.com.br>
 */
class InterfaceModel {

    public $interfaceId;
    public $dados;
    public $dataCriacao; //datetime
    public $dataAprovacao; //datetime
    public $parceiroSiteId;
    public $interfaceTipoId;
    public $enviada;

    /**
     * Mapaeia nomes de colunas do DB para atribytis da classe
     * @return array
     */
    public function columnMap() {
        return array(
            "InterfaceId" => "interfaceId",
            "Dados" => "dados",
            "DataCriacao" => "dataCriacao",
            "DataAprovacao" => "dataAprovacao",
            "ParceiroSiteId" => "parceiroSiteId",
            "InterfaceTipoId" => "interfaceTipoId",
            "Enviada" => "enviada"
        );
    }
}
