<?php
namespace App\Model;

/**
 * Class Item
 * @package App\Model
 */
class Item {
    /** @var string */
    public $Codigo;

    /** @var int */
    public $Quantidade;
}