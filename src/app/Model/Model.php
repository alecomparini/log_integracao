<?php

namespace App\Model;

/**
 * Class Model
 * @package App\Model
 */
abstract class Model {
    protected $hidden = array();

    /**
     * Retorna objeto para serialização removedo campos hidden, se existirem.
     * @return $this|object
     */
    public function jsonSerialize() {
        if (property_exists($this, 'hidden')) {
            $clone = get_object_vars($this);
            foreach ($this->hidden as $campo) {
                unset($clone[$campo]);
            }
            unset($clone['hidden']);
            return (object)$clone;
        }
        return $this;
    }

    /**
     * Tira a propriedade hidden
     * @param $field
     * @return $this
     */
    public function setVisible($field) {
        if (property_exists($this, 'hidden') && in_array($field, $this->hidden) &&
            ($key = array_search($field, $this->hidden))) {
            unset($this->hidden[$key]);
        }
        return $this;
    }

    /**
     * Adiciona a propriedade hidden
     * @param $field
     * @return $this
     */
    public function setHidden($field) {
        if (property_exists($this, 'hidden') && !in_array($field, $this->hidden)) {
            $this->hidden[] = $field;
        }
        return $this;
    }
}