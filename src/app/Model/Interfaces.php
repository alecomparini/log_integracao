<?php

namespace App\Model;

/**
 * Class Interfaces
 * @package App\Model
 */
class Interfaces {
    /** @var int */
    public $ParceiroId;

    /** @var int */
    public $SiteId;

    /** @var int */
    public $Interface;
    
    /** @var string */
    public $Dados;
    
    /** @var int */
    public $InterfaceId;
}