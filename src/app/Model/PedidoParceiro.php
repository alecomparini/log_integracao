<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 18-03-2016
 * Time: 15:09
 */

namespace App\Model;


class PedidoParceiro extends Model {
    /** @var string[] */
    protected $hidden = array();

    /** @var string */
    public $pedidoParceiro;

    /** @var int */
    public $pedidoProducao;
}