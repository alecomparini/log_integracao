<?php
namespace App\Model;

/**
 * Class Seguro
 *
 * Representação virtual da tabela Pedido_Seguro no banco de dados
 * @package App\Model
 */
class PedidoSeguro {
    /**
     * @var int
     */
    public $pedidoSeguroId;

    /**
     * @var int
     */
    public $dadosImportacaoId;

    /**
     * @var int
     */
    public $pedidoProdutoId;

    /**
     * @var int
     */
    public $seguroTabelaId;

    /**
     * @var int
     */
    public $quantidade;

    /**
     * @var float
     */
    public $valorVenda;

    /**
     * @var int
     */
    public $familia;

    public function columnMap() {
        return array(
            "PedidoSeguroId"        => "pedidoSeguroId",
            "DadosImportacaoId"     => "dadosImportacaoId",
            "PedidoProdutoId"             => "pedidoProdutoId",
            "SeguroTabelaId"        => "seguroTabelaId",
            "Quantidade"            => "quantidade",
            "ValorVenda"            => "valorVenda",
            "Familia"               => "familia",
        );
    }
}
