<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 15-02-2016
 * Time: 15:45
 */

namespace App\Model;


class NotificacaoParceiro extends Model {
    protected $hidden = array();

    const MAX_TENTATIVA_NOTIFICACAO = 5;
    const NOTIFICACAO_ESTOQUE = 1;
    const NOTIFICACAO_PRECO = 2;

    /** @var int */
    public $parceiroId;

    /** @var int */
    public $siteId;

    /** @var int */
    public $pedidoParceiroId;

    /** @var string */
    public $produtoCodigo;

    /** @var string */
    public $dataCriacao;

    /** @var string */
    public $dataUltimaTentativa;

    /** @var int */
    public $tentativas;

    /** @var int */
    public $modificacaoPreco;

    /** @var int */
    public $modificacaoEstoque;

    /** @var int */
    public $modificacaoTracking;

    public function columnMap() {
        return array(
            'ParceiroId' => 'parceiroId',
            'SiteId' => 'siteId',
            'PedidoParceiroId' => 'pedidoParceiroId',
            'ProdutoCodigo' => 'produtoCodigo',
            'DataCriacao' => 'dataCriacao',
            'DataUltimaTentativa' => 'dataUltimaTentativa',
            'Tentativas' => 'tentativas',
            'ModificacaoPreco' => 'modificacaoPreco',
            'ModificacaoEstoque' => 'modificacaoEstoque',
            'ModificacaoTracking' => 'modificacaoTracking'
        );
    }
}
