<?php
namespace App\Model;

/**
 * Class Frete
 * @package App\Model
 */
class Frete {
    /** @var int */
    public $prazoEntrega;

    /** @var float */
    public $valorPromocional;

    /** @var float */
    public $valorTransportadora;

}
