<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 2016-01-06
 * Time: 10:42:32
 */

namespace App\Model;

/**
 * Class ClientePj
 * @package App\Model
 */
class ClientePj extends Cliente {

    /** @var string */
    public $razaoSocial;

    /** @var string */
    public $nomeFantasia;

    /** @var string */
    public $cnpj;

    /** @var string */
    public $inscricaoEstadual;

    /** @var int */
    public $isento;

    /** @var string */
    public $telefone;

    /** @var string */
    public $site;

    /** @var bool */
    public $ramoAtividade;

    /** @var string */
    public $cnae;

    public function columnMap() {
        $internal = array(
            'RazaoSocial' => 'razaoSocial',
            'NomeFantasia' => 'nomeFantasia',
            'Cnpj' => 'cnpj',
            'InscricaoEstadual' => 'inscricaoEstadual',
            'Isento' => 'isento',
            'Telefone' => 'telefone',
            'Site' => 'site',
            'RamoAtividade' => 'ramoAtividade',
            'Cnae' => 'cnae'
        );
        $parent = parent::columnMap();
        return array_merge($internal, $parent);
    }
}