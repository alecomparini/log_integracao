<?php

namespace App\Model;

use App\Helper\Utils;

/**
 * Class Produto
 * @package App\Model
 */
class Produto {
    const STATUS_OFFLINE = 0;
    const STATUS_ONLINE = 1;
    const STATUS_OCULTO = 2;
    const QTD_MAX_PRODUTO_CONSULTA = 10;

    /** @var int */
    public $produtoId;

    /** @var int */
    public $fabricanteId;

    /** @var string */
    public $codigo;

    /** @var string */
    public $nome;

    /** @var string */
    public $descricao;

    /** @var int */
    public $tipo;

    /** @var string */
    public $visualizacao;

    /** @var float */
    public $precoCusto;

    /** @var string */
    public $dataCadastroUniconsult;

    /** @var string */
    public $dataImportacao;

    /** @var string */
    public $dataCadastro;

    /** @var string */
    public $dataDisponivel;

    /** @var string */
    public $dataLancamento;

    /** @var int */
    public $parcelamentoSemJuros;

    /** @var string */
    public $nomeUniconsult;

    /** @var int */
    public $vendeAvulso;

    /** @var string */
    public $vale;

    /** @var string */
    public $unidade;

    /** @var string */
    public $brinde;

    /** @var int */
    public $prazoCrossDocking;

    /** @var int */
    public $tipoTransporte;

    /** @var string */
    public $okFiscal;

    /** @var int */
    public $prazoGarantia;

    /** @var string */
    public $modeloFabricante;

    /** @var string */
    public $tipoABC;

    /** @var string */
    public $situacao;

    /** @var int */
    public $usuarioId;

    /** @var string */
    public $iframe;

    /** @var int */
    public $alturaIframe;

    /** @var int */
    public $emLinha;

    /** @var int */
    public $lojaId;

    /** @var float */
    public $precoPor;

    /** @var float */
    public $precoDe;

    /** @var \App\Model\AtributoSubstituicao[] */
    public $atributosSubstituicao = array();

    /** @var \App\Model\AtributoCorTamanho[] */
    public $atributosCorTamanho = array();

    public function columnMap() {
        return array(
            "ProdutoId" => "produtoId",
            "FabricanteId" => "fabricanteId",
            "Codigo" => "codigo",
            "Nome" => "nome",
            "Descricao" => "descricao",
            "Tipo" => "tipo",
            "Visualizacao" => "visualizacao",
            "PrecoCusto" => "precoCusto",
            "DataCadastroUniconsult" => "dataCadastroUniconsult",
            "DataImportacao" => "dataImportacao",
            "DataCadastro" => "dataCadastro",
            "DataDisponivel" => "dataDisponivel",
            "DataLancamento" => "dataLancamento",
            "ParcelamentoSemJuros" => "parcelamentoSemJuros",
            "NomeUniconsult" => "nomeUniconsult",
            "VendeAvulso" => "vendeAvulso",
            "Vale" => "vale",
            "Unidade" => "unidade",
            "Brinde" => "brinde",
            "PrazoCrossDocking" => "prazoCrossDocking",
            "TipoTransporte" => "tipoTransporte",
            "OkFiscal" => "okFiscal",
            "PrazoGarantia" => "prazoGarantia",
            "ModeloFabricante" => "modeloFabricante",
            "TipoABC" => "tipoABC",
            "Situacao" => "situacao",
            "UsuarioId" => "usuarioId",
            "Iframe" => "iframe",
            "AlturaIframe" => "alturaIframe",
            "EmLinha" => "emLinha",
        );
    }

    /**
     * Remove do objeto de retorno os campos que não estão presentes no retorno do wsdl antigo,
     * mantendo assim a compatibilidade
     */
    public function preparaWsdl() {
        $permitidos = array(
            'codigo',
            'EAN',
            'CodigoFornecedor',
            'nome',
            'descricao',
            'LojaId',
            'Loja',
            'CategoriaId',
            'Categoria',
            'SubCategoriaId',
            'SubCategoria',
            'Fabricante',
            'Status',
            'emLinha',
            'PrecoDe',
            'PrecoPor',
            'ParcelamentoMaximoSemJuros',
            'Caracteristicas',
            'DataAlteracao',
            'Parcelamento'
        );

        return $this->geraObjeto($permitidos);
    }

    /**
     * Define os campos específicos para o retorno de produtoFull conforme WSDL da versão 1.0
     *
     * @param $fatoresCartao
     * @return \stdClass
     */
    public function preparaProdutoFull($fatoresCartao) {
        $permitidos = array(
            'produtoId',
            'codigo',
            'EAN',
            'CodigoFornecedor',
            'nome',
            'descricao',
            'LojaId',
            'Loja',
            'CategoriaId',
            'Categoria',
            'SubCategoriaId',
            'SubCategoria',
            'fabricanteId',
            'Fabricante',
            'Status',
            'emLinha',
            'PrecoDe',
            'PrecoPor',
            'Quantidade',
            'ParcelamentoMaximoSemJuros',
            'Caracteristicas',
            'DataAlteracao',
            'Medias',
            'Altura',
            'Comprimento',
            'Largura',
            'Peso',
            'AlturaEmbalagem',
            'ComprimentoEmbalagem',
            'LarguraEmbalagem',
            'PesoEmbalagem',
            'Disponivel',
            'url',
            'parcelamento',
            'Filhos',
            'ItensIncluso',
            'TipoTransporte'
        );

        $obj = $this->geraObjeto($permitidos);

        $parcelas = new \stdClass();

        for ($parcela = 1; $parcela <= 12; $parcela++) {
            if ($parcela <= $obj->ParcelamentoMaximoSemJuros) {
                $juros = false;
                $valorParcela = ($obj->PrecoPor / $parcela);
            } else {
                $juros = true;
                $valorParcela = ($obj->PrecoPor * $fatoresCartao[$parcela]);
            }

            if ($valorParcela > 5) {
                $parcelas->$parcela = Utils::formataMoeda($valorParcela);
                $parcelas->$parcela .= ($juros) ? ' com juros ' : ' sem juros ';
            } else {
                break;
            }
        }

        $obj->parcelamento = $parcelas;

        //arrumando as medias
        $medias = new \stdClass();
        $oldMedias = $obj->Medias->jpg;
        if (is_array($oldMedias)) {
            foreach ($oldMedias as $i => $media) {
                $medias->$i = $media;
            }
        }
        $obj->Medias = new \stdClass();
        $obj->Medias->jpg = $medias;

        $obj->PrecoDe = Utils::formataMoeda($obj->PrecoDe);
        $obj->PrecoPor = Utils::formataMoeda($obj->PrecoPor);

        return $obj;
    }

    /**
     * Formata o objeto de retorno com base nos valores importantes para o consumidor do rest
     * @return \stdClass
     */
    public function preparaRetornoRest() {

        $proibidos = array(
            "tipo",
            "visualizacao",
            "precoCusto",
            "dataCadastroUniconsult",
            "dataImportacao",
            "dataCadastro",
            "dataDisponivel",
            "dataLancamento",
            "parcelamentoSemJuros",
            "nomeUniconsult",
            "vale",
            "unidade",
            "brinde",
            "prazoCrossDocking",
            //"tipoTransporte",
            "okFiscal",
            "prazoGarantia",
            "modeloFabricante",
            "tipoABC",
            "situacao",
            "usuarioId",
            "iframe",
            "alturaIframe",
            "descontoAVista"
        );
        $novoObjeto = new \stdClass();
        $propriedades = array_keys(get_object_vars($this));

        foreach ($propriedades as $prop) {
            if (!in_array($prop, $proibidos)) {
                $novoObjeto->$prop = $this->$prop;
            }
        }

        if (!$novoObjeto->PrecoPor) {
            $novoObjeto->PrecoPor = '0';
        }
        if (!$novoObjeto->PrecoDe) {
            $novoObjeto->PrecoDe = '0';
        }

        return $novoObjeto;
    }

    /**
     * Gera um novo objeto tipo stdClass com os campos listados a partir do array recebido
     * @param $permitidos
     * @return \stdClass
     */
    private function geraObjeto($permitidos) {
        $novoObjeto = new \stdClass();
        foreach ($permitidos as $prop) {
            $novoObjeto->$prop = $this->$prop;
        }

        return $novoObjeto;
    }
}
