<?php
namespace App\Model;

/**
 * Classe modelo que representa o parceiro site
 * Class ParceiroSite
 * @package App\Model
 */
class ParceiroSite extends Parceiro {
    protected $hidden = array('tokenAutorizacao');

    /** @var int */
    public $parceiroSiteId;
    
    /** @var int */
    public $siteId;
    
    /** @var int */
    public $siteBase;
    
    /** @var int */
    public $tipoIntegrador;
    
    
    public function columnMap() {
        $parentColumnMap = parent::columnMap();
        $thisColumnMap = array(
            "ParceiroSiteId" => "parceiroSiteId",
            "Status" => "status",
            "SiteId" => "siteId",
            "SiteBase" => "siteBase",
            "TipoIntegrador" => "tipoIntegrador"
        );
        return array_merge($thisColumnMap, $parentColumnMap);
    }
}