<?php

namespace App\Model;

/**
 * Description of Localidade
 *
 * @author suportesoftbox
 */
class Localidade extends Model {

    protected $hidden = array("localidadeId");

    public $localidadeId;
    
    public $sigla;
    
    public $cepInicio;
    
    public $cepFim;
    
    public $local;
    
    public function columnMap() {
        return  array(
                    'LocalidadeId' => 'localidadeId',
                    'Sigla' => 'sigla',
                    'CepInicio' => 'cepInicio',
                    'CepFim' => 'cepFim',
                    'Local' => 'local'
                );
    }
}
