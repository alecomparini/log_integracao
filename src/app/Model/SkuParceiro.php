<?php
namespace App\Model;

class SkuParceiro extends Model {
    /**
     * ID do parceiro
     * @var int
     */
    public $parceiroId;

    /**
     * Codigo (SKU) no sistema da máquina de vendas
     * @var string
     */
    public $codigo;

    /**
     * SKU referente ao produto no sistema do parceiro
     * @var string
     */
    public $skuParceiro;

    /**
     * Data em que esse mapeamento foi criado
     * @var string
     */
    public $dataCriacao;
}