<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 25-01-2016
 * Time: 15:12
 */

namespace App\Model;


class Estabelecimento extends Model {
    /** @var */
    public $estabelecimentoId;

    /** @var */
    public $nome;

    /** @var */
    public $status;

    /** @var */
    public $cnpj;

    /** @var */
    public $prioridade;

    /** @var */
    public $prazoCdLeve;

    /** @var */
    public $prazoCdPesado;

    /** @var */
    public $prazoCdLevePos;

    /** @var */
    public $prazoCdPesadoPos;

    /** @var */
    public $horaCorte;

    /**
     * Mapeamento
     */
    public function columnMap() {
        return array (
            'EstabelecimentoId' => 'estabelecimentoId',
            'Nome' => 'nome',
            'Status' => 'status',
            'Cnpj' => 'cnpj',
            'Prioridade' => 'prioridade',
            'PrazoCdLeve' => 'prazoCdLeve',
            'PrazoCdPesado' => 'prazoCdPesado',
            'PrazoCdLevePos' => 'prazoCdLevePos',
            'PrazoCdPesadoPos' => 'prazoCdPesadoPos',
            'HoraCorte' => 'horaCorte',
        );
    }

}