<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 2016-01-06
 * Time: 14:53:12
 */

namespace App\Model;

/**
 * Class CategoriaSite
 * @package App\Model
 */
class CategoriaSite extends Categoria {

    /** @var int */
    public $siteId;

    /** @var int */
    public $categoriaAsc;

    /** @var bool */
    public $status;

    /** @var int */
    public $ordem;

    /** @var bool */
    public $mostrarNaHome;

    /** @var bool */
    public $mostrarNaLoja;

    /** @var int */
    public $vitrineId;

    /** @var string */
    public $corBackground;

    /** @var string */
    public $corFonte;

    /** @var string */
    public $icone;

    /** @var bool */
    public $mostrarFabricante;

    /** @var bool */
    public $mostrarMenuReduzido;

    /** @var string */
    public $textoSEO;

    /** @var string */
    public $variacoesTitulo;

    /** @var string */
    public $variacoesDescricao;

    /** @var string */
    public $iconeMobile;

    /** @var int */
    public $ordemMobile;

    public function columnMap() {
        $parent = parent::columnMap();
        $internal = array(
            'SiteId' => 'siteId',
            'CategoriaAsc' => 'categoriaAsc',
            'Status' => 'status',
            'Ordem' => 'ordem',
            'MostrarNaHome' => 'mostrarNaHome',
            'MostrarNaLoja' => 'mostrarNaLoja',
            'VitrineId' => 'vitrineId',
            'CorBackground' => 'corBackground',
            'CorFonte' => 'corFonte',
            'Icone' => 'icone',
            'MostrarFabricante' => 'mostrarFabricante',
            'MostrarMenuReduzido' => 'mostrarMenuReduzido',
            'TextoSEO' => 'textoSEO',
            'VariacoesTitulo' => 'variacoesTitulo',
            'VariacoesDescricao' => 'variacoesDescricao',
            'IconeMobile' => 'iconeMobile',
            'OrdemMobile' => 'ordemMobile'
        );
        return array_merge($internal, $parent);
    }
}