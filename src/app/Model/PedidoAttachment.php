<?php

namespace App\Model;

/**
 * Class PedidoAttachment
 * @package App\Model
 */
class PedidoAttachment {
    public $pedidoAttachmentId;
    public $dadosImportacaoId;
    public $pedidoProdutoId;
    public $type;
    public $label;
    public $dataCriacao;

    /**
     * Mapeamento
     */
    public function columnMap() {
        $inner = array (
            'PedidoAttachmentId' => 'pedidoAttachmentId',
            'DadosImportacaoId' => 'dadosImportacaoId',
            'PedidoProdutoId' => 'pedidoProdutoId',
            'Tipo' => 'type',
            'Rotulo' => 'label',
            'DataCriacao' => 'dataCriacao',
        );
        return $inner;
    }
}
