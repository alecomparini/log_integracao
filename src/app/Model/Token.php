<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29-02-2016
 * Time: 16:00
 */

namespace App\Model;

class Token extends Model {
    /** @var int */
    public $tokenId;

    /** @var string */
    public $token;

    /** @var string */
    public $dataValidade;

    /** @var string */
    public $dataCriacao;

    /** @var string */
    public $dataUltimaUtilizacao;

    /** @var int */
    public $parceiroId;

    public function columnMap() {
        return array(
            'TokenId' => 'tokenId',
            'Token' => 'token',
            'DataValidade' => 'dataValidade',
            'DataCriacao' => 'dataCriacao',
            'DataUltimaUtilizacao' => 'dataUltimaUtilizacao',
            'ParceiroId' => 'parceiroId',
        );
    }
}