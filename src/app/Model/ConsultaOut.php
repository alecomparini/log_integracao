<?php

namespace App\Model;

/**
 * Classe responsável pela saída da consulta
 */
class ConsultaOut {
    /** @var int */
    public $id;

    /** @var string */
    public $msg;

    /** @var int */
    public $codigo;
}
