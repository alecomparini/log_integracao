<?php

namespace App\Model;

/**
 * Class ProdutoEstoque
 * @package App\Model
 */
class ProdutoEstoque {
    /**
     * @var string
     */
    public $produtoCodigo;

    /**
     * @var int
     */
    public $estoqueDisponivel;

    public function columnMap() {
        return array(
            'ProdutoCodigo' => 'produtoCodigo',
            'EstoqueDisponivel' => 'estoqueDisponivel',
        );
    }
}