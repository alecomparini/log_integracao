<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 05-02-2016
 * Time: 15:04
 */

namespace App\Model;


class ParcelamentoIntervalo extends Model {

    /** @var int */
    public $parcelamentoIntervaloId;

    /** @var int */
    public $parcelamento;

    /** @var float */
    public $inicio;

    /** @var float */
    public $fim;

    public function columnMap() {
        return array(
            "ParcelamentoIntervalosId"  => "parcelamentoIntervaloId",
            "Parcelamento"              => "parcelamento",
            "Inicio"                    => "inicio",
            "Fim"                       => "fim"
        );
    }

}