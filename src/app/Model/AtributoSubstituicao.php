<?php

namespace App\Model;

class AtributoSubstituicao extends Model {
    protected $hidden = array('produtoIdReferencia', 'produtoId');

    /** @var string */
    public $atributoNome;

    /** @var string */
    public $valor;

    /** @var int */
    public $produtoIdReferencia;

    /** @var int */
    public $produtoId;

    /** @var string */
    public $nomeProduto;

    /** @var string */
    public $codigo;

    /** @var string */
    public $ean;

    /** @var string */
    public $produtoOrigem;

    /** @var string */
    public $disponivel;
}