<?php
namespace App\Model;

/**
 * Model Catalogo_Categoria
 *
 * @author Ítallo Costa <itallocosta@softbox.com.br>
 */
class CatalogoProduto extends Catalogo {
    
    public $catalogoId;
    
    public $produtoId;
    
    public $desconto;
    
    public $parceiroId;
    
    public $siteId;
    
    
    public function columnMap() {
        $arr = parent::columnMap();
        return array_merge(
                    $arr,
                    array(
                        'CatalogoId' => 'catalogoId',
                        'ProdutoId' => 'produtoId',
                        'Desconto' => 'desconto',
                        'ParceiroId' => 'parceiroId',
                        'SiteId' => 'siteId',
                    )
                );
    }
}
