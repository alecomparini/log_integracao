<?php
namespace App\Model;

/**
 * Model Campanha
 *
 * @author Ítallo Costa <itallocosta@softbox.com.br>
 */
class Campanha extends Model {
    /** @var int */
    public $campanhaId;
    
    /** @var string */
    public $nome;
    
    /** @var int */
    public $status;
    
    /** @var int */
    public $catalogoId;
    
    /** @var int */
    public $tipoFrete;
    
    /** @var double */
    public $desconto;
    
    /** @var double */
    public $fretePonderado;
    
    /** @var string */
    public $localidades;
    
    public function columnMap() {
        return array(
            'CampanhaId' => 'campanhaId',
            'Nome' => 'nome',
            'Status' => 'status',
            'CatalogoId' => 'catalogoId',
            'TipoFrete' => 'tipoFrete',
            'Desconto'  => 'desconto',
            'FretePonderado' => 'fretePonderado',
            "Localidades" => 'localidades'
        );
    }
}
