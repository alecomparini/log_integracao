<?php
namespace App\Model;

/**
 * Class PedidoGarantia
 * @package App\Model
 */
class PedidoGarantia {
    /**
     * @var int
     */
    public $produtoGarantiaId;

    /**
     * @var int
     */
    public $dadosImportacaoId;

    /**
     * @var int
     */
    public $pedidoProdutoId;

    /**
     * @var int
     */
    public $codigo;

    /**
     * @var int
     */
    public $quantidade;

    /**
     * @var int
     */
    public $prazo;

    /**
     * @var float
     */
    public $valorVenda;

    public function columnMap() {
        return array(
            "ProdutoGarantiaId" => "produtoGarantiaId",
            "DadosImportacaoId" => "dadosImportacaoId",
            "PedidoProdutoId"   => "pedidoProdutoId",
            "GarantiaCodigo"    => "codigo",
            "Quantidade"        => "quantidade",
            "Prazo"             => "prazo",
            "ValorVenda"        => "valorVenda",
        );
    }
}