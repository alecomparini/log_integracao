<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 05-02-2016
 * Time: 11:28
 */

namespace App\Model;


class Parcelamento extends Model {

    /** @var int */
    public $parcelas;

    /** @var float */
    public $valorParcelamento;

    /** @var float */
    public $total;

    /** @var float */
    public $taxaJuros;

    /** @var boolean */
    public $contemJuros;
}