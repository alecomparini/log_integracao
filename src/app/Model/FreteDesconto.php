<?php

namespace App\Model;

/**
 * Description of FreteDesconto
 *
 * @author suportesoftbox
 */
class FreteDesconto extends Model {
    
    public $freteDescontoId;
    
    public $parceiroId;
    
    public $tipoFrete;
    
    public $desconto;
    
      
    public function columnMap() {
        return array(
            'FreteDescontoId' => 'freteDescontoId',
            'ParceiroId' => 'parceiroId',
            'TipoFrete' => 'tipoFrete',
            'Desconto' => 'desconto'
        );
    }
}
