<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 17-03-2016
 * Time: 17:27
 */

namespace App\Model;


class AtributoCorTamanho extends Model {
    protected $hidden = array('produtoId');

    /** @var string */
    public $corNome;

    /** @var string */
    public $tamanho;

    /** @var string */
    public $codigo;

    /** @var int */
    public $produtoId;

    /** @var int */
    public $produtoStatus;

    /** @var int */
    public $disponivel;

    /** @var int */
    public $produtoOrigem;
}