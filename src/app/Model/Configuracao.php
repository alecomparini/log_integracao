<?php

namespace App\Model;

class Configuracao {
    const URL_NOTIFICACAO = 'URL_NOTIFICACAO';
    const URL_NOTIFICACAO_TRACKING = 'URL_NOTIFICACAO';
    const ACEITA_NOTIFICACAO = 'ACEITA_NOTIFICACAO';
    const ACEITA_ATTACHMENTS = 'ACEITA_ATTACHMENTS';
    const ATTACHMENT_SIX_PACK = 'ATTACHMENT_SIX_PACK';
    const URL_NOTIFICAO_ESTORNO_PONTOS = 'URL_NOTIFICAO_ESTORNO_PONTOS';
    const QTD_TENTATIVAS_NOTIFICACAO = 'QTD_TENTATIVAS_NOTIFICACAO';
    const ESTRATEGIA_SET_NOTIFICACAO = 'ESTRATEGIA_SET_NOTIFICACAO';
    const ENDPOINT_BUSCA_SKU_PARCEIRO = 'ENDPOINT_BUSCA_SKU_PARCEIRO';
    const ENDPOINT_BUSCA_ESTOQUE_PARCEIRO = 'ENDPOINT_BUSCA_ESTOQUE_PARCEIRO';
    const BUSCA_ESTOQUE_APP_TOKEN = 'BUSCA_ESTOQUE_APP_TOKEN';
    const BUSCA_ESTOQUE_APP_KEY = 'BUSCA_ESTOQUE_APP_KEY';
    const RELATORIO_ESTOQUE_DIFERENCA_GERAR_NOTIFICACAO = 'RELATORIO_ESTOQUE_DIFERENCA_GERAR_NOTIFICACAO';
    const ESTRATEGIA_RELATORIO_ESTOQUE = 'ESTRATEGIA_RELATORIO_ESTOQUE';
    const DELAY_MINUTOS_NOTIFICACAO_FILA_LENTA = 'DELAY_MINUTOS_NOTIFICACAO_FILA_LENTA';
    const ESTRATEGIA_BUSCA_PRODUTOS_API = 'ESTRATEGIA_BUSCA_PRODUTOS_API';
    const ESTRATEGIA_BUSCA_ESTOQUE_API = 'ESTRATEGIA_BUSCA_ESTOQUE_API';
    const ACEITA_PRE_PEDIDO = 'ACEITA_PRE_PEDIDO';
    const MINUTOS_CANCELAMENTO_PRE_PEDIDO = 'MINUTOS_CANCELAMENTO_PRE_PEDIDO';
    const VALIDA_DATA_AGENDADA = 'VALIDA_DATA_AGENDADA';
    const LIMITE_MAXIMO_ATTACHMENTS = 'LIMITE_MAXIMO_ATTACHMENTS';
    const PRAZO_CD_OBRIGATORIO_PARCEIRO = 'PRAZO_CD_OBRIGATORIO_PARCEIRO';
    const ITENS_PARCEIRO_ATTACH_OBRIGATORIO = 'ITENS_PARCEIRO_ATTACH_OBRIGATORIO';
}