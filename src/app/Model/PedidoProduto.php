<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 05/01/2016
 * Time: 15:52
 */

namespace App\Model;



/**
 * Class PedidoProduto
 * @package App\Model
 */
class PedidoProduto {

    /** @var int */
    public $pedidoProdutoId;

    /** @var int */
    public $dadosImportacaoId;

    /** @var int */
    public $produtoId;

    /** @var int */
    public $codigoProduto;

    /** @var int */
    public $quantidade;

    /** @var float */
    public $valorUnitario;

    /** @var float */
    public $valorTotal;

    /** @var float */
    public $valorFrete;

    /** @var int */
    public $tipo;

    /** @var int */
    public $prazoEntregaCombinado;

    /** @var int */
    public $sequencial;

    /** @var float */
    public $valorDesconto;

    /** @var float */
    public $valorCustoEntrada;

    /** @var \App\Model\PedidoGarantia */
    public $garantia;

    /** @var \App\Model\PedidoSeguro */
    public $seguro;

    /** @var \App\Model\PedidoAttachment[] */
    public $attachments;

    public function columnMap() {
        return array(
            'PedidoProdutoId' => 'pedidoProdutoId',
            'DadosImportacaoId' => 'dadosImportacaoId',
            'ProdutoId' => 'produtoId',
            'ProdutoCodigo' => 'codigoProduto',
            'Quantidade' => 'quantidade',
            'ValorUnitario' => 'valorUnitario',
            'ValorTotal' => 'valorTotal',
            'ValorFrete' => 'valorFrete',
            'Tipo' => 'tipo',
            'PrazoEntregaCombinado' => 'prazoEntregaCombinado',
            'Sequencial' => 'sequencial',
            'ValorDesconto' => 'valorDesconto',
            'ValorCustoEntrada' => 'valorCustoEntrada',
        );
    }

    public static function createFromLegado($pedidoProdutoLegado) {
        $obj = new static();
        foreach ($pedidoProdutoLegado as $propriedade => $valor) {
            $obj->{lcfirst($propriedade)} = $valor;
        }
        return $obj;
    }
}