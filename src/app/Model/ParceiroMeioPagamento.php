<?php

namespace App\Model;

/**
 * Model MeioPagamento
 *
 * @author Ítallo Costa <itallocosta@softbox.com.br>
 */
class ParceiroMeioPagamento extends Model {
    /** @var int  */
    public $meioPagamentoId;
    
    /** @var int  */
    public $parceiroId;
    
    public function columnMap() {
        return array(
            'MeioPagamentoId' => 'meioPagamentoId',
            'ParceiroId' => 'parceiroId'
        );
    }
}
