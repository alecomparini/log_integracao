<?php
namespace App\Model;

/**
 * Class SeguroTabela
 * @package App\Model
 */
class SeguroTabela {
    /**
     * @var int
     */
    public $SeguroTabelaId;

    /**
     * @var int
     */
    public $CategoriaId;

    /**
     * @var float
     */
    public $PrecoInicial;

    /**
     * @var float
     */
    public $PrecoFinal;

    /**
     * @var float
     */
    public $ValorVenda;

    /**
     * @var int
     */
    public $Familia;
}