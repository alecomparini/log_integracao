<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 05-02-2016
 * Time: 17:39
 */

namespace App\Model;


class FatorCartao extends Model {

    protected $hidden = array('bandeiraId');

    /** @var int */
    public $fatorCartaoId;

    /** @var int */
    public $bandeiraId;

    /** @var int */
    public $parcela;

    /** @var float */
    public $fator;

    public function columnMap() {
        return array(
            'FatorCartaoId' => 'fatorCartaoId',
            'BandeiraId' => 'bandeiraId',
            'Parcela' => 'parcela',
            'Fator' => 'fator'
        );
    }
}