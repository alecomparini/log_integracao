<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 05-02-2016
 * Time: 17:05
 */

namespace App\Model;


class Bandeira extends Model {

    protected $hidden = array('status', 'parcelamentos');

    /** @var int */
    public $bandeiraId;

    /** @var string */
    public $nome;

    /** @var int */
    public $status;

    /**
     * Variável auxiliar que armazena os parcelamentos (quando existir)
     * @var \App\Model\Parcelamento[]
     */
    public $parcelamentos;

    public function columnMap() {
        return array(
            'BandeiraId' => 'bandeiraId',
            'Nome' => 'nome',
            'Status' => 'status'
        );
    }
}