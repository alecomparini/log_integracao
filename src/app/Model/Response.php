<?php
namespace App\Model;

/**
 * Class Response
 * @package App\Model
 */
class Response {
    /** @var boolean */
    public $success;

    /** @var string */
    public $data;

    /** @var string */
    public $mensagem;

    /** @var string */
    public $codigo;

    /**
     * Armazena as mensagens padrão de resposta do sistema
     *
     * @var array
     */
    private $responses;

    public function __construct($mensagem, $codigoParam, $data = array(), $success = false) {
        $this->configResponses();

        $this->success = $success;
        $this->data = $data;
        $this->codigo = $codigoParam;

        if (is_null($mensagem) || $mensagem == '') {
            $this->mensagem = $this->getMessage($codigoParam);
        } else if (in_array($codigoParam, $this->getMensagemContrutor(), false)) {
            $this->mensagem = $mensagem;
        } else {
            $ignoreArray = $this->getIgnoreMessageArray();
            $this->mensagem = $this->getMessage($codigoParam)
                . (in_array($this->codigo, $ignoreArray, false) ? '' : ' - ' . $mensagem);
        }
    }

    /**
     * Retorna uma mensagem padrão com base no código recebido ou retorna mensagem informando que não há erros com o
     * código especificado
     *
     * @param int $codigo
     * @return string
     */
    public function getMessage($codigo) {
        if (isset($this->responses[$codigo])) {
            return $this->responses[$codigo];
        }

        return 'ERRO DE SISTEMA: Código de erro não existente';
    }

    /**
     * Recupera o array contendo todas as mensagens de erro do REST
     * @return array
     */
    public function getResponses() {
        return $this->responses;
    }

    /**
     * Erros listados aqui, ignora mensagens provindas da chamada do construtor e retornam apenas a mensagem registrada
     * nesta classe
     *
     * @return array
     */
    public function getIgnoreMessageArray() {
        return array(
            self::PERMISSAO_NEGADA,
            self::DADOS_INTERFACE_ENVIADO,
            self::PRODUTOS_NAO_ENCONTRADOS,
            self::DADOS_INTERFACE_ENVIADO,
            self::PRODUTO_SEM_PERMISSAO,
            //self::ERRO_VALOR_PEDIDO_DIFERENTE,
            self::PEDIDO_SEM_LOCK_EXCLUSIVO,
            self::PERMISSAO_NEGADA_MEIO_PAGAMENTO,
            self::PAGE_NOT_FOUND,
            self::QUANTIDADE_DIVERGENTE,
            self::CODIGO_PARCEIRO_INVALIDO,
            self::INTERFACE_UPDATE_ERROR,
            self::SEQUENCIAL_ITEM_ERRADO,
            self::DATA_ENTREGA_AGENDADA_ERRADA,
            self::DADOS_VAZIOS_PARA_INTERFACE,
			self::AGENDAMENTO_NAO_ENCONTRADO,
			self::USUARIO_NAO_ENCONTRADO,
			self::USUARIO_SENHA_INVALIDO,
            self::ACCESS_TOKEN_NAO_ENCONTRADO,
            self::ACCESS_TOKEN_INVALIDO,
        );
    }

    /**
     * Erros listados aqui ignoram as mensagens registradas e enviam apenas a mensagem que vem na chamada do construtor
     * @return array
     */
    public function getMensagemContrutor() {
        return array(
            static::ERRO_VALOR_PEDIDO_DIFERENTE,
            static::PAGINA_NAO_ENCONTRADA,
            static::PEDIDO_PARCEIRO_JA_IMPORTADO,
        );
    }

    /**
     * Configura as mensagens padrão para retorno nas chamadas
     */
    private function configResponses() {
        $this->responses = array(
            0 => 'Sucesso',
            1 => 'Sem permissão de acesso',
            2 => 'Erro de validação',
            3 => 'Quantidade limite de produtos excedidos na chamada',
            4 => 'Código de parceiro inválido',
            5 => 'Erro de geração de boleto',
            6 => 'Página não encontrada',
            7 => 'Cliente sem permissao para a Interface',
            8 => 'Erro: Sequencial do Pedido_Produto invalido - Verificar o valor de sequencial enviado',
            9 => 'Não há interfaces para confirmação',
            10 => 'Os códigos de produtos recebidos não são válidos',
            11 => 'Erro ao obter dados de Tracking',
            13 => 'Erro: Valor total pago difere do valor total do pedido',
            14 => 'O parâmetro [quantidades] está incompleto',
            15 => 'Vendedor não encontrado com os parâmetros fornecidos',
            16 => 'Informações de estoque não encontradas para o(s) produto(s) fornecido(s)',
            17 => 'Pedido já está cadastrado na nossa base de integração',
            18 => 'Nenhuma bandeira encontrada para o ID fornecido',
            19 => 'Erro: ',
            20 => 'Os itens informados não necessitam de notificações',
            21 => 'Os códigos de produtos informados não são válidos.',
            22 => 'O campo [tipoNotificacao] deve possuir valores entre 1 e 2.',
            23 => 'Dados enviados, aguardando confirmacao',
            24 => 'Json mal formado, verifique os parametros enviados',
            25 => 'Erro interno, entre em contato com o administrador.',
            26 => 'Erro: A data da Entrega Agendada esta fora do prazo aceito',
            27 => 'Erro: Produto não cadastrado ou não liberado para venda',
            28 => 'Attachment incompleto, faltando informação',
            29 => 'Attachment: as propriedades [type] e [label] não podem ser vazias',
            30 => 'O total de itens em [quantidades] difere do total de itens em [produtos]',
            31 => 'Attachment: Para cada item six pack deve haver 6 attachments',
            32 => 'Produto duplicado no corpo do pedido',
            33 => 'Família inválida para o seguro do produto',
            34 => 'Não há catálogo vinculado a campanha. Entre em contato com o administrador.',
            35 => 'Attachment obrigatório para o item',
            40 => 'Pedido já em processo de internalização',
            41 => 'Codigo retail não encontrado',
            42 => 'A contagem de attachments não confere com a quantidade do produto',
            50 => 'Campo Seguro não é uma instância de PedidoSeguro',
            51 => 'Sem permissão no meio de pagamento',
            52 => 'Seguro não disponível para o produto: ',
            53 => 'Erro na gravação de attachment',
            54 => 'O update de confirmacao da interface falhou',
            55 => 'Não há dados disponíveis para exibição',
            56 => 'Agendamento de tarefa não encontrado com o ID fornecido',
            57 => 'Erro de agendamento',
            58 => 'Usuário não encontrado',
            59 => 'Usuário ou senha inválido',
            60 => 'access_token não encontrado',
            61 => 'access_token inválido',
            62 => 'Falha ao salvar pedido',
            63 => 'Erro de Attachments do item do pedido SKU [] a quantidade de attachments por item não pode ultrapassar a quantidade configurada para o parceiro.',
            64 => 'O campo prazoCd é obrigatório.',
            65 => 'O campo prazoCd deve ser maior que 0 e menor igual a 30 dias',
            401 => 'Usuário não autenticado',
            404 => 'Página não encontrada',
            1001 => 'Erro ao consultar SKU parceiro',
            1002 => 'Erro ao informar pagamento do pedido',
        );

    }

    const OK = 0;
    const PERMISSAO_NEGADA = 1;
    const ERRO_VALIDACAO = 2;
    const CODIGO_PARCEIRO_INVALIDO = 4;
    const PAGINA_NAO_ENCONTRADA = 6;
    const SEQUENCIAL_ITEM_ERRADO = 8;
    const CODIGO_PRODUTOS_INVALIDOS = 10;
    const TRACKING_ERRO_GET_DADOS = 11;
    const ERRO_VALOR_PEDIDO_DIFERENTE = 13;
    const QUANTIDADE_INCOMPLETA = 14;
    const VENDEDOR_NAO_ENCONTRADO = 15;
    const PEDIDO_PARCEIRO_JA_IMPORTADO = 17;
    const ERRO = 19;
    const DADOS_INTERFACE_ENVIADO = 23;
    const DATA_ENTREGA_AGENDADA_ERRADA = 26;
    const PRODUTO_SEM_PERMISSAO = 27;
    const ATT_OBJETO_INCOMPLETO = 28;
    const ATT_VARIAVEIS_VAZIAS = 29;
    const QUANTIDADE_DIVERGENTE = 30;
    const ATT_CONTAGEM_ATT_SIXPACK_ERRADA = 31;
    const PRODUTO_DUPLICADO = 32;
    const FAMILIA_INVALIDA = 33;
    const CAMPANHA_SEM_CATALOGO = 34;
    const ITEM_ATTACHMENTS_OBRIGATORIO = 35;
    const PEDIDO_SEM_LOCK_EXCLUSIVO = 40;
    const ATT_QUANTIDADE_NAO_CONFERE = 42;
    const PRODUTOS_NAO_ENCONTRADOS = 50;
    const PERMISSAO_NEGADA_MEIO_PAGAMENTO = 51;
    const SEGURO_NAO_DISPONIVEL = 52;
    const ATT_ERRO_GRAVACAO_DB = 53;
    const INTERFACE_UPDATE_ERROR = 54;
    const DADOS_VAZIOS_PARA_INTERFACE = 55;
    const AGENDAMENTO_NAO_ENCONTRADO = 56;
    const ERRO_AGENDAMENTO = 57;
    const USUARIO_NAO_ENCONTRADO = 58;
    const USUARIO_SENHA_INVALIDO = 59;
    const ACCESS_TOKEN_NAO_ENCONTRADO = 60;
    const ACCESS_TOKEN_INVALIDO = 61;
    const FALHA_SALVAR_PEDIDO = 62;
    const QUANTIDADE_ATTACHMENT_INVALIDA = 63;
    const PRAZO_CD_OBRIGATORIO = 64;
    const PRAZO_CD_VALIDO = 65;

    const NOT_AUTHENTICATED = 401;
    const FORBIDDEN = 403;
    const PAGE_NOT_FOUND = 404;
    const ERRO_CONSULTA_SKU_PARCEIRO = 1001;
}
