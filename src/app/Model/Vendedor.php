<?php
namespace App\Model;

/**
 * Class Vendedor
 * @package App\Model
 * @author Leonardo Ferreira
 */
class Vendedor extends Model {

    /** @var array  */
    protected $hidden = array('senha');

    /** @var string */
    public $senha;

    /** @var string */
    public $email;

    /** @var int */
    public $id;

    /** @var string */
    public $nome;

    /** @var int */
    public $tipo;

    /** @var string */
    public $grupo;

    /** @var int */
    public $grupoId;

    /** @var string */
    public $token;

    public function columnMap() {
        return array(
            'Email' => 'email',
            'Senha' => 'senha',
            'Id' => 'id',
            'Nome' => 'nome',
            'TipoOperador' => 'tipo',
            'GrupoNome' => 'grupo',
            'GrupoId' => 'grupoId',
        );
    }
}