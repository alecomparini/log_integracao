<?php
namespace App\Model\V2;

use App\Model\Model;

class Tarefa extends Model {
    /** @var int */
    public $tarefaId;

    /** @var string */
    public $descricao;

    /** @var string */
    public $nome;

    /** @var string */
    public $url;

    /** @var string JSON contendo os parametros */
    public $parametros;

    /** @var int */
    public $indJson;

    /** @var string */
    public $metodoHttp;

    /** @var string */
    public $contentType;
}