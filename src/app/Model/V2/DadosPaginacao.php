<?php
namespace App\Model\V2;

use App\Model\Model;

class DadosPaginacao extends Model {

    public $totalPaginas = 1;
    public $itensPorPagina = 100;
    public $totalItens = 0;

    /**
     * DadosPaginacao constructor.
     *
     * @param int $itensPorPagina
     * @param int $totalItens
     */
    public function __construct($totalItens, $itensPorPagina = 100) {
        $this->itensPorPagina = $itensPorPagina;
        $this->totalItens = $totalItens;
        $this->recalculaTotalPaginas();
    }

    public function getLimit() {
        return $this->itensPorPagina;
    }

    public function getOffsetPagina($pagina = 1) {
        return ($pagina - 1) * $this->itensPorPagina;
    }

    private function recalculaTotalPaginas() {
        $this->totalPaginas = ceil($this->totalItens / $this->itensPorPagina);
    }
}