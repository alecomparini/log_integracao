<?php
namespace App\Model\V2;

use App\Model\Model;
use Carbon\Carbon;
use Carbon\CarbonInterval;

class AgendamentoTarefa extends Tarefa {
    /** @var int */
    public $agendamentoTarefaId;

    /** @var int */
    public $numExecucoes;

    /** @var int */
    public $numExecutadas;

    /** @var string Usando o formato do CRONTAB */
    public $configuracaoTempo;

    /** @var bool */
    public $status;
}