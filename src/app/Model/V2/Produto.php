<?php
namespace App\Model\V2;

use App\Model\Model;
use stdClass;

class Produto extends Model {
    public $produtoId;
    public $fabricanteId;
    public $fabricante;
    public $codigo;
    public $nome;
    public $descricao;
    public $emLinha;
    public $precoPor;
    public $precoDe;
    public $atributosSubstituicao = array();
    public $atributosCorTamanho = array();
    public $eAN;
    public $codigoFornecedor;
    public $lojaId;
    public $loja;
    public $categoriaId;
    public $categoria;
    public $subCategoria;
    public $subCategoriaId;
    public $status;
    public $quantidade;
    public $parcelamentoMaximoSemJuros;
    public $caracteristicas = array();
    public $dataAlteracao;
    public $medias = array();
    public $itensIncluso = array();
    public $altura;
    public $comprimento;
    public $largura;
    public $peso;
    public $alturaEmbalagem;
    public $larguraEmbalagem;
    public $pesoEmbalagem;
    public $disponivel;
    public $url;
    public $parcelamento = array();
    public $seguro = array();
    public $garantia = array();

    public function __construct() {
        $this->caracteristicas = new stdClass();
        $this->seguro = new stdClass();
        $this->garantia = new stdClass();
        $this->medias = new stdClass();
    }
}