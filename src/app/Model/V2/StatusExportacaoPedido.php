<?php
namespace App\Model\V2;

/**
 * Classe responsável por armazenar as constantes
 * que representam os status de exportacao na tabela
 * riel_integracao.DadosImportacao
 *
 * @package App\Model\V2
 */
class StatusExportacaoPedido {
    const AGUARDANDO_IMPORTACAO = 0;
    const PRE_PEDIDO = 4;
    const PRE_PEDIDO_ERRO = 5;
    const PRE_PEDIDO_CANCELADO = 6;
}