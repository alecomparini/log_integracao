<?php
namespace App\Model\V2;

use App\Model\Model;

class Usuario extends Model {
    /** @var string */
    public $usuarioId;

    /** @var string */
    public $email;

    /** @var string */
    public $senha;

    /** @var string */
    public $dataCriacao;

    /** @var int */
    public $parceiroId;

    public function __construct() {
        $this->hidden[] = 'senha';
    }
}