<?php
namespace App\Model\V2;

use App\Model\Model;

class TokenUsuario extends Model {
    public $tokenUsuarioId;
    public $token;
    public $usuarioId;
    public $dataValidade;
    public $dataCriacao;
}