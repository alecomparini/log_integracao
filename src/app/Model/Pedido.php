<?php

namespace App\Model;

/**
 * Class Pedido
 * @package App\Model
 */
class Pedido {
    /** @var int */
    public $dadosImportacaoId;

    /** @var int */
    public $siteId;

    /** @var int */
    public $siteBase;

    /** @var int */
    public $parceiroId;

    /** @var int */
    public $parceiroSiteId;

    /** @var int */
    public $vendedorId;

    /** @var int */
    public $grupoVendedorId;

    /** @var string */
    public $parceiroIp;

    /** @var string */
    public $clienteEmail;

    /** @var string */
    public $clienteTipo;

    /** @var string */
    public $clientePfNome;

    /** @var string */
    public $clientePfSobrenome;

    /** @var string */
    public $clientePfCPF;

    /** @var string */
    public $clientePfDataNascimento;

    /** @var string */
    public $clientePfSexo;

    /** @var string */
    public $clientePjRazaoSocial;

    /** @var string */
    public $clientePjNomeFantasia;

    /** @var string */
    public $clientePjCNPJ;

    /** @var int */
    public $clientePjIsento;

    /** @var string */
    public $clientePjInscEstadual;

    /** @var string */
    public $clientePjTelefone;

    /** @var  string */
    public $clientePjSite;

    /** @var int */
    public $clientePjRamoAtividade;

    /** @var string */
    public $endEntregaTipo;

    /** @var string */
    public $endEntregaDestinatario;

    /** @var string */
    public $endEntregaCep;

    /** @var string */
    public $endEntregaEndereco;

    /** @var int */
    public $endEntregaNumero;

    /** @var string */
    public $endEntregaComplemento;

    /** @var string */
    public $endEntregaBairro;

    /** @var string */
    public $endEntregaCidade;

    /** @var string */
    public $endEntregaEstado;

    /** @var string */
    public $endEntregaTelefone1;

    /** @var string */
    public $endEntregaTelefone2;

    /** @var string */
    public $endEntregaCelular;

    /** @var string */
    public $endEntregaReferencia;

    /** @var string */
    public $endCobrancaTipo;

    /** @var string */
    public $endCobrancaDestinatario;

    /** @var string */
    public $endCobrancaCep;

    /** @var string */
    public $endCobrancaEndereco;

    /** @var int */
    public $endCobrancaNumero;

    /** @var string */
    public $endCobrancaComplemento;

    /** @var string */
    public $endCobrancaBairro;

    /** @var string */
    public $endCobrancaCidade;

    /** @var string */
    public $endCobrancaEstado;

    /** @var string */
    public $endCobrancaTelefone1;

    /** @var string */
    public $endCobrancaTelefone2;

    /** @var string */
    public $endCobrancaCelular;

    /** @var string */
    public $endCobrancaReferencia;

    /** @var string */
    public $parceiroPedidoId;

    /** @var int */
    public $producaoPedidoId;

    /** @var float */
    public $valorTotal;

    /** @var float */
    public $valorTotalFrete;

    /** @var float */
    public $valorTotalProdutos;

    /** @var float */
    public $valorJuros;

    /** @var float */
    public $valorFreteSemPromocao;

    /** @var \App\Model\PedidoProduto[] */
    public $pedidoProduto;

    /** @var \App\Model\Pagamento[] */
    public $pagamento;

    /** @var string */
    public $dataEntrega;

    /** @var int */
    public $turnoEntrega;

    /** @var float */
    public $valorEntrega;

    /** @var string */
    public $vendedorToken;

    /** @var int */
    public $tipoInterfaceId;

    /** @var string */
    public $attachments;
    
    /** @var integer */
    public $integradorId;

    /** @var integer */
    public $campanhaId;

    /** @var int */
    public $statusExportacao;

    /** @var string */
    public $mensagemErroImportacao;

    /** @var string */
    public $dataErro;

    /** @var  int */
    public $prazoCd;

    /**
     * Cria e hidrata uma nova instância de Model/Pedido a partir dos parâmetros recebidos
     *
     * @param $params
     * @return Pedido
     */
    public static function createPedido($params) {
        $pedido = new Pedido();
        $propriedades = array_keys(get_object_vars($pedido));

        foreach ($params as $prop => $value) {
            if (in_array($prop, $propriedades, false)) {
                $pedido->$prop = $value;
            }
        }

        //convertendo os stdClasses de PedidoProduto para Objetos PedidoProduto
        $tmpArray = array();

        if (is_array($pedido->pedidoProduto) && count($pedido->pedidoProduto) > 0) {
            foreach ($pedido->pedidoProduto as $stdPedido) {
                $pedidoProduto = new PedidoProduto();
                $propriedades = array_keys(get_object_vars($stdPedido));

                foreach ($propriedades as $prop) {
                    if (in_array($prop, $propriedades, false)) {
                        $pedidoProduto->$prop = $stdPedido->$prop;
                    }
                }

                //convertendo os stdClasses de Garantia para Objetos Garantia
                $stdGarantia = $pedidoProduto->garantia;

                if (isset($pedidoProduto->garantia) && is_object($pedidoProduto->garantia)) {
                    $garantia = new PedidoGarantia();
                    $propriedades = array_keys(get_object_vars($stdGarantia));

                    foreach ($propriedades as $prop) {
                        $garantia->{lcfirst($prop)} = $stdGarantia->$prop;
                    }

                    $pedidoProduto->garantia = $garantia;
                }

                //convertendo os stdClasses de Seguro para Objetos Seguro
                $stdSeguro = $pedidoProduto->seguro;

                if (isset($pedidoProduto->seguro) && is_object($pedidoProduto->seguro)) {
                    $seguro = new PedidoSeguro();
                    $seguro->produtoId = $pedidoProduto->produtoId;
                    $propriedades = array_keys(get_object_vars($stdSeguro));

                    foreach ($propriedades as $prop) {
                        if (in_array($prop, $propriedades, false)) {
                            $seguro->{lcfirst($prop)} = $stdSeguro->$prop;
                        }
                    }

                    $pedidoProduto->seguro = $seguro;
                }

                //Convertendo o array de attachments para Objetos PedidoAttachment
                if (null !== $pedidoProduto->attachments && is_array($pedidoProduto->attachments)) {
                    $attachments = array();
                    foreach ($pedidoProduto->attachments as $stdAttachment) {
                        $attachment = new PedidoAttachment();
                        $propriedades = array_keys(get_object_vars($stdAttachment));
                        foreach ($propriedades as $prop) {
                            if (in_array($prop, $propriedades, false)) {
                                $attachment->{lcfirst($prop)} = $stdAttachment->$prop;
                            }
                        }
                        $attachments[] = $attachment;
                    }

                    // Troca pelo novo convertido objeto
                    $pedidoProduto->attachments = $attachments;
                }

                $tmpArray[] = $pedidoProduto;
            }

            $pedido->pedidoProduto = $tmpArray;
        }

        //convertendo os stdClasses de pagamento para Objetos Pagamento
        $tmpArray = array();

        if (is_array($pedido->pagamento) && count($pedido->pagamento) > 0) {
            foreach ($pedido->pagamento as $stdPagamento) {
                $pgtoProduto = new Pagamento();
                $propriedades = array_keys(get_object_vars($stdPagamento));

                foreach ($propriedades as $prop) {
                    if (in_array($prop, $propriedades)) {
                        $pgtoProduto->$prop = $stdPagamento->$prop;
                    }
                }

                $tmpArray[] = $pgtoProduto;
            }

            $pedido->pagamento = $tmpArray;
        }

        return $pedido;
    }
    public static function createFromLegado($pedidoLegado) {
        $obj = new static();
        foreach ($pedidoLegado as $propriedade => $valor) {
            if ($propriedade == "PedidoProduto") {
                $pedidosProduto = array();
                foreach ($valor as $pedidoProduto) {
                    $pedidosProduto[] = PedidoProduto::createFromLegado($pedidoProduto);
                }
                $obj->{lcfirst($propriedade)} = $pedidosProduto;
            }elseif ($propriedade == "Pagamento") {
                $pagamentos = array();
                foreach ($valor as $pagamento) {
                    $pagamentos[] = Pagamento::createFromLegado($pagamento);
                }
                $obj->{lcfirst($propriedade)} = $pagamentos;
            } else {
                $obj->{lcfirst($propriedade)} = $valor;
            }
        }
        return $obj;
    }
}