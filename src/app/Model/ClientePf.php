<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 2016-01-06
 * Time: 10:37:24
 */

namespace App\Model;

/**
 * Class ClientePf
 * @package App\Model
 */
class ClientePf extends Cliente {

    /** @var string */
    public $nome;

    /** @var string */
    public $sobrenome;

    /** @var string */
    public $apelido;

    /** @var string */
    public $cpf;

    /** @var string */
    public $dataNascimento;

    /** @var string */
    public $sexo;

    /** @var bool */
    public $profissao;

    /** @var int */
    public $atributoTamanhoId;

    public function columnMap() {
        $parent = parent::columnMap();
        $internal = array(
            'Nome' => 'nome',
            'Sobrenome' => 'sobrenome',
            'Apelido' => 'apelido',
            'Cpf' => 'cpf',
            'DataNascimento' => 'dataNascimento',
            'Sexo' => 'sexo',
            'Profissao' => 'profissao',
            'AtributoTamanhoId' => 'atributoTamanhoId'
        );

        return array_merge($internal, $parent);
    }
}