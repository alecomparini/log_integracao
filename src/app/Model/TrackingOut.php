<?php
namespace App\Model;

/**
 * Class TrackingOut
 * @package App\Model
 */
class TrackingOut {
    /** @var string */
    public $msg;
    
    /** @var int */
    public $codigo;
    
    /** @var string */
    public $dados;
    
    /**
     * Metodo para montar o objeto de retorno de tracking
     * Retorna mensagens para tracking
     * @param int
     * @return string
     */
    public function getMsg($codigoParam) {
        $this->codigo = $codigoParam;
        $mensagens = array(
            0 => 'Sucesso',
            1 => 'Erro: ParceiroId informado não corresponde ao SiteId informado',
            2 => 'Erro: Sem acesso a interface de tracking',
            3 => 'Erro: Permissao negada para o seu IP',
            4 => 'Erro: Dados enviados, aguardando confirmacao',
            5 => 'Erro: Erro de validação'
        );

        $this->msg = "Código de mensagem inexistente!";
        if (is_numeric($codigoParam) && isset($mensagens[$codigoParam])) {
            $this->msg = $mensagens[$codigoParam];
        }

        return $this->msg;
    }
}