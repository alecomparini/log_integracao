<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 05/01/2016
 * Time: 15:52
 */

namespace App\Model;



/**
 * Class Pagamento
 * @package App\Model
 */
class Pagamento {
    /** @var int */
    public $meioPagamentoId;

    /** @var int */
    public $bandeiraId;

    /** @var int */
    public $parcelas;

    /** @var string */
    public $cartaoNumero;

    /** @var string */
    public $cartaoValidade;

    /** @var string */
    public $cartaoCodSeguranca;

    /** @var string */
    public $cartaoNomePortador;

    /** @var float */
    public $valorPago;

    /** @var float */
    public $percentualJuros;

    /** @var float */
    public $valorJuros;

    /** @var int */
    public $sequencial;

    /** @var string */
    public $boletoUrl;

    /** @var string */
    public $dataVencimentoBoleto;

    /** @var string */
    public $codigoVale;

    /** @var int */
    public $nsu;

    /** @var int */
    public $autorizacaoId;

    /** @var string */
    public $dataAutorizacao;

    /** @var string */
    public $dataCaptura;

    /** @var string */
    public $adquirente;
    
    /** @var string */
    public $tokenAutorizacao;

    /** @var float */
    public $pontos;

    public static function createFromLegado($pagamentoLegado) {
        $obj = new static();
        foreach ($pagamentoLegado as $propriedade => $valor) {
            $obj->{lcfirst($propriedade)} = $valor;
        }
        return $obj;
    }

    const CARTAO_CREDITO = 1;
    const BOLETO_BANCARIO = 2;
    const VALE = 3;
    const BOLETO_POS_PAGO = 7;
    const PONTOS = 8;
    const CARTAO_PAGO = 9;
    const BOLETO_TERCEIROS = 10;
    const BOLETO_PAYU = 11;
}