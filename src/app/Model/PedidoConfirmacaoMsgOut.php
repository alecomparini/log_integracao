<?php

namespace App\Model;

/**
 * Classe que mapeia uma mensagem de acordo com um código fornecido
 * Class PedidoConfirmacaoMsgOut
 * @package App\Model
 */
class PedidoConfirmacaoMsgOut {
    
    /** @var int */
    public $Codigo;

    /** @var string */
    public $Msg;

    /**
    * Metodo para montar o objeto de Retorno
    * Descricao do metodo
    * @return self
    */
    public function getMsg() {
        $mensagens = array(
            0 => 'Interface confirmada com sucesso',
            1 => 'Erro: Cadastro do parceiro incompleto',
            2 => 'Erro: Nao existe Interface em aberto',
            3 => 'Erro: Cliente sem permissao para a Interface',
            4 => 'Erro: O update de confirmacao da interface falhou',
            5 => 'Erro de Validacao: ',
            20 => 'Erro de Validacao - Entrar em contato com o Administrador da integracao MV',
        );

        if (isset($this->Codigo) && is_numeric($this->Codigo)) {
            if (isset($mensagens[$this->Codigo])) {
                $this->Msg = $mensagens[$this->Codigo];
            } else {
                $this->Msg = "Codigo de mensagem inexistente";
            }
        }
        
        return $this;
    }
}