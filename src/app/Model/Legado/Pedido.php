<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 05/01/2016
 * Time: 14:30
 */

namespace App\Model\Legado;


use App\Model\Garantia;
use App\Model\PedidoGarantia;
use App\Model\PedidoSeguro;

/**
 * Class Pedido
 * @package App\Model\Legado
 */
class Pedido {
    /** @var int */
    public $SiteId;

    /** @var int */
    public $SiteBase;

    /** @var int */
    public $ParceiroId;

    /** @var int */
    public $ParceiroSiteId;

    /** @var int */
    public $VendedorId;

    /** @var int */
    public $GrupoVendedorId;

    /** @var string */
    public $ParceiroIp;

    /** @var string */
    public $ClienteEmail;

    /** @var string */
    public $ClienteTipo;

    /** @var string */
    public $ClientePfNome;

    /** @var string */
    public $ClientePfSobrenome;

    /** @var string */
    public $ClientePfCPF;

    /** @var string */
    public $ClientePfDataNascimento;

    /** @var string */
    public $ClientePfSexo;

    /** @var string */
    public $ClientePjRazaoSocial;

    /** @var string */
    public $ClientePjNomeFantasia;

    /** @var string */
    public $ClientePjCNPJ;

    /** @var int */
    public $ClientePjIsento;

    /** @var string */
    public $ClientePjInscEstadual;

    /** @var string */
    public $ClientePjTelefone;

    /** @var  string */
    public $ClientePjSite;

    /** @var int */
    public $ClientePjRamoAtividade;

    /** @var string */
    public $EndEntregaTipo;

    /** @var string */
    public $EndEntregaDestinatario;

    /** @var string */
    public $EndEntregaCep;

    /** @var string */
    public $EndEntregaEndereco;

    /** @var int */
    public $EndEntregaNumero;

    /** @var string */
    public $EndEntregaComplemento;

    /** @var string */
    public $EndEntregaBairro;

    /** @var string */
    public $EndEntregaCidade;

    /** @var string */
    public $EndEntregaEstado;

    /** @var string */
    public $EndEntregaTelefone1;

    /** @var string */
    public $EndEntregaTelefone2;

    /** @var string */
    public $EndEntregaCelular;

    /** @var string */
    public $EndEntregaReferencia;

    /** @var string */
    public $EndCobrancaTipo;

    /** @var string */
    public $EndCobrancaDestinatario;

    /** @var string */
    public $EndCobrancaCep;

    /** @var string */
    public $EndCobrancaEndereco;

    /** @var int */
    public $EndCobrancaNumero;

    /** @var string */
    public $EndCobrancaComplemento;

    /** @var string */
    public $EndCobrancaBairro;

    /** @var string */
    public $EndCobrancaCidade;

    /** @var string */
    public $EndCobrancaEstado;

    /** @var string */
    public $EndCobrancaTelefone1;

    /** @var string */
    public $EndCobrancaTelefone2;

    /** @var string */
    public $EndCobrancaCelular;

    /** @var string */
    public $EndCobrancaReferencia;

    /** @var string */
    public $ParceiroPedidoId;

    /** @var float */
    public $ValorTotal;

    /** @var float */
    public $ValorTotalFrete;

    /** @var float */
    public $ValorTotalProdutos;

    /** @var float */
    public $ValorJuros;

    /** @var float */
    public $ValorFreteSemPromocao;

    /** @var \App\Model\Legado\PedidoProduto[] */
    public $PedidoProduto;

    /** @var \App\Model\Legado\Pagamento[] */
    public $Pagamento;

    /** @var string */
    public $DataEntrega;

    /** @var int */
    public $TurnoEntrega;

    /** @var float */
    public $ValorEntrega;

    /** @var string */
    public $VendedorToken;

    /** @var int */
    public $TipoInterfaceId;

    /**
     * Cria e hidrata uma nova instância de Model/Pedido a partir dos parâmetros recebidos
     *
     * @param $params
     * @return Pedido
     */
    public static function createPedido($params) {
        $pedido = new Pedido();
        $propriedades = array_keys(get_object_vars($pedido));

        foreach ($params as $prop => $value) {
            if (in_array($prop, $propriedades)) {
                $pedido->$prop = $value;
            }
        }

        //convertendo os stdClasses de PedidoProduto para Objetos PedidoProduto
        $tmpArray = array();

        if (is_array($pedido->PedidoProduto) && count($pedido->PedidoProduto) > 0) {
            foreach ($pedido->PedidoProduto as $stdPedido) {
                $pedidoProduto = new PedidoProduto();
                $propriedades = array_keys(get_object_vars($stdPedido));

                foreach ($propriedades as $prop) {
                    if (in_array($prop, $propriedades)) {
                        $pedidoProduto->$prop = $stdPedido->$prop;
                    }
                }

                //convertendo os stdClasses de Garantia para Objetos Garantia
                $stdGarantia = $pedidoProduto->Garantia;

                if (isset($pedidoProduto->Garantia) && is_object($pedidoProduto->Garantia)) {
                    $garantia = new PedidoGarantia();
                    $propriedades = array_keys(get_object_vars($stdGarantia));

                    foreach ($propriedades as $prop) {
                        $garantia->{lcfirst($prop)} = $stdGarantia->$prop;
                    }

                    $pedidoProduto->Garantia = $garantia;
                }

                //convertendo os stdClasses de Seguro para Objetos Seguro
                $stdSeguro = $pedidoProduto->Seguro;

                if (isset($pedidoProduto->Seguro) && is_object($pedidoProduto->Seguro)) {
                    $seguro = new PedidoSeguro();
                    $seguro->produtoId = $pedidoProduto->ProdutoId;
                    $propriedades = array_keys(get_object_vars($stdSeguro));

                    foreach ($propriedades as $prop) {
                        if (in_array($prop, $propriedades)) {
                            $seguro->{lcfirst($prop)} = $stdSeguro->$prop;
                        }
                    }

                    $pedidoProduto->Seguro = $seguro;
                }

                $tmpArray[] = $pedidoProduto;
            }

            $pedido->PedidoProduto = $tmpArray;
        }

        //convertendo os stdClasses de pagamento para Objetos Pagamento
        $tmpArray = array();

        if (is_array($pedido->Pagamento) && count($pedido->Pagamento) > 0) {
            foreach ($pedido->Pagamento as $stdPagamento) {
                $pgtoProduto = new Pagamento();
                $propriedades = array_keys(get_object_vars($stdPagamento));

                foreach ($propriedades as $prop) {
                    if (in_array($prop, $propriedades)) {
                        $pgtoProduto->$prop = $stdPagamento->$prop;
                    }
                }

                $tmpArray[] = $pgtoProduto;
            }

            $pedido->Pagamento = $tmpArray;
        }

        return $pedido;
    }
}