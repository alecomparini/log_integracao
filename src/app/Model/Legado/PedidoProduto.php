<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 05/01/2016
 * Time: 15:52
 */

namespace App\Model\Legado;

/**
 * Class PedidoProduto
 * @package App\Model\Legado
 */
class PedidoProduto {
    /** @var int */
    public $ProdutoId;

    /** @var int */
    public $CodigoProduto;

    /** @var int */
    public $Quantidade;

    /** @var float */
    public $ValorUnitario;

    /** @var float */
    public $ValorTotal;

    /** @var float */
    public $ValorFrete;

    /** @var int */
    public $Tipo;

    /** @var int */
    public $PrazoEntregaCombinado;

    /** @var int */
    public $Sequencial;

    /** @var float */
    public $ValorDesconto;

    /** @var float */
    public $ValorCustoEntrada;

    /** @var \App\Model\PedidoGarantia */
    public $Garantia;

    /** @var \App\Model\PedidoSeguro */
    public $Seguro;
}