<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 05/01/2016
 * Time: 15:52
 */

namespace App\Model\Legado;

/**
 * Class Pagamento
 * @package App\Model\Legado
 */
class Pagamento {
    /** @var int */
    public $MeioPagamentoId;

    /** @var int */
    public $BandeiraId;

    /** @var int */
    public $Parcelas;

    /** @var string */
    public $CartaoNumero;

    /** @var string */
    public $CartaoValidade;

    /** @var string */
    public $CartaoCodSeguranca;

    /** @var string */
    public $CartaoNomePortador;

    /** @var float */
    public $ValorPago;

    /** @var float */
    public $PercentualJuros;

    /** @var float */
    public $ValorJuros;

    /** @var int */
    public $Sequencial;

    /** @var string */
    public $BoletoUrl;

    /** @var string */
    public $DataVencimentoBoleto;

    /** @var string */
    public $CodigoVale;
}