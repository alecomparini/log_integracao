<?php

namespace App\Model;

/**
 * Class InterfacePermissao
 * @package App\Model
 */
class InterfacePermissao {
    
    /** @var int */
    public $interfacePermissaoId;
    
    /** @var int */
    public $parceiroSiteId;
    
    /** @var int */
    public $interfaceTipoId;
    
    /** @var string */
    public $nome;
    
    /**
     * Deveria ser bool!
     * @var int
     */
    public $status;
    
    public function columnMap() {
        return array(
            "InterfacePermissaoId" => "interfacePermissaoId",
            "ParceiroSiteId" => "parceiroSiteId",
            "InterfaceTipoId" => "interfaceTipoId",
            "Nome" => "nome",
            "Status" => "status"
        );
    }
}