<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 2016-01-06
 * Time: 10:22:42
 */

namespace App\Model;

/**
 * Class Cliente
 * @package App\Model
 */
class Cliente {

    /** @var int */
    public $clienteId;

    /** @var int */
    public $enderecoCobrancaId;

    /** @var int */
    public $enderecoEntregaId;

    /** @var string */
    public $email;

    /** @var string */
    public $senha;

    /** @var string */
    public $dataCadastro;

    /** @var string */
    public $tipo;

    /** @var bool */
    public $status;

    /** @var int */
    public $news;

    /** @var int */
    public $siteId;

    public function columnMap() {
        return array(
            'ClienteId' => 'clienteId',
            'EnderecoCobrancaId' => 'enderecoCobrancaId',
            'Email' => 'email',
            'Senha' => 'senha',
            'DataCadastro' => 'dataCadastro',
            'Status' => 'status',
            'News' => 'news',
            'SiteId' => 'siteId',
        );
    }
}