<?php
namespace App\Model;

/**
 * Model Catalogo
 *
 * @author suportesoftbox
 */
class Catalogo extends Campanha {
    
    /** @var int */
    public $catalogoId;
    
    /** @var string */
    public $nome;
    
    /** @var int */
    public $status;   
    
    /**
     * Variavel auxiliar para carregar o status da campanha
     * @var int 
     */
    public $statusCampanha;
    
    /**
     * Variavel auxiliar para carregar o nome da campanha
     * @var string 
     */
    public $campanha;
    
    public function columnMap() {
        $arr = parent::columnMap();
        
        return array_merge(
                $arr,
                array(
                    'CatalogoId' => 'catalogoId',
                    'Nome' => 'nome',
                    'Status' => 'status'            
                )
            );
    }
}
