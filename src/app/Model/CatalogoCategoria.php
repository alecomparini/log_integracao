<?php
namespace App\Model;

/**
 * Model Catalogo_Categoria
 *
 * @author suportesoftbox
 */
class CatalogoCategoria extends Catalogo {
    
    public $catalogoId;
    
    public $subCategoriaId;
    
    public $categoriaId;
    
    public $lojaId;
    
    public $valorDesconto;
    
    public $categorias;
    
    public function columnMap() {
        $arr = parent::columnMap();
        return array_merge(
                    $arr,
                    array(
                        'CatalogoId' => 'catalogoId',
                        'SubCategoriaId' => 'subCategoriaId',
                        'CategoriaId' => 'categoriaId',
                        'LojaId'      => 'lojaId',
                        'ValorDesconto' => 'valorDesconto',
                        'Categorias' => 'categorias'
                    )
                );
    }
}
