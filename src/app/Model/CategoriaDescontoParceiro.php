<?php
namespace App\Model;

/**
 * @author: William Okano <williamokano@gmail.com>
 * @createdAt: 20-04-2016 17:38
 * @package: App\Model
 */
class CategoriaDescontoParceiro extends Model {
    /** @var int */
    public $categoriaDescontoParceiroId;

    /** @var int */
    public $categoriaId;

    /** @var int */
    public $parceiroId;

    /** @var float */
    public $desconto;

    /** @var int */
    public $tipoDesconto;

    public function columnMap() {
        return array(
            'CategoriaDescontoParceiroId' => 'categoriaDescontoParceiroId',
            'CategoriaId' => 'categoriaId',
            'ParceiroId' => 'parceiroId',
            'Desconto' => 'desconto',
            'TipoDesconto' => 'tipoDesconto',
        );
    }
}