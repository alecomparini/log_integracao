<?php
namespace App\Model;

/**
 * Classe que representa um formato de dados retornado pelo sistema
 * Class DadosOut
 * @package App\Model
 */
class DadosOut {
    /** @var int */
    public $Codigo;

    /** @var string */
    public $Dados;

    /** @var string */
    public $Msg;

    public function __construct() {
        $this->Codigo = 0;
        $this->Dados = null;
        $this->Msg = "";
    }
}
