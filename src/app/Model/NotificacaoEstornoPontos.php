<?php
namespace App\Model;

/**
 * Description of NotificacaoEstornoPontos
 *
 * @author suportesoftbox
 */
class NotificacaoEstornoPontos {

    /** @var int */
    public $parceiroId;

    /** @var string */
    public $pedidoParceiroId;

    /** @var string */
    public $dataCriacao;

    /** @var string */
    public $dataUltimaTentativa;

    /** @var int */
    public $tentativas;

    /** @var int */
    public $clienteDocumento;

    /** @var int */
    public $pontos;

    public function columnMap() {
        return array(
            'ParceiroId' => 'parceiroId',
            'PedidoParceiroId' => 'pedidoParceiroId',
            'DataCriacao' => 'dataCriacao',
            'DataUltimaTentativa' => 'dataUltimaTentativa',
            'Tentativas' => 'tentativas',
            'ClienteDocumento' => 'clienteDocumento',
            'Pontos' => 'pontos'
        );
    }

}
