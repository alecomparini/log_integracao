<?php
namespace App\Model;

/**
 * DTO (Model) representando o estoque na aplicação do parceiro
 * @package App\Model
 */
class EstoqueParceiro extends Model {

    /**
     * ID interno do produto dentro do sitema do website
     * @var int
     */
    public $produtoId;

    /**
     * Código (SKU) da MV
     * @var string
     */
    public $codigo;

    /**
     * Nome do produto associado ao SKU
     * @var string
     */
    public $nome;

    /**
     * SKU na aplicação do parceiro
     * @var string
     */
    public $skuParceiro;

    /**
     * ID do Parceiro dentro do sistema da MV
     * @var int
     */
    public $parceiroId;

    /**
     * Quantidade de itens no estoque do parceiro
     * @var int
     */
    public $quantidadeParceiro;

    /**
     * Quantidade de itens no estoque na máquina de vendas
     * @var int
     */
    public $quantidadeMV;

    /**
     * Calcula a diferença de itens MV e parceiro
     * 
     * @return int
     */
    public function getDiferenca() {
        return (int)$this->quantidadeMV - (int)$this->quantidadeParceiro;
    }
}