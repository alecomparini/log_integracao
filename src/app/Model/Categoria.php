<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 2016-01-06
 * Time: 14:49:46
 */

namespace App\Model;

/**
 * Class Categoria
 * @package App\Model
 */
class Categoria extends Model {

    protected $hidden = array();

    /** @var int */
    public $categoriaId;

    /** @var int */
    public $categoriaAsc;

    /** @var string */
    public $nome;

    /** @var int */
    public $nivel;

    /** @var string */
    public $metaEstoque;

    /** @var bool */
    public $vendeAvulso;

    /** @var string */
    public $variacoesTitulo;

    /** @var string */
    public $variacoesDescricao;

    /** @var int */
    public $tipo;

    /** @var string */
    public $url;

    /** @var int */
    public $novaAba;

    public function columnMap() {
        return array(
            'CategoriaId' => 'categoriaId',
            'CategoriaAsc' => 'categoriaAsc',
            'Nome' => 'nome',
            'Nivel' => 'nivel',
            'MetaEstoque' => 'metaEstoque',
            'VendeAvulso' => 'vendeAvulso',
            'VariacoesTitulo' => 'variacoesTitulo',
            'VariacoesDescricao' => 'variacoesDescricao',
            'Tipo' => 'tipo',
            'Url' => 'url',
            'NovaAba' => 'novaAba'
        );
    }
}