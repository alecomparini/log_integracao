<?php
namespace App\Model;

/**
 * Class Tracking
 * @package App\Model
 */
class Tracking {
    public $pedidoId;
    public $parceiroId;
    public $pedidoParceiroId;
    public $json;
    public $hit;
    public $versao;
    public $dataCriacao;
    public $dataAlteracao;
    public $dataUltimoConsumo;
    
    public function columnMap() {
        return array(
            "PedidoId" => "pedidoId",
            "ParceiroId" => "parceiroId",
            "PedidoParceiroId" => "pedidoParceiroId",
            "Json" => "json",
            "Hit" => "hit",
            "Versao" => "versao",
            "DataCriacao" => "dataCriacao",
            "DataAlteracao" => "dataAlteracao",
            "DataUltimoConsumo" => "dataUltimoConsumo"
        );
    }
}