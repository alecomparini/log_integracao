<?php
namespace App\Model;

/**
 * Class ParceiroSitePermissao
 * @package App\Model
 */
class ParceiroSitePermissao extends Parceiro {

    const TUDO = 3;
    const FABRICANTE = 1;
    const CATEGORIA = 3;
    const CAMPANHA = 4;

    /** @var int */
    public $parceiroSitePermissaoId;
    
    /** @var int */
    public $permissaoTipoId;
    
    /** @var int */
    public $permissao;
    
    /** @var int */
    public $excecao;

    /** @var int */
    public $siteBase;

    /** @var int */
    public $siteId;

    /** @var int */
    public $parceiroSiteId;

    public function columnMap() {
        $parentColumnMap = parent::columnMap();
        $thisColumnMap = array(
            "ParceiroSitePermissaoId" => "parceiroSitePermissaoId",
            "PermissaoTipoId" => "permissaoTipoId",
            "Permissao" => "permissao",
            "Excecao" => "excecao",
            "SiteBase" => "siteBase",
            "ParceiroSiteId" => "parceiroSiteId",
            "SiteId" => "siteId"
        );
        return array_merge($thisColumnMap, $parentColumnMap);
    }
}