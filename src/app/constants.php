<?php
use App\Core\Config;

// Nome da pasta App
define('APP_FOLDER', 'app');

// Nome da pasta dos logs
define('LOG_FOLDER', '___logs');

// Separador de diretório
define('DS', DIRECTORY_SEPARATOR);

// Separador de caminho
define('PS', PATH_SEPARATOR);

// Separador de namespace
define('NSS', '\\');

// Caminho real do root
define('ROOT_DIR', realpath(dirname(__DIR__)) . DS);

// Caminho da pasta APP
define('APP_DIR', ROOT_DIR . APP_FOLDER . DS);

// Caminho real do root
define('SRC_DIR', ROOT_DIR);

// Caminho real dos Testes
define('TEST_DIR', realpath(dirname(ROOT_DIR)) . DS . 'test' . DS);

// Caminho real dos Logs
define('LOG_DIR', SRC_DIR . LOG_FOLDER . DS);

// Caminho dos mocks
define('MOCK_DIR', TEST_DIR . 'mocks' . DS);

define('QTD_MAX_PRODUTO_CONSULTA', 10);

define('BASE_URL', Config::get('base_url'));

define('DB_INTEGRACAO', Config::get('db.base.integracao', 'riel_integracao'));
define('DB_MAGAZINE', Config::get('db.base.magazine', 'riel_producao'));
define('DB_GATEWAY', Config::get('db.base.gateway', 'mv_gateway'));
define('DB_SIGE', Config::get('db.base.sige', 'riel_sige'));

define('IPS_BYPASS', '189.125.137.196');