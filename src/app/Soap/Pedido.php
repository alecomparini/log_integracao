<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 05/01/2016
 * Time: 14:28
 */

namespace App\Soap;

use App\BO\PedidoBO;
use App\Exceptions\DatabaseException;
use App\Exceptions\ValidacaoException;
use App\Helper\PermissaoHelper;
use App\Model\DadosOut;

use App\Model\Pedido as PedidoModel;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class Pedido
 * @package App\Soap
 */
class Pedido {

    /**
     * Titulo do metodo
     * Descricao do metodo
     * @param \App\Model\Legado\Pedido
     * @return PedidoMsgOut
     */
    public function setPedido($Pedido) {
        set_time_limit(600);
        $request = Request::createFromGlobals();
        $pedidoBO = new PedidoBO();
        $dadosOut = new DadosOut();
        $dadosOut->Msg = "Pedido importado com sucesso";

        $permissoes = array();
        try {
            if (!PermissaoHelper::checaPermissao($Pedido->ParceiroId, $request->getClientIp(), $permissoes)) {
                throw new \Exception("Permissão negada para o ParceiroId {$Pedido->ParceiroId} no IP {$request->getClientIp()}");
            }

            if (!PermissaoHelper::checaPermissaoInterface($Pedido->ParceiroId, 2)) {
                throw new \Exception("Sem permissão na interface de pedidos");
            }

            $Pedido->ParceiroIp = $request->getClientIp();
            $Pedido->ParceiroSiteId = $permissoes[0]->parceiroSiteId;
            $Pedido->SiteBase = $permissoes[0]->siteBase;

            $pedidoNovo = PedidoModel::createFromLegado($Pedido);
            $pedidoBO->setPedido($pedidoNovo);

            if ($Pedido->Pagamento[0]->MeioPagamentoId == 2) {
                $jsonBoleto = file_get_contents("http://gateway.maquinadevendas.com.br/Integracao/ImportarIntegracaoParceiro/" . $Pedido->ParceiroPedidoId . "/" . $Pedido->SiteId);
                $dadosBoleto = json_decode($jsonBoleto);
                $dadosOut->Msg = "Pedido importado com sucesso <url_boleto>{$dadosBoleto->Url}</url_boleto>";
            }
        } catch (DatabaseException $dbE) {
            $dadosOut->Codigo = 999;
            $dadosOut->Msg = 'Erro ao comunicar com o Sistema MV. Entre em contato com o administrador do sistema';
        } catch (ValidacaoException $e) {
            $dadosOut->Codigo = 20;
            $dadosOut->Msg = sprintf('Erro de Validacao - Entrar em contato com o Administrador da integracao MV - %s', $e->getMessage());
        } catch (\Exception $e) {
            switch ($e->getCode()) {
                case 0:
                    $codigo = 20;
                    break;
                case 50:
                    $codigo = 17;
                    break;
                case 51:
                    $codigo = 17;
                    break;
                case 2:
                    $codigo = 2;
                    break;
                case 3:
                    $codigo = 1;
                    break;
                default:
                    $codigo = $e->getCode();
                    break;
            }
            if ($codigo != 0) {
                $dadosOut->Codigo = $codigo;
                $dadosOut->Msg = $e->getMessage();
            } else {
                $dadosOut->Codigo = 999;
                $dadosOut->Msg = 'Erro ao comunicar com o Sistema MV. Entre em contato com o administrador do sistema';
            }
        }

        return $dadosOut;
    }
}