<?php

namespace App\Soap;

use Symfony\Component\HttpFoundation\Request;
use App\Model\PedidoConfirmacaoMsgOut;
use App\Helper\Validacao;
use App\Helper\PermissaoHelper;
use App\BO\InterfaceConfirmacaoBO;
use App\BO\ParceiroSiteBO;

/**
 * Class InterfaceConfirmacao
 * @package App\Soap
 */
class InterfaceConfirmacao {
    /** @var int */
    public $InterfaceTipo;

    /** @var int */
    public $ParceiroId;

    /** @var int */
    public $Site;

    /** @var int */
    public $Confirmacao;
    
    /**
    * Titulo do metodo
    * Descricao do metodo
    * @param \App\Soap\InterfaceConfirmacao $InterfaceConfirmacao
    * @return \App\Model\PedidoConfirmacaoMsgOut
    */
    public function setInterfaceConfirmacao($InterfaceConfirmacao) {
        try {
            $pedidoConfirmacaoMsgOut = new PedidoConfirmacaoMsgOut;
            $request = Request::createFromGlobals();
            $condicoes = array(
                'ParceiroId' => array('notempty', 'int'),
                'Site' => array('notempty', 'int'),
                'InterfaceTipo' => array('notempty', 'int'),
                'Confirmacao' => array('notempty', 'int')
            );

            $validacao = new Validacao($condicoes);

            $parceiroId = $InterfaceConfirmacao->ParceiroId;
            if (!$parceiroId) {
                $pedidoConfirmacaoMsgOut->Codigo = 20;
                $pedidoConfirmacaoMsgOut->getMsg();
                $pedidoConfirmacaoMsgOut->Msg .= " - O campo [ParceiroId] nao deve ser vazio.";
                return $pedidoConfirmacaoMsgOut;
            }

            $ip = $request->getClientIp();

            if (!PermissaoHelper::checaPermissao($parceiroId, $ip)) {
                $pedidoConfirmacaoMsgOut->Codigo = 20;
                $pedidoConfirmacaoMsgOut->getMsg();
                return $pedidoConfirmacaoMsgOut;
            }

            $interfaceTipo = $InterfaceConfirmacao->InterfaceTipo;

            if (!$interfaceTipo) {
                $pedidoConfirmacaoMsgOut->Codigo = 20;
                $pedidoConfirmacaoMsgOut->getMsg();
                $pedidoConfirmacaoMsgOut->Msg .= " - O campo [InterfaceTipo] nao deve ser vazio.";
                return $pedidoConfirmacaoMsgOut;
            }

            if (!PermissaoHelper::checaPermissaoInterface($parceiroId, $interfaceTipo)) {
                $pedidoConfirmacaoMsgOut->Codigo = 3;
                $pedidoConfirmacaoMsgOut->getMsg();
                return $pedidoConfirmacaoMsgOut;
            }

            if (!$validacao->executaValidacao($InterfaceConfirmacao)) {
                $pedidoConfirmacaoMsgOut->Codigo = 20;
                $pedidoConfirmacaoMsgOut->getMsg();
                $pedidoConfirmacaoMsgOut->Msg .= $validacao->msg;
                return $pedidoConfirmacaoMsgOut;
            }

            //Pega o parceiro site
            $parceiroSiteBO = new ParceiroSiteBO();
            $parceiroSite = $parceiroSiteBO->getParceiroSiteById($parceiroId);
            $siteId = $parceiroSite->siteId;

            if ($InterfaceConfirmacao->Site != $siteId) {
                $pedidoConfirmacaoMsgOut->Codigo = 20;
                $pedidoConfirmacaoMsgOut->Msg = "Erro: O [Site] informado e invalido para o [ParceiroId] fornecido";
                return $pedidoConfirmacaoMsgOut;
            }

            //Retorna todos os valores de InterfaceId a serem atualizados
            $interfaceConfirmacaoBO = new InterfaceConfirmacaoBO;
            $result = $interfaceConfirmacaoBO->getInterfaceConfirmacao($parceiroId, $siteId, $interfaceTipo);

            // Se existir interfaces a serem atualizadas
            if (count($result) > 0) {
                $interfaces = array();

                //Concatena as IDS
                foreach ($result as $interface) {
                    $interfaces[] = $interface->InterfaceId;
                }

                $tipo = $InterfaceConfirmacao->Confirmacao;
                if ($interfaceConfirmacaoBO->updateInterfaceConfirmacao($interfaces, $tipo) === false) {
                    $pedidoConfirmacaoMsgOut->Codigo = 4;
                } else {
                    $pedidoConfirmacaoMsgOut->Codigo = 0;
                }

            } else {
                // Erro de confirmação da interface
                $pedidoConfirmacaoMsgOut->Codigo = 2;
            }
        } catch (\Exception $e) {
            $pedidoConfirmacaoMsgOut->Codigo = 20;
        }

        $pedidoConfirmacaoMsgOut->getMsg();
        return $pedidoConfirmacaoMsgOut;
    }
}