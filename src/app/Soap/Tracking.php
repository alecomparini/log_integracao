<?php

namespace App\Soap;
use App\BO\TrackingBO;
use App\Model\TrackingOut;

/**
 * Class Tracking
 * @package App\Soap
 */
class Tracking {
    /**
     * Retorna uma consulta de tracking
     * @param array $pedidoId
     * @param integer $parceiroId
     * @param integer $siteId
     * @return \App\Model\TrackingOut retorno do webservice
     */
    public function getTracking(array $pedidoId, $parceiroId, $siteId) {
        $trackingOut = new TrackingOut;
        $trackingBO = new TrackingBO;
        
        try {
            $trackings = $trackingBO->consulta($pedidoId, $parceiroId, $siteId);
            $trackingOut->getMsg(0);
            $trackingOut->dados = json_encode($trackings);
        } catch (\Exception $e) {
            $trackingOut->getMsg($e->getCode());
            $trackingOut->dados = null;
        }

        return $trackingOut;
    }
}