<?php

namespace App\Soap;

use App\BO\ParceiroSiteBO;
use Symfony\Component\HttpFoundation\Request;
use App\Model\DadosOut;
use App\Helper\PermissaoHelper;
use App\Helper\Validacao;
use App\BO\InterfacesBO;
use App\Model\Interfaces;

/**
 * Class Dados
 * @package App\Soap
 */
class Dados {

    /**
     * getDados
     * Retorno dos Dados solicitados
     * @param \App\Model\Interfaces
     * @return \App\Model\DadosOut
     */
    public function getDados($interfaces) {
        $dadosOut = new DadosOut();

        try {
            //Cria a variavel de request do symfony
            $request = Request::createFromGlobals();

            $regras = array(
                "ParceiroId" => array("required", "notempty", "int"),
                "SiteId" => array("required", "notempty", "int"),
                "Interface" => array("required", "notempty", "int")
            );

            $validacao = new Validacao($regras);
            if (!$validacao->executaValidacao($interfaces)) {
                $dadosOut->Codigo = 1;
                $dadosOut->Msg = $validacao->msg;
                return $dadosOut;
            }

            //Verifica parceiro site
            $parceiroSiteBO = new ParceiroSiteBO();
            $parceiroSite = $parceiroSiteBO->getParceiroSite($interfaces->ParceiroId, $interfaces->SiteId);
            if (is_null($parceiroSite)) {
                throw new \Exception("Erro: Cadastro do parceiro incompleto", 1);
            }

            // Valida permissão IP
            if (!PermissaoHelper::checaPermissao($interfaces->ParceiroId, $request->getClientIp())) {
                $dadosOut->Codigo = 2;
                $dadosOut->Msg = "Permissão negada para o IP " . $request->getClientIp();
                return $dadosOut;
            }

            // Valida permissão Interface
            if (!PermissaoHelper::checaPermissaoInterface($interfaces->ParceiroId, $interfaces->Interface)) {
                $dadosOut->Codigo = 3;
                $dadosOut->Msg = "Sem permissão na Interface " . $interfaces->Interface;
                return $dadosOut;
            }
            $interfacesBO = new InterfacesBO();
            $estoque = $interfacesBO->getDados($interfaces->ParceiroId,
                $interfaces->SiteId,
                $interfaces->Interface);

            if (count($estoque) === 0) {
                $dadosOut->Codigo = 2;
                $dadosOut->Msg = "Erro: Dados sem alteração";
            } else {
                $arrayDadosOut = array();

                foreach ($estoque as $item) {
                    $arrayDadosOut[] = $item->dados;
                }

                $dadosOut->Codigo = 0;
                $dadosOut->Msg = "Sucesso";
                $dadosOut->Dados = json_encode($arrayDadosOut);
            }
        } catch (\Exception $e) {
            $dadosOut->Codigo = $e->getCode();
            $dadosOut->Msg = $e->getMessage();
        }
        return $dadosOut;
    }
}