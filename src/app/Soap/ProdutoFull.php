<?php

namespace App\Soap;

use App\BO\ParceiroSiteBO;

use App\Exceptions\ValidacaoException;
use App\Helper\Utils;
use App\Model\Produto;
use Symfony\Component\HttpFoundation\Request;
use App\Helper\Validacao;
use App\Helper\PermissaoHelper;
use App\Model\DadosOut;
use App\BO\ProdutoBO;

/**
 * Class ProdutoFull
 * @package App\Soap
 */
class ProdutoFull {

    /**
     * getProdutoFull
     * Recupera um produto
     *
     * @author Cristiano Gomes <cristianogomes@softbox.com.br>
     * @param int ParceiroId
     * @param string[] ProdutoCodigo
     * @return \App\Model\DadosOut
     */
    public function getProdutoFull($ParceiroId, array $ProdutoCodigo = array()) {

        try {
            $dadosOut = new DadosOut();
            $dadosOut->Codigo = 200;
            $request = Request::createFromGlobals();
            $clientIp = $request->getClientIp();
            $permissao = PermissaoHelper::checaPermissao($ParceiroId, $clientIp);

            if (count($ProdutoCodigo) == 0) {
                throw new \Exception('Campo [ProdutoCodigo] nao deve ser vazio', 15);
            }

            if (!$permissao) {
                $dadosOut->Codigo = 3;
                $dadosOut->Msg = sprintf("Permissão negada para o parceiro %d - IP: %s", $ParceiroId, $clientIp);
                return $dadosOut;
            }

            $regras = array(
                'parceiroId' => array('int', 'required', 'notempty'),
                'produtoCodigo' => array('array', 'required', 'notempty')
            );

            $obj = (object)array(
                'parceiroId' => $ParceiroId,
                'produtoCodigo' => $ProdutoCodigo
            );

            $validacao = new Validacao($regras);
            if (!$validacao->executaValidacao($obj)) {
                $dadosOut->Codigo = 1;
                $dadosOut->Msg = $validacao->msg;
                return $dadosOut;
            }

            if (count($ProdutoCodigo) > Produto::QTD_MAX_PRODUTO_CONSULTA) {
                $dadosOut->Codigo = 14;
                $dadosOut->Msg = sprintf('Erro: Quantidade máxima de produtos ( %s ) excedido', Produto::QTD_MAX_PRODUTO_CONSULTA);
                return $dadosOut;
            }

            $parceiroSiteBO = new ParceiroSiteBO();
            $parceiro = $parceiroSiteBO->getParceiroSiteById($ParceiroId);
            $siteId = $parceiro->siteBase;

            //continuando a brincadeira
            $produtoBO = new ProdutoBO();
            $result = $produtoBO->getProdutoFull($siteId, $ProdutoCodigo, null, $ParceiroId);

            if (is_array($result) && isset($result['produtos']) && count($result['produtos']) > 0) {
                $fatoresCartao = $produtoBO->getFatoresCartao();
                $result = $result['produtos'];
                foreach ($result as &$produto) {
                    $produto = $produto->preparaProdutoFull($fatoresCartao);
                }
            }

            $result = Utils::ucfirstRecursivo($result);

            foreach ($result as &$produto) {
                $produto->Medias->jpg = $produto->Medias->Jpg;
                unset($produto->Medias->Jpg);
            }

            if (count($result) > 0) {
                $dadosOut->Codigo = 0;
                $dadosOut->Msg = 'Sucesso';
                $dadosOut->Dados = Utils::jsonEncode($result);
            } else {
                $dadosOut->Codigo = 10;
                $dadosOut->Msg = 'Erro: Produto não encontrado';
            }
        } catch (ValidacaoException $e) {
            $dadosOut->Codigo = 12;
            $dadosOut->Msg = $e->getMessage();
        } catch (\Exception $e) {
            $dadosOut->Codigo = $e->getCode();
            $dadosOut->Msg = $e->getMessage();
        }

        return $dadosOut;
    }

    public function getArrayColumn($dados, $value) {
        $resultado = array();

        foreach ($dados as $item) {
            $item = (array)$item;
            if (isset($item[$value])) {
                $resultado[] = $item[$value];
            }
        }

        return $resultado;
    }
}
