<?php

namespace App\Soap;

use App\BO\VendedorBO;
use App\Core\Cache;
use App\DAO\ParceiroSiteDAO;
use App\Exceptions\BusinessException;
use App\Helper\Utils;
use App\BO\ProdutoSiteBO;
use App\Model\DadosOut;
use Symfony\Component\HttpFoundation\Request;
use App\Helper\PermissaoHelper;
use App\BO\ProdutoBO;


/**
 * Classe que realiza consultas
 */
class Consulta {

    /**
     * getEstoque
     * Retorna as informacoes de estoque
     * @param int $ParceiroId
     * @param string $ProdutoCodigo
     * @return \App\Model\DadosOut
     */
    public function getEstoque($ParceiroId, $ProdutoCodigo = null) {
        $dadosOut = new DadosOut();

        //Cria a variavel de request do symfony
        $request = Request::createFromGlobals();

        if ($ProdutoCodigo == '') {
            $ProdutoCodigo = null;
        }

        try {
            if (!PermissaoHelper::checaPermissao($ParceiroId, $request->getClientIp())) {
                throw new \Exception("Permissão negada.");
            }

            // Implementação manual de cache pois está muito lento
            $cacheKey = Cache::generateKey('Estoque_' . $ProdutoCodigo);
            $estoques = Cache::get($cacheKey);

            if (!$estoques) {
                $produtoBO = new ProdutoBO();
                $estoques = $produtoBO->getEstoques($ParceiroId, $ProdutoCodigo);

                if (count($estoques) > 0) {
                    $dadosOut->Codigo = 0;
                    $dadosOut->Msg = "Sucesso";
                    $dadosOut->Dados = Utils::jsonEncode($estoques);

                } else {
                    $dadosOut->Codigo = 6;
                    $dadosOut->Msg = "Erro: Nao foi possivel consultar o Estoque";
                }

                // Estoque full apenas 1x por dia
                Cache::set($cacheKey, $estoques, 86400);
            }
        } catch (\Exception $e) {
            $dadosOut->Codigo = 3;
            $dadosOut->Msg = $e->getMessage();
            $dadosOut->Dados = null;
        }

        return $dadosOut;
    }

    /**
     * getFrete
     * Retorna as informacoes de frete
     * @param int $ParceiroId
     * @param string $Cep
     * @param \App\Model\Item[] $Produtos
     * @return \App\Model\DadosOut
     */
    public function getFrete($ParceiroId, $Cep, $Produtos) {
        //Cria a variavel de request do symfony
        $request = Request::createFromGlobals();
        $dadosOut = new DadosOut();

        try {
            if (!PermissaoHelper::checaPermissao($ParceiroId, $request->getClientIp())) {
                throw new \Exception("Permissão negada.");
            }

            $produtoSiteBO = new ProdutoSiteBO;

            $frete = Utils::ucfirstRecursivo($produtoSiteBO->getFrete($ParceiroId, $Cep, $Produtos));
            $dadosOut->Codigo = 0;
            $dadosOut->Msg = "Sucesso";
            $dadosOut->Dados = Utils::jsonEncode($frete);
        } catch (\Exception $e) {
            switch ($e->getCode()) {
                case 6:
                    $codigo = 6;
                    break;
                case 0:
                    $codigo = 3;
                    break;
                default:
                    $codigo = $e->getCode();
            }

            $dadosOut->Codigo = $codigo;

            $dadosOut->Msg = $e->getMessage();
            $dadosOut->Dados = Utils::jsonEncode(array());
        }

        return $dadosOut;
    }

    /**
     * getProduto
     * Retorna as informacoes de produtos
     * @param int $ParceiroId
     * @param int $SiteId
     * @param string $ProdutoCodigo
     * @return \App\Model\DadosOut
     * @throws \Exception
     */
    public function getProduto($ParceiroId, $SiteId, $ProdutoCodigo = null) {
        $dadosOut = new DadosOut();

        try {
            $request = Request::createFromGlobals();

            if (!PermissaoHelper::checaPermissao($ParceiroId, $request->getClientIp())) {
                throw new \Exception("Permissão negada para o parceiro {$ParceiroId} no ip {$request->getClientIp()}.", 3);
            }

            if (!$SiteId) {
                $dadosOut->Codigo = 3;
                $dadosOut->Msg = 'Erro: O campo [SiteId] e obrigatorio';
                return $dadosOut;
            }

            $parceiroSiteDao = new ParceiroSiteDAO();
            $parceiroInfo = $parceiroSiteDao->getParceiroSiteById($ParceiroId);
            if ($SiteId != $parceiroInfo->siteId) {
                $dadosOut->Codigo = 3;
                $dadosOut->Msg = 'Erro: O [SiteId] informado e invalido para o [ParceiroId] fornecido';
                return $dadosOut;
            }

            $produtoBO = new ProdutoBO();

            try {
                $produtos = $produtoBO->getProduto($ParceiroId, $SiteId, $ProdutoCodigo);

                $result = Utils::ucfirstRecursivo($produtos);

                if (count($result) > 0) {
                    $dadosOut->Codigo = 0;
                    $dadosOut->Msg = 'Sucesso';
                    $dadosOut->Dados = Utils::jsonEncode($result);
                } else {
                    $dadosOut->Codigo = 7;
                    $dadosOut->Msg = 'Erro: Nao foi possivel consultar o Produto';
                }
            } catch (\Exception $e) {
                $dadosOut->Codigo = 7;
                $dadosOut->Msg = 'Erro: Nao foi possivel consultar o Produto';
                return $dadosOut;
            }

        } catch (\Exception $e) {
            if ($e->getCode() == 7) {
                $codigo = 7;
            } else {
                $codigo = 3;
            }
            $dadosOut->Codigo = $codigo;
            $dadosOut->Msg = $e->getMessage();
        }

        return $dadosOut;
    }

    /**
     * getVendedor
     * Retorna as informacoes do Vendedor
     * @param int
     * @param string
     * @param string
     * @return \App\Model\DadosOut
     */
    public function getVendedor($ParceiroId, $Email, $Senha) {
        //Cria a variavel de request do symfony
        $request = Request::createFromGlobals();

        $dadosOut = new DadosOut();

        try {

            // ParceiroId não informado
            if (empty($ParceiroId)) {
                throw new \Exception("Erro: Cliente sem permissao para a Interface.", 3);
            }

            if (!PermissaoHelper::checaPermissao($ParceiroId, $request->getClientIp())) {
                throw new \Exception("Permissão negada.", 3);
            }

            $vendedorBO = new VendedorBO;

            // Email não informado
            if (empty($Email)) {
                throw new \Exception("Erro: Vendedor nao encontrado.", 9);
            }

            // Senha não informada
            if (empty($Senha)) {
                throw new \Exception("Erro: Senha do vendedor nao confere.", 8);
            }

            $getVendedor = $vendedorBO->getVendedor($Email, $Senha);

            if (empty($getVendedor)) {
                throw new \Exception("Erro: Vendedor nao encontrado.", 9);
            }

            // Senha inválida
            if ($getVendedor->senha !== Utils::criptSenhaB2C($Senha)) {
                throw new \Exception("Erro: Senha do vendedor nao confere.", 8);
            }

            $vendedor = Utils::ucfirstRecursivo($getVendedor->jsonSerialize());

            $dadosOut->Codigo = 0;
            $dadosOut->Msg = "Sucesso";
            $dadosOut->Dados = Utils::jsonEncode(array($vendedor));
        } catch (\Exception $e) {
            $dadosOut->Codigo = ($e->getCode() == 2) ? 9 : $e->getCode();
            $dadosOut->Msg = $e->getMessage();
            $dadosOut->Dados = Utils::jsonEncode(array());
        }

        return $dadosOut;
    }
}
