<?php
namespace App\Service;

use Exception;
use App\Helper\Utils;

class FileLockController {

    /**
     * Array que armazena os locks.
     * @var array
     */
    private $locks;

    /**
     * @var string
     */
    private $locksDir;


    /**
     * @var boolean
     */
    private $enableException;

    /**
     * @var boolean
     */
    private $reentrant;

    /**
     *
     * @param string $enableException - indica se deverá ser lançada exception em caso de não conseguir obter o lock
     * @param string $locksDir - endereço onde os locks deverão ser salvos
     */
    public function __construct($reentrant = false, $enableException = true, $locksDir = "") {
        $this->locks = array();

        if (empty($locksDir)) {
            $this->locksDir = Utils::getTempDir() . '/lock';
        } else {
            $this->locksDir = $locksDir;
        }
        if (!file_exists($this->locksDir)) {
            if (!mkdir($this->locksDir)) {
                throw new Exception("Não foi possível criar diretório de escrita de locks!");
            }
        }
        if (!is_dir($this->locksDir)) {
            throw new Exception("Não foi possível usa arquivo como diretório de escrita de locks!");
        }

        $this->reentrant = ($reentrant === true);

        $this->enableException = ($enableException !== false);
    }

    /**
     * @param string $lockName
     * @throws Exception
     * @return boolean
     */
    public function lock($lockName) {
        $lockFile = $this->locksDir . "/" . $lockName;

        $handle = fopen($lockFile, "w");

        if (!$handle) {
            throw new Exception("Não foi possível abrir o arquivo para lock!");
        }

        if (!flock($handle, LOCK_EX | LOCK_NB)) {
            if ($this->reentrant && isset($this->locks[$lockName])){
                return true;
            }
            if ($this->enableException) {
                throw new Exception("Lock já obtido por outro processo!");
            }
            return false;
        }

        @fwrite($handle, (string)getmypid());

        $this->locks[$lockName] = $handle;

        return true;
    }

    public function release($lockName) {
        if (!isset($this->locks[$lockName])) {
            return;
        }
        $fid = $this->locks[$lockName];

        $this->doRelease($lockName, $fid);
    }

    public function releaseAll() {
        foreach ($this->locks as $lockName => $fid) {
            $this->doRelease($lockName, $fid);
        }
    }

    private function doRelease($lockName, $fid) {
        flock($fid, LOCK_UN);
        fclose($fid);
        @unlink($this->locksDir . '/' . $lockName);
        unset($this->locks[$lockName]);
    }

    public function readLock($lockName) {
        $lockFile = $this->locksDir . "/" . $lockName;
        return file_get_contents($lockFile);
    }

}