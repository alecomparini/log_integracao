<?php
namespace App\Service\Converter;

/**
 * Classe responsável por realizar o fix do tipos de retorno
 * antes de aplicar o jsonEncode e parar de sair tudo como string
 * Vários parceiros costumam ter problemas por causa de tipos inválidos
 * como por exemplo tipos que deveriam ser string mas retorna sem aspas
 * ou então tipos que deveria ser números mas vão com aspas.
 *
 * @package App\Service\Converter
 */
class ConversorSaidaV2 {
    private function convertToStr(&$property) {
        $property = (string)$property;
    }

    private function convertToInt(&$property) {
        if (is_numeric($property)) {
            $property = (int)$property;
        }
    }

    public function convertToFloat(&$property) {
        if (is_numeric($property)) {
            $property = (float)$property;
        }
    }

    public function convertTo($type, &$property) {
        $method = "convertTo" . ucfirst($type);
        if (method_exists($this, $method)) {
            $this->$method($property);
        }
    }

    /**
     * Método responsável por fazer o fix na consulta de produtos
     *
     * @param $produtos
     *
     * @return mixed
     */
    public function converteSaidaProduto($produtos) {
        $regras = array(
            'produtoId' => 'int',
            'fabricanteId' => 'int',
            'codigo' => 'string',
            'codigoRetail' => 'string',
            'vendeAvulso' => 'int',
            'emLinha' => 'int',
            'tipoTransporte' => 'int',
            'lojaId' => 'int',
            'precoPor' => 'float',
            'precoDe' => 'float',
            'categoriaId' => 'int',
            'subCategoriaId' => 'int',
            'status' => 'int',
            'descontoAVista' => 'float',
            'quantidade' => 'int',
            'parcelamentoMaximoSemJuros' => 'int',
            'altura' => 'float',
            'comprimento' => 'float',
            'largura' => 'float',
            'peso' => 'float',
            'alturaEmbalagem' => 'float',
            'comprimentoEmbalagem' => 'float',
            'larguraEmbalagem' => 'float',
            'pesoEmbalagem' => 'float',
            'disponivel' => 'int',
        );
        if (is_array($produtos)) {
            foreach ($produtos as $produto) {
                foreach ($regras as $campo => $regra) {
                    if (property_exists($produto, $campo)) {
                        $this->convertTo($regra, $produto->$campo);
                    }
                }

                if (array_key_exists('filhos', $produto)) {
                    $this->converteSaidaProduto($produto->filhos);
                }
            }
        }

        return $produtos;
    }

    /**
     * Método responsável por fazer o fix na consulta de estoque
     *
     * @param $estoques
     *
     * @return mixed
     */
    public function converteSaidaEstoque($estoques) {
        $regras = array(
            'Codigo' => 'str',
            'produtoId' => 'int',
            'disponivel' => 'int',
            'PrecoDe' => 'float',
            'PrecoPor' => 'float',
            'vendeAvulso' => 'int',
            'emLinha' => 'int',
            'LojaId' => 'int',
            'CategoriaId' => 'int',
            'SubCategoriaId' => 'int',
            'Status' => 'int'
        );
        if (is_array($estoques)) {
            foreach ($estoques as $estoque) {
                foreach ($regras as $campo => $regra) {
                    if (property_exists($estoque, $campo)) {
                        $this->convertTo($regra, $estoque->$campo);
                    }
                }
            }
        }
        return $estoques;
    }

    /**
     * Método responsável por fazer o fix na consulta de frete
     *
     * @param $frete
     *
     * @return mixed
     */
    public function converteSaidaFrete($frete) {
        return $frete;
    }

    /**
     * Método responsável por fazer o fix na consulta de pedido
     *
     * @param $pedido
     *
     * @return mixed
     */
    public function converteSaidaPedido($pedido) {
        return $pedido;
    }

    /**
     * Método responsável por fazer o fix na consulta de tracking
     *
     * @param $tracking
     *
     * @return mixed
     */
    public function converteSaidaTracking($tracking) {
        return $tracking;
    }

    /**
     * Método responsável por fazer o fix na consulta de parcelamento
     *
     * @param $parcelamento
     *
     * @return mixed
     */
    public function converteSaidaParcelamento($parcelamento) {
        return $parcelamento;
    }

    /**
     * Método responsável por fazer o fix na consulta de campanha
     *
     * @param $campanha
     *
     * @return mixed
     */
    public function converteSaidaCampanha($campanha) {
        return $campanha;
    }
}