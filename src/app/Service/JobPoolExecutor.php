<?php
namespace App\Service;

/**
 * Classe utilitaria para executar jobs de forma assincrona.
 *
 * Entende-se por job, qualquer servico que pode ser chamado via Rest pelo metodo GET.
 * @author Tarcisio A. de Magalhaes Jr. <tarcisio@softbox.com.br>
 * @createdAt: 27-04-2016 9:28
 * @package: App\Service
 */
class JobPoolExecutor {
    /** Tamanho do pool para envio de e-mail */
    private $poolSize;

    /** Tempo de espera para envio de e-mail  */
    private $timeLimit;

    /** Host do servidor. */
    private $host;

    /** Porta do servidor. */
    private $port;

    /** Usuário para conexão no ARC. */
    private $user;

    /** Senha para conexão no ARC. */
    private $pass;

    /** Respostas das chamadas efetuadas indexadas pelo ID do job. */
    private $responses = array();

    /** Gerador de ids automaticos. */
    private $idCount = 1;

    /** Fila de jobs que estao em execucao. */
    private $jobsRunning = array();

    /** Controla se será enviado json ou não */
    private $isJson = false;

    /** Controla o método que vai ser utilizado */
    private $httpMethod = 'GET';

    /** Fila de jobs que estao esperando para serem executados. */
    private $jobsWaiting = array();

    public function __construct($poolSize, $timeLimit, $host, $port, $user, $pass) {
        $this->poolSize = $poolSize;
        $this->timeLimit = $timeLimit;

        $this->host = $host == null ? $_SERVER["SERVER_NAME"] : $host;
        $this->port = $port == null ? $_SERVER["SERVER_PORT"] : $port;

        $this->user = $user;
        $this->pass = $pass;
    }

    /**
     * Adiciona um job para execucao.
     *
     * @param string $url Url que sera chamada (job).
     * @param array $params
     * @param string $id Identificador do job.
     *
     * @param bool|string $processUntilEnd Indica se deve-se esperar ate que todos os jobs tenham executado.
     * @param bool $waitForResponse
     */
    public function addJob($url, $params = null, $id = null, $processUntilEnd = false, $waitForResponse = true) {
        $id = $id == null ? $this->idCount++ : $id;

        // Coloca o job na fila de espera.
        $this->jobsWaiting[] = array(
            "id" => $id,
            "job" => false,
            "url" => $url,
            "params" => $params,
        );

        // Registra a resposta com vazia
        $this->responses[$id] = array(
            // URL.
            "url" => $url,

            // Retorno da chamada.
            "ret" => ""
        );

        // Manda executar o job pendente.
        $this->processJobs($processUntilEnd, $waitForResponse);
    }

    /**
     * Processa todos os jobs e aguarda ate todos finalizarem.
     * @param bool $waitForResponse
     */
    public function processAll($waitForResponse = true) {
        $this->processJobs(true, $waitForResponse);
    }

    /**
     * Retorna a resposta de todos os jobs.
     *
     * @return array Do tipo chave => valor, onde chave e o id do item e valor a resposta do job.
     */
    public function getAllResponses() {
        $ret = array();

        foreach ($this->responses as $id => $resp) {
            $ret[$id] = $this->getResponse($id);
        }

        return $ret;
    }

    /**
     * Obtem a resposta do job registrado.
     *
     * @param string $id Identificador do job.
     *
     * @return NULL|string String retornada pela chamada do servico.
     */
    public function getResponse($id) {
        $resp = isset($this->responses[$id]) ? $this->responses[$id] : "";

        if (empty($resp)) {
            return null;
        }

        $tokens = explode("\n", $resp["ret"]);

        $chunked = false;

        // Removendo as informacoes de cabecalho.
        while (($cfg = trim(array_shift($tokens), "\r")) != "") {

            if (stripos($cfg, "Transfer-Encoding") !== false && stripos($cfg, "chunked") !== false) {
                $chunked = true;
            }
        }

        $ret = implode("\n", $tokens);

        return $chunked ? $this->decodeChunked($ret) : $ret;
    }

    /**
     * @return mixed
     */
    public function isJson() {
        return $this->isJson;
    }

    /**
     * @param mixed $isJson
     */
    public function setIsJson($isJson) {
        $this->isJson = $isJson;
    }

    /**
     * @return mixed
     */
    public function getHttpMethod() {
        return $this->httpMethod;
    }

    /**
     * @param mixed $httpMethod
     */
    public function setHttpMethod($httpMethod) {
        $this->httpMethod = $httpMethod;
    }

    /**
     * @param $str
     * @return string
     */
    private function decodeChunked($str) {
        for ($res = ''; !empty($str); $str = trim($str)) {
            $pos = strpos($str, "\r\n");
            $len = hexdec(substr($str, 0, $pos));
            $res .= substr($str, $pos + 2, $len);
            $str = substr($str, $pos + 2 + $len);
        }
        return $res;
    }

    /**
     * Processa os job adicionados.
     *
     * @param booelan|bool $untilEnd Aguarda até que o ultimo job seja concluido.
     * @param bool $waitForResponse
     */
    private function processJobs($untilEnd = false, $waitForResponse = true) {
        do {
            while (count($this->jobsRunning) < $this->poolSize && count($this->jobsWaiting) > 0) {
                // Remove o job da fila de espera
                $jobInfo = array_shift($this->jobsWaiting);

                // Inicia o job
                $job = $this->jobStartAsync($this->host, $jobInfo["url"], $jobInfo['params'], $this->port);

                if ($job !== false) {
                    // Coloca o job na fila de executando.
                    $jobInfo["job"] = $job;

                    $this->jobsRunning[] = $jobInfo;
                }
            }

            if ($untilEnd) {
                sleep($this->timeLimit);
            }

            // Verificando se os jobs foram encerrados.
            foreach ($this->jobsRunning as $i => &$infoJob) {
                if ($this->jobPollAsync($infoJob["job"], $infoJob["id"], $infoJob["url"]) === false) {

                    // Se finalizou o socket, remove o job da fila de execucao.
                    unset($this->jobsRunning[$i]);
                }
            }

        } while ($untilEnd && (($waitForResponse && count($this->jobsRunning) > 0) || count($this->jobsWaiting) > 0));
    }

    /**
     * Executa um GET na url fornecida de forma assincrona.
     *
     * @param string $server Host.
     * @param string $url URL que sera chamada.
     * @param $params
     * @param int|number $port Porta do servidor.
     * @param int|number $connTimeout Timeout de conexao.
     * @param int|number $rwTimeout Timeout de escrita e leitura.
     * @return bool|resource Resource, em caso de erro retorna-se false.
     */
    private function jobStartAsync($server, $url, $params = null, $port = 80, $connTimeout = 30, $rwTimeout = 120) {
        $errno = "";
        $errstr = "";

        set_time_limit(0);

        $fp = @fsockopen($server, $port, $errno, $errstr, $connTimeout);

        if (!$fp) {
            return false;
        }

        $contentType = "application/x-www-form-urlencoded";
        if ($this->isJson()) {
            $contentType = "application/json";
        }
        $payload = "";
        if (!is_null($params)) {
            if (is_string($params)) {
                $payload = $params;
            } else {
                if ($this->isJson()) {
                    $payload = json_encode($params);
                } else {
                    $payload = http_build_query($params);
                }
            }
        }

        if ($this->getHttpMethod() == 'GET') {
            $url .= "?" . $payload;
        }

        $out = "$this->httpMethod $url HTTP/1.1\r\n";
        $out .= "Host: $server\r\n";
        $out .= "Content-Type: $contentType\r\n";
        if (strlen($payload) > 0 && $this->getHttpMethod() != 'GET') {
            $out .= "Content-length: " . strlen($payload) . "\r\n";
        }
        $out .= "Authentication: Basic " . base64_encode($this->user . ":" . $this->pass) . "\r\n";
        $out .= "Connection: Close\r\n\r\n";

        if (!is_null($params) && $this->getHttpMethod() != 'GET') {
            $out .= $payload . "\r\n\r\n";
        }

        stream_set_blocking($fp, false);
        stream_set_timeout($fp, $rwTimeout);

        fwrite($fp, $out);

        return $fp;
    }

    /**
     * Realiza a leitura dos dados do socket fornecido.
     *
     * @param resource $fp Ponteiro para o socket.
     *
     * @param $id
     * @param $url
     * @return bool|string Valor lido do socket ou false caso o socket tenha retornado EOF.
     */
    private function jobPollAsync(&$fp, $id, $url) {
        if ($fp === false) {
            return false;
        }

        if (feof($fp)) {
            fclose($fp);

            $fp = false;
            return false;
        }

        $buffer = fread($fp, 1024);

        // Concatena a reposta a partir do id.
        $this->responses[$id]["ret"] .= $buffer;
        return $buffer;
    }

}