<?php
//Busca fatores (taxas) de uma bandeira de cartão
return "SELECT FatorCartaoId, BandeiraId, Parcela, Fator
        FROM " . DB_MAGAZINE . ".FatorCartao
        WHERE BandeiraId = :bandeiraId
        ORDER BY Parcela ASC;";