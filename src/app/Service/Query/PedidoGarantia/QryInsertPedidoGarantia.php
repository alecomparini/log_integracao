<?php

$sql = "INSERT INTO
  " . DB_INTEGRACAO . ".Pedido_Garantia (
      DadosImportacaoId,
      GarantiaCodigo,
      PedidoProdutoId,
      Prazo,
      Quantidade,
      ValorVenda)
  VALUES (
    :dadosImportacaoID,
    :garantiaCodigo,
    :pedidoProdutoId,
    :prazo,
    :quantidade,
    :valorVenda
)";

return $sql;