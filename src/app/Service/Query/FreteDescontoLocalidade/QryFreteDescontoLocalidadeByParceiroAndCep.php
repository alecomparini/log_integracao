<?php

return "SELECT 
            FD.FreteDescontoId,
            FD.ParceiroId,
            FD.TipoFrete,
            FD.Desconto,
            FL.FreteDescontoLocalidadeId,
            FL.Localidade,
            MIN(FL.ValorFrete) AS ValorFrete
        FROM ".DB_INTEGRACAO.".FreteDesconto FD
        LEFT JOIN
            (".DB_INTEGRACAO.".FreteDesconto_Localidade FL 
                JOIN ".DB_MAGAZINE.".Localidade LO ON (LO.Sigla = FL.Localidade AND (CepInicio <= :cep AND CepFim >= :cep))        
            ) ON FD.FreteDescontoId = FL.FreteDescontoId
        WHERE FD.ParceiroId = :parceiroId
         GROUP BY FD.FreteDescontoId";

