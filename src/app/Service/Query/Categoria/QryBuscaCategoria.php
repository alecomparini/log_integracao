<?php

$qry = "
        SELECT 
            CA.CategoriaId as categoriaId,
            CA.nome as categoria,
            LJ.CategoriaId as lojaId,
            LJ.nome as loja
          FROM " . DB_MAGAZINE . ".Categoria as CA
          JOIN " . DB_MAGAZINE . ".Categoria as LJ on CA.CategoriaAsc = LJ.CategoriaId
        WHERE
            CA.CategoriaId IN :categorias
       ";

return $qry;