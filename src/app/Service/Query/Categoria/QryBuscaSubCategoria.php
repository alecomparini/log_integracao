<?php

$qry = "
        SELECT 
            SB.CategoriaId as subCategoriaId,
            SB.nome as subCategoria,
            CA.CategoriaId as categoriaId,
            CA.nome as categoria,
            LJ.CategoriaId as lojaId,
            LJ.nome as loja
          FROM " . DB_MAGAZINE . ".Categoria as SB
          JOIN " . DB_MAGAZINE . ".Categoria as CA ON CA.CategoriaId  = SB.CategoriaAsc
          JOIN " . DB_MAGAZINE . ".Categoria as LJ on CA.CategoriaAsc = LJ.CategoriaId
        WHERE
            SB.CategoriaId IN :categorias
       ";

return $qry;