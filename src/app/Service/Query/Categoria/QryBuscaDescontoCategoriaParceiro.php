<?php
/**
 * @author: William Okano <williamokano@gmail.com>
 * @createdAt: 25-04-2016 11:13
 */

$whereA = "";

if (isset($params['parceirosId'])) {
    $whereA .= " AND cdp.ParceiroId = :parceirosId";
}

if (isset($params['categoriasId']) && !empty($params['categoriasId'])) {
    $whereA .= " AND CategoriaId IN :categoriasId";
}

$sql = "SELECT
          cdp.CategoriaDescontoParceiroId,
          cdp.CategoriaId,
          cdp.ParceiroId,
          cdp.Desconto,
          cdp.TipoDesconto
        FROM " . DB_INTEGRACAO . ".CategoriaDescontoParceiro cdp
        WHERE 1=1 {$whereA}";

return $sql;