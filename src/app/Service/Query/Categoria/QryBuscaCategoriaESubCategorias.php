<?php

$categorias = implode(',', $params['categoriaId']);

$sql = "select
          c.CategoriaId,
          c.CategoriaAsc,
          c.Nome,
          c.Nivel,
          c.MetaEstoque,
          c.VendeAvulso,
          c.VariacoesTitulo,
          c.VariacoesDescricao,
          c.Tipo,
          c.Url,
          c.NovaAba
        from " . DB_MAGAZINE . ".Categoria c
        where c.CategoriaId IN :categoriaId
        
        union
        
        select
          categorias_ordenadas.CategoriaId,
          categorias_ordenadas.CategoriaAsc,
          categorias_ordenadas.Nome,
          categorias_ordenadas.Nivel,
          categorias_ordenadas.MetaEstoque,
          categorias_ordenadas.VendeAvulso,
          categorias_ordenadas.VariacoesTitulo,
          categorias_ordenadas.VariacoesDescricao,
          categorias_ordenadas.Tipo,
          categorias_ordenadas.Url,
          categorias_ordenadas.NovaAba
        from (
              select *
              from " . DB_MAGAZINE . ".Categoria
              order by CategoriaAsc, CategoriaId
             ) categorias_ordenadas,
             (
              select @pv := '{$categorias}'
             ) init
             where find_in_set(CategoriaAsc, @pv) > 0
        and @pv := concat(@pv, ',', CategoriaId)";

return $sql;