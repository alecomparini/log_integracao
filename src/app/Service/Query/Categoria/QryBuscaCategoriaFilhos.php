<?php
//Busca Categorias dos produtos filhos de produto(s) pai em um site
$sql = "SELECT *
        FROM " . DB_MAGAZINE . ".Categoria c
          INNER JOIN " . DB_MAGAZINE . ".Categoria_Site cs ON cs.CategoriaId = c.CategoriaId
        WHERE c.CategoriaAsc IN :idCategoriaPai AND cs.SiteId = :siteId;";

return $sql;