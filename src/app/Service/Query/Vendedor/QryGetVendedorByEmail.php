<?php

return "SELECT
          O.OperadorB2CId AS Id,
          O.Email,
          O.Senha,
          O.Nome,
          O.Tipo          AS TipoOperador,
          GB.Nome         AS GrupoNome,
          GB.GrupoB2CId   AS GrupoId
        FROM
          " . DB_MAGAZINE . ".OperadorB2C O INNER JOIN " . DB_MAGAZINE . ".GrupoB2C GB ON (O.GrupoB2CId = GB.GrupoB2CId)
        WHERE
          O.Status = 1 AND O.Email = :email;";