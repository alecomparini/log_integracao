<?php
$whereAnd = '';

if (isset($params['escopo'])) {
    $whereAnd = 'AND te.Escopo = :escopo';
}

$sql = "SELECT
          te.Escopo,
          te.TokenEscopoId,
          te.TokenId,
          t.ParceiroId,
          t.DataCriacao,
          t.DataUltimaUtilizacao,
          t.DataValidade,
          t.Token
        FROM " . DB_INTEGRACAO . ".TokenEscopo te
        INNER JOIN Token t ON te.TokenId = t.TokenId
        WHERE t.Token = :token {$whereAnd};";

return $sql;