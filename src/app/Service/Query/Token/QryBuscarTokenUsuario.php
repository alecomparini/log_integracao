<?php
return "
SELECT
    tu.TokenUsuarioId           AS tokenUsuarioId,
    tu.Token                    AS token,
    tu.UsuarioId                AS usuarioId,
    tu.DataValidade             AS dataValidade,
    tu.DataCriacao              AS dataCriacao
FROM        riel_integracao.TokenUsuario        AS tu
WHERE       tu.Token = :token
";