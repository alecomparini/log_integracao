<?php
return "
INSERT INTO " . DB_INTEGRACAO . ".TokenUsuario (
    Token,
    UsuarioId,
    DataValidade) VALUES (
    :token,
    :usuarioId,
    :dataValidade);
";