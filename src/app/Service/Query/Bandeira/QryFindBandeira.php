<?php
// Retorna a(s) bandeira(s) passadas por pelo paramentro :bandeiraId com Status = 1 (ativa)
return 'SELECT * FROM ' . DB_MAGAZINE . '.Bandeira WHERE BandeiraId IN :bandeiraId AND Status = 1';