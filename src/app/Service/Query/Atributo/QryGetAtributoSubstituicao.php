<?php
$sql = "SELECT
          A.Nome                                                                  AS atributoNome,
          AV.Valor                                                                AS valor,
          PAV.ProdutoId                                                           AS produtoIdReferencia,
          P2.ProdutoId                                                            AS produtoId,
          P2.Nome                                                                 AS nomeProduto,
          P2.Codigo                                                               AS codigo,
          PC.EAN                                                                  AS ean,
          IF(P2.ProdutoId = PAV.ProdutoId, 1, 0)                                  AS produtoOrigem,
          (IFNULL(E_Produto.Disponivel, 0) + IFNULL(MIN(E_Filhos.Disponivel), 0)) AS disponivel
        FROM         " . DB_MAGAZINE . ".Atributo AS A
          INNER JOIN " . DB_MAGAZINE . ".AtributoValor AS AV ON A.AtributoId = AV.AtributoId
          INNER JOIN " . DB_MAGAZINE . ".Produto_AtributoValor AS PAV ON AV.AtributoValorId = PAV.AtributoValorId
          INNER JOIN " . DB_MAGAZINE . ".Produto P ON P.ProdutoId = PAV.ProdutoId
          INNER JOIN " . DB_MAGAZINE . ".Produto_Site PS ON PS.ProdutoId = P.ProdutoId
          INNER JOIN " . DB_MAGAZINE . ".Produto P2 ON (PAV.Codigo = P2.Codigo)
          INNER JOIN " . DB_MAGAZINE . ".Produto_Site PS2 ON PS2.ProdutoId = P2.ProdutoId
          INNER JOIN " . DB_MAGAZINE . ".ProdutoCusto PC ON PC.Codigo = P2.Codigo

          LEFT JOIN " . DB_MAGAZINE . ".Combo Co ON (P2.ProdutoId = Co.ProdutoId AND Co.SiteId = :siteId)
          LEFT JOIN " . DB_MAGAZINE . ".Estoque E_Produto ON (P2.ProdutoId = E_Produto.ProdutoId)
          LEFT JOIN " . DB_MAGAZINE . ".Estoque E_Filhos ON (Co.ComboId = E_Filhos.ProdutoId)

        WHERE
          A.Status = 1
          AND AV.Status = 1
          AND PAV.Status = 1
          AND PAV.ProdutoId IN :produtosId
          AND PS.SiteId = :siteId
          AND PS.Status IN (1, 2)
          AND PS2.SiteId = :siteId
          AND PS2.Status IN (1, 2)
        GROUP BY P2.ProdutoId";

return $sql;
