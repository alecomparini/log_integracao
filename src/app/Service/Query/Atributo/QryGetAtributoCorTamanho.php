<?php

$sql = "SELECT DISTINCT
          c.Nome                               AS corNome,
          t.Tamanho                            AS tamanho,
          p.Codigo                             AS codigo,
          p.ProdutoId                          AS produtoId,
          ps.Status                            AS produtoStatus,
          e.Disponivel                         AS disponivel,
          IF(p.ProdutoId IN :produtosId, 1, 0) AS produtoOrigem
        FROM         " . DB_MAGAZINE . ".AtributoGrupo g
          INNER JOIN " . DB_MAGAZINE . ".AtributoGrupo_Produto gp ON (g.AtributoGrupoId = gp.AtributoGrupoId)
          INNER JOIN " . DB_MAGAZINE . ".Produto_Atributo pa ON (gp.ProdutoId = pa.ProdutoId)
          LEFT JOIN  " . DB_MAGAZINE . ".AtributoCor c ON (pa.AtributoCorId = c.AtributoCorId AND c.Status = 1)
          LEFT JOIN  " . DB_MAGAZINE . ".AtributoTamanho t ON (pa.AtributoTamanhoId = t.AtributoTamanhoId AND t.Status = 1)
          INNER JOIN " . DB_MAGAZINE . ".Produto p ON (pa.ProdutoId = p.ProdutoId)
          INNER JOIN " . DB_MAGAZINE . ".Produto_Site ps ON (ps.ProdutoId = p.ProdutoId AND ps.SiteId = 1)
          LEFT JOIN  " . DB_MAGAZINE . ".Estoque e ON (e.ProdutoId = p.ProdutoId)
        WHERE
          g.AtributoGrupoId IN (
            SELECT AtributoGrupoId
            FROM
              " . DB_MAGAZINE . ".AtributoGrupo_Produto
            WHERE
              ProdutoId IN :produtosId
          )
        GROUP BY c.AtributoCorId, t.AtributoTamanhoId
        ORDER BY
          c.Nome, t.Tamanho";

return $sql;