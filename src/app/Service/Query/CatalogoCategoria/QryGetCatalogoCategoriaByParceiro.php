<?php
$qry = "
    SELECT
        CC.CatalogoId,
        CC.ValorDesconto,
        CC.SubCategoriaId,
        CC.CategoriaId,
        CC.LojaId,
	C.Desconto as descontoCampanha
    FROM
	" . DB_INTEGRACAO . ".Catalogo_Categoria CC
    JOIN " . DB_INTEGRACAO . ".Campanha C ON C.CatalogoId = CC.CatalogoId
    JOIN " . DB_INTEGRACAO . ".Parceiro_Site_Permissao PSP ON PSP.Permissao = C.CampanhaId AND PSP.PermissaoTipoId = 4
    JOIN " . DB_INTEGRACAO . ".Parceiro_Site PS ON PS.ParceiroSiteId = PSP.ParceiroSiteId
    WHERE
        PS.ParceiroId = :parceiroId";

return $qry;

