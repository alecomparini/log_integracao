<?php
$qry = "
    SELECT
        CC.CatalogoId as catalogoId,
        CC.ValorDesconto as valorDesconto,
        CC.SubCategoriaId as subCategoriaId,
        CC.CategoriaId as categoriaId,
        CC.LojaId as lojaId
    FROM " . DB_INTEGRACAO . ".Catalogo_Categoria CC    
    WHERE
        CC.CatalogoId = :catalogoId";

return $qry;

