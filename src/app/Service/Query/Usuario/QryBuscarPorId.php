<?php
return "
SELECT
    UsuarioId       AS usuarioId,
    Email           AS email,
    Senha           AS senha,
    Status          AS status,
    DataCriacao     AS dataCriacao,
    ParceiroId      AS parceiroId
FROM
    Usuario
WHERE
    UsuarioId = :usuarioId
";