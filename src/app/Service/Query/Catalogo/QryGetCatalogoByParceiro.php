<?php

$qry = "
    SELECT    
	CA.CampanhaId,
	CA.Nome as campanha,
	CA.TipoFrete,
	CA.Desconto,
	CA.FretePonderado,
	CA.Status as statusCampanha,
        GROUP_CONCAT(CF.Localidade) as localidades,
	CO.CatalogoId,
	CO.Nome,
	CO.Status
    FROM " . DB_INTEGRACAO . ".Campanha CA
    JOIN " . DB_INTEGRACAO . ".Catalogo CO ON CO.CatalogoId = CA.CatalogoId
    JOIN " . DB_INTEGRACAO . ".Parceiro_Site_Permissao PSP ON PSP.Permissao = CA.CampanhaId AND PSP.PermissaoTipoId = 4    
    JOIN " . DB_INTEGRACAO . ".Parceiro_Site PS ON PS.ParceiroSiteId = PSP.ParceiroSiteId
    LEFT OUTER JOIN " . DB_INTEGRACAO . ".Campanha_Frete CF ON CF.CampanhaId = CA.CampanhaId 
    WHERE
        PS.ParceiroId = :parceiroId";

return $qry;