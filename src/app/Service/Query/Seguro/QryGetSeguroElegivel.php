<?php

$sql = "SELECT
          S.*
        FROM
          " . DB_MAGAZINE . ".SeguroTabela S
          LEFT JOIN " . DB_MAGAZINE . ".Categoria C ON S.CategoriaId = C.CategoriaId
        WHERE
          S.CategoriaId IN :categoriasId
          AND :precoPor BETWEEN S.PrecoInicial AND S.PrecoFinal
        ORDER BY C.Nivel DESC
        LIMIT 1";

return $sql;