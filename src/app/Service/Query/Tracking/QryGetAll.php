<?php
return "SELECT IT.*
        FROM " . DB_INTEGRACAO . ".InterfaceTracking IT
        INNER JOIN " . DB_INTEGRACAO . ".Parceiro_Site PS ON PS.ParceiroId = IT.ParceiroId
        WHERE IT.PedidoParceiroId IN :sqlIn
        AND IT.ParceiroId = :parceiroId
        AND PS.SiteId = :siteId;";