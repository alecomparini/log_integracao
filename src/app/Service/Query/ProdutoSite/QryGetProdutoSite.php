<?php
$sql = "SELECT P.Codigo, PRS.PrecoPor
        FROM " . DB_INTEGRACAO . ".Parceiro_Site PS
        INNER JOIN " . DB_MAGAZINE . ".Produto_Site PRS ON (PS.SiteId = PRS.SiteId AND PS.ParceiroSiteId = :parceiro_id)
        INNER JOIN " . DB_MAGAZINE . ".Produto P ON (PRS.ProdutoId = P.ProdutoId)
        WHERE P.Codigo IN :codigos";
return $sql;
