<?php
$produtoWhere = "";
if (!empty($params["produtos"])) {
    $produtoWhere = "AND CA.ProdutoId IN :produtos ";
}

$query = "SELECT 
            CatalogoId,
            ProdutoId,
            Desconto,
            ParceiroId,
            SiteId
         FROM " . DB_INTEGRACAO . ".Catalogo_Produto CA
        WHERE CA.CatalogoId = :catalogoId
          AND CA.SiteId = :siteId
          AND CA.ParceiroId = (SELECT MAX(ParceiroId)
                                FROM " . DB_INTEGRACAO . ".Catalogo_Produto
                               WHERE CatalogoId = CA.CatalogoId
                                 AND SiteId = CA.SiteId 
                                 AND ProdutoId = CA.ProdutoId
                                 AND (ParceiroId = :parceiroId OR ParceiroId = 0))
        {$produtoWhere}
        GROUP BY CA.ProdutoId";
        
return $query;