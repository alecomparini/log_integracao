<?php

$sql = "SELECT
          if(GarantiaProdutoId,true, false) as confirmacao
        FROM
          " . DB_MAGAZINE . ".GarantiaProduto
        WHERE
          ProdutoId = :produtoId
          AND GarantiaCodigo = :codigo";

return $sql;