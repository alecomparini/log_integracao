<?php

$sql = "SELECT
                G.ProdutoId,
                G.GarantiaCodigo,
                GP.Prazo,
                P.PrazoGarantia as PrazoGarantiaFabricante,
                GT.ValorCusto,
                GT.ValorVenda as ValorVendaOrig,
                ROUND((PSI.PrecoPor * GT.ValorVenda),2) as ValorVenda,
                GT.GarantiaTabelaId,
                P.Nome
        FROM
                " . DB_MAGAZINE . ".GarantiaProduto G
                INNER JOIN " . DB_MAGAZINE . ".GarantiaPrazo GP ON (G.GarantiaPrazoId = GP.GarantiaPrazoId)
                INNER JOIN " . DB_MAGAZINE . ".GarantiaTabela GT ON (G.GarantiaPrazoId = GT.GarantiaPrazoId)
                INNER JOIN " . DB_MAGAZINE . ".Produto P ON (P.ProdutoId = G.ProdutoId)
                INNER JOIN " . DB_MAGAZINE . ".Produto_Site PSI ON (P.ProdutoId = PSI.ProdutoId AND PSI.SiteId = :siteId)
        WHERE
                G.ProdutoId = :produtoId
                AND G.Status = 1
                AND GP.Status = 1
                AND GT.CategoriaId IN :categorias
                AND :precoPor BETWEEN GT.PrecoInicial AND GT.PrecoFinal

                AND P.PrazoGarantia BETWEEN 3 AND 12
        GROUP BY GP.GarantiaPrazoId
        ORDER BY GP.Prazo ASC
        LIMIT 100";

return $sql;