<?php

$sql = "SELECT
    P.ProdutoId,
    P.Codigo,
    PCU.EAN,
    CONCAT(PCU.CodFornecedor, ' ') as CodigoFornecedor,
    P.Nome,
    P.Descricao,

    IF (C2.Nome = 'Home', C1.CategoriaId, C2.CategoriaId) AS LojaId,
    IF (C2.Nome = 'Home', C1.Nome, C2.Nome) AS Loja,

    IF (C2.Nome = 'Home', C.CategoriaId, C1.CategoriaId) AS CategoriaId,
    IF (C2.Nome = 'Home', C.Nome, C1.Nome) AS Categoria,

    IF (C2.Nome = 'Home', '', C.CategoriaId) AS SubCategoriaId,
    IF (C2.Nome = 'Home', '', C.Nome) AS SubCategoria,

    F.Nome AS Fabricante,
    PS.Status,
    P.EmLinha,
    PS.PrecoDe,
    PS.PrecoPor,
    PS.ParcelamentoMaximo AS ParcelamentoMaximoSemJuros,

    (SELECT
        GROUP_CONCAT(CAST(CONCAT(CR.Nome, ': ', CV.Valor, ' ', CR.Unidade) AS CHAR)  SEPARATOR '|')
    FROM
        " . DB_MAGAZINE . ".Produto_CaracteristicaValor PCV
        INNER JOIN " . DB_MAGAZINE . ".CaracteristicaValor CV ON CV.CaracteristicaValorId = PCV.CaracteristicaValorId
        INNER JOIN " . DB_MAGAZINE . ".Caracteristica CR ON CR.CaracteristicaId = CV.CaracteristicaId
    WHERE
        P.ProdutoId = PCV.ProdutoId
        AND CR.Nome <> '' AND CV.Valor <> '') AS Caracteristicas,
    S.Url as UrlSite,
    PS.DataAlteracao

FROM
    " . DB_MAGAZINE . ".Produto P
    INNER JOIN " . DB_MAGAZINE . ".Produto_Site PS ON (P.ProdutoId = PS.ProdutoId AND PS.SiteId = :siteId)
    INNER JOIN " . DB_MAGAZINE . ".Site S ON (PS.SiteId = S.SiteId)
    INNER JOIN " . DB_MAGAZINE . ".Produto_Categoria PC ON (P.ProdutoId = PC.ProdutoId AND PC.Principal=1)
    INNER JOIN " . DB_MAGAZINE . ".Categoria C  ON PC.CategoriaId = C.CategoriaId
    INNER JOIN " . DB_MAGAZINE . ".Categoria C1 ON C.CategoriaAsc = C1.CategoriaId
    INNER JOIN " . DB_MAGAZINE . ".ProdutoCusto PCU ON P.Codigo = PCU.Codigo
    INNER JOIN " . DB_MAGAZINE . ".Fabricante F ON (F.FabricanteId = P.FabricanteId AND F.Status = 1)

    INNER JOIN " . DB_MAGAZINE . ".Categoria_Site CS ON (PC.CategoriaId = CS.CategoriaId AND CS.SiteId = 1)

    LEFT JOIN " . DB_MAGAZINE . ".Produto_CaracteristicaValor PCV ON P.ProdutoId = PCV.ProdutoId
    LEFT JOIN " . DB_MAGAZINE . ".CaracteristicaValor CV ON CV.CaracteristicaValorId = PCV.CaracteristicaValorId
    LEFT JOIN " . DB_MAGAZINE . ".Caracteristica CR ON CR.CaracteristicaId = CV.CaracteristicaId
    LEFT JOIN " . DB_MAGAZINE . ".Categoria C2 ON C1.CategoriaAsc = C2.CategoriaId

WHERE
  P.VendeAvulso = 1
  AND PS.Status = 1
GROUP BY P.ProdutoId
LIMIT 1000";

return $sql;
