<?php
// Retorna ids de produtos (de conhecimento dos programadores) por códigos de produtos (de conhecimento dos usuários)
$sql = "SELECT ProdutoId FROM " . DB_MAGAZINE . ".Produto p WHERE p.Codigo IN :codigos";

return $sql;
