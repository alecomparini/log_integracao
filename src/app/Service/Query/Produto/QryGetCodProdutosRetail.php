<?php
// Retorna ids de produtos (de conhecimento dos programadores) por códigos de produtos retail (de conhecimento dos usuários da fisica)
$sql = "SELECT ProdutoId FROM " . DB_MAGAZINE . ".Produto p WHERE p.CodigoRetail IN :codigosRetail";

return $sql;
