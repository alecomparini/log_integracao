<?php

$sql = "SELECT
            CONCAT(C1.CategoriaId,',',C2.CategoriaId,',',C3.CategoriaId) AS Categorias,
            PS.PrecoPor,
            GREATEST(PS.ParcelamentoMaximo, 1)                           AS ParcelamentoMaximo,
            CONCAT(C1.Nome, ', ', C2.Nome, ', ', C3.Nome)                AS CategoriasNome
        FROM " . DB_MAGAZINE . ".Produto_Categoria PC
            INNER JOIN " . DB_MAGAZINE . ".Categoria C1 ON PC.CategoriaId = C1.CategoriaId
            INNER JOIN " . DB_MAGAZINE . ".Categoria C2 ON C1.CategoriaAsc = C2.CategoriaId
            INNER JOIN " . DB_MAGAZINE . ".Categoria C3 ON C2.CategoriaAsc = C3.CategoriaId
            INNER JOIN " . DB_MAGAZINE . ".Produto_Site PS ON (PC.ProdutoId = PS.ProdutoId AND PS.SiteId = :siteId)
        WHERE PC.ProdutoId = :produtoId
        AND PC.Principal = 1";

return $sql;
