<?php
$whereAnd = array('1 = 1', 'p.ProdutoId > 0');

/* SESSÃO DO LIMITE */
$limit = '';
if (isset($params['limit'])) {
    $limit = 'LIMIT ' . $params['limit'];
}

$offset = '';
if (isset($params['offset'])) {
    $offset = 'OFFSET ' . $params['offset'];
}

if (isset($params['fabricanteId'])) {
    $whereAnd[] = 'f.FabricanteId = :fabricanteId';
}

if (isset($params['siteId'])) {
    $whereAnd[] = 's.SiteId = :siteId';
}

if (isset($params['emLinha'])) {
    $whereAnd[] = 'p.EmLinha = :siteId';
}

if (isset($params['status'])) {
    $whereAnd[] = 'ps.Status IN :status';
}

if (isset($params['codigo'])) {
    if (!is_array($params['codigo'])) {
        $whereAnd[] = 'p.Codigo = :codigo';
    } else {
        $whereAnd[] = 'p.Codigo IN :codigo';
    }
}

if (!$params['todos']) {
    if (!empty($params['categorias'])) {
        $whereAnd[] = 'c.CategoriaId IN categorias';
    }

    if (!empty($params['fabricantes'])) {
        $whereAnd[] = 'f.FabricanteId IN :fabricantes';
    }
}

$where = implode(' AND ', $whereAnd);

$sql = "
SELECT
    COUNT(*)            AS total
FROM        riel_producao.Produto           p
INNER JOIN  riel_producao.Fabricante        f       ON f.FabricanteId = p.FabricanteId
INNER JOIN  riel_producao.Produto_Site      ps      ON ps.ProdutoId = p.ProdutoId
INNER JOIN  riel_producao.Site              s       ON s.SiteId = ps.SiteId
LEFT JOIN   riel_producao.Estoque           e       ON e.ProdutoId = p.ProdutoId
LEFT JOIN   riel_producao.ProdutoCusto      pcu     ON pcu.Codigo = p.Codigo
LEFT JOIN   riel_producao.ProdutoDimensao   pd      ON pd.ProdutoId = p.ProdutoId
INNER JOIN  riel_producao.Produto_Categoria pc      ON pc.ProdutoId = p.ProdutoId AND pc.Principal = 1
INNER JOIN  riel_producao.Categoria         c       ON c.CategoriaId = pc.CategoriaId
INNER JOIN  riel_producao.Categoria         c1      ON c1.CategoriaId = c.CategoriaAsc
LEFT JOIN   riel_producao.Categoria         c2      ON c2.CategoriaId = c1.CategoriaAsc

WHERE       {$where}

ORDER BY p.ProdutoId
";

return $sql;