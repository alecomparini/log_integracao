<?php
$whereAnd = array('1 = 1', 'p.ProdutoId > 0');

/* SESSÃO DO LIMITE */
$limit = '';
if (isset($params['limit'])) {
    $limit = 'LIMIT ' . $params['limit'];
}

$offset = '';
if (isset($params['offset'])) {
    $offset = 'OFFSET ' . $params['offset'];
}

if (isset($params['fabricanteId'])) {
    $whereAnd[] = 'f.FabricanteId = :fabricanteId';
}

if (isset($params['siteId'])) {
    $whereAnd[] = 's.SiteId = :siteId';
}

if (isset($params['emLinha'])) {
    $whereAnd[] = 'p.EmLinha = :siteId';
}

if (isset($params['status'])) {
    $whereAnd[] = 'ps.Status IN :status';
}

if (isset($params['codigo'])) {
    if (!is_array($params['codigo'])) {
        $whereAnd[] = 'p.Codigo = :codigo';
    } else {
        $whereAnd[] = 'p.Codigo IN :codigo';
    }
}

if (!$params['todos']) {
    if (!empty($params['categorias'])) {
        $whereAnd[] = 'c.CategoriaId IN categorias';
    }

    if (!empty($params['fabricantes'])) {
        $whereAnd[] = 'f.FabricanteId IN :fabricantes';
    }
}

$where = implode(' AND ', $whereAnd);

$sql = "
SELECT
    p.ProdutoId                     AS produtoId,
    p.Codigo                        AS codigo,
    p.Nome                          AS nome,
    p.Descricao                     AS descricao,
    p.EmLinha                       AS emLinha,
                                    
    '1'                             AS quantidade,
    
    pcu.EAN                         AS eAN,
    CONCAT(pcu.CodFornecedor, ' ')  AS codigoFornecedor,
    
    ps.Status                       AS status,
    COALESCE(ps.PrecoDe, 0)         AS precoDe,
    COALESCE(ps.PrecoPor, 0)        AS precoPor,
    ps.ParcelamentoMaximo           AS parcelamentoMaximoSemJuros,
    ps.DataAlteracao                AS dataAlteracao,
    
    f.FabricanteId                  AS fabricanteId,
    f.Nome                          AS fabricante,
    
    COALESCE(e.Disponivel, 0)       AS disponivel,
    
    pd.Altura                       AS altura,
    pd.Comprimento                  AS comprimento,
    pd.Largura                      AS largura,
    pd.Peso                         AS peso,
    
    pd.AlturaEmbalagem              AS alturaEmbalagem,
    pd.ComprimentoEmbalagem         AS comprimentoEmbalagem,
    pd.LarguraEmbalagem             AS larguraEmbalagem,
    pd.PesoEmbalagem                AS pesoEmbalagem,
    
    s.Url                                               AS url,

    IF(c.Nivel = 3, c2.CategoriaId, c1.CategoriaId)     AS lojaId,
    IF(c.Nivel = 3, c2.Nome, c1.Nome)                   AS loja,
    
    IF(c.Nivel = 3, c1.CategoriaId, c.CategoriaId)      AS categoriaId,
    IF(c.Nivel = 3, c1.Nome, c.Nome)                    AS categoria,
    
    IF(c.Nivel = 3, c.CategoriaId, null)                AS subCategoriaId,
    IF(c.Nivel = 3, c.Nome, null)                       AS subCategoria

FROM        riel_producao.Produto           p
INNER JOIN  riel_producao.Fabricante        f       ON f.FabricanteId = p.FabricanteId
INNER JOIN  riel_producao.Produto_Site      ps      ON ps.ProdutoId = p.ProdutoId
INNER JOIN  riel_producao.Site              s       ON s.SiteId = ps.SiteId
LEFT JOIN   riel_producao.Estoque           e       ON e.ProdutoId = p.ProdutoId
LEFT JOIN   riel_producao.ProdutoCusto      pcu     ON pcu.Codigo = p.Codigo
LEFT JOIN   riel_producao.ProdutoDimensao   pd      ON pd.ProdutoId = p.ProdutoId
INNER JOIN  riel_producao.Produto_Categoria pc      ON pc.ProdutoId = p.ProdutoId AND pc.Principal = 1
INNER JOIN  riel_producao.Categoria         c       ON c.CategoriaId = pc.CategoriaId
INNER JOIN  riel_producao.Categoria         c1      ON c1.CategoriaId = c.CategoriaAsc
LEFT JOIN   riel_producao.Categoria         c2      ON c2.CategoriaId = c1.CategoriaAsc

WHERE       {$where}

ORDER BY p.ProdutoId
{$limit}
{$offset}
";

return $sql;