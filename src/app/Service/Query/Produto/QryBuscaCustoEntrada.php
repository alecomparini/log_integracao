<?php
// Retorna custo(s) de entrada de produtos por código
return "SELECT ValorCustoEntrada
        FROM " . DB_MAGAZINE . ".ProdutoCusto
        WHERE Codigo = :codigo;";