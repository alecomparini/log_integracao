<?php

$statusWhereIn = '';
$categoriasWhereIn = '';
$fabricantesWhereIn = '';
$joinCategoria = '';
if (!isset($params['todos']) || !$params['todos']) {
    if (isset($params['categorias']) && !empty($params['categorias'])) {
        $categoriasWhereIn = ' AND C.CategoriaId IN :categorias';

        $joinCategoria = "
            INNER JOIN " . DB_MAGAZINE . ".Produto_Categoria PC ON (P.ProdutoId = PC.ProdutoId AND PC.Principal=1)
            INNER JOIN " . DB_MAGAZINE . ".Categoria C  ON PC.CategoriaId = C.CategoriaId
            INNER JOIN " . DB_MAGAZINE . ".Categoria C1 ON C.CategoriaAsc = C1.CategoriaId
            LEFT JOIN " . DB_MAGAZINE . ".Categoria C2 ON C1.CategoriaAsc = C2.CategoriaId
            INNER JOIN " . DB_MAGAZINE . ".Categoria_Site CS ON (PC.CategoriaId = CS.CategoriaId AND CS.SiteId = 1)";
    }

    if (isset($params['fabricantes']) && !empty($params['fabricantes'])) {
        $fabricantesWhereIn = ' AND P.FabricanteId IN :fabricantes';
    }
} else {
    $joinCategoria = "
            INNER JOIN " . DB_MAGAZINE . ".Produto_Categoria PC ON (P.ProdutoId = PC.ProdutoId AND PC.Principal=1)
            INNER JOIN " . DB_MAGAZINE . ".Categoria C  ON PC.CategoriaId = C.CategoriaId
            INNER JOIN " . DB_MAGAZINE . ".Categoria C1 ON C.CategoriaAsc = C1.CategoriaId
            LEFT JOIN " . DB_MAGAZINE . ".Categoria C2 ON C1.CategoriaAsc = C2.CategoriaId
            INNER JOIN " . DB_MAGAZINE . ".Categoria_Site CS ON (PC.CategoriaId = CS.CategoriaId AND CS.SiteId = 1)";
}

if (isset($params['status']) && !empty($params['status'])) {
    $statusWhereIn = ' AND PS.Status IN :status ';
}

$sql = "SELECT
            COUNT(P.ProdutoId) AS total
        FROM
            " . DB_MAGAZINE . ".Produto P
            INNER JOIN " . DB_MAGAZINE . ".Fabricante F ON (F.FabricanteId = P.FabricanteId)
            INNER JOIN " . DB_MAGAZINE . ".Produto_Site PS ON (P.ProdutoId = PS.ProdutoId AND PS.SiteId = :siteId)
            INNER JOIN " . DB_MAGAZINE . ".Site S ON (PS.SiteId = S.SiteId)
            {$joinCategoria}
            LEFT JOIN " . DB_MAGAZINE . ".ProdutoCusto PCU ON P.Codigo = PCU.Codigo
            LEFT JOIN " . DB_MAGAZINE . ".Estoque E ON (E.ProdutoId = P.ProdutoId)
            LEFT JOIN " . DB_MAGAZINE . ".ProdutoDimensao PD ON PD.ProdutoId = P.ProdutoId
            WHERE 1=1 {$statusWhereIn} {$categoriasWhereIn} {$fabricantesWhereIn}";

return $sql;


