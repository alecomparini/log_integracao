<?php

$sql = "SELECT ComboId
        FROM " . DB_MAGAZINE . ".Combo C
        INNER JOIN " . DB_MAGAZINE . ".Produto P ON P.ProdutoId = C.ProdutoId AND C.SiteId = :siteId
        INNER JOIN " . DB_MAGAZINE . ".Produto_Site PS ON P.ProdutoId = PS.ProdutoId AND PS.SiteId = :siteId
        WHERE C.ProdutoId = :codComboPai";

return $sql;
