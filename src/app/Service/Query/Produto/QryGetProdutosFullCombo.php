<?php
$where = "";

if (isset($params['produtosId']) && !empty($params['produtosId'])) {
    $where = "AND P.ProdutoId IN :produtosId";
}

if (isset($params['status']) && !empty($params['status'])) {
    $where .= ' AND PS.Status IN :status ';
}

$categoriasWhereIn = '';
$fabricantesWhereIn = '';
$joinCategoria = '';
if (!isset($params['todos']) || !$params['todos']) {
    $joinCategoria = "INNER JOIN " . DB_MAGAZINE . ".Produto_Categoria PC ON P.ProdutoId = PC.ProdutoId AND PC.Principal = 1";

    if (isset($params['categorias']) && !empty($params['categorias'])) {
        $categoriasWhereIn = ' AND C.CategoriaId IN :categorias';
    }

    if (isset($params['fabricantes']) && !empty($params['fabricantes'])) {
        $fabricantesWhereIn = ' AND P.FabricanteId IN :fabricantes';
    }
    $where .= " {$categoriasWhereIn} {$fabricantesWhereIn}";
}

$sql = "SELECT
            P.ProdutoId,
            P.Codigo,
            PCU.EAN,
            CONCAT(PCU.CodFornecedor, ' ') AS CodigoFornecedor,
            P.Nome,
            P.Descricao,
            P.VendeAvulso,
            P.TipoTransporte,

            IF (C2.Nome = 'Home', C1.CategoriaId, C2.CategoriaId) AS LojaId,
            IF (C2.Nome = 'Home', C1.Nome, C2.Nome) AS Loja,

            IF (C2.Nome = 'Home', C.CategoriaId, C1.CategoriaId) AS CategoriaId,
            IF (C2.Nome = 'Home', C.Nome, C1.Nome) AS Categoria,

            IF (C2.Nome = 'Home', '', C.CategoriaId) AS SubCategoriaId,
            IF (C2.Nome = 'Home', '', C.Nome) AS SubCategoria,

            F.FabricanteId,
            F.Nome AS Fabricante,
            PS.Status,
            P.EmLinha,
            PS.PrecoDe,
            ROUND(COALESCE(CMB.Valor, PS.PrecoPor), 2) AS PrecoPor,
            COALESCE(CMB.Quantidade, 1) AS Quantidade,
            PS.ParcelamentoMaximo AS ParcelamentoMaximoSemJuros,

            (SELECT
                GROUP_CONCAT(CAST(CONCAT(CR.Nome, ': ', CV.Valor, ' ', CR.Unidade) AS CHAR)  SEPARATOR '|')
            FROM
                " . DB_MAGAZINE . ".Produto_CaracteristicaValor PCV
                INNER JOIN " . DB_MAGAZINE . ".CaracteristicaValor CV ON CV.CaracteristicaValorId = PCV.CaracteristicaValorId
                INNER JOIN " . DB_MAGAZINE . ".Caracteristica CR ON CR.CaracteristicaId = CV.CaracteristicaId
            WHERE
                P.ProdutoId = PCV.ProdutoId
                AND CR.Nome <> '' AND CV.Valor <> '') AS Caracteristicas,

            S.Url AS UrlSite,
            PS.DataAlteracao,

            (SELECT
                GROUP_CONCAT(CAST( CONCAT(PM.Src, '::', PM.Mime) AS CHAR) ORDER BY PM.Ordem SEPARATOR '|')
             FROM
                " . DB_MAGAZINE . ".ProdutoMedia PM
             WHERE PM.ProdutoId = P.ProdutoId
                AND PM.Src <> '' AND PM.Mime <> '') AS Medias,

            (SELECT GROUP_CONCAT(II.Nome ORDER BY II.ItemInclusoId SEPARATOR '|')
             FROM " . DB_MAGAZINE . ".ItemIncluso II
             WHERE P.ProdutoId = II.ProdutoId) AS ItensIncluso,

             PD.Altura,
             PD.Comprimento,
             PD.Largura,
             PD.Peso,
             PD.AlturaEmbalagem,
             PD.ComprimentoEmbalagem,
             PD.LarguraEmbalagem,
             PD.PesoEmbalagem,
             IFNULL(E.Disponivel,0) AS Disponivel
 
        FROM
            " . DB_MAGAZINE . ".Produto P
            INNER JOIN " . DB_MAGAZINE . ".Produto_Categoria PC ON (P.ProdutoId = PC.ProdutoId AND PC.Principal=1)
            INNER JOIN " . DB_MAGAZINE . ".Combo CMB ON CMB.ComboId = P.ProdutoId AND CMB.ProdutoId = :produtoPai AND CMB.SiteId = :siteId
            INNER JOIN " . DB_MAGAZINE . ".Categoria C  ON PC.CategoriaId = C.CategoriaId
            INNER JOIN " . DB_MAGAZINE . ".Categoria C1 ON C.CategoriaAsc = C1.CategoriaId
            INNER JOIN " . DB_MAGAZINE . ".Fabricante F ON (F.FabricanteId = P.FabricanteId)
            INNER JOIN " . DB_MAGAZINE . ".Produto_Site PS ON (P.ProdutoId = PS.ProdutoId AND PS.SiteId = :siteId)
            INNER JOIN " . DB_MAGAZINE . ".Categoria_Site CS ON (PC.CategoriaId = CS.CategoriaId AND CS.SiteId = 1)
            INNER JOIN " . DB_MAGAZINE . ".Site S ON (PS.SiteId = S.SiteId)
            LEFT JOIN " . DB_MAGAZINE . ".Estoque E ON (E.ProdutoId = P.ProdutoId)
            LEFT JOIN " . DB_MAGAZINE . ".ProdutoDimensao PD ON PD.ProdutoId = P.ProdutoId
            LEFT JOIN " . DB_MAGAZINE . ".ProdutoCusto PCU ON P.Codigo = PCU.Codigo
            LEFT JOIN " . DB_MAGAZINE . ".Categoria C2 ON C1.CategoriaAsc = C2.CategoriaId
        WHERE
            1=1 {$where}";

return $sql;