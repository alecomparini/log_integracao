<?php
$sqlComp = "";

if (isset($params['codigos'])) {
    $sqlComp = " AND Codigo IN :codigos";
}

$sql = "select
            ParceiroId      as parceiroId,
            Codigo          as codigo,
            SkuParceiro     as skuParceiro,
            DataCriacao     as dataCriacao
        from " . DB_INTEGRACAO . ".SkuParceiro
        where 1 = 1 AND ParceiroId = :parceiroId {$sqlComp};";

return $sql;