<?php

$sql = "SELECT p.ProdutoId, e.Disponivel
        FROM " . DB_MAGAZINE . ".Produto p
        INNER JOIN " . DB_MAGAZINE . ".Estoque e
        ON p.ProdutoId = e.ProdutoId
        WHERE p.Codigo = :codigo";

return $sql;