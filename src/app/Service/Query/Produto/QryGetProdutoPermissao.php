<?php

$categoriasWhereIn = '';
$fabricantesWhereIn = '';
if (!isset($params['todos']) || !$params['todos']) {

    if (isset($params['categorias']) && !empty($params['categorias'])) {
        $categoriasWhereIn = ' AND pc.CategoriaId IN :categorias';
    }

    if (isset($params['fabricantes']) && !empty($params['fabricantes'])) {
        $fabricantesWhereIn = ' AND p.FabricanteId IN :fabricantes';
    }
}

$sql = "
            SELECT ps.SiteId
            FROM " . DB_MAGAZINE . ".Produto p
            INNER JOIN " . DB_MAGAZINE . ".Produto_Site ps ON ps.ProdutoId = p.ProdutoId
            INNER JOIN " . DB_MAGAZINE . ".Produto_Categoria pc ON p.ProdutoId = pc.ProdutoId AND pc.Principal = 1

            WHERE ps.SiteId = :siteId
            AND p.Codigo = :produtoCodigo
            {$categoriasWhereIn} {$fabricantesWhereIn}
";

return $sql;