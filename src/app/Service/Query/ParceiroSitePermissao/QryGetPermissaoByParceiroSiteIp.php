<?php
$sql = "SELECT 
            P.ParceiroId,
            P.Nome,
            P.Status,
            PS.ParceiroSiteId,
            PS.SiteId,
            PS.SiteBase
        FROM " . DB_INTEGRACAO . ".Parceiro P
        INNER JOIN " . DB_INTEGRACAO . ".Parceiro_Site   PS  ON P.ParceiroId = PS.ParceiroId
        INNER JOIN " . DB_INTEGRACAO . ".Parceiro_SiteIp PSI ON PSI.ParceiroSiteId = PS.ParceiroSiteId
        WHERE PS.ParceiroSiteId = :parceiroSiteId
        AND PS.SiteId = :siteId
        AND PSI.Ip = :ip;";
return $sql;