<?php
return "SELECT
          PSP.ParceiroSitePermissaoId AS ParceiroSitePermissaoId,
          PSP.PermissaoTipoId         AS PermissaoTipoId,
          PSP.Permissao               AS Permissao,
          PSP.Excecao                 AS Excecao,
          PS.SiteBase                 AS SiteBase,
          PS.SiteId                   AS SiteId,
          PS.ParceiroSiteId           AS ParceiroSiteId,
          P.ParceiroId                AS ParceiroId,
          P.Nome                      AS Nome,
          P.Status                    AS Status
        FROM
          " . DB_INTEGRACAO . ".Parceiro P
          INNER JOIN " . DB_INTEGRACAO . ".Parceiro_Site PS             ON P.ParceiroId = PS.ParceiroId
          INNER JOIN " . DB_INTEGRACAO . ".Parceiro_Site_Permissao PSP  ON PSP.ParceiroSiteId = PS.ParceiroSiteId
          INNER JOIN " . DB_INTEGRACAO . ".PermissaoTipo PT             ON PT.PermissaoTipoId = PSP.PermissaoTipoId
        WHERE
          P.Status = 1
          AND PS.Status = 1
          AND PS.SiteBase = :siteBase
          AND PS.ParceiroId = :parceiroId
          AND PSP.PermissaoTipoId <=> COALESCE(:permissaoTipoId, PSP.PermissaoTipoId)
        ORDER BY PSP.PermissaoTipoId DESC, PSP.Permissao";