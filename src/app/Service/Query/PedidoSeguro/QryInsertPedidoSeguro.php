<?php

$sql = "INSERT INTO
  " . DB_INTEGRACAO . ".Pedido_Seguro(
      DadosImportacaoId,
      PedidoProdutoId,
      SeguroTabelaId,
      Quantidade,
      ValorVenda,
      Familia)
  VALUES (
    :dadosImportacaoID,
    :pedidoProdutoId,
    :seguroTabelaId,
    :quantidade,
    :valorVenda,
    :familia
)";

return $sql;