<?php
$sql = "SELECT
            IP.InterfacePermissaoId,
            IP.ParceiroSiteId,
            IP.InterfaceTipoId,
            IT.Nome,
            IT.Status
        FROM " . DB_INTEGRACAO . ".InterfacePermissao IP
        INNER JOIN " . DB_INTEGRACAO . ".InterfaceTipo IT ON IT.InterfaceTipoId = IP.InterfaceTipoId
        WHERE IP.ParceiroSiteId = :parceiroSiteId
        AND   IP.InterfaceTipoId = :interfaceTipo;";
return $sql;