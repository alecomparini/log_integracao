<?php
return "
SELECT
    at.AgendamentoTarefaId              AS agendamentoTarefaId,
    at.TarefaId                         AS tarefaId,
    at.NumExecucoes                     AS numExecucoes,
    at.NumExecutadas                    AS numExecutadas,
    at.ConfiguracaoTempo                AS configuracaoTempo,
    at.Status                           AS status,
    at.DataUltimaExecucao               AS dataUltimaExecucao,
    t.Nome                              AS nome,
    t.Descricao                         AS descricao,
    t.Url                               AS url,
    t.Parametros                        AS parametros,
    t.IndJson                           AS indJson,
    t.MetodoHttp                        AS metodoHttp,
    t.ContentType                       AS contentType
FROM        riel_integracao.AgendamentoTarefa   AS at
INNER JOIN  riel_integracao.Tarefa              AS t        ON t.TarefaId = at.TarefaId
WHERE       at.Status = 1
AND         at.NumExecucoes IS NULL

UNION 

SELECT
    at.AgendamentoTarefaId              AS agendamentoTarefaId,
    at.TarefaId                         AS tarefaId,
    at.NumExecucoes                     AS numExecucoes,
    at.NumExecutadas                    AS numExecutadas,
    at.ConfiguracaoTempo                AS configuracaoTempo,
    at.Status                           AS status,
    at.DataUltimaExecucao               AS dataUltimaExecucao,
    t.Nome                              AS nome,
    t.Descricao                         AS descricao,
    t.Url                               AS url,
    t.Parametros                        AS parametros,
    t.IndJson                           AS indJson,
    t.MetodoHttp                        AS metodoHttp,
    t.ContentType                       AS contentType
FROM        riel_integracao.AgendamentoTarefa   AS at
INNER JOIN  riel_integracao.Tarefa              AS t        ON t.TarefaId = at.TarefaId
WHERE       at.Status = 1
AND         at.NumExecucoes IS NOT NULL
AND         at.NumExecutadas < at.NumExecucoes
";