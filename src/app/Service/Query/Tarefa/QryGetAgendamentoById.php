<?php
return "
SELECT
    at.AgendamentoTarefaId              AS agendamentoTarefaId,
    at.TarefaId                         AS tarefaId,
    at.NumExecucoes                     AS numExecucoes,
    at.NumExecutadas                    AS numExecutadas,
    at.ConfiguracaoTempo                AS configuracaoTempo,
    at.Status                           AS status,
    at.DataUltimaExecucao               AS dataUltimaExecucao,
    t.Nome                              AS nome,
    t.Descricao                         AS descricao,
    t.Url                               AS url,
    t.Parametros                        AS parametros,
    t.IndJson                           AS indJson,
    t.MetodoHttp                        AS metodoHttp,
    t.ContentType                       AS contentType
FROM        " . DB_INTEGRACAO . ".AgendamentoTarefa   AS at
INNER JOIN  " . DB_INTEGRACAO . ".Tarefa              AS t        ON t.TarefaId = at.TarefaId
WHERE       at.Status = 1
AND         at.AgendamentoTarefaId = :agendamentoTarefaId
";