<?php
return "
UPDATE " . DB_INTEGRACAO . ".AgendamentoTarefa SET
    DataUltimaExecucao = :dataUltimaExecucao,
    NumExecutadas = NumExecutadas + 1    
WHERE AgendamentoTarefaId = :agendamentoTarefaId
";