<?php
return "
INSERT INTO " . DB_INTEGRACAO . ".AgendamentoTarefaLog (
    AgendamentoTarefaId,
    DataInicioExecucao,
    DataTerminoExecucao,
    HttpStatusCode,
    RespostaHttp,
    CabecalhosHttp) VALUES
(:agendamentoTarefaId, :dataInicioExecucao, :dataTerminoExecucao, :httpStatusCode, :respostaHttp, :cabecalhosHttp)
";