<?php
//Busca dados sobre a importação de pedidos de parceiros em um site
/** @lang mysql */
$sql = "SELECT DI.DadosImportacaoId FROM
          " . DB_INTEGRACAO . ".DadosImportacao DI
        WHERE
          DI.ParceiroPedidoId = :parceiroPedidoId AND
          DI.ParceiroSiteId = :parceiroSiteId;";

return $sql;