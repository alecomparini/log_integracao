<?php
return "
SELECT
    p.PagamentoId AS pagamentoId,
    p.DadosIntegracaoId AS dadosIntegracaoId,
    p.MeioPagamentoId AS meioPagamentoId,
    p.BandeiraId AS bandeiraId,
    p.ValorPago AS valorPago,
    p.Parcelas AS parcelas,
    p.PercentualJuros AS percentualJuros,
    p.ValorJuros AS valorJuros,
    p.CartaoNumero AS cartaoNumero,
    p.CartaoValidade AS cartaoValidade,
    p.CartaoCodSeguranca AS cartaoCodSeguranca,
    p.CartaoNomePortador AS cartaoNomePortador,
    p.BoletoUrl AS boletoUrl,
    p.DataVencimento AS dataVencimento,
    p.CodigoVale AS codigoVale,
    p.Sequencial AS sequencial,
    p.Pontos AS pontos,
    p.Nsu AS nsu,
    p.AutorizacaoId AS autorizacaoId,
    p.DataAutorizacao AS dataAutorizacao,
    p.DataCaptura AS dataCaptura,
    p.Adquirente AS adquirente,
    p.TokenAutorizacao AS tokenAutorizacao
FROM " . DB_INTEGRACAO . ".Pagamento p
WHERE p.DadosIntegracaoId = :dadosImportacaoId
";