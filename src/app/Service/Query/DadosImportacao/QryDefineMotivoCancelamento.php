<?php
return "
UPDATE " . DB_INTEGRACAO . ".DadosImportacaoErro
SET
MotivoCancelamento = :motivoCancelamento,
UsuarioIdCancelamento = :usuarioIdCancelamento
WHERE DadosImportacaoId = :dadosImportacaoId
";