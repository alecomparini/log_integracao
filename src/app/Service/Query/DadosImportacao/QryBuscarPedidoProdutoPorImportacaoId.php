<?php
return "
SELECT
        PedidoProdutoId,
        DadosImportacaoId,
        ProdutoId,
        ProdutoCodigo,
        Quantidade,
        ValorUnitario,
        ValorTotal,
        ValorFrete,
        Tipo,
        PrazoEntregaCombinado,
        Sequencial,
        ValorDesconto,
        ValorCustoEntrada
FROM " . DB_INTEGRACAO . ".Pedido_Produto pp
WHERE pp.DadosImportacaoId = :dadosImportacaoId
";