<?php

$colunas = "";
$values = "";

if ($params['meioPagamentoId'] == 1) {
    $colunas = ", BandeiraId, CartaoNumero, CartaoValidade, CartaoCodSeguranca, CartaoNomePortador";
    $values = ", :bandeiraId, :cartaoNumero, :cartaoValidade, :cartaoCodSeguranca, :cartaoNomePortador";
} elseif ($params['meioPagamentoId'] == 2) {
    $colunas = ", BoletoUrl, DataVencimento";
    $values = ", :boletoUrl, :dataVencimento";
} elseif ($params['meioPagamentoId'] == 4) {
    $colunas = ", CodigoVale";
    $values = ", :codigoVale";
} elseif ($params["meioPagamentoId"] == 8) {
    $colunas = ", Pontos";
    $values = ", :pontos";
} elseif ($params['meioPagamentoId'] == 9) {
    $colunas = ", BandeiraId, CartaoNumero, CartaoValidade, CartaoCodSeguranca, CartaoNomePortador, NSU, AutorizacaoId, DataAutorizacao, DataCaptura, Adquirente";
    $values = ", :bandeiraId, :cartaoNumero, :cartaoValidade, :cartaoCodSeguranca, :cartaoNomePortador, :NSU, :autorizacaoId, :dataAutorizacao, :dataCaptura, :adquirente";
} elseif ($params['meioPagamentoId'] == 11) {
    $colunas = ", DataVencimento";
    $values = ", :dataVencimento";
}


return "
INSERT INTO " . DB_INTEGRACAO . ".Pagamento(
    DadosIntegracaoId
    ,MeioPagamentoId
    ,ValorPago
    ,Parcelas
    ,Sequencial
    ,ValorJuros
    ,PercentualJuros
    {$colunas}
) VALUES (
    :dadosIntegracaoId,
    :meioPagamentoId,
    :valorPago,
    :parcelas,
    :sequencial,
    :valorJuros,
    :percentualJuros
    {$values}
)";
