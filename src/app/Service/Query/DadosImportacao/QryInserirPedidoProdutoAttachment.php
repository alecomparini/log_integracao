<?php
return "
INSERT INTO " . DB_INTEGRACAO . ".Pedido_Attachment (
    DadosImportacaoId, PedidoProdutoId, Tipo, Rotulo, DataCriacao
) VALUES (
    :dadosImportacaoId, :pedidoProdutoId, :tipo, :rotulo, :dataCriacao
)
";