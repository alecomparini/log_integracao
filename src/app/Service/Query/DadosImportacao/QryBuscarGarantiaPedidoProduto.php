<?php
return "
SELECT
    pg.ProdutoGarantiaId,
    pg.DadosImportacaoId,
    pg.PedidoProdutoId,
    pg.GarantiaCodigo,
    pg.Quantidade,
    pg.Prazo,
    pg.ValorVenda
FROM " . DB_INTEGRACAO . ".Pedido_Garantia pg
WHERE pg.DadosImportacaoId = :dadosImportacaoId AND pg.PedidoProdutoId = :pedidoProdutoId
";