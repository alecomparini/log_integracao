<?php
return "
INSERT INTO " . DB_INTEGRACAO . ".Pedido_Garantia (
    DadosImportacaoId, PedidoProdutoId, GarantiaCodigo, Quantidade, Prazo, ValorVenda
) VALUES (
    :dadosImportacaoId, :pedidoProdutoId, :garantiaCodigo, :quantidade, :prazo, :valorVenda
)
";