<?php
return "
SELECT
    di.DadosImportacaoId as dadosImportacaoId,
    di.Email as clienteEmail,
    di.DataCadastro as dataCadastro,
    di.Tipo as clienteTipo,
    di.SiteId as siteId,
    di.Nome as clientePfNome,
    di.Sobrenome as clientePfSobrenome,
    di.CPF as clientePfCPF,
    di.DataNascimento as clientePfDataNascimento,
    di.Sexo as clientePfSexo,
    di.RazaoSocial as clientePjRazaoSocial,
    di.NomeFantasia as clientePjNomeFantasia,
    di.CNPJ as clientePjCNPJ,
    di.InscricaoEstadual as clientePjInscEstadual,
    di.Isento as clientePjIsento,
    di.Telefone as clientePjTelefone,
    di.Site as clientePjSite,
    di.RamoAtividade as clientePjRamoAtividade,
    di.CobrancaTipo as endCobrancaTipo,
    di.CobrancaDestinatario as endCobrancaDestinatario,
    di.CobrancaCep as endCobrancaCep,
    di.CobrancaEndereco as endCobrancaEndereco,
    di.CobrancaNumero as endCobrancaNumero,
    di.CobrancaComplemento as endCobrancaComplemento,
    di.CobrancaBairro as endCobrancaBairro,
    di.CobrancaCidade as endCobrancaCidade,
    di.CobrancaEstado as endCobrancaEstado,
    di.CobrancaTelefone1 as endCobrancaTelefone1,
    di.CobrancaTelefone2 as endCobrancaTelefone2,
    di.CobrancaCelular as endCobrancaCelular,
    di.CobrancaReferencia as endCobrancaReferencia,
    di.EntregaTipo as endEntregaTipo,
    di.EntregaDestinatario as endEntregaDestinatario,
    di.EntregaCep as endEntregaCep,
    di.EntregaEndereco as endEntregaEndereco,
    di.EntregaNumero as endEntregaNumero,
    di.EntregaComplemento as endEntregaComplemento,
    di.EntregaBairro as endEntregaBairro,
    di.EntregaCidade as endEntregaCidade,
    di.EntregaEstado as endEntregaEstado,
    di.EntregaTelefone1 as endEntregaTelefone1,
    di.EntregaTelefone2 as endEntregaTelefone2,
    di.EntregaCelular as endEntregaCelular,
    di.EntregaReferencia as endEntregaReferencia,
    di.CupomDescontoId as cupomDescontoId,
    di.ValorTotal as valorTotal,
    di.ValorTotalFrete as valorTotalFrete,
    di.ValorCupomDesconto as valorCupomDesconto,
    di.ValorCupomDescontoFrete as valorCupomDescontoFrete,
    di.IP as parceiroIp,
    di.StatusExportacao as statusExportacao,
    di.ValorTotalProdutos as valorTotalProdutos,
    di.ValorDespesas as valorDespesas,
    di.ValorJuros as valorJuros,
    di.ValorFreteSemPromocao as valorFreteSemPromocao,
    di.ParceiroPedidoId as parceiroPedidoId,
    di.ProducaoPedidoId as producaoPedidoId,
    di.ParceiroSiteId as parceiroSiteId,
    di.DataEntrega as dataEntrega,
    di.TurnoEntrega as turnoEntrega,
    di.ValorEntrega as valorEntrega,
    di.VendedorId as vendedorId,
    di.GrupoVendedorId as grupoVendedorId,
    di.IntegradorId as integradorId,
    di.TipoIntegrador as tipoIntegrador,
    ps.ParceiroId as parceiroId,
    die.MensagemErro as mensagemErroImportacao
FROM " . DB_INTEGRACAO . ".DadosImportacao di
INNER JOIN " . DB_INTEGRACAO . ".Parceiro_Site ps ON ps.ParceiroSiteId = di.ParceiroSiteId
LEFT JOIN " . DB_INTEGRACAO . ".DadosImportacaoErro die ON die.DadosImportacaoId = di.DadosImportacaoId
WHERE di.parceiroSiteId = :parceiroSiteId AND di.ParceiroPedidoId IN :parceiroPedidoId;
";