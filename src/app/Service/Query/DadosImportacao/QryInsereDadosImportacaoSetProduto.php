<?php
$columns = "";
$values = "";

if ($params['clienteTipo'] == 'pf') {
    $columns = ", Nome, Sobrenome, CPF, DataNascimento, Sexo";
    $values  = ", :clientePfNome, :clientePfSobrenome, :clientePfCpf, :clientePfDataNascimento, :clientePfSexo";
} else {
    $columns = ", RazaoSocial, NomeFantasia, CNPJ, InscricaoEstadual, Isento, Telefone, Site, RamoAtividade";
    $values  = ", :clientePjRazaoSocial, :clientePjNomeFantasia, :clientePjCNPJ, :clientePjInscEstadual, :clientePjIsento";
    $values .= ", :clientePjTelefone, :clientePjSite, :clientePjRamoAtividade";
}

$sql = "INSERT INTO " . DB_INTEGRACAO . ".DadosImportacao (
          Email, DataCadastro, Tipo, SiteId, CobrancaTipo, CobrancaDestinatario, CobrancaCep, CobrancaEndereco,
          CobrancaNumero, CobrancaComplemento, CobrancaBairro, CobrancaCidade, CobrancaEstado, CobrancaTelefone1,
          CobrancaTelefone2, CobrancaCelular, CobrancaReferencia, EntregaTipo, EntregaDestinatario, EntregaCep,
          EntregaEndereco, EntregaNumero, EntregaComplemento, EntregaBairro, EntregaCidade, EntregaEstado,
          EntregaTelefone1, EntregaTelefone2, EntregaCelular, EntregaReferencia, ValorTotal, ValorTotalFrete,
          IP, ValorTotalProdutos, ValorJuros, ValorFreteSemPromocao, ParceiroPedidoId, ParceiroSiteId, DataEntrega,
          TurnoEntrega, ValorEntrega, VendedorId, GrupoVendedorId, IntegradorId, TipoIntegrador, CampanhaId, PrazoCd {$columns}) VALUES (
          :clienteEmail, now(), :clienteTipo, :siteId, :endCobrancaTipo, :endCobrancaDestinatario, :endCobrancaCep, :endCobrancaEndereco,
          :endCobrancaNumero, :endCobrancaComplemento, :endCobrancaBairro, :endCobrancaCidade, :endCobrancaEstado, :endCobrancaTelefone1,
          :endCobrancaTelefone2, :endCobrancaCelular, :endCobrancaReferencia, :endEntregaTipo, :endEntregaDestinatario, :endEntregaCep,
          :endEntregaEndereco, :endEntregaNumero, :endEntregaComplemento, :endEntregaBairro, :endEntregaCidade, :endEntregaEstado,
          :endEntregaTelefone1, :endEntregaTelefone2, :endEntregaCelular, :endEntregaReferencia, :valorTotal, :valorTotalFrete,
          :parceiroIp, :valorTotalProdutos, :valorJuros, :valorFreteSemPromocao, :parceiroPedidoId, :parceiroSiteId, :dataEntrega,
          :turnoEntrega, :valorEntrega, :vendedorId, :grupoVendedorId, :integradorId, :tipoIntegrador, :campanhaId, :prazoCd {$values});";

return $sql;
