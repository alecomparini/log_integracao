<?php
return "
INSERT INTO " . DB_INTEGRACAO . ".DadosImportacao (
  Email, DataCadastro, Tipo, SiteId, Nome, Sobrenome, CPF,
  DataNascimento, Sexo, RazaoSocial, NomeFantasia, CNPJ,
  InscricaoEstadual, Isento, Telefone, Site, RamoAtividade,
  CobrancaTipo, CobrancaDestinatario, CobrancaCep, CobrancaEndereco,
  CobrancaNumero, CobrancaComplemento, CobrancaBairro,
  CobrancaCidade, CobrancaEstado, CobrancaTelefone1,
  CobrancaTelefone2, CobrancaCelular, CobrancaReferencia,
  EntregaTipo, EntregaDestinatario, EntregaCep, EntregaEndereco,
  EntregaNumero, EntregaComplemento, EntregaBairro,
  EntregaCidade, EntregaEstado, EntregaTelefone1, EntregaTelefone2,
  EntregaCelular, EntregaReferencia, CupomDescontoId,
  ValorTotal, ValorTotalFrete, ValorCupomDesconto,
  ValorCupomDescontoFrete, IP, StatusExportacao, ValorTotalProdutos,
  ValorDespesas, ValorJuros, ValorFreteSemPromocao, ParceiroPedidoId,
  ProducaoPedidoId, ParceiroSiteId, DataEntrega, TurnoEntrega,
  ValorEntrega, VendedorId, GrupoVendedorId, IntegradorId, TipoIntegrador, CampanhaId) VALUES (
  :email, :dataCadastro, :tipo, :siteId, :nome, :sobrenome, :cpf,
  :dataNascimento, :sexo, :razaoSocial, :nomeFantasia, :cnpj,
  :inscricaoEstadual, :isento, :telefone, :site, :ramoAtividade,
  :cobrancaTipo, :cobrancaDestinatario, :cobrancaCep, :cobrancaEndereco,
  :cobrancaNumero, :cobrancaComplemento, :cobrancaBairro,
  :cobrancaCidade, :cobrancaEstado, :cobrancaTelefone1,
  :cobrancaTelefone2, :cobrancaCelular, :cobrancaReferencia,
  :entregaTipo, :entregaDestinatario, :entregaCep, :entregaEndereco,
  :entregaNumero, :entregaComplemento, :entregaBairro,
  :entregaCidade, :entregaEstado, :entregaTelefone1, :entregaTelefone2,
  :entregaCelular, :entregaReferencia, :cupomDescontoId,
  :valorTotal, :valorTotalFrete, :valorCupomDesconto,
  :valorCupomDescontoFrete, :ip, :statusExportacao, :valorTotalProdutos,
  :valorDespesas, :valorJuros, :valorFreteSemPromocao, :parceiroPedidoId,
  :producaoPedidoId, :parceiroSiteId, :dataEntrega, :turnoEntrega,
  :valorEntrega, :vendedorId, :grupoVendedorId, :integradorId, :tipoIntegrador, :campanhaId
)
";