<?php
if (isset($params['dadosImportacaoErroId'])) {
    $sql = "
        UPDATE " . DB_INTEGRACAO . ".DadosImportacaoErro
        SET
        MensagemErro = :mensagem,
        DataErro = :dataErro
        WHERE DadosImportacaoErroId = :dadosImportacaoErroId
      ";
} else {
    $sql = "
      INSERT INTO " . DB_INTEGRACAO . ".DadosImportacaoErro
        (DadosImportacaoId, MensagemErro, DataErro)
      VALUES
        (:dadosImportacaoId, :mensagem, :dataErro)
      ";
}
return $sql;