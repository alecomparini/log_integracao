<?php
return "
INSERT INTO " . DB_INTEGRACAO . ".Pedido_Seguro (
    DadosImportacaoId, PedidoProdutoId, SeguroTabelaId, Quantidade, ValorVenda, Familia
) VALUES (
    :dadosImportacaoId, :pedidoProdutoId, :seguroTabelaId, :quantidade, :valorVenda, :familia
)
";