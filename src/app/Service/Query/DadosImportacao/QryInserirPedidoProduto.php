<?php
return "
INSERT INTO " . DB_INTEGRACAO . ".Pedido_Produto (
    DadosImportacaoId, ProdutoId, ProdutoCodigo,
    Quantidade, ValorUnitario, ValorTotal, ValorFrete, Tipo,
    PrazoEntregaCombinado, Sequencial, ValorDesconto, ValorCustoEntrada
) VALUES (
    :dadosImportacaoId, :produtoId, :produtoCodigo,
    :quantidade, :valorUnitario, :valorTotal, :valorFrete, :tipo,
    :prazoEntregaCombinado, :sequencial, :valorDesconto, :valorCustoEntrada
)
";