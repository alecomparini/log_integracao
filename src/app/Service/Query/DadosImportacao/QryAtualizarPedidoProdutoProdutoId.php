<?php
return "
UPDATE
" . DB_INTEGRACAO . ".Pedido_Produto
SET
ProdutoId = :produtoId
WHERE
DadosImportacaoId = :dadosImportacaoId
AND PedidoProdutoId = :pedidoProdutoId
";