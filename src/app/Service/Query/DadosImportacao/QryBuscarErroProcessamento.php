<?php
return "
SELECT
    die.DadosImportacaoErroId   AS dadosImportacaoErroId,
    die.DadosImportacaoId       AS dadosImportacaoId,
    die.MensagemErro            AS mensagemErro
FROM " . DB_INTEGRACAO . ".DadosImportacaoErro die
WHERE die.DadosImportacaoId = :dadosImportacaoId
";