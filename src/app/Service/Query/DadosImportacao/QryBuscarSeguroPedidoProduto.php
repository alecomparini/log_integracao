<?php
return "
SELECT
    PedidoSeguroId,
    DadosImportacaoId,
    PedidoProdutoId,
    SeguroTabelaId,
    Quantidade,
    ValorVenda,
    Familia
FROM " . DB_INTEGRACAO . ".Pedido_Seguro ps
WHERE ps.DadosImportacaoId = :dadosImportacaoId AND ps.PedidoProdutoId = :pedidoProdutoId
";