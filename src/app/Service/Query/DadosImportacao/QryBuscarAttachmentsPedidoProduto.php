<?php
return "
SELECT
    PedidoAttachmentId,
    DadosImportacaoId,
    PedidoProdutoId,
    Tipo,
    Rotulo,
    DataCriacao
FROM " . DB_INTEGRACAO . ".Pedido_Attachment pa
WHERE pa.DadosImportacaoId = :dadosImportacaoId AND pa.PedidoProdutoId = :pedidoProdutoId
";