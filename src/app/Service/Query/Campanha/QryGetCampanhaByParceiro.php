<?php

$qry = "
    SELECT
	CA.CampanhaId,
	CA.Nome,
	CA.Status,
	CA.CatalogoId,
	CA.TipoFrete,
	CA.Desconto,
	CA.FretePonderado
    FROM " . DB_INTEGRACAO . ".Campanha CA
    JOIN " . DB_INTEGRACAO . ".Parceiro_Site_Permissao PSP ON PSP.Permissao = CA.CampanhaId AND PSP.PermissaoTipoId = 4
    JOIN " . DB_INTEGRACAO . ".Parceiro_Site PS ON PS.ParceiroSiteId = PSP.ParceiroSiteId
    WHERE
        PS.ParceiroId = :parceiroId";

return $qry;

