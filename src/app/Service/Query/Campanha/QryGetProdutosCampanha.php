<?php
$produtoWhere = "";
if (!empty($params["produtoCodigo"])) {
    $produtoWhere = "AND PR.Codigo = :produtoCodigo ";
}

$qry = "
    SELECT
	CA.CampanhaId as campanhaId,
	CA.Nome as campanha,
	CO.CatalogoId as catalogoId,
	CO.Nome as catalogo,
	PR.ProdutoId as produtoId,
	PR.Nome as produto,
        PR.Codigo as codigo
    FROM " . DB_INTEGRACAO . ".Campanha CA
    JOIN " . DB_INTEGRACAO . ".Catalogo CO ON CO.CatalogoId = CA.CatalogoId
    JOIN " . DB_INTEGRACAO . ".Catalogo_Produto CP ON CP.CatalogoId = CO.CatalogoId
    JOIN " . DB_MAGAZINE . ".Produto PR ON PR.ProdutoId = CP.ProdutoId
    JOIN " . DB_MAGAZINE . ".Produto_Site PS ON PS.ProdutoId = PR.ProdutoId
    WHERE
        PS.SiteId = :siteId
    AND CA.Status = 1
    AND CO.Status = 1
    AND PS.Status = 1
    AND CA.CampanhaId = :campanhaId
    {$produtoWhere}";

return $qry;