<?php
$where = "";
if (!empty($params["meioPagamentoId"])) {
    $where .= " AND PM.MeioPagamentoId = :meioPagamentoId";
}

$sql = "
            SELECT 
                PM.ParceiroId,
                PM.MeioPagamentoId
            FROM Parceiro_MeioPagamento PM
            WHERE ParceiroId = :parceiroId
            {$where}
       ";

return $sql;
        