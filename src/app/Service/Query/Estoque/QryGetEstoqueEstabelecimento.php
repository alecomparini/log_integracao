<?php

$sqlComp = "";

if (isset($params['produtoId']) && !empty($params['produtoId'])) {
    $sqlComp .= " AND EE.ProdutoId = {$params['produtoId']} ";
}

if (isset($params['estabelecimentoId']) && !empty($params['estabelecimentoId'])) {
    $sqlComp .= " AND E.EstabelecimentoId = {$params['estabelecimentoId']} ";
}

return "SELECT *
        FROM " . DB_MAGAZINE . ".Estabelecimento E
          INNER JOIN " . DB_MAGAZINE . ".Estoque_Estabelecimento EE ON (E.EstabelecimentoId = EE.EstabelecimentoId)
        WHERE E.Status = 1 {$sqlComp}
        ORDER BY E.Prioridade ASC";