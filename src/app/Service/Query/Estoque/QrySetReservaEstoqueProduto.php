<?php
// Atualiza reserva de estoque de um produto
return "UPDATE " . DB_MAGAZINE . ".Estoque_Estabelecimento
        SET Reservado   = Reservado + :qtdReserva,
        Disponivel  = Disponivel - :qtdReserva,
        Total       = Reservado + Disponivel - PreVenda - Virtual
        WHERE ProdutoId = :produtoId AND EstabelecimentoId = :estabelecimentoId";