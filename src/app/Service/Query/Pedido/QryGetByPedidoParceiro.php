<?php

$sql = "SELECT
          di.ParceiroPedidoId AS pedidoParceiro,
          di.ProducaoPedidoId AS pedidoProducao
        FROM " . DB_INTEGRACAO . ".DadosImportacao di
          INNER JOIN " . DB_INTEGRACAO . ".Parceiro_Site ps ON ps.ParceiroSiteId = di.ParceiroSiteId
          INNER JOIN " . DB_INTEGRACAO . ".Parceiro p ON p.ParceiroId = ps.ParceiroId
        WHERE p.ParceiroId = :parceiroId
              AND di.ParceiroPedidoId IN :pedidoParceiro";

return $sql;