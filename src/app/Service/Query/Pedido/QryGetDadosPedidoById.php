<?php

return "SELECT
          d.ParceiroPedidoId,
          ps.ParceiroId,
          ps.SiteId,
          ps.SiteBase
        FROM
          DadosImportacao d
          INNER JOIN Parceiro_Site ps on ps.ParceiroSiteId = d.ParceiroSiteId
        WHERE
          d.ProducaoPedidoId IN :pedidoIds
          AND ps.Status = 1";