<?php
// Retorna dados do Cliente e clientePJ por email, siteId e CNPJ
/** @lang mysql */
$sql =" SELECT * FROM " . DB_MAGAZINE . ".Cliente c
        INNER JOIN " . DB_MAGAZINE . ".ClientePj cp ON c.ClienteId = cp.ClienteId
        WHERE c.Email = :email
        AND c.SiteId = :siteId
        AND cp.CNPJ <> :cnpj;";

return $sql;