<?php
// Retorna dados do Cliente e clientePF por email, siteId e CPF
/** @lang mysql */
$sql =" SELECT * FROM " . DB_MAGAZINE . ".Cliente c
        INNER JOIN " . DB_MAGAZINE . ".ClientePf cp ON c.ClienteId = cp.ClienteId
        WHERE c.Email = :email
        AND c.SiteId = :siteId
        AND cp.CPF <> :cpf;";

return $sql;