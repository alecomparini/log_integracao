<?php

$campos = "COUNT(*) AS total";
$groupBy = "";

if (isset($params['groupByParceiro']) && $params['groupByParceiro']) {
    $campos = "COUNT(*) AS total, ParceiroId as parceiroId";
    $groupBy = "GROUP BY ParceiroId";
}

$sql = "SELECT
          $campos
        FROM " . DB_INTEGRACAO . ".NotificacaoParceiro
        $groupBy";

return $sql;