<?php

$updatable = ', ' . ($params['preco'] ? 'ModificacaoPreco = 1' : 'ModificacaoEstoque = 1');


$sql = "INSERT IGNORE INTO " . DB_INTEGRACAO . ".NotificacaoParceiro (
          ParceiroId, 
          SiteId, 
          ProdutoCodigo, 
          DataCriacao, 
          Tentativas, 
          ModificacaoPreco, 
          ModificacaoEstoque
        ) VALUES (
          :parceiroId, 
          :siteId, 
          :produtoCodigo, 
          NOW(), 
          0, 
          :preco, 
          :estoque)
        ON DUPLICATE KEY UPDATE Tentativas = 0 " . $updatable;
return $sql;
