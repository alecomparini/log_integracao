<?php

$sql = "INSERT IGNORE INTO " . DB_INTEGRACAO . ".NotificacaoEstornoPonto
        VALUES (:parceiroId, :pedidoParceiroId, NOW(), null, 0, :clienteDocumento, :pontos)
        ON DUPLICATE KEY UPDATE Pontos = :pontos";

return $sql;
