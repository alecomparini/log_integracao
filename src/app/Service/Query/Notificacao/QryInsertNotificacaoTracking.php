<?php

return "INSERT IGNORE INTO NotificacaoParceiro (
      ParceiroId, 
      SiteId, 
      PedidoParceiroId, 
      ProdutoCodigo, 
      DataCriacao, 
      DataUltimaTentativa, 
      Tentativas, 
      ModificacaoPreco, 
      ModificacaoEstoque, 
      ModificacaoTracking
  ) VALUES (
    :parceiroId,
    :siteId,
    :pedidoParceiroId,
    :produtoCodigo,
    NOW(),
    NULL,
    0,
    0,
    0,
    1
  )";
