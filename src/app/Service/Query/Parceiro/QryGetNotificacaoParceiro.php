<?php
//Retorna Notificacoes de Parceiros que ainda possuem tentativas
return "SELECT 
			ParceiroId,
            SiteId,
            ProdutoCodigo,
            DataCriacao,
            DataUltimaTentativa,
            Tentativas,
            ModificacaoPreco,
            ModificacaoEstoque
        FROM " . DB_INTEGRACAO . ".NotificacaoParceiro
        WHERE 
			ModificacaoTracking IS NULL
			AND Tentativas < :limite
        ORDER BY Tentativas ASC
        LIMIT :limitOffset;";