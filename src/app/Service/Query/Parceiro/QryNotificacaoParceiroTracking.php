<?php

return "SELECT 
            ParceiroId,
            SiteId,
            PedidoParceiroId,
            DataCriacao,
            DataUltimaTentativa,
            Tentativas,
            ModificacaoTracking

        FROM 
          " . DB_INTEGRACAO . ".NotificacaoParceiro 
        WHERE 
          ModificacaoTracking = 1
          AND Tentativas < :limite LIMIT 500;";

