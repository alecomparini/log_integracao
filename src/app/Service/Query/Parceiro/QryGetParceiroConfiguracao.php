<?php

$whereIn = "";

if (isset($params['configuracoes']) && count($params['configuracoes']) > 0) {
    $whereIn = "WHERE c.Chave IN :configuracoes";
}

$sql = "SELECT pc.*
        FROM
          (SELECT
             :parceiroId                        AS ParceiroId,
             c.ConfiguracaoId,
             c.Chave,
             COALESCE(pc.Valor, c.ValorDefault) AS Valor,
             COALESCE(pc.Tipo, c.TipoDefault)   AS Tipo
           FROM " . DB_INTEGRACAO . ".Configuracao c
             LEFT JOIN " . DB_INTEGRACAO . ".Parceiro_Configuracao pc ON c.ConfiguracaoId = pc.ConfiguracaoId AND pc.ParceiroId = :parceiroId
           {$whereIn}
          ) pc
          INNER JOIN " . DB_INTEGRACAO . ".Parceiro p ON p.ParceiroId = pc.ParceiroId;";

return $sql;