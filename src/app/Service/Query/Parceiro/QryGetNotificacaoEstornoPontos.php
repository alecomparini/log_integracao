<?php

return "SELECT 
            ParceiroId,
            PedidoParceiroId,
            DataCriacao,
            DataUltimaTentativa,
            Tentativas,
            ClienteDocumento,
            Pontos
          FROM " . DB_INTEGRACAO . ".NotificacaoEstornoPonto
          WHERE Tentativas < :limite LIMIT :limitOffset;";
