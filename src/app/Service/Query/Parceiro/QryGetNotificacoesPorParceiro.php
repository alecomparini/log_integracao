<?php
//Retorna Notificacoes de Parceiros que ainda possuem tentativas
return "SELECT *
        FROM " . DB_INTEGRACAO . ".NotificacaoParceiro
        WHERE Tentativas < :limite
        AND ParceiroId = :parceiroId
        ORDER BY Tentativas ASC
        LIMIT :limitOffset;";