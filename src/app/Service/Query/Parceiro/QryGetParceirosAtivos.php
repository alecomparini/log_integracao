<?php

$sql = "SELECT
          p.ParceiroId,
          ps.SiteId,
          ps.SiteBase,
          c.Chave,
          pc.Valor
        FROM
          " . DB_INTEGRACAO . ".Parceiro p
          INNER JOIN " . DB_INTEGRACAO . ".Parceiro_Site ps ON ps.ParceiroId = p.ParceiroId and ps.Status = 1
          INNER JOIN " . DB_INTEGRACAO . ".Parceiro_Configuracao pc on pc.ParceiroId = p.ParceiroId
          INNER JOIN " . DB_INTEGRACAO . ".Configuracao c on c.ConfiguracaoId = pc.ConfiguracaoId
        WHERE
          p.Status = 1
          AND c.Chave = 'ACEITA_NOTIFICACAO'
          AND pc.Valor = '1'";

return $sql;
