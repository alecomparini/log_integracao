<?php
//Retorna Notificacoes de Parceiros que ainda possuem tentativas
return "SELECT *
        FROM " . DB_INTEGRACAO . ".NotificacaoParceiro
        WHERE Tentativas >= :limite
        AND ParceiroId = :parceiroId
        AND DataUltimaTentativa <= :dataCorte
        ORDER BY Tentativas ASC
        LIMIT :limitOffset;";