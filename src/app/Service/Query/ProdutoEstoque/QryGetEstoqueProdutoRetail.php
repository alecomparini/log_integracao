<?php

$sql = "SELECT
          p.CodigoRetail,
          e.Disponivel as Disponivel,
          ps.Status,
          p.VendeAvulso as vendeAvulso,
          p.EmLinha as emLinha
          
        FROM
          " . DB_MAGAZINE . ".Produto p
          INNER JOIN ".DB_MAGAZINE.".Produto_Site ps ON p.ProdutoId = ps.ProdutoId
          LEFT JOIN " . DB_MAGAZINE . ".Estoque e ON e.ProdutoId = p.ProdutoId
        WHERE
          p.CodigoRetail IN :codigoRetail";

return $sql;
