<?php
$categoriasWhereIn = '';
$fabricantesWhereIn = '';
$joinCategoria = '';
if (!isset($params['todos']) || !$params['todos']) {
    $joinCategoria = "INNER JOIN " . DB_MAGAZINE . ".Produto_Categoria pc ON p.ProdutoId = pc.ProdutoId AND pc.Principal = 1";

    if (isset($params['categorias']) && !empty($params['categorias'])) {
        $categoriasWhereIn = ' AND pc.CategoriaId IN :categorias';
    }

    if (isset($params['fabricantes']) && !empty($params['fabricantes'])) {
        $fabricantesWhereIn = ' AND p.FabricanteId IN :fabricantes';
    }
}

$sql = "SELECT
          p.Codigo,
          p.ProdutoId as produtoId,
          IF(e.ProdutoId, MIN(e.Disponivel), pai.Disponivel) as disponivel,
          ps.PrecoDe,
          ps.PrecoPor,
          p.VendeAvulso as vendeAvulso,
          p.EmLinha as emLinha,
          
          IF (C2.Nome = 'Home', C1.CategoriaId, C2.CategoriaId) AS LojaId,
          IF (C2.Nome = 'Home', C1.Nome, C2.Nome) AS Loja,
          
          IF (C2.Nome = 'Home', C.CategoriaId, C1.CategoriaId) AS CategoriaId,
          IF (C2.Nome = 'Home', C.Nome, C1.Nome) AS Categoria,
          
          IF (C2.Nome = 'Home', '', C.CategoriaId) AS SubCategoriaId,
          IF (C2.Nome = 'Home', '', C.Nome) AS SubCategoria,
          IF (C2.Nome = 'Home', C1.Nome, C2.Nome) AS Loja,
          ps.Status

        FROM
          " . DB_MAGAZINE . ".Produto p

          INNER JOIN " . DB_MAGAZINE . ".Produto_Categoria PCC ON p.ProdutoId = PCC.ProdutoId AND PCC.Principal = 1
          INNER JOIN " . DB_MAGAZINE . ".Categoria C  ON PCC.CategoriaId = C.CategoriaId
          INNER JOIN " . DB_MAGAZINE . ".Categoria C1 ON C.CategoriaAsc = C1.CategoriaId
          INNER JOIN " . DB_MAGAZINE . ".Categoria C2 ON C1.CategoriaAsc = C2.CategoriaId
          
          {$joinCategoria}
          INNER JOIN " . DB_MAGAZINE . ".Produto_Site ps ON ps.ProdutoId = p.ProdutoId AND ps.SiteId = :siteId
          LEFT JOIN " . DB_MAGAZINE . ".Estoque pai ON pai.ProdutoId = p.ProdutoId
          LEFT JOIN " . DB_MAGAZINE . ".Combo c ON c.ProdutoId = p.ProdutoId AND c.SiteId = :siteId
          LEFT JOIN " . DB_MAGAZINE . ".Estoque e ON e.ProdutoId = c.ComboId
        WHERE p.Codigo IN :produtoCodigo
        {$categoriasWhereIn} {$fabricantesWhereIn}
        GROUP BY p.Codigo
        LIMIT :tamanhoPagina";

return $sql;