<?php
$sql = "SELECT DISTINCT
            P.Codigo AS ProdutoCodigo,
            (IFNULL(E_Produto.Disponivel,0) + IFNULL(SUM(E_Filhos.Disponivel),0)) AS EstoqueDisponivel,
            PS.Status,
            P.VendeAvulso as vendeAvulso,
            P.EmLinha as emLinha
        FROM " . DB_MAGAZINE . ".Produto P 
        INNER JOIN " . DB_MAGAZINE . ".Produto_Site PS ON P.ProdutoId = PS.ProdutoId AND PS.SiteId = :siteId
        LEFT JOIN " . DB_MAGAZINE . ".Combo C ON P.ProdutoId = C.ProdutoId AND C.SiteId = :siteId
        LEFT JOIN " . DB_MAGAZINE . ".Estoque E_Produto ON P.ProdutoId = E_Produto.ProdutoId
        LEFT JOIN " . DB_MAGAZINE . ".Estoque E_Filhos ON C.ComboId = E_Filhos.ProdutoId
        WHERE PS.Status IN (1,2)
        GROUP BY ProdutoCodigo";
return $sql;