<?php

$sql = "SELECT
          p.Codigo,
          IF(e.ProdutoId, MIN(e.Disponivel), pai.Disponivel) as disponivel,
          ps.PrecoDe,
          ps.PrecoPor,
          ps.Status,
          p.VendeAvulso as vendeAvulso,
          p.EmLinha as emLinha
        FROM
          " . DB_MAGAZINE . ".Produto p
          INNER JOIN " . DB_MAGAZINE . ".Produto_Site ps ON ps.ProdutoId = p.ProdutoId AND ps.SiteId = :siteId
          LEFT JOIN " . DB_MAGAZINE . ".Estoque pai ON pai.ProdutoId = p.ProdutoId
          LEFT JOIN " . DB_MAGAZINE . ".Combo c ON c.ProdutoId = p.ProdutoId AND c.SiteId = :siteId
          LEFT JOIN " . DB_MAGAZINE . ".Estoque e ON e.ProdutoId = c.ComboId
        WHERE
          p.EmLinha = 1
        GROUP BY p.Codigo
        LIMIT :tamanhoPagina";

return $sql;
