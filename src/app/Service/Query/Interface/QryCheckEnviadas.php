<?php
// Retorna a quantidade de interfaces não aprovadas de um determinado tipo e com estatus Enviada = 1 de um parceiro
return "SELECT COUNT(I.Dados) AS Quantidade
        FROM " . DB_INTEGRACAO . ".Interface I
        WHERE I.DataAprovacao IS NULL
        AND   I.Enviada = 1
        AND   I.ParceiroSiteId = :parceiroSiteId
        AND   I.InterfaceTipoId = :interfaceTipoId";