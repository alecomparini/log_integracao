<?php

return "SELECT I.Dados, I.InterfaceId
        FROM " . DB_INTEGRACAO . ".Interface I
        WHERE I.DataAprovacao IS NULL
        AND I.Enviada = 0
        AND I.ParceiroSiteId = :parceiroSiteId
        AND I.InterfaceTipoId = :interfaceTipoId
        ORDER BY InterfaceId ASC
        LIMIT 1000";