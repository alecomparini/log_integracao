<?php

$sql = "select
            p.ParceiroId,
            p.Nome,
            p.Status,
            p.TokenAutorizacao,
            ps.ParceiroSiteId,
            ps.SiteId,
            ps.SiteBase
        from " . DB_INTEGRACAO . ".Parceiro p
        inner join " . DB_INTEGRACAO . ".Parceiro_Site ps ON p.ParceiroId = ps.ParceiroId";
return $sql;