<?php

$sql = "SELECT
            P.ParceiroSiteId,
            C.CampanhaId,
            Ca.CatalogoId,
            Pro.Codigo
        FROM
            " . DB_INTEGRACAO . ".Parceiro_Site_Permissao P
            JOIN " . DB_INTEGRACAO . ".Campanha C ON C.CampanhaId = P.Permissao
            JOIN " . DB_INTEGRACAO . ".Catalogo Ca ON Ca.CatalogoId = C.CatalogoId
            JOIN " . DB_INTEGRACAO . ".Catalogo_Produto CP ON CP.CatalogoId = Ca.CatalogoId
            JOIN " . DB_MAGAZINE . ".Produto Pro ON Pro.ProdutoId = CP.ProdutoId
        WHERE
            C.Status = 1
            AND Ca.Status = 1
            AND P.ParceiroSiteId = :parceiroId
            AND
        ORDER BY
            C.CampanhaId DESC";

return $sql;
