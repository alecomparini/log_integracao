<?php
$sql = "SELECT
          DISTINCT Pro.Codigo
        FROM
          " . DB_INTEGRACAO . ".Parceiro_Site_Permissao P
          JOIN " . DB_INTEGRACAO . ".Parceiro_Site PS ON PS.ParceiroSiteId = P.ParceiroSiteId
          JOIN " . DB_INTEGRACAO . ".Campanha C ON C.CampanhaId = P.Permissao
          JOIN " . DB_INTEGRACAO . ".Catalogo Ca ON Ca.CatalogoId = C.CatalogoId
          JOIN " . DB_INTEGRACAO . ".Catalogo_Produto CP ON CP.CatalogoId = Ca.CatalogoId
          JOIN " . DB_MAGAZINE . ".Produto Pro ON Pro.ProdutoId = CP.ProdutoId
        WHERE
          P.PermissaoTipoId = 4
          AND C.Status = 1
          AND Ca.Status = 1
          AND PS.ParceiroId = :parceiroId";

if (!empty($params["produtoCodigo"])) {
    $sql .= " AND Pro.Codigo IN :produtoCodigo ";
}

return $sql;
