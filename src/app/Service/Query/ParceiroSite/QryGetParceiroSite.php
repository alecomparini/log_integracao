<?php
$sql = "
SELECT
  ps.ParceiroSiteId,
  ps.SiteId,
  ps.SiteBase,
  p.ParceiroId,
  p.Nome,
  p.Status,
  p.TipoParceiro,
  p.TokenAutorizacao,
  i.TipoIntegrador,
  GROUP_CONCAT(pi.IntegradorId) as integradores
FROM " . DB_INTEGRACAO . ".Parceiro_Site ps
INNER JOIN " . DB_INTEGRACAO . ".Parceiro p ON p.ParceiroId = ps.ParceiroId
LEFT JOIN (
    " . DB_INTEGRACAO . ".Parceiro_Integrador pi
    INNER JOIN " . DB_INTEGRACAO . ".Integrador i ON i.IntegradorId = pi.IntegradorId
                                                 AND i.Status = 1 
) ON pi.ParceiroId = p.ParceiroId

WHERE p.ParceiroId = :parceiro_id
AND ps.SiteId = :site_id

GROUP BY        
  ps.ParceiroSiteId,
  ps.SiteId,
  ps.SiteBase,
  p.ParceiroId,
  p.Nome,
  p.Status,
  p.TipoParceiro,
  p.TokenAutorizacao,
  i.TipoIntegrador
";
return $sql;