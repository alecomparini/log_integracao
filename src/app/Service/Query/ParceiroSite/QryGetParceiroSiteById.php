<?php
$sql = "SELECT
          ps.ParceiroSiteId,
          ps.SiteId,
          ps.SiteBase,
          p.ParceiroId,
          p.Nome,
          p.Status,
          p.TipoParceiro,
          p.TokenAutorizacao

        FROM " . DB_INTEGRACAO . ".Parceiro_Site ps
        INNER JOIN " . DB_INTEGRACAO . ".Parceiro p ON p.ParceiroId = ps.ParceiroId
        WHERE ps.ParceiroId = :parceiroId;";
return $sql;