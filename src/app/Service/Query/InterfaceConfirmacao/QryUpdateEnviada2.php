<?php
//Atualiza Interface(s) com status Enviada = 2 e com DataAprovacao
return "UPDATE " . DB_INTEGRACAO . ".Interface
        SET DataAprovacao = NOW(), Enviada = 2
        WHERE InterfaceId IN :interfaces;";