<?php

$sql = "SELECT IFC.InterfaceId
        FROM " . DB_INTEGRACAO . ".Interface IFC
        INNER JOIN " . DB_INTEGRACAO . ".Parceiro_Site PS ON PS.ParceiroSiteId = IFC.ParceiroSiteId
        INNER JOIN " . DB_INTEGRACAO . ".InterfacePermissao IP ON IP.ParceiroSiteId = IFC.ParceiroSiteId
        INNER JOIN " . DB_INTEGRACAO . ".InterfaceTipo IT ON IT.InterfaceTipoId = IFC.InterfaceTipoId
        WHERE PS.ParceiroId = :parceiroId
        AND PS.SiteId = :siteId
        AND IFC.InterfaceTipoId = :interfaceTipo
        AND IFC.Enviada = 1";

return $sql;