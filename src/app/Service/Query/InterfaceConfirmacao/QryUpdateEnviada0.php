<?php
//Atualiza Interface(s) com status Enviada = 0 e sem DataAprovacao
return "UPDATE " . DB_INTEGRACAO . ".Interface
        SET DataAprovacao = NULL, Enviada = 0
        WHERE InterfaceId IN :interfaces;";