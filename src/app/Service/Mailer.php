<?php
namespace App\Service;

use App\Adapter\Email\Contracts\IEmailAdapter;
use App\Adapter\Email\EmailAdapterFactory;
use App\Core\Config;
use App\Exceptions\FrameworkException;
use App\Exceptions\ValidacaoException;
use App\Helper\Validacao;

/**
 * Class Mailer
 *
 * @package App\Service
 */
class Mailer {
    /** @var string */
    private $from;
    /** @var array */
    private $to = array();
    /** @var string */
    private $body;
    /** @var string */
    private $subject;
    /** @var bool */
    private $isHtml;
    /** @var IEmailAdapter */
    private $adapter;

    public function __construct() {
        $this->setIsHtml(false);
        $this->setAdapter(EmailAdapterFactory::create(Config::get('email.strategy')));
    }

    public function setAdapter(IEmailAdapter $adapter) {
        $this->adapter = $adapter;
    }

    public function setFrom($from, $name = null) {
        if (!Validacao::validar($from, 'email')) {
            throw new ValidacaoException(Validacao::$lastMsg);
        }
        $this->from = $from;
        $this->adapter->setFrom($from, $name);

        return $this;
    }

    public function getFrom() {
        return Config::get('email.from.email');
    }

    public function addTo($toParam, $name = '') {
        if (!Validacao::validar($toParam, 'email')) {
            throw new ValidacaoException(Validacao::$lastMsg);
        }

        if (!in_array($toParam, $this->to)) {
            $this->to[] = $toParam;
            $this->adapter->addTo($toParam, $name);
        }

        return $this;
    }

    public function removeTo($toParam) {
        if (!Validacao::validar($toParam, 'email')) {
            throw new ValidacaoException(Validacao::$lastMsg);
        }

        if (in_array($toParam, $this->to) && ($key = array_search($this->to, $toParam) !== false)) {
            unset($this->to[$key]);
        }

        return $this;
    }

    public function getTo() {
        return $this->to;
    }

    public function setBody($body) {
        $this->body = $body;
        $this->adapter->setBody($body);

        return $this;
    }

    public function getBody() {
        return $this->body;
    }

    public function setSubject($subject) {
        $this->subject = $subject;
        $this->adapter->setSubject($subject);

        return $this;
    }

    public function getSubject() {
        return $this->subject;
    }

    public function setIsHtml($boolean) {
        $this->isHtml = $boolean;

        return $this;
    }

    public function isHtml() {
        return $this->isHtml;
    }

    public function send() {
        $regras = array('body' => array('required', 'notempty'), 'subject' => array('required', 'notempty'), 'to' => array('required', 'array', 'notempty'), 'from' => array('required', 'notempty'));
        $validacao = new Validacao($regras);
        if (!$validacao->executaValidacao($this)) {
            throw new FrameworkException("Erro ao enviar e-mail: " . $validacao->msg);
        }

        return $this->adapter->send(false);
    }

    /**
     * Adiciona um arquivo anexo ao e-mail
     *
     * @param string $path Caminho do arquivo a ser enviado (Deve ser completo)
     * @param string $name Nome do arquivo ao enviar
     *
     * @return $this
     */
    public function addAttachment($path, $name = '') {
        $this->adapter->addAttachment($path, $name);
        return $this;
    }
}