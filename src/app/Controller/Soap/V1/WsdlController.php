<?php

namespace App\Controller\Soap\V1;

use App\Core\Controller\SoapController;
use SoapFault;
use App\Core\Logger;
use App\Exceptions\FrameworkException;

/**
 * Class WsdlController
 * @package App\Controller\Soap\V1
 */
class WsdlController extends SoapController {
    protected $soapClass;

    /**
     * @param $class
     * @param bool $wsdl
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index($class, $wsdl = false) {
        //Prepara o nome da classe chamada via SOAP
        $this->soapClass = ucfirst($class);
        $response = $this->serve($wsdl);

        $call = json_encode(array(
            'input' => $this->getRawBody(),
            'output' => $response,
        ));

        //Remove os dados de cartão para padrão PCI
        $call = preg_replace('/(<CartaoCodSeguranca>)(.*?)(<\\\\\\/CartaoCodSeguranca>)/mi', '$1***$3', $call);
        $call = preg_replace('/(<CartaoNumero>)(.*?)(<\\\\\\/CartaoNumero>)/mi', '$1****************$3', $call);
        $call = preg_replace('/(<CartaoValidade>)(.*?)(<\\\\\\/CartaoValidade>)/mi', '$1*******$3', $call);


        Logger::getInstance()->log($call);

        return $this->xml($response);
    }
}
