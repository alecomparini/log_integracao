<?php
namespace App\Controller;

use App\Core\Controller\Controller;
use Symfony\Component\HttpFoundation\Response;

class AssetController extends Controller {
    public function getAsset($module, $type, $file) {
        if ($type === 'js') {
            $file = SRC_DIR . 'assets' . DS . 'modules' . DS . $module . DS . $type . DS . $file;
            //header('content-type: application/javascript');
            //readfile($file);
            $response = new Response(file_get_contents($file), 200);
            $response->headers->set('content-type', 'application/javascript');
            return $response;
        }
    }
}