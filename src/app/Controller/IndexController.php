<?php

namespace App\Controller;

use App\Core\Controller\Controller;

/**
 * Class IndexController
 * @package App\Controller
 */
class IndexController extends Controller {

    /**
     * Método responsável por buscar nada
     * É só um teste de teste a ser testado
     * @return mixed Muita coisa pode ser retornada aqui
     */
    public function index() {
        return $this->view('permissiondenied');
    }
}
