<?php

namespace App\Controller;

use App\BO\UsuarioBO;
use App\Core\Controller\RestController;
use App\Facades\Auth;

class TestController extends RestController {
    public function index() {
        return $this->json(Auth::getLoggedInUser());
    }

    public function post() {

    }
}