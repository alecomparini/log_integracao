<?php

namespace App\Controller;

use App\BO\NotificacaoParceiroBO;
use App\BO\ParceiroConfiguracaoBO;
use App\BO\ParceiroSiteBO;
use App\BO\RelatorioBO;
use App\Core\Config;
use App\Core\Controller\RestController;
use App\Core\Logger;
use App\Core\View;
use App\Exceptions\BusinessException;
use App\Helper\Utils;
use App\Model\Configuracao;
use App\Model\NotificacaoParceiro;
use App\Service\Mailer;
use DateTime;
use Exception;

class ReportsController extends RestController {
    public function estoque() {
        $exibeSaida = $this->request->get('exibeSaida', true);

        // Define o tempo máximo para geração = 15 minutos
        set_time_limit(60 * 30);

        // Define o tamanho máximo de memória para 2GB
        ini_set('memory_limit', '2048M');

        $parceiroId = $this->request->query->get('parceiroId');
        $relatorioBO = new RelatorioBO();

        $pagina = $this->request->query->get('pagina', 1);
        $codigo = $this->request->query->get('codigo', array());
        if ($exibeSaida) {
            header("content-type: text/html; charset=utf-8");
            ob_clean();

            echo "<!doctype html>";
            echo "<head>";
            echo '<meta charset="UTF-8">';
            echo "<title>Relatório</title></head>";
            echo "<body>";
            echo "<h1>Relatório Estoque</h1>";
            echo "<table class=\"table table-stripped table-bordered table-hover table-condensed\">";

            echo "<tr>";
            echo "<td>SKU MV</td>";
            echo "<td>SKU PCO</td>";
            echo "<td>QTD MV</td>";
            echo "<td>QTD PCO</td>";
            echo "<td>DIFF</td>";
            echo "<td>OBS</td>";
            echo "<td>PROD. ID</td>";
            echo "<td>NOME</td>";
            echo "</tr>";
            ob_flush();
        }

        $paginaAtual = $pagina;
        $retentativas = 0;

        $parceiroBO = new ParceiroSiteBO();
        $notificacaoBO = new NotificacaoParceiroBO();
        $configBO = new ParceiroConfiguracaoBO();

        $confLimiarDesbalanceio = $configBO->getConfiguracoes($parceiroId, Configuracao::RELATORIO_ESTOQUE_DIFERENCA_GERAR_NOTIFICACAO, true);
        $parceiro = $parceiroBO->getParceiroSiteById($parceiroId);

        if (!$parceiro) {
            throw new BusinessException('Parceiro não encontrado com a ID ' . $parceiroId);
        }

        $codigosComDesbalanceio = array();
        do {
            $codigosComDesbalanceioQueGeramNotificacao = array();
            try {
                $results = $relatorioBO->gerarRelatorioEstoque($parceiroId, $codigo, $paginaAtual);
                $dadosPaginacao = $results['paginacao'];
                $temMaisPaginas = $paginaAtual < $dadosPaginacao['totalPaginas'];

                /** @var \App\Model\EstoqueParceiro $estoque */
                foreach ($results['estoques'] as $estoque) {
                    if (null !== $estoque->skuParceiro) {

                        if ($estoque->getDiferenca() !== 0) {
                            $codigosComDesbalanceio[] = $estoque;
                            if (abs($estoque->getDiferenca()) >= $confLimiarDesbalanceio->valor) {
                                $codigosComDesbalanceioQueGeramNotificacao[] = $estoque->codigo;
                            }
                        }
                        if ($exibeSaida) {
                            echo "<tr>";
                            echo "<td>{$estoque->codigo}</td>";
                            echo "<td>{$estoque->skuParceiro}</td>";
                            echo "<td>{$estoque->quantidadeMV}</td>";
                            echo "<td>{$estoque->quantidadeParceiro}</td>";
                            echo sprintf("<td>%d</td>", $estoque->getDiferenca());
                            echo "<td>OBS</td>";
                            echo "<td>{$estoque->produtoId}</td>";
                            echo Utils::utf8Convert("<td>{$estoque->nome}</td>");
                            echo "</tr>";
                        }
                    } else {
                        Logger::getInstance()->error("Não foi encontrado SKU parceiro para o código ({$estoque->codigo})");
                    }
                    ob_flush();
                }
                ob_flush();

                if (count($codigosComDesbalanceioQueGeramNotificacao) > 0) {
                    $notificacaoBO->setNotificacao($parceiroId, $codigosComDesbalanceioQueGeramNotificacao, NotificacaoParceiro::NOTIFICACAO_ESTOQUE);
                }
                $paginaAtual++;
                $retentativas = 0;
            } catch (Exception $e) {
                $retentativas++;
                if ($retentativas >= 3) {
                    $paginaAtual++;
                }
                Logger::getInstance()->exception($e);
            }
        } while ($temMaisPaginas);

        if ($exibeSaida) {
            echo "</table>";
            echo "</body>";
            echo "</html>";
            ob_end_clean();
        } else {
            echo "OK";
        }

        $this->enviaEmailCodigosDesbalanceio($parceiro, $codigosComDesbalanceio);
    }

    /**
     * Dispara o e-mail
     *
     * @param \App\Model\ParceiroSite $parceiro
     * @param \App\Model\EstoqueParceiro[] $codigosComDesbalanceio
     *
     * @throws \App\Exceptions\FrameworkException
     */
    private function enviaEmailCodigosDesbalanceio($parceiro, $codigosComDesbalanceio) {
        $view = $this->view('emails.relatorioDesbalanceio', array(
            'estoques' => $codigosComDesbalanceio
        ));
        $hoje = new DateTime('now');

        $mailer = new Mailer();

        $destinatariosErros = Config::get('destinatarios_erros');
        /** @var array $destinatariosErros */
        foreach ($destinatariosErros as $destinatario) {
            $mailer->addTo($destinatario[0], $destinatario[1]);
        }

        $mailer
            ->setSubject("Relatório desbalanceio: Parceiro ({$parceiro->nome}) em {$hoje->format('d/m/Y H:i:s')}")
            ->setBody($view->render())
            ->send();
    }
}
