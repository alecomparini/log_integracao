<?php
namespace App\Controller;

use App\BO\TokenBO;
use App\BO\UsuarioBO;
use App\Core\Config;
use App\Core\Controller\RestController;
use App\Core\Logger;
use App\Exceptions\BusinessException;
use App\Exceptions\ValidacaoException;
use App\Facades\Auth;
use App\Helper\Http;
use App\Model\Response;

class LoginController extends RestController {
    public function index() {
        $this->forceHttps();

        return $this->view('login');
    }

    public function accessToken() {
        try {
            $email = $this->request->query->get('email');
            $senha = $this->request->query->get('senha');

            $usuarioBO = new UsuarioBO();
            $usuario = $usuarioBO->buscarUsuarioPorEmailESenha($email, $senha);

            if (null === $usuario) {
                throw new BusinessException('Usuário não encontrado com o email ' . $email, Response::USUARIO_SENHA_INVALIDO);
            }

            $tokenBO = new TokenBO();
            $token = $tokenBO->createTokenUsuario($usuario);

            $this->setResponseData($token)
                ->setResponseSuccess(true)
                ->setResponseCode(Response::OK)
                ->setHttpCode(200);
        } catch (BusinessException $be) {
            Logger::getInstance()->exception($be);
            $this->setResponseCode($be->getCode())
                ->setResponseMessage($be->getMessage())
                ->setHttpCode(Http::ERRO_PRECONDICAO);
        } catch (ValidacaoException $ve) {
            $this->setResponseCode($ve->getCode())
                ->setResponseMessage($ve->getMessage())
                ->setHttpCode(Http::ERRO_PRECONDICAO);
        }

        return $this->getResponse();
    }

    public function criarTokenEstendido() {
        try {
            $usuarioLogado = Auth::getLoggedInUser();
            $validade = $this->request->request->get('validadeEmSegundos', Config::get('segundos_expirar_token'));
            $tokenBO = new TokenBO();
            $token = $tokenBO->createTokenUsuario($usuarioLogado, $validade);
            $this->setResponseData($token)
                ->setResponseSuccess(true)
                ->setResponseCode(Response::OK)
                ->setHttpCode(200);
        } catch (BusinessException $be) {
            Logger::getInstance()->exception($be);
            $this->setResponseCode($be->getCode())
                ->setResponseMessage($be->getMessage())
                ->setHttpCode(Http::ERRO_PRECONDICAO);
        } catch (ValidacaoException $ve) {
            $this->setResponseCode($ve->getCode())
                ->setResponseMessage($ve->getMessage())
                ->setHttpCode(Http::ERRO_PRECONDICAO);
        }

        return $this->getResponse();
    }

    public function invalidarAccessToken() {
        $tokenBO = new TokenBO();
        $tokenBO->invalidarAccessToken(Auth::getAccessToken());

        return $this->setHttpCode(200)
            ->setResponseCode(Response::OK)
            ->setResponseData(array('token' => Auth::getAccessToken()))
            ->getResponse();
    }
}