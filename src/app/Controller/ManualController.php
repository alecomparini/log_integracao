<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 12/01/2016
 * Time: 11:44
 */

namespace App\Controller;


use App\Core\Controller\Controller;

use App\Model\Response;


/**
 * Class ManualController
 * @package App\Controller
 */
class ManualController extends Controller {
    public function index() {
        return $this->v1();
    }

    /**
     * Exibe o manual versão 1 da integração
     * @return \App\Core\View
     */
    public function v1() {
        return $this->view('documentacao.v1.index', array(
            'httpHost' => $this->request->getSchemeAndHttpHost()
        ));
    }

    /**
     * Exibe o manual versão 2 da integração
     * @return \App\Core\View
     */
    public function v2() {
        $response = new Response('', 0);
        $erros = $response->getResponses();
        return $this->view('documentacao.v2.index', array(
            'httpHost' => $this->request->getSchemeAndHttpHost(),
            'erros' => $erros
        ));
    }
}