<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 11/01/2016
 * Time: 10:05
 */

namespace App\Controller;

use App\Core\Controller\RestController;
use App\Core\Exception\PhpRuntimeException;
use App\Exceptions\AuthenticationException;
use App\Exceptions\AuthorizationException;
use App\Exceptions\BusinessException;
use App\Exceptions\DatabaseException;
use App\Exceptions\PageNotFoundException;
use App\Model\Response;

/**
 * Class ErrorController
 * @package App\Controller
 */
class ErrorController extends RestController {
    public function notFound() {
        $data = array(
            'request_uri' => $this->request->getRequestUri()
        );
        if ($this->request->headers->get('content-type') == 'application/json') {
            return $this->json(new Response($data['request_uri'], 6));
        }
        return $this->view('notfound', $data);
    }

    public function runtimeError(PhpRuntimeException $errEx) {
        return $this->view('erro.runtime', array('ex' => $errEx));
    }

    /**
     * @param DatabaseException $errEx
     * @return \App\Core\View
     */
    public function databaseError(DatabaseException $errEx) {
        $params = array('ex' => $errEx);

        if (!$this->startsWith('/v2')) {
            $response = $this->view('erro.database', $params, 500);
        } else {
            $response = $this->setHttpCode(500)
                ->setResponseCode(19)
                ->setResponseMessage($errEx->getMessage())
                ->setResponseData(array())
                ->setResponseSuccess(false)
                ->getResponse();
        }
        return $response;
    }

    public function authorizationError(AuthorizationException $ex) {
        return $this->setHttpCode(403)
            ->setResponseCode(Response::PERMISSAO_NEGADA)
            ->setResponseMessage($ex->getMessage())
            ->setResponseData(array())
            ->setResponseSuccess(false)
            ->getResponse();
    }

    public function authenticationError(AuthenticationException $ex) {
        return $this->setHttpCode(Response::NOT_AUTHENTICATED)
            ->setResponseCode(Response::NOT_AUTHENTICATED)
            ->setResponseMessage($ex->getMessage())
            ->setResponseData(array())
            ->setResponseSuccess(false)
            ->getResponse();
    }

    public function businessError(BusinessException $be) {
        return $this->setResponseSuccess(false)
            ->setResponseCode($be->getCode())
            ->setResponseMessage($be->getMessage())
            ->setResponseData(null)
            ->getResponse();
    }

    public function pageNotFound(PageNotFoundException $e) {
        return $this->view('notfound', array('mensagem' => $e->getMessage()));
    }
}