<?php
namespace App\Controller\Rest;

use App\BO\NotificacaoParceiroBO;
use App\Core\Controller\RestController;
use App\Exceptions\AuthorizationException;

/**
 * @author: William Okano <williamokano@softbox.com.br>
 * @createdAt: 04-05-2016 10:38
 * @package: App\Controller\Rest
 */
class EstatisticasController extends RestController {

    public function index() {
        $this->verificaPermissao(__FUNCTION__);
        return $this->view('estatisticas.notificacoes');
    }

    /**
     * Busca os dados de quantas notificacoes faltam enviar
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws AuthorizationException
     * @throws \App\Exceptions\ValidacaoException
     */
    public function notificacoesRestante() {
        $this->verificaPermissao(__FUNCTION__);
        $notificacaoBO = new NotificacaoParceiroBO();
        $groupByParceiro = $this->request->query->get('groupByParceiro', 0) == 1;

        return $this->json($notificacaoBO->getTotalNotificacoes($groupByParceiro));
    }

    /**
     * Busca os dados de quantas notificacoes deram erro e faltam enviar
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function notificacoesErro() {
        $this->verificaPermissao(__FUNCTION__);
        $notificacaoBO = new NotificacaoParceiroBO();
        return $this->json($notificacaoBO->getTotalNotificacoesErro());
    }

    private function verificaPermissao($method) {
        if (!$this->request->query->has('access_token') ||
            $this->request->query->get('access_token') != 's0ftb0x') {
            throw new AuthorizationException('Sem permissão de acesso ao método ' . $method);
        }
    }
}