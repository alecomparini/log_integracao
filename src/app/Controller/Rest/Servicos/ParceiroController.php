<?php
namespace App\Controller\Rest\Servicos;

use App\Adapter\SkuParceiro\SkuParceiroAdapterFactory;
use App\BO\ParceiroConfiguracaoBO;
use App\Core\Controller\RestController;
use App\Exceptions\BusinessException;
use App\Helper\Http;
use App\Library\VtexSDK;
use App\Model\Configuracao;
use App\Model\Response;

/**
 * Class ParceiroController
 *
 * @package App\Controller\Rest\Servicos
 */
class ParceiroController extends RestController {
    /**
     * @param $parceiroId
     * @param $codigo
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getSkuParceiro($parceiroId, $codigo) {
        try {
            $endpoint = $this->getParam('endpoint');
            $response = Http::get($endpoint . $codigo);

            if ($response->responseCode === Http::OK) {
                $skuParceiro = trim($response->body, '"');
            } else if ($response->responseCode === Http::NOT_FOUND) {
                $data = json_decode($response->body);
                if (json_last_error() === JSON_ERROR_NONE) {
                    throw new BusinessException("Exceção VTEX: " . $data->error->message . " [$codigo]", Response::ERRO_CONSULTA_SKU_PARCEIRO);
                } else {
                    throw new BusinessException('Falha do decodificar JSON: Consulta SKU VTEX', Response::ERRO_CONSULTA_SKU_PARCEIRO);
                }
            }

            $this->setHttpCode(200)
                ->setResponseCode(Response::OK)
                ->setResponseData(array(
                    'codigo' => $codigo,
                    'sku' => $skuParceiro
                ))
                ->setResponseSuccess(true);
        } catch (BusinessException $be) {
            $this->setHttpCode(500)
                ->setResponseData(array(
                    'codigo' => $codigo
                ))
                ->setResponseMessage($be->getMessage())
                ->setResponseCode($be->getCode())
                ->setResponseSuccess(false);
        }

        return $this->getResponse();
    }
}
