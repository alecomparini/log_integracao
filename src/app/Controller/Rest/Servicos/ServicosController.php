<?php
namespace App\Controller\Rest\Servicos;

use App\BO\DBLockBO;
use App\BO\NotificacaoParceiroBO;
use App\BO\ParceiroConfiguracaoBO;
use App\BO\ParceiroSiteBO;
use App\BO\PedidoBO;
use App\BO\ServicoBO;
use App\Core\Benchmark;
use App\Core\Config;
use App\Core\Controller\Controller;
use App\Core\Controller\RestController;
use App\Core\Logger;
use App\DAO\ParceiroDAO;
use App\Exceptions\BusinessException;
use App\Facades\Auth;
use App\Helper\Http;
use App\Model\Configuracao;
use App\Model\ParceiroConfiguracao;
use App\Model\Response;
use App\Model\V2\StatusExportacaoPedido;
use App\Service\JobPoolExecutor;
use App\DAO\NotificacaoEstornoPontosDAO;
use App\Model\NotificacaoParceiro;
use Mockery\CountValidator\Exception;

/**
 * Class ServicosController
 *
 * Classe reponsável por fornecer serviços internos, para outros produtos da MV.COM
 *
 * @package App\Controller\Rest\Servicos
 */
class ServicosController extends RestController {
    /**
     * Método responsável por gerar registros de notificação a serem enviadas para os parceiros
     */
    public function setNotificacao() {
        $produtos = $this->getParam('produtos');
        $tipoNotificacao = $this->getParam('tipoNotificacao');
        $paramsLog = array('produtos' => $produtos, 'tipoNotificacao' => $tipoNotificacao);
        //Logger::getInstance('api_gateway')->log('setNotificacao', (array)$this->body);
        $servicoBO = new ServicoBO();

        try {
            $resultado = $servicoBO->setNotificacao($produtos, $tipoNotificacao);
        } catch (\Exception $e) {
            //Logger::getInstance('api_gateway')->error('setNotificacao', array('exception' => $e));
            $resp = array('Exception' => array('Codigo' => $e->getCode(), 'Mensagem' => $e->getMessage()));
            Logger::getInstance("api_log")->log("[API:Setar Notificação] [Parceiro:00] [Mensagem_tojs:" . json_encode(Logger::getContextData(__METHOD__,$paramsLog , $resp)) ."]");
            return $this->json(new Response($e->getMessage(), $e->getCode()), 412);
        }

        if ($resultado) {
            // Logger::getInstance('api_gateway')->log('setNotificacao: SUCESSO');
            $resp = array('Mensagem' => 'Notificações registradas com sucesso');
            $return = $this->json(new Response('Notificações registradas com sucesso', 0, array(), true), 200);
        } else {
            //Logger::getInstance('api_gateway')->log('setNotificacao: SEM NOTIF');
            $resp = array('Mensagem' => 'Os itens informados não necessitam de notificações');
            $return = $this->json(new Response('', 20, array(), true), 200);
        }

        Logger::getInstance("api_log")->log("[API:Setar Notificação] [Parceiro:00] [Mensagem_tojs:" . json_encode(Logger::getContextData(__METHOD__,$paramsLog , $resp)) ."]");

        return $return;
    }

    /**
     * Notifica parceiros sobre alterações no estoque e/ou preço dos produtos
     */
    public function notificar() {
        $host = Config::get('services.tarefas.host');
        $port = Config::get('services.tarefas.port');

        $pool = new JobPoolExecutor(5, 1,
                $host ? $host : $this->request->getHost(),
                $port ? $port : $this->request->getPort(),
                '', '');

        //Adiciona fila rápida
        $pool->addJob('/servicos/notificarFilaRapida', $this->request->query->all(), 'fila-rapida', false, false);

        //Adiciona fila lenta
        $pool->addJob('/servicos/notificarFilaLenta', $this->request->query->all(), 'fila-lenta', false, false);

        $debug = $this->request->query->get('debug') == 's0ftb0x';
        if ($debug) {
            $pool->processAll();

            return $this->json($pool->getAllResponses());
        }

        return $this->text('done');
    }

    public function notificarFilaRapida() {
        $limite = $this->request->query->get('limite', 250);
        $bo = new NotificacaoParceiroBO();
        $notificacoes = $bo->getNotificacoes($limite);

        return $this->notificarFila($notificacoes, 'notificar-fila-rapida');
    }

    public function notificarFilaLenta() {
        $limite = $this->request->query->get('limite', 250);
        $bo = new NotificacaoParceiroBO();
        $notificacoes = $bo->getNotificacoesExpiradas($limite);

        return $this->notificarFila($notificacoes, 'notificar-fila-lenta');
    }

    private function notificarFila($notificacoes, $label = 'notificar-geral') {
        $response = DBLockBO::getLock($label, 0);
        if ($response->ok != 1) {
            return $this->text('Outra instância já em execução', 200);
        }
        Benchmark::start('notificar');
        $bo = new NotificacaoParceiroBO();
        $debug = $this->request->query->get('debug') === 's0ftb0x';
        $bo->debug = $debug;

        $parceiroDAO = new ParceiroDAO();
        $parceirosAtivos = $parceiroDAO->getParceiroAtivo();

        $configs = array();
        $pcoCfgBO = new ParceiroConfiguracaoBO();
        foreach ($parceirosAtivos as $parceiro) {
            $configs[$parceiro->parceiroId] = $pcoCfgBO->getConfiguracoes($parceiro->parceiroId, Configuracao::URL_NOTIFICACAO, true);
        }

        $poolSize = Config::get('services.notifications.poolsize', 50);
        $host = Config::get('services.notifications.host');
        $port = Config::get('services.notifications.port', 80);
        $timeout = Config::get('services.notifications.timeout', 1);
        $username = Config::get('services.notifications.username', 'william');
        $password = Config::get('services.notifications.password', 'william');

        $pool = new JobPoolExecutor($poolSize, $timeout, $host, $port, $username, $password);
        $pool->setIsJson(true);
        $pool->setHttpMethod('POST');

        foreach ($notificacoes as $notificacao) {
            $params = array(
                'url' => $configs[$notificacao->parceiroId]->valor,
                'notificacao' => $notificacao,
            );
            $pool->addJob('/servicos/enviarNotificacao', $params);
        }

        $pool->processAll();
        $respostas = $pool->getAllResponses();

        $tempoExecucao = Benchmark::end('notificar', true);

        DBLockBO::releaseLock($label);
        if ($debug) {
            $output = implode('-----------------' . PHP_EOL, $respostas);
            $output .= 'Tempo execução total: ' . $tempoExecucao;

            return $this->text($output);
        } else {
            return $this->text('ok');
        }
    }

    public function enviarNotificacao() {
        $notificacao = $this->getParam('notificacao');
        $url = $this->getParam('url');

        $notificacaoBO = new NotificacaoParceiroBO();
        $response = $notificacaoBO->enviarNotificacao($notificacao, $url);

        return $this->text($response);
    }

    /**
     * Notifica parceiros sobre alteração nas informações de tracking dos seus pedidos
     */
    public function notificarTracking() {
        $bo = new NotificacaoParceiroBO();
        $bo->notificarTrackingParceiros();
    }

    /**
     * Método responsável por gerar registros de notificação de tracking para serem enviados para os parceiros
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    //public function setNotificacaoTracking() {
    //    $pedidos = $this->getParam('pedidos');
    //    $servicoBO = new ServicoBO();
    //
    //    try {
    //        $resultado = $servicoBO->setNotificacaoTracking($pedidos);
    //    } catch (\Exception $e) {
    //        return $this->json(new Response($e->getMessage(), $e->getCode()), 412);
    //    }
    //
    //    if ($resultado) {
    //        return $this->json(new Response('Notificações registradas com sucesso', 0, array(), true), 200);
    //    } else {
    //        return $this->json(new Response('', 20), 412);
    //    }
    //}

    public function enviarNotificaoEstornoPontos() {
        $notificacao = $this->getParam('notificacao');
        $url = $this->getParam('url');

        $notificacaoBO = new NotificacaoParceiroBO();
        $response = $notificacaoBO->enviarNotificacaoEstornoPontos($notificacao, $url);

        return $this->text($response);
    }

    public function notificarEstornoPontos() {
        $response = DBLockBO::getLock('enviar-notificacoes-pontos', 0);
        if ($response->ok != 1) {
            return $this->text('Outra instância já em execução', 200);
        }
        Benchmark::start('notificar pontos');
        $bo = new NotificacaoParceiroBO();
        $debug = $this->request->query->get('debug') === 's0ftb0x';
        $bo->debug = $debug;

        $parceiroDAO = new ParceiroDAO();
        $parceirosAtivos = $parceiroDAO->getParceiroAtivo();

        $limite = $this->request->query->get('limite', 50);
        $notificacaoEstornoPontosDAO = new NotificacaoEstornoPontosDAO();
        $notificacoes = $notificacaoEstornoPontosDAO->getNotificacoesEstornoPontos(NotificacaoParceiro::MAX_TENTATIVA_NOTIFICACAO, $limite);

        $configs = array();
        $pcoCfgBO = new ParceiroConfiguracaoBO();
        foreach ($parceirosAtivos as $parceiro) {
            $configs[$parceiro->parceiroId] = $pcoCfgBO->getConfiguracoes($parceiro->parceiroId, Configuracao::URL_NOTIFICAO_ESTORNO_PONTOS, true);
        }

        $poolSize = Config::get('services.notifications.poolsize', 50);
        $host = Config::get('services.notifications.host', $this->request->getHost());
        $port = Config::get('services.notifications.port', 80);
        $timeout = Config::get('services.notifications.timeout', 1);
        $username = Config::get('services.notifications.username', 'william');
        $password = Config::get('services.notifications.password', 'william');

        $pool = new JobPoolExecutor($poolSize, $timeout, $host, $port, $username, $password);
        $pool->setIsJson(true);
        $pool->setHttpMethod('POST');

        foreach ($notificacoes as $notificacao) {
            $params = array(
                'url' => $configs[$notificacao->parceiroId]->valor,
                'notificacao' => $notificacao,
            );
            $pool->addJob('/servicos/enviarNotificaoEstornoPontos', $params);
        }

        $pool->processAll();
        $respostas = $pool->getAllResponses();

        $tempoExecucao = Benchmark::end('notificar', true);

        DBLockBO::releaseLock('enviar-notificacoes-pontos');
        if ($debug) {
            $output = implode('-----------------' . PHP_EOL, $respostas);
            $output .= 'Tempo execução total: ' . $tempoExecucao;

            return $this->text($output);
        } else {
            return $this->text('ok');
        }
    }

    public function setNotificacaoEstornoPontos() {
        $notificacoesPonto = $this->getParam('notificacoesPonto');
        $servicoBO = new ServicoBO();

        try {
            $resultado = $servicoBO->setNotificacaoEstornoPonto($notificacoesPonto);
        } catch (\Exception $e) {
            return $this->json(new Response($e->getMessage(), $e->getCode()), 412);
        }

        if ($resultado) {
            return $this->json(new Response('Notificações registradas com sucesso', 0, array(), true), 200);
        } else {
            return $this->json(new Response('', 20), 412);
        }
    }

    public function processarPrePedidos() {
        $this->verificaPermissao();

        $pedidoBO = new PedidoBO();
        $prePedidos = $pedidoBO->buscarPrePedidos();
        foreach ($prePedidos as $pedido) {
            try {
                $pedidoBO->processarPedido($pedido);
            } catch (\Exception $e) {

            }
        }

        return $this->text('ok');
    }

    public function buscarPedidosComErro() {
        $pedidoBO = new PedidoBO();

        return $this->setResponseCode(0)
            ->setHttpCode(200)
            ->setResponseData($pedidoBO->buscarPedidosErroExportacao())
            ->getResponse();
    }

    public function processarPrePedido($dadosImportacaoId) {
        $this->verificaPermissao();

        $pedidoBO = new PedidoBO();
        $pedido = $pedidoBO->buscarPedidoPorDadosImportacaoId($dadosImportacaoId);
        $statusValidosProcessamento = array(
            StatusExportacaoPedido::PRE_PEDIDO,
            StatusExportacaoPedido::PRE_PEDIDO_ERRO,
        );
        $this->setResponseSuccess(false);
        if (in_array((int)$pedido->statusExportacao, $statusValidosProcessamento, true)) {
            try {
                $pedidoBO->processarPedido($pedido);

                $this->setResponseCode(Response::OK)
                    ->setResponseData("ok")
                    ->setResponseSuccess(true)
                    ->setHttpCode(Http::OK);
            } catch (\Exception $e) {
                $this->setResponseCode(Response::ERRO)
                    ->setResponseData(null)
                    ->setHttpCode(Http::ERRO_PRECONDICAO)
                    ->setResponseMessage($e->getMessage());
            }
        } else {
            $this->setResponseCode(Response::ERRO)
                ->setResponseData(null)
                ->setHttpCode(Http::ERRO_PRECONDICAO)
                ->setResponseMessage("StatusExportacao inválido!");
        }

        return $this->getResponse();
    }

    public function cancelarPrePedidos() {
        $this->verificaPermissao();

        $parceiroSiteBO = new ParceiroSiteBO();
        $pedidoBO = new PedidoBO();

        $parceiros = $parceiroSiteBO->buscarTodosParceiros();

        $pedidosACancelar = array();

        foreach ($parceiros as $parceiro) {
            $pedidos = $pedidoBO->buscarPedidosComErroCancelamento($parceiro);
            foreach ($pedidos as $pedido) {
                $pedidosACancelar[] = $pedido;
            }
        }

        $host = Config::get('services.tarefas.host');
        $port = Config::get('services.tarefas.port');
        $pool = new JobPoolExecutor(5, 1, $host, $port, '', '');

        /** @var \App\Model\Pedido[] $pedidosACancelar */
        foreach ($pedidosACancelar as $pedido) {
            $pool->addJob('/servicos/cancelarPrePedido/' . $pedido->dadosImportacaoId . '?token=s0ftb0x');
        }

        $pool->processAll();
        return $this->setHttpCode(Http::OK)
            ->setResponseSuccess(true)
            ->setResponseCode(Response::OK)
            ->setResponseData($pool->getAllResponses())
            ->getResponse();
    }

    public function cancelarPrePedido($dadosImportacaoId) {
        $this->verificaPermissao();

        $pedidoBO = new PedidoBO();
        try {
            $pedido = $pedidoBO->buscarPedidoPorDadosImportacaoId($dadosImportacaoId);
            if ($pedido === null) {
                throw new BusinessException('Pedido não encontrado com a ID informado', Response::ERRO_VALIDACAO);
            }
            $this->setResponseSuccess(true)
                ->setHttpCode(Http::OK)
                ->setResponseCode(Response::OK);

            $usuarioLogado = Auth::getLoggedInUser();
            $usuarioIdCancelamento = $usuarioLogado !== null ? $usuarioLogado->usuarioId : null;
            $mensagemDefault = 'Cancelado automaticamente pelo sistema';
            $mensagem = $this->request->query->get('mensagem', $mensagemDefault);

            $pedidoBO->cancelarPrePedido($pedido, $mensagem, $usuarioIdCancelamento);
        } catch (BusinessException $be) {
            $this->setResponseSuccess(false)
                ->setHttpCode(Http::ERRO_PRECONDICAO)
                ->setResponseCode($be->getCode())
                ->setResponseMessage($be->getMessage())
                ->setResponseData(null);
        }

        return $this->getResponse();
    }

    private function verificaPermissao() {
        if ($this->request->query->get('token') !== 's0ftb0x') {
            throw new BusinessException('Sem permissão', Response::PERMISSAO_NEGADA);
        }
    }
}
