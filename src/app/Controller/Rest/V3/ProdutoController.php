<?php
namespace App\Controller\Rest\V3;

use App\BO\CampanhaBO;
use App\BO\CategoriaBO;
use App\BO\ParceiroConfiguracaoBO;
use App\BO\ProdutoBO;
use App\Core\Cache;
use App\Core\Controller\RestController;
use App\Core\Logger;
use App\Exceptions\AuthorizationException;
use App\Exceptions\BusinessException;
use App\Helper\Http;
use App\Helper\PermissaoHelper;
use App\Model\Configuracao;
use App\Model\Response;
use App\Strategy\BuscarEstoque\BuscarEstoqueStrategyFactory;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class ProdutoController
 *
 * Controlador responsável por buscar produtos e serviços vinculados para a plataforma VTEX do ShopFacil
 *
 * @package app\Controller\Rest\V3
 * @author Cristiano Gomes <cmgomes.es@gmail.com>
 * @since 05 de janeiro, 2015
 */
class ProdutoController extends RestController {
    /**
     * Consulta os produtos no banco de dados
     *
     * Se houver código de produto ou códigos de produtos, chamar o método ProdutoBO::ProdutoFull()
     * caso contrário chamara ProdutoBO::getProtudo()
     *
     * @throws \InvalidArgumentException
     * @throws \App\Exceptions\ValidacaoException
     * @throws \App\Exceptions\AuthorizationException
     * @throws \Exception
     */
    public function consulta() {
        try {
            $clientIp = $this->request->getClientIp();
            $parceiroId = $this->request->query->get('parceiroId', null);
            $codigos = $this->request->query->get('produtoCodigo', null);
            $pagina = $this->request->query->get('pagina', 1);

            if (!PermissaoHelper::checaPermissao($parceiroId, $clientIp)) {
                throw new AuthorizationException('Sem permissão para a interface de consulta de produto', Response::PERMISSAO_NEGADA);
            }

            $produtoBO = new ProdutoBO();
            $dadosPaginacao = null;
            $response = array(
                'dadosPaginacao' => null,
                'produtos' => array()
            );

            if (null === $codigos) {
                $produtos = $produtoBO->buscarProdutosPorPagina($parceiroId, $pagina, $dadosPaginacao);
                $response['dadosPaginacao'] = $dadosPaginacao;
            } else {
                $produtos = $produtoBO->buscarProdutosPorCodigo($parceiroId, $codigos);
            }
            $response['produtos'] = $produtos;

            $this->setResponseSuccess(true)
                ->setResponseCode(Http::OK)
                ->setResponseData($response)
                ->setResponseCode(Response::OK);


        } catch (BusinessException $be) {
            Logger::getInstance()->exception($be, $this->request->query->all());

            $this->setResponseSuccess(false)
                ->setHttpCode(Http::ERRO_PRECONDICAO)
                ->setResponseData(array())
                ->setResponseCode($be->getCode())
                ->setResponseMessage($be->getMessage());
        }
        return $this->getResponse();

        //try {
        //    $request = $this->request;
        //    $clientIp = $request->getClientIp();
        //    $parceiroId = $request->query->get('parceiroId');
        //    $produtoCodigo = $request->query->get('produtoCodigo');
        //    $pagina = $request->query->get('pagina');
        //
        //    $permissao = PermissaoHelper::checaPermissao($parceiroId, $clientIp);
        //    if (!$permissao) {
        //        throw new AuthorizationException('Sem permissão para a interface de consulta de produto');
        //    }
        //
        //    $regras = array(
        //        'parceiroId' => array('int', 'required', 'notempty'),
        //        'produtoCodigo' => array('array')
        //    );
        //    $validador = new Validacao($regras);
        //    $validacao = $validador->executaValidacao($request->query->all());
        //
        //    if (!$validacao) {
        //        throw new ValidacaoException($validador->msg);
        //    }
        //
        //    $parceiroDAO = new ParceiroSiteDAO();
        //    $parceiroSite = $parceiroDAO->getParceiroSiteById($parceiroId);
        //
        //    if (!$parceiroSite instanceof ParceiroSite) {
        //        throw new ValidacaoException($validacao->msg);
        //    }
        //
        //    if (is_array($produtoCodigo) && count($produtoCodigo) > Produto::QTD_MAX_PRODUTO_CONSULTA) {
        //        throw new ValidacaoException(sprintf('O campo [produtoCodigo] deve conter no máximo %s elementos.', Produto::QTD_MAX_PRODUTO_CONSULTA));
        //    }
        //
        //    $siteId = $parceiroSite->siteBase;
        //    $produtoBO = new ProdutoBO();
        //    $produtos = $produtoBO->getProdutoFull($siteId, $produtoCodigo, $pagina, $parceiroSite->parceiroId);
        //
        //    if (count($produtos['produtos']) > 0) {
        //        $this->preparaRetornoRest($produtos);
        //    } else {
        //        throw new BusinessException('Nenhum produto encontrado', 17);
        //    }
        //
        //    $produtosServicos = $produtoBO->getServicos($produtos, $siteId);
        //    $response = $this->json(new Response('', 0, Utils::lcfirstRecursivo($produtosServicos), true), Http::OK);
        //} catch (\Exception $e) {
        //    $response = $this->json(new Response($e->getMessage(), $e->getCode()), Http::ERRO_PRECONDICAO);
        //}
        //
        //return $response;
    }

    /**
     * getEstoque
     * Retorna as informacoes de estoque
     *
     * @return \Symfony\Component\HttpFoundation\Response        Response do symfony
     */
    public function getEstoque() {
        //Cria a variavel de request do symfony
        $request = Request::createFromGlobals();

        $parceiroId = $request->query->get('parceiroId');
        $produtoCodigo = $request->query->get('produtoCodigo');

        if (!$parceiroId) {
            return $this->setResponseCode(4)
                ->setHttpCode(Http::ERRO_PRECONDICAO)
                ->getResponse();
        }

        try {
            if (!PermissaoHelper::checaPermissao($parceiroId, $request->getClientIp())) {
                throw new \Exception("Permissão negada.");
            }

            // Ordena o array para evitar bizu
            sort($produtoCodigo);

            // Implementação manual de cache pois está muito lento
            $cacheKey = Cache::generateKey('Produto_' . implode('-', $produtoCodigo));
            $estoques = Cache::get($cacheKey);

            if (!$estoques) {
                $produtoBO = new ProdutoBO();
                $estoques = $produtoBO->getEstoques($parceiroId, $produtoCodigo);
                // Estoque full apenas 1x por dia
                Cache::set($cacheKey, $estoques, 86400);
            }

            if (count($estoques) > 0) {
                return $this->json(new Response('', 0, $estoques), Http::OK);
            } else {
                return $this->json(new Response('', 16, $estoques), Http::ERRO_PRECONDICAO);
            }

        } catch (\Exception $e) {
            return $this->json(new Response($e->getMessage(), 2), Http::ERRO_PRECONDICAO);
        }
    }

    /**
     * Retorna informações de estoque de forma mais eficiente que a anterior, tendo o cuidado de
     * nos casos de combo verificar o estoque com base em seus produtos filhos
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \InvalidArgumentException
     */
    public function getEstoquePlus() {
        $parceiroId = $this->request->query->get('parceiroId');
        $produtoCodigo = $this->request->query->get('produtoCodigo');

        if (!$parceiroId) {
            return $this->setResponseCode(4)
                ->setHttpCode(Http::ERRO_PRECONDICAO)
                ->getResponse();
        }

        try {
            if (!PermissaoHelper::checaPermissao($parceiroId, $this->request->getClientIp())) {
                throw new BusinessException("Permissão negada.", Response::PERMISSAO_NEGADA);
            }

            $pcoConfBO = new ParceiroConfiguracaoBO();
            $buscarEstoqueStrategy = $pcoConfBO->getConfiguracoes($parceiroId, Configuracao::ESTRATEGIA_BUSCA_ESTOQUE_API, true)->valor;

            $strategy = BuscarEstoqueStrategyFactory::create($buscarEstoqueStrategy);
            $estoques = $strategy->buscarEstoque($parceiroId, $produtoCodigo);

            $this->setHttpCode(Http::ERRO_PRECONDICAO)
                ->setResponseData(array())
                ->setResponseSuccess(false)
                ->setResponseCode(16);
            if (count($estoques) > 0) {
                $this->setResponseCode(0)
                    ->setResponseSuccess(true)
                    ->setResponseData($estoques)
                    ->setHttpCode(Http::OK);
            }

            return $this->getResponse();
        } catch (\Exception $e) {
            return $this->json(new Response($e->getMessage(), 2), Http::ERRO_PRECONDICAO);
        }
    }

    public function getCampanha() {
        try {
            $request       = Request::createFromGlobals();
            $clientIp      = $request->getClientIp();
            $parceiroId    = $request->query->get('parceiroId');

            if (!PermissaoHelper::checaPermissao($parceiroId, $clientIp)) {
                throw new AuthorizationException('Sem permissão para a interface de campanha');
            }


            if (!$parceiroId) {
                return $this->setResponseCode(4)
                        ->setHttpCode(Http::ERRO_PRECONDICAO)
                        ->getResponse();
            }

            $campanhaBO = new CampanhaBO();
            $catalogos = $campanhaBO->getCampanhaByParceiro($parceiroId);

            if (count($catalogos) > 0) {
                $this->setResponseCode(0)
                    ->setResponseSuccess(true)
                    ->setResponseData($catalogos)
                    ->setHttpCode(Http::OK);
            }

            return $this->setResponseCode(0)
                    ->setHttpCode(Http::OK)
                    ->setResponseData($catalogos)
                    ->setResponseSuccess(true)
                    ->getResponse();
        } catch (\Exception $e) {
            return $this->json(new Response($e->getMessage(), 2), Http::ERRO_PRECONDICAO);
        }
    }

    /**
     * Retorna informações de estoque de acordo com o codigo retail informado
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getEstoqueRetail() {
        //Cria a variavel de request do symfony
        $request = Request::createFromGlobals();

        $codigoRetail = $request->query->get('codigoRetail');

        if (!$codigoRetail) {
            $this->setResponseCode(41)
                ->setHttpCode(Http::ERRO_PRECONDICAO);
        }

        try {
            $produtoBO = new ProdutoBO();
            $estoques = $produtoBO->getEstoqueRetail($codigoRetail);

            if (count($estoques) > 0) {
                $this->setResponseCode(0)
                    ->setResponseSuccess(true)
                    ->setResponseData($estoques)
                    ->setHttpCode(Http::OK);
            } else {
                $this->setResponseCode(41)
                    ->setHttpCode(Http::ERRO_PRECONDICAO);
            }

            return $this->getResponse();

        } catch (\Exception $e) {
            return $this->json(new Response($e->getMessage(), 2), Http::ERRO_PRECONDICAO);
        }
    }

}
