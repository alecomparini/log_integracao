<?php
namespace App\Controller\Rest\V3;

use App\BO\PedidoBO;
use App\Core\Controller\RestController;

use App\Exceptions\AuthorizationException;
use App\Exceptions\BusinessException;
use App\Exceptions\ValidacaoException;
use App\Helper\PermissaoHelper;

/**
 * Classe controladora responsável por atender as chamadas de pedido parceiro
 * @package App\Controller\Rest\V3
 */
class PedidoParceiroController extends RestController {

    /**
     * Método responsável por buscar
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     */
    public function index() {
        $parceiroId = $this->request->query->get('parceiroId');
        $clientIp = $this->request->getClientIp();
        $pedidoParceiroArray = $this->request->query->get('pedidoParceiro');

        try {
            if (!PermissaoHelper::checaPermissao($parceiroId, $clientIp)) {
                throw new AuthorizationException("Permissão negada para o ParceiroId {$parceiroId} no IP {$clientIp}", 1);
            }

            $pedidoBO = new PedidoBO();
            $pedidosParceiro = $pedidoBO->getPedidosParceiro($parceiroId, $pedidoParceiroArray);

            $resposta = array(
                'pedidosImportados' => array(),
                'pedidosFilaImportacao' => array(),
                'pedidosNaoEncontrados' => array(),
            );
            $idsImportados = array();
            foreach ($pedidosParceiro as $pedidoParceiro) {
                if (!is_null($pedidoParceiro->pedidoProducao)) {
                    $resposta['pedidosImportados'][] = $pedidoParceiro;
                } else {
                    $resposta['pedidosFilaImportacao'][] = $pedidoParceiro;
                }
                $idsImportados[] = $pedidoParceiro->pedidoParceiro;
            }

            foreach ($pedidoParceiroArray as $pedPar) {
                if (!in_array($pedPar, $idsImportados)) {
                    $resposta['pedidosNaoEncontrados'][] = $pedPar;
                }
            }


            $this->setResponseSuccess(true)
                ->setResponseCode(0)
                ->setResponseData($resposta);
        } catch (BusinessException $be) {
            $this->setResponseSuccess(false)
                ->setResponseCode($be->getCode())
                ->setResponseMessage($be->getMessage());
        } catch (ValidacaoException $ve) {
            $this->setResponseSuccess(false)
                ->setResponseCode($ve->getCode())
                ->setResponseMessage($ve->getMessage());
        } catch (\Exception $e) {
            $this->setResponseSuccess(false)
                ->setResponseCode($e->getCode())
                ->setResponseMessage($e->getMessage());
        }

        return $this->getResponse();
    }
}