<?php
namespace App\Controller\Rest\V3;

use App\BO\VendedorBO;
use App\Core\Controller\RestController;
use App\Exceptions\BusinessException;
use App\Exceptions\ValidacaoException;
use App\Facades\Auth;
use App\Helper\Http;
use App\Helper\PermissaoHelper;
use App\Model\Response;

/**
 * Class VendedorController
 * @package App\Controller\Rest\V3
 */
class VendedorController extends RestController {
    /**
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \Exception
     * @throws \InvalidArgumentException
     */
    public function index() {
        //Obter parâmetros
        $email = $this->request->query->get('email');
        $senha = $this->request->query->get('senha');

        try {
            $vendedorBO = new VendedorBO();

            //Consultar dados de vendedor por email e senha
            $vendedor = $vendedorBO->getVendedor($email, $senha);

            if (null !== $vendedor) {
                $this->setResponseData($vendedor->jsonSerialize())
                    ->setHttpCode(Http::OK)
                    ->setResponseCode(0)
                    ->setResponseMessage(null)
                    ->setResponseSuccess(true);
            } else {
                $this->setResponseCode(Response::VENDEDOR_NAO_ENCONTRADO)
                    ->setHttpCode(Http::ERRO_PRECONDICAO)
                    ->setResponseMessage(null)
                    ->setResponseSuccess(false);
            }

            //Retorna dados do vendedor
            return $this->getResponse();

        } catch (BusinessException $e) {
            //Erro ao obter dados do vendedor
            return $this->setHttpCode(Http::ERRO_PRECONDICAO)
                ->setResponseCode($e->getCode())
                ->setResponseMessage($e->getMessage())
                ->setResponseSuccess(false)
                ->getResponse();
        } catch (ValidacaoException $ve) {
            // Erro de validação
            return $this->setHttpCode(Http::ERRO_PRECONDICAO)
                ->setResponseCode($ve->getCode())
                ->setResponseMessage($ve->getMessage())
                ->setResponseSuccess(false)
                ->getResponse();
        }
    }

}