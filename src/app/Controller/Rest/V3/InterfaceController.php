<?php
namespace App\Controller\Rest\V3;

use App\BO\InterfaceConfirmacaoBO;
use App\Core\Controller\RestController;
use App\DAO\ParceiroSiteDAO;
use App\Helper\Http;
use App\Helper\PermissaoHelper;
use App\Helper\Validacao;
use App\Model\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class InterfaceController
 *
 * @package App\Controller\Rest\V3
 * @author Cristiano M Gomes <cristianogomes@softbox.com.br>
 */
class InterfaceController extends RestController {

    public function setInterfaceConfirmacao() {
        // confirmacao interfaceTipo, parceiroId, site
        $request = Request::createFromGlobals();
        $parceiroId = $request->query->get('parceiroId');
        $interfaceTipo = $request->query->get('interfaceTipo');

        if (!$parceiroId) {
            return $this->setResponseCode(4)
                ->setHttpCode(Http::ERRO_PRECONDICAO)
                ->getResponse();
        }

        try {
            if (!PermissaoHelper::checaPermissao($parceiroId, $request->getClientIp())) {
                return $this->json(new Response('', 1), Http::ERRO_PRECONDICAO);
            }
        } catch (\Exception $e) {
            return $this->json(new Response($e->getMessage(), 2), Http::ERRO_PRECONDICAO);
        }

        try {
            if (!PermissaoHelper::checaPermissaoInterface($parceiroId, $interfaceTipo)) {
                return $this->json(new Response('', 7), Http::ERRO_PRECONDICAO);
            }
        } catch (\Exception $e) {
            return $this->json(new Response($e->getMessage(), 2), Http::ERRO_PRECONDICAO);
        }

        $condicoes = array(
            'parceiroId' => array('required', 'notempty', 'int'),
            'interfaceTipo' => array('required', 'notempty', 'int'),
            'confirmacao' => array('required', 'notempty', 'int')
        );
        $validacao = new Validacao($condicoes);

        if (!$validacao->executaValidacao($request->query->all())) {
            return $this->json(new Response($validacao->msg, 2), Http::ERRO_PRECONDICAO);
        }

        try {
            $parceiroDAO = new ParceiroSiteDAO();
            $parceiroSite = $parceiroDAO->getParceiroSiteById($parceiroId);
            $siteId = $parceiroSite->siteId;

            $interfaceConfirmacaoBO = new InterfaceConfirmacaoBO();
            $result = $interfaceConfirmacaoBO->getInterfaceConfirmacao($parceiroId, $siteId, $interfaceTipo);

        } catch (\Exception $e) {
            return $this->json(new Response($e->getMessage(), 2), Http::ERRO_PRECONDICAO);
        }

        if (count($result) > 0) {
            $interfaces = array();

            //Concatena as ID's
            foreach ($result as $interface) {
                $interfaces[] = $interface->InterfaceId;
            }

            $confirmacao = $request->query->get('confirmacao');
            if ($interfaceConfirmacaoBO->updateInterfaceConfirmacao($interfaces, $confirmacao) === false) {
                return $this->json(new Response('', 8), Http::OK);
            } else {
                return $this->json(new Response('', 0, array(), true), Http::OK);
            }

        } else {
            return $this->json(new Response('', 9, array(), true), Http::OK);
        }
    }
}
