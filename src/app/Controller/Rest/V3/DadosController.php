<?php
namespace App\Controller\Rest\V3;

use App\BO\BandeiraBO;
use App\BO\InterfaceConfirmacaoBO;
use App\BO\InterfacesBO;
use App\BO\ParceiroSiteBO;
use App\BO\ProdutoBO;
use App\Core\Controller\RestController;
use App\DAO\ParceiroSiteDAO;
use App\Exceptions\BusinessException;
use App\Exceptions\ValidacaoException;
use App\Facades\Auth;
use App\Helper\Http;
use App\Helper\PermissaoHelper;
use App\Helper\Utils;
use App\Helper\Validacao;
use App\Model\Response;

/**
 * Class DadosController
 *
 * @package App\Controller\Rest\V3
 * @author Cristiano Gomes <cristianogomes@softbox.com.br>
 */
class DadosController extends RestController {
    /**
     * Retorna os dados das interfaces para o cliente
     *
     * @throws \InvalidArgumentException
     */
    public function getDados() {
        $parceiroId = Auth::getLoggedInUser()->parceiroId;
        $interface = $this->request->query->get('interface', null);

        if (!$parceiroId) {
            return $this->setResponseCode(4)
                ->setHttpCode(Http::ERRO_PRECONDICAO)
                ->getResponse();
        }

        try {
            $regras = array(
                "parceiroId" => array("required", "notempty", "int"),
                "interface" => array("required", "notempty", "int")
            );

            $validacao = new Validacao($regras);
            if (!$validacao->executaValidacao($this->request->query->all())) {
                throw new ValidacaoException($validacao->msg, Response::ERRO_VALIDACAO);
            }

            $parceiroDAO = new ParceiroSiteDAO();
            $parceiroSite = $parceiroDAO->getParceiroSiteById($parceiroId);
            $siteId = $parceiroSite->siteId;
            $interfacesBO = new InterfacesBO();
            $estoque = $interfacesBO->getDados(
                $parceiroId,
                $siteId,
                $interface
            );

            if (count($estoque) === 0) {
                return $this->setResponseCode(10)
                    ->setHttpCode(Http::OK)
                    ->getResponse();
            } else {
                $output = array();

                foreach ($estoque as $item) {
                    $output[] = json_decode($item->dados);
                }

                $output = Utils::lcfirstRecursivo($output);

                return $this->setResponseCode(Response::OK)
                    ->setHttpCode(Http::OK)
                    ->setResponseData($output)
                    ->setResponseSuccess(true)
                    ->getResponse();
            }
        } catch (\Exception $e) {
            return $this->setResponseCode($e->getCode())
                ->setHttpCode(Http::ERRO_PRECONDICAO)
                ->setResponseMessage($e->getMessage())
                ->getResponse();
        }
    }

    /**
     * Confirma os dados retornados pelo cliente
     *
     * @return mixed
     * @throws \Exception
     */
    public function confirmar() {
        // confirmacao interfaceTipo, parceiroId, site
        $parceiroId = Auth::getLoggedInUser()->parceiroId;
        $interfaceTipo = $this->getParam('interfaceTipo');
        $confirmacao = $this->getParam('confirmacao');

        try {
            $condicoes = array(
                'parceiroId' => array('required', 'notempty', 'int'),
                'interfaceTipo' => array('required', 'notempty', 'int'),
                'confirmacao' => array('required', 'notempty', 'int')
            );
            $validacao = new Validacao($condicoes);

            if (!$validacao->executaValidacao($this->body)) {
                throw new ValidacaoException($validacao->msg, Response::ERRO_VALIDACAO);
            }

            $parceiroDAO = new ParceiroSiteDAO();
            $parceiroSite = $parceiroDAO->getParceiroSiteById($parceiroId);
            $siteId = $parceiroSite->siteId;

            $interfaceConfirmacaoBO = new InterfaceConfirmacaoBO();
            $result = $interfaceConfirmacaoBO->getInterfaceConfirmacao($parceiroId, $siteId, $interfaceTipo);

        } catch (\Exception $e) {
            return $this->json(new Response($e->getMessage(), 2), Http::ERRO_PRECONDICAO);
        }

        if (count($result) > 0) {
            $interfaces = array();

            //Concatena as ID's
            foreach ($result as $interface) {
                $interfaces[] = $interface->InterfaceId;
            }

            if ($interfaceConfirmacaoBO->updateInterfaceConfirmacao($interfaces, $confirmacao) === false) {
                return $this->json(new Response('', 8), Http::ERRO_PRECONDICAO);
            } else {
                return $this->json(new Response('', 0, array(), true), Http::OK);
            }

        } else {
            return $this->json(new Response('', 9, array(), true), Http::OK);
        }
    }

    /**
     * Método responsável por exibir os parcelamentos de uma certa bandeira
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function parcelamento() {
        try {
            $parceiroId = Auth::getLoggedInUser()->parceiroId;
            $codigo = $this->request->query->get('codigo', null);
            $valor = $this->request->query->get('valor', null);
            $bandeiraId = $this->request->query->get('bandeiraId', null);

            if (!$codigo) {
                $bandeiraBO = new BandeiraBO();

                if ($bandeiraId) {
                    $bandeiras = $bandeiraBO->getBandeiras($bandeiraId);
                    $existe = false;

                    foreach ($bandeiras as $bandeira) {
                        if ($bandeiraId === $bandeira->bandeiraId) {
                            $existe = true;
                        }
                    }

                    if (!$existe) {
                        return $this->setResponseSuccess(false)
                            ->setHttpCode(Http::ERRO_PRECONDICAO)
                            ->setResponseCode(18)
                            ->getResponse();
                    }
                } else {
                    $bandeiraId = null;
                }

                $parcelamento = $bandeiraBO->getParcelamentos($valor, $bandeiraId);
                $this->setResponseData($parcelamento);
            } else {
                $produtoBO = new ProdutoBO();
                $parcelamentos = $produtoBO->buscarParcelamentoProduto($parceiroId, $codigo, $valor);
                $this->setResponseData(array(
                    array(
                        'bandeiraId' => null,
                        'nome' => 'Parcelamento Produto',
                        'parcelamentos' => $parcelamentos
                    )
                ));
            }

            $this->setResponseSuccess(true)
                ->setHttpCode(Http::OK)
                ->setResponseCode(0);

        } catch (\Exception $e) {
            $this->setResponseSuccess(false)
                ->setResponseData(null)
                ->setHttpCode(Http::ERRO_PRECONDICAO)
                ->setResponseMessage($e->getMessage())
                ->setResponseCode($e->getCode());
        }


        return $this->getResponse();
    }

    /**
     * @param null $bandeiraId
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws ValidacaoException
     */
    public function bandeira($bandeiraId = null) {
        try {
            $bandeiraBO = new BandeiraBO();
            $bandeiras = $bandeiraBO->getBandeiras($bandeiraId);

            if (count($bandeiras) === 0) {
                throw new BusinessException('Nenhuma Bandeira encontrada com o ID fornecido', 18);
            }

            $bandeiras = array_map('\App\Helper\Utils::aplicaSerializacao', $bandeiras);

            $this->setResponseSuccess(true)
                ->setHttpCode(Http::OK)
                ->setResponseMessage(null)
                ->setResponseCode(0)
                ->setResponseData(null === $bandeiraId ? $bandeiras : $bandeiras[0]);
        } catch (\Exception $e) {
            $this->setResponseSuccess(false)
                ->setHttpCode(Http::ERRO_PRECONDICAO)
                ->setResponseMessage($e->getMessage())
                ->setResponseCode($e->getCode())
                ->setResponseData(null);
        }

        return $this->getResponse();
    }
}
