<?php

namespace App\Controller\Rest\V3;

use App\BO\ProdutoSiteBO;
use App\Core\Controller\RestController;
use App\Exceptions\AuthorizationException;
use App\Exceptions\BusinessException;
use App\Exceptions\ValidacaoException;
use App\Facades\Auth;
use App\Helper\Http;
use App\Helper\PermissaoHelper;
use App\Model\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class FreteController
 *
 * Classe responsável por fornecer o serviço REST para calculo de frete
 *
 * @package App\Controller\Rest\V3
 * @author William Johnson dos Santos Okano <williamokano@gmail.com>
 */
class FreteController extends RestController {

    public function consulta() {
        $request = Request::createFromGlobals();

        $parceiroId = Auth::getLoggedInUser()->parceiroId;
        $produtos = $request->query->get('produtos');
        $quantidades = $request->query->get('quantidades');
        $cep = $request->query->get('cep');

        try {

            //verificar e fundir os arrays de produtos e quantidades em um só
            if (!is_array($produtos) || count($produtos) < 1) {
                throw new ValidacaoException('Campo produtos não informado ou vazio.', Response::ERRO_VALIDACAO);
            } else {
                if (!is_array($quantidades) || count($quantidades) !== count($produtos)) {
                    throw new BusinessException('', Response::QUANTIDADE_DIVERGENTE);
                } else {
                    $tmpProdutos = array();

                    foreach ($produtos as $i => $codigo) {
                        if (!isset($quantidades[$i])) {
                            throw new BusinessException('', Response::QUANTIDADE_INCOMPLETA);
                        }
                        $tmp = new \stdClass();
                        $tmp->Codigo = $codigo;
                        $tmp->Quantidade = $quantidades[$i];
                        $tmpProdutos[] = $tmp;
                    }
                    $produtos = $tmpProdutos;
                }
            }

            $produtoSiteBO = new ProdutoSiteBO();
            $frete = $produtoSiteBO->getFrete($parceiroId, $cep, $produtos);

            return $this->json(new Response('', Response::OK, $frete, true), Http::OK);
        } catch (\Exception $e) {
            return $this->json(new Response($e->getMessage(), $e->getCode(), array(), false), Http::ERRO_PRECONDICAO);
        }
    }
}
