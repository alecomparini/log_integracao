<?php
namespace App\Controller\Rest\V3;

use App\BO\PedidoBO;
use App\Core\Controller\RestController;
use App\Core\Logger;
use App\Exceptions\ValidacaoAttachmentsException;
use App\Exceptions\AuthorizationException;
use App\Exceptions\ValidacaoException;
use App\Helper\Http;
use App\Helper\PermissaoHelper;
use App\Model\Pedido;
use App\Model\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Core\Config;

/**
 * Class PedidoController
 * @package App\Controller\Rest\V3
 */
class PedidoController extends RestController {

    public function criar() {
        $request = $this->request;
        $clientIp = $request->getClientIp();
        $parceiroId = $this->getParam('parceiroId');

        if (!$parceiroId) {
            return $this->setResponseCode(4)
                ->setHttpCode(Http::ERRO_PRECONDICAO)
                ->getResponse();
        }

        try {
            if (!$this->body) {
                throw new ValidacaoException("Parametros inválidos.");
            }
            $pedido = Pedido::createPedido($this->body);
            $permissoes = array();

            if (!PermissaoHelper::checaPermissao($parceiroId, $clientIp, $permissoes)) {
                throw new AuthorizationException("Permissão negada para o ParceiroId {$parceiroId} no IP {$request->getClientIp()}");
            }

            if (!PermissaoHelper::checaPermissaoInterface($parceiroId, 2)) {
                throw new AuthorizationException("Sem permissão na interface de pedidos");
            }

            $pedido->parceiroId = $parceiroId;
            $pedido->parceiroIp = $clientIp;
            $pedido->parceiroSiteId = $permissoes[0]->parceiroSiteId;
            $pedido->siteBase = $permissoes[0]->siteBase;
            $pedido->siteId = $permissoes[0]->siteId;

            $pedidoBO = new PedidoBO();
            $pedidoBO->setPedido($pedido);
            foreach ($pedido->pagamento as $pagamento) {
                if ($pagamento->meioPagamentoId == 2) {
                    $urlGateway = Config::get('gateway.url');
                    $url = "{$urlGateway}/Integracao/ImportarIntegracaoParceiro/" . $pedido->parceiroPedidoId . "/" . $pedido->siteId;
                    $jsonBoleto = file_get_contents($url);
                    $dadosBoleto = json_decode($jsonBoleto);

					if (is_object($dadosBoleto)) {
						$mensagem = "Pedido importado com sucesso";
						$response = $this->json(new Response($mensagem, 0, array('url_boleto' => $dadosBoleto->Url), true), Http::OK);
					} else {
						return $this->json(new Response('', 5), Http::ERRO_PRECONDICAO);
					}
            } elseif (!isset($response)) {
                $response = $this->json(new Response('', 0, array(), true), Http::OK);
			} 
				
			$erro = false;
			} 
		} catch (\Exception $e) {
            $response = $this->json(new Response($e->getMessage(), $e->getCode()), Http::ERRO_PRECONDICAO);
            $erro = true;
        }

		$call = json_encode(array(
            'input' => str_replace(array("\t", "\n", "\n"), ' ', $this->getRawBody()),
            'output' => $response->getContent(),
            'output_code' => $response->getStatusCode(),
        ));

        //Remove os dados de cartão para padrão PCI
        $call = preg_replace('/(\\\\"cartaoCodSeguranca\\\\"\\s*:\\s*\\\\")(.*?)(\\\\")/mi', '$1***$3', $call);
        $call = preg_replace('/(?P<P1>\\\\"cartaoNumero\\\\"\\s*:\\s*\\\\")(?P<P2>.*?)(\\\\")/mi', '$1****************$3', $call);
        $call = preg_replace('/(?P<P1>\\\\"cartaoValidade\\\\": \\\\")(?P<P2>.*?)(?P<P3>\\\\")/mi', '$1*******$3', $call);

        if (!$erro) {
            Logger::getInstance()->log($call);
        } else {
            Logger::getInstance()->error($call);
        }

        return $response;
    }

}
