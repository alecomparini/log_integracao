<?php
namespace App\Controller\Rest\V2;

use App\BO\TarefaBO;
use App\Core\Controller\RestController;
use Cron\CronExpression;
use Symfony\Component\HttpFoundation\Request;

class TarefasController extends RestController {
    private $tarefasBO;

    public function __construct(Request $request) {
        parent::__construct($request);

        $this->tarefasBO = new TarefaBO();
    }

    public function index() {
        $tarefas = $this->tarefasBO->buscarTarefasAtivas();
        foreach ($tarefas as $tarefa) {
            $cron = CronExpression::factory($tarefa->configuracaoTempo);
            $tarefa->dataProximaExecucao = $cron->getNextRunDate('now')->format('Y-m-d H:i:s');
        }
        return $this
            ->setResponseCode(0)
            ->setResponseSuccess(true)
            ->setResponseData($tarefas)
            ->getResponse();
    }
}