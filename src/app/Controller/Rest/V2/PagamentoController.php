<?php
namespace App\Controller\Rest\V2;

use App\BO\PedidoBO;
use App\Core\Logger;
use App\Core\Controller\RestController;
use App\Exceptions\ValidacaoException;
use App\Helper\Http;
use App\Helper\PermissaoHelper;
use App\Model\Response;
use Symfony\Component\HttpFoundation\Request;
use App\Core\Config;
use App\Helper\Validacao;

/**
 * Class PagamentoController
 * @package App\Controller\Rest\V2
 */
class PagamentoController extends RestController {

    public function setStatusPagamento() {

        $parceiroId = $this->request->query->get('parceiroId');
        $pedidoParceiroId = $this->request->query->get('pedidoParceiroId');
        $statusPagamento = $this->request->query->get('statusPagamento');

        $parametrosEnviados = $this->request->query->all();
        $parametrosEnviados['IP'] = $this->request->getClientIp();

        if (!$parceiroId) {
            Logger::getInstance("api_log")->log("[API:Enviar Status Pagamento] [Parceiro:{$parceiroId}] [Pedido:{$pedidoParceiroId}] [Mensagem_tojs:" . json_encode(Logger::getContextData(__METHOD__,$parametrosEnviados , 'Campo [parceiroId] Obrigatório')) ."]");
            return $this->setResponseCode(4)
                ->setHttpCode(Http::ERRO_PRECONDICAO)
                ->getResponse();
        }

        try {

            if (!PermissaoHelper::checaPermissao($parceiroId, $this->request->getClientIp())) {
                throw new \Exception("Permissão negada para o IP {$this->request->getClientIp()}", 1);
            }

            $obj = (object)array(
                'pedidoParceiroId' => $pedidoParceiroId,
                'statusPagamento' => $statusPagamento
            );

            $regras = array(
                'pedidoParceiroId' => array('notempty', 'required', 'str'),
                'statusPagamento' => array('notempty', 'required', 'str'),
            );

            $validacao = new Validacao($regras);
            
            if (!$validacao->executaValidacao($obj)) {
                throw new ValidacaoException($validacao->msg, 2);
            }

            if ('AP' === $statusPagamento) {
                $statusPagamentoSigla = 'AP';
            } else {
                $statusPagamentoSigla = 'RP';
            }

            $pedidoBO = new PedidoBO();
            $pedidosProducao = $pedidoBO->getPedidosParceiro($parceiroId, $pedidoParceiroId);

            $producaoPedidoId = null;

            if (count($pedidosProducao) > 0) {
                $producaoPedidoId = $pedidosProducao[0]->pedidoProducao;
            }

            if (!is_null($producaoPedidoId)) {
                
                $urlGateway = Config::get('gateway.url');
                $url = "{$urlGateway}/AnalisePagamento/NotificarPagamento/?NumPedido=" . $producaoPedidoId . "&CodPagamento=10&Status=" . $statusPagamentoSigla;
                $json = file_get_contents($url);

                $retornoGateway = json_decode($json);

                if (is_object($retornoGateway)) {
                    if ($retornoGateway->success) {
                        $response = 'Pedido Confirmado com Sucesso - ' . $json;
                        $return = $this->json(new Response('', 0, array(), true), Http::OK);
                    } else {
                        $response = '1002 - Erro ao informar pagamento do pedido - ' . $json;
                        $return = $this->json(new Response('', 1002, array(), false), Http::ERRO_PRECONDICAO);
                    }
                } else {
                    $response = 'O update de confirmacao da interface falhou. Retorno do Gateway - ' . $json;
                    $return = $this->json(new Response('', Response::INTERFACE_UPDATE_ERROR), Http::ERRO_PRECONDICAO);
                }
            } else {
                $response = 'Pedido não consta em nossa base de Integrações';
                $return = $this->json(new Response('' , Response::INTERFACE_UPDATE_ERROR), Http::ERRO_PRECONDICAO);
            }

            //Logger::getInstance("api_log")->log('',Logger::getContextData(__METHOD__,$this->request->query->all(),$response));
            Logger::getInstance("api_log")->log("[API:Enviar Status Pagamento] [Parceiro:{$parceiroId}] [Pedido:{$pedidoParceiroId}] [Mensagem_tojs:" . json_encode(Logger::getContextData(__METHOD__,$parametrosEnviados , $response)) ."]");
            return $return;

        } catch (\Exception $e) {
            $response = array('Exception' => array('Código' => $e->getCode(), 'Mensagem' => $e->getMessage()));
            Logger::getInstance("api_log")->log("[API:Enviar Status Pagamento] [Parceiro:{$parceiroId}] [Pedido:{$pedidoParceiroId}] [Mensagem_tojs:" . json_encode(Logger::getContextData(__METHOD__,$parametrosEnviados , $response)) ."]");
            //Logger::getInstance("api_log")->error('',Logger::getContextData(__METHOD__,$this->request->query->all(),$response));
            return $this->setHttpCode(Http::ERRO_PRECONDICAO)
                ->setResponseCode($e->getCode())
                ->setResponseMessage($e->getMessage())
                ->setResponseSuccess(false)
                ->getResponse();
        }
    }
}