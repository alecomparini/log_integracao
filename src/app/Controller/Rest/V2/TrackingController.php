<?php

namespace App\Controller\Rest\V2;

use App\BO\TrackingBO;
use App\Core\Controller\RestController;
use App\DAO\ParceiroSiteDAO;
use App\Exceptions\BusinessException;
use App\Exceptions\ValidacaoException;
use App\Helper\Http;
use App\Model\ParceiroSite;
use App\Model\Response;

/**
 * Class TrackingController
 * @package App\Controller\Rest\V2
 * @author Cristiano Gomes <cristianogomes@softbox.com.br>
 */
class TrackingController extends RestController {
    /**
     * Retorna uma consulta de tracking
     *
     * @return \App\Model\TrackingOut retorno do webservice
     * @throws \InvalidArgumentException
     */
    public function getTracking() {
        //Obtém os parâmetros
        $parceiroId = $this->request->query->get('parceiroId');
        $pedidoId = $this->request->query->get('pedidoId');

        $trackingBO = new TrackingBO();
        $parceiroDAO = new ParceiroSiteDAO();

        try {
            $parceiroSite = $parceiroDAO->getParceiroSiteById($parceiroId);

            if (!$parceiroSite instanceof ParceiroSite) {
                throw new BusinessException('codigo de parceiros inválido', Response::CODIGO_PARCEIRO_INVALIDO);
            }

            $siteId = $parceiroSite->siteId;

            //Consulta dados de tracking
            $trackings = $trackingBO->consulta($pedidoId, $parceiroId, $siteId);
            $output = array();

            //Prepara dados de retorno
            if (count($trackings) > 0) {
                foreach ($trackings as &$tracking) {
                    $decoded = json_decode($tracking->json);

                    // HACK FIX TRACKING SEM QTD (REMOVER DEPOIS DE 2017-01-01)
                    $trk = $decoded[0];
                    foreach ($trk->entregas as &$entrega) {
                        if (isset($entrega->itens)) {
                            foreach ($entrega->itens as &$item) {
                                if (is_numeric($item)) {
                                    $oldValue = (int)$item;
                                    $item = new \stdClass();
                                    $item->qtd = $oldValue;
                                }
                            }
                        }
                    }

                    $output = array_merge($output, $decoded);
                }
            }

            return $this->json(new Response('', 0, $output, true), Http::OK);
        } catch (ValidacaoException $e) {
            return $this->json(new Response($e->getMessage(), 2, false), Http::ERRO_PRECONDICAO);
        } catch (BusinessException $be) {
            return $this->json(new Response($be->getMessage(), $be->getCode(), false), Http::ERRO_PRECONDICAO);
        } catch (\Exception $e) {
            //Erro ao obter dados de tracking
            return $this->json(new Response('', 11, false), Http::ERRO_PRECONDICAO);
        }
    }
}
