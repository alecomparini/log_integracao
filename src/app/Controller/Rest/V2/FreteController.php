<?php

namespace App\Controller\Rest\V2;

use App\BO\ProdutoSiteBO;
use App\Core\Controller\RestController;
use App\Exceptions\AuthorizationException;
use App\Exceptions\BusinessException;
use App\Helper\Http;
use App\Helper\PermissaoHelper;
use App\Model\Response;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class FreteController
 *
 * Classe responsável por fornecer o serviço REST para calculo de frete
 *
 * @package App\Controller\Rest\V2
 * @author Cristiano Gomes <cristianogomes@sotftbox.com.br>
 */
class FreteController extends RestController {

    public function consulta() {
        $request = Request::createFromGlobals();

        $parceiroId = $request->query->get('parceiroId');
        $produtos = $request->query->get('produtos');
        $quantidades = $request->query->get('quantidades');
        $cep = $request->query->get('cep');
        
        /*Informações sobre o agendamento*/
        $dataAgendamento  = $request->query->get('dataAgendamento');
        $turnoAgendamento = $request->query->get('turno');

        try {

            //verificar e fundir os arrays de produtos e quantidades em um só
            if (!is_array($produtos) || count($produtos) < 1) {
                throw new BusinessException('', 21);
            } else {
                if (!is_array($quantidades) || count($quantidades) != count($produtos)) {
                    throw new BusinessException('Quantidade de itens divergente', Response::QUANTIDADE_DIVERGENTE);
                } else {
                    $tmpProdutos = array();

                    foreach ($produtos as $i => $codigo) {
                        if (!isset($quantidades[$i])) {
                            throw new BusinessException('', 14);
                        }
                        $tmp = new \stdClass();
                        $tmp->Codigo = $codigo;
                        $tmp->Quantidade = $quantidades[$i];
                        $tmpProdutos[] = $tmp;
                    }
                    $produtos = $tmpProdutos;
                }
            }

            if (!PermissaoHelper::checaPermissao($parceiroId, $request->getClientIp())) {
                throw new AuthorizationException("Permissão negada.", Response::PERMISSAO_NEGADA);
            }

            $produtoSiteBO = new ProdutoSiteBO();
            $frete = $produtoSiteBO->getFrete($parceiroId, $cep, $produtos, $dataAgendamento, $turnoAgendamento);

            return $this->json(new Response('', Response::OK, $frete, true), Http::OK);
        } catch (\Exception $e) {
            return $this->json(new Response($e->getMessage(), $e->getCode(), array(), false), Http::ERRO_PRECONDICAO);
        }
    }
}
