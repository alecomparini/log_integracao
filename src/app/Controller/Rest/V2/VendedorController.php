<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 21-01-2016
 * Time: 10:43
 */

namespace App\Controller\Rest\V2;


use App\BO\VendedorBO;
use App\Core\Controller\RestController;
use App\Helper\Http;
use App\Helper\PermissaoHelper;


/**
 * Class VendedorController
 * @package App\Controller\Rest\V2
 */
class VendedorController extends RestController {

    /**
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function index() {
        //Obter parâmetros
        $parceiroId = $this->request->query->get('parceiroId');
        $email = $this->request->query->get('email');
        $senha = $this->request->query->get('senha');

        if (!$parceiroId) {
            return $this->setResponseCode(4)
                ->setHttpCode(Http::ERRO_PRECONDICAO)
                ->getResponse();
        }

        try {
            if (!PermissaoHelper::checaPermissao($parceiroId, $this->request->getClientIp())) {
                throw new \Exception("Permissão negada para o IP {$this->request->getClientIp()}", 1);
            }
            $vendedorBO = new VendedorBO();

            //Consultar dados de vendedor por email e senha
            $vendedor = $vendedorBO->getVendedor($email, $senha);

            if (!is_null($vendedor)) {
                $this->setResponseData($vendedor->jsonSerialize())
                    ->setHttpCode(Http::OK)
                    ->setResponseCode(0)
                    ->setResponseMessage(null)
                    ->setResponseSuccess(true);
            } else {
                $this->setResponseCode(15)
                    ->setHttpCode(Http::ERRO_PRECONDICAO)
                    ->setResponseMessage(null)
                    ->setResponseSuccess(false);
            }

            //Retorna dados do vendedor
            return $this->getResponse();

        } catch (\Exception $e) {
            //Erro ao obter dados do vendedor
            return $this->setHttpCode(Http::ERRO_PRECONDICAO)
                ->setResponseCode($e->getCode())
                ->setResponseMessage($e->getMessage())
                ->setResponseSuccess(false)
                ->getResponse();
        }
    }

}