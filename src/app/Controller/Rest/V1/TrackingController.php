<?php

namespace App\Controller\Rest\V1;

use App\BO\TrackingBO;
use App\Core\Controller\RestController;
use App\DAO\ParceiroSiteDAO;
use App\Helper\PermissaoHelper;


/**
 * Class TrackingController
 * @package App\Controller\Rest\V1
 */
class TrackingController extends RestController {

    /**
     * Consulta dados de tracking via REST
     * Recebe como parâmetros:
     * pedido_id
     * parceiro_id
     * site_id
     * @return null|\Symfony\Component\HttpFoundation\Response
     */
    public function consulta() {
        //Obtém os parâmetros
        $pedidoId = $this->getParam("pedido_id");
        $parceiroId = $this->getParam("parceiro_id");
        $siteId = $this->getParam("site_id");
        try {
            if (!PermissaoHelper::checaPermissao($parceiroId, $this->request->getClientIp())) {
                throw new \Exception("Permissão negada para o parceiro {$parceiroId} e o IP: {$this->request->getClientIp()}.");
            }

            $parceiroSiteDAO = new ParceiroSiteDAO();
            $parceiroInfo = $parceiroSiteDAO->getParceiroSiteById($parceiroId);

            if ($siteId == '') {
                throw new \Exception("Campo [site_id] e obrigatorio.", 5);
            }

            if ($siteId != $parceiroInfo->siteId) {
                throw new \Exception("Erro: parceiro_id informado não corresponde ao site_id informado", 1);
            }

            $trackingBO = new TrackingBO();

            $response = null;

            //Realiza a consulta do tracking
            $trackings = $trackingBO->consulta($pedidoId, $parceiroId, $siteId);

            //Prepara os dados de retorno
            $dados = array();
            //Prepara dados de retorno
            if (count($trackings) > 0) {
                foreach ($trackings as &$tracking) {
                    $decoded = json_decode($tracking->json);

                    // HACK FIX TRACKING SEM QTD (REMOVER DEPOIS DE 2017-01-01)
                    $trk = $decoded[0];
                    foreach ($trk->entregas as &$entrega) {
                        if (isset($entrega->itens)) {
                            foreach ($entrega->itens as &$item) {
                                if (is_numeric($item)) {
                                    $oldValue = (int)$item;
                                    $item = new \stdClass();
                                    $item->qtd = $oldValue;
                                }
                            }
                        }
                    }

                    $dados = array_merge($dados, $decoded);
                }
            }

            $response = $this->json(array(
                'status' => 200,
                'msg' => 'Ok',
                'dados' => $dados
            ), 200);

        } catch (\Exception $e) {
            //Erro ao consultar dados de tracking
            switch ($e->getCode()) {
                case 3:
                case 1:
                    $status = 1;
                    break;
                case 500:
                    $status = 500;
                    break;
                default:
                    $status = 5;
            }

            $response = $this->json(array(
                'status' => $status,
                'msg' => $e->getMessage(),
                'dados' => array()
            ), 400);
        }
        return $response;
    }
}
