<?php
namespace App\Controller\Admin;

use App\Core\Controller\RestController;

class DashboardController extends RestController {
    public function index() {
        return $this->view('dashboard.index');
    }

    public function tarefas() {
        return $this->view('dashboard.tarefas');
    }

    public function pedidosErro() {
        return $this->view('dashboard.pedidos.erro');
    }
}