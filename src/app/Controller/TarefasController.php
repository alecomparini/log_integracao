<?php
namespace App\Controller;

use App\BO\TarefaBO;
use App\Core\Config;
use App\Core\Controller\RestController;
use App\Core\Database;
use App\Core\Logger;
use App\Exceptions\AuthorizationException;
use App\Exceptions\BusinessException;
use App\Exceptions\DatabaseException;
use App\Exceptions\FrameworkException;
use App\Helper\Http;
use App\Helper\Utils;
use App\Model\Response;
use App\Service\JobPoolExecutor;
use Cron\CronExpression;
use App\Service\FileLockController;

class TarefasController extends RestController {

    CONST TOKEN = 's0ftb0x';

    /**
     * Só pra ter uma tela pra rota padrão
     *
     * @return \App\Core\View
     */
    public function index() {
        return $this->view('permissiondenied');
    }

    /**
     * Executa as tarefas no horário
     * Este método deve ser chamado de minuto em minuto pois
     * ele é autogerido, ou seja, ele faz o controle se deve
     * chamar a uma tarefa o não.
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \App\Exceptions\DatabaseException
     * @throws \InvalidArgumentException
     */
    public function iniciar() {
        try {
            $this->verificaToken();
            $tarefaBO = new TarefaBO();
            $agendamentos = $tarefaBO->buscarTarefasExecutar();
            
            $waitForResponses = $this->request->query->get('wait', false);
            $poolSize = Config::get('services.tarefas.poolsize');
            $host = Config::get('services.tarefas.host');
            $port = Config::get('services.tarefas.port');
            $timeout = Config::get('services.tarefas.timeout');
            $username = Config::get('services.tarefas.username');
            $password = Config::get('services.tarefas.password');

            $pool = new JobPoolExecutor($poolSize, $timeout, $host, $port, $username, $password);
            if (is_array($agendamentos)) {
                foreach ($agendamentos as $agendamento) {
                    $pool->addJob('/servicos/tarefas/executar/' . $agendamento->agendamentoTarefaId, array('token' => static::TOKEN), Utils::limpaString(sprintf('%s [%d]', $agendamento->nome, $agendamento->agendamentoTarefaId)));
                }
            }
            $this->setResponseSuccess(true)->setHttpCode(Http::OK)->setResponseCode(Response::OK)->setResponseData(null);
            if ($waitForResponses) {
                $pool->processAll();
                $respostas = $pool->getAllResponses();
                $this->setResponseData($respostas);
            } else {
                $this->setResponseData(null);
            }

        } catch (BusinessException $be) {
            $this->setResponseSuccess(false)
                ->setResponseCode($be->getCode())
                ->setResponseMessage($be->getMessage())
                ->setResponseData(null);
            Logger::getInstance()->exception($be);
        } catch (DatabaseException $dbe) {
            Database::getInstance()->rollback();

            $this->setResponseSuccess(false)
                ->setResponseCode($dbe->getCode())
                ->setResponseMessage($dbe->getMessage())
                ->setResponseData(null);
            Logger::getInstance()->exception($dbe);
        }

        return $this->getResponse();
    }

    /**
     * Executa uma tarefa de acordo com o ID do agendamento
     *
     * @param int  $id ID da tarefa a ser executada
     * @param bool $forcado
     *
     * @return \Symfony\Component\HttpFoundation\Response
     * @throws \InvalidArgumentException
     * @throws \App\Exceptions\AuthorizationException
     * @throws \RuntimeException
     * @throws \App\Exceptions\ValidacaoException
     */
    public function executar($id, $forcado = false) {
    	
    	$lockName = "executar-".$id;
    	$lock = new FileLockController(true, false);
    	
    	if ($lock->lock($lockName)) {
          try {
            $this->verificaToken();
            $dataInicioExecucao = date('Y-m-d H:i:s');
            $tarefaBO = new TarefaBO();
            $agendamento = $tarefaBO->buscarTarefaPorAgendamentoId(filter_var($id, FILTER_VALIDATE_INT));

            if (null === $agendamento) {
            	$lock->release($lockName);
                throw new BusinessException('Agendamento não encontrado', Response::AGENDAMENTO_NAO_ENCONTRADO);
            }

            // Verifica se vai ser executado forçado ou não
            if (!$forcado) {
                // Verifica se o agendamento pode ser executado
                $cron = CronExpression::factory($agendamento->configuracaoTempo);
                if (!$cron->isDue()) {
                	$lock->release($lockName);
                    throw new BusinessException('Fora do período agendado. Próxima execução em ' . $cron->getNextRunDate()->format('d/m/Y H:i:s'), Response::ERRO_AGENDAMENTO);
                }
            }

            if (!empty($agendamento->parametros)) {
                if ($agendamento->indJson) {
                    $parametros = json_decode($agendamento->parametros, true);
                } else {
                    parse_str($agendamento->parametros, $parametros);
                }
            }

            switch ($agendamento->metodoHttp) {
                case 'GET':
                    $resposta = Http::get($agendamento->url);
                    break;
                case 'POST':
                    $resposta = Http::post($agendamento->url, $parametros);
                    break;
                default:
                    throw new BusinessException('Método HTTP ainda não suportado');
            }

            // Salva o log de execução mas não deixa enviar exception de banco de dados, pq neste ponto é irrelevante e
            // não deve interromper de forma alguma a execução da resposta!!!! Filling sausage!!!!!
            try {
                $dataTerminoExecucao = date('Y-m-d H:i:s');
                $tarefaBO->salvarLogExecucao($agendamento->agendamentoTarefaId, $resposta->responseCode, $dataInicioExecucao, $dataTerminoExecucao, $resposta->body, '');
                $tarefaBO->atualizarDataUltimaExecucao($agendamento);
            } catch (DatabaseException $dbe) {
                Logger::getInstance()->error('Erro ao salvar log de tarefa');
                Logger::getInstance()->exception($dbe);
            }

            $this->setResponseSuccess(true)
                ->setResponseData($resposta);
            
            $lock->release($lockName);
          } catch (BusinessException $be) {
          	
          	$lock->release($lockName);
          	
            Logger::getInstance()->exception($be, array('agendamentoTarefaId' => $id));
            $this->setResponseSuccess(false)
                ->setResponseData($be->getMessage())
                ->setResponseMessage($be->getMessage())
                ->setResponseCode($be->getCode())
                ->setHttpCode(Http::ERRO_PRECONDICAO);
          }

          return $this->getResponse();
          
    	} else {
            throw new BusinessException('Já existe um serviço de "' . $lockName . '" em curso.');
        }
    }

    /**
     * Verifica se pode executar a URL
     *
     * @return bool
     * @throws \App\Exceptions\AuthorizationException
     * @throws \InvalidArgumentException
     */
    private function verificaToken() {
        if ($this->request->query->get('token') !== static::TOKEN) {
            throw new AuthorizationException('Token inválido', Response::ERRO);
        }
    }
}