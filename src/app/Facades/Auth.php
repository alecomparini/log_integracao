<?php
namespace App\Facades;

use App\BO\TokenBO;
use App\BO\UsuarioBO;
use App\Core\Config;
use App\Core\Logger;
use Firebase\JWT\JWT;
use Symfony\Component\HttpFoundation\Request;

abstract class Auth {
    /**
     * Recupera o access_token seja da URL ou então dos headers
     *
     * @return string
     * @throws \InvalidArgumentException
     */
    public static function getAccessToken() {
        $request = Request::createFromGlobals();
        $token = $request->headers->get('Authorization', null);
        if (null !== $token) {
            list(, $token) = explode(' ', $token);
        }
        return $token;
    }

    /**
     * Verifica se existe algum usuário autenticado no request atual
     *
     * @return bool
     * @throws \InvalidArgumentException
     */
    public static function isLoggedIn() {
        $token = static::getAccessToken();
        if (null !== $token) {
            //Valida se o access_token está válido e se foi criado pelo sistema
            $tokenBO = new TokenBO();
            $tokenUsuario = $tokenBO->getTokenUsuario($token);
            if (null !== $tokenUsuario) {
                return true;
            }
        }
        return false;
    }

    public static function getLoggedInUser() {
        if (static::isLoggedIn()) {
            $token = static::getAccessToken();
            try {
                $decodedToken = JWT::decode($token, Config::get('jwt.key'), Config::get('jwt.alg'));
                $usuarioBO = new UsuarioBO();
                return $usuarioBO->buscarUsuarioPorId($decodedToken->usuarioId);
            } catch (\Exception $e) {
                Logger::getInstance()->exception($e);
            }
        }
        return null;
    }
}