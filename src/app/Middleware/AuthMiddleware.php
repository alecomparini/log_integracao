<?php

namespace App\Middleware;

use App\Core\Router;
use App\Exceptions\AuthorizationException;
use App\Helper\PermissaoHelper;
use App\Helper\Utils;
use Symfony\Component\HttpFoundation\Request;

/**
 * Class AuthMiddleware
 * @package App\Middleware
 */
class AuthMiddleware extends BaseMiddleware {

    /**
     * Trata uma requisição.
     * Verifica token de acesso e permissão de acesso a recursos.
     * @param Request $request
     * @throws AuthorizationException
     */
    public function handle(Request $request) {
        // Tenta pegar o token da URL
        $token = $request->query->get('access_token');

        // Caso não encontrado, tento pegar do cabeçalho
        if (is_null($token)) {
            $token = $request->headers->get('auth');
            if (!$token) {
                throw new AuthorizationException('Faltando o cabeçalho Auth', 23);
            }
        }
        // Checa permissão de acordo com o token obtido
        $token = Utils::sanitizeString($token);
        $routeParams = Router::getParams();
        $escopo = $routeParams['_path'];
        $temPermissao = PermissaoHelper::checaPermissaoTokenEscopo($token, $escopo);
        if (!$temPermissao) {
            throw new AuthorizationException("Sem permissão para acessar o recurso ({$escopo})", 23);
        }

        return $this->handleNext($request);
    }
}