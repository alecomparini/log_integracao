<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29-02-2016
 * Time: 14:31
 */

namespace App\Middleware;


use Symfony\Component\HttpFoundation\Request;

class TestMiddleware extends BaseMiddleware {

    public function handle(Request $request) {
        echo 'test';
        return $this->handleNext($request);
    }
}