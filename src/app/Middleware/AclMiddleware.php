<?php
namespace App\Middleware;

use App\BO\TokenBO;
use App\Core\Config;
use App\Core\Router;
use App\Exceptions\AuthenticationException;
use App\Exceptions\AuthorizationException;
use App\Facades\Auth;
use App\Model\Response;
use DateTime;
use Exception;
use Firebase\JWT\JWT;
use Symfony\Component\HttpFoundation\Request;

class AclMiddleware extends BaseMiddleware {
    public function handle(Request $request) {
        //if (Auth::isLoggedIn()) {
        //    $user = Auth::getLoggedInUser();
        //
        //    // Verifica a permissão de alguma maneira eficiente. Implementar ACL
        //    if ($user->parceiroId !== null) {
        //        //throw new AuthorizationException('Sem permissão para o recurso');
        //    }
        //}
        return $this->handleNext($request);
    }
}