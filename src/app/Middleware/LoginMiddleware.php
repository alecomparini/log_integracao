<?php
namespace App\Middleware;

use App\BO\TokenBO;
use App\Core\Config;
use App\Exceptions\AuthenticationException;
use App\Exceptions\AuthorizationException;
use App\Facades\Auth;
use App\Model\Response;
use DateTime;
use Exception;
use Firebase\JWT\JWT;
use Symfony\Component\HttpFoundation\Request;

class LoginMiddleware extends BaseMiddleware {
    public function handle(Request $request) {
        $authHeader = $request->headers->get('Authorization', null);
        $token = null;
        if (null !== $authHeader) {
            list(, $token) = explode(' ', $authHeader);
        }

        if (null === $token) {
            throw new AuthenticationException('Não foi informado o access_token', Response::ACCESS_TOKEN_NAO_ENCONTRADO);
        }

        //Valida se o access_token está válido
        $tokenBO = new TokenBO();
        $tokenUsuario = $tokenBO->getTokenUsuario($token);
        if (null === $tokenUsuario) {
            throw new AuthenticationException('Não foi encontrado o access_token informado', Response::ACCESS_TOKEN_NAO_ENCONTRADO);
        }

        try {
            JWT::decode($token, Config::get('jwt_key'), array('HS256'));
        } catch (Exception $e) {
            throw new AuthenticationException('Não foi possível decodificar o token', Response::ACCESS_TOKEN_INVALIDO, $e);
        }

        return $this->handleNext($request);
    }
}