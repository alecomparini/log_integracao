<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29-02-2016
 * Time: 12:14
 */

namespace App\Middleware\Contracts;


use Symfony\Component\HttpFoundation\Request;

interface IMiddleware {
    public function __construct($next);
    public function handle(Request $request);
}