<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29-02-2016
 * Time: 12:16
 */

namespace App\Middleware;

use App\Middleware\Contracts\IMiddleware;
use Symfony\Component\HttpFoundation\Request;

/**
 * Classe base para tratamento de regras em nível de middleware
 * Class BaseMiddleware
 * @package App\Middleware
 */
abstract class BaseMiddleware implements IMiddleware {
    protected $next;

    /**
     * BaseMiddleware constructor.
     * @param $next
     */
    public function __construct($next) {
        if ($next instanceof IMiddleware) {
            $this->next = $next;
        }
    }

    /**
     * Altera o cursor para tratar a próxima regra a nível de middleware
     * @param Request $request
     */
    public function handleNext(Request $request) {
        if (!is_null($this->next)) {
            $this->next->handle($request);
        }
    }
}