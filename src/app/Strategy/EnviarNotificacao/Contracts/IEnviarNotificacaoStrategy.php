<?php
namespace App\Strategy\EnviarNotificacao\Contracts;

interface IEnviarNotificacaoStrategy {
    function notificar($notificacao, $url);
}