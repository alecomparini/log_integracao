<?php
/**
 * Created by PhpStorm.
 * User: suportesoftbox
 * Date: 06/06/16
 * Time: 15:27
 */

namespace App\Strategy\EnviarNotificacao;

use App\Core\Benchmark;
use App\Helper\Http;
use App\Strategy\EnviarNotificacao\Contracts\IEnviarNotificacaoStrategy;

class DefaultPrecoEstoqueStrategy implements IEnviarNotificacaoStrategy {
    public function notificar($notificacao, $url) {
        $response = "";
        Benchmark::start('Tempo de post');
        $resp = Http::post($url, array(
            'skuId' => $notificacao->produtoCodigo,
            'stockModified' => $notificacao->modificacaoEstoque == 1,
            'priceModified' => $notificacao->modificacaoPreco == 1,
        ), true);
        $response .= sprintf('Tempo para realizar o post: %s segundos', Benchmark::end('Tempo de post', true)) . PHP_EOL;

        // Parceiro foi notificado
        if ($resp->responseCode >= Http::OK && $resp->responseCode <= 299) {
            Benchmark::start('Delete se ok');
            $response .= 'Parceiro ID: ' . $notificacao->parceiroId . ' notificado sobre o produto ' . $notificacao->produtoCodigo . PHP_EOL;
            $this->notificacaoParceiroDAO->delete(array(
                'ParceiroId' => $notificacao->parceiroId,
                'ProdutoCodigo' => $notificacao->produtoCodigo
            ));

            $response .= sprintf('Tempo para executar delete se OK: %s segundos', Benchmark::end('Delete se ok', true)) . PHP_EOL;
        } else {
            // Parceiro não foi notificado
            Benchmark::start('Update se erro');
            $this->notificacaoParceiroDAO->update(array(
                'ParceiroId' => $notificacao->parceiroId,
                'ProdutoCodigo' => $notificacao->produtoCodigo
            ), array(
                'DataUltimaTentativa' => date("Y-m-d H:i:s"),
                'Tentativas' => $notificacao->tentativas + 1
            ));
            $response .= sprintf('Tempo para executar update se Erro: %s segundos', Benchmark::end('Update se erro', true)) . PHP_EOL;
            $response .= 'Parceiro ID: ' . $notificacao->parceiroId . ' erro ao notificar produto ' . $notificacao->produtoCodigo . '. Atualizando' . PHP_EOL;
        }

        return $response;
    }
}