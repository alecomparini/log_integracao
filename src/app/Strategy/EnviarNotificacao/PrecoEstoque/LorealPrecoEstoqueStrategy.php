<?php
namespace App\Strategy\EnviarNotificacao;

use App\BO\SkuParceiroBO;
use App\Strategy\EnviarNotificacao\Contracts\IEnviarNotificacaoStrategy;

class LorealPrecoEstoqueStrategy implements IEnviarNotificacaoStrategy {
    public function notificar($dados, $url) {
        // Verifica se tem esse SKU no nosso banco.

        // Se não tiver, verifica no parceiro se tem SKU

        // Se achar, atualiza no banco

        // Envia notificação
    }
}