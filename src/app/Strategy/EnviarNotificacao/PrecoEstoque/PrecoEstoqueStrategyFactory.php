<?php
namespace App\Strategy\EnviarNotificacao\PrecoEstoque;

use App\Core\Contracts\IFactory;
use App\Strategy\EnviarNotificacao\DefaultPrecoEstoqueStrategy;
use App\Strategy\EnviarNotificacao\LorealPrecoEstoqueStrategy;

class PrecoEstoqueStrategyFactory implements IFactory {
    /**
     * @param $type
     *
     * @return \App\Strategy\EnviarNotificacao\DefaultPrecoEstoqueStrategy|\App\Strategy\EnviarNotificacao\LorealPrecoEstoqueStrategy
     */
    public static function create($type) {
        if ($type == 'loreal') {
            return new LorealPrecoEstoqueStrategy();
        }

        return new DefaultPrecoEstoqueStrategy();
    }
}