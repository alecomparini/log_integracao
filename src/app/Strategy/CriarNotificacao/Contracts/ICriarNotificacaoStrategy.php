<?php
namespace App\Strategy\CriarNotificacao\Contracts;

use App\DAO\NotificacaoDAO;

/**
 * Interface ICriarNotificacaoStrategy
 * Responsável por criar as notificações
 * Deve ser implementadas nas estratégias
 *
 * @package App\Strategy\CriarNotificacao\Contracts
 */
interface ICriarNotificacaoStrategy {
    public function criarNotificacaoPreco($parceiroId, array $skus);
    public function criarNotificacaoEstoque($parceiroId, array $skus);
}