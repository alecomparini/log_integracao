<?php
namespace App\Strategy\CriarNotificacao;

use App\BO\ParceiroSiteBO;
use App\BO\ParceiroSitePermissaoBO;
use App\BO\ProdutoBO;
use App\DAO\NotificacaoDAO;
use App\Model\NotificacaoParceiro;
use App\Strategy\CriarNotificacao\Contracts\ICriarNotificacaoStrategy;

/**
 * Class DefaultCriarNotificacaoStrategy
 *
 * @package App\Strategy\CriarNotificacao
 */
class DefaultCriarNotificacaoStrategy implements ICriarNotificacaoStrategy {

    /**
     * @var \App\DAO\NotificacaoDAO
     */
    private $notificacaoDAO;

    /**
     * LorealCriarNotificacaoStrategy constructor.
     *
     * @param \App\DAO\NotificacaoDAO $notificacaoDAO
     */
    public function __construct(NotificacaoDAO $notificacaoDAO) {
        $this->notificacaoDAO = $notificacaoDAO;
    }

    /**
     * @param       $parceiroId
     * @param array $skus
     */
    public function criarNotificacaoPreco($parceiroId, array $skus) {
        $this->criarNotificacaoPrecoEstoque($parceiroId, NotificacaoParceiro::NOTIFICACAO_PRECO, $skus);
    }

    /**
     * @param       $parceiroId
     * @param array $skus
     */
    public function criarNotificacaoEstoque($parceiroId, array $skus) {
        $this->criarNotificacaoPrecoEstoque($parceiroId, NotificacaoParceiro::NOTIFICACAO_ESTOQUE, $skus);
    }

    /**
     * @param       $parceiroId
     * @param       $tipoNotificacao
     * @param array $skus
     *
     * @throws \App\Exceptions\ValidacaoException
     * @throws \Exception
     */
    private function criarNotificacaoPrecoEstoque($parceiroId, $tipoNotificacao, array $skus) {
        $parceiroBO = new ParceiroSiteBO();
        $pspBO = new ParceiroSitePermissaoBO();
        $produtoBO = new ProdutoBO();

        $parceiro = $parceiroBO->getParceiroSiteById($parceiroId);
        $notificacoes = array();

        foreach ($skus as $sku) {
            // Verifica se tem permissão
            if ($pspBO->verificaPermissaoProduto($sku, $parceiroId)) {

                // Verifica se tem preço
                $produtoPreco = $produtoBO->getEstoquePlus($parceiroId, array($sku));
                if (sizeof($produtoPreco) > 0 && $produtoPreco[0]->PrecoPor > 0) {
                    $tmp = array('parceiroId' => $parceiro->parceiroId, 'siteId' => $parceiro->siteId, 'produtoCodigo' => $sku);
                    $notificacoes[] = $tmp;
                }
            }
        }

        $notificacaoDAO = new NotificacaoDAO();
        foreach ($notificacoes as $notificacao) {
            $notificacaoDAO->setNotificacao($notificacao, $tipoNotificacao);
        }
    }
}