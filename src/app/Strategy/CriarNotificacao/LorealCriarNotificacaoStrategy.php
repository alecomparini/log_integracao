<?php
namespace App\Strategy\CriarNotificacao;

use App\BO\ParceiroSiteBO;
use App\BO\ParceiroSitePermissaoBO;
use App\DAO\NotificacaoDAO;
use App\Model\NotificacaoParceiro;
use App\Strategy\CriarNotificacao\Contracts\ICriarNotificacaoStrategy;

/**
 * Class LorealCriarNotificacaoStrategy
 *
 * @package App\Strategy\CriarNotificacao
 */
class LorealCriarNotificacaoStrategy implements ICriarNotificacaoStrategy {
    /**
     * @param       $parceiroId
     * @param array $skus
     */
    public function criarNotificacaoPreco($parceiroId, array $skus) {
        $this->criarNotificacaoPrecoEstoque($parceiroId, NotificacaoParceiro::NOTIFICACAO_PRECO, $skus);
    }

    /**
     * @param       $parceiroId
     * @param array $skus
     */
    public function criarNotificacaoEstoque($parceiroId, array $skus) {
        $this->criarNotificacaoPrecoEstoque($parceiroId, NotificacaoParceiro::NOTIFICACAO_ESTOQUE, $skus);
    }

    /**
     * @param       $parceiroId
     * @param       $tipoNotificacao
     * @param array $skus
     *
     * @throws \App\Exceptions\ValidacaoException
     * @throws \Exception
     */
    private function criarNotificacaoPrecoEstoque($parceiroId, $tipoNotificacao, array $skus) {
        $parceiroBO = new ParceiroSiteBO();
        $pspBO = new ParceiroSitePermissaoBO();

        $parceiro = $parceiroBO->getParceiroSiteById($parceiroId);
        $notificacoes = array();

        // Busca os SKUs do parceiro
        $poolSize = Config::get('services.notifications.poolsize', 50);
        $host = Config::get('services.notifications.host', 'integracao.dev');
        $port = Config::get('services.notifications.port', 80);
        $timeout = Config::get('services.notifications.timeout', 1);
        $username = Config::get('services.notifications.username', 'william');
        $password = Config::get('services.notifications.password', 'william');
        $pool = new JobPoolExecutor($poolSize, $timeout, $host, $port, $username, $password);

        foreach ($skus as $sku) {
            $pool->addJob("/servicos/parceiro/sku/{$parceiroId}/{$sku}");
        }

        $pool->processAll();
        $respostas = $pool->getAllResponses();
        foreach ($respostas as $resposta) {
            $data = json_decode($resposta);
            if (!$data->success) {
                $skus = array_diff($skus, array($data->codigo));
            }
        }

        foreach ($skus as $sku) {
            // Verifica se tem permissão
            if ($pspBO->verificaPermissaoProduto($sku, $parceiroId)) {
                $tmp = array('parceiroId' => $parceiro->parceiroId, 'siteId' => $parceiro->siteId, 'produtoCodigo' => $sku);
                $notificacoes[] = $tmp;
            }
        }

        $notificacaoDAO = new NotificacaoDAO();
        foreach ($notificacoes as $notificacao) {
            $notificacaoDAO->setNotificacao($notificacao, $tipoNotificacao);
        }
    }
}