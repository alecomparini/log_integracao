<?php
namespace App\Strategy\CriarNotificacao;

use App\Core\Contracts\IFactory;

/**
 * Class CriarNotificacaoStrategyFactory
 * Fábrica que retorna qual estratégia será utilizadas
 *
 * @package App\Strategy\CriarNotificacao
 */
abstract class CriarNotificacaoStrategyFactory implements IFactory {
    /**
     * Cria uma estratégia de acordo com o type informado
     *
     * @param string $type
     *
     * @return \App\Strategy\CriarNotificacao\Contracts\ICriarNotificacaoStrategy
     */
    public static function create($type) {
        if ($type == 'loreal') {
            return new LorealCriarNotificacaoStrategy();
        }

        return new DefaultCriarNotificacaoStrategy();
    }
}