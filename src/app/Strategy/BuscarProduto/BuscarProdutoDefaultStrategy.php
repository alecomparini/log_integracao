<?php
namespace App\Strategy\BuscarProduto;

use App\BO\ParceiroSiteBO;
use App\BO\ParceiroSitePermissaoBO;
use App\BO\ProdutoBO;
use App\DAO\ParceiroSiteDAO;
use App\Exceptions\BusinessException;
use App\Exceptions\ValidacaoException;
use App\Helper\Utils;
use App\Model\ParceiroSitePermissao;
use App\Model\Produto;
use App\Strategy\BuscarProduto\Contracts\IBuscarProdutoStrategy;

class BuscarProdutoDefaultStrategy implements IBuscarProdutoStrategy {
    private $cachePagina = array();

    public function buscarPorCodigo($parceiroId, $codigos) {
        $parceiroBO = new ParceiroSiteBO();
        $parceiro = $parceiroBO->getParceiroSiteById($parceiroId);

        if (is_array($codigos) && count($codigos) > Produto::QTD_MAX_PRODUTO_CONSULTA) {
            throw new ValidacaoException(sprintf('O campo [produtoCodigo] deve conter no máximo %s elementos.', Produto::QTD_MAX_PRODUTO_CONSULTA));
        }

        $parceiroTemCampanha = false;
        $parceiroSiteBO = new ParceiroSiteBO();
        $parceiroDAO = new ParceiroSiteDAO();
        $parceiroSite = $parceiroSiteBO->getParceiroSiteById($parceiroId);
        $parceiroSitePermissaoBO = new ParceiroSitePermissaoBO();
        if (!$parceiroSitePermissaoBO->parceiroPermissaoTudo($parceiroId, $parceiroSite->siteBase, ParceiroSitePermissao::TUDO)) {
            $parceiroCampanha = $parceiroDAO->getParceiroSiteCampanhaAtiva($parceiroId, (array) $codigos);
            if (count($parceiroCampanha) > 0) {
                $parceiroTemCampanha = true;
                $codigos = $parceiroCampanha;
            }
        }

        $produtoBO = new ProdutoBO();
        $produtos = $produtoBO->getProdutoFull($parceiro->siteBase, $codigos, null, $parceiroId, $parceiroTemCampanha);

        if (count($produtos['produtos']) === 0) {
            throw new BusinessException('Nenhum produto encontrado', 17);
        }
        $produtosServicos = $produtoBO->getServicos($produtos, $parceiro->siteBase);
        $this->preparaRetornoRest($produtosServicos['produtos']);
        return Utils::lcfirstRecursivo($produtosServicos['produtos']);
    }

    public function buscarPorCodigoRetail($parceiroId, $codigosRetail) {
        $parceiroBO = new ParceiroSiteBO();
        $parceiro = $parceiroBO->getParceiroSiteById($parceiroId);

        if (is_array($codigosRetail) && count($codigosRetail) > Produto::QTD_MAX_PRODUTO_CONSULTA) {
            throw new ValidacaoException(sprintf('O campo [produtoCodigo] deve conter no máximo %s elementos.', Produto::QTD_MAX_PRODUTO_CONSULTA));
        }

        $produtoBO = new ProdutoBO();
        $produtos = $produtoBO->getProdutoFull($parceiro->siteBase, $codigosRetail, null, $parceiroId, false, true);

        if (count($produtos['produtos']) === 0) {
            throw new BusinessException('Nenhum produto encontrado', 17);
        }
        $produtosServicos = $produtoBO->getServicos($produtos, $parceiro->siteBase);
        $this->preparaRetornoRest($produtosServicos['produtos']);
        return Utils::lcfirstRecursivo($produtosServicos['produtos']);
    }

    public function buscarPaginado($parceiroId, $pagina = 1, array $produtoStatus = array()) {
        $produtoBO = new ProdutoBO();

        if (!isset($this->cachePagina[$parceiroId][$pagina])) {
            $parceiroBO = new ParceiroSiteBO();
            $parceiro = $parceiroBO->getParceiroSiteById($parceiroId);

            $parceiroDAO = new ParceiroSiteDAO();
            $parceiroTemCampanha = false;
            $parceiroSitePermissaoBO = new ParceiroSitePermissaoBO();
            $produtoCodigo = array();
            if (!$parceiroSitePermissaoBO->parceiroPermissaoTudo($parceiroId, $parceiro->siteBase, ParceiroSitePermissao::TUDO)) {
                $parceiroCampanha = $parceiroDAO->getParceiroSiteCampanhaAtiva($parceiroId, (array) $produtoCodigo);
                if (count($parceiroCampanha) > 0) {
		            $parceiroTemCampanha = true;
                    $produtoCodigo = $parceiroCampanha;
                }
            }

            $getProdutoFullResponse = $produtoBO->getProdutoFull($parceiro->siteBase, $produtoCodigo, $pagina, $parceiroId, $parceiroTemCampanha, false, $produtoStatus);

            $this->cachePagina[$parceiroId][$pagina] = $getProdutoFullResponse;
            $produtos = $getProdutoFullResponse;
        } else {
            $produtos = $this->cachePagina[$parceiroId][$pagina];
        }
        $produtosServicos = $produtoBO->getServicos($produtos, $parceiro->siteBase);
        $this->preparaRetornoRest($produtosServicos['produtos']);
        return Utils::lcfirstRecursivo($produtosServicos['produtos']);
    }

    public function buscarDadosPaginacao($parceiroId, array $produtoStatus = array()) {
        if (!array_key_exists($parceiroId, $this->cachePagina)) {
            $parceiroBO = new ParceiroSiteBO();
            $parceiro = $parceiroBO->getParceiroSiteById($parceiroId);

            $produtoBO = new ProdutoBO();
            $getProdutoFullResponse = $produtoBO->getProdutoFull($parceiro->siteBase, array(), 1, $parceiroId, false, false, $produtoStatus);

            $this->cachePagina[$parceiroId][1] = $getProdutoFullResponse;
            $dadosPaginacao = $getProdutoFullResponse['$dadosPaginacao'];
        } else {
            $response = current($this->cachePagina[$parceiroId]);
            $dadosPaginacao = $response['dadosPaginacao'];
        }

        return $dadosPaginacao;
    }

    private function preparaRetornoRest(&$produtos) {
        foreach ($produtos as &$produto) {
            if (is_array($produto->Filhos) && count($produto->Filhos) > 0) {
                foreach ($produto->Filhos as &$filho) {
                    $filho = $filho->preparaRetornoRest();
                }
            }

            $produto = $produto->preparaRetornoRest();
        }
    }
}
