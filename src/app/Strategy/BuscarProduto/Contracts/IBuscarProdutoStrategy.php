<?php
namespace App\Strategy\BuscarProduto\Contracts;

interface IBuscarProdutoStrategy {
    public function buscarPorCodigo($parceiroId, $codigos);
    public function buscarPaginado($parceiroId, $pagina = 1, array $produtoStatus = array());
    public function buscarDadosPaginacao($parceiroId, array $produtoStatus = array());
}