<?php
namespace App\Strategy\BuscarProduto;

class BuscarProdutoShopfacilStrategy extends BuscarProdutoDefaultStrategy {
    public function buscarPorCodigo($parceiroId, $codigos) {
        $produtos = parent::buscarPorCodigo($parceiroId, $codigos);
        $this->trataVendeAvulso($produtos);
        return $produtos;
    }

    public function buscarPaginado($parceiroId, $pagina = 1, array $produtoStatus = array()) {
        $produtos = parent::buscarPaginado($parceiroId, $pagina, $produtoStatus);
        $this->trataVendeAvulso($produtos);
        return $produtos;
    }

    private function trataVendeAvulso(&$produtos) {
        foreach ($produtos as $produto) {
            if (0 === (int)$produto->vendeAvulso) {
                $produto->disponivel = 0;
            }

            if (0 === (int)$produto->status) {
                $produto->disponivel = 0;
            }

            //Solicitado pelo Pablo em 30/08
            /*if (0 === (int)$produto->emLinha) {
                $produto->disponivel = 0;
            }*/
        }
    }
}