<?php
namespace App\Strategy\BuscarProduto;

use App\Core\Contracts\IFactory;
use App\Exceptions\FrameworkException;

class BuscarProdutoStrategyFactory implements IFactory {
    /**
     * Retorna uma classe que implementa IBuscarProduto
     *
     * @param string $strategyName
     *
     * @return \App\Strategy\BuscarProduto\Contracts\IBuscarProdutoStrategy
     * @throws \App\Exceptions\FrameworkException
     */
    public static function create($strategyName) {
        $strategyClassName = __NAMESPACE__ . NSS . sprintf("BuscarProduto%sStrategy", ucfirst($strategyName));
        if (class_exists($strategyClassName)) {
            return new $strategyClassName();
        }
        throw new FrameworkException("Classe [$strategyClassName] não encontrada.");
    }
}