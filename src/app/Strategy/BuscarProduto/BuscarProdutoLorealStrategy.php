<?php
namespace App\Strategy\BuscarProduto;

use App\BO\ProdutoBO;
use App\DAO\ProdutoDAO;
use App\Strategy\BuscarProduto\Contracts\IBuscarProdutoStrategy;

class BuscarProdutoLorealStrategy implements IBuscarProdutoStrategy {

    private $produtoBO;
    /** @var \App\Model\V2\ProdutosPaginado[] */
    private $cache = array();

    /**
     * BuscarProdutoLorealStrategy constructor.
     */
    public function __construct() {
        $this->produtoBO = new ProdutoBO();
    }

    public function buscarPorCodigo($parceiroId, $codigos) {
        $produtoDAO = new ProdutoDAO();
        return $produtoDAO->buscarPorCodigo($parceiroId, $codigos);
    }

    public function buscarPaginado($parceiroId, $pagina = 1, array $produtoStatus = array()) {
        if (!array_key_exists($pagina, $this->cache)) {
            $produtoDAO = new ProdutoDAO();
            $this->cache[$pagina] = $produtoDAO->buscarPaginado($parceiroId, $pagina, $produtoStatus);
        }
        return $this->cache[$pagina]->produtos;
    }

    public function buscarDadosPaginacao($parceiroId, array $produtoStatus = array()) {
        if (count($this->cache) === 0) {
            $this->buscarPaginado($parceiroId, 1, $produtoStatus);
        }
        $produtoPaginado = current($this->cache);
        return $produtoPaginado->dadosPaginacao;
    }
}