<?php
namespace App\Strategy\BuscarEstoque;

use App\BO\CategoriaBO;
use App\BO\ParceiroSiteBO;
use App\BO\ProdutoBO;
use App\Model\CategoriaDescontoParceiro;
use App\DAO\ProdutoEstoqueDAO;
use App\DAO\ParceiroSiteDAO;
use App\Strategy\BuscarEstoque\Contracts\IBuscarEstoqueStrategy;

class BuscarEstoqueCocacolaStrategy implements IBuscarEstoqueStrategy {
    /**
     * Busca o estoque de determinados produtos dado uma lista de SKU
     *
     * @param $parceiroId
     * @param $codigos
     *
     * @return mixed
     */
    public function buscarEstoque($parceiroId, $codigos) {

        $produtoEstoqueDAO = new ProdutoEstoqueDAO();
        $parceiroSite = new ParceiroSiteDAO();
        $Site = $parceiroSite->getParceiroSiteById($parceiroId);

        $estoques = $produtoEstoqueDAO->getEstoques($parceiroId, $Site->siteBase, $codigos, true);

        return $estoques;
    }

    public function precisaVerificarPreco() {
        return false;
    }
}