<?php
namespace App\Strategy\BuscarEstoque;

use App\BO\CategoriaBO;
use App\BO\ProdutoBO;
use App\Model\CategoriaDescontoParceiro;
use App\Strategy\BuscarEstoque\Contracts\IBuscarEstoqueStrategy;

class BuscarEstoqueShopfacilStrategy extends BuscarEstoqueDefaultStrategy {
    /**
     * Busca o estoque de determinados produtos dado uma lista de SKU
     *
     * @param $parceiroId
     * @param $codigos
     *
     * @return mixed
     * @throws \App\Exceptions\ValidacaoException
     * @throws \App\Exceptions\BusinessException
     */
    public function buscarEstoque($parceiroId, $codigos) {
        $estoques = parent::buscarEstoque($parceiroId, $codigos);
        $this->aplicaRegraVendeAvulso($estoques);
        return $estoques;
    }

    private function aplicaRegraVendeAvulso(&$estoques) {
        foreach ($estoques as $estoque) {
            if (0 === (int)$estoque->vendeAvulso) {
                $estoque->disponivel = 0;
            }

            //Solicitacao Pablo 30/08
            /*if (0 === (int)$estoque->emLinha) {
                $estoque->disponivel = 0;
            }*/

            if (0 === (int)$estoque->Status) {
                $estoque->disponivel = 0;
            }
        }
    }
}