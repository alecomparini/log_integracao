<?php
namespace App\Strategy\BuscarEstoque;

use App\Core\Contracts\IFactory;
use App\Exceptions\FrameworkException;

class BuscarEstoqueStrategyFactory implements IFactory {
    /**
     * @param $strategyName
     *
     * @return \App\Strategy\BuscarEstoque\Contracts\IBuscarEstoqueStrategy
     * @throws \App\Exceptions\FrameworkException
     */
    public static function create($strategyName) {
        $strategyClassName = __NAMESPACE__ . NSS . sprintf("BuscarEstoque%sStrategy", ucfirst($strategyName));
        if (class_exists($strategyClassName)) {
            return new $strategyClassName();
        }
        throw new FrameworkException("Classe [$strategyClassName] não encontrada.");
    }
}