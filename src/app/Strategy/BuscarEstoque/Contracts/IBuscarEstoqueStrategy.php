<?php
namespace App\Strategy\BuscarEstoque\Contracts;

interface IBuscarEstoqueStrategy {
    /**
     * Busca o estoque de determinados produtos dado uma lista de SKU
     * @param $parceiroId
     * @param $codigos
     *
     * @return mixed
     */
    public function buscarEstoque($parceiroId, $codigos);

    public function precisaVerificarPreco();
}