<?php
namespace App\Strategy\BuscarEstoque;

use App\BO\CategoriaBO;
use App\BO\ProdutoBO;
use App\Model\CategoriaDescontoParceiro;
use App\Strategy\BuscarEstoque\Contracts\IBuscarEstoqueStrategy;

class BuscarEstoqueDefaultStrategy implements IBuscarEstoqueStrategy {
    /**
     * Busca o estoque de determinados produtos dado uma lista de SKU
     *
     * @param $parceiroId
     * @param $codigos
     *
     * @return mixed
     * @throws \App\Exceptions\ValidacaoException
     * @throws \App\Exceptions\BusinessException
     */
    public function buscarEstoque($parceiroId, $codigos) {
        $parceiroTemCampanha = false;
        $produtoBO = new ProdutoBO();
        $estoques = $produtoBO->getEstoquePlus($parceiroId, $codigos, $parceiroTemCampanha);

        if (is_array($estoques) && !$parceiroTemCampanha && count($estoques) > 0) {
            $categoriaBO = new CategoriaBO();
            $categoriasDescontos = $categoriaBO->buscaCategoriaDescontoParceiro($parceiroId);

            // Rearranja as categorias para buscar pelo índice
            $categoriasIndice = array();
            foreach ($categoriasDescontos as $catDesc) {
                $categoriasIndice[$catDesc->categoriaId] = $catDesc;
            }

            foreach ($estoques as &$produto) {
                $this->calculaDescontoCategoria($produto, $categoriasIndice);
            }
            unset($produto);
        }

        return $estoques;
    }

    /**
     * Retorna informações de estoque de acordo com o codigo retail informado
     *
     * Altera o valor do campo PrecoPor de acordo com os descontos cadastrados
     * por categoria e parceiro no SGW
     *
     * @param       $produto          - Produto a ter o valor recalculado, caso seja necessário!!! <-_->
     * @param array $categoriasIndice - Descontos cadastrados para parceiro e categoria
     *
     * @throws BusinessException
     */
    private function calculaDescontoCategoria(&$produto, &$categoriasIndice) {
        // Verifica se tem desconto por categoria
        if (isset($categoriasIndice[$produto->LojaId])) {
            /** @var $desconto CategoriaDescontoParceiro */
            $desconto = $categoriasIndice[$produto->LojaId];
            if ($desconto instanceof CategoriaDescontoParceiro) {
                // Chega o tipo de desconto
                // Tipo 1 => Percentual, 2 => Desconto fixo
                if ($desconto->tipoDesconto == 1) {
                    $produto->PrecoPor *= ((100 - $desconto->desconto) / 100);
                } else if ($desconto->tipoDesconto == 2) {
                    $produto->PrecoPor -= $desconto->desconto;
                }
            }
        }
    }

    public function precisaVerificarPreco() {
        return true;
    }
}