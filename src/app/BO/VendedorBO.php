<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 31/12/2015
 * Time: 10:46
 */

namespace App\BO;

use App\DAO\VendedorDAO;
use App\Exceptions\ValidacaoException;
use App\Helper\Utils;
use App\Helper\Validacao;

/**
 * Class VendedorBO
 *
 * @package App\BO
 */
class VendedorBO extends BaseBO {
    /** @var \App\DAO\VendedorDAO */
    private $vendedorDAO;

    public function __construct() {
        parent::__construct();
        $this->vendedorDAO = new VendedorDAO();
    }

    /**
     * Retorna dados do vendedor
     *
     * @param $email
     * @param $senha
     *
     * @return \App\Model\Vendedor
     * @throws \Exception
     */
    public function getVendedor($email, $senha) {
        $obj = (object)array(
            'email' => $email,
            'senha' => $senha
        );

        $regras = array(
            'email' => array('notempty', 'required', 'email'),
            'senha' => array('notempty', 'required', 'str'),
        );

        $validacao = new Validacao($regras);
        if (!$validacao->executaValidacao($obj)) {
            throw new ValidacaoException($validacao->msg, 2);
        }

        $vendedor = $this->vendedorDAO->getVendedorByEmail($email);

        if (null !== $vendedor) {

            // Valida a senha
            if ($vendedor->senha !== Utils::criptSenhaB2C($senha)) {
                return null;
            }

            $token = sprintf("%s|%s", $vendedor->id, $vendedor->grupoId);
            $vendedor->token = Utils::encryptDecrypt('encrypt', $token);

            $vendedor->tipo = (1 === (int)$vendedor->tipo ? 'Gerente' : 'Vendedor');
        }

        return $vendedor;
    }
}