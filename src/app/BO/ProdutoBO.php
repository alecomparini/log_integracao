<?php
namespace App\BO;

use App\Core\Config;
use App\Core\Logger;
use App\DAO\CartaoDAO;
use App\DAO\CatalogoProdutoDAO;
use App\DAO\GarantiaDAO;
use App\DAO\ParceiroSiteDAO;
use App\DAO\ProdutoDAO;
use App\DAO\ProdutoEstoqueDAO;
use App\DAO\SeguroDAO;
use App\DAO\SkuParceiroDAO;
use App\Exceptions\BusinessException;
use App\Exceptions\DatabaseException;
use App\Exceptions\PageNotFoundException;
use App\Exceptions\ValidacaoException;
use App\Helper\Utils;
use App\Helper\Validacao;
use App\Model\CategoriaDescontoParceiro;
use App\Model\Configuracao;
use App\Model\EstoqueParceiro;
use App\Model\ParceiroSite;
use App\Model\Parcelamento;
use App\Model\Response;
use App\Strategy\BuscarProduto\BuscarProdutoStrategyFactory;
use App\Model\ParceiroSitePermissao;
use App\Model\Produto;
use Exception;

/**
 * Class ProdutoBO
 * Responsável por processar as solicitações que envolvem produtos
 *
 * @author Cristiano Gomes <cristianogomes@softbox.com.br>
 * @package App\BO
 */
class ProdutoBO extends BaseBO {

    const QTD_MAX_CONSULTA_ESTOQUE = 100;

    /**
     * @var ProdutoDAO
     */
    public $produtoDAO;

    public function __construct() {
        $this->produtoDAO = new ProdutoDAO();
        parent::__construct();
    }

    /**
     * Busca os estoques disponíveis dos produtos do parceiro determinado
     *
     * @param $parceiroId
     * @param null $produtoCodigo
     * @param bool $mapear
     *
     * @return array
     * @throws \Exception
     */
    public function getEstoques($parceiroId, $produtoCodigo = null, $mapear = true) {
        $regras = array(
            'parceiroId' => array(
                'required',
                'notempty',
                'int'
            ),
            'produtoCodigo' => array(
                'tiposaceitos' => array('null', 'int_array')
            )
        );

        $obj = new \stdClass();
        $obj->parceiroId = $parceiroId;
        $obj->produtoCodigo = $produtoCodigo;

        $validacao = new Validacao($regras);

        if (!$validacao->executaValidacao($obj)) {
            throw new ValidacaoException($validacao->msg, 2);
        }

        $parceiroSiteDAO = new ParceiroSiteDAO();
        $parceiroSite = $parceiroSiteDAO->getParceiroSiteById($parceiroId);
        if (is_null($parceiroSite)) {
            throw new BusinessException('Parceiro não encontrado', 4);
        }
        $siteId = $parceiroSite->siteId;

        $produtoEstoqueDAO = new ProdutoEstoqueDAO();

        return $produtoEstoqueDAO->getEstoques($parceiroId, $siteId, $produtoCodigo);
    }

    /**
     * Efetua a busca completa por produtos com base em array de códigos do produto
     *
     * @param       $siteId
     * @param array $produtoCodigo
     * @param int $pagina
     * @param int $parceiroId
     * @param bool $parceiroTemCampanha
     * @param bool $buscarCodigoRetail
     * @param array $produtoStatus
     *
     * @return array
     * @throws ValidacaoException
     * @throws \Exception
     */
    public function getProdutoFull($siteId, $produtoCodigo = array(), $pagina = 1, $parceiroId, $parceiroTemCampanha=false, $buscarCodigoRetail = false, array $produtoStatus = array()) {
        $atributosBO = new AtributoBO();
        if (count($produtoCodigo) > 0) {

            if ($buscarCodigoRetail) {
                $produtoCodigos = $this->produtoDAO->getCodProdutosRetail($produtoCodigo);
            } else {
                $produtoCodigos = $this->produtoDAO->getCodProdutos($produtoCodigo);
            }

            if (!is_array($produtoCodigos) || count($produtoCodigos) == 0) {
                throw new BusinessException('Produtos inválidos', Response::CODIGO_PRODUTOS_INVALIDOS);
            }

            $idsProdutos = $this->mountArrayPorId($produtoCodigos);
            $dadosPaginacao = null;
        } else {
            //quando não foi passado códigos de produto a busca deve ser paginada para o cliente
            try {
                $dadosPaginacao = $this->getDadosPaginacao($siteId, $pagina, $parceiroId, $produtoStatus);
            } catch (\Exception $e) {
                throw $e;
            }

            $idsProdutos = array();
        }

        $produtos = $this->findProdutoFull($idsProdutos, array(), $siteId, null, $dadosPaginacao, $parceiroId, $produtoStatus);

        if ($parceiroTemCampanha) {
            // Lista das subcategorias e categorias do catálogo
            $catalogoCategoriaBO = new CatalogoCategoriaBO();
            $catalogoCategorias = $catalogoCategoriaBO->getCatalogoCategoriaByParceiro($parceiroId);

            // Recupera o valor de desconto da campanha
            $descontoCampanha = end($catalogoCategorias);
            $descontoCampanhaValor = $descontoCampanha->descontoCampanha;

            $objCatalogoProdutoDAO = new CatalogoProdutoDAO();
            $produtosDesconto = $objCatalogoProdutoDAO->getProdutoCatalogoComDesconto($descontoCampanha->catalogoId, $idsProdutos, $parceiroId, $siteId);
        }

        if (!is_array($produtos) || count($produtos) == 0) {
            return array(
                'dadosPaginacao' => $dadosPaginacao,
                'produtos'       => array()
            );
        }

        $categoriaBO = new CategoriaBO();
        $categoriasDescontos = $categoriaBO->buscaCategoriaDescontoParceiro($parceiroId);

        // Rearranja as categorias para buscar pelo índice
        $categoriasIndice = array();
        foreach ($categoriasDescontos as $catDesc) {
            $categoriasIndice[$catDesc->categoriaId] = $catDesc;
        }

        $produtosEncontradosId = array();
        foreach ($produtos as &$produto) {
            $produtosEncontradosId[] = $produto->produtoId;
            $isCombo = strtoupper(substr($produto->codigo, 0, 1)) == 'C';

            if ($isCombo) {
                unset($produto->EAN);
                unset($produto->CodigoFornecedor);

                $combos = $this->produtoDAO->getComboFilhos($produto->produtoId, $siteId);
                $ids = array();

                foreach ($combos as $c) {
                    $ids[] = $c->ComboId;
                }

                $produto->Filhos = $this->findProdutoFull($ids, array(), $siteId, $produto->produtoId, null, $parceiroId, $produtoStatus);

                if (count($produto->Filhos) == 0) {
                    unset($produto);
                    continue;
                }
                $disponiveis = array();
                foreach ($produto->Filhos as $filho) {
                    $disponiveis[] = $filho->Disponivel;
                    if ($filho->emLinha == Produto::STATUS_OFFLINE) {
                        unset($produto);
                        continue 2;
                    }

                    if ($parceiroTemCampanha) {
                        $this->aplicarDescontoCategoriaCampanha($filho, $catalogoCategorias, $produtosDesconto, $descontoCampanhaValor);
                    }
                }
			 $produto->Disponivel = min($disponiveis);

            } elseif ($parceiroTemCampanha) {
                $this->aplicarDescontoCategoriaCampanha($produto, $catalogoCategorias, $produtosDesconto, $descontoCampanhaValor);
            }

            $produto->atributosCorTamanho = $atributosBO->getAtributosCorTamanhoById($produto->produtoId);

            // Verifica se tem desconto por categoria
            if (isset($categoriasIndice[$produto->LojaId]) && !$parceiroTemCampanha) {
                /** @var $desconto CategoriaDescontoParceiro */
                $desconto = $categoriasIndice[$produto->LojaId];
                if ($desconto instanceof CategoriaDescontoParceiro) {
                    // Chega o tipo de desconto
                    // Tipo 1 => Percentual, 2 => Desconto fixo
                    if ($desconto->tipoDesconto == 1) {
                        $produto->PrecoPor *= ((100 - $desconto->desconto) / 100);
                    } else if ($desconto->tipoDesconto == 2) {
                        $produto->PrecoPor -= $desconto->desconto;
                    }
                }
            }

            $produto->PrecoPor = Utils::convertMoeda($produto->PrecoPor);
            if ($produto->PrecoPor <= 0) {
                $this->zeraEstoqueRetorno($parceiroId, $produto);
            }

        }

        /* Trata atributos */
        $atributosSubstituicao = $atributosBO->getAtributosSubstituicaoById($produtosEncontradosId, $siteId);
        $this->setAtributosSubstituicao($atributosSubstituicao, $produtos);

        if (is_array($dadosPaginacao) && count($dadosPaginacao) > 0) {
            unset($dadosPaginacao['limite'], $dadosPaginacao['offset']);
        }

        return array(
            'dadosPaginacao' => $dadosPaginacao,
            'produtos' => $produtos
        );
    }

    /**
     * Zera o item disponivel do produto e seus filhos
     * quando houver filho, isto é, for um combo
     *
     * @param int $parceiroId
     * @param \App\Model\Produto $produto
     */
    private function zeraEstoqueRetorno($parceiroId, &$produto) {
        $idsIgnorarRegraPreco = array("3", "5", "6", "7", "9", "10");

        if (in_array($parceiroId, $idsIgnorarRegraPreco, false)) {
            return;
        }

        if (isset($produto->Disponivel)) {
            $produto->Disponivel = 0;
        }

        if (isset($produto->disponivel)) {
            $produto->disponivel = 0;
        }

        if (isset($produto->Filhos) && is_array($produto->Filhos)) {
            foreach ($produto->Filhos as $filho) {
                $this->zeraEstoqueRetorno($parceiroId, $filho);
            }
        }
    }

    /**
     * Víncula os atributos encontrados com os produtos
     *
     * @param \App\Model\AtributoSubstituicao[] $atribSub
     * @param \App\Model\Produto[] $produtos
     */
    private function setAtributosSubstituicao($atribSub, $produtos) {
        $atribsPorProdId = array();
        foreach ($atribSub as $atrib) {
            $atribsPorProdId[$atrib->produtoIdReferencia][] = $atrib;
        }

        foreach ($produtos as $prod) {
            if (array_key_exists($prod->produtoId, $atribsPorProdId)) {
                $prod->atributosSubstituicao = $atribsPorProdId[$prod->produtoId];
            }
        }
    }

    /**
     * @param $parceiroId
     * @param $siteId
     * @param $produtoCodigo
     *
     * @return array
     * @throws \Exception
     */
    public function getProduto($parceiroId, $siteId, $produtoCodigo) {
        $parceiroDAO = new ParceiroSiteDAO();
        $parceiroSite = $parceiroDAO->getParceiroSiteById($parceiroId);

        if (!$parceiroSite instanceof ParceiroSite) {
            throw new ValidacaoException('Código de parceiro inválido.');
        }

        //Pegando os produtos IDS com base nos códigos recebidos

        /**
         * Aqui são 3 tipos de buscas diferentes:
         * - Busca por todos os produtos, considerando apenas o status = 1
         * - Busca por produtos CHANGED que possuem data de alteração
         * - Busca por produto com código específico para isso deve-se buscar o produtoID com base no código recebido
         */

        $produtoDao = new ProdutoDAO();

        if ($produtoCodigo == null || $produtoCodigo == 'ALL') {
            $produtos = $produtoDao->getProdutosAll($siteId);
        } else {
            $isChanged = substr($produtoCodigo, 0, 7);

            if ($isChanged == 'CHANGED') {
                $dataAlteracao = trim(substr($produtoCodigo, 8));

                if ($dataAlteracao != '') {
                    $produtos = $produtoDao->getProdutosChanged($siteId, $dataAlteracao);
                } else {
                    $produtos = $produtoDao->getProdutosAll($siteId);
                }

            } else {
                $produtoInfo = $produtoDao->getCodProdutos(array($produtoCodigo));

                if (is_array($produtoInfo) && count($produtoInfo) > 0) {
                    $produtoId = $produtoInfo[0]->produtoId;
                } else {
                    throw new \Exception('Erro: Produto não encontrado', 10);
                }

                $produtos = $produtoDao->getProdutos($siteId, $produtoId);
            }
        }

        if (is_array($produtos) && count($produtos)) {
            /** @var array $fatoresCartao */
            $fatoresCartao = $this->getFatoresCartao();
            $produtosValidos = array();
            $parcelas = array();

            foreach ($produtos as $produto) {
                $produto->descricao = htmlentities($produto->descricao);
                $produto->descricao = preg_replace('/[^(\x20-\x7F)]*/', '', $produto->descricao);
                $produto->descricao = html_entity_decode($produto->descricao);
                $produto->Caracteristicas = explode("|", $produto->Caracteristicas);

                $stdCaracteristicas = new \stdClass();

                if (count($produto->Caracteristicas) > 0) {
                    foreach ($produto->Caracteristicas as $k => $v) {
                        $stdCaracteristicas->$k = $v;
                    }
                }

                $produto->Caracteristicas = $stdCaracteristicas;

                for ($i = 1; $i <= 12; $i++) {
                    if ($i <= $produto->ParcelamentoMaximoSemJuros) {
                        $valorParcela = ($produto->PrecoPor / $i);
                        $juros = ' sem juros ';
                    } else {
                        $valorParcela = ($produto->PrecoPor * $fatoresCartao[$i]);
                        $juros = ' com juros ';
                    }

                    if ($valorParcela > 5) {
                        $parcelas[$i] = Utils::formataMoeda($valorParcela) . $juros;
                    } else {
                        break;
                    }
                }

                $produto->Parcelamento = count($parcelas) > 0 ? $parcelas : "";
                $produto->PrecoDe = Utils::formataMoeda($produto->PrecoDe);
                $produto->PrecoPor = Utils::formataMoeda($produto->PrecoPor);

                unset($produto->ProdutoId);
                unset($produto->UrlSite);
                $produtosValidos[] = $produto->preparaWsdl();
            }

            return $produtosValidos;
        }

        return array();
    }

    /**
     * Método de fachada para utilização dos métodos getGarantias() e getSeguro() de maneira simplificada
     *
     * @param array $produtos array de produtos resultantes
     * @param $siteId
     *
     * @return array
     */
    public function getServicos($produtos, $siteId) {
        foreach ($produtos['produtos'] as &$produto) {
            $produto = $this->getSeguros($produto, $siteId);
        }

        foreach ($produtos['produtos'] as &$produto) {
            $produto = $this->getGarantias($produto, $siteId);
        }

        return $produtos;
    }

    //privates

    private function getDadosPaginacao($siteId, $pagina = null, $parceiroId, array $produtoStatus = array()) {
        if (!$pagina) {
            $pagina = 1;
        }

        $produtoDao = new ProdutoDAO();
        $totalProdutos = $produtoDao->getTotalProdutos($siteId, $parceiroId, $produtoStatus);
        $limite = Config::get('produtos.produtos-por-pagina');
        $paginas = $totalProdutos->total / $limite;
        $teste = $paginas - (int)$paginas;

        if ($teste > 0) {
            $paginas = (int)$paginas + 1;
        }

        if ($paginas == 0) {
            $paginas = 1;
        }

        if ($pagina < 1 || $pagina > $paginas) {
            throw new PageNotFoundException('Página não encontrada');
        }

        $offset = ($pagina - 1) * $limite;

        return array(
            'totalProdutos' => $totalProdutos->total,
            'produtosPorPagina' => $limite,
            'totalPaginas' => $paginas,
            'offset' => $offset,
            'limite' => $limite,
            'paginaAtual' => $pagina
        );
    }

    private function getGarantias($produto, $siteId) {
        $objRetorno = clone $produto;
        $garantiaDao = new GarantiaDAO();
        $produtoDao = new ProdutoDAO();
        $produtoInfo = $produtoDao->getProdutoInfo($produto->produtoId, $siteId);
        $produtoInfo->produtoId = $produto->produtoId;
        $categorias = explode(',', $produtoInfo->Categorias);
        $precoPor = $produto->PrecoPor;

        if (substr($produto->codigo, 0, 1) == 'C') {
            $filhoSemGarantia = false;
            $garantiasEncontradas = array();

            foreach ($produto->Filhos as &$filho) {
                $garantias = $garantiaDao->getGarantias($filho->produtoId, $siteId, $categorias, $precoPor);

                if (is_array($garantias) && count($garantias) > 0) {
                    $arrayGarantias = $this->processaGarantia($garantias, $produtoInfo);
                } else {
                    $filhoSemGarantia = true;
                    break;
                }

                if (isset($arrayGarantias) && count($arrayGarantias) > 0 && !$filhoSemGarantia) {
                    $garantiasEncontradas[] = $arrayGarantias;
                } else {
                    $filhoSemGarantia = true;
                }
            }

            if (!$filhoSemGarantia && count($garantiasEncontradas) > 0) {
                for ($i = 0; $i < count($garantiasEncontradas); $i++) {
                    $produto->Filhos[$i]->Garantias = $garantiasEncontradas[$i];
                }

            }

        } else {
            $garantias = $garantiaDao->getGarantias($produto->produtoId, $siteId, $categorias, $precoPor);

            if (array($garantias) && count($garantias) > 0) {
                $arrayGarantias = $this->processaGarantia($garantias, $produtoInfo);
            }

            if (isset($arrayGarantias) && is_array($arrayGarantias) && count($arrayGarantias) > 0) {
                $objRetorno->Garantias = $arrayGarantias;
            }
        }

        return $objRetorno;
    }

    /**
     * Processa as garantias, agrupando e formatando os valores
     *
     * @param array $garantias
     * @param \stdClass $produtoInfo
     *
     * @return array
     */
    private function processaGarantia($garantias, $produtoInfo) {
        $arrayGarantias = array();
        foreach ($garantias as $garantia) {
            $prazo = $garantia->Prazo;
            if (!isset($arrayGarantias[$prazo])) {
                $arrayGarantias[$prazo] = new \stdClass();
            }
            $arrayGarantias[$prazo]->ProdutoId = $garantia->ProdutoId;
            $arrayGarantias[$prazo]->Codigo = $garantia->GarantiaCodigo;
            $arrayGarantias[$prazo]->Parcelamento = $produtoInfo->ParcelamentoMaximo;
            $arrayGarantias[$prazo]->Prazo += $garantia->Prazo;

            if (isset($arrayGarantias[$prazo]->ValorVenda)) {
                $valorVenda = $arrayGarantias[$prazo]->ValorVenda;
            } else {
                $valorVenda = 0;
            }

            $valorVenda += $garantia->ValorVenda;
            $arrayGarantias[$prazo]->ValorVenda = Utils::convertMoeda($valorVenda);

            if (isset($arrayGarantias[$prazo]->ValorParcelado)) {
                $valorParcelado = $arrayGarantias[$prazo]->ValorParcelado;
            } else {
                $valorParcelado = 0;
            }

            $valorParcelado += $garantia->ValorVenda / $produtoInfo->ParcelamentoMaximo;
            $arrayGarantias[$prazo]->ValorParcelado = Utils::convertMoeda($valorParcelado);
        }

        return $arrayGarantias;
    }

    /**
     * Busca os seguros disponíveis para  o produto especificado
     *
     * @param $produto
     * @param $siteId
     *
     * @return
     */
    private function getSeguros($produto, $siteId) {
        $objRetorno = clone $produto;
        $seguroDao = new SeguroDAO();
        $produtoDao = new ProdutoDAO();
        $produtoInfo = $produtoDao->getProdutoInfo($produto->produtoId, $siteId);
        $categorias = explode(',', $produtoInfo->Categorias);

        if (substr($produto->codigo, 0, 1) == 'C') {

            $filhoSemSeguro = false;
            $arraySeguros = array();

            foreach ($produto->Filhos as &$filho) {
                $precoPor = $filho->PrecoPor;
                $seguro = $seguroDao->getSeguroElegivel($precoPor, $categorias);

                if (is_object($seguro) && !$filhoSemSeguro) {
                    $arraySeguros[$filho->produtoId] = $seguro;
                } else {
                    $filhoSemSeguro = true;
                    continue;
                }
            }

            if (!$filhoSemSeguro && count($arraySeguros) == count($objRetorno->Filhos)) {
                foreach ($objRetorno->Filhos as &$filho) {
                    foreach ($arraySeguros as $produtoId => $seguro) {
                        if ($filho->produtoId == $produtoId) {
                            $seguroFinal = $this->mountSeguroArray($seguro, $produtoInfo);
                            $filho->seguro = $seguroFinal;
                        }
                    }
                }
            }
        } else {
            $precoPor = $produto->PrecoPor;
            $seguro = $seguroDao->getSeguroElegivel($precoPor, $categorias);

            if (is_object($seguro)) {
                $seguroFinal = $this->mountSeguroArray($seguro, $produtoInfo);
                $objRetorno->Seguro = $seguroFinal;
            }
        }

        return $objRetorno;
    }

    private function mountSeguroArray($seguro, $produtoInfo) {
        return array(
            'SeguroTabelaId' => $seguro->SeguroTabelaId,
            'ValorVenda' => Utils::convertMoeda($seguro->ValorVenda),
            'ValorParcelado' => Utils::convertMoeda($seguro->ValorVenda / $produtoInfo->ParcelamentoMaximo),
            'Parcelamento' => $produtoInfo->ParcelamentoMaximo,
            'Familia' => $seguro->Familia
        );
    }

    /**
     * @param array $produtosId
     * @param array $where
     * @param int $siteId
     * @param null $produtoPai
     * @param null $dadosPaginacao
     * @param $parceiroId
     *
     * @return \App\Model\Produto[]
     * @throws \Exception
     */
    private function findProdutoFull(array $produtosId, $where, $siteId = 1, $produtoPai = null, $dadosPaginacao = null, $parceiroId = null, array $produtoStatus = array()) {

        $produtoDao = new ProdutoDAO();

        if (count($produtosId) == 0 && !$dadosPaginacao) {
            return array();
        }

        if (!is_null($produtoPai)) {
            $produtos = $produtoDao->getProdutosFullCombo($siteId, $produtosId, $produtoPai, $dadosPaginacao, $produtoStatus);
        } else {
            $produtos = $produtoDao->getProdutosFull($siteId, $produtosId, $dadosPaginacao, $parceiroId, $produtoStatus);
        }

        $concatSeparator = "|";
        foreach ($produtos as &$produto) {

            $produto->descricao = htmlentities($produto->descricao);
            $produto->descricao = preg_replace('/[^(\x20-\x7F)]*/', '', $produto->descricao);
            $produto->descricao = html_entity_decode($produto->descricao);
            $produto->url = $this->criarUrlProduto(
                $produto->nome,
                $produto->LojaId,
                $produto->CategoriaId,
                $produto->SubCategoriaId,
                $produto->produtoId,
                $produto->UrlSite
            );

            unset($produto->UrlSite);

            /* Trata as caracteristicas */
            $stdCaracteristicas = new \stdClass();

            if (!empty($produto->Caracteristicas)) {
                $caracteristicas = explode($concatSeparator, $produto->Caracteristicas);
                if (count($caracteristicas) > 0) {
                    foreach ($caracteristicas as $k => $v) {
                        $stdCaracteristicas->$k = $v;
                    }
                }
            }

            $produto->Caracteristicas = $stdCaracteristicas;

            /* Trata os itens inclusos */
            $itensInclusos = explode($concatSeparator, $produto->ItensIncluso);
            $inclusos = array();
            foreach ($itensInclusos as $item) {
                if (!empty($item)) {
                    array_push($inclusos, $item);
                }
            }
            $produto->ItensIncluso = $inclusos;

            /* Trata as medias */
            $medias = explode($concatSeparator, $produto->Medias);
            $produto->Medias = new \stdClass();

            if (is_array($medias) && count($medias) > 0) {
                foreach ($medias as $media) {
                    if (empty($media)) {
                        continue;
                    }

                    /* Posicoes: [0] -> Src [1] -> Mime */
                    list($src, $mime) = explode("::", $media);

                    if (!is_array($produto->Medias->{$mime})) {
                        $produto->Medias->{$mime} = array();
                    }

                    if (in_array(trim($mime), array("gif", "jpeg", "jpg", "png"))) {
                        $produto->Medias->{$mime}[] = sprintf(
                            "http://parceiroimg.maquinadevendas.com.br/produto/%s",
                            $src
                        );
                    } else {
                        $produto->Medias->{$mime}[] = $src;
                    }
                }
            }

            if (is_null($produtoPai)) {
                $fatorCartao = $this->getFatoresCartao();
                $parcelas = array();

                for ($parcela = 1; $parcela <= 12; $parcela++) {
                    if ($parcela <= $produto->ParcelamentoMaximoSemJuros) {
                        $valorParcela = ($produto->PrecoPor / $parcela);
                    } else {
                        $valorParcela = ($produto->PrecoPor * $fatorCartao[$parcela]);
                    }

                    if ($valorParcela > 5) {
                        $parcelas[$parcela] = Utils::convertMoeda($valorParcela);
                    } else {
                        break;
                    }
                }

                $produto->parcelamento = $parcelas;
            }
        }

        return $produtos;
    }

    /**
     * Cria um array apenas com os valores das propriedades ID dos objetos no array recebido
     *
     * @param array $item array de objetos Model\Produtos
     *
     * @return array
     */
    private function mountArrayPorId($item) {
        $array = array();
        foreach ($item as $i) {
            $array[] = $i->produtoId;
        }

        return $array;
    }

    /**
     * Cria uma url de produto com base nos parâmetros recebidos
     *
     * @param $produtoNome
     * @param $lojaId
     * @param $categoriaId
     * @param $subCategoriaId
     * @param $produtoId
     * @param $baseURL
     *
     * @return string
     */
    private function criarUrlProduto($produtoNome, $lojaId, $categoriaId, $subCategoriaId, $produtoId, $baseURL) {
        $urlCompAux = array();

        if (!empty($lojaId)) {
            $urlCompAux[] = $lojaId;
        }

        if (!empty($categoriaId)) {
            $urlCompAux[] = $categoriaId;
        }

        if (!empty($subCategoriaId)) {
            $urlCompAux[] = $subCategoriaId;
        }

        $urlCompAux[] = $produtoId;
        $urlComp = implode('-', $urlCompAux);
        $string = Utils::limpaString($produtoNome);

        return $baseURL . '/Produto/' . $string . '/' . $urlComp;
    }

    /**
     * Busca do cache ou do banco os fatores dos cartões cadastrados
     *
     * @return float[]
     */
    public function getFatoresCartao() {
        $cartaoDAO = new CartaoDAO();
        $fatores = $cartaoDAO->getFatoresCartao();
        $fatorCartao = array();

        if (count($fatores) > 0) {
            foreach ($fatores as $fator) {
                $fatorCartao[$fator->Parcela] = $fator->Fator;
            }
        }

        return $fatorCartao;
    }

    /**
     * Retorna informações de estoque de forma mais eficiente que a anterior, tendo o cuidado de
     * nos casos de combo verificar o estoque com base em seus produtos filhos
     *
     * @param $parceiroId
     * @param array|null $produtoCodigo
     * @param bool $parceiroTemCampanha Variavel de referência
     *
     * @return array
     * @throws \App\Exceptions\BusinessException
     * @throws \App\Exceptions\ValidacaoException
     */
    public function getEstoquePlus($parceiroId, $produtoCodigo = null, &$parceiroTemCampanha=false) {
        $regras = array(
            'parceiroId' => array(
                'required',
                'notempty',
                'int'
            ),
            'produtoCodigo' => array(
                'required',
                'notempty',
                'array' => array('min' => 1, 'max' => self::QTD_MAX_CONSULTA_ESTOQUE)
            )
        );

        $obj = new \stdClass();
        $obj->parceiroId = $parceiroId;
        $obj->produtoCodigo = $produtoCodigo;

        $validacao = new Validacao($regras);

        if (!$validacao->executaValidacao($obj)) {
            throw new ValidacaoException($validacao->msg, 2);
        }

        $parceiroSiteDAO = new ParceiroSiteDAO();
        $parceiroSite = $parceiroSiteDAO->getParceiroSiteById($parceiroId);
        if (!$parceiroSite instanceof ParceiroSite) {
            throw new BusinessException("ParceiroId [$parceiroId] inválido.", 4);
        }

        $siteId = $parceiroSite->siteBase;
        $produtoEstoqueDAO = new ProdutoEstoqueDAO();
        $produtos = $produtoEstoqueDAO->getEstoquePlus($siteId, $produtoCodigo, self::QTD_MAX_CONSULTA_ESTOQUE, $parceiroId);
        $parceiroSitePermissaoBO = new ParceiroSitePermissaoBO();

        if ($parceiroSitePermissaoBO->parceiroPermissaoTudo($parceiroId, $siteId, ParceiroSitePermissao::CAMPANHA) && is_array($produtos)) {
            $parceiroTemCampanha = true;
            $produtoCodigo = array();
            foreach ($produtos as $pr) {
                $produtoCodigo[] = $pr->produtoId;
            }

            // Lista das subcategorias e categorias do catálogo
            $catalogoCategoriaBO = new CatalogoCategoriaBO();
            $catalogoCategorias = $catalogoCategoriaBO->getCatalogoCategoriaByParceiro($parceiroId);

            // Recupera o valor de desconto da campanha
            $descontoCampanha = end($catalogoCategorias);
            $descontoCampanhaValor = $descontoCampanha->descontoCampanha;

            $objCatalogoProdutoDAO = new CatalogoProdutoDAO();
            $produtosDesconto = $objCatalogoProdutoDAO->getProdutoCatalogoComDesconto($descontoCampanha->catalogoId, $produtoCodigo, $parceiroId, $siteId);

            foreach ($produtos as &$produto) {
                $this->aplicarDescontoCategoriaCampanha($produto, $catalogoCategorias, $produtosDesconto, $descontoCampanhaValor);
                unset($produto->CategoriaId, $produto->Categoria, $produto->SubCategoria, $produto->SubCategoriaId, $produto->ProdutoId);
            }
            unset($produto);
        }

        foreach ($produtos as $produto) {
            if ($produto->PrecoPor <= 0) {
                $this->zeraEstoqueRetorno($parceiroId, $produto);
            }
        }

        return $produtos;
    }

    /**
     * Retorna informações de estoque de acordo com o codigo do retail
     *
     * @param array $codigoRetail
     *
     * @return array
     * @throws ValidacaoException
     * @throws \Exception
     */
    public function getEstoqueRetail($codigoRetail) {

        $regras = array('codigoRetail' => array('int_array'));

        $obj = (object)array('codigoRetail' => $codigoRetail);

        $validador = new Validacao($regras);

        if (!$validador->executaValidacao($obj)) {
            throw new ValidacaoException($validador->msg);
        }

        $produtoEstoqueDAO = new ProdutoEstoqueDAO();

        return $produtoEstoqueDAO->getEstoqueRetail($codigoRetail);
    }

    /**
     * Aplica desconto no valor do produto de acordo com subcategoria, categoria e campanha respectivamente
     *
     * @param object $objProduto
     * @param array $catalogoCategorias
     * @param array $produtosDesconto
     * @param float $descontoCampanhaValor
     */
    private function aplicarDescontoCategoriaCampanha(&$objProduto, array $catalogoCategorias, array $produtosDesconto, $descontoCampanhaValor) {
        if (!is_object($objProduto) || !property_exists($objProduto, 'PrecoPor')) {
            throw new BusinessException("Erro ao calcular desconto. Objeto Inválido");
        }

        $descontoProduto = (float)$produtosDesconto[$objProduto->produtoId]->desconto;
        if ($descontoProduto > 0) {
            $objProduto->PrecoPor = $objProduto->PrecoPor - ($objProduto->PrecoPor * $descontoProduto / 100);
        } elseif (!empty($catalogoCategorias[$objProduto->SubCategoriaId])) {
            $objProduto->PrecoPor = $objProduto->PrecoPor - ($objProduto->PrecoPor * $catalogoCategorias[$objProduto->SubCategoriaId]->valorDesconto / 100);
        } elseif (!empty($catalogoCategorias[$objProduto->CategoriaId])) {
            $objProduto->PrecoPor = $objProduto->PrecoPor - ($objProduto->PrecoPor * $catalogoCategorias[$objProduto->CategoriaId]->valorDesconto / 100);
        } elseif ($descontoCampanhaValor > 0) {
            $objProduto->PrecoPor = $objProduto->PrecoPor - ($objProduto->PrecoPor * $descontoCampanhaValor / 100);
        }

        $objProduto->PrecoPor = round($objProduto->PrecoPor, 2);
    }

    /**
     * @param $parceiroId
     * @param $codigos
     *
     * @return \App\Model\EstoqueParceiro[]
     * @throws \App\Exceptions\BusinessException
     * @throws \Exception
     */
    public function getProdutoEstoqueParceiro($parceiroId, $codigos, $pagina) {
        try {
            $parceiroSiteBO = new ParceiroSiteBO();
            $parceiro = $parceiroSiteBO->getParceiroSiteById($parceiroId);

            $estoques = array();
            $paginaAtual = $pagina;
            $resultado = $this->getProdutoFull($parceiro->siteBase, $codigos, $paginaAtual, $parceiro->parceiroId);
            $dadosPaginacao = $resultado['dadosPaginacao'];
            $produtos = $resultado['produtos'];

            if (!empty($resultado)) {
                /** @var \App\Model\Produto $produto */
                foreach ($produtos as $produto) {
                    $estoqueParceiro = new EstoqueParceiro();
                    $estoqueParceiro->codigo = $produto->codigo;
                    $estoqueParceiro->parceiroId = $parceiro->parceiroId;
                    $estoqueParceiro->quantidadeMV = $produto->Disponivel;
                    $estoqueParceiro->produtoId = $produto->produtoId;
                    $estoqueParceiro->nome = $produto->nome;

                    $estoques[$produto->codigo] = $estoqueParceiro;
                }
            }

            return array('estoques' => $estoques, 'paginacao' => $dadosPaginacao,);
        } catch (BusinessException $be) {
            Logger::getInstance()->exception($be);
            throw $be;
        } catch (DatabaseException $dbe) {
            Logger::getInstance()->exception($dbe);
            throw $dbe;
        }
    }

    /**
     * Busca os SKUs dos parceiros no Sistema da MV (quando existentes)
     *
     * @param $parceiroId
     * @param $codigos
     *
     * @return \App\Model\SkuParceiro[]
     *
     * @throws ValidacaoException
     */
    public function buscaSkuParceiro($parceiroId, $codigos) {
        $codigos = is_array($codigos) ? $codigos : array($codigos);
        $regras = array(
            "parceiroId" => array("required", "int"),
            "codigos" => array("array"),
        );
        $obj = (object)array("parceiroId" => $parceiroId, "codigos" => $codigos);
        $validacao = new Validacao($regras);
        if (!$validacao->executaValidacao($obj)) {
            throw new ValidacaoException($validacao->msg);
        }
        $skuParceiroDAO = new SkuParceiroDAO();

        return $skuParceiroDAO->buscarSkuParceiro($parceiroId, $codigos);
    }

    public function buscarProdutosPorCodigo($parceiroId, $codigos) {
        $configBO = new ParceiroConfiguracaoBO();
        $cnfEstratBusca = $configBO->getConfiguracoes($parceiroId, Configuracao::ESTRATEGIA_BUSCA_PRODUTOS_API, true);

        $buscaProdutoStrategy = BuscarProdutoStrategyFactory::create($cnfEstratBusca->valor);
        return $buscaProdutoStrategy->buscarPorCodigo($parceiroId, $codigos);
    }

    public function buscarProdutosPorCodigoRetail($parceiroId, $codigosRetail) {
        $configBO = new ParceiroConfiguracaoBO();
        $cnfEstratBusca = $configBO->getConfiguracoes($parceiroId, Configuracao::ESTRATEGIA_BUSCA_PRODUTOS_API, true);

        $buscaProdutoStrategy = BuscarProdutoStrategyFactory::create($cnfEstratBusca->valor);
        return $buscaProdutoStrategy->buscarPorCodigoRetail($parceiroId, $codigosRetail);
    }

    public function buscarProdutosPorPagina($parceiroId, $pagina = 1, &$dadosPaginacao = null, array $produtoStatus = array()) {
        $configBO = new ParceiroConfiguracaoBO();
        $cnfEstratBusca = $configBO->getConfiguracoes($parceiroId, Configuracao::ESTRATEGIA_BUSCA_PRODUTOS_API, true);

        $buscaProdutoStrategy = BuscarProdutoStrategyFactory::create($cnfEstratBusca->valor);
        $produtos = $buscaProdutoStrategy->buscarPaginado($parceiroId, $pagina, $produtoStatus);
        $dadosPaginacao = $buscaProdutoStrategy->buscarDadosPaginacao($parceiroId, $produtoStatus);
        return $produtos;
    }

    public function buscarParcelamentoProduto($parceiroId, $codigo, $valor = null) {
        $regras = array(
            'parceiroId' => array('int'),
            'codigo' => array('str'),
        );
        $obj = (object)array('parceiroId' => $parceiroId, 'codigo' => $codigo);
        $validacao = new Validacao($regras);
        if (!$validacao->executaValidacao($obj)) {
            throw new ValidacaoException($validacao->msg, Response::ERRO_VALIDACAO);
        }


        $parceiroSiteBO = new ParceiroSiteBO();
        $parceiro = $parceiroSiteBO->getParceiroSiteById($parceiroId);

        $produtos = $this->getProdutoFull($parceiro->siteBase, array($codigo), null, $parceiro->parceiroId, false);
        $produto = $produtos['produtos'][0];
        $preco = null !== $valor ? $valor : (float)$produto->PrecoPor;
        $maxParcelamento = (int)$produto->ParcelamentoMaximoSemJuros;

        $parcelamentos = array();
        for ($numParcela = 1; $numParcela <= $maxParcelamento; $numParcela++) {
            $parcelamento = new Parcelamento();
            $parcelamento->contemJuros = false;
            $parcelamento->parcelas = $numParcela;
            $parcelamento->taxaJuros = 0;
            $parcelamento->valorParcelamento = Utils::convertMoeda($preco / $numParcela);
            $parcelamento->total = $parcelamento->valorParcelamento * $parcelamento->parcelas;

            $parcelamentos[] = $parcelamento;
        }

        $fatoresCartao = $this->getFatoresCartao();
        for ($numParcela = $maxParcelamento + 1; $numParcela <= 12; $numParcela++) {
            $parcelamento = new Parcelamento();
            $parcelamento->contemJuros = true;
            $parcelamento->parcelas = $numParcela;
            $parcelamento->taxaJuros = $fatoresCartao[$numParcela] * $numParcela;
            $parcelamento->valorParcelamento = Utils::convertMoeda($preco * $fatoresCartao[$numParcela]);
            $parcelamento->total = $parcelamento->valorParcelamento * $parcelamento->parcelas;

            $parcelamentos[] = $parcelamento;
        }

        return $parcelamentos;
    }
}
