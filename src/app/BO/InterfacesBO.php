<?php

namespace App\BO;

use App\DAO\InterfacesDAO;
use App\Helper\Validacao;

/**
 * Class InterfacesBO
 * @package App\BO
 */
class InterfacesBO extends BaseBO {

    /**
     * Retorna dados como código do produto e quantidade no estoque
     * @param $parceiroId int
     * @param $siteId int
     * @param $interface int
     * @return array
     * @throws \Exception
     */
    public function getDados($parceiroId, $siteId, $interface) {
        // Regras de validação dos parametros
        $condicoes = array(
            "parceiroId" => array("notempty", "int", "required"),
            "siteId" => array("notempty", "int", "required"),
            "interface" => array("notempty", "int", "required")
        );

        // Parametros enviados no post
        $validacao = new Validacao($condicoes);

        $dados = new \stdClass();
        $dados->parceiroId = $parceiroId;
        $dados->siteId = $siteId;
        $dados->interface = $interface;

        //Executa a validação e em caso de erro, joga exception
        if (!$validacao->executaValidacao($dados)) {
            throw new \Exception($validacao->msg);
        }

        $interfacesDAO = new InterfacesDAO();
        return $interfacesDAO->checkDadosInterface($parceiroId, $siteId, $interface);
    }
}
