<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 29-02-2016
 * Time: 16:08
 */

namespace App\BO;

use App\DAO\TokenDAO;
use App\DAO\TokenUsuarioDAO;
use App\Exceptions\ValidacaoException;
use App\Helper\Validacao;
use App\Model\Response;
use App\Model\Token;
use App\Model\V2\TokenUsuario;
use App\Model\V2\Usuario;

/**
 * Class TokenBO
 *
 * @package App\BO
 */
class TokenBO extends BaseBO {
    /**
     * Busca token no banco
     *
     * @param $token
     *
     * @return Token
     * @throws ValidacaoException
     */
    public function getToken($token) {
        $tokenDao = new TokenDAO();

        return $tokenDao->getToken($token);
    }

    /**
     * Busca token por escopo
     *
     * @param Token $token
     * @param null  $escopo
     *
     * @return \App\Model\TokenEscopo|\App\Model\TokenEscopo[]
     */
    public function getTokenEscopo(Token $token, $escopo = null) {
        $tokenDao = new TokenDAO();

        return $tokenDao->getTokenEscopo($token, $escopo);
    }

    public function getTokenUsuario($token) {
        $tokenUsuarioDAO = new TokenUsuarioDAO();

        return $tokenUsuarioDAO->buscarTokenUsuario($token);
    }

    /**
     * @param \App\Model\V2\Usuario $usuario
     * @param int                   $validadeEmSegundos
     *
     * @return \App\Model\V2\TokenUsuario
     */
    public function createTokenUsuario(Usuario $usuario, $validadeEmSegundos = 3600) {
        $tokenUsuarioDAO = new TokenUsuarioDAO();

        return $tokenUsuarioDAO->criarTokenUsuario($usuario, $validadeEmSegundos);
    }

    /**
     * Invalida um token de usuário.
     *
     * @param string $token
     *
     * @return bool
     * @throws \App\Exceptions\ValidacaoException
     */
    public function invalidarAccessToken($token) {
        $regras = array('token' => array('required', 'notemptynorwhitespaces', 'str'));
        $obj = (object)array('token' => $token);
        $validacao = new Validacao($regras);
        if (!$validacao->executaValidacao($obj)) {
            throw new ValidacaoException($validacao->msg, Response::ERRO_VALIDACAO);
        }
        $tokenUsuario = $this->getTokenUsuario($token);
        if (null !== $tokenUsuario) {
            $tokenUsuarioDAO = new TokenUsuarioDAO();
            return $tokenUsuarioDAO->removerToken($tokenUsuario->token);
        }
        return false;
    }
}