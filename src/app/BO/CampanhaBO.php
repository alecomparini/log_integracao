<?php
namespace App\BO;

use App\Helper\Validacao;
use App\DAO\CampanhaDAO;
use App\Exceptions\ValidacaoException;
use App\DAO\CatalogoDAO;
use App\DAO\CatalogoCategoriaDAO;
use App\Model\ParceiroSitePermissao;
use App\Exceptions\BusinessException;
use App\DAO\ParceiroSiteDAO;
use App\Model\Response;

/**
 * Description of CampanhaBO
 *
 * @author Ítallo Costa <itallocosta@softbox.com.br>
 */
class CampanhaBO extends BaseBO {
    
    public function getCampanhaByProduto($campanhaId, $produtoId, $siteBase) {
       $obj = (object)array(
           'produtoId' => $produtoId,
           'campanhaId' => $campanhaId,
           'siteBase'   => $siteBase
        );
        $regras = array(
            'produtoId' => array('required','int'),
            'campanhaId' => array('required','int'),
            'siteBase' => array('required','int')
        );
        $validador = new Validacao($regras);   
        if (!$validador->executaValidacao($obj)) {
            throw  new ValidacaoException($validador->msg, 2);
        }
        
        $campanhaDao = new CampanhaDAO();
        return $campanhaDao->getCampanhaByProduto($campanhaId, $siteBase, $produtoId);

    }
    
    public function getCampanhaByParceiro($parceiroId) {
        $obj = (object)array(
           'parceiroId' => $parceiroId
        );
        $regras = array(
            'parceiroId' => array('required', 'int')
        );
        $validador = new Validacao($regras);   
        if (!$validador->executaValidacao($obj)) {
            throw new ValidacaoException($validador->msg, 2);
        }
        
        $this->checarPermissaoCampanha($parceiroId);

        $catalogoCategoria = new CatalogoCategoriaBO();
        $catalogo = $catalogoCategoria->getCatalogoByParceiro($parceiroId);
        
        if (!$catalogo->catalogoId) {
            $objResponse = new Response('', Response::CAMPANHA_SEM_CATALOGO);
            throw new BusinessException($objResponse->getMessage(Response::CAMPANHA_SEM_CATALOGO), Response::CAMPANHA_SEM_CATALOGO);
        }
        
        if ($catalogo->catalogoId) {
            $categorias = $catalogoCategoria->filtrarCategoriaByCatalogo($parceiroId, $catalogo->catalogoId);
        } else {
            $categorias = array();
        }

        if (!empty($catalogo->localidades)) {
            $localidades = explode(",", $catalogo->localidades);
            $localidadeBO = new LocalidadeBO();
            $catalogo->localidades = $localidadeBO->getLocalidadeBySigla($localidades);
        }
        
        $parceiroSiteBO = new ParceiroSiteBO();
        $catalogo->produtos = $parceiroSiteBO->getProdutosParceiroByCampanha($parceiroId);
        $catalogo->catalogo = $catalogo->nome;
        $catalogo->statusCatalogo = $catalogo->status;
        return $this->formatarDadosExportacao($catalogo, $categorias);
    }
    /**
     * Formata os dados para envio da resposta
     * @param int $catalogo
     * @param array $categorias
     * @return \stdClass
     */
    private function formatarDadosExportacao($catalogo, $categorias) {
        $objRetorno = new \stdClass();
        $objRetorno->campanhaId = $catalogo->campanhaId;
        $objRetorno->campanha = $catalogo->campanha;
        $objRetorno->tipoFrete = $catalogo->tipoFrete;
        $objRetorno->desconto = $catalogo->desconto;
        $objRetorno->fretePonderado = $catalogo->fretePonderado;
        $objRetorno->statusCampanha = $catalogo->statusCampanha;
        $objRetorno->localidades = $catalogo->localidades;
        $objRetorno->catalogo = new \stdClass();
        $objRetorno->catalogo->catalogoId = $catalogo->catalogoId;
        $objRetorno->catalogo->catalogo = $catalogo->catalogo;
        $objRetorno->catalogo->statusCatalogo = $catalogo->statusCatalogo;
        $objRetorno->catalogo->categorias = $categorias;
        $objRetorno->catalogo->produtos = $catalogo->produtos;
        
        return $objRetorno;
    }
    
    private function checarPermissaoCampanha($parceiroId) {
        $objParceiro = new ParceiroSiteDAO();
        $parceiro = $objParceiro->getParceiroSiteById($parceiroId);
        
        $objParceiroPermissaoBO = new ParceiroSitePermissaoBO();
        $permissao = $objParceiroPermissaoBO->parceiroPermissaoTudo($parceiroId, $parceiro->siteBase, ParceiroSitePermissao::CAMPANHA);
        if (!$permissao) {
            throw new BusinessException("Parceiro sem permissão de campanha");
        }
    }
    
   
}
