<?php
namespace App\BO;

use App\Adapter\EstoqueParceiro\EstoqueParceiroAdapterFactory;
use App\Model\Configuracao;

class RelatorioBO extends BaseBO {
    public function gerarRelatorioEstoque($parceiroId, $codigos = array(), $pagina = 1) {
        $configBO = new ParceiroConfiguracaoBO();
        $configEstrategia = $configBO->getConfiguracoes($parceiroId, Configuracao::ESTRATEGIA_RELATORIO_ESTOQUE, true);

        $adapter = EstoqueParceiroAdapterFactory::create($configEstrategia->valor);

        return $adapter->getEstoques($parceiroId, $codigos, $pagina);
    }
}