<?php
namespace App\BO;

use App\Core\Logger;
use App\DAO\AgendamentoTarefaDAO;
use App\Exceptions\ValidacaoException;
use App\Helper\Validacao;
use App\Model\Response;
use App\Model\V2\AgendamentoTarefa;
use Cron\CronExpression;

/**
 * Class TarefaBO
 *
 * @package App\BO
 */
class TarefaBO extends BaseBO {
    /**
     * @return \App\Model\V2\AgendamentoTarefa[]
     */
    public function buscarTarefasAtivas() {
        $agendamentoTarefaDAO = new AgendamentoTarefaDAO();
        return $agendamentoTarefaDAO->getAtivos();
    }

    /**
     * Busca as tarefas a ser executadas no momento atual
     *
     * @return \App\Model\V2\AgendamentoTarefa[]
     */
    public function buscarTarefasExecutar() {
        $agendamentos = $this->buscarTarefasAtivas();
        /** @var \App\Model\V2\AgendamentoTarefa[] $agendamentosExecutar */
        $agendamentosExecutar = array();
     
        foreach ($agendamentos as $agendamento) {
            if (!CronExpression::isValidExpression($agendamento->configuracaoTempo)) {
                Logger::getInstance()->error("Agendamento contém uma expressão inválida.", array("agendamento" => $agendamento));
                continue;
            }
            $cron = CronExpression::factory($agendamento->configuracaoTempo);
            if ($cron->isDue()) {
                $agendamentosExecutar[] = $agendamento;
            }
        }

        return $agendamentosExecutar;
    }

    /**
     * Busca uma tarefa e os dados de seu agendamento de acordo com o ID do AGENDAMENTO informado
     *
     * @param $id
     *
     * @return \App\Model\V2\AgendamentoTarefa
     * @throws \App\Exceptions\ValidacaoException
     */
    public function buscarTarefaPorAgendamentoId($id) {
        $regras = array('id' => array('int', 'notempty'));
        $obj = array('id' => $id);
        $validacao = new Validacao($regras);
        if (!$validacao->executaValidacao($obj)) {
            throw new ValidacaoException($validacao->msg, Response::ERRO_VALIDACAO);
        }

        $agendamentoTarefaDAO = new AgendamentoTarefaDAO();

        return $agendamentoTarefaDAO->buscarPorIdAgendamento($id);
    }

    /**
     * Salva o log da execução da chamada do agendamento
     *
     * @param $id
     * @param $httpCode
     * @param $dataInicioExecucao
     * @param $body
     * @param $headers
     *
     * @return int
     * @throws \App\Exceptions\ValidacaoException
     */
    public function salvarLogExecucao($id, $httpCode, $dataInicioExecucao, $dataTerminoExecucao, $body, $headers) {
        $regras = array(
            'id' => array('int', 'notempty'),
            'httpCode' => array('int', 'notempty'),
            'body' => array('str'),
            'headers' => array('str'),
        );
        $obj = (object)array(
            'id' => $id,
            'httpCode' => $httpCode,
            'body' => $body,
            'headers' => $headers,
        );
        $validacao = new Validacao($regras);
        if (!$validacao->executaValidacao($obj)) {
            throw new ValidacaoException($validacao->msg, Response::ERRO_VALIDACAO);
        }

        $agendamentoTarefaDAO = new AgendamentoTarefaDAO();

        return $agendamentoTarefaDAO->salvarLogExecucao($id, $httpCode, $dataInicioExecucao, $dataTerminoExecucao, $body, $headers);
    }

    /**
     * Atualiza a data da última execução tanto da tarefa quanto do agendamento
     *
     * @param \App\Model\V2\AgendamentoTarefa $agendamentoTarefa
     *
     * @return bool
     *
     * @throws \App\Exceptions\DatabaseException
     * @throws \App\Exceptions\ValidacaoException
     */
    public function atualizarDataUltimaExecucao(AgendamentoTarefa $agendamentoTarefa) {
        $regras = array('agendamentoTarefaId' => array('int'), 'tarefaId' => array('int'));
        $validacao = new Validacao($regras);
        if (!$validacao->executaValidacao($agendamentoTarefa)) {
            throw new ValidacaoException($validacao->msg, Response::ERRO_VALIDACAO);
        }

        $agendamentoTarefaDAO = new AgendamentoTarefaDAO();

        return $agendamentoTarefaDAO->atualizarDataUltimaExecucao($agendamentoTarefa);
    }
}