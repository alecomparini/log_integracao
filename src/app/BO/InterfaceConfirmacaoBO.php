<?php

namespace App\BO;

use App\DAO\InterfaceConfirmacaoDAO;
use App\Helper\Validacao;

/**
 * Class InterfaceConfirmacaoBO
 * @package App\BO
 */
class InterfaceConfirmacaoBO extends BaseBO {

    private $interfaceConfirmacaoDAO;

    /**
     * Instancia um objeto DAO: InterfaceConfirmacaoDAO
     * InterfaceConfirmacaoBO constructor.
     */
    public function __construct() {
        $this->interfaceConfirmacaoDAO = new InterfaceConfirmacaoDAO();
    }

    /**
     * Verifica se houve a confirmação de interface
     * @param $parceiroId int
     * @param $siteId int
     * @param $interfaceTipo int
     * @return array
     * @throws \Exception
     */
    public function getInterfaceConfirmacao($parceiroId, $siteId, $interfaceTipo) {
        $obj = (object) array(
            "parceiroId" => $parceiroId,
            "siteId" => $siteId,
            "interfaceTipo" => $interfaceTipo
        );

        $regras = array(
            "parceiroId" => array("int", "notempty", "required"),
            "siteId" => array("int", "notempty", "required"),
            "interfaceTipo" => array("int", "notempty", "required")
        );

        $validacao = new Validacao($regras);
        if (!$validacao->executaValidacao($obj)) {
            throw new \Exception($validacao->msg);
        }

        return $this->interfaceConfirmacaoDAO->getInterfaceConfirmacao($parceiroId, $siteId, $interfaceTipo);
    }

    /**
     * Altera o campo enviada da interfaceConfirmação
     * @param array $interfaces
     * @param $tipo
     * @return bool|int
     * @throws \Exception
     */
    public function updateInterfaceConfirmacao(array $interfaces, $tipo) {

        $regras = array(
            'interfaces' => array('required', 'notempty', 'int_array'),
            'tipo' => array('required', 'notempty', 'int')
        );

        $obj = (object) array(
            'interfaces' => $interfaces,
            'tipo' => $tipo
        );

        $validacao = new Validacao($regras);
        if (!$validacao->executaValidacao($obj)) {
            throw new \Exception($validacao->msg);
        }

        return $this->interfaceConfirmacaoDAO->updateInterfaceConfirmacao($interfaces, $tipo);
    }

}