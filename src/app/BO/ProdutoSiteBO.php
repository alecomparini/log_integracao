<?php
namespace App\BO;

use App\Core\Config;
use App\DAO\FreteDescontoLocalidadeDAO;
use App\DAO\ProdutoSiteDAO;
use App\Exceptions\BusinessException;
use App\Exceptions\ValidacaoException;
use App\Helper\Utils;
use App\Helper\Validacao;
use App\Model\Frete;
use App\Model\FreteDescontoLocalidade;
use App\Model\ProdutoSiteOut;

/**
 * Classe resonsável por gerenciar todas as Regras
 * de negócio do objeto ProdutoSite
 */
class ProdutoSiteBO extends BaseBO {
    /**
     * getFrete
     * Retorna as informacoes de frete
     * @param $parceiroId
     * @param $cep
     * @param $produtos
     * @param $dataAgendamento 
     * @param $turnoAgendamento
     * @return Frete
     * @throws \Exception
     */
    public function getFrete($parceiroId, $cep, $produtos, $dataAgendamento = null, $turnoAgendamento = null) {
        // Regras de validação dos parametros
        $condicoes = array(
            "parceiroId" => array("notempty", "int", "required"),
            "cep" => array("notempty", "required")
        );

        // Parametros enviados no post
        $validacao = new Validacao($condicoes);

        $produtoSiteOut = new ProdutoSiteOut();
        
        $dataAgendada = !empty($dataAgendamento) ? (substr($dataAgendamento, 0, 2) . "/".substr($dataAgendamento, 2, 2) . "/".substr($dataAgendamento, 4, 4)) : null;;

        $dados = new \stdClass();
        $dados->parceiroId      = $parceiroId;
        $dados->cep             = $cep;
        $dados->dataAgendamento = $dataAgendada;
        $dados->turno           = $turnoAgendamento;

        //Executa a validação e em caso de erro, joga exception
        if (!$validacao->executaValidacao($dados)) {
            throw new ValidacaoException($validacao->msg, 2);
        }

        //Ativa o buffer de saída para dados do frete
        ob_start();

        //Obtém os parâmetros da query
        $fields['Produtos'] = json_encode($produtos);
        $fields['Cep'] = $cep;
        $fields['DataAgendamento'] = $dataAgendada;
        $fields['turno'] = $turnoAgendamento;
        
        
        //Constrói a query
        $fieldsString = http_build_query($fields);

        //Realiza requisição no site da RE
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, Config::get('frete.url'));
        curl_setopt($ch, CURLOPT_POST, count($fields));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $fieldsString);
        curl_setopt($ch, CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.13) Gecko/20080311 Firefox/2.0.0.13');
        $result = curl_exec($ch);
        curl_close($ch);

        //Limpa e desativa o buffer de saída
        ob_end_clean();
        $obj = json_decode($result);
        //Calcula os preços por dos produtos e valor da promoção
        if (count($produtos) > 0) {
            $valorTotal = 0;
            $codigos = array();
            $produtoQuantidade = array();

            foreach ($produtos as $produto) {
                $codigos[] = $produto->Codigo;
                $produtoQuantidade[$produto->Codigo] = $produto->Quantidade;
            }

            $produtoSiteDAO = new ProdutoSiteDAO();

            //Obtém os códigos e os preços por dos produtos
            $produtos = $produtoSiteDAO->getProdutoSite($parceiroId, $codigos);

            if (count($produtos) > 0) {
                //Soma valor total de acordo com a quantidade e precoPor dos produtos
                foreach ($produtos as $produto) {
                    $valorTotal += $produto->precoPor * $produtoQuantidade[$produto->codigo];
                }
            }

            if ($valorTotal >= 79.9) {
                $obj->valorPromocao = 1.99;
            }
        }

        //Se a requisição no site da RE obeteve sucesso, prepare os retorna os dados com sucesso
        //Senão, lança um erro
        if ($obj->success) {

            $freteDescontoDAO = new FreteDescontoLocalidadeDAO();
            $freteDesconto = $freteDescontoDAO->getDescontoFrete($parceiroId, preg_replace('/[^0-9]/','',$cep));
            
            if ($freteDesconto instanceof FreteDescontoLocalidade) {
                switch ($freteDesconto->tipoFrete) {
                    case 1:
                        $obj->valor_promocao = 0;
                        break;
                    case 2:
                        $obj->valor_promocao = ($freteDesconto->freteDescontoLocalidadeId) ? $freteDesconto->valorFrete : $obj->valor_promocao;
                        break;
                    case 4:
                        $obj->valor_promocao = number_format(($obj->valor_promocao - ($obj->valor_promocao * $freteDesconto->desconto / 100)),2);
                        break;
                }
            }

            //Solicitado pelo Vaz em carater de urgencia
            $obj->valor_transportadora = $obj->valor_transportadora * Config::get('frete.fator_calculo');

            //Cria um frete e o retorna
            $frete = new Frete();
            $frete->prazoEntrega = $obj->prazo;
            $frete->valorPromocional = Utils::convertMoeda($obj->valor_promocao);
            $frete->valorTransportadora = Utils::convertMoeda($obj->valor_transportadora);
            return $frete;
        } else {
            //Lança erro
            $msg = $produtoSiteOut->getMsg(6);
            if ($validacao->msg) {
                $msg .= ' - ' . $validacao->msg;
            }
            throw new BusinessException($msg, $produtoSiteOut->codigo);
        }
    }
}
