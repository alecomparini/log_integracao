<?php
namespace App\BO;

use App\DAO\SkuParceiroDAO;

class SkuParceiroBO extends BaseBO {
    /**
     * @var \App\DAO\SkuParceiroDAO
     */
    private $skuParceiroDAO;

    /**
     * SkuParceiroBO constructor.
     */
    public function __construct() {
        $this->skuParceiroDAO = new SkuParceiroDAO();
    }

    /**
     * Busca as equivalencias de SKU no sistema dos parceiros
     *
     * @param           $parceiroId
     * @param string[]  $codigos
     *
     * @return \App\Model\SkuParceiro[]
     */
    public function buscarSkuParceiro($parceiroId, $codigos) {
        $codigos = is_array($codigos) ? $codigos : array($codigos);
        return $this->skuParceiroDAO->buscarSkuParceiro($parceiroId, $codigos);
    }

    public function inserirSkuParceiro($parceiroId, $codigo, $skuParceiro) {
        return $this->skuParceiroDAO->inserirSkuParceiro($parceiroId, $codigo, $skuParceiro);
    }
}