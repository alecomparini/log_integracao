<?php
namespace App\BO;

use App\Helper\Validacao;
use App\Exceptions\ValidacaoException;
use App\DAO\ParceiroMeioPagamentoDAO;

/**
 * Classe responsável por executar as regras de negócio referentes aos Meios de Pagamento
 *
 * @author Ítallo Costa <itallocosta@softbox.com.br>
 */
class MeioPagamentoBO extends BaseBO {

    /**
     * Método responsável por validar se o meio de pagamento existe para o parceiro informado
     * @param type $parceiroId
     * @param type $meioPagamentoId
     * @return boolean
     * @throws ValidacaoException
     */
    public function validarMeioPagamentoParceiro($parceiroId, $meioPagamentoId) {
        $regras = array(
            'meioPagamentoId' => array('required','int'),
            'parceiroId'      => array('required', 'int')
        );
        
        $params = array(
            "meioPagamentoId" => $meioPagamentoId,
            "parceiroId"      => $parceiroId
        );
        
        $validacao = new Validacao($regras);
        if (!$validacao->executaValidacao($params)) {
            throw new ValidacaoException($validacao->msg, 2);
        }
        
        $meioPagamento  = new ParceiroMeioPagamentoDAO();
        $meiosPagamento = $meioPagamento->getMeiosPagamentoByParceiro($parceiroId, $meioPagamentoId);
        $return = false;

        if (!empty($meiosPagamento)) {
            $return = true;
        }
        return $return;
    }
}
