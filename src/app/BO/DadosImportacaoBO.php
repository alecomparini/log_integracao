<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 05/01/2016
 * Time: 17:36
 */

namespace App\BO;


use App\DAO\DadosImportacaoDAO;
use App\Helper\Validacao;

/**
 * Class DadosImportacaoBO
 * @package App\BO
 */
class DadosImportacaoBO {

    /**
     * Verifica se já foi registrado o pedido do parceiro
     *
     * @param string $parceiroPedidoId
     * @param int $parceiroId
     * @return bool
     * @throws \Exception
     */
    public function isImportado($parceiroPedidoId, $parceiroId) {
        $obj = (object) array(
            'parceiroPedidoId' => $parceiroPedidoId,
            'parceiroId' => $parceiroId
        );
        $regras = array(
            'parceiroPedidoId' => array('notempty', 'required'),
            'parceiroId' => array('int', 'notempty', 'required')
        );

        $validador = new Validacao($regras);
        if (!$validador->executaValidacao($obj)) {
            throw new \Exception($validador->msg);
        }

        $parceiroBO = new ParceiroSiteBO();
        $parceiro = $parceiroBO->getParceiroSiteById($parceiroId);

        $dadosImportacaoDAO = new DadosImportacaoDAO();
        return $dadosImportacaoDAO->isImportado($parceiroPedidoId, $parceiro->parceiroSiteId);
    }
}