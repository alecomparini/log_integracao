<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 31/12/2015
 * Time: 10:46
 */

namespace App\BO;

use App\DAO\ParcelamentoIntervaloDAO;
use App\Exceptions\ValidacaoException;
use App\Helper\Validacao;


/**
 * Class ParcelamentoBO
 * @package App\BO
 */
class ParcelamentoBO {

    /** @var ParcelamentoIntervaloDAO */
    protected $parcelamentoIntervaloDAO;

    /**
     * ParcelamentoBO constructor.
     */
    public function __construct() {
        $this->parcelamentoIntervaloDAO = new ParcelamentoIntervaloDAO();
    }


    /**
     * Busca o parcelamento de um valor informado
     * @param $valor
     * @return \App\Model\ParcelamentoIntervalo
     * @throws ValidacaoException
     */
    public function getParcelamentoIntervaloPorValor($valor) {
        if (!Validacao::validar($valor, 'float')) {
            throw new ValidacaoException('Campo valor deve ser float', 2);
        }
        return $this->parcelamentoIntervaloDAO->getParcelamentoIntervalo($valor);
    }

}