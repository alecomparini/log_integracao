<?php
namespace App\BO;

use App\DAO\LocalidadeDAO;

/**
 * Description of LocalidadeBO
 *
 * @author suportesoftbox
 */
class LocalidadeBO extends BaseBO {
    public function getLocalidadeBySigla(array $siglas) {
        $localidadeDao = new LocalidadeDAO();
        return $localidadeDao->getLocalidadeBySigla($siglas);
    }
    
    public function getLocalidadeByCep($cep) {
        $localidadeDao = new LocalidadeDAO();
        return $localidadeDao->getLocalidadeByCep($cep);
    }
}
