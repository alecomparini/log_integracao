<?php

namespace App\BO;

use App\DAO\BandeiraDAO;
use App\Exceptions\ValidacaoException;
use App\Helper\Utils;
use App\Helper\Validacao;
use App\Model\Parcelamento;

/**
 * Classe de negócios responsáveis por manipulações de bandeiras
 * @package App\BO
 */
class BandeiraBO extends BaseBO {

    /**
     * Busca Bandeiras de cartões
     * @param null $bandeiraId
     * @return \App\Model\Bandeira[]
     * @throws ValidacaoException
     */
    public function getBandeiras($bandeiraId = null) {
        $regras = array(
            'bandeiraId' => array('tipoAceitos' => array('null', 'int', 'int_array')),
        );
        $validacao = new Validacao($regras);
        if (!$validacao->executaValidacao(array('bandeiraId' => $bandeiraId))) {
            throw new ValidacaoException($validacao->msg, 2);
        }
        $bandeiraDAO = new BandeiraDAO();
        if (is_null($bandeiraId)) {
            return $bandeiraDAO->all();
        }
        return $bandeiraDAO->findById($bandeiraId);
    }

    /**
     * Busca os parcelamentos com e sem juros de uma ou mais bandeiras
     * @param $valor
     * @param null $bandeiraId
     * @return \App\Model\Bandeira[]
     * @throws ValidacaoException
     */
    public function getParcelamentos($valor, $bandeiraId = null) {

        //Validacao dos campos
        $regras = array(
            'valor' => array('float'),
            'bandeiraId' => array('tiposAceiros' => array('null', 'int', 'int_array'))
        );
        $validacao = new Validacao($regras);
        $obj = (object)array(
            'valor' => $valor,
            'bandeiraId' => $bandeiraId
        );

        if (!$validacao->executaValidacao($obj)) {
            throw new ValidacaoException($validacao->msg, 2);
        }

        //Dependências
        $parcelamentoBO = new ParcelamentoBO();
        $fatoresCartaoBO = new FatorCartaoBO();

        //Busco os dados de parcelamento máximo sem juros
        $parcelamentoIntervalo = $parcelamentoBO->getParcelamentoIntervaloPorValor($valor);

        //Busco as bandeiras pra pegar os fatores
        $bandeiras = $this->getBandeiras($bandeiraId);

        foreach ($bandeiras as $bandeira) {
            $bandeira->setVisible('parcelamentos');
            $parcelamentos = array();

            //Faz os parcelamentos sem juros
            for ($numParcela = 1; $numParcela <= $parcelamentoIntervalo->parcelamento; $numParcela++) {
                $parcelamento = new Parcelamento();
                $parcelamento->contemJuros = false;
                $parcelamento->taxaJuros = 0;
                $parcelamento->parcelas = $numParcela;
                $parcelamento->valorParcelamento = Utils::convertMoeda($valor / $numParcela);
                $parcelamento->total = Utils::convertMoeda($parcelamento->valorParcelamento * $parcelamento->parcelas);

                $parcelamentos[] = $parcelamento;
            }

            //Busca a taxa de fatores da bandeira selecionada
            $fatoresCartao = $fatoresCartaoBO->getFatoresCartao($bandeira->bandeiraId);

            //Monta o array de taxas
            $taxas = array();
            if (!empty($fatoresCartao)) {
                foreach ($fatoresCartao as $fator) {
                    $taxas[$fator->parcela] = $fator->fator;
                }
            } else {
                for ($i = 1; $i <= 12; $i++) {
                    $taxas[$i] = 1 / $i;
                }
            }

            //Completa os parcelamentos com os juros
            for ($numParcela = $parcelamentoIntervalo->parcelamento + 1; $numParcela <= 12; $numParcela++) {
                $parcelamento = new Parcelamento();
                $parcelamento->contemJuros = isset($fatoresCartao[$numParcela - 1]);
                $parcelamento->taxaJuros = $taxas[$numParcela];
                $parcelamento->parcelas = $numParcela;
                $parcelamento->valorParcelamento = Utils::convertMoeda($valor * $taxas[$numParcela]);
                $parcelamento->total = Utils::convertMoeda($parcelamento->valorParcelamento * $parcelamento->parcelas);

                $parcelamentos[] = $parcelamento;
            }

            $bandeira->parcelamentos = $parcelamentos;
        }

        return $bandeiras;
    }
}