<?php
namespace App\BO;

use App\DAO\DBLockDAO;

/**
 * Class DBLockBO
 * @package App\BO
 */
class DBLockBO extends BaseBO {
    /** @var array */
    protected static $locks = array();

    /** @var DBLockDAO */
    protected static $dbLockDAO;

    /** @var bool */
    protected static $initialized = false;

    protected static $totalLocks = 0;

    /**
     * @param $name
     * @param $timeout
     * @return \App\Model\DBLock
     */
    public static function getLock($name, $timeout) {
        if (!isset(static::$locks[$name])) {
            static::$locks[$name] = $timeout;
            static::$totalLocks++;
        }
        return static::$dbLockDAO->getLock($name, $timeout);
    }

    /**
     * @param $name
     * @return \App\Model\DBLock
     */
    public static function releaseLock($name) {
        if (isset(static::$locks[$name])) {
            unset(static::$locks[$name]);
            static::$totalLocks--;
        }
        return static::$dbLockDAO->releaseLock($name);
    }

    /**
     * Libera todos os locks
     */
    public static function releaseAll() {
        $locksKeys = array_keys(static::$locks);
        foreach ($locksKeys as $lockName) {
            static::releaseLock($lockName);
        }
    }

    /**
     * Espécie de construtor
     */
    public static function init() {
        if (!static::$initialized) {
            static::$initialized = true;

            static::$dbLockDAO = new DBLockDAO();
        }
    }
}

DBLockBO::init();