<?php

namespace App\BO;

use App\DAO\AtributoDAO;
use App\Exceptions\ValidacaoException;
use App\Helper\Validacao;

/**
 * Class AtributoBO
 * @package App\BO
 */
class AtributoBO extends BaseBO {
    private $atributoDao;

    /**
     * AtributoBO constructor.
     */
    public function __construct() {
        $this->atributoDao = new AtributoDAO();
    }

    /**
     * Busca, se houver, os atributos de cor e tamanho do produto
     * @param int|int[] $produtosId ID's dos produtos a serem consultados
     * @return \App\Model\AtributoCorTamanho[]
     * @throws ValidacaoException
     */
    public function getAtributosCorTamanhoById($produtosId) {
        $produtosId = is_array($produtosId) ? $produtosId : array($produtosId);
        $regras = array('produtosId' => array('required', 'int_array', 'notempty'));
        $obj = (object)array('produtosId' => $produtosId);

        $validacao = new Validacao($regras);
        if (!$validacao->executaValidacao($obj)) {
            throw new ValidacaoException($validacao->msg, 2);
        }

        return $this->atributoDao->getAtributosCorTamanhoById($produtosId);
    }

    /**
     * Busca, se houver, os atributos de substituicao do produto
     * @param int[] $produtosId
     * @param $siteId
     * @return \App\Model\AtributoSubstituicao[]
     * @throws ValidacaoException
     */
    public function getAtributosSubstituicaoById($produtosId, $siteId) {
        $produtosId = is_array($produtosId) ? $produtosId : array($produtosId);
        $regras = array('produtosId' => array('required', 'int_array', 'notempty'),
                        'siteId' => array('required', 'int'));
        $obj = (object)array('produtosId' => $produtosId, 'siteId' => $siteId);

        $validacao = new Validacao($regras);
        if (!$validacao->executaValidacao($obj)) {
            throw new ValidacaoException($validacao->msg, 2);
        }

        return $this->atributoDao->getAtributosSubstituicaoById($produtosId, $siteId);
    }
}