<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 06/01/2016
 * Time: 11:01
 */

namespace App\BO;


use App\DAO\ClientePfDAO;
use App\DAO\ClientePjDAO;
use App\Exceptions\BusinessException;
use App\Helper\Validacao;

/**
 * Class ClienteBO
 * @package App\BO
 */
class ClienteBO extends BaseBO {

    private $clientePfDAO;
    private $clientePjDAO;

    public function __construct() {
        parent::__construct();
        $this->clientePfDAO = new ClientePfDAO();
        $this->clientePjDAO = new ClientePjDAO();
    }

    /**
     * Veriica se o e-mail já está sendo utilizado por outro Cliente
     * @param string $email
     * @param string $documento CPF ou CNPJ
     * @param string $tipoCliente pf ou pj
     * @param int $siteId
     * @return bool Se o e-mail está em uso por outra pessoa neste siteId ou não
     * @throws \Exception Validação não foi executada com sucesso.
     */
    public function emailEmUso($email, $documento, $tipoCliente, $siteId) {
        $obj = (object) array(
            'email' => $email,
            'documento' => $documento,
            'tipoCliente' => $tipoCliente,
            'siteId' => $siteId
        );
        $tipoDocumento = $tipoCliente === 'pf' ? 'cpf' : 'cnpj';
        $regras = array(
            'email' => array('email'),
            'documento' => array('required', 'notempty', $tipoDocumento),
            'tipoCliente' => array('str' => array('TiposAceitos' => array('pf', 'pj'))),
            'siteId' => array('int')
        );
        $validador = new Validacao($regras);
        if (!$validador->executaValidacao($obj)) {
            throw new BusinessException("Erro validação: " . $validador->msg, 2);
        }

        if ($tipoCliente === 'pf') {
            return $this->clientePfDAO->emailEmUso($email, $documento, $siteId);
        }
        return $this->clientePjDAO->emailEmUso($email, $documento, $siteId);
    }

}