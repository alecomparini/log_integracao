<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 05-02-2016
 * Time: 17:47
 */

namespace App\BO;


use App\DAO\FatorCartaoDAO;
use App\Exceptions\ValidacaoException;
use App\Helper\Validacao;

class FatorCartaoBO extends BaseBO {

    /**
     * Busca os fatores de um determinado cartão
     * @param $bandeiraId
     * @return \App\Model\FatorCartao[]
     * @throws ValidacaoException
     */
    public function getFatoresCartao($bandeiraId) {
        $regras = array(
            'bandeiraId' => array('required', 'notempty', 'int'),
        );
        $obj = (object)array('bandeiraId' => $bandeiraId);
        $validacao = new Validacao($regras);
        if (!$validacao->executaValidacao($obj)) {
            throw new ValidacaoException($validacao->msg, 2);
        }

        $fatorCartaoDAO = new FatorCartaoDAO();
        return $fatorCartaoDAO->getFatores($bandeiraId);
    }
}