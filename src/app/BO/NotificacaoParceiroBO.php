<?php
namespace App\BO;

use App\Core\Benchmark;
use App\Core\Logger;
use App\DAO\NotificacaoDAO;
use App\DAO\NotificacaoParceiroDAO;
use App\DAO\ParceiroDAO;
use App\DAO\ProdutoDAO;
use App\Exceptions\DatabaseException;
use App\Exceptions\ValidacaoException;
use App\Helper\Http;
use App\Model\Configuracao;
use App\DAO\NotificacaoEstornoPontosDAO;
use App\Model\NotificacaoParceiro;
use DateInterval;
use DateTime;

/**
 * Class NotificacaoParceiroBO
 *
 * @package App\BO
 */
class NotificacaoParceiroBO extends BaseBO {
    const MAX_TENTATIVA_NOTIFICACAO = 10;
    private $notificacaoParceiroDAO;
    private $notificacaoDAO;

    /**
     * NotificacaoParceiroBO constructor.
     */
    public function __construct() {
		parent::__construct();
        $this->notificacaoParceiroDAO = new NotificacaoParceiroDAO();
        $this->notificacaoDAO = new NotificacaoDAO();
    }

    public $debug = false;

    public function getNotificacoes($limite = 250) {
        $parceiroDAO = new ParceiroDAO();
        $parceirosAtivos = $parceiroDAO->getParceiroAtivo();

        $pcoCfgBO = new ParceiroConfiguracaoBO();

        $notificacoes = array();
        $notificacoesDAO = new NotificacaoParceiroDAO();
        foreach ($parceirosAtivos as $parceiro) {
            $qtdMaxTentativasConfig = $pcoCfgBO->getConfiguracoes($parceiro->parceiroId, Configuracao::QTD_TENTATIVAS_NOTIFICACAO, true);

            $qtdMaxTentaiva = is_null($qtdMaxTentativasConfig) ? 5 : $qtdMaxTentativasConfig->valor;
            $notificacoesParceiro = $notificacoesDAO->getNotificacoesParceiro($parceiro->parceiroId, $qtdMaxTentaiva, $limite);
            $notificacoes = array_merge($notificacoes, $notificacoesParceiro);
        }

        return $notificacoes;
    }

    public function getNotificacoesExpiradas($limite = 50) {
        $parceiroDAO = new ParceiroDAO();
        $parceirosAtivos = $parceiroDAO->getParceiroAtivo();

        $notificacoes = array();
        $notificacoesDAO = new NotificacaoParceiroDAO();

        $configBO = new ParceiroConfiguracaoBO();

        foreach ($parceirosAtivos as $parceiro) {
            $configDelayRenotificacao = $configBO->getConfiguracoes($parceiro->parceiroId, Configuracao::DELAY_MINUTOS_NOTIFICACAO_FILA_LENTA, true);

            $agora = new DateTime('now');
            $periodo = new DateInterval("PT{$configDelayRenotificacao->valor}M");
            $dataCorte = $agora->sub($periodo);

            $notificacoesParceiro = $notificacoesDAO->getNotificacoesExpiradasParceiro($parceiro->parceiroId, NotificacaoParceiroBO::MAX_TENTATIVA_NOTIFICACAO, $limite, $dataCorte->format('Y-m-d H:i:s'));
            $notificacoes = array_merge($notificacoes, $notificacoesParceiro);
        }

        return $notificacoes;
    }

    /**
     * Envia notificação de atualizações de preço e estoque para parceiros.
     * Faz leitura na tabela de notificações e envia uma notificação para o parceiro.
     *
     * @param int $limite limite na query para buscar notificações
     *
     * @return string
     * @throws \App\Exceptions\DatabaseException
     * @throws \App\Exceptions\ValidacaoException
     */
    public function notificarParceiros($limite = 500) {
        Benchmark::start('Método completo');
        $response = "";
        $notificacoes = $this->notificacaoParceiroDAO->getNotificacoes(NotificacaoParceiroBO::MAX_TENTATIVA_NOTIFICACAO, $limite);
        $parceiroConfigBO = new ParceiroConfiguracaoBO();

        $configuracoes = array();

        // Enviar notificações para os parceiros
        foreach ($notificacoes as $notificacao) {
            try {
                Benchmark::start('Get config parceiro');
                if (!isset($configuracoes[$notificacao->parceiroId]['confUrl'])) {
                    $configuracoes[$notificacao->parceiroId]['confUrl'] = $parceiroConfigBO->getConfiguracoes($notificacao->parceiroId, Configuracao::URL_NOTIFICACAO, true);
                }

                if (!isset($configuracoes[$notificacao->parceiroId]['confNotifica'])) {
                    $configuracoes[$notificacao->parceiroId]['confNotifica'] = $parceiroConfigBO->getConfiguracoes($notificacao->parceiroId, Configuracao::ACEITA_NOTIFICACAO, true);
                }

                $confNotifica = $configuracoes[$notificacao->parceiroId]['confNotifica'];
                $confUrl = $configuracoes[$notificacao->parceiroId]['confUrl'];

                $response .= sprintf('Tempo buscar config: %s segundos', Benchmark::end('Get config parceiro', true)) . PHP_EOL;

                if ($confNotifica->valor == 1) {
                    // Tenta notificar parceiro
                    Benchmark::start('Tempo de post');
                    $resp = Http::post($confUrl->valor, array('skuId' => $notificacao->produtoCodigo, 'stockModified' => $notificacao->modificacaoEstoque == 1, 'priceModified' => $notificacao->modificacaoPreco == 1,), true);
                    $response .= sprintf('Tempo para realizar o post: %s segundos', Benchmark::end('Tempo de post', true)) . PHP_EOL;

                    // Parceiro foi notificado
                    if ($resp->responseCode >= Http::OK && $resp->responseCode <= 299) {
                        Benchmark::start('Delete se ok');
                        $response .= 'Parceiro ID: ' . $notificacao->parceiroId . ' notificado sobre o produto ' . $notificacao->produtoCodigo . PHP_EOL;
                        $this->notificacaoParceiroDAO->delete(array('ParceiroId' => $notificacao->parceiroId, 'ProdutoCodigo' => $notificacao->produtoCodigo));

                        $response .= sprintf('Tempo para executar delete se OK: %s segundos', Benchmark::end('Delete se ok', true)) . PHP_EOL;
                    } else {
                        // Parceiro não foi notificado
                        Benchmark::start('Update se erro');
                        $this->notificacaoParceiroDAO->update(array('ParceiroId' => $notificacao->parceiroId, 'ProdutoCodigo' => $notificacao->produtoCodigo), array('DataUltimaTentativa' => date("Y-m-d H:i:s"), 'Tentativas' => $notificacao->tentativas + 1));
                        $response .= sprintf('Tempo para executar update se Erro: %s segundos', Benchmark::end('Update se erro', true)) . PHP_EOL;
                        $response .= 'Parceiro ID: ' . $notificacao->parceiroId . ' erro ao notificar produto ' . $notificacao->produtoCodigo . '. Atualizando' . PHP_EOL;
                    }
                } else {
                    // Remove notificação para o parceiro
                    Benchmark::start('Delete se notificacao off');
                    $this->notificacaoParceiroDAO->delete(array('ParceiroId' => $notificacao->parceiroId, 'ProdutoCodigo' => $notificacao->produtoCodigo));
                    $response .= sprintf('Tempo para executar delete se não notificar: %s segundos', Benchmark::end('Delete se notificacao off', true)) . PHP_EOL;
                    $response .= 'Parceiro ID: ' . $notificacao->parceiroId . ' não quer ser notificado. BUG' . PHP_EOL;
                }
            } catch (DatabaseException $dbEx) {
                $response .= 'Exception: ' . $dbEx->getMessage() . PHP_EOL;
                $response .= sprintf('Falha ao remover notificação do ParceiroId %d e código %s', $notificacao->parceiroId, $notificacao->produtoCodigo) . PHP_EOL;
            }

            $response .= 'Http Code:' . $resp->responseCode . PHP_EOL;
            $response .= 'Body:' . $resp->body . PHP_EOL;
            $response .= 'contentType:' . $resp->contentType . PHP_EOL;

            $response .= '-------------------' . PHP_EOL;
        }

        return $response . 'Execution time: ' . Benchmark::end('Método completo', true) . ' seconds.' . PHP_EOL;
    }

    /**
     * Avisa aos parceiros que houve alteração de tracking em algum(ns) de seus pedidos
     *
     * @throws \App\Exceptions\DatabaseException
     * @throws \App\Exceptions\ValidacaoException
     */
    public function notificarTrackingParceiros() {
        $notificacoes = $this->notificacaoParceiroDAO->getNotificacoesTracking(NotificacaoParceiro::MAX_TENTATIVA_NOTIFICACAO);
        $parceiroConfigBO = new ParceiroConfiguracaoBO();

        foreach ($notificacoes as $notificacao) {
            $confUrl = $parceiroConfigBO->getConfiguracoes($notificacao->parceiroId, Configuracao::URL_NOTIFICACAO_TRACKING, true);
            $confNotifica = $parceiroConfigBO->getConfiguracoes($notificacao->parceiroId, Configuracao::ACEITA_NOTIFICACAO, true);

            if ($confNotifica->valor == 1) {
                $resp = Http::post($confUrl->valor, array(
                    'An' => 'sandboxintegracao',
                    'IdAffiliate' => $notificacao->parceiroId,
                    'IdAffiliateOrder' => $notificacao->pedidoParceiroId,
                    'TrackingModified' => $notificacao->modificacaoTracking == 1
                ), true);
                print_r($resp);
                if ($resp->responseCode == Http::OK) {
                    echo 'Parceiro ID: ' . $notificacao->parceiroId . ' notificado sobre o tracking ' . $notificacao->pedidoParceiroId . PHP_EOL;
                    $this->notificacaoParceiroDAO->deleteTracking($notificacao->parceiroId, $notificacao->pedidoParceiroId);
                } else {
                    $this->notificacaoParceiroDAO->update(array(
                        'ParceiroId' => $notificacao->parceiroId,
                        'ProdutoCodigo' => $notificacao->produtoCodigo
                    ), array(
                        'DataUltimaTentativa' => date("Y-m-d H:i:s"),
                        'Tentativas' => $notificacao->tentativas + 1
                    ));
                    echo 'Parceiro ID: ' . $notificacao->parceiroId . ' erro ao notificar. Atualizando' . PHP_EOL;
                }
            } else {
                $this->notificacaoParceiroDAO->delete(array(
                    'ParceiroId' => $notificacao->parceiroId,
                    'ProdutoCodigo' => $notificacao->produtoCodigo
                ));
                echo 'Parceiro ID: ' . $notificacao->parceiroId . ' não quer ser notificado. BUG' . PHP_EOL;
            }
        }
    }
    
    public function enviarNotificacao($notificacao, $url) {
        $response = "";
        $resp = Http::post($url, array('skuId' => $notificacao->produtoCodigo, 'stockModified' => $notificacao->modificacaoEstoque == 1, 'priceModified' => $notificacao->modificacaoPreco == 1,), true);

        $paramsLog = array('UrlParceiro' => $url, 'skuId' => $notificacao->produtoCodigo, 'stockModified' => $notificacao->modificacaoEstoque == 1, 'priceModified' => $notificacao->modificacaoPreco == 1);

        if ($resp->responseCode >= Http::OK && $resp->responseCode <= 299) {
            // Parceiro foi notificado
            $response = 'SUCESSO. ParceiroID(' . $notificacao->parceiroId . ') foi notificado do Produto (' . $notificacao->produtoCodigo . ') - StatusCode (' . $resp->responseCode .')';
            $this->notificacaoParceiroDAO->delete(array('ParceiroId' => $notificacao->parceiroId, 'ProdutoCodigo' => $notificacao->produtoCodigo));
        } else {
            // Parceiro não foi notificado
            $this->notificacaoParceiroDAO->update(array('ParceiroId' => $notificacao->parceiroId, 'ProdutoCodigo' => $notificacao->produtoCodigo), array('DataUltimaTentativa' => date("Y-m-d H:i:s"), 'Tentativas' => $notificacao->tentativas + 1));
            $response = 'FALHA. ParceiroID(' . $notificacao->parceiroId . ') NÃO foi notificado do Produto (' . $notificacao->produtoCodigo . ')  - StatusCode (' . $resp->responseCode .')';
        }

        $retorno = array("Mensagem=" => $response, "RetornoParceiro=" => $resp->body );
        Logger::getInstance("api_log")->log("[API:Enviar Notificação] [Parceiro:{$notificacao->parceiroId}] [Mensagem_tojs:" . json_encode(Logger::getContextData(__METHOD__,$paramsLog ,$retorno)) ."]");
        return $response;
    }

    public function getTotalNotificacoes($groupByParceiro = false) {
        if (!is_bool($groupByParceiro)) {
            throw new ValidacaoException('Campo [groupByParceiro] deve ser boolean');
        }

        return $this->notificacaoDAO->getTotalNotificacoes($groupByParceiro);
    }

    public function getTotalNotificacoesErro() {
        return $this->notificacaoDAO->getTotalNotificacoesErro();
    }

    public function setNotificacao($parceiroId, $codigos, $tipoNotificacao) {
        $configBO = new ParceiroConfiguracaoBO();
        $parceiroBO = new ParceiroSiteBO();
        $notificacaoDAO = new NotificacaoDAO();

        $parceiro = $parceiroBO->getParceiroSiteById($parceiroId);
        $configAceitaNotificacao = $configBO->getConfiguracoes($parceiroId, Configuracao::ACEITA_NOTIFICACAO, true);
        $notificacoes = array();
        if ($configAceitaNotificacao->valor == '1') {
            foreach ($codigos as $codigo) {
                $notificacoes[] = array('parceiroId' => $parceiroId, 'siteId' => $parceiro->siteId, 'produtoCodigo' => $codigo,);
            }
            foreach ($notificacoes as $notificacao) {
                $notificacaoDAO->setNotificacao($notificacao, $tipoNotificacao);
            }
        }
    }
    
     /**
     * Avisa aos parceiros que houve alteração de tracking em algum(ns) de seus pedidos
     *
     * @throws \App\Exceptions\DatabaseException
     * @throws \App\Exceptions\ValidacaoException
     */
    public function setNotificacaoEstornoPontos() {
        $notificacoesPonto = $this->getParam('notificacoesPonto');
        $servicoBO = new ServicoBO();

        try {
            $resultado = $servicoBO->setNotificacaoEstornoPonto($notificacoesPonto);
        } catch (\Exception $e) {
            return $this->json(new Response($e->getMessage(), $e->getCode()), 412);
        }

        if ($resultado) {
            return $this->json(new Response('Notificações registradas com sucesso', 0, array(), true), 200);
        } else {
            return $this->json(new Response('', 20), 412);
        }       
    }
	
	
	public function enviarNotificacaoEstornoPontos($notificacao, $url) {
		$response = "";
		$notificacaoEstornoPontosDAO = new NotificacaoEstornoPontosDAO();
        Benchmark::start('Tempo de post');
		 $resp = Http::post($url, array(
                    'ClienteDocumento' => $notificacao->clienteDocumento,
                    'Pontos' => $notificacao->pontos,
                ), true);
		 
        $response .= sprintf('Tempo para realizar o post: %s segundos', Benchmark::end('Tempo de post', true)) . PHP_EOL;

        // Parceiro foi notificado
        if ($resp->responseCode >= Http::OK && $resp->responseCode <= 299) {
			$this->deletarNotificacaoEstornoPontos($notificacao->parceiroId, $notificacao->pedidoParceiroId, $notificacao->clienteDocumento);
	        $response .= 'Parceiro ID: ' . $notificacao->parceiroId . ' notificado sobre o estorno de pontos ' . $notificacao->ClienteDocumento .'.' . ' Pontos ' . $notificacao->pontos . PHP_EOL;
            Benchmark::start('Delete se ok');
            $response .= sprintf('Tempo para executar delete se OK: %s segundos', Benchmark::end('Delete se ok', true)) . PHP_EOL;
        } else {
            // Parceiro não foi notificado
            Benchmark::start('Update se erro');
            $notificacaoEstornoPontosDAO->update(array(
                        'ParceiroId' => $notificacao->parceiroId,
                        'PedidoParceiroId' => $notificacao->pedidoParceiroId,
                        'ClienteDocumento' => $notificacao->clienteDocumento
                    ), array(
                        'DataUltimaTentativa' => date("Y-m-d H:i:s"),
                        'Tentativas' => $notificacao->tentativas + 1
                    ));
            $response .= sprintf('Tempo para executar update se Erro: %s segundos', Benchmark::end('Update se erro', true)) . PHP_EOL;
            $response .= 'Parceiro ID: ' . $notificacao->parceiroId . ' erro ao notificar produto ' . $notificacao->produtoCodigo . '. Atualizando' . PHP_EOL;
        }
        
        return $response;		
	}
	
   /**
     * Avisa aos parceiros que houve alteração de tracking em algum(ns) de seus pedidos
     *
     * @throws \App\Exceptions\DatabaseException
     * @throws \App\Exceptions\ValidacaoException
     */
    public function notificarEstornoPontos() {
        $notificacaoEstornoPontosDAO = new NotificacaoEstornoPontosDAO();
        $notificacoes = $notificacaoEstornoPontosDAO->getNotificacoesEstornoPontos(NotificacaoParceiro::MAX_TENTATIVA_NOTIFICACAO);
        $parceiroConfigBO = new ParceiroConfiguracaoBO();

        foreach ($notificacoes as $notificacao) {
            $confUrl = $parceiroConfigBO->getConfiguracoes($notificacao->parceiroId, Configuracao::URL_NOTIFICAO_ESTORNO_PONTOS, true);
            $confNotifica = $parceiroConfigBO->getConfiguracoes($notificacao->parceiroId, Configuracao::ACEITA_NOTIFICACAO, true);
            $parceiroId = $notificacao->parceiroId;
            $pedidoParceiroId = $notificacao->pedidoParceiroId;
            $clienteDocumento = $notificacao->clienteDocumento;
            if ($confNotifica->valor == 1) {
                $resp = Http::post($confUrl->valor, array(
                    'ClienteDocumento' => $notificacao->clienteDocumento,
                    'Pontos' => $notificacao->pontos,
                ), true);
                if ($resp->responseCode == Http::OK) {
                    echo 'Parceiro ID: ' . $notificacao->parceiroId . ' notificado sobre o estorno de pontos ' . $notificacao->ClienteDocumento .'.' . ' Pontos ' . $notificacao->pontos . PHP_EOL;                                        
                } else {
                    $notificacaoEstornoPontosDAO->update(array(
                        'ParceiroId' => $notificacao->parceiroId,
                        'PedidoParceiroId' => $notificacao->pedidoParceiroId,
                        'ClienteDocumento' => $notificacao->clienteDocumento
                    ), array(
                        'DataUltimaTentativa' => date("Y-m-d H:i:s"),
                        'Tentativas' => $notificacao->tentativas + 1
                    ));
                    echo 'Parceiro ID: ' . $notificacao->parceiroId . ' erro ao notificar. Atualizando' . PHP_EOL;
                }
            } else {
                $notificacaoEstornoPontosDAO->deleteEstornoPonto($parceiroId, $pedidoParceiroId, $clienteDocumento);
                echo 'Parceiro ID: ' . $notificacao->parceiroId . ' não quer ser notificado. BUG' . PHP_EOL;
            }
        }
    }
	
    /**
     * Deleta notificação de estorno pontos
     * @param int $parceiroId
     * @param string $pedidoParceiroId
     * @param string $clienteDocumento
     * @return bool
     */
    private function deletarNotificacaoEstornoPontos($parceiroId, $pedidoParceiroId, $clienteDocumento) {
        $notificacaoEstornoPontosDAO = new NotificacaoEstornoPontosDAO();
        return $notificacaoEstornoPontosDAO->deleteEstornoPonto($parceiroId, $pedidoParceiroId, $clienteDocumento);
    }

    public function deletaNotificacoesExpiradas() {
        $qtdDeletados = 0;
        $parceiroBO = new ParceiroSiteBO();
        $parceiros = $parceiroBO->buscarTodosParceiros();

        $parceiroConfigBO = new ParceiroConfiguracaoBO();

        $notificacaoDAO = new NotificacaoDAO();
        foreach ($parceiros as $parceiro) {
            $tentativas = $parceiroConfigBO->getConfiguracoes($parceiro->parceiroId, Configuracao::QTD_TENTATIVAS_NOTIFICACAO, true);
            $qtdTentativas = is_null($tentativas) ? 5 : $tentativas->valor;
            $qtdDeletados += $notificacaoDAO->deletaNotificacaoParceiroTentativa($parceiro->parceiroId, $qtdTentativas);
        }

        return $qtdDeletados;
    }
}