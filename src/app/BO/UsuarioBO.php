<?php
namespace App\BO;

use App\Core\Logger;
use App\DAO\UsuarioDAO;
use App\Exceptions\BusinessException;
use App\Exceptions\ValidacaoException;
use App\Helper\PasswordHelper;
use App\Helper\Validacao;
use App\Model\Response;
use Exception;

/**
 * Class UsuarioBO
 *
 * @package App\BO
 */
class UsuarioBO extends BaseBO {

    /**
     * Retorna um usuário dado a sua ID. OU null caso não encontrado.
     *
     * @param $usuarioId
     *
     * @return \App\Model\V2\Usuario
     * @throws \App\Exceptions\BusinessException
     */
    public function buscarUsuarioPorId($usuarioId) {
        try {
            $regras = array('usuarioId' => array('int'));
            $obj = (object)array('usuarioId' => $usuarioId);
            $usuarioDAO = new UsuarioDAO();
            $validacao = new Validacao($regras);
            if (!$validacao->executaValidacao($obj)) {
                throw new ValidacaoException($validacao->msg, Response::ERRO_VALIDACAO);
            }

            return $usuarioDAO->buscarPorId($usuarioId);
        } catch (Exception $e) {
            throw new BusinessException('Erro ao buscar usuário', $e);
        }
    }

    /**
     * @param $email
     *
     * @return \App\Model\V2\Usuario
     * @throws \App\Exceptions\BusinessException
     * @throws \App\Exceptions\ValidacaoException
     */
    public function buscarUsuarioPorEmail($email) {
        try {
            $regras = array('email' => array('email'));
            $obj = (object)array('email' => $email);
            $validacao = new Validacao($regras);
            if (!$validacao->executaValidacao($obj)) {
                throw new ValidacaoException($validacao->msg, Response::ERRO_VALIDACAO);
            }

            $usuarioDAO = new UsuarioDAO();

            return $usuarioDAO->buscarPorEmail($email);
        } catch (ValidacaoException $ve) {
            throw new BusinessException($ve->getMessage(), $ve->getCode(), $ve);
        }
    }

    /**
     * @param $email
     * @param $senha
     *
     * @return \App\Model\V2\Usuario
     * @throws \App\Exceptions\BusinessException
     * @throws \App\Exceptions\ValidacaoException
     */
    public function buscarUsuarioPorEmailESenha($email, $senha) {
        $regras = array(
            'email' => array('email', 'notempty'),
            'senha' => array('str', 'notempty'),
        );
        $obj = (object)array('email' => $email, 'senha' => $senha);
        $validacao = new Validacao($regras);
        if (!$validacao->executaValidacao($obj)) {
            throw new ValidacaoException($validacao->msg, Response::ERRO_VALIDACAO);
        }
        $passwordHelper = new PasswordHelper();
        $usuario = $this->buscarUsuarioPorEmail($email);

        if (null !== $usuario && $passwordHelper->check($senha, $usuario->senha)) {
            return $usuario;
        }

        return null;
    }
}