<?php

namespace App\BO;

use App\DAO\CategoriaDAO;
use App\DAO\CategoriaDescontoParceiroDAO;
use App\Exceptions\ValidacaoException;
use App\Helper\Validacao;

/**
 * Class CategoriaBO
 * @package App\BO
 */
class CategoriaBO extends BaseBO {

    /** @var CategoriaDAO */
    protected $categoriaDAO;

    /**
     * CategoriaBO constructor.
     */
    public function __construct() {
        $this->categoriaDAO = new CategoriaDAO();
    }


    /**
     * Busca uma categoria e toda sub-árvore de categorias filhos dela
     * @param $categoriaId
     * @return \App\Model\Categoria[] As categorias solicitadas e todas as suas filhas
     * @throws ValidacaoException
     */
    public function buscarCategoriaEFilhos($categoriaId) {
        $regras = array(
            'categoriaId' => array('tiposaceitos' => array('int', 'int_array'))
        );
        $obj = (object) array('categoriaId' => $categoriaId);
        //Valida as regras
        $validacao = new Validacao($regras);
        if (!$validacao->executaValidacao($obj)) {
            throw new ValidacaoException($validacao->msg, 2);
        }

        $categoriaId = is_array($categoriaId) ? $categoriaId : array($categoriaId);
        if (empty($categoriaId)) {
            return array();
        }
        return $this->categoriaDAO->buscarCategoriaEFilhos($categoriaId);
    }
    
    public function buscarCategorias(array $categorias) {
        $regras = array(
            'categorias' => array('tiposaceitos' => array('int', 'int_array'))
        );
        $obj = (object) array('categorias' => $categorias);
        $validacao = new Validacao($regras);
        if (!$validacao->executaValidacao($obj)) {
            throw new ValidacaoException($validacao->msg, 2);
        }
        $retorno = array();
        if (!empty($categorias)) {
            $categorias = $this->categoriaDAO->buscarCategorias($categorias); 
            foreach ($categorias as &$categoria) {
                $retorno[$categoria->categoriaId] = $categoria;
                unset($categoria);
            }
        }
        return $retorno;
    }
    
    public function buscarSubCategorias(array $categorias) {
        $regras = array(
            'categorias' => array('tiposaceitos' => array('int', 'int_array'))
        );
        $obj = (object) array('categorias' => $categorias);
        $validacao = new Validacao($regras);
        if (!$validacao->executaValidacao($obj)) {
            throw new ValidacaoException($validacao->msg, 2);
        }
        if (!empty($categorias)) {
            $subCategorias = $this->categoriaDAO->buscarSubCategorias($categorias);        
            $retorno = array();
            foreach ($subCategorias as &$categoria) {
                $retorno[$categoria->subCategoriaId] = $categoria;
                unset($categoria);
            }
            return $retorno;
        }
        return array();
    }

    /**
     * Busca os ID's das categorias e exibe seus respectivos descontos.
     *
     * @param $parceiroId
     * @return \App\Model\CategoriaDescontoParceiro[]
     */
    public function buscaCategoriaDescontoParceiro($parceiroId) {
        $categoriaDescontoParceiroDAO = new CategoriaDescontoParceiroDAO();
        return $categoriaDescontoParceiroDAO->getDescontoCategoriaParceiro($parceiroId);
    }

}