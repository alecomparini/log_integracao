<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 06/01/2016
 * Time: 11:53
 */

namespace App\BO;

use App\DAO\ParceiroSitePermissaoDAO;
use App\DAO\ProdutoDAO;
use App\Exceptions\ValidacaoException;
use App\Helper\Validacao;
use App\Model\ParceiroSitePermissao;

/**
 * Class ParceiroSitePermissaoBO
 * @package App\BO
 */
class ParceiroSitePermissaoBO extends BaseBO {

    /**
     * Retorna as permissões em um determinado site para um determinado parceiro
     * @param $parceiroId int
     * @param $siteBase
     * @param null $tipoPermissao
     * @return \App\Model\ParceiroSitePermissao[]
     * @throws \Exception
     */
    public function getPermissoesBySiteBaseParceiroId($parceiroId, $siteBase, $tipoPermissao = null) {
        $obj = (object)array(
            'parceiroId' => $parceiroId,
            'siteBase' => $siteBase,
        );
        $regras = array(
            'parceiroId' => array('int'),
            'siteBase' => array('int'),
        );
        $validador = new Validacao($regras);
        if (!$validador->executaValidacao($obj)) {
            throw  new \Exception("Erro de validação: " . $validador->msg, 2);
        }

        $parceiroSitePermissaoDAO = new ParceiroSitePermissaoDAO();
        return $parceiroSitePermissaoDAO->getPermissoes($parceiroId, $siteBase, $tipoPermissao);
    }

    /**
     * Verifica a permissão de um produto no site de acordo com categoria, fabricante e outros fatores.
     * @param $produtoCodigo
     * @param $parceiroId
     * @return bool
     * @throws ValidacaoException
     */
    public function verificaPermissaoProduto($produtoCodigo, $parceiroId) {
        $regras = array(
            'parceiroId' => array('int')
        );
        $obj = array('parceiroId' => $parceiroId);
        $validador = new Validacao($regras);
        if (!$validador->executaValidacao($obj)) {
            throw new ValidacaoException("Erro de validação: " . $validador->msg, 2);
        }

        //getDadosPermissao

        $produtoDAO = new ProdutoDAO();
        $produtosPermissao = $produtoDAO->buscaProdutoPermissao($produtoCodigo, $parceiroId);

        return sizeof($produtosPermissao) > 0;
    }

    /**
     * Verifica se o IP que está acessando existe na tabela de permissões
     * Este método é utilizado na página de manual, onde só é verificado
     * se o IP possui permissão, independente de informar parceiro ID ou não
     * pois a documentação é igual para todos. Pelo menos por enquanto.....<<<HAULE>>>
     * @param $ip
     * @return bool
     * @throws \Exception
     */
    public function verificaPermissaoIp($ip) {
        if (!Validacao::validar($ip, 'ip')) {
            throw new \Exception(Validacao::$lastMsg, 2);
        }
        $parceiroSitePermissaoDAO = new ParceiroSitePermissaoDAO();
        $permissoes = $parceiroSitePermissaoDAO->getPermissoesByIp($ip);

        return count($permissoes) > 0;
    }

    public function parceiroPermissaoTudo($parceiroId, $siteId, $tipoPermissao = null) {
        $parceiroPermissoesTudo = $this->getPermissoesBySiteBaseParceiroId($parceiroId, $siteId, $tipoPermissao);
        if (count($parceiroPermissoesTudo) > 0) {
            return true;
        }
        return false;
    }

    /**
     * Obtem permissoes de um parceiro em um site
     * @param int $parceiroId       ParceiroId do parceiro
     * @param int $siteId           SiteId do parceiro
     * @return array
     * @throws ValidacaoException
     * @throws \Exception
     */
    public function getDadosPermissao($parceiroId, $siteId) {
        //Database::getInstance()->beginTransaction();
        $fabricantesPermitidos = array();
        $fabricantesNaoPermitidos = array();
        $categoriasPermitidas = array();
        $categoriasNaoPermitidas = array();
        $permiteTodos = $this->parceiroPermissaoTudo($parceiroId, $siteId, ParceiroSitePermissao::TUDO);
        $campanhaPermitida = false;

        if (!$permiteTodos) {
            $parceiroPermissoes = $this->getPermissoesBySiteBaseParceiroId($parceiroId, $siteId);
            foreach ($parceiroPermissoes as $permissao) {
                switch (intval($permissao->permissaoTipoId)) {
                    case 1: // Fabricante
                        if ($permissao->excecao == 0) {
                            $fabricantesPermitidos[] = $permissao->permissao;
                        } else {
                            $fabricantesNaoPermitidos[] = $permissao->permissao;
                        }
                        break;
                    case 2: // Categoria
                        if ($permissao->excecao == 0) {
                            $categoriasPermitidas[] = $permissao->permissao;
                        }
                        break;
                    case 4: // Campanha
                            $campanhaPermitida = $permissao->permissao;
                        break;
                }
            }

            //Busca as categorias
            $categoriaBO = new CategoriaBO();
            $catPermitidas = $categoriaBO->buscarCategoriaEFilhos($categoriasPermitidas);
            $catNaoPermitidas = $categoriaBO->buscarCategoriaEFilhos($categoriasNaoPermitidas);

            $categoriasPermitidas = array();
            foreach ($catPermitidas as $cat) {
                $categoriasPermitidas[] = $cat->categoriaId;
            }

            $categoriasNaoPermitidas = array();
            foreach ($catNaoPermitidas as $cat) {
                $categoriasNaoPermitidas[] = $cat->categoriaId;
            }
        }

        //Database::getInstance()->commit();
        return array(
            'todos' => $permiteTodos,
            'categorias' => array_diff($categoriasPermitidas, $categoriasNaoPermitidas),
            'fabricantes' => array_diff($fabricantesPermitidos, $fabricantesNaoPermitidos),
            'campanha' => $campanhaPermitida,
        );
    }
}