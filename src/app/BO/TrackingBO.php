<?php

namespace App\BO;

use App\DAO\TrackingDAO;
use App\Exceptions\ValidacaoException;
use App\Model\Tracking;
use App\Model\TrackingOut;
use App\Helper\Validacao;

/**
 * Classe resonsável por gerenciar todas as Regras
 * de negócio do objeto Tracking
 */
class TrackingBO extends BaseBO {

    const MAX_PEDIDO_CONSULTA = 10;

    /**
     * Método responsável por busca  um tracking
     * @method consulta
     * @param  array $pedidoId Lista de ID~s do pedido
     * @param  int $parceiroId ID do Parceiro
     * @param  int $siteId ID do Site
     * @return \App\Model\Tracking[] Lista contendo objetos de tracking
     * @throws \Exception
     */
    public function consulta($pedidoId, $parceiroId, $siteId) {
        $trackingsCancelamentoPrePedido = $this->buscaTrackingsCancelamentoPrePedido($pedidoId, $parceiroId);

        // Regras de validação dos parametros
        $condicoes = array(
            "parceiroId" => array("notempty", "int", "required"),
            "siteId" => array("notempty", "int", "required"),
            "pedidoId" => array("notempty", "array" =>
                        array("min" => 1, "max" => static::MAX_PEDIDO_CONSULTA),
                "array", "required")
        );

        // Parametros enviados no post
        $validacao = new Validacao($condicoes);
        $trackingOut = new TrackingOut();
        $dados = new \stdClass();
        
        $dados->parceiroId = $parceiroId;
        $dados->siteId = $siteId;
        $dados->pedidoId = $pedidoId;
        
        //Executa a validação e em caso de erro, joga exception
        if (!$validacao->executaValidacao($dados)) {
            throw new ValidacaoException($validacao->msg, $trackingOut->codigo);
        }

        $trackingDAO = new TrackingDAO();

        $trackingsTabela = $trackingDAO->find($pedidoId, $parceiroId, $siteId);
        return array_merge($trackingsCancelamentoPrePedido, $trackingsTabela);
    }

    private function buscaTrackingsCancelamentoPrePedido($pedidoId, $parceiroId) {
        $pedidoBO = new PedidoBO();
        $pedidos = $pedidoBO->buscarPedidosPorParceiroPedidoId($pedidoId, $parceiroId);

        return $this->getTrackingsPrePedidoCancelado($pedidos);
    }

    /**
     * @param $pedidos \App\Model\Pedido[]
     */
    private function getTrackingsPrePedidoCancelado($pedidos) {
        $trackings = array();
        foreach ($pedidos as $pedido) {
            $itens = array();

            foreach ($pedido->pedidoProduto as $pedidoProduto) {
                $itens[$pedidoProduto->codigoProduto] = $pedidoProduto->quantidade;
            }

            if (6 === (int)$pedido->statusExportacao) {
                $trackingJson = json_encode(
                    array(
                        array(
                            'pedido_parceiro' => $pedido->parceiroPedidoId,
                            'entregas' => array(
                                'ENT_' . $pedido->parceiroPedidoId . '01' => array(
                                    'itens' => $itens,
                                    'trackings' => array(
                                        'CAN' => array(
                                            'd' => date('Y-m-d H:i:s'),
                                            'mensagem' => $pedido->mensagemErroImportacao,
                                        )
                                    )
                                )
                            )
                        )
                    )
                );

                $tracking = new Tracking();
                $tracking->json = $trackingJson;
                $tracking->parceiroId = $pedido->parceiroId;
                $tracking->pedidoParceiroId = $pedido->parceiroPedidoId;
                $trackings[] = $tracking;
            }
        }

        return $trackings;
    }
}