<?php
namespace App\BO;

use App\DAO\ParceiroSiteDAO;
use App\Exceptions\ValidacaoException;
use App\Helper\Validacao;
use App\Model\ParceiroSite;
use App\Model\Response;

/**
 * Class ParceiroSiteBO
 *
 * @package App\BO
 */
class ParceiroSiteBO extends BaseBO {
    /**
     * Recupera um ParceiroSite ou null (quando não existente)
     * after
     *
     * @param  int $parceiroId ID do Parceiro
     * @param  int $siteId     ID do Site
     *
     * @return parceiroSite|null ParceiroSite
     * @throws \Exception
     */
    public function getParceiroSite($parceiroId, $siteId) {

        //Define as regras de validação
        $regras = array(
            "parceiroId" => array("int", "notempty", "required"),
            "siteId" => array("int", "notempty", "required")
        );
        $parceiroSiteDAO = new ParceiroSiteDAO;
        $validacaoParceiroSite = new Validacao($regras);

        //Cria objeto para a validação
        $objValidacao = new \stdClass;
        $objValidacao->parceiroId = $parceiroId;
        $objValidacao->siteId = $siteId;

        //Valida e caso não passa, disparar exception
        if (!$validacaoParceiroSite->executaValidacao($objValidacao)) {
            throw new ValidacaoException($validacaoParceiroSite->msg, Response::ERRO_VALIDACAO);
        }

        $parceiro = $parceiroSiteDAO->getParceiroSite($parceiroId, $siteId);
        if (null !== $parceiro) {
            $parceiro->integradores = explode(",", $parceiro->integradores);
        }

        return $parceiro;

    }

    /**
     * Retorna o(s) ParceiroSite(s) de um parceiro
     *
     * @param $parceiroId int
     *
     * @return ParceiroSite|null
     * @throws \Exception
     */
    public function getParceiroSiteById($parceiroId) {
        //Define as regras de validação
        $regras = array(
            "parceiroId" => array("int", "notempty", "required")
        );
        $parceiroSiteDAO = new ParceiroSiteDAO;
        $validacaoParceiroSite = new Validacao($regras);

        //Cria objeto para a validação
        $objValidacao = new \stdClass;
        $objValidacao->parceiroId = $parceiroId;

        //Valida e caso não passa, disparar exception
        if (!$validacaoParceiroSite->executaValidacao($objValidacao)) {
            throw new \Exception($validacaoParceiroSite->msg);
        }

        return $parceiroSiteDAO->getParceiroSiteById($parceiroId);
    }

    /**
     * Retorna os IDs dos produtos do catálogo da campanha ativa a qual o parceiro está vinculado
     *
     * @param $parceiroId int
     *
     * @return ParceiroSite|null
     * @throws \Exception
     */
    public function getParceiroCampanhaAtiva($parceiroId) {
        //Define as regras de validação
        $regras = array(
            "parceiroId" => array("int", "notempty", "required")
        );
        $parceiroSiteDAO = new ParceiroSiteDAO;
        $validacaoParceiroSite = new Validacao($regras);

        //Cria objeto para a validação
        $objValidacao = new \stdClass;
        $objValidacao->parceiroId = $parceiroId;

        //Valida e caso não passa, disparar exception
        if (!$validacaoParceiroSite->executaValidacao($objValidacao)) {
            throw new \Exception($validacaoParceiroSite->msg);
        }

        return $parceiroSiteDAO->getParceiroSiteCampanhaAtiva($parceiroId);
    }

    /**
     * Retorna os produtos da vinculados a campanha do parceiro
     *
     * @param int $parceiroId
     *
     * @return array
     * @throws ValidacaoException
     */
    public function getProdutosParceiroByCampanha($parceiroId) {
        $obj = (object)array(
            'parceiroId' => $parceiroId
        );
        $regras = array(
            'parceiroId' => array('required', 'int')
        );
        $validador = new Validacao($regras);
        if (!$validador->executaValidacao($obj)) {
            throw  new ValidacaoException($validador->msg, 2);
        }

        $parceiroSite = $this->getParceiroSiteById($parceiroId);

        $parceiroDAO = new ParceiroSiteDAO();
        $produtoCodigo = $parceiroDAO->getParceiroSiteCampanhaAtiva($parceiroId);
        if (empty($produtoCodigo)) {
            return array();
        }
        $produtoBO = new ProdutoBO();

        return $produtoBO->getProdutoFull($parceiroSite->siteBase, $produtoCodigo, null, $parceiroSite->parceiroId, true);
    }

    /**
     * Busca todos os parceiros
     *
     * @return \App\Model\ParceiroSite[]
     */
    public function buscarTodosParceiros() {
        $parceiroSiteDAO = new ParceiroSiteDAO();

        return $parceiroSiteDAO->buscarTodosParceiros();
    }
}