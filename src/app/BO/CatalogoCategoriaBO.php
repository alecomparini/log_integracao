<?php

namespace App\BO;

use App\Exceptions\ValidacaoException;
use App\DAO\CatalogoCategoriaDAO;
use App\Helper\Validacao;

/**
 * Description of CatalogoCategoriaBO
 *
 * @author suportesoftbox
 */
class CatalogoCategoriaBO extends BaseBO {
    
    public function filtrarCategoriaByCatalogo($parceiroId, $catalogoId) {
        $obj = (object)array(
           'parceiroId' => $parceiroId,
           'catalogoId' => $catalogoId
        );
        $regras = array(
            'parceiroId' => array('required','int'),
            'catalogoId' => array('required','int')
        );
        
        $validador = new Validacao($regras);   
        if (!$validador->executaValidacao($obj)) {
            throw  new ValidacaoException($validador->msg, 2);
        }
        
        $catalogoCategoriaDao = new CatalogoCategoriaDAO();
        $categorias = $catalogoCategoriaDao->getCatalogoCategoriaByCatalogo($catalogoId);
        $findSubCategoria = array();
        $findCategoria = array();
        
        foreach ($categorias as &$categoria) {
            $categoria->setHidden(array("categorias","nome","status","statusCampanha","campanha","campanhaId","tipoFrete","desconto","fretePonderado","localidades"));
            if (!empty($categoria->subCategoriaId)) {
                array_push($findSubCategoria, $categoria->subCategoriaId);
            } else {
                array_push($findCategoria, $categoria->categoriaId);
            }
        }
        
        $categoriaBO = new CategoriaBO();
        $subCategorias = $categoriaBO->buscarSubCategorias($findSubCategoria);
        $categorias = $categoriaBO->buscarCategorias($findCategoria);
        
        foreach ($categorias as &$categoria) {
            if (!empty($categoria->subCategoriaId) && !empty($subCategorias[$categoria->subCategoriaId])) {
                $objSubCategoria = $subCategorias[$categoria->subCategoriaId];
                $this->setarAtributos($objSubCategoria, $categoria);
            } elseif (!empty($categoria->categoriaId) && !empty($categorias[$categoria->categoriaId])) {
                $objCategoria = $categorias[$categoria->categoriaId];
                $this->setarAtributos($objCategoria, $categoria);
                $categoria->subCategoria = null;
            }
        }
        return $categorias;
    }
    
    public function getCatalogoByParceiro($parceiroId) {
        $obj = (object)array(
           'parceiroId' => $parceiroId,
        );
        $regras = array(
            'parceiroId' => array('required','int'),
        );
        
        $validador = new Validacao($regras);   
        if (!$validador->executaValidacao($obj)) {
            throw  new ValidacaoException($validador->msg, 2);
        }
        
        $catalogoCategoriaDao = new CatalogoCategoriaDAO();
        return $catalogoCategoriaDao->getCampanhaByParceiro($parceiroId);
    }

    public function getCatalogoCategoriaByParceiro($parceiroId) {
        $obj = (object) array(
           'parceiroId' => $parceiroId,
        );
        $regras = array(
            'parceiroId' => array('required','int'),
        );

        $validador = new Validacao($regras);
        if (!$validador->executaValidacao($obj)) {
            throw  new ValidacaoException($validador->msg, 2);
        }

        $catalogoCategoriaDao = new CatalogoCategoriaDAO();
        $catalogoCategorias = $catalogoCategoriaDao->getCatalogoCategoriaByParceiro($parceiroId);

	if (!empty($catalogoCategorias)) {
	    $novoArray = array();
	    foreach ($catalogoCategorias as $itemCatalogoCategoria) {
		$key = (empty($itemCatalogoCategoria->subCategoriaId)) ? $itemCatalogoCategoria->categoriaId : $itemCatalogoCategoria->subCategoriaId;
		$novoArray[$key] = $itemCatalogoCategoria;
	    }
	    $catalogoCategorias = $novoArray;
	}

	return (array) $catalogoCategorias;

    }
    
    private function setarAtributos($objetoDe, &$objetoPara) {
        foreach ($objetoDe as $key => $valor) {
            $objetoPara->$key = $valor;
        }
    }
}
