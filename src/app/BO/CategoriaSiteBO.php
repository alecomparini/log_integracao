<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 06/01/2016
 * Time: 15:07
 */

namespace App\BO;

use App\DAO\CategoriaSiteDAO;
use App\Helper\Validacao;

/**
 * Class CategoriaSiteBO
 * @package App\BO
 */
class CategoriaSiteBO extends BaseBO {

    /**
     * @param $idCategoriaPai
     * @param $siteId
     * @return \App\Model\Categoria[]
     * @throws \Exception
     */
    public function buscaCategoriasFilho($idCategoriaPai, $siteId) {

        if (!is_array($idCategoriaPai)) {
            $idCategoriaPai = array($idCategoriaPai);
        }

        $obj = array(
            'idCategoriaPai' => $idCategoriaPai,
            'siteId' => $siteId
        );
        $regras = array(
            'idCategoriaPai' => array('int_array'),
            'siteId' => array('int')
        );
        $validador = new Validacao($regras);
        if (!$validador->executaValidacao($obj)) {
            throw new \Exception("Erro de validação: " . $validador->msg, 2);
        }

        $categoriaSiteDAO = new CategoriaSiteDAO();
        return $categoriaSiteDAO->buscaCategoriasFilho($idCategoriaPai, $siteId);
    }
}