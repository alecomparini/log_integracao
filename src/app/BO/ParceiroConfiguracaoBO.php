<?php
/**
 * Created by PhpStorm.
 * User: root
 * Date: 15-02-2016
 * Time: 13:45
 */

namespace App\BO;

use App\DAO\ParceiroConfiguracaoDAO;
use App\Exceptions\ValidacaoException;
use App\Helper\Validacao;
use App\Model\ParceiroConfiguracaoBag;

/**
 * Class ParceiroConfiguracaoBO
 * @package App\BO
 */
class ParceiroConfiguracaoBO extends BaseBO {
    private $parceiroConfiguracaoDAO;

    /**
     * ParceiroConfiguracaoBO constructor.
     */
    public function __construct() {
        $this->parceiroConfiguracaoDAO = new ParceiroConfiguracaoDAO();
    }

    /**
     * Obtém as configurações de um parceiro
     * @param $parceiroId
     * @param array $configuracoes
     * @param bool $single
     * @return \App\Model\ParceiroConfiguracao|\App\Model\ParceiroConfiguracao[]
     * @throws ValidacaoException
     */
    public function getConfiguracoes($parceiroId, $configuracoes = array(), $single = false) {
        if (!is_array($configuracoes)) {
            $configuracoes = array($configuracoes);
        }
        $obj = (object) array(
            'parceiroId' => $parceiroId,
            'configuracoes' => $configuracoes
        );

        $configuracaoReflection = new \ReflectionClass('\App\Model\Configuracao');
        $configuracaoConsts = array_keys($configuracaoReflection->getConstants());
        $regras = array(
            'parceiroId' => array('required', 'int'),
            'configuracoes' => array('required', 'array' => array('permitido' => $configuracaoConsts)),
        );

        $validacao = new Validacao($regras);

        if (!$validacao->executaValidacao($obj)) {
            throw new ValidacaoException($validacao->msg, 2);
        }

        $response = $this->parceiroConfiguracaoDAO->getConfiguracoes($parceiroId, $configuracoes);
        //Retorna apenas um objeto (e não um array) no caso onde single = true
        if ($single) {
            if (count($response) > 0) {
                $response = $response[0];

            } else {
                $response = null;
            }
        }
        return $response;
    }
}