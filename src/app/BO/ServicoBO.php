<?php

namespace App\BO;

use App\DAO\NotificacaoDAO;
use App\DAO\ParceiroConfiguracaoDAO;
use App\DAO\ParceiroDAO;
use App\DAO\PedidoParceiroDAO;
use App\DAO\ProdutoDAO;
use App\Exceptions\BusinessException;
use App\Exceptions\ValidacaoException;
use App\Helper\Validacao;
use App\Model\NotificacaoEstornoPontos;
use App\DAO\NotificacaoEstornoPontosDAO;
use App\Model\Configuracao;
use App\Model\NotificacaoParceiro;
use App\Model\Response;
use App\Strategy\CriarNotificacao\CriarNotificacaoStrategyFactory;
use App\Strategy\BuscarEstoque\BuscarEstoqueStrategyFactory;
use App\BO\ParceiroConfiguracaoBO;

/**
 * Class ServicoBO
 *
 * Classe responsável por executar as lógicas dos serviços internos disponibilizados para outros
 * sistemas da MV.COM
 *
 * @package App\BO
 * @author  Cristiano Gomes <cristianogomes@softbox.com.br>
 */
class ServicoBO extends BaseBO {
    /**
     * Recebe a lista de produtos que sofreram alterações e registra notificações para serem enviadas aos
     * parceiros cadastrados
     *
     * @param     $produtos
     * @param int $tipoNotificacao 1: Estoque; 2: Preco
     *
     * @return bool
     * @throws \Exception
     */
    public function setNotificacao($produtos, $tipoNotificacao) {
        $obj = (object)array('produtos' => $produtos, 'tipoNotificacao' => $tipoNotificacao);

        $regras = array('produtos' => array('notempty', 'required', 'array'), 'tipoNotificacao' => array('int', 'notempty', 'required'));
        $validador = new Validacao($regras);

        if (!$validador->executaValidacao($obj)) {
            throw new ValidacaoException($validador->msg, 2);
        }

        if (!in_array($tipoNotificacao, array(1, 2), false)) {
            throw new BusinessException('', 22);
        }

        $produtoDao = new ProdutoDAO();
        $produtos = (array)$produtoDao->getCodsById((array)$produtos);

        if (count($produtos) === 0) {
            throw new BusinessException('Nenhum produto encontrado', 21);
        }

        $parceiroDao = new ParceiroDAO();
        $parceiros = $parceiroDao->getParceiroAtivo();
        $pspBO = new ParceiroSitePermissaoBO();
        $produtoBO = new ProdutoBO();
        $notificacoes = array();

        $idsIgnorarRegraPreco = array("3", "5", "6", "7", "9", "10");

        try {
            foreach ($parceiros as $parceiro) {
                foreach ($produtos as $produto) {
                    $teste = $pspBO->verificaPermissaoProduto($produto->Codigo, $parceiro->parceiroId);
                    if ($teste) {

                        $pcoConfBO = new ParceiroConfiguracaoBO();
                        $buscarEstoqueStrategy = $pcoConfBO->getConfiguracoes($parceiro->parceiroId, Configuracao::ESTRATEGIA_BUSCA_ESTOQUE_API, true)->valor;

                        $strategy = BuscarEstoqueStrategyFactory::create($buscarEstoqueStrategy);
                        $produtoPreco = $strategy->buscarEstoque($parceiro->parceiroId, array($produto->Codigo));
                        //Verifica preço do produto
                        //$produtoPreco = $produtoBO->getEstoquePlus($parceiro->parceiroId, array($produto->Codigo));
                        if (!$strategy->precisaVerificarPreco() || (in_array($parceiro->parceiroId, $idsIgnorarRegraPreco, false) || (count($produtoPreco) > 0 && $produtoPreco[0]->PrecoPor > 0))) {
                            $tmp = array('parceiroId' => $parceiro->parceiroId, 'siteId' => $parceiro->SiteId, 'produtoCodigo' => $produto->Codigo);
                            $notificacoes[] = $tmp;
                        }
                    }
                }
            }

        } catch (\Exception $e) {
            throw new BusinessException($e->getMessage(), 2);
        }

        if (!count($notificacoes)) {
            return false;
        }

        try {
            $notificacaoDAO = new NotificacaoDAO();

            foreach ($notificacoes as $notificacao) {
                $notificacaoDAO->setNotificacao($notificacao, $tipoNotificacao);
            }

        } catch (\Exception $e) {
            throw new \Exception($e->getMessage(), 2);
        }

        return true;
    }

    public function setNotificacao2($produtos, $tipoNotificacao) {
        $obj = (object)array('produtos' => $produtos, 'tipoNotificacao' => $tipoNotificacao);

        $regras = array('produtos' => array('notempty', 'required', 'array'), 'tipoNotificacao' => array('int', 'notempty', 'required'));
        $validador = new Validacao($regras);

        if (!$validador->executaValidacao($obj)) {
            throw new ValidacaoException($validador->msg, Response::ERRO_VALIDACAO);
        }

        if (!in_array($tipoNotificacao, array(1, 2))) {
            throw new BusinessException('Campo [tipo_notificacao] deve ser "1" (estoque) ou "2" (preço)', Response::ERRO_VALIDACAO);
        }

        $produtoDao = new ProdutoDAO();
        $produtos = (array)$produtoDao->getCodsById((array)$produtos);

        if (count($produtos) === 0) {
            return false;
        }

        $parceiroDao = new ParceiroDAO();
        $parceiros = $parceiroDao->getParceiroAtivo();
        $configuracaoBO = new ParceiroConfiguracaoBO();
        try {
            foreach ($parceiros as $parceiro) {
                $configuracao = $configuracaoBO->getConfiguracoes($parceiro->parceiroId, Configuracao::ESTRATEGIA_SET_NOTIFICACAO, true);
                $strategy = CriarNotificacaoStrategyFactory::create($configuracao->valor);
                if ($tipoNotificacao === NotificacaoParceiro::NOTIFICACAO_ESTOQUE) {
                    $strategy->criarNotificacaoEstoque($parceiro->parceiroId, $produtos);
                }
            }

        } catch (\Exception $e) {
            throw new BusinessException($e->getMessage(), 2);
        }

        return true;
    }
    
    /**
     * Recebe a lista de produtos que sofreram alterações e registra notificações para serem enviadas aos
     * parceiros cadastrados
     *
     * @param $produtos
     * @param int   $tipoNotificacao    1: Estoque; 2: Preco
     * @return bool
     * @throws \Exception
     */
    public function setNotificacaoEstornoPonto($notificacoesPonto) {
        $obj = (object) array(
            'notificacoesPonto' => $notificacoesPonto,
        );

        $regras = array(
            'notificacoesPonto' => array('notempty', 'required', 'array')
        );
        $validador = new Validacao($regras);

        if (!$validador->executaValidacao($obj)) {
            throw new \Exception($validador->msg, 2);
        }

        $notificacaoDAO = new NotificacaoEstornoPontosDAO();
        try {
            $notificacaoDAO->beginTransaction();
            foreach ($notificacoesPonto as $notificacao) { 
                $regras = array(
                    'parceiroId' => array('int'),
                    'pedidoParceiroId' => array('str' => array('min' => '11','max' => '14')),
                    'clienteDocumento' => array('str' => array('min' => '11','max' => '14')),
                    'pontos' => array('int')
                );
                $validador = new Validacao($regras);
                if (!$validador->executaValidacao($notificacao)) {
                    throw new \Exception($validador->msg, 2);
                }   
                $notificacaoDAO->setNotificacaoEstornoPontos($notificacao->parceiroPedidoId, $notificacao->parceiroId, $notificacao->clienteDocumento, $notificacao->pontos);
            }
            $notificacaoDAO->commit();
        } catch (\Exception $e) {
            $notificacaoDAO->rollback();
            throw new \Exception($e->getMessage(), 2);
        }

        return true;
    }

    /**
     * Recebe a lista de pedidos que receberam atualização de tracking no gateway e registra para os seus devidos
     * parceiros que possuem URL de notificação de tracking
     *
     * @param $pedidos
     * @return bool
     * @throws \Exception
     */
    //public function setNotificacaoTracking($pedidos) {
    //    $obj = (object) array(
    //        'pedidos' => $pedidos
    //    );
    //
    //    $regras = array(
    //        'pedidos' => array('notempty', 'required', 'array'),
    //    );
    //
    //    $validador = new Validacao($regras);
    //
    //    if (!$validador->executaValidacao($obj)) {
    //        throw new \Exception($validador->msg, 2);
    //    }
    //
    //    $pedidoParceiroDao = new PedidoParceiroDAO();
    //    $dados = $pedidoParceiroDao->getDadosPedidoById($pedidos);
    //    $config = new ParceiroConfiguracaoDAO();
    //    $notificacaoDAO = new NotificacaoDAO();
    //
    //    try {
    //
    //        $notificacoes = array();
    //        foreach ($dados as $pedidoInfo) {
    //            //Se houver url de envio de tracking para o parceiro, registramos a notificação
    //            $pConf = $config->getConfiguracoes($pedidoInfo->ParceiroId, array(Configuracao::URL_NOTIFICACAO_TRACKING));
    //            $pConf = $pConf[0];
    //
    //            if ($pConf->valor) {
    //                $teste = $notificacaoDAO->getNotificacaoExistentePedido($pedidoInfo->ParceiroPedidoId);
    //
    //                if ($teste->total == 0) {
    //                    $tmp = array(
    //                        'parceiroId' => $pedidoInfo->ParceiroId,
    //                        'siteId' => $pedidoInfo->SiteId,
    //                        'pedidoParceiroId' => $pedidoInfo->ParceiroPedidoId
    //                    );
    //                    $notificacoes[] = $tmp;
    //                }
    //            }
    //        }
    //
    //    } catch (\Exception $e) {
    //        throw new \Exception($e->getMessage(), 2);
    //    }
    //
    //    if (!count($notificacoes)) {
    //        throw new \Exception('Nenhuma notificação gerada.', 2);
    //    }
    //
    //    try {
    //        $notificacaoDAO->beginTransaction();
    //
    //        foreach ($notificacoes as $notificacao) {
    //            $notificacaoDAO->setNotificacaoTracking($notificacao['parceiroId'], $notificacao['siteId'], $notificacao['pedidoParceiroId']);
    //        }
    //
    //        $notificacaoDAO->commit();
    //    } catch (\Exception $e) {
    //        $notificacaoDAO->rollback();
    //        throw new \Exception($e->getMessage(), 2);
    //    }
    //
    //    return true;
    //}

}
