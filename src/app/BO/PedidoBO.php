<?php

namespace App\BO;

use App\Core\Config;
use App\Core\Logger;
use App\DAO\AttachmentDao;
use App\DAO\DadosImportacaoDAO;
use App\DAO\EstabelecimentoDAO;
use App\DAO\EstoqueEstabelecimentoDAO;
use App\DAO\GarantiaDAO;
use App\DAO\PedidoDAO;
use App\DAO\PedidoGarantiaDAO;
use App\DAO\PedidoParceiroDAO;
use App\DAO\PedidoSeguroDAO;
use App\DAO\ProdutoDAO;
use App\DAO\SeguroDAO;
use App\Exceptions\BusinessException;
use App\Exceptions\DatabaseException;
use App\Exceptions\ValidacaoAttachmentsException;
use App\Exceptions\ValidacaoException;
use App\Helper\Utils;
use App\Helper\Validacao;
use App\Model\Configuracao;
use App\Model\Pagamento;
use App\Model\Parceiro;
use App\Model\ParceiroSite;
use App\Model\Pedido;
use App\Model\PedidoGarantia;
use App\Model\PedidoProduto;
use App\Model\PedidoSeguro;
use App\Model\Response;
use App\Model\V2\StatusExportacaoPedido;
use App\Validator\Pedido\PedidoValidator;
use App\Validator\Pedido\Regras\PedidoValidatorRuleFactory;
use DateInterval;
use DateTime;

/**
 * Class PedidoBO
 *
 * @package App\BO
 */
class PedidoBO {
    /**
     * Faz a triagem de qual setPedido será utilizado, online ou pré-pedido de acordo
     *
     * @param \App\Model\Pedido $pedido
     *
     * @return bool
     * @throws \InvalidArgumentException
     * @throws \App\Exceptions\DatabaseException
     *
     * @throws \App\Exceptions\BusinessException
     * @throws \App\Exceptions\ValidacaoAttachmentsException
     * @throws \App\Exceptions\ValidacaoException
     * @throws \Exception
     */
    public function setPedido(Pedido $pedido) {
        // filtros do pedido
        $pedido->clientePjInscEstadual = Utils::soNumeros($pedido->clientePjInscEstadual);
        $pedido->clientePjCNPJ = Utils::soNumeros($pedido->clientePjCNPJ);
        $pedido->clientePfNome = Utils::removeEspeciais($pedido->clientePfNome);
        $pedido->clientePjNomeFantasia = Utils::removeEspeciais($pedido->clientePjNomeFantasia);
        $pedido->clientePfSobrenome = Utils::removeEspeciais($pedido->clientePfSobrenome);
        $pedido->clientePjRazaoSocial = Utils::removeEspeciais($pedido->clientePjRazaoSocial);
        $pedido->endCobrancaBairro = Utils::removeEspeciais($pedido->endCobrancaBairro);
        $pedido->endCobrancaComplemento = Utils::removeEspeciais($pedido->endCobrancaComplemento);
        $pedido->endCobrancaDestinatario = Utils::removeEspeciais($pedido->endCobrancaDestinatario);
        $pedido->prazoCd = Utils::soNumeros($pedido->prazoCd);
        $pedido->endEntregaCep = Utils::soNumeros($pedido->endEntregaCep);
        $pedido->endCobrancaCep = Utils::soNumeros($pedido->endCobrancaCep);

        if (!isset($pedido->campanhaId) || !is_numeric($pedido->campanhaId)) {
            $pedido->campanhaId = 0;
        }

        // InterfaceId = 2 -> Interface de Pedido
        $pedido->tipoInterfaceId = 2;

        // Validador
        $validadorPedido = new PedidoValidator();
        $validadorPedido->adicionarRegra(PedidoValidatorRuleFactory::create('campos'));
        $validadorPedido->adicionarRegra(PedidoValidatorRuleFactory::create('pedidoProduto'));
        $validadorPedido->adicionarRegra(PedidoValidatorRuleFactory::create('pedidoPagamento'));
        $validadorPedido->adicionarRegra(PedidoValidatorRuleFactory::create('estoque'));
        $validadorPedido->adicionarRegra(PedidoValidatorRuleFactory::create('prazoCd'));

        $validadorPedido->executarValidacao($pedido);

        $confBO = new ParceiroConfiguracaoBO();
        $aceitaPrePedido = (bool)$confBO->getConfiguracoes($pedido->parceiroId, Configuracao::ACEITA_PRE_PEDIDO, true)->valor;

        // Verifica se algum dos pagamentos envolve boleto, caso sim, NÃO ENTRAR EM PRÉ-PEDIDO
        foreach ($pedido->pagamento as $pagamento) {
            if (Pagamento::BOLETO_BANCARIO === (int)$pagamento->meioPagamentoId) {
                $aceitaPrePedido = false;
                break;
            }
        }

        if ($aceitaPrePedido) {
            return $this->setPrePedido($pedido);
        }

        return $this->setPedidoOnline($pedido);
    }

    /**
     * @return \App\Model\Pedido[]
     */
    public function buscarPrePedidos() {
        $pedidoDAO = new PedidoDAO();
        return $pedidoDAO->buscarPrePedidos();
    }

    /**
     * @return \App\Model\Pedido[]
     */
    public function buscarPedidosErroExportacao() {
        $pedidoDAO = new PedidoDAO();
        return $pedidoDAO->buscarPedidosErroExportacao();
    }

    public function cancelarPrePedido(Pedido $pedido, $motivoCancelamento, $usuarioIdCancelamento = null) {
        if ((int)$pedido->statusExportacao !== StatusExportacaoPedido::PRE_PEDIDO_ERRO) {
            throw new BusinessException('Pedido não se encontra no status de Pre-Pedido com erro', Response::ERRO_VALIDACAO);
        }

        $pedidoDAO = new PedidoDAO();
        try {
            $pedidoDAO->beginTransaction();
            // Muda o status para 6
            $this->defineStatusExportacao($pedido, StatusExportacaoPedido::PRE_PEDIDO_CANCELADO);
            $this->defineMotivoCancelamento($pedido, $motivoCancelamento, $usuarioIdCancelamento);

            // Remove reserva
            foreach ($pedido->pedidoProduto as $pedidoProduto) {
                $pedidoProduto->quantidade *= -1; // Inverte o valor
                $this->setReservaEstoqueProduto($pedidoProduto);
            }
            $pedidoDAO->commit();
        } catch (DatabaseException $dbe) {
            $pedidoDAO->rollback();
            throw new BusinessException('Não foi possível cancelar o pedido', Response::ERRO, $dbe);
        }
    }

    private function defineMotivoCancelamento(Pedido $pedido, $motivoCancelamento, $usuarioIdCancelamento) {
        $pedidoDAO = new PedidoDAO();
        $pedidoDAO->defineMotivoCancelamento($pedido->dadosImportacaoId, $motivoCancelamento, $usuarioIdCancelamento);
    }

    public function buscarPedidoPorDadosImportacaoId($dadosImportacaoId) {
        $pedidoDAO = new PedidoDAO();
        return $pedidoDAO->buscarPedidoPorDadosImportacaoId($dadosImportacaoId);
    }

    /**
     * @param \App\Model\Pedido $pedido
     *
     * @throws \Exception
     */
    public function processarPedido(Pedido $pedido) {
        Logger::getInstance()->log('processarPedido', array('dadosImportacaoId' => $pedido->dadosImportacaoId));
        try {
            $pedidoValidator = new PedidoValidator();
            $validacoes = array(
                'valorProdutos', 'valorTotal', 'valorFrete', 'valorPagamento',
                'pedidoProduto', 'pedidoPagamento', 'itemDuplicado', 'entregaAgendada',
                'emailEmUso', 'parceiro', 'integrador', 'permissaoVendaItens', 'estoque',
                'permissaoMeioPagamento',
                'pedidoSeguro', 'pedidoGarantia',
                'sixPack'
            );
            foreach ($validacoes as $validacao) {
                $pedidoValidator->adicionarRegra(PedidoValidatorRuleFactory::create($validacao));
            }

            $pedidoValidator->executarValidacao($pedido);
            $this->atualizarPedidoProdutoProdutoId($pedido);
            $this->defineStatusExportacao($pedido, StatusExportacaoPedido::AGUARDANDO_IMPORTACAO);

        } catch (\Exception $e) {
            Logger::getInstance()->exception($e, array('pedido' => $pedido));
            $this->defineStatusExportacao($pedido, StatusExportacaoPedido::PRE_PEDIDO_ERRO);
            $this->defineErroProcessamento($pedido, $e->getMessage());
            throw $e;
        }
    }

    private function atualizarPedidoProdutoProdutoId(Pedido $pedido) {
        $produtoDAO = new ProdutoDAO();

        foreach ($pedido->pedidoProduto as $pedidoProduto) {
            $produtosId = $produtoDAO->getCodProdutos(array($pedidoProduto->codigoProduto));

            if (0 === count($produtosId)) {
                throw new BusinessException("Produto não encontrado com o código ({$pedidoProduto->codigoProduto})", Response::ERRO_VALIDACAO);
            }

            // Isso é muito feio, setar campos dentro da validação, mas por enquanto vai ter que ser assim
            $pedidoProduto->produtoId = $produtosId[0]->produtoId;
            $produtoDAO->atualizarPedidoProdutoProdutoId($pedido->dadosImportacaoId, $pedidoProduto, $produtosId[0]->produtoId);
        }
    }

    /**
     * @param \App\Model\Pedido $pedido
     * @param                   $statusExportacao
     *
     * @return bool
     */
    public function defineStatusExportacao(Pedido $pedido, $statusExportacao) {
        $pedidoDAO = new PedidoDAO();
        return $pedidoDAO->defineStatusExportacao($pedido, $statusExportacao);
    }

    public function defineErroProcessamento(Pedido $pedido, $mensagem) {
        $pedidoDAO = new PedidoDAO();
        return $pedidoDAO->defineErroProcessamento($pedido, $mensagem);
    }

    /**
     * @param \App\Model\ParceiroSite $parceiroSite
     *
     * @return \App\Model\Pedido[]
     * @throws \App\Exceptions\ValidacaoException
     */
    public function buscarPedidosComErroCancelamento(ParceiroSite $parceiroSite) {
        $configBO = new ParceiroConfiguracaoBO();
        $minutosCancelamento = (int)$configBO->getConfiguracoes($parceiroSite->parceiroId, Configuracao::MINUTOS_CANCELAMENTO_PRE_PEDIDO, true)->valor;
        $dataCorte = new DateTime('now');
        $pedidoDAO = new PedidoDAO();

        $dataCorte->sub(new DateInterval("PT{$minutosCancelamento}M"));

        return $pedidoDAO->buscarPedidosPassivelCancelamento($parceiroSite->parceiroSiteId, $dataCorte->format("Y-m-d H:i:s"));
    }

    /**
     * Salva o pedido no banco de dados para processamento posterior.
     *
     * @param \App\Model\Pedido $pedido
     *
     * @throws \InvalidArgumentException
     * @throws \App\Exceptions\BusinessException
     * @throws \App\Exceptions\DatabaseException
     */
    public function setPrePedido(Pedido $pedido) {

        $key = sha1(sprintf('setPedido_ParceiroId_%d_PedidoParceiro_%s', $pedido->parceiroId, $pedido->parceiroPedidoId));
        $lock = DBLockBO::getLock($key, 2);

        // Validador
        PedidoValidatorRuleFactory::create('importacaoDuplicada')->validar($pedido);

        if (1 !== (int)$lock->ok) {
            throw new BusinessException('Já existe um pedido com a mesma chave de internalização. Tente novamente em alguns segundos.', Response::PEDIDO_SEM_LOCK_EXCLUSIVO);
        }

        $pedidoDAO = new PedidoDAO();
        $pedidoDAO->beginTransaction();
        try {
            // Converte os pontos em valor
            $valorTotalPago = 0;
            foreach ($pedido->pagamento as $pagamento) {
                $valorTotalPago += $pagamento->valorPago;
                if ((int)$pagamento->meioPagamentoId === Pagamento::PONTOS) {
                    $this->converterPontoEmValor($pagamento, $pedido->valorTotal, $valorTotalPago);
                }
            }

            // Busca os produtos ID, porque não tem como fazer reserva sem eles, logo é regra forte
            // e impactante na hora de descer pedido, mesmo com prePedido.
            $this->definirProdutosId($pedido);

            // Salva o pedido
            $pedidoDAO->salvarPedido($pedido, true);

            // Reservar estoque dos itens
            foreach ($pedido->pedidoProduto as $pedidoProduto) {
                $this->setReservaEstoqueProduto($pedidoProduto);
            }

            $pedidoDAO->commit();
            DBLockBO::releaseLock($key);
        } catch (\Exception $e) {
            $pedidoDAO->rollback();
            DBLockBO::releaseLock($key);
            throw new BusinessException('Erro ao salvar no banco de dados com a seguinte mensagem: ' . $e->getMessage(), Response::FALHA_SALVAR_PEDIDO, $e);
        }
    }

    private function definirProdutosId(Pedido $pedido) {
        $produtoDAO = new ProdutoDAO();
        foreach ($pedido->pedidoProduto as $pedidoProduto) {
            $produtoId = $produtoDAO->getCodProdutos(array($pedidoProduto->codigoProduto));
            if (count($produtoId) === 0) {
                throw new BusinessException('Código de produto informado não existe', Response::ERRO_VALIDACAO);
            }

            $pedidoProduto->produtoId = $produtoId[0]->produtoId;
        }
    }

    /**
     * Método que realizar o envio do pedido ONLINE (ou seja, é o método que chegou, já processa e da o retorno na hora)
     * Deverá ser utilizado por parceiros que configurar o pré-pedido como falso ou então para os pedidos de boleto.
     *
     * @param \App\Model\Pedido $pedido
     *
     * @return bool
     *
     * @throws \App\Exceptions\BusinessException
     * @throws \App\Exceptions\ValidacaoAttachmentsException
     * @throws \App\Exceptions\ValidacaoException
     * @throws \Exception
     */
    public function setPedidoOnline(Pedido $pedido) {
        $key = sha1(sprintf('setPedido_ParceiroId_%d_PedidoParceiro_%s', $pedido->parceiroId, $pedido->parceiroPedidoId));
        $lock = DBLockBO::getLock($key, 0);
        if ($lock->ok != 1) {
            throw new BusinessException('Já existe um pedido com a mesma chave de internalização. Tente novamente em alguns segundos.', Response::PEDIDO_SEM_LOCK_EXCLUSIVO);
        }

        // filtros do pedido
        $pedido->clientePjInscEstadual = preg_replace('/\D/i', '', $pedido->clientePjInscEstadual); // Remoção de tudo que não é número

        // cast do parceiroPedidoId pra string
        $pedido->parceiroPedidoId = (string)$pedido->parceiroPedidoId;

        // InterfaceId = 2 -> Interface de Pedido
        $pedido->tipoInterfaceId = 2;
        $regras = array(
            'parceiroId' => array('notempty', 'int'),
            'siteId' => array('notempty', 'int'),
            'parceiroIp' => array('notemptynorwhitespaces', 'str' => array('min' => '7', 'max' => '15')),
            'clienteEmail' => array('notemptynorwhitespaces', 'str' => array('min' => '5', 'max' => '60')),
            'clienteTipo' => array('str' => array('tipocliente' => true)),
            'clientePfNome' => array('pf', 'notemptynorwhitespaces', 'str' => array('min' => '2', 'max' => '30')),
            'clientePfSobrenome' => array('pf', 'notemptynorwhitespaces', 'str' => array('min' => '2', 'max' => '30')),
            'clientePfCPF' => array('pf', 'notemptynorwhitespaces', 'str' => array('max' => '11', 'min' => '11')),
            'clientePfDataNascimento' => array('pf', 'str' => array('max' => '10', 'min' => '10'), 'date'),
            'clientePfSexo' => array('pf', 'str' => array('sexo' => true)),
            'clientePjRazaoSocial' => array('pj', 'notempty', 'str' => array('min' => '5', 'max' => '100')),
            'clientePjNomeFantasia' => array('pj', 'notempty', 'str' => array('min' => '5', 'max' => '100')),
            'clientePjCNPJ' => array('pj', 'notempty', 'str' => array('max' => '18', 'min' => '11')),
            'clientePjIsento' => array('pj', 'bool'),
            'clientePjInscEstadual' => array('pj', 'inscEstadual', 'str'),
            'clientePjTelefone' => array('pj', 'notnull', 'notempty', 'str' => array('max' => '14', 'min' => '10')),
            //'clientePjSite' => array('pj', 'str' => array('max' => '150')),
            'clientePjRamoAtividade' => array('pj', 'int' => array('ramoAtividade' => true)),
            'endCobrancaTipo' => array('str' => array('tipoEndCobranca' => true)),
            'endCobrancaDestinatario' => array('notemptynorwhitespaces', 'str' => array('max' => '100')),
            'endCobrancaCep' => array('cep', 'str' => array('max' => '8', 'min' => '8')),
            'endCobrancaEndereco' => array('notemptynorwhitespaces', 'str' => array('min' => '3', 'max' => '100')),
            'endCobrancaNumero' => array('int'),
            'endCobrancaComplemento' => array('str' => array('max' => '45')),
            'endCobrancaBairro' => array('notemptynorwhitespaces', 'str' => array('min' => '2', 'max' => '50')),
            'endCobrancaCidade' => array('notemptynorwhitespaces', 'str' => array('min' => '2', 'max' => '50')),
            'endCobrancaEstado' => array('notemptynorwhitespaces', 'str' => array('max' => '2', 'min' => '2')),
            'endCobrancaTelefone1' => array('notemptynorwhitespaces', 'str' => array('max' => '13', 'min' => '9')),
            'endCobrancaTelefone2' => array('str' => array('max' => '13', 'min' => '9', 'acceptNull' => true)),
            'endCobrancaCelular' => array('str' => array('max' => '13', 'min' => '9', 'acceptNull' => true)),
            'endCobrancaReferencia' => array('tiposaceitos' => array('str', 'null')),
            'endEntregaTipo' => array('notemptynorwhitespaces', 'str' => array('tipoEndCobranca' => true)),
            'endEntregaDestinatario' => array('notemptynorwhitespaces', 'str' => array('min' => '3', 'max' => '100')),
            'endEntregaCep' => array('cep', 'str' => array('max' => '8', 'min' => '8')),
            'endEntregaEndereco' => array('notemptynorwhitespaces', 'str' => array('min' => '3', 'max' => '100')),
            'endEntregaNumero' => array('int'),
            'endEntregaComplemento' => array('str' => array('max' => '45')),
            'endEntregaBairro' => array('notemptynorwhitespaces', 'str' => array('min' => '2', 'max' => '50')),
            'endEntregaCidade' => array('notemptynorwhitespaces', 'str' => array('min' => '2', 'max' => '50')),
            'endEntregaEstado' => array('notemptynorwhitespaces', 'str' => array('max' => '2', 'min' => '2')),
            'endEntregaTelefone1' => array('notemptynorwhitespaces', 'str' => array('max' => '13', 'min' => '9')),
            'endEntregaTelefone2' => array('str' => array('max' => '13', 'min' => '9', 'acceptNull' => true)),
            'endEntregaCelular' => array('str' => array('max' => '13', 'min' => '9', 'acceptNull' => true)),
            'endEntregaReferencia'  => array('tiposaceitos' => array('str', 'null')),
            'parceiroPedidoId' => array('notemptynorwhitespaces', 'str' => array('min' => '1', 'max' => '25')),
            'valorTotal' => array('notempty', 'float' => array('notEmptyFloat' => true)),
            'valorTotalFrete' => array('float' => array('notNegativeFloat' => true)),
            'valorTotalProdutos' => array('notempty', 'float' => array('notEmptyFloat' => true)),
            'valorJuros' => array(),
            'pedidoProduto' => array('PP'),
            'pagamento' => array('PAG'),
            'dataEntrega' => array('str' => array('max' => '10', 'min' => '10')),
            'turnoEntrega' => array('int'),
            'valorEntrega' => array('required', 'float'),
            'vendedorToken' => array('tiposaceitos' => array('str' => array('VendedorToken' => true), 'null')),
            'prazoCd' => array('tiposaceitos' => array('int', 'null', 'empty')),
        );

        //limpa o CNPJ
        $pedido->clientePjCNPJ = Utils::soNumeros($pedido->clientePjCNPJ);

        $pedido->clientePfNome = Utils::removeEspeciais($pedido->clientePfNome);
        $pedido->clientePjNomeFantasia = Utils::removeEspeciais($pedido->clientePjNomeFantasia);
        $pedido->clientePfSobrenome = Utils::removeEspeciais($pedido->clientePfSobrenome);
        $pedido->clientePjRazaoSocial = Utils::removeEspeciais($pedido->clientePjRazaoSocial);
        $pedido->endCobrancaBairro = Utils::removeEspeciais($pedido->endCobrancaBairro);
        $pedido->endCobrancaComplemento = Utils::removeEspeciais($pedido->endCobrancaComplemento);
        $pedido->endCobrancaDestinatario = Utils::removeEspeciais($pedido->endCobrancaDestinatario);

        // Se isento, não verifica
        if (1 === (int)$pedido->clientePjIsento) {
            $pedido->clientePjInscEstadual = "0";
        } else {
            $regras['clientePjInscEstadual'][] = 'notemptynorwhitespaces';
        }

        if (empty($pedido->dataEntrega)) {
            $pedido->dataEntrega = '';
            unset($regras['dataEntrega']);
        }

        if (empty($pedido->turnoEntrega)) {
            $pedido->turnoEntrega = '';
            unset($regras['turnoEntrega']);
        }

        if (empty($pedido->valorEntrega)) {
            $pedido->valorEntrega = '';
            unset($regras['valorEntrega']);
        }

        if (empty($pedido->clientePjIsento)) {
            $pedido->clientePjIsento = 0;
        }

        $validacao = new Validacao($regras);
        if (!$validacao->executaValidacao($pedido)) {
            throw new ValidacaoException($validacao->msg, 2);
        }

        //Valida o documento (CPF ou CNPJ)
        $documento = $pedido->clienteTipo == 'pf' ? $pedido->clientePfCPF : $pedido->clientePjCNPJ;
        $tipoValidacao = $pedido->clienteTipo == 'pf' ? 'cpf' : 'cnpj';
        if (!Validacao::validar($documento, $tipoValidacao)) {
            throw new BusinessException(Validacao::$lastMsg, 2);
        }
        //Verifica se não está em uso o e-mail
        $clienteBO = new ClienteBO();
        if ($clienteBO->emailEmUso($pedido->clienteEmail, $documento, $pedido->clienteTipo, $pedido->siteId)) {
            throw new BusinessException("Erro: Email enviado no pedido esta cadastrado para outro CNPJ/CPF", 22);
        }

        //Recupera as permissões do parceiro
        $permissoesBO = new ParceiroSitePermissaoBO();
        $permissoes = $permissoesBO->getPermissoesBySiteBaseParceiroId($pedido->parceiroId, $pedido->siteBase);

        //Verifica se o parceiroId e SiteId informado batem
        $parceiroSiteBO = new ParceiroSiteBO();
        $parceiroSite = $parceiroSiteBO->getParceiroSite($pedido->parceiroId, $pedido->siteId);
        if (null === $parceiroSite) {
            throw new BusinessException('Erro: Dados de acesso do parceiro inválido - Verificar SiteId e ParceiroId se estao corretos', 1);
        }

        if ((!empty($pedido->integradorId) || (is_numeric($pedido->integradorId) && intval($pedido->integradorId) == 0)) && !in_array($pedido->integradorId, $parceiroSite->integradores)) {
            throw new BusinessException('Erro: Dados de acesso do parceiro inválido - Verificar se o Integrador está vinculado ao parceiro corretamente.', 1);
        }
        //valida se não há produto duplicado no corpo do pedido
        $cdProdutos = array();

        foreach ($pedido->pedidoProduto as $produto) {
            if (in_array($produto->codigoProduto, $cdProdutos, false)) {
                throw new BusinessException('Produto duplicado no corpo do pedido', Response::PRODUTO_DUPLICADO);
            } else {
                $cdProdutos[] = $produto->codigoProduto;
            }
        }

        $valorTotal = 0;
        $valorDesconto = 0;
        $valorTotalFrete = 0;

        foreach ($pedido->pedidoProduto as $pedProdAtualizacao) {

            // Atualiza os dados
            $valorTotal += $pedProdAtualizacao->valorTotal;
            $valorDesconto += $pedProdAtualizacao->valorDesconto * $pedProdAtualizacao->quantidade;
            $valorTotalFrete += $pedProdAtualizacao->valorFrete;

            // Verifica se eu posso vender o produto
            if (!$permissoesBO->verificaPermissaoProduto($pedProdAtualizacao->codigoProduto, $pedido->parceiroId)) {
                throw new BusinessException("Erro: Produto nao cadastrado ou nao esta liberado para venda pela integracao do sistema", Response::PRODUTO_SEM_PERMISSAO);
            }

            //Busca produto ID e Código
            $produtoDAO = new ProdutoDAO();
            $produtoIdEstoque = $produtoDAO->getProdutoEstoque($pedProdAtualizacao->codigoProduto);
            if ((int)$produtoIdEstoque->Disponivel < (int)$pedProdAtualizacao->quantidade) {
                $nomeProduto = $produtoDAO->buscarNomeProdutoSitePorCodigo((int)$pedProdAtualizacao->codigoProduto);
                throw new BusinessException("Erro: Estoque indisponivel - Nao existe estoque disponivel do item "
                    .$pedProdAtualizacao->codigoProduto." - ".$nomeProduto[0]->Nome." para venda no ARC", Response::ERRO_VALIDACAO);

            }

            $pedProdAtualizacao->produtoId = $produtoIdEstoque->ProdutoId;
            $pedProdAtualizacao->valorCustoEntrada = $produtoDAO->getProdutoCustoEntrada($pedProdAtualizacao->codigoProduto);
        }

        /* Valida se o total de produtos enviado estão corretos com o valor enviado pelo parceiro */
        $total = $pedido->valorTotalProdutos + $pedido->valorTotalFrete + $pedido->valorJuros - $valorDesconto;
        $totalEnviado = round($pedido->valorTotal * 100);
        $totalComputado = round($total * 100);
        if ($totalEnviado != $totalComputado) {
            $msg = "Erro: Valor Total do Pedido difere do Valor Total Calculado.";
            $msg .= "Valor Total Pedido: {$pedido->valorTotal} Valor Total Calculado: {$total}";
            throw new BusinessException($msg, Response::ERRO_VALOR_PEDIDO_DIFERENTE);
        }

        /* Valida se o valor dos fretes estão corretos com o enviado e o somado */
        $valorTotalFreteComputado = round($valorTotalFrete * 100);
        $valorTotalFreteEnviado = round($pedido->valorTotalFrete * 100);
        if ($valorTotalFreteComputado != $valorTotalFreteEnviado) {
            $msg = "Erro: Valor Total Frete difere do calculado por produto.'";
            $msg .= "Valor Frete Pedido: {$pedido->valorTotalFrete} Valor Frete Calculado (Soma Frete Produtos): {$valorTotalFrete}";
            throw new BusinessException($msg, 25);
        }

        /* Valida os pedidos enviados */
        $regrasValidacaoPedidoProduto = array(
            'quantidade' => array('required', 'notempty', 'int'),
            'valorUnitario' => array('required', 'float'),
            'valorTotal' => array('required', 'float'),
        );

        /* Validaçao de seguro */
        $regrasSeguro = array(
            'seguroTabelaId' => array('int', 'required', 'notempty'),
            'valorVenda' => array('float', 'required'),
            'familia' => array('int', 'required'),
            'quantidade' => array('int', 'required', 'notempty'),
        );
        $validadorSeguro = new Validacao($regrasSeguro);

        /* $Validacao de garantia */
        $regrasGarantia = array(
            'codigo' => array('str', 'required', 'notempty'),
            'prazo' => array('int', 'required'),
            'valorVenda' => array('float', 'required'),
            'quantidade' => array('int', 'required', 'notempty'),
        );
        $validadorGarantia = new Validacao($regrasGarantia);

        $validadorProdutos = new Validacao($regrasValidacaoPedidoProduto);
        foreach ($pedido->pedidoProduto as $pedidoProduto) {
            if (!$validadorProdutos->executaValidacao($pedidoProduto)) {
                throw new BusinessException($validadorProdutos->msg, 2);
            }

            if (!is_null($pedidoProduto->seguro)) {
                if ($pedidoProduto->seguro instanceof PedidoSeguro) {
                    if (!$validadorSeguro->executaValidacao($pedidoProduto->seguro)) {
                        throw new BusinessException($validadorProdutos->msg, 2);
                    }
                } else {
                    throw new BusinessException("Erro: campo Seguro não é uma instância de PedidoSeguro", 50);
                }
            }

            if (!is_null($pedidoProduto->garantia)) {
                if ($pedidoProduto->garantia instanceof PedidoGarantia) {
                    if (!$validadorGarantia->executaValidacao($pedidoProduto->garantia)) {
                        throw new BusinessException($validadorProdutos->msg, 2);
                    }
                } else {
                    throw new BusinessException("Erro: campo Garantia não é uma instância de PedidoGarantia", 51);
                }
            }
        }

        /* Valida os pagamentos enviados */
        $regraValidacaoPagamento = array(
            'meioPagamentoId' => array('required', 'notempty', 'int'),
            'bandeiraId' => array('required', 'notempty', 'int'),
            'parcelas' => array('required', 'notempty', 'int'),
            'cartaoNumero' => array('required', 'notempty', 'str'),
            'cartaoValidade' => array('required', 'notempty', 'str'),
            'cartaoCodSeguranca' => array('required', 'notempty', 'str'),
            'cartaoNomePortador' => array('required', 'notempty', 'str')
        );
        $validadorPagamento = new Validacao($regraValidacaoPagamento);
        $valorJurosPagamentos = 0;
        foreach ($pedido->pagamento as &$pagValidacao) {
            /* Rotinas de Cartão */
            if (in_array($pagValidacao->meioPagamentoId, Config::get('pedidos.meios_pagamentos_aceitos'))) {
                if ($pagValidacao->meioPagamentoId == 1) {
                    if (!$validadorPagamento->executaValidacao($pagValidacao)) {
                        throw new ValidacaoException("Erro: " . $validadorPagamento->msg, 2);
                    }
                } elseif ($pagValidacao->meioPagamentoId == 2) {
                    $pagValidacao->bandeiraId = 0;
                }
            }

            /* Rotinas de boleto */
            if (!in_array($pagValidacao->meioPagamentoId, Config::get('pedidos.meios_pagamentos_aceitos'))) {
                throw new BusinessException("Erro: Meio de pagamento invalido - MeioPagamentoId diferente de 1", 12);
            }

            /* soma valor do juros para validar com o total de juros na capa do pedido */
            $valorJurosPagamentos += $pagValidacao->valorJuros;
        }

        /* Valida valor do juros total com valor do juros do pagamento */
        if($pedido->valorJuros != $valorJurosPagamentos){
            throw new ValidacaoException("Erro: O total de juros informado não confere com os valores de juros informados nos pagamentos", 2);
        }

        /* Começa a salvar */
        $this->validarPedido($pedido, $parceiroSite);
        $dadosImportacaoDAO = new DadosImportacaoDAO();

        $parceiroConfigBO = new ParceiroConfiguracaoBO();
        $att = $parceiroConfigBO->getConfiguracoes($pedido->parceiroId, Configuracao::ACEITA_ATTACHMENTS, true);

        if ($att->valor) {
            $codSixPack = $parceiroConfigBO->getConfiguracoes(
                $pedido->parceiroId,
                Configuracao::ATTACHMENT_SIX_PACK,
                true
            )->valor;

            foreach ($pedido->pedidoProduto as $produto) {
                if (isset($produto->attachments) && is_array($produto->attachments) && count($produto->attachments) > 0) {

                    // para cada item sixpack deve haver 6 attachments

                    if ($produto->codigoProduto == $codSixPack) {
                        $totalAtts = $produto->quantidade * 6;

                        if (count($produto->attachments) != $totalAtts) {
                            throw new ValidacaoAttachmentsException('', Response::ATT_CONTAGEM_ATT_SIXPACK_ERRADA);
                        }

                    } else {
                        if (count($produto->attachments) != $produto->quantidade) {
                            throw new ValidacaoAttachmentsException('', Response::ATT_QUANTIDADE_NAO_CONFERE);
                        }
                    }

                    foreach ($produto->attachments as $att) {
                        if (!isset($att->type) || !is_numeric($att->type) || !isset($att->label)) {
                            throw new ValidacaoAttachmentsException('', Response::ATT_OBJETO_INCOMPLETO);
                        }

                        if ($att->type == null || $att->label == null || strlen(trim($att->label)) <= 0) {
                            throw new ValidacaoAttachmentsException('', Response::ATT_VARIAVEIS_VAZIAS);
                        }
                    }
                } else {
                    //verifica se o item informado pelo parceiro deve ter attachments
                    $attachemtsValida = new AttachmentDao();
                    $attachemtsValida->validarItemAttachmentsObrigatorio($produto, $pedido->parceiroId);
                }
            }
        } else {
            //se não houver configuração para attachments para o parceiro, garante que o atributo não passará daqui
            foreach ($pedido->pedidoProduto as &$produto) {
                if (isset($produto->attachments)) {
                    unset($produto->attachments);
                }
            }
        }

        try {
            //Verifica se já não existe no dados importação
            $dadosImportacaoBO = new DadosImportacaoBO();
            if ($dadosImportacaoBO->isImportado($pedido->parceiroPedidoId, $pedido->parceiroId)) {
                throw new BusinessException("Pedido {$pedido->parceiroPedidoId} já foi importado.", Response::PEDIDO_PARCEIRO_JA_IMPORTADO);
            }

            $dadosImportacaoDAO->beginTransaction();
            if (!$idDadosImportacao = $dadosImportacaoDAO->salvarPedido($pedido, $parceiroSite)) {
                throw new BusinessException("Erro: Erro na gravacao do Pedido - Entrar em contato com o Administrador da integracao MV", 5);
            }

            foreach ($pedido->pedidoProduto as $pedidoProduto) {
                $pedidoProdutoId = $dadosImportacaoDAO->salvarPedidoProduto($idDadosImportacao, $pedidoProduto);
                $this->setReservaEstoqueProduto($pedidoProduto);

                if (!is_null($pedidoProduto->garantia)) {
                    //salvando garantia
                    if (!$pedidoProduto->garantia instanceof PedidoGarantia) {
                        throw new BusinessException('Garantia inválida', 30);
                    } else {
                        $garantia = $pedidoProduto->garantia;
                        $garantia->dadosImportacaoId = $idDadosImportacao;
                        $garantia->pedidoProdutoId = $pedidoProdutoId;
                        $pedidoGarantiaDAO = new PedidoGarantiaDAO();
                        if (!$pedidoGarantiaDAO->setPedidoGarantia($garantia)) {
                            throw new BusinessException("Erro: Erro na gravação de garantia - Entrar em contato com o Administrador da integracao MV", 31);
                        }
                    }
                }

                //salvando seguro
                if (!is_null($pedidoProduto->seguro)) {
                    if (!$pedidoProduto->seguro instanceof PedidoSeguro) {
                        throw new BusinessException('Garantia inválida', 30);
                    } else {
                        $seguro = $pedidoProduto->seguro;
                        $seguro->dadosImportacaoId = $idDadosImportacao;
                        $seguro->pedidoProdutoId = $pedidoProdutoId;
                        $pedidoSeguroDAO = new PedidoSeguroDAO();

                        if (!$pedidoSeguroDAO->setPedidoSeguro($seguro)) {
                            throw new BusinessException("Erro: Erro na gravação de seguro - Entrar em contato com o Administrador da integracao MV", 32);
                        }
                    }
                }

                //salvando attachments caso se aplique
                if (isset($pedidoProduto->attachments)) {
                    $attDao = new AttachmentDao();
                    try {
                        foreach ($pedidoProduto->attachments as $att) {
                            $attDao->insertAttachment($idDadosImportacao, $pedidoProdutoId, $att);
                        }
                    } catch (\Exception $e) {
                        Logger::getInstance()->exception($e);
                        throw new BusinessException("", Response::ATT_ERRO_GRAVACAO_DB);
                    }
                }

            }

            foreach ($pedido->pagamento as $pagamento) {
                if ($pagamento->meioPagamentoId == Pagamento::CARTAO_PAGO && $pagamento->dataCaptura == '') {
                    $pagamento->dataCaptura = $pagamento->dataAutorizacao;
                }

                $dadosImportacaoDAO->salvarPedidoPagamento($idDadosImportacao, $pagamento);
            }

            $dadosImportacaoDAO->commit();
            DBLockBO::releaseLock($key);

            // Busca o pedido para validar que realmente foi inserido
            $pedido = $this->getPedidosParceiro($pedido->parceiroId, $pedido->parceiroPedidoId);

            // Valida se não foi commitado e/ou se o pedido não foi inserido
            if (sizeof($pedido) != 1) {
                throw new ValidacaoException('Pedido não pode ser importado. PEDNOTINSERTED.');
            }

            if (!$dadosImportacaoDAO->isCommited()) {
                throw new ValidacaoException('Pedido não pode ser importado. TRX.');
            }
        } catch (BusinessException $e) {
            $dadosImportacaoDAO->rollback();
            DBLockBO::releaseLock($key);
            throw $e;
        }

        return true;
    }

    public function setReservaEstoqueProduto(PedidoProduto $pedidoProduto) {
        $estabelecimentoDAO = new EstabelecimentoDAO();
        $estoqueDAO = new EstoqueEstabelecimentoDAO();
        $estabelecimentoId = null;

        $estabelecimentoPrioritario = $estabelecimentoDAO->getEstabelecimentoPrioritario();
        if (!is_null($estabelecimentoPrioritario)) {
            $estabelecimentoId = $estabelecimentoPrioritario->estabelecimentoId;
        }

        $estoquesEstabelecimento = $estoqueDAO->getEstoqueEstabelecimento($pedidoProduto->produtoId);
        foreach ($estoquesEstabelecimento as $e) {
            if (($e->disponivel - $e->jit) >= $pedidoProduto->quantidade) {
                $estabelecimentoId = $e->estabelecimentoId;
                break;
            }
        }

        $estabelecimentoId = (isset($estabelecimentoId) && $estabelecimentoId > 0) ? $estabelecimentoId : 2;
        $estoqueDAO->setReservaEstoqueProduto($pedidoProduto, $estabelecimentoId);
    }

    /**
     * Valida se o pedido está apto a ser inserido. Em caso de erro, dispara uma exceção
     *
     * @param Pedido $pedido
     * @param        $parceiroSite
     *
     * @throws BusinessException
     * @throws \Exception
     */
    public function validarPedido(Pedido $pedido, Parceiro $parceiroSite) {
        $somaPedidoProdutos = 0;
        $valorDescontos = 0;
        $sequencial = 1;
        $prazoEntregaCombinado = null;
        $somatorioTotalProdutos = 0;

        // Nessa parte o campo ValorTotalProduto deve ser igual a soma * quantidade de todos os produtos
        // Atualizado para inserir garantia e seguro
        foreach ($pedido->pedidoProduto as $pedidoProduto) {
            $valorTotal = $pedidoProduto->quantidade * $pedidoProduto->valorUnitario;
            $valorDescontos -= $pedidoProduto->quantidade * $pedidoProduto->valorDesconto;
            // Verifica se tem seguro e se é válido
            if (null !== $pedidoProduto->seguro) {
                $seguro = $pedidoProduto->seguro;
                $produtoDao = new ProdutoDAO();
                $produtoInfo = $produtoDao->getProdutoInfo($pedidoProduto->produtoId, $pedido->siteBase);
                $seguroDao = new SeguroDAO();
                $categorias = explode(',', $produtoInfo->Categorias);
                $conf = $seguroDao->getSeguroElegivel($produtoInfo->PrecoPor, $categorias);

                if ($conf->Familia != $seguro->familia) {
                    throw new BusinessException("Erro: Família inválida para o seguro do produto ({$pedidoProduto->produtoId})", 29);
                }
                if ($conf->SeguroTabelaId != $seguro->seguroTabelaId) {
                    throw new BusinessException("Erro: Seguro não disponível para o produto ({$pedidoProduto->produtoId})", 29);
                }

                $valorTotal += $seguro->quantidade * $seguro->valorVenda;
            }

            // Verifica se tem garantia e se é válido
            if (!is_null($pedidoProduto->garantia)) {
                $garantia = $pedidoProduto->garantia;
                $garantiaDAO = new GarantiaDAO();

                $conf = $garantiaDAO->confirmaGarantia($pedidoProduto->produtoId, $garantia->codigo);

                if ($conf->confirmacao) {
                    $valorTotal += $garantia->quantidade * $garantia->valorVenda;
                } else {
                    throw new BusinessException("Erro: Garantia não disponível para o produto ({$pedidoProduto->produtoId})", 28);
                }
            }

            $valorTotalArredondado = round($valorTotal * 100);
            $valorTotalEnviadoArredondado = round($pedidoProduto->valorTotal * 100);
            if ($valorTotalArredondado != $valorTotalEnviadoArredondado) {
                throw new BusinessException("Erro: Valor total do produto inconsistente - Valor total do produto diferente da quantidade X o valor unitario valorTotalArredondado:" . $valorTotal . " valorTotalEnviadoArredondado: " . $pedidoProduto->valorTotal, 7);
            }

            if ($sequencial != $pedidoProduto->sequencial) {
                throw new BusinessException("Erro de Sequencial do item do pedido", Response::SEQUENCIAL_ITEM_ERRADO);
            }
            $sequencial++;
            $somaPedidoProdutos += $valorTotal;
            $prazoEntregaCombinado = $pedidoProduto->prazoEntregaCombinado;

            $somatorioTotalProdutos += $valorTotal;
        }

        if (round($somatorioTotalProdutos * 100) != round($pedido->valorTotalProdutos * 100)) {
            throw new BusinessException("Erro: Valor total do produto inconsistente - Valor total dos produtos informado diverge da soma total dos itens. valorTotalProdutos: " . $pedido->valorTotalProdutos . ", valorTotalItens: " . $somatorioTotalProdutos, 7);
        }

        /* Executa foreach com os meios de pagamento e realiza as validações  */
        $this->iterarMeioPagamento($pedido, $parceiroSite);

        $confBO = new ParceiroConfiguracaoBO();
        $validarDataAgendamento = (bool)$confBO->getConfiguracoes($pedido->parceiroId, Configuracao::VALIDA_DATA_AGENDADA, true)->valor;

        if ($validarDataAgendamento) {
            PedidoValidatorRuleFactory::create('entregaAgendada')->validar($pedido);
        } else {
        	PedidoValidatorRuleFactory::create('entregaAgendada')->dataValida($pedido);
        }
    }

    /**
     * @param $parceiroId
     * @param $pedidoParceiro
     *
     * @return \App\Model\PedidoParceiro[]
     * @throws ValidacaoException
     * @throws DatabaseException
     */
    public function getPedidosParceiro($parceiroId, $pedidoParceiro) {
        $regras = array(
            'parceiroId' => array('required', 'notempty', 'int'),
            'pedidoParceiro' => array('required', 'notempty', 'tipoaceitos' => array('int_array', 'str')),
        );
        $obj = (object)array(
            'parceiroId' => $parceiroId,
            'pedidoParceiro' => $pedidoParceiro,
        );
        $validacao = new Validacao($regras);
        if (!$validacao->executaValidacao($obj)) {
            throw new ValidacaoException($validacao->msg, 2);
        }

        $dao = new PedidoParceiroDAO();
        $pedidoParceiro = is_array($pedidoParceiro) ? $pedidoParceiro : array($pedidoParceiro);

        return $dao->getPedidosParceiro($parceiroId, $pedidoParceiro);
    }

    /**
     * Método responsável por converter o valor de pontos para reais (R$)
     *
     * @param Pagamento $objPagamento
     * @param float     $valorTotalPedido
     * @param float     $valorPago
     *
     * @throws BusinessException
     */
    private function converterPontoEmValor(Pagamento &$objPagamento, $valorTotalPedido, &$valorPago) {
        $valorTotalPedido -= $valorPago;
        if ($valorTotalPedido > 0) {
            $valorPago += $valorTotalPedido;
            $objPagamento->valorPago = $valorTotalPedido;
        }
    }

    /**
     * Método responsável por executar foreach com os pagamentos e validar se estão
     * dentro do padrão pŕe estabelecido.
     *
     * @param Pedido   $pedido
     * @param Parceiro $parceiroSite
     *
     * @throws BusinessException
     */
    public function iterarMeioPagamento(Pedido &$pedido, Parceiro $parceiroSite) {
        $objPagamentoPonto = null;
        $objMeioPagamentoBO = new MeioPagamentoBO();
        $valorTotalPago = 0;
        $boletoPos = false;
        $contCartao = 0;
        $boleto = false;
        $sequencial = 1;
        foreach ($pedido->pagamento as $pagamento) {
            if ($sequencial++ !== (int)$pagamento->sequencial) {
                throw new BusinessException("Sequencial do Pagamento invalido - Verificar o valor de sequencial enviado", Response::ERRO_VALIDACAO);
            }

            $this->preValidarPagamentoPedido($pedido, $parceiroSite->parceiroId, $pagamento, $objMeioPagamentoBO, $valorTotalPago, $boletoPos, $contCartao, $boleto);

            if ($pagamento->meioPagamentoId == 8 && is_null($objPagamentoPonto)) {
                $objPagamentoPonto = $pagamento;
            } elseif ($pagamento->meioPagamentoId == 8) {
                throw new BusinessException("Não é possível fechar pedido com mais de um meioPagamentoId = 8", 2);
            }

            //Valida se é boleto pós pago e se o token informado está correto
                //Verifica se foi informado no campo cartaoCodSeguranca (legacy) ou no campo tokenAutorizacao
            if ($pagamento->meioPagamentoId == Pagamento::BOLETO_POS_PAGO &&
                ($pagamento->cartaoCodSeguranca != $parceiroSite->tokenAutorizacao
                    && $pagamento->tokenAutorizacao != $parceiroSite->tokenAutorizacao)
            ) {

                throw new BusinessException('Sem permissão para utilizar este meio de pagamento', Response::PERMISSAO_NEGADA_MEIO_PAGAMENTO);
            }
        }

        /* Validação pós execução do foreach */
        $this->posValidarPagamentoPedido($boletoPos, $contCartao, $boleto, $objPagamentoPonto);

        /* Conversão de pontos para valor  */
        if ($objPagamentoPonto instanceof Pagamento) {
            $this->converterPontoEmValor($objPagamentoPonto, $pedido->valorTotal, $valorTotalPago);
        }

        if (round($valorTotalPago * 100) != round($pedido->valorTotal * 100)) {
            throw new BusinessException("Valor Informado: R$ {$valorTotalPago}. Valor do Pedido {$pedido->valorTotal}", 13);
        }
    }

    /**
     * Método responsável por contabilizar os valores pagos e setar os
     * Meios de Pagamento que já foram pagos para uma possível validação futura.
     *
     * @param boolean   $boletoPos
     * @param boolean   $boleto
     * @param type      $contCartao
     * @param type      $sequencial
     * @param type      $valorTotalPago
     * @param Pagamento $pagamento
     *
     * @throws BusinessException
     */
    private function contabilizarPagamento(&$boletoPos, &$boleto, &$contCartao, &$sequencial, &$valorTotalPago, Pagamento $pagamento) {
        if ($pagamento->meioPagamentoId == 7) {
            $boletoPos = true;
        }

        if ($pagamento->meioPagamentoId == 2) {
            $boleto = true;
        }

        if ($pagamento->meioPagamentoId == 1) {
            $contCartao++;
        }

        if ($contCartao > 1 && $boletoPos) {
            throw new BusinessException("Não é possível fechar pedido de 2 cartões e pontos", 2);
        }
        $sequencial++;
        $valorTotalPago += $pagamento->valorPago;
    }

    /**
     * Método responsável por validar os meios de pagamento permitidos pela MV
     *
     * @staticvar int $sequencial
     *
     * @param \App\Model\Pedido $pedido
     * @param int               $parceiroId
     * @param Pagamento         $pagamento
     * @param MeioPagamentoBO   $objMeioPagamentoBO
     *
     * @param                   $valorTotalPago
     * @param                   $boletoPos
     * @param                   $contCartao
     * @param                   $boleto
     *
     * @throws \App\Exceptions\BusinessException
     */
    private function preValidarPagamentoPedido(Pedido $pedido, $parceiroId, Pagamento $pagamento, MeioPagamentoBO $objMeioPagamentoBO, &$valorTotalPago, &$boletoPos, &$contCartao, &$boleto) {
        if ($pagamento->parcelas < 1) {
            throw new BusinessException("O número de parcelas não pode ser inferior a 1 (um).");
        }

        if ($pagamento->valorPago < 0) {
            throw new BusinessException("O valor pago deve ser maior que 0 (zero).", 2);
        }

        if (!$objMeioPagamentoBO->validarMeioPagamentoParceiro($parceiroId, $pagamento->meioPagamentoId)) {
            throw new BusinessException("Meio de Pagamento Inválido para este parceiro", 2);
        }

        if (in_array($pagamento->meioPagamentoId, array(2, 7, 8)) && $pagamento->parcelas > 1) {
            throw new BusinessException("O meio de pagamento ({$pagamento->meioPagamentoId}) não pode ter mais de uma parcela.", 2);
        }

        if (in_array($pagamento->meioPagamentoId, array(2, 8)) && ($pagamento->valorJuros > 0 || $pagamento->percentualJuros > 0)) {
            throw new BusinessException("O meio de pagamento ({$pagamento->meioPagamentoId}) não pode conter juros.", 2);
        }

        if (in_array($pagamento->meioPagamentoId, array(2, 7, 1)) && $pagamento->pontos > 1) {
            throw new BusinessException("O meio de pagamento ({$pagamento->meioPagamentoId}) não pode ter pontos.", 2);
        }

        if ($boleto && $pagamento->meioPagamentoId == 2) {
            throw new BusinessException("Não é possível fechar pedido com 2 boletos", 2);
        }

        if ($boletoPos && $pagamento->meioPagamentoId == 7) {
            throw new BusinessException("Não é possível fechar pedido com 2 boletos pós pagos", 2);
        }

        // Só executa essa validação se o pedido for online, ou seja, não veio do banco
        if ($pedido->dadosImportacaoId === null) {
            if ((int)$pagamento->meioPagamentoId === Pagamento::PONTOS && $pagamento->valorPago > 0) {
                throw new BusinessException("O campo ( valorPago ) foi informado incorretamente. O valor deve ser igual a 0", 2);
            }
        }

        if ($contCartao > 2) {
            throw new BusinessException("Não é possível fechar pedido com mais de 2 cartões", 2);
        }

        $this->contabilizarPagamento($boletoPos, $boleto, $contCartao, $sequencial, $valorTotalPago, $pagamento);
    }

    /**
     * Executa validação dos pagamentos após a execução do foreach
     *
     * @param bool      $boletoPos
     * @param int       $contCartao
     * @param Pagamento $objPagamentoPonto
     *
     * @throws BusinessException
     */
    private function posValidarPagamentoPedido($boletoPos, $contCartao, $boleto, Pagamento $objPagamentoPonto = null) {
        if ($boletoPos && $objPagamentoPonto instanceof Pagamento) {
            throw new BusinessException("Não é possível fecha pedido com BOLETO PÓS PAGO + PONTOS", 2);
        }

        if ($contCartao > 1 && $objPagamentoPonto instanceof Pagamento) {
            throw new BusinessException("Não é possível fechar pedido com mais de um CARTÃO + PONTOS", 2);
        }

        if ($boleto && $contCartao > 0) {
            throw new BusinessException("Não é possível fechar pedido com CARTÃO + BOLETO", 2);
        }

        if ($boletoPos && $contCartao > 0) {
            throw new BusinessException("Não é possível fechar pedido com CARTÃO + BOLETO PÓS PAGO", 2);
        }
    }

    public function buscarPedidosPorParceiroPedidoId($parceiroPedidoId, $parceiroId) {
        if (!is_array($parceiroPedidoId)) {
            throw new ValidacaoException('Campo (parceiroPedidoId) deve ser um array', Response::ERRO_VALIDACAO);
        }

        if (!is_numeric($parceiroId)) {
            throw new ValidacaoException('Campo (parceiroId) deve ser numérico', Response::ERRO_VALIDACAO);
        }

        $pedidoDAO = new PedidoDAO();
        return $pedidoDAO->buscarPedidosPorParceiroPedidoId($parceiroPedidoId, $parceiroId);
    }
}
