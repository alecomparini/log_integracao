<?php
/**
 * Created by PhpStorm.
 * User: williamokano
 * Date: 05/01/2016
 * Time: 18:18
 */

define('BIT', 16);
define('TINYINT', 1);
define('BOOL', 1);
define('INTEGER', 3);
define('FLOAT', 4);
define('DOUBLE', 5);
define('VARCHAR', 253);

if ($argc < 2) {
    echo sprintf("Usage: php %s arguments", $argv[0]);
}

// Parse args
$args = array();
for ($i = 1; $i < $argc; $i++) {
    @list($arg, $value) = explode("=", $argv[$i]);
    if (is_null($arg) || is_null($value)) {
        echo sprintf("Invalid parameter format!\nParameter format: name=value\n");
        return 1;
    }
    $args[$arg] = $value;
}

if (!isset($args['table'])) {
    echo sprintf("Missing mandatory parameter 'table'");
    return 2;
}

$mysqli = new mysqli('192.168.152.3', 'riel_db', 'riel##2015@@');
if (mysqli_connect_error()) {
    sprintf("Connect failed: %s\n", mysqli_connect_error());
    return 3;
}
$mysqli->select_db('riel_producao');
$result = $mysqli->query(sprintf("SELECT * FROM %s LIMIT 1;", $args['table']));

function camelize($str) {
    $tokens = explode("_", $str);
    foreach ($tokens as &$token) {
        $token = ucfirst($token);
    }
    if (sizeof($tokens) > 0) {
        $tokens[0] = strtolower(substr($tokens[0], 0, 1)) . substr($tokens[0], 1);
    }

    return implode("", $tokens);
}
$fieldSample = file_get_contents('Field.sample.txt');
$fields = array();
if ($result !== false) {
    $finfo = $result->fetch_fields();
    foreach ($finfo as $column) {
        $type = "";
        $columnName = camelize($column->name);

        switch ($column->type) {
            case FLOAT:
            case DOUBLE:
                $type = "float";
                break;

            case BIT:
            case BOOL:
            case TINYINT:
                $type = "bool";
                break;

            case INTEGER:
                $type = "int";
                break;

            case VARCHAR:
            default:
                $type = "string";
        }

        $fields[] = str_replace(array('{{type}}', '{{field}}'), array($type, $columnName), $fieldSample);
    }
}

$classSample = file_get_contents('Class.Sample.txt');
$attrs = array('{{date}}', '{{time}}', '{{attributes}}', '{{$rules}}');
$replace = array(date("Y-m-d"), date("H:i:s"), implode("\n\n", $fields), "");
$output = str_replace($attrs, $replace, $classSample);
file_put_contents($args['table'] . '.php', $output);