var Softbox = function (callback) {
    var softbox = {
        addEvent: function (obj, type, fn, ref_obj) {
            if (obj.addEventListener)
                obj.addEventListener(type, fn, false);
            else if (obj.attachEvent) {
                // IE
                obj["e" + type + fn] = fn;
                obj[type + fn] = function () {
                    obj["e" + type + fn](window.event, ref_obj);
                }
                obj.attachEvent("on" + type, obj[type + fn]);
            }
        },
        input: "",
        pattern: "38384040373937396665",
        load: function (link) {
            this.addEvent(document, "keydown", function (e, ref_obj) {
                if (ref_obj) softbox = ref_obj; // IE
                softbox.input += e ? e.keyCode : event.keyCode;
                if (softbox.input.length > softbox.pattern.length)
                    softbox.input = softbox.input.substr((softbox.input.length - softbox.pattern.length));
                if (softbox.input == softbox.pattern) {
                    softbox.code(link);
                    softbox.input = "";
                    e.preventDefault();
                    return false;
                }
            }, this);
            this.iphone.load(link);
        },
        code: function (link) {
            window.location = link
        },
        iphone: {
            start_x: 0,
            start_y: 0,
            stop_x: 0,
            stop_y: 0,
            tap: false,
            capture: false,
            orig_keys: "",
            keys: ["UP", "UP", "DOWN", "DOWN", "LEFT", "RIGHT", "LEFT", "RIGHT", "TAP", "TAP"],
            code: function (link) {
                softbox.code(link);
            },
            load: function (link) {
                this.orig_keys = this.keys;
                softbox.addEvent(document, "touchmove", function (e) {
                    if (e.touches.length == 1 && softbox.iphone.capture == true) {
                        var touch = e.touches[0];
                        softbox.iphone.stop_x = touch.pageX;
                        softbox.iphone.stop_y = touch.pageY;
                        softbox.iphone.tap = false;
                        softbox.iphone.capture = false;
                        softbox.iphone.check_direction();
                    }
                });
                softbox.addEvent(document, "touchend", function (evt) {
                    if (softbox.iphone.tap == true) softbox.iphone.check_direction(link);
                }, false);
                softbox.addEvent(document, "touchstart", function (evt) {
                    softbox.iphone.start_x = evt.changedTouches[0].pageX;
                    softbox.iphone.start_y = evt.changedTouches[0].pageY;
                    softbox.iphone.tap = true;
                    softbox.iphone.capture = true;
                });
            },
            check_direction: function (link) {
                x_magnitude = Math.abs(this.start_x - this.stop_x);
                y_magnitude = Math.abs(this.start_y - this.stop_y);
                x = ((this.start_x - this.stop_x) < 0) ? "RIGHT" : "LEFT";
                y = ((this.start_y - this.stop_y) < 0) ? "DOWN" : "UP";
                result = (x_magnitude > y_magnitude) ? x : y;
                result = (this.tap == true) ? "TAP" : result;

                if (result == this.keys[0]) this.keys = this.keys.slice(1, this.keys.length);
                if (this.keys.length == 0) {
                    this.keys = this.orig_keys;
                    this.code(link);
                }
            }
        }
    }

    typeof callback === "string" && softbox.load(callback);
    if (typeof callback === "function") {
        softbox.code = callback;
        softbox.load();
    }

    return softbox;
};