var http_helper = {
    build_query: function (obj, num_prefix, temp_key) {
        var output_string = [];
        Object.keys(obj).forEach(function (val) {
            var key = val;
            if (num_prefix && !isNaN(key)) {
                key = num_prefix + key;
            }
            key = encodeURIComponent(key.replace(/[!'()*]/g, escape));
            if (temp_key) {
                key = temp_key + '[' + key + ']';
            }

            if (typeof obj[val] === 'object') {
                var query = http_helper.build_query(obj[val], null, key);
                output_string.push(query);
            } else {
                var value = encodeURIComponent(obj[val].replace(/[!'()*]/g, escape));
                output_string.push(key + '=' + value);
            }
        });
        return output_string.join('&');
    },
    parse_query: function (query) {
        var $output = {};
        (function parse_str (str, array) {
            var strArr = String(str).replace(/^&/, '').replace(/&$/, '').split('&');
            var sal = strArr.length;
            var i;
            var j;
            var ct;
            var p;
            var lastObj;
            var obj;
            var undef;
            var chr;
            var tmp;
            var key;
            var value;
            var postLeftBracketPos;
            var keys;
            var keysLen;

            var _fixStr = function (str) {
                return decodeURIComponent(str.replace(/\+/g, '%20'));
            };

            var $global = (typeof window !== 'undefined' ? window : GLOBAL);
            $global.$locutus = $global.$locutus || {};
            var $locutus = $global.$locutus;
            $locutus.php = $locutus.php || {};

            if (!array) {
                array = $global;
            }

            for (i = 0; i < sal; i++) {
                tmp = strArr[i].split('=');
                key = _fixStr(tmp[0]);
                value = (tmp.length < 2) ? '' : _fixStr(tmp[1]);

                while (key.charAt(0) === ' ') {
                    key = key.slice(1);
                }
                if (key.indexOf('\x00') > -1) {
                    key = key.slice(0, key.indexOf('\x00'));
                }
                if (key && key.charAt(0) !== '[') {
                    keys = [];
                    postLeftBracketPos = 0;
                    for (j = 0; j < key.length; j++) {
                        if (key.charAt(j) === '[' && !postLeftBracketPos) {
                            postLeftBracketPos = j + 1;
                        } else if (key.charAt(j) === ']') {
                            if (postLeftBracketPos) {
                                if (!keys.length) {
                                    keys.push(key.slice(0, postLeftBracketPos - 1));
                                }
                                keys.push(key.substr(postLeftBracketPos, j - postLeftBracketPos));
                                postLeftBracketPos = 0;
                                if (key.charAt(j + 1) !== '[') {
                                    break;
                                }
                            }
                        }
                    }
                    if (!keys.length) {
                        keys = [key];
                    }
                    for (j = 0; j < keys[0].length; j++) {
                        chr = keys[0].charAt(j);
                        if (chr === ' ' || chr === '.' || chr === '[') {
                            keys[0] = keys[0].substr(0, j) + '_' + keys[0].substr(j + 1);
                        }
                        if (chr === '[') {
                            break;
                        }
                    }

                    obj = array;
                    for (j = 0, keysLen = keys.length; j < keysLen; j++) {
                        key = keys[j].replace(/^['"]/, '').replace(/['"]$/, '');
                        lastObj = obj;
                        if ((key !== '' && key !== ' ') || j === 0) {
                            if (obj[key] === undef) {
                                obj[key] = {};
                            }
                            obj = obj[key];
                        } else {
                            // To insert new dimension
                            ct = -1;
                            for (p in obj) {
                                if (obj.hasOwnProperty(p)) {
                                    if (+p > ct && p.match(/^\d+$/g)) {
                                        ct = +p;
                                    }
                                }
                            }
                            key = ct + 1;
                        }
                    }
                    lastObj[key] = value;
                }
            }
        })(query, $output);
        return $output;
    }
};