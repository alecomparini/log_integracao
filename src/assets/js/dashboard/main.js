$(document).ready(function() {
    if (!window.localStorage || window.localStorage.getItem('access_token') === null) {
        window.location.href = '/login';
    }
});