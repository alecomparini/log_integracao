var app = new Vue({
    el: '#app',
    data: {
        email: "",
        senha: "",
        errorMessage: ""
    },
    methods: {
        login: function () {
            this.errorMessage = "";
            var loginResource = this.$resource('/access_token?email=' + encodeURIComponent(this.email) + '&senha=' + encodeURIComponent(this.senha));
            loginResource.get().then(function (res) {
                return res.json()
            }, function (error) {
                var res = error.json();
                this.errorMessage = res.mensagem;
            }).then(function (response) {
                if (response.success) {
                    localStorage.setItem('access_token', response.data.token);
                    this.afterLogin();
                } else {
                    this.errorMessage = response.mensagem;
                }
            });
        },
        afterLogin: function () {
            location.href = '/admin';
        }
    }
});