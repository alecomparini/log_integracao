<?php

use App\Core\Cache;

require_once realpath(__DIR__) . DIRECTORY_SEPARATOR . 'vendor/autoload.php';
require_once realpath(__DIR__) . DIRECTORY_SEPARATOR . 'app/constants.php';

date_default_timezone_set('America/Sao_Paulo');

Cache::init();
