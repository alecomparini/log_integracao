# O total de att's nessa tabela equivale ao campo quantidade da tabela Pedido_Produto, se a quantidade for 8
# logo haverão 8 registros nessa tabela, que podem ou não ter o mesmo valor [tipo, label] no campo attachment
use riel_integracao;
DROP TABLE IF EXISTS Pedido_Attachment;
CREATE TABLE Pedido_Attachment (
  PedidoAttachmentId INT(11) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  DadosImportacaoId INT(11) UNSIGNED NOT NULL,
  PedidoProdutoId INT(11) UNSIGNED NOT NULL, #referencia a tabela riel_integracarao.Pedido_Produto
  Tipo VARCHAR(15) NOT NULL,
  Rotulo VARCHAR(200) NOT NULL,
  DataCriacao DATETIME NOT NULL DEFAULT NOW(),
  INDEX idx_attachmentid (DadosImportacaoId, PedidoProdutoId)
);

#tabela para ser utilizada no riel_producao junto ao site para transferencia das informações sobre os attachments
use riel_producao;

DROP TABLE IF EXISTS Pedido_Produto_Attachment;
CREATE TABLE Pedido_Produto_Attachment (
  PedidoAttachmentId INT(11) UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT,
  PedidoId INT(11) UNSIGNED NOT NULL,
  Codigo VARCHAR(10) NOT NULL,
  Tipo VARCHAR(15) NOT NULL,
  Rotulo VARCHAR(200) NOT NULL,
  DataCriacao DATETIME NOT NULL DEFAULT NOW(),
  INDEX idx_attachmentid (PedidoId, Codigo)
);

# Tabela para ser utilizada em mv_gateway
use mv_gateway;
DROP TABLE IF EXISTS `Pedido_Produto_Attachment`;
CREATE TABLE `Pedido_Produto_Attachment` (
  `PedidoProdutoAttachmentId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PedidoId` int(11) unsigned NOT NULL,
  `Codigo` varchar(10) NOT NULL,
  `Tipo` VARCHAR(15) NOT NULL,
  `Rotulo` VARCHAR(200) NOT NULL,
  `DataCriacao` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`PedidoProdutoAttachmentId`),
  KEY `idx_attachmentid` (`PedidoId`,`Codigo`)
);