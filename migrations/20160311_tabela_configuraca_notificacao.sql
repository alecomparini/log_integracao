DROP TABLE IF EXISTS riel_integracao.NotificacaoParceiro;
CREATE TABLE riel_integracao.NotificacaoParceiro (
  ParceiroId INT(11) UNSIGNED NOT NULL,
  SiteId INT(11) UNSIGNED NOT NULL,
  ProdutoCodigo VARCHAR(45) NOT NULL,
  DataCriacao DATETIME NOT NULL DEFAULT NOW(),
  DataUltimaTentativa DATETIME,
  Tentativas TINYINT(2) DEFAULT 0,
  ModificacaoPreco TINYINT(1) DEFAULT 0,
  ModificacaoEstoque TINYINT(1) DEFAULT 0,
  PRIMARY KEY (ParceiroId, ProdutoCodigo)
);

DROP TABLE IF EXISTS riel_integracao.Parceiro_Configuracao;
DROP TABLE IF EXISTS riel_integracao.Configuracao;
CREATE TABLE riel_integracao.Configuracao (
  ConfiguracaoId INT          NOT NULL PRIMARY KEY AUTO_INCREMENT,
  Chave          VARCHAR(100) NOT NULL,
  ValorDefault   VARCHAR(100) NULL                 DEFAULT NULL,
  TipoDefault    VARCHAR(30)  NULL                 DEFAULT NULL
);

INSERT INTO riel_integracao.Configuracao (Chave, ValorDefault, TipoDefault) VALUES

  ('URL_NOTIFICACAO', NULL, 'string'),
  ('ACEITA_NOTIFICACAO', 0, 'bool');

CREATE TABLE riel_integracao.Parceiro_Configuracao (
  ParceiroId     TINYINT(1) UNSIGNED NOT NULL,
  ConfiguracaoId INT                 NOT NULL,
  Valor          VARCHAR(100)        NULL DEFAULT NULL,
  Tipo           VARCHAR(100)        NULL DEFAULT NULL,

  CONSTRAINT FkParceiroConfiguracaoParceiro
  FOREIGN KEY (ParceiroId) REFERENCES riel_integracao.Parceiro (ParceiroId),

  CONSTRAINT FkParceiroConfiguracaoConfiguracao
  FOREIGN KEY (ConfiguracaoId) REFERENCES riel_integracao.Configuracao (ConfiguracaoId),

  CONSTRAINT PkParceiroConfiguracao PRIMARY KEY (ParceiroId, ConfiguracaoId),
  INDEX IdxParceiroId (ParceiroId),

  CONSTRAINT UqParceiroConfiguracao UNIQUE (ParceiroId, ConfiguracaoId)
);