
#inserção de novo meio de pagamento no site, novo ID: 9
INSERT INTO riel_producao.MeioPagamento (Nome, Status) VALUES ('Cartão Pago', 1);

#inserção do novo meio de pagamento no gateway

SET @SITEID = 62; # COLOCAR O SITE ID DA COCA COLA
SET @ADQUIRINTE = 5; # COLOCAR Id do novo adquirente PayU na tabela mv_gateway.Adquirente
SET @MEIOPAGAMENTO = 9; #novo meio de pagamento DO SITE
SET @MPAGAMENTOARC = 12; #NOVO MEIO DE PAGAMENTO DO ARC
SET @PARCEIROID = 35;

#meio de pagamento cartão pago VISA
INSERT INTO mv_gateway.MeioPagamento (
  Nome,
  SiteMeioPagamentoId,
  SiteBandeiraId,
  ErpMeioPagamentoId,
  ErpBandeiraId,
  PagMeioPagamentoId,
  PagBandeiraId,
  GatewayRiscoMeioPagamentoId,
  GatewayRiscoBandeiraId,
  Banco,
  Agencia,
  Conta,
  Fluxo,
  GatewayPagamento,
  PrazoPendente,
  SiteId,
  AdquirenteId,
  Status
) VALUES (
  'Cartão Pago Visa',
  @MEIOPAGAMENTO,
  2,
  @MPAGAMENTOARC,
  1,
  997,
  NULL,
  1,
  1,
  NULL,
  NULL,
  NULL,
  9,
  'COCA-COLA',
  NULL,
  @SITEID,
  @ADQUIRINTE,
  1
);

#meio de pagamento cartão pago MASTER
INSERT INTO mv_gateway.MeioPagamento (
  Nome,
  SiteMeioPagamentoId,
  SiteBandeiraId,
  ErpMeioPagamentoId,
  ErpBandeiraId,
  PagMeioPagamentoId,
  PagBandeiraId,
  GatewayRiscoMeioPagamentoId,
  GatewayRiscoBandeiraId,
  Banco,
  Agencia,
  Conta,
  Fluxo,
  GatewayPagamento,
  PrazoPendente,
  SiteId,
  AdquirenteId,
  Status
) VALUES (
  'Cartão Pago MASTER',
  @MEIOPAGAMENTO,
  1,
  @MPAGAMENTOARC,
  2,
  997,
  NULL,
  1,
  1,
  NULL,
  NULL,
  NULL,
  9,
  'COCA-COLA',
  NULL,
  @SITEID,
  @ADQUIRINTE,
  1
);

#meio de pagamento cartão pago Amex
INSERT INTO mv_gateway.MeioPagamento (
  Nome,
  SiteMeioPagamentoId,
  SiteBandeiraId,
  ErpMeioPagamentoId,
  ErpBandeiraId,
  PagMeioPagamentoId,
  PagBandeiraId,
  GatewayRiscoMeioPagamentoId,
  GatewayRiscoBandeiraId,
  Banco,
  Agencia,
  Conta,
  Fluxo,
  GatewayPagamento,
  PrazoPendente,
  SiteId,
  AdquirenteId,
  Status
) VALUES (
  'Cartão Pago Amex',
  @MEIOPAGAMENTO,
  3,
  @MPAGAMENTOARC,
  3,
  997,
  NULL,
  1,
  1,
  NULL,
  NULL,
  NULL,
  9,
  'COCA-COLA',
  NULL,
  @SITEID,
  @ADQUIRINTE,
  1
);

#meio de pagamento cartão pago Dinners
INSERT INTO mv_gateway.MeioPagamento (
  Nome,
  SiteMeioPagamentoId,
  SiteBandeiraId,
  ErpMeioPagamentoId,
  ErpBandeiraId,
  PagMeioPagamentoId,
  PagBandeiraId,
  GatewayRiscoMeioPagamentoId,
  GatewayRiscoBandeiraId,
  Banco,
  Agencia,
  Conta,
  Fluxo,
  GatewayPagamento,
  PrazoPendente,
  SiteId,
  AdquirenteId,
  Status
) VALUES (
  'Cartão Pago Dinners',
  @MEIOPAGAMENTO,
  4,
  @MPAGAMENTOARC,
  4,
  997,
  NULL,
  1,
  1,
  NULL,
  NULL,
  NULL,
  9,
  'COCA-COLA',
  NULL,
  @SITEID,
  @ADQUIRINTE,
  1
);

#meio de pagamento cartão pago Hypercard
INSERT INTO mv_gateway.MeioPagamento (
  Nome,
  SiteMeioPagamentoId,
  SiteBandeiraId,
  ErpMeioPagamentoId,
  ErpBandeiraId,
  PagMeioPagamentoId,
  PagBandeiraId,
  GatewayRiscoMeioPagamentoId,
  GatewayRiscoBandeiraId,
  Banco,
  Agencia,
  Conta,
  Fluxo,
  GatewayPagamento,
  PrazoPendente,
  SiteId,
  AdquirenteId,
  Status
) VALUES (
  'Cartão Pago Hypercard',
  @MEIOPAGAMENTO,
  6,
  @MPAGAMENTOARC,
  5,
  997,
  NULL,
  1,
  1,
  NULL,
  NULL,
  NULL,
  9,
  'COCA-COLA',
  NULL,
  @SITEID,
  @ADQUIRINTE,
  1
);

#meio de pagamento cartão pago Elo
INSERT INTO mv_gateway.MeioPagamento (
  Nome,
  SiteMeioPagamentoId,
  SiteBandeiraId,
  ErpMeioPagamentoId,
  ErpBandeiraId,
  PagMeioPagamentoId,
  PagBandeiraId,
  GatewayRiscoMeioPagamentoId,
  GatewayRiscoBandeiraId,
  Banco,
  Agencia,
  Conta,
  Fluxo,
  GatewayPagamento,
  PrazoPendente,
  SiteId,
  AdquirenteId,
  Status
) VALUES (
  'Cartão Pago Elo',
  @MEIOPAGAMENTO,
  13,
  @MPAGAMENTOARC,
  9,
  997,
  NULL,
  1,
  1,
  NULL,
  NULL,
  NULL,
  9,
  'COCA-COLA',
  NULL,
  @SITEID,
  @ADQUIRINTE,
  1
);

#Vinculando o parceiro Coca-cola ao novo meio de pagamento
INSERT INTO riel_integracao.Parceiro_MeioPagamento (
  MeioPagamentoId,
  ParceiroId
) VALUES (
    @MEIOPAGAMENTO,
    @PARCEIROID
);

# Adição dos novos campos para meio de pagamento na integração
ALTER TABLE riel_integracao.Pagamento
  ADD COLUMN NSU INT(11) NULL,
  ADD COLUMN AutorizacaoId INT(11) NULL,
  ADD COLUMN DataAutorizacao DATETIME,
  ADD COLUMN DataCaptura DATETIME,
  ADD COLUMN Adquirente VARCHAR(50);

# Adição dos novos campos para meio de pagamento na produção
ALTER TABLE riel_producao.Pagamento
ADD COLUMN AutorizacaoId INT(11) NULL,
ADD COLUMN NSU INT(11) NULL,
ADD COLUMN DataCaptura DATETIME,
ADD COLUMN Adquirente VARCHAR(50);

