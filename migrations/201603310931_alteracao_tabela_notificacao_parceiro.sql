
ALTER TABLE riel_integracao.NotificacaoParceiro
ADD COLUMN ModificacaoTracking TINYINT(1) NULL AFTER ModificacaoEstoque,
ADD COLUMN PedidoParceiroId INT(10) NULL AFTER SiteId;
