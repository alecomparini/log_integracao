/* ADD Meio de Pagamento Pontos no Banco riel_producao */
INSERT INTO riel_producao.MeioPagamento values (8, 'Pontos',1);

CREATE TABLE riel_integracao.NotificacaoEstornoPonto (
	ParceiroId int(11) unsigned,
	SiteId int(11) unsigned,
	PedidoParceiroId varchar(45),
	DataCriacao datetime not null,
	DataUltimaTentativa datetime null,
	Tentativas tinyint null,
	ClienteDocumento varchar(14) not null,
	Pontos mediumint not null	
) ENGINE = InnoDB; 