DROP TABLE IF EXISTS Token;
CREATE TABLE Token (
  TokenId INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  Token VARCHAR(40) NOT NULL,
  DataValidade DATETIME NULL DEFAULT NULL,
  DataCriacao DATETIME NOT NULL DEFAULT NOW(),
  DataUltimaUtilizacao DATETIME NULL DEFAULT NOW(),
  ParceiroId TINYINT(1) UNSIGNED NOT NULL,

  CONSTRAINT fk_Token_Parceiro FOREIGN KEY (ParceiroId) REFERENCES Parceiro (ParceiroId),
  CONSTRAINT uq_Token UNIQUE (Token)
);

DROP TABLE IF EXISTS TokenEscopo;
CREATE TABLE TokenEscopo (
  TokenEscopoId INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
  TokenId INT NOT NULL,
  Escopo VARCHAR (50) NOT NULL,

  CONSTRAINT fk_TokenEscopo_Token FOREIGN KEY (TokenId) REFERENCES Token (TokenId)
);

INSERT INTO Token (Token, DataValidade, ParceiroId) values
  ('a94a8fe5ccb19ba61c4c0873d391e987982fbbd3', date_add(NOW(), INTERVAL 7 day), 2);

INSERT INTO TokenEscopo (TokenId, Escopo) values (1, '/closure');