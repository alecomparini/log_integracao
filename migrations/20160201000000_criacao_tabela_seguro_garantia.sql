CREATE TABLE Pedido_Seguro(
  PedidoSeguroId INT(11) UNSIGNED AUTO_INCREMENT PRIMARY KEY AUTO_INCREMENT,
  DadosImportacaoId INT(11) UNSIGNED NOT NULL,
  PedidoProdutoId INT(11) UNSIGNED NOT NULL,
  SeguroTabelaId INT(11) NOT NULL,
  Quantidade TINYINT(2) UNSIGNED,
  ValorVenda DECIMAL(10,2) UNSIGNED,
  Familia TINYINT(3)
);

CREATE TABLE Pedido_Garantia
(
  ProdutoGarantiaId INT(11) UNSIGNED PRIMARY KEY NOT NULL AUTO_INCREMENT,
  DadosImportacaoId INT(11) UNSIGNED NOT NULL,
  PedidoProdutoId INT(11) UNSIGNED NOT NULL,
  GarantiaCodigo INT(11) UNSIGNED NOT NULL COMMENT 'Referência da tabela riel_producao.GarantiaProduto',
  Quantidade TINYINT(2) UNSIGNED,
  Prazo TINYINT(2) NOT NULL,
  ValorVenda DECIMAL(10,2)
);

INSERT INTO db_integracao.PermissaoTipo (PermissaoTipoId, Nome) VALUES (3, 'Tudo');