module.exports = function (shipit) {
    require('shipit-deploy')(shipit);

    if (!process.env.SHIPIT_TAREFA || !process.env.SHIPIT_SRV || !process.env.TARGET) {
        shipit.log('############################################################################');
        shipit.log('Preencha TODAS as variáveis de ambiente: SHIPIT_TAREFA, SHIPIT_SRV e TARGET.');
        shipit.log('############################################################################');
        process.exit(1);
    }

    shipit.initConfig({
        default: {
            workspace: '/tmp/integracao', // CAMINHO DA DOCKER
            dirToCopy: './',
            deployTo: '/home/projetos/releases/integracao', // CAMINHO DO SERVIDOR DE DEPLOY
            ignores: ['.git'],
            repositoryUrl: '/fontes/integracao/.git',
            rsync: ['--del'],
            keepReleases: 2,
            key: '/root/.ssh/id_rsa',
            shallowClone: true
        },
        staging: {
            servers: 'projetos@192.168.12.52',
            branch: process.env.TARGET
        }
    });

    shipit.blTask('run-build', function () {
        shipit.log('########################################################');
        shipit.log('###### Executando o build customizado. ----> ' + process.env.TARGET);
        shipit.log('########################################################');

        return shipit.local('[ -f build.sh ] && sh build.sh', {cwd: shipit.config.workspace})
            .then(function () {
                shipit.log('#############################################################');
                shipit.log('Build customizado executado com sucesso.');
                shipit.log('#############################################################');
            })
            .catch(function (err) {
                shipit.log('#############################################################');
                shipit.log('Erro ao executar o build customizado! O deploy será abortado.');
                shipit.log('#############################################################');
                shipit.log(err);
                process.exit(1);
            });
    });

    shipit.blTask('run-install', function () {
        shipit.log('Executando o install customizado.');

        return shipit.remote('cd ' + shipit.config.deployTo + '/current && pwd && [ -f install.sh ] && sh install.sh')
            .then(function () {
                shipit.log('#############################################################');
                shipit.log('Install customizado executado com sucesso.');
                shipit.log('#############################################################');
            })
            .catch(function (err) {
                shipit.log('#############################################################');
                shipit.log('Erro ao executar o install customizado! O deploy será abortado.');
                shipit.log('#############################################################');
                shipit.log(err);
                process.exit(1);
            });
    });

    shipit.on('fetched', function () {
        return shipit.start('run-build');
    });

    shipit.on('published', function () {
        return shipit.start('run-install');
    });
};
