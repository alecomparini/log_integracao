"use strict";
var async   = require('async');
var request = require('request');
var fs      = require('fs');

var apiUrl      = 'http://integracao.maquinadevendas.com.br/v2/produto?parceiroId=33&pagina=';
var requests    = [];

// Consulta a primeira página para saber quantas páginas têm
request(apiUrl + "1", function (error, response, body) {
    if (!error) {
        var ret = JSON.parse(body);
        for (var i = 849; i <= ret.data.dadosPaginacao.totalPaginas; i++) {
            (function (i) {
                requests.push(function (callback) {
                    request.call(null, apiUrl + i, function (err, res, bod) {
                        console.log('Consultado page: ' + i);
                        var re = JSON.parse(bod);
                        if (err || re.status === false) {
                            console.log('Error on page: ' + i);
                            console.log(re.codigo);
                            console.log(re.mensagem);
                            console.log(re.data);
                        }
                        callback(null, true);
                    });
                });
            })(i);
        }
        async.parallelLimit(requests, 15, function (err, res) {
            console.log(res);
        });
    } else {
        console.log('Failed to consume page 1');
    }
});

process.on('uncaughtException', function (err) {
    console.log('Uncaught exception: ' + err);
});