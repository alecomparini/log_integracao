<?php
$pdo = new PDO('mysql:host=192.168.152.3;port=3306;dbname=riel_integracao', 'dev', 'dev@2016', array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
));

$pdoTeste = new PDO('mysql:host=127.0.0.1;port=3306;dbname=teste_interface', 'root', 'root', array(
    PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
));

$client = new SoapClient('http://integracao.dev/ws/Dados/?wsdl');
$clientConfirmacao = new SoapClient('http://integracao.dev/ws/InterfaceConfirmacao/?wsdl');

define('INTERFACE_TIPO', 1);
define('PARCEIRO_ID', 2);
define('PARCEIRO_SITE_ID', 2);
define('SITE_ID', 4);

$clientConfirmacao->setInterfaceConfirmacao(array(
    'Confirmacao' => 2,
    'InterfaceTipo' => INTERFACE_TIPO,
    'ParceiroId' => PARCEIRO_ID,
    'Site' => SITE_ID
));

$execucao = 1;
do {
    try {
        $stmt = $pdo->prepare("SELECT * FROM " . DB_INTEGRACAO . ".Interface WHERE ParceiroSiteId = :parceiro_site_id AND InterfaceTipoId = :interface_tipo_id AND Enviada = 0 LIMIT 1000");
        $stmt->bindValue('parceiro_site_id', PARCEIRO_SITE_ID, PDO::PARAM_INT);
        $stmt->bindValue('interface_tipo_id', INTERFACE_TIPO, PDO::PARAM_INT);
        $stmt->execute();
        $result = $stmt->fetchAll(PDO::FETCH_OBJ);

        $soapResult = $client->getDados(array(
            'Interface' => INTERFACE_TIPO,
            'ParceiroId' => PARCEIRO_ID,
            'SiteId' => SITE_ID,
        ));
        $dados = json_decode($soapResult->Dados);

        //Verifica a quantidade enviada
        if (sizeof($result) != sizeof($dados)) {
            fwrite(STDOUT, 'Qtd itens enviados divergente' . PHP_EOL);
        }

        if ($soapResult->Codigo == 0) {
            $hasMore = true;
            foreach ($dados as $key => $value) {
                $items = json_decode($value);
                $itemsDB = json_decode($result[$key]->Dados);

                $stmtInsertConsumo = $pdoTeste->prepare('INSERT INTO Consumo (Json, DataConsumo) VALUES (:json, :dt_consumo);');
                $stmtInsertConsumo->bindValue('json', $value);
                $stmtInsertConsumo->bindValue('dt_consumo', date("Y-m-d H:i:s"));
                $stmtInsertConsumo->execute();

                foreach ($items as $keyItem => $item) {
                    if ($item->Codigo != $itemsDB[$keyItem]->Codigo) {
                        echo 'Erro encontrado na execução #' . $execucao;
                    }
                    //Insere dado na tabela de teste
                    $stmtInsertTblTeste = $pdoTeste->prepare('INSERT INTO Estoque (Codigo, Disponivel) VALUES (:codigo, :disponivel) ON DUPLICATE KEY UPDATE Disponivel = VALUES(Disponivel);');
                    $stmtInsertTblTeste->bindValue('codigo', $item->Codigo, PDO::PARAM_INT);
                    $stmtInsertTblTeste->bindValue('disponivel', $item->Estoque, PDO::PARAM_INT);
                    $stmtInsertTblTeste->execute();
                }
            }

            $clientConfirmacao->setInterfaceConfirmacao(array(
                'Confirmacao' => 1,
                'InterfaceTipo' => INTERFACE_TIPO,
                'ParceiroId' => PARCEIRO_ID,
                'Site' => SITE_ID
            ));

            echo 'OK';
        } else {
            $hasMore = false;
        }
    } catch (PDOException $pe) {
        throw $pe;
    }
} while ($hasMore);