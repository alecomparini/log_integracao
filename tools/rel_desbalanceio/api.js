var request     = require('request');
var config      = require('./config');

module.exports = {
    getFrete: function (parceiroId, cep, produtos, callback) {
        this._call('GET', 'frete', {
            parceiroId: parceiroId,
            produtos: produtos.map(function (item) { return item.codigo }),
            quantidades: produtos.map(function (item) { return item.quantidade }),
            cep: cep
        }, null, callback)
    },
    getProdutosPagina: function (parceiroId, pagina, callback) {
        this._call('GET', 'produto', {
            parceiroId: parceiroId,
            pagina: pagina
        }, {}, callback);
    },
    _call: function (method, resource, qs, data, callback) {
        resource = resource.substr(0, 1) !== "/" ? "/".concat(resource) : resource;

        let url = config.api.endpoint.concat(resource);
        let requestConfig = {
            method: method,
            url: url,
            qs: qs
        };

        if (data) {
            requestConfig['postData']  = data;
        }

        request(requestConfig, function (err, response, body) {
            let ret = body;
            if (!err && response.headers['content-type'].indexOf('application/json') !== -1) {
                ret = JSON.parse(body);
            }
            if (typeof callback === "function") {
                callback(err, ret);
            }
        });
    }
};
