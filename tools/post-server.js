var http        = require('http');
var fs          = require('fs');
var httpCode    = process.argv[2] || 200;

console.log('Server UP. Http Code always: ' + httpCode);

function rand(min, max) {
    return parseInt(Math.random() * (max - min) + min, 10);
}
var conexoesAbertas = 0;
http.createServer(function (req, res) {
    conexoesAbertas++;
    var delay = rand(500, 2500);
    if (req.method == 'POST') {
        console.log('POST');

        var body = '';
        req.on('data', function (data) {
            body += data;
        });

        req.on('end', function () {
            console.log('Body: ' + body);
        });
        res.writeHead(httpCode, {"Content-type": "text/plain"});
        console.log('Conexão aberta. Num Abertas: ' + conexoesAbertas.toString());
        console.log('Delay: ' + delay);
        setTimeout(function () {
            conexoesAbertas--;
            res.end(JSON.stringify({
                refId: "123123",
                skuId: "321331",
                stock: "33"
            }, null, 4));
            console.log('Conexão fechada. Num abertas: ' + conexoesAbertas.toString());
        }, delay);
        //res.end(body);
    } else {
        res.end('END GET');
    }
}).listen(9001);
