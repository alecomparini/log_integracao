'use strict';
var request         = require('request');
var mysql           = require('mysql');
var program         = require('commander');
var moment          = require('moment');
var config          = require('./gera-interfaces.config');
var utils           = require('./utils');

var connection      = {
    "local": mysql.createConnection({
        host    : config.mysql.local.host,
        user    : config.mysql.local.user,
        password: config.mysql.local.pass,
        database: config.mysql.local.base,
        port    : config.mysql.local.port
    }),
    "remote": mysql.createConnection({
        host    : config.mysql.remote.host,
        user    : config.mysql.remote.user,
        password: config.mysql.remote.pass,
        database: config.mysql.remote.base,
        port    : config.mysql.remote.port
    })
};
program
    .version('0.0.1')
    .option('-i, --interface <interface>',          'Tipo Interface',           "1")
    .option('-s, --siteid <siteid>',                'SiteId',                   "2")
    .option('-p, --parceirositeid <parceirositeid>','ParceiroSiteId',           "2")
    .option('-n, --numinterfaces <numinterfaces>',  'Num. Interfaces',          "1000")
    .option('-q, --qttitems <qttitems>',            'Qtt. (MAX) Per Interface', "5")
    .parse(process.argv);

switch (program.interface) {
    case "1":
        for (let i = 0; i < parseInt(program.numinterfaces, 10); i++) {
            let numItems = utils.randInt(1, parseInt(program.qttitems, 10));
            let items = [];
            for (let numItem = 0; numItem <= numItems; numItem++) {
                items.push({
                    Codigo: "" + utils.randInt(1, 282309),
                    Estoque: "" + utils.randInt(0, 1000)
                });
            }
            let jsonStr = JSON.stringify(items);
            let insertObject = {
                Dados: jsonStr,
                DataCriacao: moment().format('YYYY-MM-DD HH:mm:ss'),
                ParceiroSiteId: program.parceirositeid,
                InterfaceTipoId: 1,
                Enviada: 0
            };
            connection.remote.query('INSERT INTO riel_integracao.Interface SET ?', insertObject, function (err, result) {
                if (err) {
                    console.log('Err: ' + err);
                }
            });
        }
        connection.remote.end();
        break;
    default:
        console.log('Interface não programada');
}