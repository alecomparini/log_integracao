module.exports = {
    /**
     * Returns a random integer between min and max (not included)
     * @param min included
     * @param max not included
     * @returns {Number}
     */
    randInt: function (min, max) {
        return parseInt(Math.random() * (max - min) + min, 10);
    }
};
