var request     = require('request');
var timeout     = process.argv[2] || 500;

console.log('Timeout = ' + timeout + ' miliseconds');

function rand(min, max) {
    return parseInt(Math.random() * (max - min) + min, 10);
}

function enviaPost() {
    var qtdItems = rand(1, 10);
    var produtos = [];
    var tipoNotificacao = 1;
    for (var i = 0; i < qtdItems; i++) {
        produtos.push(rand(1, 60000));
    }
    request({
        uri: 'http://integracao.dev/servicos/notificacao',
        method: 'POST',
        json: {
            produtos: produtos,
            tipoNotificacao: tipoNotificacao
        }
    }, function (error, response, body) {
        if (error) {
            console.log('error');
        } else {
            console.log(response.body.mensagem + ': ' + JSON.stringify(produtos));
        }
    });
    setTimeout(enviaPost, timeout);
}

enviaPost();
