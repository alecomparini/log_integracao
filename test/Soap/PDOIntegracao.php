<?php

/**
 * Class PDOIntegracao
 *
 * @package tests\Soap
 */
class PDOIntegracao {

    /**
     * @var Mysqli
     */
    private $conn;

    /**
     * CONSTRUCT A Mysqli
     */
    public function __construct() {
        $conn = new Mysqli(
            Config::get('db.master.host'),
            Config::get('db.master.user'),
            Config::get('db.master.pass'),
            Config::get('db.master.base'),
            Config::get('db.master.port'));

        if ($conn->connect_errno) {
            fwrite(STDOUT, "Falha na conexão com o banco de dados" . PHP_EOL);
            exit;
        }

        $this->conn = $conn;
    }

    /**
     * Executa as queries
     *
     * @param $query
     * @return bool|mysqli_result
     */
    public function query($query) {
        try {
            return $this->conn->query($query);
        } catch (PDOException $err) {
            fwrite(STDOUT, $err->getMessage() . PHP_EOL);
            exit;
        }
    }
}

