<?php

/**
 * Class InterfaceConfirmacao
 *
 * @package tests\Soap
 * @covers \App\Soap\InterfaceConfirmacao
 */
class InterfaceConfirmacaoTest extends SoapBase {

    // URL of WSDL
    private $soapUrl = '/ws/InterfaceConfirmacao/?wsdl';
    
    // Opcoes para realizar a chamada via Soap
    public function getOptions() {
        return array(
            'location' => Config::get('base_url') . '/ws/InterfaceConfirmacao/'
        );
    }
    
    public static function setUpBeforeClass()
    {
//        $PDOIntegracao = new PDOIntegracao;
//        $PDOIntegracao->query("UPDATE Interface I SET I.ENVIADA = 1 WHERE I.InterfaceId = 22");
    }

    /**
     * Testa se a InterfaceConfirmacao informada retorna o resultado esperado no mock
     *
     * @covers \App\Soap\InterfaceConfirmacao::setInterfaceConfirmacao
     * @test   testSetInterfaceConfirmacao
     */
    public function testSetInterfaceConfirmacao() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        // Obtem o prefixo desta classe
        $classname = parent::getDefaultClassName(__CLASS__);
        
        // Obtem o nome da funcao a ser chamada via Soap dinamicamente
        $function = parent::getDefaultFunction(__METHOD__);
        
        // Argumentos de entrada da funcao da chamada do Soap
        $arguments = array(
            'InterfaceConfirmacao' => 
                array('Confirmacao' => 1, 'InterfaceTipo' => 1, 'ParceiroId' => 2, 'Site' => 4),
            
        );
        
        // Executa chamada Soap
        $result = (array) parent::soapCall($this->soapUrl, $function, $arguments, $this->getOptions());
        
        //         sobreescreve o arquivo de mock antigo
        //         parent::filePutContents($classname, $function, $result);

        // Obtem o dado mockado
        $antigo = parent::fileGetContents($classname, $function);
        
        // echo "SE FALHOU, VERIFICAR ESSA QUERY\n SELECT * FROM " . DB_INTEGRACAO . ".Interface I WHERE I.InterfaceId = 22\n MUDAR NO BANCO DE DADOS O CAMPO   ENVIADA PARA 1!!!\n";
        
        parent::arrayDiff($antigo, $result);
    }
}