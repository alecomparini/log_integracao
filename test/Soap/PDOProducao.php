<?php

/**
 * Class PDOProducao
 *
 * @package tests\Soap
 */
class PDOProducao extends PDO {
    /**
     * @var \Mysqli
     */
    private $conn;

    // Constantes de Banco de dados
    const DB_HOST = '192.168.152.3';
    const DB_PORT = '3306';
    const DB_NAME = 'riel_producao';
    const DB_USER = 'dev';
    const DB_PASS = 'dev@2016';

    /**
     * CONSTRUCT A Mysqli
     */
    public function __construct($options = array(PDO::MYSQL_ATTR_INIT_COMMAND => 'SET NAMES utf8')) {
        $conn = new \Mysqli(
            Config::get('db.master.host'),
            Config::get('db.master.user'),
            Config::get('db.master.pass'),
            Config::get('db.master.base'),
            Config::get('db.master.port')
        );

        if ($conn->connect_errno) {
            fwrite(STDOUT, "Falha na conexão com o banco de dados" . PHP_EOL);
            exit;
        }

        $this->conn = $conn;
    }

    /**
     * Executa as queries
     *
     * @param $query
     * @return bool|mysqli_result
     */
    public function query($query) {
        try {
            return $this->conn->query($query);
        } catch (PDOException $err) {
            fwrite(STDOUT, $err->getMessage() . PHP_EOL);
            exit;
        }
    }
}