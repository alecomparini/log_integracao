<?php

/**
 * Class DadosTest
 *
 * @package tests\Soap
 * @covers \App\Soap\Dados
 */
class DadosTest extends SoapBase {

    // URL of WSDL
    private $soapUrl = '/ws/Dados/?wsdl';
    
    // Opcoes para realizar a chamada via Soap
    public function getOptions() {
        return array(
            'location' => Config::get('base_url') . '/ws/Dados/'
        );
    }

    public static function setUpBeforeClass()
    {
        $PDOIntegracao = new PDOIntegracao;
        $PDOIntegracao->query("UPDATE " . DB_INTEGRACAO . ".Interface I SET I.Enviada = 0, DataAprovacao = null WHERE I.InterfaceId = 22");
    }

    /**
     * Testa se para a Interface, ParceiroId e SiteId informados o metodo retorna um objeto com os campos desejaveis
     *
     * @covers \App\Soap\Dados::getDados
     * @test   testGetDados
     */
    public function testGetDados() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        // Obtem o prefixo desta classe
        $classname = parent::getDefaultClassName(__CLASS__);
        
        // Obtem o nome da funcao a ser chamada via Soap dinamicamente
        $function = parent::getDefaultFunction(__METHOD__);
        
        // Argumentos de entrada da funcao da chamada do Soap
        $arguments = array(
            'Interfaces' => 
                array('Interface' => 1, 'ParceiroId' => 2, 'SiteId' => 4),
        );
        
        // Executa chamada Soap
        $result = (array) parent::soapCall($this->soapUrl, $function, $arguments, $this->getOptions());

        $this->assertTrue(array_key_exists("Codigo", $result));
        $this->assertTrue(array_key_exists("Dados", $result));
        $this->assertTrue(array_key_exists("Msg", $result));
    }
}