<?php

/**
 * Class ProdutoFullTest
 *
 * @package tests\Soap
 * @covers \App\Soap\ProdutoFull
 */
class ProdutoFullTest extends SoapBase {

    // URL of WSDL
    private $soapUrl = '/ws/ProdutoFull/?wsdl';

    // Opcoes para realizar a chamada via Soap
    public function getOptions() {
        return array(
            'location' => Config::get('base_url') . '/ws/ProdutoFull/'
        );
    }

    /**
     * Testa se para o ParceiroId e ProdutoCodigo informados ira retornar os dados dos esperados dos produtos
     *
     * @covers \App\Soap\ProdutoFull::getProdutoFull
     * @test   testGetProdutoFull
     */
    public function testGetProdutoFull() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        // Obtem o prefixo desta classe
        $classname = parent::getDefaultClassName(__CLASS__);

        // Obtem o nome da funcao a ser chamada via Soap dinamicamente
        $function = parent::getDefaultFunction(__METHOD__);

        // Argumentos de entrada da funcao da chamada do Soap
        $arguments = array(
            'ParceiroId' => 2,
            'ProdutoCodigo' => array('13377'),
        );

        // Exemplo de chamada Soap
        $result = (array)parent::soapCall($this->soapUrl, $function, $arguments, $this->getOptions());
        if ('0' == $result['Codigo']) {
            // Ordenar para evitar erros na comparação de string de campos desordenados
            $result["Dados"] = json_decode($result["Dados"]);
            $result["Dados"] = json_encode(ksort($result["Dados"]));

            // Obtem o dado mockado
            $antigo = parent::fileGetContents($classname, $function);
            $antigo["Dados"] = json_decode($antigo["Dados"]);
            // Ordenar para evitar erros na comparação de string de campos desordenados
            $antigo["Dados"] = json_encode(ksort($antigo["Dados"]));

            parent::arrayDiff($antigo, $result);
        } else {
            $this->fail('A chamada ao webservice não retornou sucesso com a seguinte mensagem: ' . $result['Msg']);
        }
    }
}