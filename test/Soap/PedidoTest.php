<?php

/**
 * Class PedidoTest
 *
 * @package tests\Soap
 * @covers \App\Soap\Pedido
 */
class PedidoTest extends SoapBase {

    // URL of WSDL
    private $soapUrl = '/ws/Pedido/?wsdl';
    
    // Opcoes para realizar a chamada via Soap
    public function getOptions() {
        return array(
            'location' => Config::get('base_url') . '/ws/Pedido/'
        );
    }
    
    /**
     * Testa se o pedido enviado por um parceiro eh integrado
     *
     * @dataProvider pedidoProvider
     * @covers \App\Soap\Pedido::setPedido
     * @test   testSetPedido
     */
    public function testSetPedido($pedido) {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        // Obtem o prefixo desta classe
        $classname = parent::getDefaultClassName(__CLASS__);
        
        // Obtem o nome da funcao a ser chamada via Soap dinamicamente
        $function = parent::getDefaultFunction(__METHOD__);
        
        // Argumentos de entrada da funcao da chamada do Soap
        $arguments = $pedido;

        // Exemplo de chamada Soap
        $result = (array) parent::soapCall($this->soapUrl, $function, $arguments, $this->getOptions());
        
        //         sobreescreve o arquivo de mock antigo
        //         parent::filePutContents($classname, $function, $result);

        // Obtem o dado mockado
        $antigo = parent::fileGetContents($classname, $function);

        parent::arrayDiff($antigo, $result);
    }

    public function pedidoProvider() {
        return array( array(array(
            'Pedido' => array(
                'ClienteEmail' => 'saba@saba.com.br',
                'ClientePfCPF' => '64767738792',
                'ClientePfDataNascimento' => '1980-05-31',
                'ClientePfNome' => 'Saba',
                'ClientePfSexo' => 'M',
                'ClientePfSobrenome' => 'da Silva',
                'ClientePjCNPJ' => null,
                'ClientePjInscEstadual' => null,
                'ClientePjIsento' => null,
                'ClientePjNomeFantasia' => null,
                'ClientePjRamoAtividade' => null,
                'ClientePjRazaoSocial' => null,
                'ClientePjTelefone' => null,
                'ClienteTipo' => 'pf',
                'EndCobrancaBairro' => 'Brasil',
                'EndCobrancaCelular' => '34999998888',
                'EndCobrancaCep' => '38400098',
                'EndCobrancaCidade' => 'Uberlândia',
                'EndCobrancaComplemento' => 'Empresa',
                'EndCobrancaDestinatario' => 'Saba',
                'EndCobrancaEndereco' => 'Av. Cesário Alvim',
                'EndCobrancaEstado' => 'MG',
                'EndCobrancaNumero' => 3345,
                'EndCobrancaReferencia' => 'Justiça Federal',
                'EndCobrancaTelefone1' => '3432383300',
                'EndCobrancaTelefone2' => '3432383300',
                'EndCobrancaTipo' => 'Comercial',
                'EndEntregaBairro' => 'Brasil',
                'EndEntregaCelular' => '34999998888',
                'EndEntregaCep' => '38400098',
                'EndEntregaCidade' => 'Uberlândia',
                'EndEntregaComplemento' => 'Comercial',
                'EndEntregaDestinatario' => 'Saba',
                'EndEntregaEndereco' => 'Av. Cesário Alvim',
                'EndEntregaEstado' => 'MG',
                'EndEntregaNumero' => 3345,
                'EndEntregaReferencia' => 'Justiça Federal',
                'EndEntregaTelefone1' => '3432383300',
                'EndEntregaTelefone2' => '3432383300',
                'EndEntregaTipo' => 'Comercial',
                'Pagamento' => array(
                    array(
                        'BandeiraId' => 1,
                        'CartaoCodSeguranca' => 222,
                        'CartaoNomePortador' => 'Teste 1',
                        'CartaoNumero' => '5555666677778884',
                        'CartaoValidade' => date('m/Y', strtotime("+4 years")),
                        'MeioPagamentoId' => 1,
                        'Parcelas' => 1,
                        'PercentualJuros' => 0,
                        'Sequencial' => 1,
                        'ValorJuros' => 0,
                        'ValorPago' => 1499.59,
                    ),
                ),
                'ParceiroId' => 11,
                'ParceiroPedidoId' => intval(microtime(true)),
                'PedidoProduto' => array(
                    array(
                        'CodigoProduto' => 514124,
                        'PrazoEntregaCombinado' => 5,
                        'Quantidade' => 1,
                        'Sequencial' => 1,
                        'Tipo' => 1,
                        'ValorDesconto' => 1.00,
                        'ValorFrete' => 16.99,
                        'ValorTotal' => 1483.6,
                        'ValorUnitario' => 1483.6,
                    ),
                ),
                'SiteId' => 32,
                'ValorJuros' => 0,
                'ValorTotal' => 1499.59,
                'ValorTotalFrete' => 16.99,
                'ValorTotalProdutos' => 1483.60,
                'DataEntrega' => date('Y-m-d', strtotime("+25 days")),
                'TurnoEntrega' => 1,
                'ValorEntrega' => 1,
                'VendedorToken' => '',
            )
        )));
    }

}