<?php

/**
 * Class SoapBase
 *
 * @package tests\Soap
 */
class SoapBase extends PHPUnit_Framework_TestCase {
    
    /**
     * Cria uma chamada via Soap
     * @param string $soapUrl
     * @param string $function
     * @param array $arguments
     * @param array $options
     * @return mixed
     */
    function soapCall($soapUrl, $function, $arguments, $options) {
        $client = new SoapClient(Config::get('base_url') . $soapUrl);
//         $functions = $client->__getFunctions ();
//         var_dump ($functions);
        return $client->__soapCall($function, $arguments, $options);
    }
    
    /**
     * Verifica as diferencas encontradas entre dois arrays e exibe elas
     * @param array $antigo
     * @param array $novo
     */
    function arrayDiff($antigo, $novo) {
        if(is_array($antigo) && is_array($novo)) {
            ksort($antigo);
            ksort($novo);            
            $arrayDiff = array_diff_assoc($antigo, $novo);
            $arrayDiff2 = array_diff_assoc($novo, $antigo);
            if(empty($arrayDiff)){
                $this->assertTrue(true);
            } else {
                $msg =  "----------------Teste falhou---------------\n\n";
                $msg .= "DIFERENCAS ENCONTRADAS\nResultado esperado:\n";
                $msg .= print_r($arrayDiff, true);
                $msg .= "\nResultado obtido:\n";
                $msg .= print_r($arrayDiff2, true);
                $msg .= "--------------------------------------------\n\n";
                $this->assertTrue(false, $msg);
            }
        } else {
            $this->assertTrue(false, "Verifique se os argumentos são do tipo array");
        }
    }
    
    /**
     * Retorna o nome de uma funcao ao encontrar um padrao  
     * @param string $str
     * @return string
     */
    function getDefaultFunction($str) {
        return lcfirst(substr($str, strpos($str,"test")+4));
    }
    
    /**
     * Retorna o nome de uma classe ao encontrar um padrao
     * @param string $str
     * @return string
     */
    function getDefaultClassName($str) {
        $pos = strpos($str,"Test");
        return substr($str, 0, $pos);
    }
    
    /**
     * Recebe o nome de uma classe e de um metodo para garavar os dados em arquivo passados pela variavel $data
     * @param string $filename
     * @param string $function
     * @param array $data
     */
    function filePutContents($filename, $function, $data) {
        file_put_contents(MOCK_DIR.$filename."_".$function.".json",json_encode($data));
    }
    
    /**
     * Recebe o nome de uma classe e de um metodo para ler os dados em arquivo 
     * @param unknown $filename
     * @param unknown $function
     * @return mixed
     */
    function fileGetContents($filename, $function) {
        return json_decode(file_get_contents(MOCK_DIR.$filename."_"."$function.json"), true);
    }
}