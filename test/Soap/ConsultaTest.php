<?php

/**
 * Class ConsultaTest
 *
 * @package tests\Soap
 * @covers \App\Soap\Consulta
 */
class ConsultaTest extends SoapBase {

    // URL of WSDL
    private $soapUrl = '/ws/Consulta/?wsdl';

    // Opcoes para realizar a chamada via Soap

    public function getOptions() {
        return array(
            'location' => Config::get('base_url') . '/ws/Consulta/'
        );
    }

    /**
     * Testa se para o ParceiroId e ProdutoCodigo informados o metodo retorna o resultado esperado que encontra-se no mock
     *
     * @covers \App\Soap\Consulta::getEstoque
     * @test   testGetEstoque
     */
    public function testGetEstoque() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        // Obtem o prefixo desta classe
        $classname = parent::getDefaultClassName(__CLASS__);

        // Obtem o nome da funcao a ser chamada via Soap dinamicamente
        $function = parent::getDefaultFunction(__METHOD__);

        // Argumentos de entrada da funcao da chamada do Soap
        $arguments = array(
            'ParceiroId' => 2,
            'ProdutoCodigo' => '100000'
        );

        // Executa chamada Soap
        $result = (array)parent::soapCall($this->soapUrl, $function, $arguments, $this->getOptions());

//         sobreescreve o arquivo de mock antigo
//         parent::filePutContents($classname, $function, $result);

        // Obtem o dado mockado 
        $antigo = parent::fileGetContents($classname, $function);

        parent::arrayDiff($antigo, $result);
    }

    /**
     * Testa se para o ParceiroId, Cep e Produtos informados o metodo retorna o resultado esperado que encontra-se no mock
     *
     * @covers \App\Soap\Consulta::getFrete
     * @test   testGetFrete
     */
    public function testGetFrete() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        // Obtem o prefixo desta classe
        $classname = parent::getDefaultClassName(__CLASS__);

        // Obtem o nome da funcao a ser chamada via Soap dinamicamente
        $function = parent::getDefaultFunction(__METHOD__);

        // Argumentos de entrada da funcao da chamada do Soap
        $arguments = array(
            'ParceiroId' => 2,
            'Cep' => '38415129',
            'Produtos' => array(
                array("Codigo" => 1, "Quantidade" => 1),
            )

        );

        // Executa chamada Soap
        $result = (array)parent::soapCall($this->soapUrl, $function, $arguments, $this->getOptions());

        //         sobreescreve o arquivo de mock antigo
        //         parent::filePutContents($classname, $function, $result);

        $antigo = parent::fileGetContents($classname, $function);

        parent::arrayDiff($antigo, $result);
    }

    /**
     * Testa se para o ParceiroId, SiteId e ProdutoCodigo informados o metodo retorna o resultado esperado que encontra-se no mock
     *
     * @covers \App\Soap\Consulta::getProduto
     * @test   testGetProduto
     */
    public function testGetProduto() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        // Obtem o prefixo desta classe
        $classname = parent::getDefaultClassName(__CLASS__);

        // Obtem o nome da funcao a ser chamada via Soap dinamicamente
        $function = parent::getDefaultFunction(__METHOD__);

        // Argumentos de entrada da funcao da chamada do Soap
        $arguments = array(
            'ParceiroId' => 2,
            'SiteId' => 4,
            'ProdutoCodigo' => 8019,
        );

        // Executa chamada Soap
        $result = (array)parent::soapCall($this->soapUrl, $function, $arguments, $this->getOptions());

        // Ordenar para evitar erros na comparação de string de campos desordenados
        $result["Dados"] = json_decode($result["Dados"]);
        $result["Dados"] = json_encode(ksort($result["Dados"]));

        //         sobreescreve o arquivo de mock antigo
        //         parent::filePutContents($classname, $function, $result);

        // Obtem o dado mockado
        $antigo = parent::fileGetContents($classname, $function);
        $antigo["Dados"] = json_decode($antigo["Dados"]);
        // Ordenar para evitar erros na comparação de string de campos desordenados
        $antigo["Dados"] = json_encode(ksort($antigo["Dados"]));
        parent::arrayDiff($antigo, $result);
    }

    /**
     * Testa se para o ParceiroId, Email e Senha informados o metodo retorna o resultado esperado que encontra-se no mock
     *
     * @covers \App\Soap\Consulta::getVendedor
     * @test   testGetVendedor
     */
    public function testGetVendedor() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        // Obtem o prefixo desta classe
        $classname = parent::getDefaultClassName(__CLASS__);

        // Obtem o nome da funcao a ser chamada via Soap dinamicamente
        $function = parent::getDefaultFunction(__METHOD__);

        // Argumentos de entrada da funcao da chamada do Soap
        $arguments = array(
            'ParceiroId' => 2,
            'Email' => 'igorsop@gmail.com',
            'Senha' => 123456,
        );

        // Executa chamada Soap
        $result = (array)parent::soapCall($this->soapUrl, $function, $arguments, $this->getOptions());

        // Ordenar para evitar erros na comparação de string de campos desordenados
        $result["Dados"] = json_decode($result["Dados"]);
        $result["Dados"] = json_encode(ksort($result["Dados"]));

        //         sobreescreve o arquivo de mock antigo
        //         parent::filePutContents($classname, $function, $result);

        // Obtem o dado mockado        
        $antigo = parent::fileGetContents($classname, $function);
        $antigo["Dados"] = json_decode($antigo["Dados"]);
        // Ordenar para evitar erros na comparação de string de campos desordenados
        $antigo["Dados"] = json_encode(ksort($antigo["Dados"]));
        parent::arrayDiff($antigo, $result);
    }
}
