<?php



/**
 * Class VendedorBOTest
 *
 * @package tests\BO
 * @covers \App\BO\VendedorBO
 */
class VendedorBOTest extends PHPUnit_Framework_TestCase
{

    /**
     * @covers \App\BO\VendedorBO::getVendedor
     */
    public function testGetVendedor(){
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $VendedorBO = new \App\BO\VendedorBO();
        $Vendedor = $VendedorBO->getVendedor('igorsop@gmail.com', '123456');
        $this->assertInstanceOf('\App\Model\Vendedor', $Vendedor, 'Vendedor não foi instanciado corretamente.');

        try {
            $VendedorBO->getVendedor(null, null);
            $this->assertTrue(false, "Deveria ter dado excecao");
        } catch (\Exception $e) {
            $this->assertEquals(2, $e->getCode());
        }

    }


}