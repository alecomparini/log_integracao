<?php

use App\BO\ProdutoBO;


/**
 * Class ProdutoBOTest
 *
 * @package tests\BO
 * @covers \App\BO\ProdutoBO
 */
class ProdutoBOTest extends PHPUnit_Framework_TestCase {

    /**
     * @var ProdutoBO
     */
    public static $produtoBO;

    /**
     * Instancia o \App\BO\ProdutoBO
     * @method setUpBeforeClass
     */
    public static function setUpBeforeClass() {
        self::$produtoBO = new ProdutoBO();
    }

    /**
     * @covers \App\BO\ProdutoBO::__construct
     */
    public function testConstruct() {
        $bo = new ProdutoBO();
        $this->assertInstanceOf('\App\BO\ProdutoBO', $bo, "Erro ao instânciar produto BO");
    }

    /**
     * @covers \App\BO\ProdutoBO::getProdutoFull
     */
    public function testGetProdutoFull() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        try {
            $result = static::$produtoBO->getProdutoFull(2, array('8019', '13377', '24551', '24561', '30714'));
            $this->assertTrue(is_array($result), "Resultado não é um array");
        } catch (\Exception $e) {
            $this->fail($e->getMessage());
        }

        try {
            static::$produtoBO->getProdutoFull(2, array('6666'));
            $this->fail('Busca por 6666 deveria retornar um exception');
        } catch (\Exception $e) {
            $this->assertTrue(true);
        }

        try {
            static::$produtoBO->getProdutoFull(666, array('8019'));
            $this->fail('Busca por 6666 deveria retornar um exception');
        } catch (\Exception $e) {
            $this->assertTrue(true);
        }

        $res = static::$produtoBO->getProdutoFull(2, array('C39056', 'C39574'));

    }

    /**
     * @covers \App\BO\ProdutoBO::getFatoresCartao
     */
    public function testGetFatoresCartao() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $bo = new ProdutoBO();
        $reflectionBo = new \ReflectionClass(get_class($bo));
        $method = $reflectionBo->getMethod('getFatoresCartao');
        $method->setAccessible(true);
        $res = $method->invokeArgs($bo, array());
        $this->assertTrue(is_array($res), 'Não foi retornado um array');
        $this->assertEquals(12, count($res), 'Deveria retornar 12 fatores de cartão');
        $this->assertEquals(1, $res[1], 'Fator 1 deveria ser 1');
    }

    /**
     * @covers \App\BO\ProdutoBO::findProdutoFull
     */
    public function testFindProdutoFull() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $bo = new ProdutoBO();
        $reflectionBo = new \ReflectionClass(get_class($bo));
        $method = $reflectionBo->getMethod('findProdutoFull');
        $method->setAccessible(true);

        $res = $method->invokeArgs($bo, array(array(30441,30451), array(), 1, 40461));
        $this->assertTrue(is_array($res), 'Não foi retornado um array');
        $this->assertEquals(2, count($res), 'Deveria retornar 2 produtos');

        $res = $method->invokeArgs($bo, array(array(11774, 'C47104'), array(), 1));
        $this->assertTrue(is_array($res), 'Não foi retornado um array');
        $this->assertEquals(1, count($res), 'Deveria retornar 1 produto');

//        TODO teste para verificar quando $valorParcela < R$ 5
//        $res = $method->invokeArgs($bo, array(array(4221), array(), 1));
//        $this->assertTrue(is_array($res), 'Não foi retornado um array');
//        $this->assertEquals(1, count($res), 'Deveria retornar 1 produto');

    }

    /**
     * @covers \App\BO\ProdutoBO::getEstoques
     */
    public function testGetEstoques() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $bo = new ProdutoBO();
        $parceiroId = 2;
        $result = $bo->getEstoques($parceiroId);
        $produtoCodigo = array(8019);
        $result2 = $bo->getEstoques($parceiroId, $produtoCodigo);
        $produtoCodigo = array(8019, 13377);
        $result3 = $bo->getEstoques($parceiroId, $produtoCodigo);

        $result4 = $bo->getEstoques($parceiroId, array());

        $this->assertTrue(is_array($result));
        $this->assertTrue(is_array($result2));
        $this->assertTrue(is_array($result3));
        $this->assertTrue(is_array($result4));


        try {
            $bo->getEstoques($parceiroId, new \stdClass());
        } catch (\Exception $e ) {
            $this->assertEquals(2, $e->getCode(), "Deveria ter retornado erro de validacao de tipo");
            $this->assertInstanceOf("\App\Exceptions\ValidacaoException", $e, "Nao e do tipo ValidacaoException");
        }

        try {
            $bo->getEstoques($parceiroId, array(8019, false));
        } catch (\Exception $e ) {
            $this->assertEquals(2, $e->getCode(), "Deveria ter retornado erro de validacao de tipo");
            $this->assertInstanceOf("\App\Exceptions\ValidacaoException", $e, "Nao e do tipo ValidacaoException");
        }
    }

    /**
     * @covers \App\BO\ProdutoBO::getProduto
     * @group dev
     */
    public function testGetProduto() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $bo = new ProdutoBO();
        $siteId = 1;
        $produtoCodigo = 8019;
        $parceiroId = 2;
        $res = $bo->getProduto($parceiroId, $siteId, $produtoCodigo);

        $this->assertTrue(is_array($res), 'Não foi retornado um array');
        $this->assertEquals(1, count($res), 'Deveria retornar 1 produto');

        try {
            $bo->getProduto(8648465, $siteId, $produtoCodigo);
            $this->fail('Deveria ter retornado erro de parceiro invalido');
        } catch (\Exception $e ) {
            $this->assertEquals(0, $e->getCode(), "Deveria ter retornado Código de parceiro inválido");
        }

        $res = $bo->getProduto($parceiroId, 4, null);
        $this->assertTrue(is_array($res), 'Não foi retornado um array');

        try {
            $bo->getProduto($parceiroId, 4, '123134342');
            $this->fail('Deveria ter retornado erro de produto invalido');
        } catch (\Exception $e ) {
            $this->assertEquals(10, $e->getCode(), "Deveria ter retornado Erro: Produto não encontrado");
        }
    }

}
