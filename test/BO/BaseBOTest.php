<?php

use \App\BO\ProdutoBO;

/**
 * Created by PhpStorm.
 * User: leonardo
 * Date: 29/01/16
 * Time: 11:52
 * @package tests\BO
 * @covers \App\BO\BaseBO
 */
class BaseBOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @covers \App\BO\BaseBO::__construct
     */
    public function testConstruct() {
        $baseBO = new ProdutoBO();
        self::assertInstanceOf('\App\BO\BaseBO', $baseBO);
    }
}