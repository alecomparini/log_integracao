<?php

use App\Core\Config as CoreConfig;

class Config extends CoreConfig {
    
    /**
     * Carrega um arquivo .env de um determinado ambiente no seu sistema
     * Ex: test.env e app.env
     * @method load
     * @param  string $env Nome do ambiente, prefixo do arquivo .env
     */
    public function load($env) {
        $this->setEnv($env);
    }
    
}
