use riel_integracao;
-- MySQL dump 10.13  Distrib 5.7.10, for Win64 (x86_64)
--
-- Host: 192.168.152.3    Database: riel_integracao
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `DadosImportacao`
--

DROP TABLE IF EXISTS `DadosImportacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `DadosImportacao` (
  `DadosImportacaoId` int(11) NOT NULL AUTO_INCREMENT,
  `Email` varchar(60) NOT NULL DEFAULT '',
  `DataCadastro` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Tipo` enum('pf','pj') NOT NULL DEFAULT 'pf',
  `SiteId` int(11) DEFAULT NULL,
  `Nome` varchar(30) NOT NULL DEFAULT '',
  `Sobrenome` varchar(70) NOT NULL DEFAULT '',
  `CPF` varchar(14) NOT NULL DEFAULT '',
  `DataNascimento` date NOT NULL DEFAULT '0000-00-00',
  `Sexo` enum('M','F') NOT NULL DEFAULT 'M',
  `RazaoSocial` varchar(100) NOT NULL DEFAULT '',
  `NomeFantasia` varchar(100) NOT NULL DEFAULT '',
  `CNPJ` varchar(18) NOT NULL DEFAULT '',
  `InscricaoEstadual` varchar(14) DEFAULT NULL,
  `Isento` int(1) NOT NULL DEFAULT '0' COMMENT 'se = 0 nao isento, se = 1 isento',
  `Telefone` varchar(14) DEFAULT NULL,
  `Site` varchar(150) DEFAULT NULL,
  `RamoAtividade` tinyint(4) DEFAULT '0',
  `CobrancaTipo` enum('Residencial','Comercial') NOT NULL DEFAULT 'Residencial',
  `CobrancaDestinatario` varchar(100) NOT NULL DEFAULT '',
  `CobrancaCep` varchar(9) NOT NULL DEFAULT '',
  `CobrancaEndereco` varchar(100) NOT NULL DEFAULT '',
  `CobrancaNumero` int(11) DEFAULT NULL,
  `CobrancaComplemento` varchar(45) DEFAULT NULL,
  `CobrancaBairro` varchar(50) NOT NULL DEFAULT '',
  `CobrancaCidade` varchar(50) NOT NULL DEFAULT '',
  `CobrancaEstado` char(2) NOT NULL DEFAULT '',
  `CobrancaTelefone1` varchar(13) NOT NULL DEFAULT '',
  `CobrancaTelefone2` varchar(13) DEFAULT NULL,
  `CobrancaCelular` varchar(13) DEFAULT NULL,
  `CobrancaReferencia` varchar(100) DEFAULT NULL,
  `EntregaTipo` enum('Residencial','Comercial') NOT NULL DEFAULT 'Residencial',
  `EntregaDestinatario` varchar(100) NOT NULL DEFAULT '',
  `EntregaCep` varchar(9) NOT NULL DEFAULT '',
  `EntregaEndereco` varchar(100) NOT NULL DEFAULT '',
  `EntregaNumero` int(11) DEFAULT NULL,
  `EntregaComplemento` varchar(45) DEFAULT NULL,
  `EntregaBairro` varchar(50) NOT NULL DEFAULT '',
  `EntregaCidade` varchar(50) NOT NULL DEFAULT '',
  `EntregaEstado` char(2) NOT NULL DEFAULT '',
  `EntregaTelefone1` varchar(13) NOT NULL DEFAULT '',
  `EntregaTelefone2` varchar(13) DEFAULT NULL,
  `EntregaCelular` varchar(13) DEFAULT NULL,
  `EntregaReferencia` varchar(100) DEFAULT NULL,
  `CupomDescontoId` int(11) DEFAULT '0',
  `ValorTotal` decimal(12,2) unsigned NOT NULL,
  `ValorTotalFrete` decimal(9,2) unsigned DEFAULT NULL,
  `ValorCupomDesconto` decimal(9,2) unsigned DEFAULT '0.00',
  `ValorCupomDescontoFrete` decimal(9,2) unsigned DEFAULT '0.00',
  `IP` varchar(15) DEFAULT NULL,
  `StatusExportacao` tinyint(4) DEFAULT '0',
  `ValorTotalProdutos` decimal(12,2) unsigned DEFAULT NULL,
  `ValorDespesas` decimal(9,2) unsigned DEFAULT '0.00',
  `ValorJuros` decimal(9,2) unsigned DEFAULT NULL,
  `ValorFreteSemPromocao` decimal(9,2) unsigned DEFAULT NULL,
  `ParceiroPedidoId` int(10) unsigned NOT NULL COMMENT 'PedidoId do Parceiro',
  `ProducaoPedidoId` int(10) unsigned DEFAULT NULL COMMENT 'PedidoId do riel_producao',
  `ParceiroSiteId` mediumint(8) NOT NULL COMMENT 'ParceiroSiteId coluna de relacionamento ParceiroSite',
  `DataEntrega` date NOT NULL,
  `TurnoEntrega` tinyint(1) unsigned NOT NULL COMMENT '1 => Manha, 2 => Tarde, 3 => Noite',
  `ValorEntrega` float unsigned NOT NULL COMMENT 'Valor do frete da entrega agendada',
  `VendedorId` int(11) unsigned DEFAULT NULL COMMENT 'Id do Vendedor B2c',
  `GrupoVendedorId` int(11) unsigned DEFAULT NULL COMMENT 'Grupo do Vendedor B2c',
  PRIMARY KEY (`DadosImportacaoId`)
) ENGINE=InnoDB AUTO_INCREMENT=2288 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Interface`
--

DROP TABLE IF EXISTS `Interface`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Interface` (
  `InterfaceId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Dados` text COMMENT 'Campo para gravar o objeto Json com o Codigo e Estoque Disponível',
  `DataCriacao` datetime NOT NULL,
  `DataAprovacao` datetime DEFAULT NULL COMMENT 'Confirmação de envio do parceiro',
  `ParceiroSiteId` mediumint(8) unsigned NOT NULL,
  `InterfaceTipoId` tinyint(1) unsigned DEFAULT NULL COMMENT 'Relacionamento com a tabela InterfaceTipo',
  `Enviada` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT 'Flag de envio para o Parceiro',
  PRIMARY KEY (`InterfaceId`),
  KEY `idx_interfaceid_enviada` (`Enviada`) USING HASH,
  KEY `idx_interfaceid_interfacetipo` (`InterfaceTipoId`) USING HASH
) ENGINE=InnoDB AUTO_INCREMENT=2005 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `InterfacePermissao`
--

DROP TABLE IF EXISTS `InterfacePermissao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `InterfacePermissao` (
  `InterfacePermissaoId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ParceiroSiteId` mediumint(8) unsigned NOT NULL COMMENT 'Relacionamento com a tabela Parceiro_Site',
  `InterfaceTipoId` tinyint(3) unsigned NOT NULL COMMENT 'Relacionamento com a tabela InterfaceTipo',
  PRIMARY KEY (`InterfacePermissaoId`)
) ENGINE=InnoDB AUTO_INCREMENT=64 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `InterfaceTipo`
--

DROP TABLE IF EXISTS `InterfaceTipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `InterfaceTipo` (
  `InterfaceTipoId` tinyint(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(30) NOT NULL,
  `Status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`InterfaceTipoId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `InterfaceTracking`
--

DROP TABLE IF EXISTS `InterfaceTracking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `InterfaceTracking` (
  `PedidoId` int(11) unsigned NOT NULL COMMENT 'Id Pedido Site',
  `ParceiroId` int(11) unsigned NOT NULL COMMENT 'Id Parceiro Integracao',
  `PedidoParceiroId` int(11) unsigned NOT NULL COMMENT 'Pedido Parceiro',
  `Json` text NOT NULL COMMENT 'Json Retorno Info Tracking',
  `Hit` tinyint(3) unsigned NOT NULL DEFAULT '0' COMMENT 'Quantidade de Consumo',
  `Versao` char(3) NOT NULL COMMENT 'Versao do JSON',
  `DataCriacao` datetime NOT NULL COMMENT 'Data de Criação do Tracking',
  `DataAlteracao` datetime NOT NULL COMMENT 'Data de Alteração do Tracking',
  `DataUltimoConsumo` datetime DEFAULT NULL COMMENT 'Data do Ultimo Consumo do Tracking',
  PRIMARY KEY (`PedidoId`),
  KEY `IdxPedidosParceiros` (`PedidoParceiroId`,`ParceiroId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pagamento`
--

DROP TABLE IF EXISTS `Pagamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pagamento` (
  `PagamentoId` int(11) NOT NULL AUTO_INCREMENT,
  `DadosIntegracaoId` int(11) NOT NULL,
  `MeioPagamentoId` int(11) NOT NULL,
  `BandeiraId` int(11) DEFAULT NULL,
  `ValorPago` decimal(12,2) NOT NULL,
  `Parcelas` tinyint(4) NOT NULL,
  `PercentualJuros` float DEFAULT '0',
  `ValorJuros` float DEFAULT '0',
  `CartaoNumero` varchar(19) DEFAULT NULL,
  `CartaoValidade` char(8) DEFAULT NULL,
  `CartaoCodSeguranca` varchar(5) DEFAULT NULL,
  `CartaoNomePortador` varchar(30) DEFAULT NULL,
  `BoletoUrl` text,
  `DataVencimento` date DEFAULT NULL,
  `CodigoVale` varchar(70) DEFAULT NULL,
  `Sequencial` int(11) DEFAULT NULL,
  PRIMARY KEY (`PagamentoId`)
) ENGINE=InnoDB AUTO_INCREMENT=2292 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Parceiro`
--

DROP TABLE IF EXISTS `Parceiro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Parceiro` (
  `ParceiroId` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(50) NOT NULL,
  `Status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`ParceiroId`)
) ENGINE=InnoDB AUTO_INCREMENT=24 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Parceiro_Site`
--

DROP TABLE IF EXISTS `Parceiro_Site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Parceiro_Site` (
  `ParceiroSiteId` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `ParceiroId` tinyint(1) unsigned NOT NULL,
  `SiteId` tinyint(1) unsigned NOT NULL,
  `SiteBase` tinyint(1) unsigned NOT NULL COMMENT 'Site onde serão executadas as consultas para buscar os produtos',
  `Status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`ParceiroSiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Parceiro_SiteIp`
--

DROP TABLE IF EXISTS `Parceiro_SiteIp`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Parceiro_SiteIp` (
  `ParceiroSiteIpId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ParceiroSiteId` mediumint(8) unsigned NOT NULL,
  `Ip` char(15) DEFAULT NULL,
  PRIMARY KEY (`ParceiroSiteIpId`)
) ENGINE=InnoDB AUTO_INCREMENT=1239 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Parceiro_Site_Permissao`
--

DROP TABLE IF EXISTS `Parceiro_Site_Permissao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Parceiro_Site_Permissao` (
  `ParceiroSitePermissaoId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ParceiroSiteId` mediumint(8) unsigned NOT NULL,
  `PermissaoTipoId` tinyint(1) unsigned NOT NULL COMMENT '1-Fabricante e 2-CategoriaId',
  `Permissao` int(10) unsigned NOT NULL COMMENT 'Valor do campo PermissaoTipoId',
  `Excecao` tinyint(1) unsigned NOT NULL COMMENT '0-Permissao e 1-Excessao',
  PRIMARY KEY (`ParceiroSitePermissaoId`)
) ENGINE=InnoDB AUTO_INCREMENT=6794 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pedido_Garantia`
--

DROP TABLE IF EXISTS `Pedido_Garantia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pedido_Garantia` (
  `ProdutoGarantiaId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `DadosImportacaoId` int(11) unsigned NOT NULL,
  `PedidoProdutoId` int(11) unsigned NOT NULL,
  `GarantiaCodigo` int(11) unsigned NOT NULL COMMENT 'Referência da tabela riel_producao.GarantiaProduto',
  `Quantidade` tinyint(2) unsigned DEFAULT NULL,
  `Prazo` tinyint(2) NOT NULL,
  `ValorVenda` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`ProdutoGarantiaId`)
) ENGINE=InnoDB AUTO_INCREMENT=349 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pedido_Produto`
--

DROP TABLE IF EXISTS `Pedido_Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pedido_Produto` (
  `PedidoProdutoId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `DadosImportacaoId` int(10) unsigned NOT NULL,
  `ProdutoId` int(11) NOT NULL DEFAULT '0',
  `ProdutoCodigo` varchar(45) DEFAULT NULL,
  `Quantidade` int(11) NOT NULL DEFAULT '0',
  `ValorUnitario` decimal(9,2) NOT NULL,
  `ValorTotal` decimal(12,2) NOT NULL,
  `ValorFrete` float DEFAULT NULL,
  `Tipo` char(1) DEFAULT NULL COMMENT 'Produto ou não produto',
  `PrazoEntregaCombinado` int(11) DEFAULT NULL,
  `Sequencial` tinyint(4) DEFAULT NULL,
  `ValorDesconto` float DEFAULT NULL,
  `ValorCustoEntrada` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`PedidoProdutoId`)
) ENGINE=InnoDB AUTO_INCREMENT=2308 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pedido_Seguro`
--

DROP TABLE IF EXISTS `Pedido_Seguro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pedido_Seguro` (
  `PedidoSeguroId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `DadosImportacaoId` int(11) unsigned NOT NULL,
  `PedidoProdutoId` int(11) unsigned NOT NULL,
  `SeguroTabelaId` int(11) NOT NULL,
  `Quantidade` tinyint(2) unsigned DEFAULT NULL,
  `ValorVenda` decimal(10,2) unsigned DEFAULT NULL,
  `Familia` tinyint(3) DEFAULT NULL,
  PRIMARY KEY (`PedidoSeguroId`)
) ENGINE=InnoDB AUTO_INCREMENT=377 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PermissaoTipo`
--

DROP TABLE IF EXISTS `PermissaoTipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PermissaoTipo` (
  `PermissaoTipoId` tinyint(1) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`PermissaoTipoId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

DROP TABLE IF EXISTS riel_integracao.NotificacaoParceiro;
CREATE TABLE riel_integracao.NotificacaoParceiro (
  ParceiroId INT(11) UNSIGNED NOT NULL,
  SiteId INT(11) UNSIGNED NOT NULL,
  ProdutoCodigo VARCHAR(45) NOT NULL,
  DataCriacao DATETIME NOT NULL DEFAULT NOW(),
  DataUltimaTentativa DATETIME,
  Tentativas TINYINT(2) DEFAULT 0,
  ModificacaoPreco TINYINT(1) DEFAULT 0,
  ModificacaoEstoque TINYINT(1) DEFAULT 0,
  PRIMARY KEY (ParceiroId, ProdutoCodigo)
);

DROP TABLE IF EXISTS riel_integracao.Parceiro_Configuracao;
DROP TABLE IF EXISTS riel_integracao.Configuracao;
CREATE TABLE riel_integracao.Configuracao (
  ConfiguracaoId INT          NOT NULL PRIMARY KEY AUTO_INCREMENT,
  Chave          VARCHAR(100) NOT NULL,
  ValorDefault   VARCHAR(100) NULL                 DEFAULT NULL,
  TipoDefault    VARCHAR(30)  NULL                 DEFAULT NULL
);

INSERT INTO riel_integracao.Configuracao (Chave, ValorDefault, TipoDefault) VALUES

  ('URL_NOTIFICACAO', NULL, 'string'),
  ('ACEITA_NOTIFICACAO', 0, 'bool');

CREATE TABLE riel_integracao.Parceiro_Configuracao (
  ParceiroId     TINYINT(1) UNSIGNED NOT NULL,
  ConfiguracaoId INT                 NOT NULL,
  Valor          VARCHAR(100)        NULL DEFAULT NULL,
  Tipo           VARCHAR(100)        NULL DEFAULT NULL,

  CONSTRAINT FkParceiroConfiguracaoParceiro
  FOREIGN KEY (ParceiroId) REFERENCES riel_integracao.Parceiro (ParceiroId),

  CONSTRAINT FkParceiroConfiguracaoConfiguracao
  FOREIGN KEY (ConfiguracaoId) REFERENCES riel_integracao.Configuracao (ConfiguracaoId),

  CONSTRAINT PkParceiroConfiguracao PRIMARY KEY (ParceiroId, ConfiguracaoId),
  INDEX IdxParceiroId (ParceiroId),

  CONSTRAINT UqParceiroConfiguracao UNIQUE (ParceiroId, ConfiguracaoId)
);

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-20 18:54:48
