use riel_producao;
set @old_foreign_key_checks = @@foreign_key_checks;
set FOREIGN_KEY_CHECKS  = 0;

TRUNCATE TABLE CaracteristicaValor;
TRUNCATE TABLE Categoria;
TRUNCATE TABLE Categoria_Site;
TRUNCATE TABLE Combo_Log;
TRUNCATE TABLE Estoque;
TRUNCATE TABLE Fabricante;
TRUNCATE TABLE GrupoB2C;
TRUNCATE TABLE OperadorB2C;
TRUNCATE TABLE Produto;
TRUNCATE TABLE ProdutoCusto;
TRUNCATE TABLE ProdutoDimensao;
TRUNCATE TABLE Produto_CaracteristicaValor;
TRUNCATE TABLE Produto_Categoria;
TRUNCATE TABLE Produto_Site;
TRUNCATE TABLE FatorCartao;
TRUNCATE TABLE Site;
TRUNCATE TABLE Combo;
TRUNCATE TABLE ProdutoMedia;

set FOREIGN_KEY_CHECKS = @old_foreign_key_checks;