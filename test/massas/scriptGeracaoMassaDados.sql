# riel##2015@@

#RIEL_PRODUCAO
# Dump de estrutura do banco de dados riel_producao
# mysqldump --xml -d -h 192.168.152.3 -t --user=riel_db -p riel_producao > ./tests/massas/dumpEstruturaProducao.xml

# Dump de riel_producao.Produto relacionados a loja Nokia (siteID = 4)
# mysqldump --xml -h 192.168.152.3 -t --user=riel_db -p --where="ProdutoId IN (39056,39574,40461,40468,11774,16902,30441,30451,37499,74303)" riel_producao Produto> ./tests/massas/massaProdutosNokia.xml

# Dump de riel_producao.categoria Relacionada aos produtos da loja Nokia
# mysqldump --xml -h 192.168.152.3 -t --user=riel_db -p --where="CategoriaId IN (493,522,497,354,679,2332,3503,1842,3889,4724,491)" riel_producao Categoria > ./tests/massas/massaCategoriasNokia.xml

# Dump de riel_producao.Caracteristica Relacionada aos produtos da loja Nokia
# mysqldump --xml -h 192.168.152.3 -t --user=riel_db -p --where="CaracteristicaId IN (5789,5791,5792,5795,5796,5797,5798,5799,5800,5801,5803,5804,5805,5806,5807,5821,5822,5824,5828,5829,5832,5833,5834,5835,5836,5837,5790,7721,7722,7723,5787,5808,5809,3947,3964,3966,3972,3975,3982,3946,3948,3951,3952,3953,3955,3957,3958,3961,3978,3979,3984,3947,3967,3969,3970,3972,3976,3982,3985,3995,3996,3999,4004,4005,3946,3951,3952,3953,3955,3957,3958,3960,3961,3973,3978,3979,3984,3994,3998,4000,4002,4010,4012,4014,4016,4019,11485,11486,11490,11491,11491,11491,11492,11492,11493,11493,11495,11485,11490,11492,11493,11495,3947,3967,3969,3970,3972,3976,3982,3985,3995,3996,3999,4004,4005,3946,3951,3952,3953,3955,3957,3958,3973,3978,3979,3984,3994,3998,4000,4002,4010,4014,4016,4019,13992,4007,20410,3948,3950,4012,5789,5791,5795,5796,5797,5798,5799,5802,5803,5804,5805,5806,5807,5812,5813,5814,5815,5816,5817,5821,5822,5824,5827,5828,5829,5830,5833,5834,5836,7721,7722,7723,5790,5787,5808)" riel_producao Caracteristica > ./tests/massas/massaCaracteristicasNokia.xml

# Dump de riel_producao.Produto_CaracteristicaValor Relacionada aos produtos da loja Nokia
# mysqldump --xml -h 192.168.152.3 -t --user=riel_db -p --where="ProdutoId IN (39056,39574,40461,40468,11774,16902,30441,30451,37499,74303)" riel_producao Produto_CaracteristicaValor > ./tests/massas/massaProdutoCaracteristicaValorNokia.xml

# Dump de riel_producao.CaracteristicaValor Relacionada aos produtos da loja Nokia
# mysqldump --xml -h 192.168.152.3 -t --user=riel_db -p --where="CaracteristicaValorId IN (7585,7624,7625,7629,7631,7634,7635,7639,7641,7644,7648,7652,7654,7657,7659,7678,7680,7684,7692,7694,7700,7702,7704,7706,7708,7710,8923,79460,79500,80623,321638,321639,321640,4804,4808,4810,4819,4822,4830,261857,261858,261859,261860,261861,261862,261863,261864,261865,261866,261867,261868,4804,4812,4814,4816,4819,4824,4830,4832,4842,4845,4846,4852,4854,262116,262117,262118,262119,262120,262121,262122,262123,262124,262125,262126,262127,262128,262129,262131,262132,262133,262135,262136,262137,262138,262139,321108,321135,321160,321162,321164,321165,321166,321169,321173,321174,321190,321129,321160,321168,321174,321188,4804,4812,4814,4816,4819,4824,4830,4832,4842,4845,4846,4852,4854,328079,328080,328081,328082,328083,328084,328085,328088,328089,328090,328091,328092,328094,328095,328096,328098,328100,328101,328102,690475,697695,1871196,1874146,1874148,1874150,7584,7624,7630,7632,7634,7636,7639,7645,7648,7653,7654,7657,7658,7661,7665,7667,7669,7671,7673,7678,7681,7684,7691,7692,7694,7697,7702,7704,7708,79461,79501,80623,83211,605676,605677)" riel_producao CaracteristicaValor > ./tests/massas/massaCaracteristicaValorNokia.xml

# Dump de riel_producao.Estoque Relacionada aos produtos da loja Nokia
# mysqldump --xml -h 192.168.152.3 -t --user=riel_db -p --where="ProdutoId IN (39056,39574,40461,40468,11774,16902,30441,30451,37499,74303)" riel_producao Estoque > ./tests/massas/massaEstoqueNokia.xml

# Dump de riel_producao.Produto_Site Relacionada aos produtos da loja Nokia
# mysqldump --xml -h 192.168.152.3 -t --user=riel_db -p --where="ProdutoId IN (39056,39574,40461,40468,11774,16902,30441,30451,37499,74303)" riel_producao Produto_Site > ./tests/massas/massaProdutoSiteNokia.xml

# Dump de riel_producao.Fabricante Relacionada aos produtos da loja Nokia
# mysqldump --xml -h 192.168.152.3 -t --user=riel_db -p --where="FabricanteId IN (407,353,380,977,697)" riel_producao Fabricante > ./tests/massas/massaFabricanteNokia.xml

# Dump de riel_producao.Site Relacionada aos produtos da loja Nokia
# mysqldump --xml -h 192.168.152.3 -t --user=riel_db -p  riel_producao Site > ./tests/massas/massaSite.xml

# Dump de riel_producao.ProdutoDimensao Relacionada aos produtos da loja Nokia
# mysqldump --xml -h 192.168.152.3 -t --user=riel_db -p --where="ProdutoId IN (39056,39574,40461,40468,11774,16902,30441,30451,37499,74303)" riel_producao ProdutoDimensao > ./tests/massas/massaProdutoDimensaoNokia.xml

# Dump de riel_producao.Categoria_Site Relacionada aos produtos da loja Nokia
# mysqldump --xml -h 192.168.152.3 -t --user=riel_db -p --where="CategoriaId IN (493,522,497,354,354,679,2332,3503,1842,1842,354,3889,4724,497,491,493)" riel_producao Categoria_Site > ./tests/massas/massaCategoriaSiteNokia.xml

# Dump de riel_producao.Produto_Categoria Relacionada aos produtos da loja Nokia
# mysqldump --xml -h 192.168.152.3 -t --user=riel_db -p --where="ProdutoId IN (39056,39574,40461,40468,11774,16902,30441,30451,37499,74303)" riel_producao Produto_Categoria > ./tests/massas/massaProdutoCategoriaNokia.xml

#30441, 30451
# Dump de riel_producao.Combo Relacionada aos combos
#mysqldump -h 192.168.152.3 -t --user=riel_db -p --where="ComboId IN (30441, 30451)"  riel_producao Combo > ./tests/massas/novasMassas/massaSite.sql

# Dump de riel_producao.ProdutoCusto Relacionada aos ProdutoCusto da loja Nokia
#mysqldump -h 192.168.152.3 -t --user=riel_db -p --where="Codigo IN (8019,13377,24551,24561,30714,83314)"  riel_producao ProdutoCusto > ./tests/massas/massaProdutoCusto.sql

# Dump de riel_producao.FatorCartao Relacionada aos FatorCartao
#mysqldump -h 192.168.152.3 -t --user=riel_db -p --where="BandeiraId = 1"  riel_producao FatorCartao > ./tests/massas/massaFatorCartao.sql

# Dump de riel_producao.OperadorB2C Relacionada aos OperadorB2C
#mysqldump -h 192.168.152.3 -t --user=riel_db -p --where="OperadorB2CId < 10"  riel_producao OperadorB2C > ./tests/massas/massaOperadorB2C.sql

# Dump de riel_producao.GrupoB2C Relacionada aos GrupoB2C
#mysqldump -h 192.168.152.3 -t --user=riel_db -p --where="GrupoB2CId IN (1, 3, 374, 914)"  riel_producao GrupoB2C > ./tests/massas/massaGrupoB2C.sql

#39056,39574,40461,40468,11774,16902,30441,30451,37499,74303


#RIEL_INTEGRACAO


# Dump de riel_integracao.Parceiro Relacionada a vTex
# mysqldump --xml -h 192.168.152.3 -t --user=riel_db -p --where="ParceiroId = 2" riel_integracao Parceiro > ./tests/massas/massaParceiro.xml

# Dump de riel_producao.Parceiro_Site Relacionada a vTex
# mysqldump --xml -h 192.168.152.3 -t --user=riel_db -p --where="ParceiroId = 2" riel_integracao Parceiro_Site > ./tests/massas/massaParceiroSite.xml

# Dump de riel_producao.Parceiro_Site_Permissao Relacionada a vTex
# mysqldump --xml -h 192.168.152.3 -t --user=riel_db -p --where="ParceiroSiteId = 2" riel_integracao Parceiro_Site_Permissao > ./tests/massas/massaParceiroSitePermissao.xml

# Dump de riel_producao.Parceiro_SiteIp Relacionada aos produtos da loja Nokia
# mysqldump --xml -h 192.168.152.3 -t --user=riel_db -p --where="ParceiroSiteIpId IN (615, 616)" riel_integracao Parceiro_SiteIp > ./tests/massas/massaParceiroSiteIp.xml

# Dump de riel_producao.InterfacePermissao Relacionada aos produtos da loja Nokia
# mysqldump --xml -h 192.168.152.3 -t --user=riel_db -p --where="ParceiroSiteId = 2" riel_integracao InterfacePermissao > ./tests/massas/massaInterfacePermissao.xml

# Dump de riel_producao.InterfaceTracking Relacionada aos produtos da loja Nokia
# mysqldump --xml -h 192.168.152.3 -t --user=riel_db -p --where="ParceiroId =2" riel_integracao InterfaceTracking > ./tests/massas/massaInterfaceTracking.xml
