use riel_integracao;
set @old_foreign_key_checks = @@foreign_key_checks;
set FOREIGN_KEY_CHECKS  = 0;

TRUNCATE TABLE DadosImportacao;
TRUNCATE TABLE Interface;
TRUNCATE TABLE InterfacePermissao;
TRUNCATE TABLE InterfaceTipo;
TRUNCATE TABLE InterfaceTracking;
TRUNCATE TABLE Pagamento;
TRUNCATE TABLE Parceiro;
TRUNCATE TABLE Parceiro_Site;
TRUNCATE TABLE Parceiro_SiteIp;
TRUNCATE TABLE Parceiro_Site_Permissao;
TRUNCATE TABLE Pedido_Garantia;
TRUNCATE TABLE Pedido_Produto;
TRUNCATE TABLE Pedido_Seguro;
TRUNCATE TABLE PermissaoTipo;

set FOREIGN_KEY_CHECKS = @old_foreign_key_checks;