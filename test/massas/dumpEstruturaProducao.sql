use riel_producao;
-- MySQL dump 10.13  Distrib 5.7.10, for Win64 (x86_64)
--
-- Host: 192.168.152.3    Database: riel_producao
-- ------------------------------------------------------
-- Server version	5.6.26

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `AcessoVip`
--

DROP TABLE IF EXISTS `AcessoVip`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AcessoVip` (
  `AcessoVipId` int(11) NOT NULL AUTO_INCREMENT,
  `SiteId` tinyint(3) unsigned DEFAULT NULL,
  `Titulo` varchar(250) DEFAULT NULL,
  `CorBackground` char(6) DEFAULT NULL,
  `Status` tinyint(2) unsigned DEFAULT NULL,
  `UrlVip` varchar(350) DEFAULT NULL,
  `UrlDestino` varchar(350) DEFAULT NULL,
  `CampoUsuario` char(50) DEFAULT NULL,
  `CampoSenha` char(50) DEFAULT NULL,
  `MensagemErro` char(100) DEFAULT NULL,
  `TipoAcesso` tinyint(2) unsigned DEFAULT NULL,
  `TamanhoUsuario` tinyint(3) unsigned DEFAULT NULL,
  `TamanhoSenha` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`AcessoVipId`),
  KEY `AcessoVipSiteId_index` (`SiteId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=48 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AcessoVipUsuario`
--

DROP TABLE IF EXISTS `AcessoVipUsuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AcessoVipUsuario` (
  `AcessoVipUsuarioId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `AcessoVipId` int(11) unsigned DEFAULT NULL,
  `Usuario` char(100) DEFAULT NULL,
  `Senha` char(45) DEFAULT NULL,
  `Status` tinyint(2) unsigned DEFAULT NULL,
  PRIMARY KEY (`AcessoVipUsuarioId`),
  KEY `AcessoVipUsuario_Index1` (`AcessoVipId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=2082937 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Acessorio`
--

DROP TABLE IF EXISTS `Acessorio`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Acessorio` (
  `ProdutoId` int(11) NOT NULL DEFAULT '0',
  `AcessorioId` int(11) NOT NULL DEFAULT '0',
  `Ordem` tinyint(4) DEFAULT NULL,
  `TipoDesconto` tinyint(4) DEFAULT NULL,
  `ValorDesconto` float DEFAULT NULL,
  PRIMARY KEY (`ProdutoId`,`AcessorioId`),
  KEY `fk_Produto_Produto_Produto` (`ProdutoId`),
  KEY `fk_Produto_Produto_Produto1` (`AcessorioId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Admin_Area`
--

DROP TABLE IF EXISTS `Admin_Area`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Admin_Area` (
  `AreaId` int(11) NOT NULL AUTO_INCREMENT,
  `AreaAsc` int(11) DEFAULT NULL,
  `Nome` varchar(45) NOT NULL DEFAULT '',
  `Url` varchar(100) DEFAULT NULL,
  `DataCadastro` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` int(11) NOT NULL DEFAULT '0',
  `Nivel` int(11) NOT NULL DEFAULT '0',
  `Sistema` tinyint(1) unsigned DEFAULT '1' COMMENT '1 SGW - 2 BI',
  PRIMARY KEY (`AreaId`),
  KEY `fk_Admin_Area_Admin_Area` (`AreaAsc`),
  KEY `Admin_Area_Status` (`Status`),
  KEY `idx_url` (`Url`)
) ENGINE=InnoDB AUTO_INCREMENT=602 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Admin_Grupo`
--

DROP TABLE IF EXISTS `Admin_Grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Admin_Grupo` (
  `Admin_GrupoId` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`Admin_GrupoId`)
) ENGINE=InnoDB AUTO_INCREMENT=81 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Admin_Grupo_Permissao`
--

DROP TABLE IF EXISTS `Admin_Grupo_Permissao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Admin_Grupo_Permissao` (
  `Admin_GrupoId` int(11) NOT NULL DEFAULT '0',
  `AreaId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`Admin_GrupoId`,`AreaId`),
  KEY `fk_Admin_Grupo_Admin_Permissao_Admin_Grupo` (`Admin_GrupoId`),
  KEY `fk_Admin_Grupo_Permissao_Admin_Area` (`AreaId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Admin_Log`
--

DROP TABLE IF EXISTS `Admin_Log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Admin_Log` (
  `LogId` int(11) NOT NULL AUTO_INCREMENT,
  `UsuarioId` int(11) NOT NULL DEFAULT '0',
  `Acao` text,
  `Observacao` text,
  `Data` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `IP` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`LogId`),
  KEY `fk_Admin_Log_Admin_Usuario` (`UsuarioId`),
  KEY `Admin_Log_Data` (`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Admin_LogMenu`
--

DROP TABLE IF EXISTS `Admin_LogMenu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Admin_LogMenu` (
  `LogMenuId` int(11) NOT NULL AUTO_INCREMENT,
  `DataHora` datetime DEFAULT NULL,
  `Menu` varchar(100) DEFAULT NULL,
  `Usuario` varchar(100) DEFAULT NULL,
  `Sistema` char(3) DEFAULT NULL,
  PRIMARY KEY (`LogMenuId`),
  KEY `IdxMenu` (`Menu`)
) ENGINE=InnoDB AUTO_INCREMENT=990817 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Admin_Mensagem`
--

DROP TABLE IF EXISTS `Admin_Mensagem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Admin_Mensagem` (
  `MensagemId` int(11) NOT NULL AUTO_INCREMENT,
  `RemetenteId` int(11) NOT NULL DEFAULT '0',
  `Mensagem` text NOT NULL,
  `Data` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`MensagemId`),
  KEY `Admin_Mensagem_Data` (`Data`),
  KEY `fk_Admin_Mensagem_Admin_Usuario` (`RemetenteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Admin_Usuario`
--

DROP TABLE IF EXISTS `Admin_Usuario`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Admin_Usuario` (
  `UsuarioId` int(11) NOT NULL AUTO_INCREMENT,
  `GrupoId` int(11) DEFAULT NULL,
  `Nome` varchar(60) NOT NULL DEFAULT '',
  `DataNascimento` datetime DEFAULT NULL,
  `Telefone` varchar(15) DEFAULT NULL,
  `Email` varchar(100) NOT NULL DEFAULT '',
  `Senha` varchar(45) NOT NULL DEFAULT '',
  `DataCadastro` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` tinyint(1) DEFAULT '1',
  `Cpf` varchar(14) DEFAULT NULL,
  PRIMARY KEY (`UsuarioId`),
  UNIQUE KEY `Admin_Usuario_Email` (`Email`),
  KEY `fk_Admin_Usuario_Admin_Grupo` (`GrupoId`),
  KEY `fk_Admin_Usuario_Status` (`Status`)
) ENGINE=InnoDB AUTO_INCREMENT=1252 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Admin_Usuario_Mensagem`
--

DROP TABLE IF EXISTS `Admin_Usuario_Mensagem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Admin_Usuario_Mensagem` (
  `DestinatarioId` int(11) NOT NULL DEFAULT '0',
  `MensagemId` int(11) NOT NULL DEFAULT '0',
  `Status` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`DestinatarioId`,`MensagemId`),
  KEY `fk_Admin_Usuario_Admin_Mensagem_Admin_Usuario` (`DestinatarioId`),
  KEY `fk_Admin_Usuario_Admin_Mensagem_Admin_Mensagem` (`MensagemId`),
  KEY `Admin_Usuario_Mensagem_Status` (`Status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Admin_Usuario_Permissao`
--

DROP TABLE IF EXISTS `Admin_Usuario_Permissao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Admin_Usuario_Permissao` (
  `UsuarioId` int(11) NOT NULL DEFAULT '0',
  `AreaId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`UsuarioId`,`AreaId`),
  KEY `fk_Admin_Usuario_Admin_Permissao_Admin_Usuario` (`UsuarioId`),
  KEY `fk_Admin_Usuario_Permissao_Admin_Area` (`AreaId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COMMENT='Permissao individual';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Admin_Usuario_Site`
--

DROP TABLE IF EXISTS `Admin_Usuario_Site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Admin_Usuario_Site` (
  `UsuarioId` int(11) NOT NULL DEFAULT '0',
  `SiteId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`UsuarioId`,`SiteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AppFacebook`
--

DROP TABLE IF EXISTS `AppFacebook`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AppFacebook` (
  `AppFacebookId` mediumint(8) NOT NULL AUTO_INCREMENT,
  `CategoriaId` mediumint(8) unsigned NOT NULL,
  `SiteId` tinyint(3) unsigned NOT NULL,
  `Status` tinyint(1) unsigned DEFAULT '1',
  PRIMARY KEY (`AppFacebookId`),
  KEY `AppFacebookSiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AppFacebook_Produto`
--

DROP TABLE IF EXISTS `AppFacebook_Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AppFacebook_Produto` (
  `ProdutoId` int(11) NOT NULL DEFAULT '0',
  `AppFacebookId` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `Posicao` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `Prioridade` tinyint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ProdutoId`,`AppFacebookId`),
  KEY `fk_AppFacebook_Produto_Produto` (`ProdutoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AssinanteNews`
--

DROP TABLE IF EXISTS `AssinanteNews`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AssinanteNews` (
  `AssinanteNewsId` int(11) NOT NULL AUTO_INCREMENT,
  `Email` varchar(60) NOT NULL DEFAULT '',
  `Nome` varchar(100) DEFAULT NULL,
  `Apelido` varchar(60) DEFAULT NULL,
  `Status` tinyint(1) DEFAULT NULL,
  `DataCadastro` timestamp NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `SiteId` int(11) DEFAULT NULL,
  `Sexo` char(1) DEFAULT NULL,
  `DataNascimento` date DEFAULT NULL,
  `CampanhaId` int(11) unsigned DEFAULT NULL COMMENT 'Se o cliente se cadastrou mediante uma campanha',
  PRIMARY KEY (`AssinanteNewsId`),
  UNIQUE KEY `idx_Email` (`Email`,`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=10557327 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AssinanteNews_Complemento`
--

DROP TABLE IF EXISTS `AssinanteNews_Complemento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AssinanteNews_Complemento` (
  `AssinanteNewsId` int(11) unsigned NOT NULL,
  `CPF` char(11) NOT NULL,
  `Estado` char(2) NOT NULL DEFAULT '',
  `Cidade` char(70) NOT NULL,
  `Celular` char(10) DEFAULT NULL,
  `ReceberSms` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 = N�o, 1 = Sim',
  PRIMARY KEY (`AssinanteNewsId`),
  UNIQUE KEY `AssinanteNewsId` (`AssinanteNewsId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AssociadorConta`
--

DROP TABLE IF EXISTS `AssociadorConta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AssociadorConta` (
  `ClienteId` int(11) NOT NULL,
  `userId` varchar(100) NOT NULL,
  `MidiaSocialId` int(11) NOT NULL,
  PRIMARY KEY (`ClienteId`,`userId`,`MidiaSocialId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Atributo`
--

DROP TABLE IF EXISTS `Atributo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Atributo` (
  `AtributoId` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(45) NOT NULL DEFAULT '',
  `Status` tinyint(1) DEFAULT NULL,
  `Tipo` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`AtributoId`),
  KEY `Atributo_Status` (`Status`)
) ENGINE=InnoDB AUTO_INCREMENT=177 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AtributoCor`
--

DROP TABLE IF EXISTS `AtributoCor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AtributoCor` (
  `AtributoCorId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` char(30) NOT NULL,
  `Status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `Imagem` char(100) DEFAULT NULL,
  PRIMARY KEY (`AtributoCorId`),
  KEY `AtributoCorId` (`AtributoCorId`)
) ENGINE=InnoDB AUTO_INCREMENT=170 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AtributoGrupo`
--

DROP TABLE IF EXISTS `AtributoGrupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AtributoGrupo` (
  `AtributoGrupoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` char(60) NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `DataCadastro` date NOT NULL DEFAULT '0000-00-00',
  `ProdutoBase` int(11) DEFAULT NULL,
  PRIMARY KEY (`AtributoGrupoId`),
  KEY `AtributoGrupoId` (`AtributoGrupoId`)
) ENGINE=InnoDB AUTO_INCREMENT=17777 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AtributoGrupo_Produto`
--

DROP TABLE IF EXISTS `AtributoGrupo_Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AtributoGrupo_Produto` (
  `AtributoGrupoId` int(11) unsigned NOT NULL,
  `ProdutoId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`AtributoGrupoId`,`ProdutoId`),
  KEY `idx_ProdutoId` (`ProdutoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AtributoTamanho`
--

DROP TABLE IF EXISTS `AtributoTamanho`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AtributoTamanho` (
  `AtributoTamanhoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Tamanho` char(5) NOT NULL,
  `Status` tinyint(1) unsigned DEFAULT NULL,
  `MostrarNaHome` tinyint(4) unsigned DEFAULT '0',
  `MostrarNaHomeMasculino` tinyint(1) NOT NULL,
  `MostrarNaHomeFeminino` tinyint(1) NOT NULL,
  PRIMARY KEY (`AtributoTamanhoId`),
  KEY `AtributoTamanhoId` (`AtributoTamanhoId`)
) ENGINE=InnoDB AUTO_INCREMENT=164 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AtributoValor`
--

DROP TABLE IF EXISTS `AtributoValor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AtributoValor` (
  `AtributoValorId` int(11) NOT NULL AUTO_INCREMENT,
  `AtributoId` int(11) NOT NULL DEFAULT '0',
  `ProdutoId` int(11) DEFAULT NULL,
  `Valor` varchar(45) NOT NULL DEFAULT '',
  `Status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`AtributoValorId`),
  KEY `fk_AtributosValor_Atributos` (`AtributoId`),
  KEY `AtributoValor_Status` (`Status`),
  KEY `fk_AtributosValor_Produto` (`ProdutoId`)
) ENGINE=InnoDB AUTO_INCREMENT=8078 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `AvisoDisponibilidade`
--

DROP TABLE IF EXISTS `AvisoDisponibilidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `AvisoDisponibilidade` (
  `AvisoDisponibilidadeId` int(11) NOT NULL AUTO_INCREMENT,
  `ProdutoId` int(11) NOT NULL DEFAULT '0',
  `Nome` varchar(45) DEFAULT NULL,
  `Email` varchar(60) NOT NULL DEFAULT '',
  `DataSolicitacao` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DataAtendimento` datetime DEFAULT NULL,
  `Status` tinyint(4) NOT NULL DEFAULT '0',
  `StatusSimilar` tinyint(4) NOT NULL DEFAULT '0',
  `SiteId` int(11) DEFAULT NULL,
  PRIMARY KEY (`AvisoDisponibilidadeId`),
  KEY `fk_AvisoDisponibilidade_Produto` (`ProdutoId`),
  KEY `AvisoDisponibilidade_Status` (`Status`,`DataSolicitacao`),
  KEY `AvisoDisponibilidadeSiteId` (`SiteId`),
  KEY `fk_avisodisponibilidade` (`ProdutoId`,`Status`)
) ENGINE=InnoDB AUTO_INCREMENT=3889989 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Bandeira`
--

DROP TABLE IF EXISTS `Bandeira`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Bandeira` (
  `BandeiraId` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(45) NOT NULL DEFAULT '',
  `Status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`BandeiraId`),
  KEY `Bandeira_Status` (`Status`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Banner`
--

DROP TABLE IF EXISTS `Banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Banner` (
  `BannerId` int(11) NOT NULL AUTO_INCREMENT,
  `Src` varchar(100) NOT NULL DEFAULT '',
  `Mime` varchar(45) NOT NULL DEFAULT '',
  `Posicao` tinyint(4) NOT NULL DEFAULT '0' COMMENT 'Bloco na tela',
  `Ordem` tinyint(4) DEFAULT NULL COMMENT 'Ordem dos banners (ex.: Prioridade nos Banners laterais)',
  `Url` varchar(350) DEFAULT NULL,
  `Tipo` int(11) DEFAULT '0' COMMENT '0 = flash comum, 1 = flash expansivo',
  `SiteId` int(11) DEFAULT NULL,
  `Altura` int(11) DEFAULT NULL,
  `Largura` int(11) DEFAULT NULL,
  `BannerPrincipalId` int(11) unsigned DEFAULT NULL,
  `DataInicio` datetime DEFAULT NULL,
  `DataFim` datetime DEFAULT NULL,
  PRIMARY KEY (`BannerId`),
  KEY `BannerSiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=16023 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BannerCarrinho`
--

DROP TABLE IF EXISTS `BannerCarrinho`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BannerCarrinho` (
  `BannerCarrinhoId` int(11) NOT NULL AUTO_INCREMENT,
  `SiteId` int(11) NOT NULL,
  `CategoriaId` smallint(4) NOT NULL,
  `Imagem` varchar(45) NOT NULL,
  `DataInicial` datetime NOT NULL,
  `DataFinal` datetime NOT NULL,
  `Status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`BannerCarrinhoId`),
  KEY `fk_Site` (`SiteId`),
  KEY `fk_Categoria` (`CategoriaId`),
  KEY `idx_Status` (`Status`,`DataInicial`,`DataFinal`)
) ENGINE=InnoDB AUTO_INCREMENT=92 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BannerCarrinho_Produto`
--

DROP TABLE IF EXISTS `BannerCarrinho_Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BannerCarrinho_Produto` (
  `BannerCarrinhoId` int(11) NOT NULL,
  `ProdutoId` int(11) NOT NULL,
  `Nome` varchar(65) NOT NULL,
  `Ordem` tinyint(4) NOT NULL DEFAULT '1',
  PRIMARY KEY (`BannerCarrinhoId`,`ProdutoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BannerDetalhes`
--

DROP TABLE IF EXISTS `BannerDetalhes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BannerDetalhes` (
  `BannerDetalhesId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `BannerId` int(11) unsigned NOT NULL,
  `Imagem` char(100) DEFAULT NULL,
  `Texto` text,
  `Ordem` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`BannerDetalhesId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BannerDinamico`
--

DROP TABLE IF EXISTS `BannerDinamico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BannerDinamico` (
  `BannerDinamicoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SiteId` tinyint(3) unsigned NOT NULL,
  `CategoriaId` mediumint(3) unsigned NOT NULL,
  `CorTitulo` char(8) DEFAULT NULL,
  `CorPrecoDe` char(8) DEFAULT NULL,
  `CorPrecoPor` char(8) DEFAULT NULL,
  `CorParcelamentoSemJuros` char(8) DEFAULT NULL,
  `CorParcelamentoComJuros` char(8) DEFAULT NULL,
  `CorFraseFrete` char(8) DEFAULT NULL,
  `CorMenu` char(8) DEFAULT NULL,
  `CorMenuHover` char(8) DEFAULT NULL,
  `Background` char(50) DEFAULT NULL,
  `Status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `TempoTransicao` tinyint(4) unsigned DEFAULT '0',
  `Tipo` tinyint(2) unsigned DEFAULT '0' COMMENT '0- BannerDinamico, 1-DestaquesSemana',
  PRIMARY KEY (`BannerDinamicoId`),
  KEY `BannerDinamico_SiteId` (`SiteId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=364 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BannerDinamico_Produto`
--

DROP TABLE IF EXISTS `BannerDinamico_Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BannerDinamico_Produto` (
  `BannerDinamicoId` int(11) unsigned NOT NULL DEFAULT '0',
  `ProdutoId` int(11) unsigned NOT NULL DEFAULT '0',
  `Ordem` tinyint(4) unsigned NOT NULL,
  `FraseFrete` char(250) DEFAULT NULL,
  `LinkProduto` char(255) DEFAULT NULL,
  `Grupo` tinyint(2) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`BannerDinamicoId`,`ProdutoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BannerLateral`
--

DROP TABLE IF EXISTS `BannerLateral`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BannerLateral` (
  `BannerLateralId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SiteId` int(11) unsigned NOT NULL,
  `CategoriaId` int(11) unsigned DEFAULT NULL,
  `Tipo` tinyint(2) unsigned DEFAULT NULL COMMENT '0 Especial, 1 - Padrao',
  `QuantidadeOfertas` tinyint(2) unsigned NOT NULL COMMENT '1, 3 ou 4',
  `Ordem` tinyint(4) unsigned DEFAULT NULL,
  `Status` tinyint(1) unsigned DEFAULT '1',
  `TempoTransicao` tinyint(4) unsigned DEFAULT NULL,
  `TipoTransicao` varchar(50) DEFAULT NULL,
  `Background` varchar(100) DEFAULT NULL,
  `DataInicio` datetime DEFAULT NULL,
  `DataFim` datetime DEFAULT NULL,
  `AlturaIframe` int(5) DEFAULT '0',
  `Link` varchar(350) DEFAULT NULL,
  `Direita` tinyint(1) NOT NULL DEFAULT '1',
  PRIMARY KEY (`BannerLateralId`),
  KEY `BannerLateral_SiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=14424 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BannerLateral_Produto`
--

DROP TABLE IF EXISTS `BannerLateral_Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BannerLateral_Produto` (
  `BannerLateralId` int(11) unsigned NOT NULL,
  `ProdutoId` int(11) unsigned NOT NULL,
  `Imagem` varchar(50) DEFAULT NULL,
  `Ordem` tinyint(2) unsigned DEFAULT NULL,
  `CorFundo` varchar(20) DEFAULT NULL,
  `Link` varchar(350) DEFAULT NULL,
  `TextoChamada` text,
  `BannerLateralProdutoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `TipoImagem` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 produto - 1 upload',
  PRIMARY KEY (`BannerLateralProdutoId`)
) ENGINE=InnoDB AUTO_INCREMENT=41228 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BannerMidia`
--

DROP TABLE IF EXISTS `BannerMidia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BannerMidia` (
  `MidiaId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SiteId` int(11) DEFAULT NULL,
  `Status` tinyint(1) unsigned DEFAULT NULL,
  `ParceiroId` int(11) DEFAULT NULL,
  `Cupom` char(19) DEFAULT NULL,
  `NomeBanner` varchar(100) DEFAULT NULL,
  `DataInicio` datetime DEFAULT NULL,
  `DataFim` datetime DEFAULT NULL,
  `src` varchar(100) DEFAULT NULL COMMENT 'Caso o tipo da midia for 5',
  `TipoMidia` tinyint(2) unsigned DEFAULT NULL COMMENT '0 - Html / 5 - Imagem',
  `Display` text COMMENT 'Configuracao de display',
  `Home` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`MidiaId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BannerMidia_Categoria`
--

DROP TABLE IF EXISTS `BannerMidia_Categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BannerMidia_Categoria` (
  `MidiaId` int(11) unsigned NOT NULL,
  `CategoriaId` int(11) unsigned NOT NULL,
  `Excecao` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`MidiaId`,`CategoriaId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BannerMidia_Lista`
--

DROP TABLE IF EXISTS `BannerMidia_Lista`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BannerMidia_Lista` (
  `MidiaId` int(11) unsigned NOT NULL,
  `Lista` int(11) unsigned NOT NULL COMMENT 'ListaEspecialId',
  PRIMARY KEY (`MidiaId`,`Lista`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BannerMidia_Perfil`
--

DROP TABLE IF EXISTS `BannerMidia_Perfil`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BannerMidia_Perfil` (
  `MidiaId` int(11) unsigned NOT NULL,
  `Perfil` varchar(30) NOT NULL,
  PRIMARY KEY (`MidiaId`,`Perfil`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BannerMidia_Produto`
--

DROP TABLE IF EXISTS `BannerMidia_Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BannerMidia_Produto` (
  `MidiaId` int(11) unsigned NOT NULL,
  `ProdutoId` int(11) unsigned NOT NULL,
  `Excecao` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`MidiaId`,`ProdutoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BannerProduto`
--

DROP TABLE IF EXISTS `BannerProduto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BannerProduto` (
  `BannerProdutoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Nivel` tinyint(1) unsigned NOT NULL COMMENT '1: Categoria, 2: Produto',
  `Id` int(11) unsigned NOT NULL COMMENT 'Id do produto ou Id da categoria',
  `Tipo` tinyint(1) unsigned NOT NULL COMMENT '1: imagem, 2: html',
  `AlturaIframe` smallint(3) DEFAULT NULL,
  `Link` varchar(150) DEFAULT NULL,
  `UrlImagem` varchar(100) DEFAULT NULL,
  `Secundario` tinyint(1) NOT NULL DEFAULT '0' COMMENT '1 = secundio, 0 = principal',
  `DataInicio` datetime DEFAULT NULL,
  `DataFim` datetime DEFAULT NULL,
  `Status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`BannerProdutoId`)
) ENGINE=InnoDB AUTO_INCREMENT=172 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BannerProduto_Site`
--

DROP TABLE IF EXISTS `BannerProduto_Site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BannerProduto_Site` (
  `BannerProdutoId` int(11) unsigned NOT NULL,
  `SiteId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`BannerProdutoId`,`SiteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BannerTv`
--

DROP TABLE IF EXISTS `BannerTv`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BannerTv` (
  `BannerTvId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SiteId` int(11) unsigned NOT NULL,
  `Background` varchar(100) DEFAULT NULL,
  `DataInicio` datetime DEFAULT NULL,
  `DataFim` datetime DEFAULT NULL,
  `TempoTransicao` tinyint(4) DEFAULT NULL,
  `TipoTransicao` varchar(50) DEFAULT NULL,
  `Status` tinyint(1) DEFAULT NULL,
  `Dispositivo` tinyint(1) DEFAULT '1' COMMENT '0: Mobile, 1: Desktop (default)',
  PRIMARY KEY (`BannerTvId`)
) ENGINE=InnoDB AUTO_INCREMENT=4574 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BannerTv_Imagem`
--

DROP TABLE IF EXISTS `BannerTv_Imagem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BannerTv_Imagem` (
  `BannerTvImagemId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `BannerTvId` int(11) unsigned NOT NULL,
  `Imagem` varchar(50) DEFAULT NULL,
  `Link` varchar(350) DEFAULT NULL,
  `Ordem` tinyint(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`BannerTvImagemId`),
  KEY `BannerTvId_idx` (`BannerTvId`)
) ENGINE=InnoDB AUTO_INCREMENT=26017 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BlackFriday`
--

DROP TABLE IF EXISTS `BlackFriday`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BlackFriday` (
  `BlackFridayId` tinyint(11) unsigned NOT NULL AUTO_INCREMENT,
  `Url` varchar(150) NOT NULL,
  `Imagem` varchar(150) NOT NULL,
  `Target` varchar(20) NOT NULL,
  `Tipo` tinyint(1) unsigned NOT NULL COMMENT '1=Primeiro Bloco, 2=Segundo Bloco, 3=Terceiro Bloco',
  `SiteId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`BlackFridayId`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BloqueioProduto`
--

DROP TABLE IF EXISTS `BloqueioProduto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BloqueioProduto` (
  `RegraId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(80) NOT NULL,
  `Mensagem` text NOT NULL,
  `Status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`RegraId`)
) ENGINE=InnoDB AUTO_INCREMENT=66 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BloqueioProduto_Estado`
--

DROP TABLE IF EXISTS `BloqueioProduto_Estado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BloqueioProduto_Estado` (
  `BloqueioProdutoEstadoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `RegraId` int(11) unsigned NOT NULL,
  `Estado` char(4) NOT NULL,
  PRIMARY KEY (`BloqueioProdutoEstadoId`)
) ENGINE=InnoDB AUTO_INCREMENT=145 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BloqueioProduto_Produto`
--

DROP TABLE IF EXISTS `BloqueioProduto_Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BloqueioProduto_Produto` (
  `BloqueioProdutoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `RegraId` int(11) unsigned NOT NULL,
  `ProdutoId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`BloqueioProdutoId`)
) ENGINE=InnoDB AUTO_INCREMENT=1215 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BloqueioProduto_Site`
--

DROP TABLE IF EXISTS `BloqueioProduto_Site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BloqueioProduto_Site` (
  `BloqueioProdutoSiteId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `RegraId` int(11) unsigned NOT NULL,
  `SiteId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`BloqueioProdutoSiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=69 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Boleto`
--

DROP TABLE IF EXISTS `Boleto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Boleto` (
  `BoletoId` int(11) NOT NULL AUTO_INCREMENT,
  `BoletoGrupoId` int(11) DEFAULT NULL COMMENT 'Boletos avulsos não possuem grupo. Por esse motivo esse campo pode ser nulo.',
  `Valor` decimal(12,2) NOT NULL,
  `DataCadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `DataDocumento` date NOT NULL,
  `DataVencimento` date NOT NULL,
  `URL` varchar(255) NOT NULL,
  `Tipo` smallint(6) NOT NULL DEFAULT '0' COMMENT '0 - Avulso; 1 - Pós Pago',
  `Sequencial` smallint(6) NOT NULL,
  PRIMARY KEY (`BoletoId`),
  KEY `fk_Boleto_BoletoGrupo` (`BoletoGrupoId`)
) ENGINE=InnoDB AUTO_INCREMENT=13449 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BoletoGrupo`
--

DROP TABLE IF EXISTS `BoletoGrupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BoletoGrupo` (
  `BoletoGrupoId` int(11) NOT NULL AUTO_INCREMENT,
  `ClienteId` int(11) NOT NULL COMMENT 'Cada grupo está relacionado à um único cliente.',
  `DataCadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `Status` smallint(6) NOT NULL COMMENT '0 - Inativo (Cancelado); 1 - Ativo',
  PRIMARY KEY (`BoletoGrupoId`),
  KEY `fk_BoletoGrupo_Cliente` (`ClienteId`),
  KEY `idx_BoletoGrupo_DataCadastro` (`DataCadastro`)
) ENGINE=InnoDB AUTO_INCREMENT=13031 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BoletoGrupo_Pedido`
--

DROP TABLE IF EXISTS `BoletoGrupo_Pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BoletoGrupo_Pedido` (
  `BoletoGrupoId` int(11) NOT NULL,
  `PedidoId` int(11) NOT NULL,
  PRIMARY KEY (`BoletoGrupoId`,`PedidoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BoletoStatus`
--

DROP TABLE IF EXISTS `BoletoStatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BoletoStatus` (
  `BoletoStatusId` int(11) NOT NULL AUTO_INCREMENT,
  `BoletoId` int(11) NOT NULL,
  `DataCadastro` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `UsuarioId` int(11) NOT NULL,
  `Status` smallint(6) NOT NULL COMMENT '0 - Cancelado; 1 - Aguardando Pagamento; 2 - Pago',
  PRIMARY KEY (`BoletoStatusId`),
  KEY `fk_BoletoStatus_Boleto` (`BoletoId`)
) ENGINE=InnoDB AUTO_INCREMENT=24748 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BriefingComercial`
--

DROP TABLE IF EXISTS `BriefingComercial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BriefingComercial` (
  `BriefingComercialId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `BriefingMarketingId` int(11) unsigned NOT NULL,
  `CategoriaId` int(11) unsigned NOT NULL,
  `ProdutoId` int(11) unsigned NOT NULL,
  `Nome` varchar(255) DEFAULT NULL,
  `Modelo` varchar(50) DEFAULT NULL,
  `Descricao` text,
  `PrecoDe` decimal(10,2) DEFAULT NULL,
  `PrecoPor` decimal(10,2) DEFAULT NULL,
  `Parcelamento` tinyint(1) unsigned DEFAULT NULL,
  `ValorParcela` decimal(10,2) DEFAULT NULL,
  `Frete` varchar(100) DEFAULT NULL,
  `Selo` varchar(100) DEFAULT NULL,
  `Garantia` varchar(100) DEFAULT NULL,
  `CupomValor` varchar(100) DEFAULT NULL,
  `CupomFrete` varchar(100) DEFAULT NULL,
  `CupomPorcentagem` float DEFAULT NULL,
  `Link` varchar(350) DEFAULT NULL,
  `Estoque` int(11) unsigned DEFAULT NULL,
  `ExpectativaVenda` decimal(10,2) DEFAULT NULL,
  `PrecoConcorrencia` decimal(10,2) DEFAULT NULL,
  `MediaVenda` decimal(10,2) DEFAULT NULL,
  `Observacao` varchar(350) DEFAULT NULL,
  `Ordem` tinyint(4) unsigned DEFAULT NULL,
  `DataAlteracao` datetime DEFAULT NULL,
  `UsuarioAlteracao` int(11) DEFAULT NULL,
  PRIMARY KEY (`BriefingComercialId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BriefingEmail`
--

DROP TABLE IF EXISTS `BriefingEmail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BriefingEmail` (
  `BriefingEmailId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Email` varchar(100) DEFAULT NULL,
  `Status` tinyint(1) unsigned DEFAULT '1',
  PRIMARY KEY (`BriefingEmailId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BriefingMarketing`
--

DROP TABLE IF EXISTS `BriefingMarketing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BriefingMarketing` (
  `BriefingMarketingId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SiteId` int(11) unsigned NOT NULL,
  `Tipo` tinyint(2) unsigned NOT NULL COMMENT '0 MKT-EMAIL,1 MKT-HOME',
  `Nome` varchar(100) DEFAULT NULL,
  `DataCampanha` datetime DEFAULT NULL,
  `PrazoPreenchimento` datetime DEFAULT NULL,
  `Observacao` text,
  `DataCadastro` datetime DEFAULT NULL,
  `UsuarioCadastro` int(11) DEFAULT NULL,
  `DataAlteracao` datetime DEFAULT NULL,
  `UsuarioAlteracao` int(11) DEFAULT NULL,
  PRIMARY KEY (`BriefingMarketingId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BriefingMarketing_Categoria`
--

DROP TABLE IF EXISTS `BriefingMarketing_Categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BriefingMarketing_Categoria` (
  `BriefingMarketingCategoriaId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `BriefingMarketingId` int(11) unsigned NOT NULL,
  `CategoriaId` int(11) unsigned NOT NULL,
  `Quantidade` tinyint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`BriefingMarketingCategoriaId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BriefingMarketing_Email`
--

DROP TABLE IF EXISTS `BriefingMarketing_Email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BriefingMarketing_Email` (
  `BriefingMarketingEmailId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `BriefingMarketingId` int(11) unsigned NOT NULL,
  `EmailId` int(11) NOT NULL,
  PRIMARY KEY (`BriefingMarketingEmailId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Brinde`
--

DROP TABLE IF EXISTS `Brinde`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Brinde` (
  `BrindeId` int(11) NOT NULL AUTO_INCREMENT,
  `SiteId` tinyint(4) NOT NULL,
  `ProdutoId` int(11) NOT NULL,
  PRIMARY KEY (`BrindeId`),
  KEY `FK_Brinde_1` (`ProdutoId`),
  CONSTRAINT `FK_Brinde_1` FOREIGN KEY (`ProdutoId`) REFERENCES `Produto_Site` (`ProdutoId`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Busca`
--

DROP TABLE IF EXISTS `Busca`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Busca` (
  `BuscaId` int(11) NOT NULL AUTO_INCREMENT,
  `ClienteId` int(11) DEFAULT NULL,
  `CategoriaId` int(11) DEFAULT NULL,
  `SessionId` varchar(50) NOT NULL DEFAULT '0',
  `Termos` varchar(255) NOT NULL DEFAULT '',
  `Data` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` tinyint(4) NOT NULL DEFAULT '0',
  `MostrarNoHistorico` tinyint(1) DEFAULT '1',
  `SiteId` int(11) DEFAULT NULL,
  PRIMARY KEY (`BuscaId`),
  KEY `fk_TermosPesquisados_Cliente` (`ClienteId`),
  KEY `fk_Busca_Categoria` (`CategoriaId`),
  KEY `fk_Busca_Session` (`SessionId`),
  KEY `Busca_Filtro` (`Status`,`Data`),
  KEY `MostrarNoHistorico` (`MostrarNoHistorico`),
  KEY `Termos` (`Termos`,`Data`,`Status`),
  KEY `BuscaSiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=1391895 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BuscaColchao`
--

DROP TABLE IF EXISTS `BuscaColchao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BuscaColchao` (
  `BuscaColchaoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ProdutoId` int(11) unsigned NOT NULL,
  `CategoriaId` int(11) unsigned NOT NULL,
  `CaracteristicaId` int(11) unsigned NOT NULL,
  `CaracteristicaNome` varchar(45) NOT NULL,
  `CaracteristicaValorId` int(11) unsigned NOT NULL,
  `CaracteristicaValorNome` varchar(255) NOT NULL,
  `AppColchao` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`BuscaColchaoId`),
  UNIQUE KEY `BuscaColchaoId` (`BuscaColchaoId`) USING BTREE,
  KEY `CaracteristicaNomeApp` (`CaracteristicaNome`,`AppColchao`) USING BTREE,
  KEY `CaracteristicaValorNomeApp` (`CaracteristicaValorNome`,`AppColchao`) USING BTREE,
  KEY `AppColchao` (`AppColchao`) USING BTREE,
  KEY `idx_ProdutoId` (`ProdutoId`)
) ENGINE=InnoDB AUTO_INCREMENT=18851 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BuscaEspecifica`
--

DROP TABLE IF EXISTS `BuscaEspecifica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BuscaEspecifica` (
  `BuscaEspecificaId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CategoriaId` int(11) unsigned DEFAULT '0',
  `Status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`BuscaEspecificaId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BuscaEspecificaFiltro`
--

DROP TABLE IF EXISTS `BuscaEspecificaFiltro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BuscaEspecificaFiltro` (
  `FiltroId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `BuscaEspecificaId` int(11) unsigned DEFAULT NULL,
  `Label` char(45) DEFAULT NULL,
  `CaracteristicaNome` char(45) DEFAULT NULL,
  PRIMARY KEY (`FiltroId`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BuscaNuvem`
--

DROP TABLE IF EXISTS `BuscaNuvem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BuscaNuvem` (
  `BuscaNuvemId` int(11) NOT NULL AUTO_INCREMENT,
  `Palavra` char(30) DEFAULT NULL,
  `Tamanho` tinyint(4) unsigned DEFAULT NULL,
  `Posicao` tinyint(4) unsigned DEFAULT NULL,
  `SiteId` tinyint(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`BuscaNuvemId`)
) ENGINE=InnoDB AUTO_INCREMENT=67 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BuscaSugestao`
--

DROP TABLE IF EXISTS `BuscaSugestao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BuscaSugestao` (
  `ProdutoId` int(11) unsigned NOT NULL,
  `Codigo` char(10) NOT NULL,
  `Nome` varchar(255) NOT NULL,
  `Descricao` text,
  `ImagemSrc` varchar(45) NOT NULL,
  `Total` int(10) unsigned NOT NULL,
  `Disponivel` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ProdutoId`),
  FULLTEXT KEY `Nome` (`Nome`),
  FULLTEXT KEY `Codigo` (`Codigo`),
  FULLTEXT KEY `Descricao` (`Descricao`),
  FULLTEXT KEY `BuscaSugestaoIndex` (`Codigo`,`Nome`,`Descricao`)
) ENGINE=MyISAM DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `BuscaSugestaoTermos`
--

DROP TABLE IF EXISTS `BuscaSugestaoTermos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `BuscaSugestaoTermos` (
  `TermoId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Termo` char(60) NOT NULL,
  PRIMARY KEY (`TermoId`),
  FULLTEXT KEY `Termo` (`Termo`)
) ENGINE=MyISAM AUTO_INCREMENT=1101 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CaminhaoSorte`
--

DROP TABLE IF EXISTS `CaminhaoSorte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CaminhaoSorte` (
  `CaminhaoSorteId` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(255) NOT NULL,
  `ValorCusto` decimal(9,2) unsigned NOT NULL,
  `ValorVenda` decimal(9,2) unsigned NOT NULL,
  `ParcelamentoMaximo` tinyint(2) unsigned NOT NULL DEFAULT '1',
  `SiteId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`CaminhaoSorteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CaminhaoSorte_Numero`
--

DROP TABLE IF EXISTS `CaminhaoSorte_Numero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CaminhaoSorte_Numero` (
  `CaminhaoSorteNumeroId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Lote` varchar(10) NOT NULL,
  `Numero` char(5) NOT NULL,
  `Sorteado` tinyint(1) NOT NULL DEFAULT '0',
  `DataImportacao` datetime NOT NULL,
  `UsuarioImportacao` int(11) unsigned NOT NULL,
  `Usado` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`CaminhaoSorteNumeroId`),
  KEY `CaminhaoSorte_Numero_Usado` (`Usado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Campanha`
--

DROP TABLE IF EXISTS `Campanha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Campanha` (
  `CampanhaId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `SiteId` tinyint(3) unsigned NOT NULL,
  `ParceiraId` mediumint(8) unsigned DEFAULT NULL,
  `Nome` char(50) NOT NULL,
  `Status` tinyint(1) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`CampanhaId`)
) ENGINE=InnoDB AUTO_INCREMENT=18740 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Campanha_Cadastro`
--

DROP TABLE IF EXISTS `Campanha_Cadastro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Campanha_Cadastro` (
  `CampanhaId` int(11) NOT NULL,
  `Email` varchar(255) NOT NULL,
  `SiteId` int(11) NOT NULL,
  `Nome` varchar(255) DEFAULT NULL,
  `Data` datetime NOT NULL,
  PRIMARY KEY (`CampanhaId`,`Email`,`SiteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Caracteristica`
--

DROP TABLE IF EXISTS `Caracteristica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Caracteristica` (
  `CaracteristicaId` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(45) NOT NULL DEFAULT '',
  `Status` tinyint(1) NOT NULL DEFAULT '0',
  `Agrupador` varchar(45) DEFAULT NULL,
  `Tipo` tinyint(1) DEFAULT NULL,
  `Unidade` varchar(100) DEFAULT NULL,
  `Exibicao` tinyint(4) DEFAULT NULL,
  `Icone` char(100) DEFAULT NULL,
  PRIMARY KEY (`CaracteristicaId`),
  KEY `Caracteristica_Status` (`Status`),
  KEY `Agrupador` (`Agrupador`),
  KEY `Tipo` (`Tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=28499 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CaracteristicaValor`
--

DROP TABLE IF EXISTS `CaracteristicaValor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CaracteristicaValor` (
  `CaracteristicaValorId` int(11) NOT NULL AUTO_INCREMENT,
  `CaracteristicaId` int(11) NOT NULL DEFAULT '0',
  `Valor` varchar(255) NOT NULL DEFAULT '',
  `Label` varchar(255) DEFAULT NULL,
  `Exibicao` tinyint(4) DEFAULT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CaracteristicaValorId`),
  KEY `fk_CaracteristicaValor_Caracteristica` (`CaracteristicaId`),
  KEY `CaracteristicaValor_Status` (`Status`)
) ENGINE=InnoDB AUTO_INCREMENT=3203214 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CaracteristicaValor_Detalhes`
--

DROP TABLE IF EXISTS `CaracteristicaValor_Detalhes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CaracteristicaValor_Detalhes` (
  `CaracteristicaValorId` int(11) unsigned NOT NULL,
  `Titulo` char(100) DEFAULT NULL,
  `Imagem` char(100) DEFAULT NULL,
  `Video` char(255) DEFAULT NULL,
  `Descricao` text,
  PRIMARY KEY (`CaracteristicaValorId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CaracteristicaValor_Selo`
--

DROP TABLE IF EXISTS `CaracteristicaValor_Selo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CaracteristicaValor_Selo` (
  `SiteId` int(10) unsigned NOT NULL,
  `CaracteristicaValorId` int(10) unsigned NOT NULL,
  `SeloSrc` varchar(255) NOT NULL,
  `Status` tinyint(1) NOT NULL,
  PRIMARY KEY (`SiteId`,`CaracteristicaValorId`),
  UNIQUE KEY `CaracteristicaValor_Selo_Index_Status` (`Status`,`CaracteristicaValorId`,`SiteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Caracteristica_Aplicativo`
--

DROP TABLE IF EXISTS `Caracteristica_Aplicativo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Caracteristica_Aplicativo` (
  `CaracteristicaAplicativoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CaracteristicaId` int(11) unsigned NOT NULL DEFAULT '0',
  `SiteId` int(11) unsigned NOT NULL DEFAULT '0',
  `Ordem` int(11) unsigned NOT NULL,
  PRIMARY KEY (`CaracteristicaAplicativoId`)
) ENGINE=InnoDB AUTO_INCREMENT=63 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Caracteristica_Categoria`
--

DROP TABLE IF EXISTS `Caracteristica_Categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Caracteristica_Categoria` (
  `CaracteristicaId` int(11) NOT NULL DEFAULT '0',
  `CategoriaId` int(11) NOT NULL DEFAULT '0',
  `Ordem` int(11) NOT NULL,
  PRIMARY KEY (`CaracteristicaId`,`CategoriaId`),
  KEY `fk_Caracteristica_Categoria_Caracteristica` (`CaracteristicaId`),
  KEY `fk_Caracteristica_Categoria_Categoria` (`CategoriaId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Carrinho`
--

DROP TABLE IF EXISTS `Carrinho`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Carrinho` (
  `CarrinhoId` int(11) NOT NULL AUTO_INCREMENT,
  `SessionId` varchar(50) NOT NULL DEFAULT '0',
  `ClienteId` int(11) DEFAULT NULL,
  `SiteId` int(11) DEFAULT NULL,
  PRIMARY KEY (`CarrinhoId`),
  KEY `fk_Carrinho_SessionData` (`SessionId`),
  KEY `fk_Carrinho_Cliente` (`ClienteId`),
  KEY `CarrinhoSiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=6449960 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Carrinho_Produto`
--

DROP TABLE IF EXISTS `Carrinho_Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Carrinho_Produto` (
  `CarrinhoId` int(11) NOT NULL DEFAULT '0',
  `ProdutoId` int(11) NOT NULL DEFAULT '0',
  `Quantidade` int(11) NOT NULL DEFAULT '0',
  `Status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = ExcluÃ­do\n1 = Ativo\n2 = Comprado',
  `DataAdicionado` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DataAlterado` datetime DEFAULT NULL,
  PRIMARY KEY (`CarrinhoId`,`ProdutoId`),
  KEY `fk_Carrinho_Produto_Carrinho` (`CarrinhoId`),
  KEY `fk_Carrinho_Produto_Produto` (`ProdutoId`),
  KEY `Carrinho_Produto_Status` (`Status`),
  KEY `Carrinho_Produto_DataAdicionado` (`DataAdicionado`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CarrocelLojas`
--

DROP TABLE IF EXISTS `CarrocelLojas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CarrocelLojas` (
  `CarrocelLojasId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SiteId` int(11) unsigned NOT NULL,
  `CategoriaId` int(11) unsigned NOT NULL,
  `Ordem` tinyint(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`CarrocelLojasId`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CarrocelLojas_Categoria`
--

DROP TABLE IF EXISTS `CarrocelLojas_Categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CarrocelLojas_Categoria` (
  `CarrocelLojasCategoriaId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CarrocelLojasId` int(11) unsigned NOT NULL,
  `CategoriaId` int(11) unsigned NOT NULL,
  `Ordem` tinyint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`CarrocelLojasCategoriaId`)
) ENGINE=InnoDB AUTO_INCREMENT=50 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CarrocelLojas_Produto`
--

DROP TABLE IF EXISTS `CarrocelLojas_Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CarrocelLojas_Produto` (
  `CarrocelLojasProdutoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CarrocelLojasId` int(11) unsigned NOT NULL,
  `ProdutoId` int(11) unsigned NOT NULL,
  `Ordem` tinyint(4) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`CarrocelLojasProdutoId`)
) ENGINE=InnoDB AUTO_INCREMENT=68 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CarrocelNokia`
--

DROP TABLE IF EXISTS `CarrocelNokia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CarrocelNokia` (
  `ProdutoId` mediumint(8) unsigned NOT NULL,
  `Exibir` tinyint(1) unsigned DEFAULT '0',
  `Ordem` mediumint(8) unsigned DEFAULT NULL,
  PRIMARY KEY (`ProdutoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CarrocelVideos`
--

DROP TABLE IF EXISTS `CarrocelVideos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CarrocelVideos` (
  `CarrocelVideosId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CategoriaId` int(11) unsigned NOT NULL,
  `SiteId` int(11) NOT NULL,
  `Status` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`CarrocelVideosId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CarrocelVideos_Detalhes`
--

DROP TABLE IF EXISTS `CarrocelVideos_Detalhes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CarrocelVideos_Detalhes` (
  `DetalhesItemId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ItemId` int(11) unsigned NOT NULL,
  `Tipo` tinyint(3) unsigned NOT NULL,
  `Imagem` char(50) DEFAULT NULL,
  `Texto` varchar(500) DEFAULT NULL,
  `Link` varchar(350) DEFAULT NULL,
  `Titulo` varchar(350) DEFAULT NULL,
  `ProdutoId` int(11) unsigned DEFAULT NULL,
  `Ordem` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`DetalhesItemId`),
  KEY `CarrocelVideos_DetalhesIndex1` (`ItemId`)
) ENGINE=InnoDB AUTO_INCREMENT=54 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CarrocelVideos_Itens`
--

DROP TABLE IF EXISTS `CarrocelVideos_Itens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CarrocelVideos_Itens` (
  `ItemId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CarrocelVideosId` int(11) unsigned NOT NULL,
  `Link` varchar(350) DEFAULT NULL,
  `Imagem` char(50) DEFAULT NULL,
  `Ordem` tinyint(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`ItemId`),
  KEY `CarrocelVideos_ItensIndex1` (`CarrocelVideosId`)
) ENGINE=InnoDB AUTO_INCREMENT=30 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Carrocel_GrandesMarcas`
--

DROP TABLE IF EXISTS `Carrocel_GrandesMarcas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Carrocel_GrandesMarcas` (
  `CarrocelId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SiteId` int(11) unsigned NOT NULL,
  `Ordem` tinyint(3) unsigned DEFAULT NULL,
  `Imagem` varchar(50) DEFAULT NULL,
  `Link` varchar(350) DEFAULT NULL,
  `CategoriaId` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`CarrocelId`)
) ENGINE=InnoDB AUTO_INCREMENT=85 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CartuchoImpressora`
--

DROP TABLE IF EXISTS `CartuchoImpressora`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CartuchoImpressora` (
  `ProdutoId` int(11) unsigned NOT NULL,
  `ModeloImpressoraId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`ProdutoId`,`ModeloImpressoraId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Categoria`
--

DROP TABLE IF EXISTS `Categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Categoria` (
  `CategoriaId` smallint(4) unsigned NOT NULL,
  `CategoriaAsc` smallint(4) unsigned NOT NULL,
  `Nome` varchar(45) NOT NULL DEFAULT '',
  `Nivel` tinyint(3) unsigned NOT NULL,
  `MetaEstoque` mediumint(8) unsigned NOT NULL,
  `VendeAvulso` tinyint(1) DEFAULT '1',
  `VariacoesTitulo` varchar(100) DEFAULT NULL COMMENT 'SEO Title',
  `VariacoesDescricao` varchar(100) DEFAULT NULL COMMENT 'SEO Meta Description',
  `Tipo` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 = Categoria normal, 1 = Perfil',
  `Url` char(255) DEFAULT '',
  `NovaAba` tinyint(1) DEFAULT '0',
  PRIMARY KEY (`CategoriaId`),
  KEY `fk_Categoria_Categoria` (`CategoriaAsc`),
  KEY `CategoriaNivelIndex` (`Nivel`),
  KEY `TipoPerfil` (`Tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CategoriaAssociada`
--

DROP TABLE IF EXISTS `CategoriaAssociada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CategoriaAssociada` (
  `CategoriaId` int(11) NOT NULL DEFAULT '0',
  `AssociadaId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CategoriaId`,`AssociadaId`),
  KEY `fk_Categoria_Categoria_Categoria` (`CategoriaId`),
  KEY `fk_Categoria_Categoria_Categoria1` (`AssociadaId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CategoriaAssociadaGoogle`
--

DROP TABLE IF EXISTS `CategoriaAssociadaGoogle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CategoriaAssociadaGoogle` (
  `CatID` int(11) unsigned NOT NULL,
  `CatGoogleID` int(11) unsigned NOT NULL,
  PRIMARY KEY (`CatID`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CategoriaReplicacao`
--

DROP TABLE IF EXISTS `CategoriaReplicacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CategoriaReplicacao` (
  `CategoriaReplicacaoId` int(11) NOT NULL AUTO_INCREMENT,
  `SiteId` int(11) NOT NULL,
  `CategoriaId` int(11) NOT NULL,
  `SiteIdReplicar` int(11) NOT NULL,
  PRIMARY KEY (`CategoriaReplicacaoId`)
) ENGINE=InnoDB AUTO_INCREMENT=2232 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CategoriaSigla`
--

DROP TABLE IF EXISTS `CategoriaSigla`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CategoriaSigla` (
  `CategoriaId` smallint(6) unsigned NOT NULL,
  `Sigla` char(3) DEFAULT NULL,
  PRIMARY KEY (`CategoriaId`),
  UNIQUE KEY `CategoriaId` (`CategoriaId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Categoria_Banner`
--

DROP TABLE IF EXISTS `Categoria_Banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Categoria_Banner` (
  `CategoriaId` int(11) NOT NULL DEFAULT '0',
  `BannerId` int(11) NOT NULL DEFAULT '0',
  `Status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`CategoriaId`,`BannerId`),
  KEY `fk_Categoria_Banner_Categoria` (`CategoriaId`),
  KEY `fk_Categoria_Banner_Banner` (`BannerId`),
  KEY `Categoria_Banner_Status` (`Status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Categoria_Site`
--

DROP TABLE IF EXISTS `Categoria_Site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Categoria_Site` (
  `CategoriaId` int(11) NOT NULL DEFAULT '0',
  `SiteId` int(11) NOT NULL DEFAULT '0',
  `Status` tinyint(4) DEFAULT NULL,
  `Ordem` int(11) DEFAULT NULL,
  `MostrarNaHome` tinyint(4) DEFAULT NULL,
  `MostrarNaLoja` tinyint(4) DEFAULT NULL,
  `VitrineId` int(11) DEFAULT NULL,
  `CorBackground` char(6) DEFAULT NULL,
  `CorFonte` char(6) DEFAULT NULL,
  `Icone` char(100) DEFAULT NULL,
  `MostrarFabricante` tinyint(3) unsigned DEFAULT '0',
  `MostrarMenuReduzido` tinyint(1) unsigned DEFAULT NULL,
  `TextoSEO` varchar(320) DEFAULT NULL,
  `VariacoesTitulo` varchar(100) DEFAULT NULL,
  `VariacoesDescricao` varchar(160) DEFAULT NULL,
  `IconeMobile` varchar(100) DEFAULT NULL,
  `OrdemMobile` int(11) DEFAULT '0',
  PRIMARY KEY (`CategoriaId`,`SiteId`),
  KEY `CategoriaComercialStatus` (`Status`),
  KEY `CategoriaComercialMH` (`MostrarNaHome`),
  KEY `CategoriaComercialMl` (`MostrarNaLoja`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CentralLista`
--

DROP TABLE IF EXISTS `CentralLista`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CentralLista` (
  `CentralListaId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ClienteId` int(11) unsigned NOT NULL,
  `TipoListaId` int(11) NOT NULL,
  `Identificacao` char(100) DEFAULT NULL,
  `Url` char(255) DEFAULT NULL,
  `Titulo` char(100) DEFAULT NULL,
  `Mensagem` text,
  `Foto` char(100) DEFAULT NULL,
  `Origem` tinyint(4) unsigned DEFAULT NULL COMMENT '1 = Revistas\n2 = Eventos\n3 = M�dia\n4 = Twitter\n5 = Curso\n6 = Amigos\n7 = Site',
  `DataEvento` datetime DEFAULT NULL,
  `Status` tinyint(4) unsigned DEFAULT NULL COMMENT '0 = Aguardando Libera��o / 1 = Ativa / \n2 = Expirada ou  Cancelada',
  `LocalEvento` char(120) DEFAULT NULL,
  `EnderecoEntregaId` int(11) unsigned DEFAULT NULL,
  `DataCriacao` datetime DEFAULT NULL,
  `StatusExportacao` tinyint(4) unsigned DEFAULT '0',
  `SiteId` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`CentralListaId`),
  KEY `fk_CentralLista_Cliente1` (`ClienteId`),
  KEY `EntregaId` (`EnderecoEntregaId`),
  KEY `idx_StatusExportacao` (`StatusExportacao`),
  KEY `DataEvento` (`DataEvento`,`Status`,`SiteId`),
  KEY `CentralListaId` (`SiteId`,`DataEvento`,`Status`),
  KEY `idx_TipoListaId` (`TipoListaId`)
) ENGINE=InnoDB AUTO_INCREMENT=74992 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`riel_RicBDu`@`%`*/ /*!50003 TRIGGER `MaxListaId_CentralLista` BEFORE INSERT ON `CentralLista`
    FOR EACH ROW BEGIN
SET NEW.CentralListaId = (SELECT GREATEST(MAX(C.CentralListaId), MAX(L.ListaCasamentoId)) AS ListaId FROM CentralLista C, ListaCasamento L) + 1;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `CentralLista_Convidado`
--

DROP TABLE IF EXISTS `CentralLista_Convidado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CentralLista_Convidado` (
  `ConvidadoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `ConviteId` int(11) unsigned NOT NULL,
  `Email` char(120) DEFAULT NULL,
  `Nome` char(100) DEFAULT NULL,
  `StatusEnvio` tinyint(4) unsigned DEFAULT NULL,
  `SiteId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`ConvidadoId`),
  KEY `fk_CentralListaConvidado_CentralListaConvite1` (`ConviteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CentralLista_Convite`
--

DROP TABLE IF EXISTS `CentralLista_Convite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CentralLista_Convite` (
  `ConviteId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` char(120) DEFAULT NULL,
  `Mensagem` text,
  `CentralListaId` int(11) unsigned NOT NULL,
  `SiteId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`ConviteId`),
  KEY `fk_CentralListaConvite_CentralLista1` (`CentralListaId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CentralLista_Integrante`
--

DROP TABLE IF EXISTS `CentralLista_Integrante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CentralLista_Integrante` (
  `IntegranteId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` char(60) NOT NULL,
  `Tipo` tinyint(4) unsigned NOT NULL COMMENT '1 => Responsavel',
  `CentralListaId` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`IntegranteId`)
) ENGINE=InnoDB AUTO_INCREMENT=570 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CentralLista_Integrante_Tipo`
--

DROP TABLE IF EXISTS `CentralLista_Integrante_Tipo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CentralLista_Integrante_Tipo` (
  `TipoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `NomeTipo` char(60) NOT NULL,
  `Status` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1 => ONLINE, 0 => OFFLINE',
  PRIMARY KEY (`TipoId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CentralLista_MensagemPedido`
--

DROP TABLE IF EXISTS `CentralLista_MensagemPedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CentralLista_MensagemPedido` (
  `CentralListaId` int(11) unsigned NOT NULL,
  `PedidoId` int(11) unsigned NOT NULL,
  `Mensagem` text NOT NULL,
  PRIMARY KEY (`CentralListaId`,`PedidoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CentralLista_Origem`
--

DROP TABLE IF EXISTS `CentralLista_Origem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CentralLista_Origem` (
  `OrigemId` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` char(50) DEFAULT NULL,
  `Status` tinyint(1) unsigned DEFAULT NULL,
  `Ordem` tinyint(4) unsigned DEFAULT NULL,
  `SiteId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`OrigemId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CentralLista_Produto`
--

DROP TABLE IF EXISTS `CentralLista_Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CentralLista_Produto` (
  `CentralListaId` int(11) NOT NULL,
  `ProdutoId` int(11) NOT NULL,
  `Quantidade` int(11) DEFAULT NULL,
  `Status` tinyint(1) unsigned zerofill DEFAULT NULL COMMENT '0 => Offline, 1 => Online',
  `Adicionado` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`CentralListaId`,`ProdutoId`),
  KEY `fk_CentralLista_has_Produto_CentralLista1` (`CentralListaId`),
  KEY `fk_CentralLista_has_Produto_Produto1` (`ProdutoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CentralLista_Solicitacao`
--

DROP TABLE IF EXISTS `CentralLista_Solicitacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CentralLista_Solicitacao` (
  `CentralLista_SolicitacaoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CentralListaId` int(11) unsigned NOT NULL DEFAULT '0',
  `PedidoId` int(11) NOT NULL,
  `TipoSolicitacao` tinyint(1) unsigned DEFAULT '0' COMMENT '0 => Pendente, 1 => Entrega e 2 => Vale-troca',
  `DataSolicitacao` datetime DEFAULT NULL,
  `StatusExportacao` tinyint(4) unsigned DEFAULT NULL COMMENT '0 => Nao Faz nada, 1 => Exportar Sige, 2 => Exportado, 3 => Erro',
  `SiteId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`CentralLista_SolicitacaoId`),
  UNIQUE KEY `CentralLista_SolicitacaoId` (`CentralLista_SolicitacaoId`),
  KEY `CentralListaId` (`CentralListaId`),
  KEY `PedidoId` (`PedidoId`),
  KEY `TipoSolicitacao` (`TipoSolicitacao`)
) ENGINE=InnoDB AUTO_INCREMENT=13309 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CentralLista_TipoLista`
--

DROP TABLE IF EXISTS `CentralLista_TipoLista`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CentralLista_TipoLista` (
  `TipoListaId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Tipo` char(4) NOT NULL COMMENT 'Codigo no SIGE',
  `Nome` char(60) NOT NULL,
  `SiteId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`TipoListaId`),
  KEY `idx_SiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Cep`
--

DROP TABLE IF EXISTS `Cep`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Cep` (
  `Cep` varchar(8) NOT NULL DEFAULT '',
  `Estado` char(2) DEFAULT '',
  `Cidade` varchar(100) DEFAULT '',
  `Bairro` varchar(100) DEFAULT '',
  `Logradouro` varchar(150) DEFAULT '',
  `Status` tinyint(1) DEFAULT '1',
  KEY `Cep_Estado` (`Estado`,`Status`),
  KEY `Cep_Cidade` (`Cidade`,`Status`),
  KEY `Cep_Bairro` (`Bairro`,`Status`),
  KEY `Cep` (`Cep`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CheckReplicacao`
--

DROP TABLE IF EXISTS `CheckReplicacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CheckReplicacao` (
  `Referencia` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `IP` char(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Cliente`
--

DROP TABLE IF EXISTS `Cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Cliente` (
  `ClienteId` int(11) NOT NULL AUTO_INCREMENT,
  `EnderecoCobrancaId` int(11) NOT NULL DEFAULT '0',
  `EnderecoEntregaId` int(11) DEFAULT '0',
  `Email` varchar(60) NOT NULL DEFAULT '',
  `Senha` varchar(32) NOT NULL DEFAULT '',
  `DataCadastro` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Tipo` enum('pf','pj') NOT NULL DEFAULT 'pf',
  `Status` tinyint(4) NOT NULL DEFAULT '1',
  `News` int(1) NOT NULL DEFAULT '0' COMMENT 'se = 0 nao recebe news - se =1 recebe news',
  `SiteId` int(11) DEFAULT NULL,
  PRIMARY KEY (`ClienteId`),
  UNIQUE KEY `EmailUnique` (`Email`,`SiteId`),
  KEY `fk_Cliente_Endereco` (`EnderecoCobrancaId`),
  KEY `Cliente_Login` (`Email`,`Senha`),
  KEY `ClienteSiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=12169862 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ClientePf`
--

DROP TABLE IF EXISTS `ClientePf`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClientePf` (
  `ClienteId` int(11) NOT NULL DEFAULT '0',
  `Nome` varchar(30) NOT NULL DEFAULT '',
  `Sobrenome` varchar(70) NOT NULL DEFAULT '',
  `Apelido` varchar(30) DEFAULT '',
  `CPF` varchar(14) NOT NULL DEFAULT '',
  `DataNascimento` date NOT NULL DEFAULT '0000-00-00',
  `Sexo` enum('M','F') NOT NULL DEFAULT 'M',
  `Profissao` tinyint(4) DEFAULT '0',
  `AtributoTamanhoId` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`ClienteId`),
  KEY `fk_ClientePf_Cliente` (`ClienteId`),
  KEY `idx_CPF` (`CPF`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ClientePj`
--

DROP TABLE IF EXISTS `ClientePj`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClientePj` (
  `ClienteId` int(11) NOT NULL DEFAULT '0',
  `RazaoSocial` varchar(100) NOT NULL DEFAULT '',
  `NomeFantasia` varchar(100) NOT NULL DEFAULT '',
  `CNPJ` varchar(18) NOT NULL DEFAULT '',
  `InscricaoEstadual` varchar(14) DEFAULT NULL,
  `Isento` int(1) NOT NULL DEFAULT '0' COMMENT 'se = 0 nao isento, se = 1 isento',
  `Telefone` varchar(14) NOT NULL,
  `Site` varchar(150) DEFAULT NULL,
  `RamoAtividade` tinyint(4) DEFAULT '0',
  `Cnae` varchar(7) DEFAULT NULL COMMENT 'Codigo cnae',
  PRIMARY KEY (`ClienteId`),
  KEY `fk_ClientePj_Cliente` (`ClienteId`),
  KEY `idx_CNPJ` (`CNPJ`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ClubeDesconto_AssinanteNews_Confirmacao`
--

DROP TABLE IF EXISTS `ClubeDesconto_AssinanteNews_Confirmacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClubeDesconto_AssinanteNews_Confirmacao` (
  `AssinanteNewsId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`AssinanteNewsId`),
  UNIQUE KEY `AssinanteNewsId` (`AssinanteNewsId`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ClubeDesconto_Background`
--

DROP TABLE IF EXISTS `ClubeDesconto_Background`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClubeDesconto_Background` (
  `BackgroundId` int(11) NOT NULL AUTO_INCREMENT,
  `Background` char(50) NOT NULL,
  `Posicao` tinyint(3) NOT NULL,
  `SiteId` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`BackgroundId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ClubeDesconto_Config`
--

DROP TABLE IF EXISTS `ClubeDesconto_Config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClubeDesconto_Config` (
  `SiteId` tinyint(3) unsigned NOT NULL,
  `OfertaDescontoMinimo` float DEFAULT NULL,
  `OfertaMinutoDescontoMinimo` float DEFAULT NULL,
  `DescontoFrete` float DEFAULT NULL,
  `ValorCredito` float DEFAULT NULL,
  `BannerCheckout` char(60) DEFAULT NULL,
  `EstoqueExibicao` int(11) DEFAULT NULL,
  `PrazoAdicional` int(11) DEFAULT NULL,
  PRIMARY KEY (`SiteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ClubeDesconto_Indicacao`
--

DROP TABLE IF EXISTS `ClubeDesconto_Indicacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClubeDesconto_Indicacao` (
  `IndicacaoId` int(11) NOT NULL,
  `SiteId` tinyint(3) unsigned NOT NULL,
  `OfertaId` int(11) NOT NULL DEFAULT '0',
  `ClienteId` int(11) NOT NULL,
  `ValorCredito` float NOT NULL,
  `EmailIndicado` varchar(60) NOT NULL,
  `DataIndicacao` datetime NOT NULL,
  `DataCadastro` datetime NOT NULL,
  `Status` tinyint(4) NOT NULL COMMENT '0 = Pendente, 1 = Indicado se cadastrou',
  `Utilizado` tinyint(1) NOT NULL DEFAULT '0' COMMENT 'Se o cliente j� usou o valor em alguma compra',
  PRIMARY KEY (`IndicacaoId`,`SiteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ClubeDesconto_IndicacaoInauguracao`
--

DROP TABLE IF EXISTS `ClubeDesconto_IndicacaoInauguracao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClubeDesconto_IndicacaoInauguracao` (
  `IndicacaoInauguracaoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `AssinanteNewsId` int(11) unsigned NOT NULL,
  `IndicadoId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`IndicacaoInauguracaoId`),
  UNIQUE KEY `IndicacaoInauguracaoId` (`IndicacaoInauguracaoId`),
  KEY `AssinanteNewsId` (`AssinanteNewsId`)
) ENGINE=InnoDB AUTO_INCREMENT=287967 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ClubeDesconto_Oferta`
--

DROP TABLE IF EXISTS `ClubeDesconto_Oferta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClubeDesconto_Oferta` (
  `OfertaId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SiteId` tinyint(3) unsigned NOT NULL,
  `ProdutoId` int(11) NOT NULL DEFAULT '0',
  `NomeProduto` varchar(100) NOT NULL,
  `DescricaoProduto` text NOT NULL,
  `DataHoraInicio` datetime NOT NULL,
  `DataHoraFim` datetime DEFAULT NULL,
  `DescontoOferta` float NOT NULL,
  `PrazoGordura` int(3) DEFAULT NULL COMMENT 'NULL = utilizar o do RE',
  `ValorFrete` float DEFAULT NULL COMMENT '0 = frete gr�tis, NULL = utilizar o do RE',
  `Estoque` int(11) NOT NULL,
  `EstoqueDisponivel` int(11) NOT NULL,
  `Principal` tinyint(1) NOT NULL,
  `Ordem` tinyint(3) NOT NULL,
  `Tipo` tinyint(1) NOT NULL DEFAULT '1' COMMENT '1 = Oferta normal, 2 = Oferta do Minuto',
  `Status` tinyint(4) NOT NULL,
  `TipoFrete` tinyint(1) unsigned DEFAULT NULL COMMENT '1 - Adicional, 2 - Fixo, 3 - Site',
  PRIMARY KEY (`OfertaId`,`SiteId`),
  KEY `idx_site_produto` (`ProdutoId`,`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=12145 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ClubeDesconto_Oferta2`
--

DROP TABLE IF EXISTS `ClubeDesconto_Oferta2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClubeDesconto_Oferta2` (
  `OfertaId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SiteId` tinyint(3) unsigned NOT NULL,
  `ProdutoId` int(11) NOT NULL DEFAULT '0',
  `DataHoraInicio` datetime DEFAULT NULL,
  `DataHoraFim` datetime DEFAULT NULL,
  `DataHoraInicioLateral` datetime DEFAULT NULL,
  `DataHoraFimLateral` datetime DEFAULT NULL,
  `DescontoOferta` float(10,6) NOT NULL DEFAULT '0.000000',
  `TipoDesconto` tinyint(1) DEFAULT NULL,
  `Ordem` tinyint(3) NOT NULL DEFAULT '0',
  `Tipo` tinyint(1) NOT NULL DEFAULT '1',
  `Status` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`OfertaId`,`SiteId`,`ProdutoId`),
  UNIQUE KEY `idx_site_produto` (`ProdutoId`,`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=47245 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ClubeDesconto_OfertaExpirada`
--

DROP TABLE IF EXISTS `ClubeDesconto_OfertaExpirada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClubeDesconto_OfertaExpirada` (
  `OfertaExpiradaId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SiteId` tinyint(4) NOT NULL,
  `ProdutoId` int(11) NOT NULL,
  `NomeProduto` varchar(100) NOT NULL,
  `DataFim` date NOT NULL,
  `DescontoOferta` decimal(6,4) NOT NULL,
  `PrecoDe` decimal(9,2) NOT NULL,
  `PrecoPor` decimal(9,2) NOT NULL,
  `Estoque` int(11) NOT NULL,
  `Tipo` tinyint(4) NOT NULL COMMENT '1 = Oferta normal, 2 = Oferta do Minuto',
  `Principal` tinyint(4) NOT NULL,
  PRIMARY KEY (`OfertaExpiradaId`),
  UNIQUE KEY `OfertaExpiradaId` (`OfertaExpiradaId`),
  KEY `SiteId` (`SiteId`),
  KEY `DataFim` (`DataFim`)
) ENGINE=InnoDB AUTO_INCREMENT=21791 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ClubeDesconto_OfertaExpirada2`
--

DROP TABLE IF EXISTS `ClubeDesconto_OfertaExpirada2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClubeDesconto_OfertaExpirada2` (
  `OfertaExpiradaId` int(11) NOT NULL AUTO_INCREMENT,
  `SiteId` tinyint(4) unsigned NOT NULL,
  `ProdutoId` int(11) unsigned NOT NULL,
  `NomeProduto` varchar(100) NOT NULL,
  `DataFim` datetime NOT NULL,
  `DescontoOferta` decimal(6,2) NOT NULL,
  `TipoDesconto` tinyint(4) NOT NULL,
  `PrecoDe` decimal(9,2) NOT NULL,
  `PrecoPor` decimal(9,2) NOT NULL,
  `Tipo` tinyint(4) NOT NULL,
  `ParcelamentoMaximo` tinyint(4) NOT NULL,
  PRIMARY KEY (`OfertaExpiradaId`),
  KEY `SiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=47511 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ClubeDesconto_Oferta_Log`
--

DROP TABLE IF EXISTS `ClubeDesconto_Oferta_Log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClubeDesconto_Oferta_Log` (
  `OfertaLogId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `OfertaId` int(11) unsigned NOT NULL,
  `SiteId` tinyint(3) unsigned NOT NULL,
  `ProdutoId` int(11) NOT NULL DEFAULT '0',
  `NomeProduto` varchar(100) NOT NULL,
  `DescricaoProduto` text NOT NULL,
  `DataHoraInicio` datetime NOT NULL,
  `DataHoraFim` datetime DEFAULT NULL,
  `DescontoOferta` decimal(6,4) NOT NULL,
  `PrazoGordura` tinyint(3) unsigned DEFAULT NULL COMMENT 'NULL = utilizar o do RE',
  `TipoFrete` tinyint(1) unsigned DEFAULT NULL COMMENT '1 - Adicional, 2 - Fixo, 3 - Site',
  `ValorFrete` decimal(5,2) DEFAULT NULL COMMENT '0 = frete gr�tis, NULL = utilizar o do RE',
  `Estoque` int(11) unsigned NOT NULL,
  `EstoqueDisponivel` int(11) unsigned NOT NULL,
  `Principal` tinyint(1) NOT NULL,
  `Ordem` tinyint(3) unsigned NOT NULL,
  `Tipo` tinyint(1) unsigned NOT NULL DEFAULT '1' COMMENT '1 = Oferta normal, 2 = Oferta do Minuto',
  `Status` tinyint(4) unsigned NOT NULL,
  `DataAlteracao` datetime DEFAULT NULL,
  `UsuarioAlteracao` int(11) NOT NULL,
  PRIMARY KEY (`OfertaLogId`),
  UNIQUE KEY `OfertaLogId` (`OfertaLogId`),
  KEY `SiteId` (`SiteId`),
  KEY `ProdutoId` (`ProdutoId`),
  KEY `OfertaId` (`OfertaId`)
) ENGINE=InnoDB AUTO_INCREMENT=57214 DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ClubeDesconto_Parceiro`
--

DROP TABLE IF EXISTS `ClubeDesconto_Parceiro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClubeDesconto_Parceiro` (
  `ParceiroId` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` char(60) NOT NULL,
  PRIMARY KEY (`ParceiroId`),
  UNIQUE KEY `ParceiroId` (`ParceiroId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=23 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ClubeDesconto_Parceiro_Cadastro`
--

DROP TABLE IF EXISTS `ClubeDesconto_Parceiro_Cadastro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClubeDesconto_Parceiro_Cadastro` (
  `ParceiroId` tinyint(3) unsigned NOT NULL,
  `AssinanteNewsId` int(11) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ClubeDesconto_Ranking`
--

DROP TABLE IF EXISTS `ClubeDesconto_Ranking`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ClubeDesconto_Ranking` (
  `RankingPosicao` tinyint(3) unsigned NOT NULL,
  `AssinanteNewsId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`RankingPosicao`),
  UNIQUE KEY `RankingPosicao` (`RankingPosicao`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Cnae`
--

DROP TABLE IF EXISTS `Cnae`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Cnae` (
  `Id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Codigo` varchar(7) NOT NULL,
  `Descricao` varchar(200) NOT NULL,
  `Restrito` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 - não restrito, 1 - restrito',
  PRIMARY KEY (`Id`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Combo`
--

DROP TABLE IF EXISTS `Combo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Combo` (
  `ComboId` mediumint(8) unsigned NOT NULL,
  `ProdutoId` mediumint(8) unsigned NOT NULL,
  `Valor` float DEFAULT NULL,
  `Quantidade` int(11) NOT NULL DEFAULT '0',
  `SiteId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ComboId`,`ProdutoId`,`SiteId`),
  KEY `fk_Combo_Produto` (`ProdutoId`),
  KEY `ComboSiteId` (`SiteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER InserirComboLog AFTER INSERT ON Combo
  FOR EACH ROW BEGIN
   INSERT INTO Combo_Log(ComboId,ProdutoId,Valor,Quantidade,SiteId,DataAlteracao) VALUES(NEW.ComboId,NEW.ProdutoId,NEW.Valor,NEW.Quantidade,NEW.SiteId,CURRENT_TIMESTAMP);
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER AlterarComboLog AFTER UPDATE ON Combo
  FOR EACH ROW BEGIN
   INSERT INTO Combo_Log(ComboId,ProdutoId,Valor,Quantidade,SiteId,DataAlteracao) VALUES(NEW.ComboId,NEW.ProdutoId,NEW.Valor,NEW.Quantidade,NEW.SiteId,CURRENT_TIMESTAMP);
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `Combo_Log`
--

DROP TABLE IF EXISTS `Combo_Log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Combo_Log` (
  `ComboLogId` int(11) NOT NULL AUTO_INCREMENT,
  `ComboId` mediumint(8) unsigned NOT NULL,
  `ProdutoId` mediumint(8) unsigned NOT NULL,
  `Valor` float DEFAULT NULL,
  `Quantidade` mediumint(8) unsigned DEFAULT '0',
  `SiteId` mediumint(8) unsigned DEFAULT '0',
  `DataAlteracao` datetime DEFAULT NULL,
  PRIMARY KEY (`ComboLogId`)
) ENGINE=InnoDB AUTO_INCREMENT=6188735 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ComentarioEbit`
--

DROP TABLE IF EXISTS `ComentarioEbit`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ComentarioEbit` (
  `ComentarioEbitId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SiteId` tinyint(3) unsigned NOT NULL,
  `Nome` char(100) NOT NULL,
  `Email` char(100) NOT NULL,
  `Comentario` text NOT NULL,
  `Status` tinyint(1) unsigned NOT NULL DEFAULT '0' COMMENT '0 => OffLine, 1 => On Line',
  `Cidade` char(100) DEFAULT NULL,
  `Estado` char(2) DEFAULT NULL,
  PRIMARY KEY (`ComentarioEbitId`),
  KEY `SiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=235 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ComissaoB2C`
--

DROP TABLE IF EXISTS `ComissaoB2C`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ComissaoB2C` (
  `ComissaoB2CId` int(11) NOT NULL AUTO_INCREMENT,
  `OperadorB2CId` int(11) NOT NULL,
  `Periodo` char(10) NOT NULL,
  `Comissao` float DEFAULT NULL,
  `UsuarioId` int(11) DEFAULT NULL,
  `DataCadastro` datetime DEFAULT NULL,
  `Tipo` tinyint(2) unsigned DEFAULT '0' COMMENT '0 valor - 1 porcentagem',
  `CategoriaId` mediumint(8) unsigned DEFAULT NULL,
  `PedidoId` mediumint(8) DEFAULT NULL,
  `TipoEvento` tinyint(2) unsigned DEFAULT '1',
  `FolhaPagamentoId` mediumint(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`ComissaoB2CId`),
  KEY `ComissaoB2C_Operador` (`OperadorB2CId`)
) ENGINE=InnoDB AUTO_INCREMENT=77 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ComissaoB2C_Grupo`
--

DROP TABLE IF EXISTS `ComissaoB2C_Grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ComissaoB2C_Grupo` (
  `ComissaoB2CId` int(11) NOT NULL AUTO_INCREMENT,
  `GrupoB2CId` int(11) NOT NULL,
  `Periodo` char(10) NOT NULL,
  `Comissao` float DEFAULT NULL,
  `UsuarioId` int(11) DEFAULT NULL,
  `DataCadastro` datetime DEFAULT NULL,
  `Tipo` tinyint(2) unsigned DEFAULT NULL COMMENT '0 valor - 1 porcentagem',
  `CategoriaId` mediumint(8) unsigned DEFAULT NULL,
  `GarantiaEstendida` tinyint(4) unsigned DEFAULT '0' COMMENT 'Campo que define se a comissao e da garantia estendida ou nao',
  `GarantiaPrazoId` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`ComissaoB2CId`),
  KEY `ComissaoB2C_Grupo` (`GrupoB2CId`)
) ENGINE=InnoDB AUTO_INCREMENT=55582 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ConciliacaoBraspagSige`
--

DROP TABLE IF EXISTS `ConciliacaoBraspagSige`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ConciliacaoBraspagSige` (
  `ConciliacaoBraspagSigeId` int(11) NOT NULL AUTO_INCREMENT,
  `PedidoId` int(11) NOT NULL,
  `Status` varchar(15) DEFAULT NULL,
  `Fonte` tinyint(4) DEFAULT NULL,
  `Valor` double NOT NULL,
  `DataPedido` datetime NOT NULL,
  `DataImportacao` datetime NOT NULL,
  PRIMARY KEY (`ConciliacaoBraspagSigeId`) USING BTREE,
  KEY `ConciliacaoBraspagSigeIndex1` (`PedidoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ConcursoCultural`
--

DROP TABLE IF EXISTS `ConcursoCultural`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ConcursoCultural` (
  `ConcursoCulturalId` int(11) NOT NULL AUTO_INCREMENT,
  `SiteId` int(11) DEFAULT NULL,
  `Titulo` char(100) DEFAULT NULL,
  `Pergunta` text,
  `Email` char(60) DEFAULT NULL,
  `TextoEmail` text,
  `Status` tinyint(4) DEFAULT NULL,
  `TamanhoResposta` int(11) DEFAULT NULL,
  PRIMARY KEY (`ConcursoCulturalId`)
) ENGINE=InnoDB AUTO_INCREMENT=27 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ConcursoCultural_Banner`
--

DROP TABLE IF EXISTS `ConcursoCultural_Banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ConcursoCultural_Banner` (
  `ConcursoCulturalId` int(11) NOT NULL DEFAULT '0',
  `BannerId` int(11) NOT NULL DEFAULT '0',
  `Status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ConcursoCulturalId`,`BannerId`),
  KEY `fk_ConcursoCultural_Banner_Concurso` (`ConcursoCulturalId`) USING BTREE,
  KEY `fk_ConcursoCultural_Banner_Banner` (`BannerId`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=latin1 ROW_FORMAT=COMPACT;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ConteudoRelacionado`
--

DROP TABLE IF EXISTS `ConteudoRelacionado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ConteudoRelacionado` (
  `ProdutoId` int(11) unsigned NOT NULL,
  `Titulo` varchar(100) NOT NULL,
  `Texto` text NOT NULL,
  `Imagem` varchar(60) DEFAULT NULL,
  `Link` varchar(255) NOT NULL,
  PRIMARY KEY (`ProdutoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Crossell`
--

DROP TABLE IF EXISTS `Crossell`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Crossell` (
  `CrossellId` int(11) NOT NULL,
  `ProdutoId` int(11) NOT NULL,
  `Porcentagem` float DEFAULT NULL,
  `Ordem` tinyint(4) DEFAULT NULL,
  `Titulo` char(200) DEFAULT NULL,
  PRIMARY KEY (`CrossellId`,`ProdutoId`),
  KEY `Produto_has_Produto_Produto1` (`CrossellId`),
  KEY `Produto_has_Produto_Produto2` (`ProdutoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CupomAvancadoGrupo`
--

DROP TABLE IF EXISTS `CupomAvancadoGrupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CupomAvancadoGrupo` (
  `GrupoId` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `PromocaoId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`GrupoId`),
  UNIQUE KEY `GrupoId` (`GrupoId`) USING BTREE,
  KEY `PromocaoId` (`PromocaoId`)
) ENGINE=InnoDB AUTO_INCREMENT=423363 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CupomAvancadoGrupoAcao`
--

DROP TABLE IF EXISTS `CupomAvancadoGrupoAcao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CupomAvancadoGrupoAcao` (
  `AcaoId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `GrupoId` mediumint(8) unsigned NOT NULL,
  `TipoAcaoId` tinyint(3) unsigned NOT NULL,
  `Acao` char(20) NOT NULL,
  PRIMARY KEY (`AcaoId`),
  UNIQUE KEY `AcaoId` (`AcaoId`),
  KEY `GrupoId` (`GrupoId`)
) ENGINE=InnoDB AUTO_INCREMENT=484582 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CupomAvancadoGrupoCondicao`
--

DROP TABLE IF EXISTS `CupomAvancadoGrupoCondicao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CupomAvancadoGrupoCondicao` (
  `CondicaoId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `GrupoId` mediumint(8) unsigned NOT NULL,
  `TipoCondicaoId` tinyint(3) unsigned NOT NULL,
  `Condicao` char(20) NOT NULL,
  `Excecao` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`CondicaoId`),
  UNIQUE KEY `CondicaoId` (`CondicaoId`),
  KEY `GrupoId` (`GrupoId`)
) ENGINE=InnoDB AUTO_INCREMENT=8257967 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CupomAvancadoTipoAcao`
--

DROP TABLE IF EXISTS `CupomAvancadoTipoAcao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CupomAvancadoTipoAcao` (
  `TipoAcaoId` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Descricao` char(30) NOT NULL,
  PRIMARY KEY (`TipoAcaoId`),
  UNIQUE KEY `TipoAcaoId` (`TipoAcaoId`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CupomAvancadoTipoCondicao`
--

DROP TABLE IF EXISTS `CupomAvancadoTipoCondicao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CupomAvancadoTipoCondicao` (
  `TipoCondicaoId` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Descricao` char(20) NOT NULL,
  `Prioridade` tinyint(3) unsigned NOT NULL COMMENT 'Quanto maior o número menor a prioridade',
  `Consulta` tinyint(3) unsigned NOT NULL COMMENT '1 - Produtos, 2 - Carrinho e 3 - Pagamento',
  PRIMARY KEY (`TipoCondicaoId`),
  UNIQUE KEY `TipoCondicaoId` (`TipoCondicaoId`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CupomCopaConfederacoes`
--

DROP TABLE IF EXISTS `CupomCopaConfederacoes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CupomCopaConfederacoes` (
  `PedidoId` int(11) unsigned NOT NULL,
  `ClienteId` int(11) NOT NULL,
  `Cupom` text NOT NULL,
  PRIMARY KEY (`PedidoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `CupomDesconto`
--

DROP TABLE IF EXISTS `CupomDesconto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CupomDesconto` (
  `CupomDescontoId` int(11) NOT NULL AUTO_INCREMENT,
  `UsuarioId` int(11) NOT NULL DEFAULT '0',
  `Tipo` tinyint(3) unsigned DEFAULT '1' COMMENT '1 - Cupom de Desconto (cdp) e 2 - Cupom de Desconto Avançado(cda)',
  `PromocaoId` int(11) NOT NULL DEFAULT '0',
  `Observacao` varchar(255) DEFAULT NULL,
  `Codigo` varchar(45) NOT NULL DEFAULT '',
  `Status` tinyint(4) NOT NULL DEFAULT '0',
  `UtilizacaoMax` int(11) DEFAULT NULL,
  `UtilizacaoAtual` int(11) DEFAULT NULL,
  `SiteId` int(11) DEFAULT NULL,
  `DataAtualizacao` datetime DEFAULT NULL,
  `NomeCupom` varchar(16) DEFAULT NULL COMMENT 'Alias para o codigo do cupom',
  PRIMARY KEY (`CupomDescontoId`),
  UNIQUE KEY `CupomDesconto_PromoUtil2` (`PromocaoId`,`CupomDescontoId`,`UtilizacaoAtual`,`UtilizacaoMax`),
  KEY `fk_UsuarioId` (`UsuarioId`),
  KEY `CupomDesconto_Status` (`Status`),
  KEY `fk_CupomDesconto_Promocao` (`PromocaoId`),
  KEY `CupomDesconto_Codigo` (`Codigo`),
  KEY `CupomDescontoSiteId` (`SiteId`),
  KEY `Tipo` (`Tipo`),
  KEY `CupomDesconto_Nome` (`NomeCupom`)
) ENGINE=InnoDB AUTO_INCREMENT=92203 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`127.0.0.1`*/ /*!50003 TRIGGER `InserirCupomLog` AFTER INSERT ON `CupomDesconto`
FOR EACH ROW
 BEGIN
  INSERT INTO CupomDesconto_Log(CupomDescontoId,UsuarioId,Tipo,PromocaoId,Observacao,Codigo,Status,UtilizacaoMax,UtilizacaoAtual,SiteId,DataAtualizacao) VALUES(NEW.CupomDescontoId,NEW.UsuarioId,NEW.Tipo,NEW.PromocaoId,NEW.Observacao,NEW.Codigo,NEW.Status,NEW.UtilizacaoMax,NEW.UtilizacaoAtual,NEW.SiteId,CURRENT_TIMESTAMP);
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`127.0.0.1`*/ /*!50003 TRIGGER `AlterarCupomLog` AFTER UPDATE  ON CupomDesconto
FOR EACH ROW
  BEGIN
    IF NEW.UsuarioId != 0
    THEN
        INSERT INTO CupomDesconto_Log(CupomDescontoId,UsuarioId,Tipo,PromocaoId,Observacao,Codigo,STATUS,UtilizacaoMax,UtilizacaoAtual,SiteId,DataAtualizacao) VALUES(NEW.CupomDescontoId,NEW.UsuarioId,NEW.Tipo,NEW.PromocaoId,NEW.Observacao,NEW.Codigo,NEW.Status,NEW.UtilizacaoMax,NEW.UtilizacaoAtual,NEW.SiteId,CURRENT_TIMESTAMP);
    END IF;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `CupomDesconto_Log`
--

DROP TABLE IF EXISTS `CupomDesconto_Log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `CupomDesconto_Log` (
  `CupomDescontoLogId` int(11) NOT NULL AUTO_INCREMENT,
  `CupomDescontoId` int(11) NOT NULL,
  `UsuarioId` int(11) NOT NULL DEFAULT '0',
  `Tipo` tinyint(3) unsigned DEFAULT '1' COMMENT '1 - Cupom de Desconto (cdp) e 2 - Cupom de Desconto Avançado(cda)',
  `PromocaoId` int(11) NOT NULL DEFAULT '0',
  `Observacao` varchar(255) DEFAULT NULL,
  `Codigo` varchar(45) NOT NULL DEFAULT '',
  `Status` tinyint(4) NOT NULL DEFAULT '0',
  `UtilizacaoMax` int(11) DEFAULT NULL,
  `UtilizacaoAtual` int(11) DEFAULT NULL,
  `SiteId` int(11) DEFAULT NULL,
  `DataAtualizacao` datetime DEFAULT NULL,
  PRIMARY KEY (`CupomDescontoLogId`),
  KEY `CupomDescontoId` (`CupomDescontoId`),
  KEY `fk_UsuarioId` (`UsuarioId`),
  KEY `CupomDesconto_Status` (`Status`),
  KEY `fk_CupomDesconto_Promocao` (`PromocaoId`),
  KEY `CupomDesconto_Codigo` (`Codigo`),
  KEY `CupomDescontoSiteId` (`SiteId`),
  KEY `Tipo` (`Tipo`)
) ENGINE=InnoDB AUTO_INCREMENT=211244 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Dhtml`
--

DROP TABLE IF EXISTS `Dhtml`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Dhtml` (
  `DhtmlId` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `BannerId` int(10) unsigned NOT NULL,
  `PosHorizontal` tinyint(3) unsigned NOT NULL COMMENT '1 => Esquerda, 2 => Direita, 3 => Centro',
  `PosVertical` tinyint(3) unsigned NOT NULL COMMENT '1 => Acima, 2 => Abaixo, 3 => Centro',
  `Rolagem` tinyint(3) unsigned NOT NULL COMMENT '0 => false, 1 => true',
  `TempoEntrada` int(10) unsigned NOT NULL COMMENT 'Tempo em segundos que ser�o miltiplicados por 1000',
  `TempoSaida` int(10) unsigned NOT NULL COMMENT 'Tempo em segundos que ser�o miltiplicados por 1000',
  `Referencia` tinyint(3) unsigned NOT NULL COMMENT '1 => P�gina, 2 => Banner Principal',
  `Status` tinyint(3) unsigned DEFAULT '1',
  `SiteId` tinyint(4) unsigned NOT NULL,
  PRIMARY KEY (`DhtmlId`),
  UNIQUE KEY `DhtmlId` (`DhtmlId`),
  KEY `Status` (`Status`),
  KEY `SiteId` (`SiteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Documento`
--

DROP TABLE IF EXISTS `Documento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Documento` (
  `DocumentoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `NomeArquivo` varchar(100) NOT NULL,
  `ArquivoServidor` varchar(100) NOT NULL,
  `UsuarioCadastroId` int(11) unsigned NOT NULL,
  `UsuarioAlteracaoId` int(11) unsigned DEFAULT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT '0',
  `TamanhoArquivo` int(11) unsigned NOT NULL,
  `DataArquivo` datetime NOT NULL,
  `SiteId` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`DocumentoId`),
  KEY `DataArquivo_Index` (`DataArquivo`)
) ENGINE=InnoDB AUTO_INCREMENT=15 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EmbalagemPresente`
--

DROP TABLE IF EXISTS `EmbalagemPresente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EmbalagemPresente` (
  `EmbalagemPresenteId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(60) NOT NULL,
  `Preco` decimal(7,2) NOT NULL,
  `Status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SiteId` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`EmbalagemPresenteId`),
  UNIQUE KEY `EmbalagemPresenteId` (`EmbalagemPresenteId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EmbalagemPresente_Categoria`
--

DROP TABLE IF EXISTS `EmbalagemPresente_Categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EmbalagemPresente_Categoria` (
  `EmbalagemPresenteId` int(11) unsigned NOT NULL,
  `CategoriaId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`EmbalagemPresenteId`,`CategoriaId`),
  KEY `EmbalagemPresenteId` (`EmbalagemPresenteId`) USING BTREE,
  KEY `CategoriaId` (`CategoriaId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Endereco`
--

DROP TABLE IF EXISTS `Endereco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Endereco` (
  `EnderecoId` int(11) NOT NULL AUTO_INCREMENT,
  `ClienteId` int(11) DEFAULT NULL,
  `Identificacao` varchar(50) NOT NULL DEFAULT '',
  `Tipo` enum('Residencial','Comercial') NOT NULL DEFAULT 'Residencial',
  `Destinatario` varchar(100) NOT NULL DEFAULT '',
  `Cep` varchar(9) NOT NULL DEFAULT '',
  `Endereco` varchar(100) NOT NULL DEFAULT '',
  `Numero` int(11) DEFAULT NULL,
  `Complemento` varchar(45) DEFAULT NULL,
  `Bairro` varchar(50) NOT NULL DEFAULT '',
  `Cidade` varchar(50) NOT NULL DEFAULT '',
  `Estado` char(2) NOT NULL DEFAULT '',
  `Telefone1` varchar(13) NOT NULL DEFAULT '',
  `Telefone2` varchar(13) DEFAULT NULL,
  `Celular` varchar(13) DEFAULT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT '1',
  `Referencia` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`EnderecoId`),
  KEY `fk_Endereco_Cliente` (`ClienteId`),
  KEY `Status` (`Status`),
  KEY `Cliente` (`ClienteId`)
) ENGINE=InnoDB AUTO_INCREMENT=26322129 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Entrega`
--

DROP TABLE IF EXISTS `Entrega`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Entrega` (
  `EntregaId` int(11) NOT NULL AUTO_INCREMENT,
  `NumeroEntrega` int(14) unsigned DEFAULT NULL,
  `PedidoId` int(11) DEFAULT NULL,
  `ProdutoId` int(11) DEFAULT NULL,
  `NomeTransportadora` varchar(45) DEFAULT NULL,
  `DataRecolhimento` datetime DEFAULT NULL,
  `DataPrevisao` datetime DEFAULT NULL,
  `DataEntrega` datetime DEFAULT NULL,
  `Rastreamento` varchar(300) DEFAULT NULL COMMENT 'Urls de rastreamento das entregas separado por vírgula',
  `Observacao` text,
  PRIMARY KEY (`EntregaId`),
  KEY `fk_Entrega_Pedido_Produto` (`PedidoId`,`ProdutoId`),
  KEY `NumeroEntrega` (`NumeroEntrega`),
  KEY `ProdutoId` (`ProdutoId`)
) ENGINE=InnoDB AUTO_INCREMENT=31302579 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Entrega_NotaFiscal`
--

DROP TABLE IF EXISTS `Entrega_NotaFiscal`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Entrega_NotaFiscal` (
  `PedidoId` int(11) unsigned NOT NULL,
  `EntregaId` int(11) unsigned NOT NULL,
  `ChaveNfe` varchar(44) NOT NULL,
  PRIMARY KEY (`PedidoId`,`EntregaId`,`ChaveNfe`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Entrega_PedidoStatus`
--

DROP TABLE IF EXISTS `Entrega_PedidoStatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Entrega_PedidoStatus` (
  `PedidoId` int(11) NOT NULL DEFAULT '0',
  `NumeroEntrega` int(14) unsigned NOT NULL DEFAULT '0',
  `PedidoStatusId` int(11) NOT NULL DEFAULT '0',
  `Data` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`PedidoId`,`NumeroEntrega`,`PedidoStatusId`),
  KEY `fk_Entrega_PedidoStatus_Pedido` (`PedidoId`),
  KEY `fk_Entrega_PedidoStatus_PedidoStatus` (`PedidoStatusId`),
  KEY `fk_Entrega_PedidoStatus_Data` (`Data`),
  KEY `fk_Entrega_PedidoStatus_NumeroEntrega` (`NumeroEntrega`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Envelopamento`
--

DROP TABLE IF EXISTS `Envelopamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Envelopamento` (
  `EnvelopamentoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SiteId` int(11) unsigned NOT NULL,
  `Nome` varchar(50) DEFAULT NULL,
  `Status` tinyint(1) unsigned DEFAULT '1',
  `Principal` tinyint(1) unsigned DEFAULT '0',
  `ImagemTopo` varchar(50) DEFAULT NULL,
  `ImagemLateral` varchar(50) DEFAULT NULL,
  `CorFundo` varchar(20) DEFAULT NULL,
  `TipoBackground` tinyint(2) unsigned DEFAULT NULL COMMENT '0 - background fixo, 1 - background movel',
  `Url` varchar(350) DEFAULT NULL,
  `DataInicio` datetime DEFAULT NULL,
  `DataFim` datetime DEFAULT NULL,
  PRIMARY KEY (`EnvelopamentoId`),
  KEY `EvelopamentoSiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=607 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Envelopamento_Nivel`
--

DROP TABLE IF EXISTS `Envelopamento_Nivel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Envelopamento_Nivel` (
  `EnvelopamentoNivelId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `EnvelopamentoId` int(11) unsigned NOT NULL,
  `TipoNivel` tinyint(4) unsigned NOT NULL COMMENT '1 - Lista Especial, 2 - Categoria',
  `Nivel` int(11) DEFAULT NULL,
  PRIMARY KEY (`EnvelopamentoNivelId`)
) ENGINE=InnoDB AUTO_INCREMENT=7966 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Estabelecimento`
--

DROP TABLE IF EXISTS `Estabelecimento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Estabelecimento` (
  `EstabelecimentoId` int(11) NOT NULL,
  `Nome` varchar(45) DEFAULT NULL,
  `Status` tinyint(1) NOT NULL,
  `Cnpj` varchar(18) DEFAULT NULL,
  `Prioridade` tinyint(3) unsigned DEFAULT NULL,
  `PrazoCdLeve` tinyint(1) DEFAULT '0',
  `PrazoCdPesado` tinyint(1) DEFAULT '0',
  `PrazoCdLevePos` tinyint(1) DEFAULT '0',
  `PrazoCdPesadoPos` tinyint(1) DEFAULT '0',
  `HoraCorte` time DEFAULT '18:00:00',
  PRIMARY KEY (`EstabelecimentoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Estoque`
--

DROP TABLE IF EXISTS `Estoque`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Estoque` (
  `ProdutoId` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `Total` int(11) DEFAULT '0',
  `Reservado` int(11) DEFAULT '0',
  `Disponivel` int(11) NOT NULL DEFAULT '0',
  `PreVenda` int(11) DEFAULT '0',
  `Virtual` int(11) DEFAULT '0',
  `DataUltimaImportacao` datetime DEFAULT NULL,
  `PrazoDisponibilidade` int(11) DEFAULT '0',
  `Tipo` tinyint(4) DEFAULT '1',
  `Fisico` int(11) DEFAULT NULL,
  `Jit` mediumint(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ProdutoId`),
  KEY `fk_Estoque_Produto` (`ProdutoId`),
  KEY `Estoque_DataUltimaImportacao` (`DataUltimaImportacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Estoque_Estabelecimento`
--

DROP TABLE IF EXISTS `Estoque_Estabelecimento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Estoque_Estabelecimento` (
  `ProdutoId` int(11) NOT NULL,
  `EstabelecimentoId` int(11) NOT NULL,
  `Total` int(11) DEFAULT '0',
  `Reservado` int(11) DEFAULT '0',
  `Disponivel` int(11) NOT NULL DEFAULT '0',
  `PreVenda` int(11) DEFAULT '0',
  `Virtual` int(11) DEFAULT '0',
  `DataUltimaImportacao` datetime DEFAULT NULL,
  `PrazoDisponibilidade` int(11) DEFAULT '0',
  `Tipo` tinyint(4) DEFAULT '1',
  `Fisico` int(11) DEFAULT NULL,
  `Jit` mediumint(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ProdutoId`,`EstabelecimentoId`),
  KEY `Estoque_DataUltimaImportacao` (`DataUltimaImportacao`),
  KEY `fk_Estoque_Estabelecimento_Estabelecimento1` (`EstabelecimentoId`),
  KEY `ProdutoId` (`ProdutoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `InserirEstoque` AFTER INSERT ON `Estoque_Estabelecimento`
    FOR EACH ROW BEGIN

SELECT  ProdutoId            AS ProdutoId,
SUM(Total)           AS Total,
SUM(Reservado)       AS Reservado,
SUM(Disponivel)      AS Disponivel,
SUM(PreVenda)        AS PreVenda,
SUM(Virtual)         AS Virtual,
SUM(Fisico)          AS Fisico,
SUM(Jit)     AS Jit,
DataUltimaImportacao AS DataUltimaImportacao,
MAX(PrazoDisponibilidade) AS PrazoDisponibilidade,
Tipo                 AS Tipo
INTO @ProdutoId,
     @Total,
     @Reservado,
     @Disponivel,
     @PreVenda,
     @Virtual,
     @Fisico,
     @Jit,
     @DataUltimaImportacao,
     @PrazoDisponibilidade,
     @Tipo
FROM Estoque_Estabelecimento
WHERE ProdutoId = NEW.ProdutoId
GROUP BY ProdutoId;


INSERT INTO Estoque (
ProdutoId,
Total,
Reservado,
Disponivel,
PreVenda,
Virtual,
Fisico,
Jit,
DataUltimaImportacao,
PrazoDisponibilidade,
Tipo
)
VALUES (
@ProdutoId,
@Total,
@Reservado,
@Disponivel,
@PreVenda,
@Virtual,
@Fisico,
@Jit,
@DataUltimaImportacao,
@PrazoDisponibilidade,
@Tipo
       )
ON DUPLICATE KEY UPDATE
    ProdutoId            = @ProdutoId,
    Total                = @Total,
    Reservado            = @Reservado,
    Disponivel           = @Disponivel,
    PreVenda             = @PreVenda,
    Virtual              = @Virtual,
    Fisico               = @Fisico,
    Jit                 = @Jit,
    DataUltimaImportacao = @DataUltimaImportacao,
    PrazoDisponibilidade = @PrazoDisponibilidade,
    Tipo                 = @Tipo;
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `AtualizarEstoque` AFTER UPDATE ON `Estoque_Estabelecimento`
    FOR EACH ROW BEGIN

SELECT  ProdutoId            AS ProdutoId,
SUM(Total)           AS Total,
SUM(Reservado)       AS Reservado,
SUM(Disponivel)      AS Disponivel,
SUM(PreVenda)        AS PreVenda,
SUM(Virtual)         AS Virtual,
SUM(Fisico)          AS Fisico,
SUM(Jit)     AS Jit,
DataUltimaImportacao AS DataUltimaImportacao,
MAX(PrazoDisponibilidade) AS PrazoDisponibilidade,
Tipo                 AS Tipo
INTO @ProdutoId,
     @Total,
     @Reservado,
     @Disponivel,
     @PreVenda,
     @Virtual,
     @Fisico,
     @Jit,
     @DataUltimaImportacao,
     @PrazoDisponibilidade,
     @Tipo
FROM Estoque_Estabelecimento
WHERE ProdutoId = NEW.ProdutoId
GROUP BY ProdutoId;

UPDATE Estoque
SET
    Total                = @Total,
    Reservado            = @Reservado,
    Disponivel           = @Disponivel,
    PreVenda             = @PreVenda,
    Virtual              = @Virtual,
    Fisico               = @Fisico,
    Jit                 = @Jit,
    DataUltimaImportacao = @DataUltimaImportacao,
    PrazoDisponibilidade = @PrazoDisponibilidade,
    Tipo                 = @Tipo

WHERE
    ProdutoId            = @ProdutoId;
 END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `Estoque_Log`
--

DROP TABLE IF EXISTS `Estoque_Log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Estoque_Log` (
  `EstoqueLogId` int(11) NOT NULL AUTO_INCREMENT,
  `ProdutoId` int(11) unsigned NOT NULL DEFAULT '0',
  `Virtual` int(11) DEFAULT '0',
  `PrazoDisponibilidade` int(11) DEFAULT '0',
  `DataAlteracao` datetime NOT NULL,
  `UsuarioAlteracao` int(11) NOT NULL,
  PRIMARY KEY (`EstoqueLogId`),
  KEY `EstoqueLog_Index1` (`ProdutoId`)
) ENGINE=InnoDB AUTO_INCREMENT=25737 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `EstoquebKP`
--

DROP TABLE IF EXISTS `EstoquebKP`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `EstoquebKP` (
  `ProdutoId` mediumint(8) unsigned NOT NULL DEFAULT '0',
  `Total` int(11) DEFAULT '0',
  `Reservado` int(11) DEFAULT '0',
  `Disponivel` int(11) NOT NULL DEFAULT '0',
  `PreVenda` int(11) DEFAULT '0',
  `Virtual` int(11) DEFAULT '0',
  `DataUltimaImportacao` datetime DEFAULT NULL,
  `PrazoDisponibilidade` int(11) DEFAULT '0',
  `Tipo` tinyint(4) DEFAULT '1',
  `Fisico` int(11) DEFAULT NULL,
  `Jit` mediumint(8) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ProdutoId`),
  KEY `fk_Estoque_Produto` (`ProdutoId`),
  KEY `Estoque_DataUltimaImportacao` (`DataUltimaImportacao`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ExportacaoBaseEmail`
--

DROP TABLE IF EXISTS `ExportacaoBaseEmail`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ExportacaoBaseEmail` (
  `UsuarioId` int(11) NOT NULL,
  `DataExportacao` datetime NOT NULL,
  `SiteId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`UsuarioId`,`SiteId`),
  KEY `ExportacaoBaseEmailSiteId` (`SiteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Fabricante`
--

DROP TABLE IF EXISTS `Fabricante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Fabricante` (
  `FabricanteId` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(100) NOT NULL DEFAULT '',
  `LogoSrc` varchar(100) DEFAULT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT '0',
  `Telefone` char(14) DEFAULT NULL,
  `Site` varchar(60) DEFAULT NULL,
  `Informacoes` text,
  `Banner` char(100) DEFAULT NULL,
  PRIMARY KEY (`FabricanteId`),
  KEY `Fabricante_Status` (`Status`)
) ENGINE=InnoDB AUTO_INCREMENT=4119 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FabricanteBanner`
--

DROP TABLE IF EXISTS `FabricanteBanner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FabricanteBanner` (
  `FabricanteBannerId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FabricanteId` int(11) unsigned NOT NULL,
  `SiteId` int(11) unsigned NOT NULL,
  `Src` varchar(30) NOT NULL,
  `Status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`FabricanteBannerId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FabricanteBanner_Categoria`
--

DROP TABLE IF EXISTS `FabricanteBanner_Categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FabricanteBanner_Categoria` (
  `FabricanteBannerId` int(11) unsigned NOT NULL,
  `CategoriaId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`FabricanteBannerId`,`CategoriaId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FabricanteImpressora`
--

DROP TABLE IF EXISTS `FabricanteImpressora`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FabricanteImpressora` (
  `FabricanteImpressoraId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` char(50) DEFAULT NULL,
  `Status` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`FabricanteImpressoraId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FabricanteSac`
--

DROP TABLE IF EXISTS `FabricanteSac`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FabricanteSac` (
  `FabricanteSacId` int(11) NOT NULL AUTO_INCREMENT,
  `ProdutoId` int(11) DEFAULT NULL,
  `Sac` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`FabricanteSacId`),
  KEY `fk_FabricanteSac_Produto` (`ProdutoId`)
) ENGINE=InnoDB AUTO_INCREMENT=180908 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FatorCartao`
--

DROP TABLE IF EXISTS `FatorCartao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FatorCartao` (
  `FatorCartaoId` int(11) NOT NULL AUTO_INCREMENT,
  `BandeiraId` int(11) DEFAULT NULL,
  `Parcela` tinyint(4) NOT NULL DEFAULT '0',
  `Fator` float NOT NULL DEFAULT '0',
  PRIMARY KEY (`FatorCartaoId`),
  UNIQUE KEY `BandeiraId` (`BandeiraId`,`Parcela`),
  KEY `fk_FatorCartao_Bandeira` (`BandeiraId`)
) ENGINE=InnoDB AUTO_INCREMENT=97 DEFAULT CHARSET=latin1 COMMENT='Para parcelas de 1 a 12 normais nÃ£o tera BandeiraId\nFator';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FatorCartao_Taxa`
--

DROP TABLE IF EXISTS `FatorCartao_Taxa`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FatorCartao_Taxa` (
  `FatorCartaoTaxaId` int(11) NOT NULL AUTO_INCREMENT,
  `Taxa` char(5) NOT NULL,
  `Mostrar` tinyint(4) NOT NULL,
  PRIMARY KEY (`FatorCartaoTaxaId`),
  UNIQUE KEY `FatorCartaoTaxaId_UNIQUE` (`FatorCartaoTaxaId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FatorReversao`
--

DROP TABLE IF EXISTS `FatorReversao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FatorReversao` (
  `FatorReversaoId` tinyint(4) NOT NULL AUTO_INCREMENT,
  `Parcela` tinyint(4) NOT NULL,
  `Fator` decimal(10,4) DEFAULT NULL,
  PRIMARY KEY (`FatorReversaoId`),
  KEY `Parcela` (`Parcela`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Feirao`
--

DROP TABLE IF EXISTS `Feirao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Feirao` (
  `FeiraoPedidoId` int(11) unsigned NOT NULL COMMENT 'PedidoId',
  `UsuarioCaixaId` int(11) unsigned NOT NULL COMMENT 'Usuario SGW ID',
  PRIMARY KEY (`FeiraoPedidoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FolhaPagamento`
--

DROP TABLE IF EXISTS `FolhaPagamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FolhaPagamento` (
  `FolhaPagamentoId` mediumint(4) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` char(20) NOT NULL,
  `Status` tinyint(3) unsigned NOT NULL DEFAULT '1',
  `PeriodoInicio` tinyint(2) unsigned DEFAULT NULL,
  `PeriodoFim` tinyint(2) unsigned DEFAULT NULL,
  PRIMARY KEY (`FolhaPagamentoId`),
  KEY `FP_FolhaPagamentoId` (`FolhaPagamentoId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FolhaPagamento_Evento`
--

DROP TABLE IF EXISTS `FolhaPagamento_Evento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FolhaPagamento_Evento` (
  `EventoId` mediumint(4) NOT NULL AUTO_INCREMENT,
  `FolhaPagamentoId` mediumint(4) unsigned NOT NULL,
  `TipoEvento` tinyint(2) DEFAULT NULL COMMENT '1 Produto, 2 Servico',
  `TipoPessoa` tinyint(1) unsigned DEFAULT NULL COMMENT '0 Funcionario, 1 Gerente',
  `SituacaoPessoa` tinyint(1) unsigned DEFAULT NULL COMMENT '1 Ativo, 2 Desligamento',
  `Codigo` int(6) unsigned DEFAULT NULL,
  PRIMARY KEY (`EventoId`)
) ENGINE=InnoDB AUTO_INCREMENT=51 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FolhaPagamento_Site`
--

DROP TABLE IF EXISTS `FolhaPagamento_Site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FolhaPagamento_Site` (
  `FolhaPagamentoId` mediumint(4) unsigned NOT NULL,
  `SiteId` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`FolhaPagamentoId`,`SiteId`),
  KEY `FPS_FolhaPagamentoId` (`FolhaPagamentoId`),
  KEY `FPS_SiteId` (`SiteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Frete`
--

DROP TABLE IF EXISTS `Frete`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Frete` (
  `FreteId` int(11) NOT NULL AUTO_INCREMENT,
  `TabelaIdUniconsult` int(11) DEFAULT NULL,
  `CepInicial` int(8) unsigned NOT NULL,
  `CepFinal` int(8) unsigned NOT NULL,
  `CNPJExpedidor` varchar(14) DEFAULT NULL,
  `TransportadoraId` bigint(15) DEFAULT NULL,
  `ContratoId` int(15) DEFAULT NULL,
  `ContratoNome` varchar(45) DEFAULT NULL,
  `PesoInicial` float DEFAULT NULL,
  `PesoFinal` float DEFAULT NULL,
  `VolumeMaximo` float DEFAULT NULL,
  `ValorInicial` float DEFAULT NULL,
  `ValorIncremento` float DEFAULT NULL,
  `AdValorem` float DEFAULT NULL,
  `DataInicioVigencia` date NOT NULL DEFAULT '0000-00-00',
  `DataFimVigencia` date DEFAULT NULL,
  `TempoTransito` int(11) NOT NULL DEFAULT '0',
  `PesoCubado` float DEFAULT NULL,
  `TipoEntrega` int(11) DEFAULT NULL,
  `TipoTransporte` int(11) DEFAULT NULL,
  `DataImportacao` date DEFAULT NULL,
  PRIMARY KEY (`FreteId`),
  KEY `Frete_DataVigencia` (`DataInicioVigencia`,`DataFimVigencia`),
  KEY `Frete_CEP` (`CepInicial`,`CepFinal`),
  KEY `Frete_Peso` (`PesoInicial`,`PesoFinal`),
  KEY `TabelaIdUniconsult` (`TabelaIdUniconsult`),
  KEY `DataImportacaoFrete` (`DataImportacao`)
) ENGINE=InnoDB AUTO_INCREMENT=1225837 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Frete2`
--

DROP TABLE IF EXISTS `Frete2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Frete2` (
  `FreteId` int(11) NOT NULL AUTO_INCREMENT,
  `TabelaIdUniconsult` int(11) DEFAULT NULL,
  `CepInicial` varchar(8) NOT NULL DEFAULT '',
  `CepFinal` varchar(8) NOT NULL DEFAULT '',
  `CNPJExpedidor` varchar(14) DEFAULT NULL,
  `TransportadoraId` bigint(15) DEFAULT NULL,
  `ContratoId` int(15) DEFAULT NULL,
  `ContratoNome` varchar(45) DEFAULT NULL,
  `PesoInicial` float DEFAULT NULL,
  `PesoFinal` float DEFAULT NULL,
  `VolumeMaximo` float DEFAULT NULL,
  `ValorInicial` float DEFAULT NULL,
  `ValorIncremento` float DEFAULT NULL,
  `AdValorem` float DEFAULT NULL,
  `DataInicioVigencia` date NOT NULL DEFAULT '0000-00-00',
  `DataFimVigencia` date DEFAULT NULL,
  `TempoTransito` int(11) NOT NULL DEFAULT '0',
  `PesoCubado` float DEFAULT NULL,
  `TipoEntrega` int(11) DEFAULT NULL,
  `TipoTransporte` int(11) DEFAULT NULL,
  `DataImportacao` date DEFAULT NULL,
  PRIMARY KEY (`FreteId`),
  KEY `Frete_DataVigencia` (`DataInicioVigencia`,`DataFimVigencia`),
  KEY `Frete_CEP` (`CepInicial`,`CepFinal`),
  KEY `Frete_Peso` (`PesoInicial`,`PesoFinal`),
  KEY `TabelaIdUniconsult` (`TabelaIdUniconsult`),
  KEY `DataImportacaoFrete` (`DataImportacao`)
) ENGINE=InnoDB AUTO_INCREMENT=1226235 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `FreteExcecao`
--

DROP TABLE IF EXISTS `FreteExcecao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `FreteExcecao` (
  `FreteExcecaoId` int(11) NOT NULL AUTO_INCREMENT,
  `ContratoId` int(11) NOT NULL DEFAULT '0',
  `SubCategoriaId` longtext,
  `UnidadeNegocioId` longtext,
  PRIMARY KEY (`FreteExcecaoId`)
) ENGINE=InnoDB AUTO_INCREMENT=74 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Fullzinho`
--

DROP TABLE IF EXISTS `Fullzinho`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Fullzinho` (
  `FullzinhoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` varchar(120) DEFAULT NULL,
  `Texto` text,
  `SiteId` int(11) DEFAULT NULL,
  `Principal` tinyint(1) unsigned DEFAULT NULL,
  `DataInicio` datetime DEFAULT NULL,
  `DataFim` datetime DEFAULT NULL,
  `Tipo` tinyint(1) unsigned DEFAULT '0' COMMENT '0 = Default, 1 = Imagem, 2 = HTML',
  `AlturaIframe` smallint(3) unsigned DEFAULT NULL COMMENT 'Apenas quando o tipo eh 2',
  `Link` varchar(256) DEFAULT NULL COMMENT 'Link quando o tipo eh 1',
  `CorFundo` char(7) DEFAULT NULL COMMENT 'Cor de fundo para os tipos 0 e 1',
  `Status` tinyint(1) NOT NULL,
  `CorTitulo` char(7) DEFAULT NULL COMMENT 'Cor do titulo para os tipos 0 e 1',
  `UrlImagem` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`FullzinhoId`),
  KEY `FullzinhoSiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=37 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GarantiaEstendidaOferta`
--

DROP TABLE IF EXISTS `GarantiaEstendidaOferta`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GarantiaEstendidaOferta` (
  `PedidoId` int(11) unsigned NOT NULL,
  `ProdutoId` int(11) unsigned NOT NULL,
  `ProdutoPrazoGarantia` tinyint(3) unsigned NOT NULL,
  `DataFaturamento` date NOT NULL,
  `ProdutoVencimentoGarantia` date NOT NULL,
  `DataOfertaGarantia` date NOT NULL,
  `StatusEnvio` tinyint(3) unsigned NOT NULL,
  UNIQUE KEY `PedidoProduto` (`PedidoId`,`ProdutoId`),
  KEY `OfertaGarantia` (`DataOfertaGarantia`,`StatusEnvio`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GarantiaPrazo`
--

DROP TABLE IF EXISTS `GarantiaPrazo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GarantiaPrazo` (
  `GarantiaPrazoId` int(11) NOT NULL AUTO_INCREMENT,
  `Prazo` tinyint(4) DEFAULT NULL,
  `Status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`GarantiaPrazoId`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GarantiaProduto`
--

DROP TABLE IF EXISTS `GarantiaProduto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GarantiaProduto` (
  `GarantiaProdutoId` int(11) NOT NULL AUTO_INCREMENT,
  `GarantiaPrazoId` int(11) NOT NULL,
  `ProdutoId` int(11) NOT NULL,
  `Status` tinyint(1) NOT NULL,
  `GarantiaCodigo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`GarantiaProdutoId`),
  UNIQUE KEY `GarantiaCodigo` (`GarantiaCodigo`),
  KEY `Produto_Garantia` (`ProdutoId`),
  KEY `Garantia_Prazo_Produto` (`GarantiaPrazoId`)
) ENGINE=InnoDB AUTO_INCREMENT=647435 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GarantiaTabela`
--

DROP TABLE IF EXISTS `GarantiaTabela`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GarantiaTabela` (
  `GarantiaTabelaId` int(11) NOT NULL AUTO_INCREMENT,
  `GarantiaPrazoId` int(11) NOT NULL,
  `CategoriaId` int(11) NOT NULL,
  `PrecoInicial` decimal(10,2) NOT NULL,
  `PrecoFinal` decimal(10,2) NOT NULL,
  `ValorCusto` decimal(10,2) NOT NULL,
  `ValorVenda` decimal(10,2) NOT NULL,
  PRIMARY KEY (`GarantiaTabelaId`),
  KEY `garantia_garantiPrazo` (`GarantiaPrazoId`),
  CONSTRAINT `garantia_garantiPrazo` FOREIGN KEY (`GarantiaPrazoId`) REFERENCES `GarantiaPrazo` (`GarantiaPrazoId`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=14607 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GrupoB2C`
--

DROP TABLE IF EXISTS `GrupoB2C`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GrupoB2C` (
  `GrupoB2CId` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` char(45) NOT NULL DEFAULT '',
  `Status` int(1) DEFAULT NULL,
  `Tipo` tinyint(2) unsigned DEFAULT '0' COMMENT '0 Funcionarios, 1 Terceiros',
  `ComissaoFolha` tinyint(1) unsigned DEFAULT NULL,
  `SiteId` int(11) unsigned NOT NULL,
  `FolhaPagamentoId` mediumint(4) unsigned DEFAULT NULL,
  `EmailImpressora` char(60) DEFAULT NULL,
  PRIMARY KEY (`GrupoB2CId`)
) ENGINE=InnoDB AUTO_INCREMENT=917 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `GrupoB2C_Site`
--

DROP TABLE IF EXISTS `GrupoB2C_Site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `GrupoB2C_Site` (
  `GrupoB2CId` int(11) NOT NULL DEFAULT '0',
  `SiteId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`GrupoB2CId`,`SiteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Hotsite`
--

DROP TABLE IF EXISTS `Hotsite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Hotsite` (
  `HotsiteId` int(11) NOT NULL AUTO_INCREMENT,
  `VitrineId` int(11) DEFAULT NULL,
  `Nome` varchar(30) DEFAULT NULL,
  `Css` varchar(30) DEFAULT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT '0',
  `Tipo` tinyint(1) DEFAULT NULL,
  `SiteId` int(11) DEFAULT NULL,
  `Altura` int(11) unsigned NOT NULL DEFAULT '0',
  `Validade` date DEFAULT NULL,
  `DataUltimaAlteracao` datetime DEFAULT NULL,
  PRIMARY KEY (`HotsiteId`),
  KEY `Hotsite_Status` (`Status`),
  KEY `fk_Hotsite_Vitrine` (`VitrineId`),
  KEY `HotsiteSiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=5317 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HotsiteNavegavel`
--

DROP TABLE IF EXISTS `HotsiteNavegavel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HotsiteNavegavel` (
  `HotsiteNavegavelId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SiteId` int(11) unsigned NOT NULL,
  `Nome` varchar(50) DEFAULT NULL,
  `Status` tinyint(1) unsigned DEFAULT '1',
  PRIMARY KEY (`HotsiteNavegavelId`),
  KEY `HotsiteNavegavel_SiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=309 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HotsiteNavegavel_Bloco`
--

DROP TABLE IF EXISTS `HotsiteNavegavel_Bloco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HotsiteNavegavel_Bloco` (
  `BlocoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `HotsiteNavegavelId` int(11) unsigned NOT NULL,
  `Bloco` tinyint(2) unsigned NOT NULL COMMENT '1 - Header, 2 Background Esquerdo, 3 Background Direito',
  `Imagem` varchar(50) DEFAULT NULL,
  `CorFundo` varchar(20) DEFAULT NULL,
  `Link` varchar(350) DEFAULT NULL,
  PRIMARY KEY (`BlocoId`),
  KEY `HotsiteNavegavelBloco` (`HotsiteNavegavelId`)
) ENGINE=InnoDB AUTO_INCREMENT=1014 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HotsiteNavegavel_Menu`
--

DROP TABLE IF EXISTS `HotsiteNavegavel_Menu`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HotsiteNavegavel_Menu` (
  `MenuId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `HotsiteNavegavelId` int(11) unsigned NOT NULL,
  `ListaEspecialId` int(11) unsigned NOT NULL,
  `Tipo` tinyint(1) unsigned DEFAULT NULL COMMENT '1 - texto, 2 - imagem',
  `Texto` text,
  `Imagem` varchar(50) DEFAULT NULL,
  `Ordem` tinyint(2) unsigned DEFAULT NULL,
  `Link` varchar(350) DEFAULT NULL,
  PRIMARY KEY (`MenuId`),
  KEY `MenuHotsiteNavegavel` (`HotsiteNavegavelId`),
  KEY `MenuListaEspecial` (`ListaEspecialId`)
) ENGINE=InnoDB AUTO_INCREMENT=5908 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `HotsiteNavegavel_Produto`
--

DROP TABLE IF EXISTS `HotsiteNavegavel_Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `HotsiteNavegavel_Produto` (
  `HotsiteNavegavelId` int(11) unsigned NOT NULL,
  `ProdutoId` int(11) unsigned NOT NULL,
  `Ordem` tinyint(4) unsigned DEFAULT NULL,
  `Principal` tinyint(1) unsigned DEFAULT '1',
  PRIMARY KEY (`HotsiteNavegavelId`,`ProdutoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Hotsite_Banner`
--

DROP TABLE IF EXISTS `Hotsite_Banner`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Hotsite_Banner` (
  `HotsiteId` int(11) NOT NULL DEFAULT '0',
  `BannerId` int(11) NOT NULL DEFAULT '0',
  `Status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`HotsiteId`,`BannerId`),
  KEY `fk_Hotsite_Banner_Hotsite` (`HotsiteId`),
  KEY `fk_Hotsite_Banner_Banner` (`BannerId`),
  KEY `Hotsite_Banner_Status` (`Status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `IpXmlParceiro`
--

DROP TABLE IF EXISTS `IpXmlParceiro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `IpXmlParceiro` (
  `IpXmlParceiroId` int(11) NOT NULL AUTO_INCREMENT,
  `XmlParceiroId` int(11) NOT NULL,
  `IP` char(15) DEFAULT NULL,
  PRIMARY KEY (`IpXmlParceiroId`),
  UNIQUE KEY `PK` (`IpXmlParceiroId`),
  KEY `Ip` (`IP`)
) ENGINE=InnoDB AUTO_INCREMENT=23142 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ItemIncluso`
--

DROP TABLE IF EXISTS `ItemIncluso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ItemIncluso` (
  `ItemInclusoId` int(11) NOT NULL AUTO_INCREMENT,
  `ProdutoId` int(11) NOT NULL DEFAULT '0',
  `Nome` varchar(100) NOT NULL DEFAULT '',
  `Quantidade` int(11) DEFAULT NULL,
  `Ordem` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ItemInclusoId`),
  KEY `fk_ItemIncluso_Produto` (`ProdutoId`)
) ENGINE=InnoDB AUTO_INCREMENT=2925563 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Layout_Config`
--

DROP TABLE IF EXISTS `Layout_Config`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Layout_Config` (
  `ConfigId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SiteId` int(11) unsigned NOT NULL,
  `Tipo` tinyint(1) DEFAULT NULL,
  `Config` text,
  PRIMARY KEY (`ConfigId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LinkPermitido`
--

DROP TABLE IF EXISTS `LinkPermitido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LinkPermitido` (
  `LinkPermitidoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Url` varchar(350) DEFAULT NULL,
  `Status` tinyint(1) unsigned DEFAULT NULL,
  `UsuarioAlteracao` int(11) unsigned DEFAULT NULL,
  `DataAlteracao` datetime DEFAULT NULL,
  PRIMARY KEY (`LinkPermitidoId`)
) ENGINE=InnoDB AUTO_INCREMENT=220 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LinkPermitido_Marketing`
--

DROP TABLE IF EXISTS `LinkPermitido_Marketing`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LinkPermitido_Marketing` (
  `LinkPermitidoMarketingId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SiteId` int(11) unsigned NOT NULL,
  `Url` varchar(350) DEFAULT NULL,
  `Status` tinyint(1) unsigned DEFAULT NULL,
  `UsuarioAlteracao` int(11) unsigned DEFAULT NULL,
  `DataAlteracao` datetime DEFAULT NULL,
  PRIMARY KEY (`LinkPermitidoMarketingId`)
) ENGINE=InnoDB AUTO_INCREMENT=1696 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ListaCasamento`
--

DROP TABLE IF EXISTS `ListaCasamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ListaCasamento` (
  `ListaCasamentoId` int(11) NOT NULL AUTO_INCREMENT,
  `ClienteId` int(11) NOT NULL,
  `Identificacao` char(100) DEFAULT NULL,
  `Url` char(255) DEFAULT NULL,
  `Titulo` char(100) DEFAULT NULL,
  `Mensagem` text,
  `Foto` char(100) DEFAULT NULL,
  `Origem` tinyint(4) DEFAULT NULL COMMENT '1 = Revistas\n2 = Eventos\n3 = Mídia\n4 = Twitter\n5 = Curso de noiva\n6 = Amigos\n7 = Loja Ricardo Eletro',
  `DataCerimonia` datetime DEFAULT NULL,
  `DataEntrega` date DEFAULT NULL,
  `DataRecepcao` datetime DEFAULT NULL,
  `Status` tinyint(4) DEFAULT NULL COMMENT '0 = Aguardando Liberação / 1 = Ativa / \n2 = Expirada ou  Cancelada',
  `LocalCerimonia` char(120) DEFAULT NULL,
  `LocalRecepcao` char(120) DEFAULT NULL,
  `EnderecoEntregaId` int(11) DEFAULT NULL,
  `DataCriacao` datetime DEFAULT NULL,
  `StatusExportacao` tinyint(4) DEFAULT '0',
  `SiteId` int(11) DEFAULT NULL,
  `ListaCasamentoVelha` tinyint(1) unsigned DEFAULT '0',
  `DataBonus` datetime DEFAULT NULL,
  `ValorBonus` float DEFAULT NULL,
  `PorcentagemBonus` float DEFAULT NULL,
  `OperadorB2CId` int(11) unsigned DEFAULT NULL COMMENT 'Grava o Id do Operador B2C que criou a lista',
  `NumConvite` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`ListaCasamentoId`),
  KEY `fk_ListaCasamento_Cliente1` (`ClienteId`),
  KEY `EntregaId` (`EnderecoEntregaId`),
  KEY `ListaCasamentoSiteId` (`SiteId`),
  KEY `idx_StatusExportacao` (`StatusExportacao`)
) ENGINE=InnoDB AUTO_INCREMENT=75184 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`riel_RicBDu`@`%`*/ /*!50003 TRIGGER `MaxListaId_ListaCasamento` BEFORE INSERT ON `ListaCasamento`
    FOR EACH ROW BEGIN
SET NEW.ListaCasamentoId = (SELECT GREATEST(MAX(C.CentralListaId), MAX(L.ListaCasamentoId)) AS ListaId FROM CentralLista C, ListaCasamento L) + 1;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `ListaCasamentoConvidado`
--

DROP TABLE IF EXISTS `ListaCasamentoConvidado`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ListaCasamentoConvidado` (
  `ListaCasamentoConvidadoId` int(11) NOT NULL AUTO_INCREMENT,
  `ListaCasamentoConviteId` int(11) NOT NULL,
  `Email` char(120) DEFAULT NULL,
  `Nome` char(100) DEFAULT NULL,
  `StatusEnvio` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`ListaCasamentoConvidadoId`),
  KEY `fk_ListaCasamentoConvidado_ListaCasamentoConvite1` (`ListaCasamentoConviteId`)
) ENGINE=InnoDB AUTO_INCREMENT=25562 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ListaCasamentoConvite`
--

DROP TABLE IF EXISTS `ListaCasamentoConvite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ListaCasamentoConvite` (
  `ListaCasamentoConviteId` int(11) NOT NULL AUTO_INCREMENT,
  `Titulo` char(120) DEFAULT NULL,
  `Mensagem` text,
  `ListaCasamentoId` int(11) NOT NULL,
  PRIMARY KEY (`ListaCasamentoConviteId`),
  KEY `fk_ListaCasamentoConvite_ListaCasamento1` (`ListaCasamentoId`)
) ENGINE=InnoDB AUTO_INCREMENT=8192 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ListaCasamentoMensagem`
--

DROP TABLE IF EXISTS `ListaCasamentoMensagem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ListaCasamentoMensagem` (
  `ListaCasamentoId` int(11) NOT NULL,
  `De` char(120) NOT NULL DEFAULT '',
  `Para` char(120) NOT NULL DEFAULT '',
  `Mensagem` text,
  `StatusEnvio` tinyint(4) DEFAULT NULL,
  `PedidoId` int(11) NOT NULL,
  `ProdutoId` int(11) NOT NULL,
  `SessionId` char(50) NOT NULL DEFAULT '',
  PRIMARY KEY (`ListaCasamentoId`,`De`,`Para`,`ProdutoId`,`SessionId`),
  KEY `fk_ListaCasamentoMensagem_Pedido_Produto1` (`PedidoId`,`ProdutoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ListaCasamento_LogSolicitacao`
--

DROP TABLE IF EXISTS `ListaCasamento_LogSolicitacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ListaCasamento_LogSolicitacao` (
  `ListaCasamento_LogSolicitacaoId` int(11) NOT NULL AUTO_INCREMENT,
  `ListaCasamento_PedidoId` int(11) NOT NULL,
  `UsuarioId` int(11) DEFAULT NULL,
  `TipoSolicitacaoDe` tinyint(1) NOT NULL,
  `TipoSolicitacaoPara` tinyint(1) NOT NULL,
  `Data` datetime NOT NULL,
  PRIMARY KEY (`ListaCasamento_LogSolicitacaoId`) USING BTREE,
  UNIQUE KEY `ListaCasamento_LogSolicitacaoId` (`ListaCasamento_LogSolicitacaoId`)
) ENGINE=InnoDB AUTO_INCREMENT=6151 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ListaCasamento_Origem`
--

DROP TABLE IF EXISTS `ListaCasamento_Origem`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ListaCasamento_Origem` (
  `OrigemId` tinyint(3) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` char(50) DEFAULT NULL,
  PRIMARY KEY (`OrigemId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ListaCasamento_Pedido`
--

DROP TABLE IF EXISTS `ListaCasamento_Pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ListaCasamento_Pedido` (
  `ListaCasamento_PedidoId` int(11) NOT NULL AUTO_INCREMENT,
  `ListaCasamentoId` int(11) NOT NULL DEFAULT '0',
  `PedidoId` int(11) NOT NULL,
  `ProdutoId` int(11) NOT NULL,
  `TipoSolicitacao` tinyint(1) DEFAULT '0' COMMENT '0 => Pendente, 1 => Entrega e 2 => Vale-troca',
  `DataSolicitacao` datetime DEFAULT NULL,
  `TipoPendencia` tinyint(1) unsigned DEFAULT NULL COMMENT '1 - Pendente de Entrega',
  PRIMARY KEY (`ListaCasamento_PedidoId`),
  UNIQUE KEY `ListaCasamento_PedidoId` (`ListaCasamento_PedidoId`),
  KEY `ListaCasamentoId` (`ListaCasamentoId`),
  KEY `PedidoId` (`PedidoId`),
  KEY `TipoSolicitacao` (`TipoSolicitacao`)
) ENGINE=InnoDB AUTO_INCREMENT=235693 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER `AutomatizaLC` AFTER UPDATE ON `ListaCasamento_Pedido`
FOR EACH ROW BEGIN

SELECT
LCP.ListaCasamentoId,
LCP.PedidoId,
LCP.TipoSolicitacao,
LCP.DataSolicitacao,
1 AS StatusExportacao,
LC.SiteId
INTO
@CentralListaId,
@PedidoId,
@TipoSolicitacao,
@DataSolicitacao,
@StatusExportacao,
@SiteId
FROM riel_producao.ListaCasamento_Pedido LCP
INNER JOIN riel_producao.ListaCasamento LC ON (LC.ListaCasamentoId = LCP.ListaCasamentoId)
WHERE LCP.PedidoId = NEW.PedidoId
GROUP BY LCP.PedidoId;

 INSERT INTO riel_producao.CentralLista_Solicitacao
 (
 CentralLista_SolicitacaoId,
CentralListaId,
PedidoId,
TipoSolicitacao,
DataSolicitacao,
StatusExportacao,
SiteId
)
VALUES (
NULL,
@CentralListaId,
@PedidoId,
@TipoSolicitacao,
@DataSolicitacao,
@StatusExportacao,
@SiteId
 );
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `ListaCasamento_Produto`
--

DROP TABLE IF EXISTS `ListaCasamento_Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ListaCasamento_Produto` (
  `ListaCasamentoId` int(11) NOT NULL,
  `ProdutoId` int(11) NOT NULL,
  `Quantidade` int(11) DEFAULT NULL,
  `Status` tinyint(4) DEFAULT NULL,
  `QuantidadeLiberada` tinyint(4) DEFAULT '0',
  `QuantidadeVale` tinyint(4) DEFAULT '0',
  `DataSolicitacao` datetime DEFAULT NULL,
  PRIMARY KEY (`ListaCasamentoId`,`ProdutoId`),
  KEY `fk_ListaCasamento_has_Produto_ListaCasamento1` (`ListaCasamentoId`),
  KEY `fk_ListaCasamento_has_Produto_Produto1` (`ProdutoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ListaDesejo`
--

DROP TABLE IF EXISTS `ListaDesejo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ListaDesejo` (
  `ListaDesejoId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ClienteId` int(10) unsigned NOT NULL,
  `Titulo` varchar(60) DEFAULT NULL,
  `Descricao` varchar(255) DEFAULT NULL,
  `DataCriacao` datetime NOT NULL,
  `DataEvento` datetime NOT NULL,
  `Status` tinyint(1) unsigned NOT NULL,
  `SiteId` int(11) DEFAULT NULL,
  PRIMARY KEY (`ListaDesejoId`),
  KEY `Cliente` (`ClienteId`,`Status`),
  KEY `SiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=249307 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ListaDesejo_Produto`
--

DROP TABLE IF EXISTS `ListaDesejo_Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ListaDesejo_Produto` (
  `ListaDesejoId` int(11) NOT NULL,
  `ProdutoId` int(10) unsigned NOT NULL,
  `DataAdicionado` datetime DEFAULT NULL,
  `Status` tinyint(1) unsigned NOT NULL,
  KEY `Produto` (`ProdutoId`,`Status`),
  KEY `fk_ListaDesejo_Produto_ListaDesejo` (`ListaDesejoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ListaEspecial`
--

DROP TABLE IF EXISTS `ListaEspecial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ListaEspecial` (
  `ListaEspecialId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` char(100) NOT NULL,
  `DataValidade` datetime NOT NULL,
  `Banner` char(45) DEFAULT NULL,
  `Randomico` tinyint(1) DEFAULT '0',
  `Status` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SiteId` tinyint(2) unsigned NOT NULL,
  PRIMARY KEY (`ListaEspecialId`),
  KEY `ListaEspecial_Status` (`Status`) USING BTREE,
  KEY `ListaEspecial_SiteId` (`SiteId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=65492 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ListaEspecial_Destaque`
--

DROP TABLE IF EXISTS `ListaEspecial_Destaque`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ListaEspecial_Destaque` (
  `ListaEspecialId` int(11) unsigned NOT NULL DEFAULT '0',
  `ProdutoId` int(11) unsigned NOT NULL DEFAULT '0',
  `Posicao` tinyint(1) unsigned NOT NULL,
  PRIMARY KEY (`ListaEspecialId`,`ProdutoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ListaEspecial_Produto`
--

DROP TABLE IF EXISTS `ListaEspecial_Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ListaEspecial_Produto` (
  `ListaEspecialId` int(11) unsigned NOT NULL DEFAULT '0',
  `ProdutoId` int(11) unsigned NOT NULL DEFAULT '0',
  `Posicao` int(11) unsigned NOT NULL,
  PRIMARY KEY (`ListaEspecialId`,`ProdutoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Localidade`
--

DROP TABLE IF EXISTS `Localidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Localidade` (
  `LocalidadeId` int(11) NOT NULL AUTO_INCREMENT,
  `CepInicio` int(8) DEFAULT NULL,
  `CepFim` int(8) DEFAULT NULL,
  `Local` varchar(60) DEFAULT NULL,
  `Sigla` char(3) DEFAULT NULL,
  `PrazoLeve` tinyint(4) unsigned DEFAULT NULL,
  `PrazoPesado` tinyint(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`LocalidadeId`),
  KEY `Sigla` (`Sigla`)
) ENGINE=InnoDB AUTO_INCREMENT=61 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `LojaFisica`
--

DROP TABLE IF EXISTS `LojaFisica`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `LojaFisica` (
  `LojaFisicaId` int(11) NOT NULL AUTO_INCREMENT,
  `Filial` varchar(5) DEFAULT NULL,
  `Local` varchar(80) DEFAULT NULL,
  `Rua` varchar(100) DEFAULT NULL,
  `Numero` int(11) DEFAULT NULL,
  `Complemento` varchar(45) DEFAULT NULL,
  `Bairro` varchar(50) DEFAULT NULL,
  `Cidade` varchar(50) DEFAULT NULL,
  `Estado` char(2) DEFAULT NULL,
  `Cep` varchar(12) DEFAULT NULL,
  `Telefone` varchar(14) DEFAULT NULL,
  `Status` tinyint(1) DEFAULT NULL,
  `SiteId` int(11) DEFAULT NULL,
  PRIMARY KEY (`LojaFisicaId`),
  KEY `LojaFisica_Estado` (`Estado`,`Status`),
  KEY `LojaFisica_Cidade` (`Cidade`,`Status`),
  KEY `LojaFisica_Bairro` (`Bairro`,`Status`),
  KEY `LojaFisicaSiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=29730 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MaisPedido`
--

DROP TABLE IF EXISTS `MaisPedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MaisPedido` (
  `ProdutoId` int(11) NOT NULL DEFAULT '0',
  `Total` int(11) DEFAULT '0',
  `SiteId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ProdutoId`,`SiteId`),
  KEY `ProdutoId` (`ProdutoId`),
  KEY `MaisPedidoSiteId` (`SiteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MargemLoja`
--

DROP TABLE IF EXISTS `MargemLoja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MargemLoja` (
  `MargemLojaId` int(11) NOT NULL AUTO_INCREMENT,
  `Mes` char(7) DEFAULT NULL,
  `Percentual` decimal(10,2) DEFAULT NULL,
  `CategoriaId` int(11) DEFAULT NULL,
  `SiteId` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`MargemLojaId`),
  UNIQUE KEY `Mes` (`Mes`,`CategoriaId`,`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=25065 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MeioPagamento`
--

DROP TABLE IF EXISTS `MeioPagamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MeioPagamento` (
  `MeioPagamentoId` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(45) NOT NULL DEFAULT '',
  `Status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`MeioPagamentoId`),
  KEY `MeioPagamento_Status` (`Status`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MetaCaixaDia`
--

DROP TABLE IF EXISTS `MetaCaixaDia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MetaCaixaDia` (
  `MetaCaixaDiaId` int(11) NOT NULL AUTO_INCREMENT,
  `Dia` date DEFAULT NULL,
  `Valor` decimal(10,2) DEFAULT NULL,
  `SiteId` tinyint(3) unsigned NOT NULL,
  `Percentual` decimal(10,2) DEFAULT NULL,
  `CategoriaId` int(11) DEFAULT NULL,
  PRIMARY KEY (`MetaCaixaDiaId`,`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=758947 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MetaCaixaDiaGarantiaEstendida`
--

DROP TABLE IF EXISTS `MetaCaixaDiaGarantiaEstendida`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MetaCaixaDiaGarantiaEstendida` (
  `MetaCaixaDiaGarantiaEstendidaId` int(11) NOT NULL AUTO_INCREMENT,
  `Dia` date DEFAULT NULL,
  `Valor` decimal(10,2) DEFAULT NULL,
  `Percentual` decimal(10,2) DEFAULT NULL,
  `SiteId` tinyint(3) NOT NULL DEFAULT '0',
  PRIMARY KEY (`MetaCaixaDiaGarantiaEstendidaId`),
  KEY `IndexDiaSite` (`Dia`,`SiteId`) USING BTREE,
  KEY `IndexDia` (`Dia`)
) ENGINE=InnoDB AUTO_INCREMENT=1859 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MetaCaixaHora`
--

DROP TABLE IF EXISTS `MetaCaixaHora`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MetaCaixaHora` (
  `MetaCaixaHoraId` int(11) NOT NULL AUTO_INCREMENT,
  `Mes` char(7) DEFAULT NULL,
  `DiaSemana` tinyint(4) DEFAULT NULL,
  `Hora` tinyint(4) DEFAULT NULL,
  `Percentual` decimal(10,2) DEFAULT NULL,
  `SiteId` int(11) NOT NULL DEFAULT '0',
  `CategoriaId` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`MetaCaixaHoraId`,`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=173042 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MetaCaixaLoja`
--

DROP TABLE IF EXISTS `MetaCaixaLoja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MetaCaixaLoja` (
  `MetaCaixaLojaId` int(11) NOT NULL AUTO_INCREMENT,
  `Mes` char(7) DEFAULT NULL,
  `Percentual` decimal(10,2) DEFAULT NULL,
  `CategoriaId` int(11) DEFAULT NULL,
  `SiteId` tinyint(3) unsigned NOT NULL,
  `Valor` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`MetaCaixaLojaId`),
  UNIQUE KEY `Mes` (`Mes`,`CategoriaId`,`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=24668 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MetaDia`
--

DROP TABLE IF EXISTS `MetaDia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MetaDia` (
  `MetaDiaId` int(11) NOT NULL AUTO_INCREMENT,
  `Dia` date DEFAULT NULL,
  `Valor` decimal(10,2) DEFAULT NULL,
  `SiteId` int(11) NOT NULL DEFAULT '0',
  `Percentual` decimal(10,2) unsigned DEFAULT NULL,
  `CategoriaId` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`MetaDiaId`,`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=33744 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MetaFrete`
--

DROP TABLE IF EXISTS `MetaFrete`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MetaFrete` (
  `MetaFreteId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CategoriaId` int(11) unsigned NOT NULL,
  `MetaMargem` decimal(10,2) DEFAULT NULL,
  `MetaFreteLiquido` decimal(10,2) DEFAULT NULL,
  `MargemFatRebCClube` decimal(10,2) DEFAULT NULL COMMENT 'Marg Fat M�s + Rec c/ Clube',
  `MargemFatRebSClube` decimal(10,2) DEFAULT NULL COMMENT 'Marg Fat M�s + Rec s/ Clube',
  `ArrecadacaoFatFrete` decimal(10,2) DEFAULT NULL COMMENT '% Arrecada��o Fat Frete M�s',
  `PorcentagemFreteLiquido` decimal(10,2) DEFAULT NULL COMMENT '% Frete Liq M�s',
  `MetaCapFrete` decimal(10,2) DEFAULT NULL,
  `FreteClienteArrecadacaoMes` decimal(10,2) DEFAULT NULL,
  `DividaFreteMes` decimal(10,2) DEFAULT NULL,
  `MargemMesReb` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`MetaFreteId`)
) ENGINE=InnoDB AUTO_INCREMENT=155 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MetaHora`
--

DROP TABLE IF EXISTS `MetaHora`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MetaHora` (
  `MetaHoraId` int(11) NOT NULL AUTO_INCREMENT,
  `Mes` char(7) DEFAULT NULL,
  `DiaSemana` tinyint(4) DEFAULT NULL,
  `Hora` tinyint(4) DEFAULT NULL,
  `Percentual` decimal(10,2) DEFAULT NULL,
  `SiteId` int(11) NOT NULL DEFAULT '0',
  `CategoriaId` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`MetaHoraId`,`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=197402 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MetaLoja`
--

DROP TABLE IF EXISTS `MetaLoja`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MetaLoja` (
  `MetaLojaId` int(11) NOT NULL AUTO_INCREMENT,
  `Mes` char(7) DEFAULT NULL,
  `Percentual` decimal(10,2) DEFAULT NULL,
  `LojaId` int(11) DEFAULT NULL,
  `SiteId` int(11) NOT NULL DEFAULT '0',
  `Valor` int(10) DEFAULT NULL,
  PRIMARY KEY (`MetaLojaId`,`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=25684 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MetaSite`
--

DROP TABLE IF EXISTS `MetaSite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MetaSite` (
  `MetaSiteId` int(11) NOT NULL AUTO_INCREMENT,
  `Mes` char(7) DEFAULT NULL,
  `SiteId` tinyint(3) unsigned NOT NULL,
  `Valor` decimal(10,2) DEFAULT NULL,
  PRIMARY KEY (`MetaSiteId`,`SiteId`),
  UNIQUE KEY `Mes` (`Mes`,`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `MidiasSociais`
--

DROP TABLE IF EXISTS `MidiasSociais`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `MidiasSociais` (
  `MidiaSocialId` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(50) DEFAULT NULL,
  `NomeId` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`MidiaSocialId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Modal_Condicao`
--

DROP TABLE IF EXISTS `Modal_Condicao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Modal_Condicao` (
  `ModalId` int(11) unsigned NOT NULL COMMENT 'Chave estrangeira para a tabela Modal_Site',
  `TipoCondicao` tinyint(1) unsigned NOT NULL COMMENT '1 - Campanha  2 - Categoria',
  `Condicao` int(11) NOT NULL COMMENT 'Valor da condicao(refere-se a Campanha ou a Categoria. -1 para todos *)',
  `Excecao` tinyint(1) unsigned DEFAULT '0' COMMENT 'Booleano para excecao de categorias'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Modal_Site`
--

DROP TABLE IF EXISTS `Modal_Site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Modal_Site` (
  `ModalId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Chave Primaria Autoincrement',
  `Nome` varchar(50) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `SiteId` int(11) NOT NULL DEFAULT '0' COMMENT 'Site a qual o Modal pertence',
  `Src` varchar(45) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL DEFAULT '' COMMENT 'Caminho da imagem de fundo do modal',
  `Status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 - Offline 1 - Online',
  PRIMARY KEY (`ModalId`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ModeloImpressora`
--

DROP TABLE IF EXISTS `ModeloImpressora`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ModeloImpressora` (
  `ModeloImpressoraId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `FabricanteImpressoraId` int(11) unsigned DEFAULT NULL,
  `TipoImpressoraId` int(11) unsigned DEFAULT NULL,
  `Nome` char(50) DEFAULT NULL,
  `Status` tinyint(1) unsigned DEFAULT NULL,
  PRIMARY KEY (`ModeloImpressoraId`)
) ENGINE=InnoDB AUTO_INCREMENT=1428 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `NavegacaoDestaque`
--

DROP TABLE IF EXISTS `NavegacaoDestaque`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NavegacaoDestaque` (
  `NavegacaoDestaqueId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CategoriaId` int(11) unsigned NOT NULL,
  `SiteId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`NavegacaoDestaqueId`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `NavegacaoDestaque_Itens`
--

DROP TABLE IF EXISTS `NavegacaoDestaque_Itens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `NavegacaoDestaque_Itens` (
  `ItemId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `NavegacaoDestaqueId` int(11) unsigned NOT NULL,
  `CategoriaId` int(11) unsigned NOT NULL,
  `Cor` char(8) DEFAULT NULL,
  `Ordem` tinyint(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`ItemId`)
) ENGINE=InnoDB AUTO_INCREMENT=107 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Noivos`
--

DROP TABLE IF EXISTS `Noivos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Noivos` (
  `NoivosId` int(11) NOT NULL AUTO_INCREMENT,
  `ListaCasamentoId` int(11) NOT NULL,
  `Nome` char(60) DEFAULT NULL,
  `Sobrenome` char(100) DEFAULT NULL,
  `Email` char(150) DEFAULT NULL,
  `NomeDoPai` char(160) DEFAULT NULL,
  `NomeDaMae` char(160) DEFAULT NULL,
  `Tipo` tinyint(4) DEFAULT NULL COMMENT '1 = Noivo\n2 = Noiva',
  `Voce` int(1) DEFAULT NULL,
  PRIMARY KEY (`NoivosId`),
  KEY `Email` (`Email`),
  KEY `fk_Noivos_ListaCasamento` (`ListaCasamentoId`)
) ENGINE=InnoDB AUTO_INCREMENT=159383 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `OperadorB2C`
--

DROP TABLE IF EXISTS `OperadorB2C`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OperadorB2C` (
  `OperadorB2CId` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` char(100) NOT NULL,
  `Email` char(60) NOT NULL,
  `Senha` char(32) NOT NULL,
  `CPF` char(14) NOT NULL,
  `GrupoB2CId` int(11) NOT NULL,
  `Status` int(1) NOT NULL DEFAULT '0',
  `Matricula` int(9) DEFAULT NULL,
  `Chapa` int(9) DEFAULT NULL,
  `Tipo` int(11) DEFAULT '0',
  `Sistema` tinyint(2) unsigned DEFAULT NULL COMMENT '1 - B2C, 2 - B2B',
  `InicioExpediente` time DEFAULT NULL,
  `FimExpediente` time DEFAULT NULL,
  PRIMARY KEY (`OperadorB2CId`),
  KEY `OperadorB2C_CPF` (`CPF`),
  KEY `fk_OperadorB2C_GrupoB2C` (`GrupoB2CId`),
  KEY `OperadorB2C_Matricula` (`Matricula`),
  KEY `OperadorB2C_Email` (`Email`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=19432 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `OralB_Base`
--

DROP TABLE IF EXISTS `OralB_Base`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OralB_Base` (
  `CRO` varchar(11) NOT NULL,
  `UF` char(2) NOT NULL,
  `Nome` varchar(80) NOT NULL,
  PRIMARY KEY (`CRO`,`UF`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `OralB_ContaCorrente`
--

DROP TABLE IF EXISTS `OralB_ContaCorrente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OralB_ContaCorrente` (
  `ContaCorrenteId` int(11) NOT NULL AUTO_INCREMENT,
  `DentistaId` int(11) NOT NULL,
  `SaldoBloqueado` decimal(8,2) NOT NULL DEFAULT '0.00',
  `SaldoLiberado` decimal(8,2) NOT NULL DEFAULT '0.00',
  `DataProcessamento` datetime NOT NULL,
  `Motivo` char(1) NOT NULL,
  `PedidoId` varchar(50) DEFAULT '',
  PRIMARY KEY (`ContaCorrenteId`)
) ENGINE=InnoDB AUTO_INCREMENT=9 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `OralB_CupomDesconto`
--

DROP TABLE IF EXISTS `OralB_CupomDesconto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OralB_CupomDesconto` (
  `CupomDescontoId` int(11) NOT NULL,
  `DentistaId` int(11) NOT NULL,
  PRIMARY KEY (`CupomDescontoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `OralB_Dentista`
--

DROP TABLE IF EXISTS `OralB_Dentista`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OralB_Dentista` (
  `DentistaId` int(11) NOT NULL AUTO_INCREMENT,
  `CPF` varchar(11) NOT NULL,
  `DataCadastro` date NOT NULL,
  `Nome` varchar(80) NOT NULL,
  `Email` varchar(100) NOT NULL,
  `Senha` varchar(32) NOT NULL,
  `SaldoAtual` decimal(8,2) DEFAULT '0.00',
  `CRO` varchar(11) NOT NULL,
  `UF` char(2) NOT NULL,
  PRIMARY KEY (`DentistaId`)
) ENGINE=InnoDB AUTO_INCREMENT=10060 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `OralB_Paciente`
--

DROP TABLE IF EXISTS `OralB_Paciente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OralB_Paciente` (
  `PacienteId` int(11) NOT NULL AUTO_INCREMENT,
  `DentistaId` int(11) NOT NULL,
  `Nome` varchar(80) NOT NULL,
  `Email` varchar(150) NOT NULL,
  `DataCadastro` date DEFAULT NULL,
  PRIMARY KEY (`PacienteId`)
) ENGINE=InnoDB AUTO_INCREMENT=36 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `OralB_Pedido_Produto`
--

DROP TABLE IF EXISTS `OralB_Pedido_Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `OralB_Pedido_Produto` (
  `Codigo` varchar(11) NOT NULL,
  `PedidoId` int(11) NOT NULL,
  `DentistaId` int(11) NOT NULL,
  `DataPedido` datetime NOT NULL,
  `Descricao` varchar(255) NOT NULL,
  `Loja` varchar(100) NOT NULL,
  `Categoria` varchar(150) NOT NULL,
  `SubCategoria` varchar(150) NOT NULL,
  `Fabricante` varchar(150) NOT NULL,
  `ValorUnitario` decimal(8,2) NOT NULL,
  `Quantidade` int(5) NOT NULL,
  `ValorTotal` decimal(8,2) NOT NULL,
  `CustoUltimaEntrada` decimal(8,2) NOT NULL,
  `ValorComissao` decimal(8,2) NOT NULL,
  `CPF` varchar(11) NOT NULL,
  `CRO` varchar(10) NOT NULL,
  `UF` char(2) NOT NULL,
  `Utilizado` smallint(5) unsigned DEFAULT '0',
  PRIMARY KEY (`Codigo`,`PedidoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pagamento`
--

DROP TABLE IF EXISTS `Pagamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pagamento` (
  `PagamentoId` int(11) NOT NULL AUTO_INCREMENT,
  `PedidoId` int(11) NOT NULL,
  `MeioPagamentoId` int(11) NOT NULL,
  `BandeiraId` int(11) DEFAULT NULL,
  `ValorPago` decimal(12,2) NOT NULL,
  `Parcelas` tinyint(4) NOT NULL,
  `PercentualJuros` float DEFAULT '0',
  `ValorJuros` float DEFAULT '0',
  `StatusAutorizacao` varchar(20) DEFAULT NULL,
  `DataAutorizacao` datetime DEFAULT NULL,
  `CartaoNumero` varchar(70) DEFAULT NULL,
  `CartaoValidade` char(8) DEFAULT NULL,
  `CartaoCodSeguranca` varchar(5) DEFAULT NULL,
  `CartaoNomePortador` varchar(30) DEFAULT NULL,
  `BoletoUrl` text,
  `DataVencimento` date DEFAULT NULL,
  `CodigoVale` varchar(70) DEFAULT NULL,
  `Sequencial` int(11) DEFAULT NULL,
  `Cartao_Numero` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`PagamentoId`),
  KEY `fk_Pagamento_Pedido` (`PedidoId`),
  KEY `fk_Pagamento_MeioPagamento` (`MeioPagamentoId`),
  KEY `fk_Pagamento_Bandeira` (`BandeiraId`),
  KEY `CartaoNumero` (`CartaoNumero`)
) ENGINE=InnoDB AUTO_INCREMENT=21460790 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pagamento_Historico`
--

DROP TABLE IF EXISTS `Pagamento_Historico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pagamento_Historico` (
  `PagamentoHistoricoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PagamentoId` int(11) unsigned NOT NULL,
  `PedidoId` int(11) unsigned NOT NULL,
  `MeioPagamentoId` int(11) unsigned NOT NULL,
  `BandeiraId` int(11) unsigned NOT NULL,
  `StatusAutorizacao` tinyint(4) DEFAULT NULL,
  `DataAutorizacao` datetime DEFAULT NULL,
  `CartaoNumero` varchar(70) DEFAULT NULL,
  `CartaoValidade` char(8) DEFAULT NULL,
  `CartaoCodSeguranca` varchar(5) DEFAULT NULL,
  `CartaoNomePortador` varchar(30) DEFAULT NULL,
  `DataTroca` datetime DEFAULT NULL,
  PRIMARY KEY (`PagamentoHistoricoId`)
) ENGINE=InnoDB AUTO_INCREMENT=2599 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pagamento_RegraTrocaCartao`
--

DROP TABLE IF EXISTS `Pagamento_RegraTrocaCartao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pagamento_RegraTrocaCartao` (
  `RegraTrocaCartaoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SiteId` int(11) unsigned NOT NULL,
  `PrazoLimiteTrocaPagamento` tinyint(4) NOT NULL COMMENT ' PRAZO EM HORAS',
  `PermitirTrocaBoleto` tinyint(1) NOT NULL COMMENT ' 0 => NAO PERMITIDO, 1 => PERMITIDO',
  `EnvioSms` tinyint(1) NOT NULL,
  PRIMARY KEY (`RegraTrocaCartaoId`)
) ENGINE=InnoDB AUTO_INCREMENT=22 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pagamento_RetornoTrocaCartao`
--

DROP TABLE IF EXISTS `Pagamento_RetornoTrocaCartao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pagamento_RetornoTrocaCartao` (
  `RetornoTrocaCartaoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Descricao` varchar(50) NOT NULL,
  `Codigo` tinyint(5) unsigned NOT NULL,
  PRIMARY KEY (`RetornoTrocaCartaoId`)
) ENGINE=InnoDB AUTO_INCREMENT=42 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pagamento_TrocaCartao`
--

DROP TABLE IF EXISTS `Pagamento_TrocaCartao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pagamento_TrocaCartao` (
  `PagamentoId` int(11) unsigned NOT NULL,
  `StatusAutorizacao` varchar(10) DEFAULT NULL,
  `DataAutorizacao` datetime DEFAULT NULL,
  `CodigoRetorno` varchar(10) DEFAULT NULL,
  `MensagemRetorno` varchar(255) DEFAULT NULL,
  `PrazoLimiteTroca` datetime DEFAULT NULL,
  PRIMARY KEY (`PagamentoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PaginaVisitada`
--

DROP TABLE IF EXISTS `PaginaVisitada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PaginaVisitada` (
  `PaginaVisitadaId` int(11) NOT NULL AUTO_INCREMENT,
  `SessionId` varchar(50) NOT NULL DEFAULT '0',
  `CategoriaId` int(11) DEFAULT NULL,
  `ClienteId` int(11) DEFAULT NULL,
  `Url` text NOT NULL,
  `Titulo` varchar(100) DEFAULT NULL,
  `Data` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`PaginaVisitadaId`),
  KEY `fk_PaginaVisitada_Session` (`SessionId`),
  KEY `fk_PaginaVisitada_Categoria` (`CategoriaId`),
  KEY `fk_PaginaVisitada_Cliente` (`ClienteId`),
  KEY `PaginaVisitada_Data` (`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Parceira`
--

DROP TABLE IF EXISTS `Parceira`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Parceira` (
  `ParceiraId` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(50) NOT NULL,
  `Status` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`ParceiraId`),
  UNIQUE KEY `ParceiraId` (`ParceiraId`),
  KEY `Status` (`Status`),
  KEY `Nome` (`Nome`)
) ENGINE=InnoDB AUTO_INCREMENT=984 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ParcelamentoIntervalos`
--

DROP TABLE IF EXISTS `ParcelamentoIntervalos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ParcelamentoIntervalos` (
  `ParcelamentoIntervalosId` tinyint(4) NOT NULL AUTO_INCREMENT,
  `Parcelamento` tinyint(4) DEFAULT NULL,
  `Inicio` float DEFAULT NULL,
  `Fim` float DEFAULT NULL,
  PRIMARY KEY (`ParcelamentoIntervalosId`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pedido`
--

DROP TABLE IF EXISTS `Pedido`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pedido` (
  `PedidoId` int(11) NOT NULL AUTO_INCREMENT,
  `ClienteId` int(11) NOT NULL DEFAULT '0',
  `EnderecoEntregaId` int(11) NOT NULL DEFAULT '0',
  `CupomDescontoId` int(11) DEFAULT NULL,
  `ValorTotal` decimal(12,2) unsigned NOT NULL,
  `ValorTotalFrete` decimal(9,2) unsigned DEFAULT NULL,
  `ValorCupomDesconto` decimal(9,2) unsigned DEFAULT NULL,
  `ValorCupomDescontoFrete` decimal(9,2) unsigned DEFAULT NULL,
  `IP` varchar(15) DEFAULT NULL,
  `Navegador` varchar(100) DEFAULT NULL,
  `SessionId` varchar(50) DEFAULT NULL,
  `StatusExportacao` tinyint(4) DEFAULT '0',
  `ValorTotalProdutos` decimal(12,2) unsigned DEFAULT NULL,
  `ValorDespesas` decimal(9,2) unsigned DEFAULT NULL,
  `ValorJuros` decimal(9,2) unsigned DEFAULT NULL,
  `ValorFreteSemPromocao` decimal(9,2) unsigned DEFAULT NULL,
  `GrupoB2CId` smallint(5) unsigned DEFAULT NULL,
  `OperadorB2CId` smallint(5) unsigned DEFAULT NULL,
  `ListaCasamentoId` int(11) unsigned DEFAULT NULL,
  `SiteId` tinyint(3) unsigned NOT NULL,
  `CentralListaId` int(11) unsigned DEFAULT NULL,
  `OperacaoPresente` tinyint(1) NOT NULL DEFAULT '0',
  `SistemaOperacional` varchar(12) DEFAULT NULL COMMENT 'Sistema Operacional do dispositivo que fez o pedido [Android|iOS|Mobile|Desktop]',
  PRIMARY KEY (`PedidoId`),
  KEY `fk_Pedido_Cliente` (`ClienteId`),
  KEY `fk_Pedido_Endereco` (`EnderecoEntregaId`),
  KEY `fk_Pedido_CupomDesconto` (`CupomDescontoId`),
  KEY `ListaCasamentoId` (`ListaCasamentoId`),
  KEY `PedidoSiteId` (`SiteId`),
  KEY `idx_OperadorB2CId` (`OperadorB2CId`),
  KEY `PedidoStatusExportacaoIndex` (`StatusExportacao`),
  KEY `idx_CentralListaId` (`CentralListaId`)
) ENGINE=InnoDB AUTO_INCREMENT=22902732 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PedidoB2BStatus`
--

DROP TABLE IF EXISTS `PedidoB2BStatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PedidoB2BStatus` (
  `B2BStatusId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`B2BStatusId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PedidoBI`
--

DROP TABLE IF EXISTS `PedidoBI`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PedidoBI` (
  `PedidoId` int(11) unsigned NOT NULL,
  `SiteId` tinyint(3) unsigned DEFAULT NULL,
  `NomeSite` varchar(25) DEFAULT NULL,
  `ClienteId` int(11) NOT NULL DEFAULT '0',
  `EnderecoEntregaId` int(11) NOT NULL DEFAULT '0',
  `CupomDescontoId` int(11) DEFAULT NULL,
  `CupomCodigo` varchar(20) DEFAULT NULL,
  `ValorTotal` decimal(9,2) NOT NULL,
  `ValorTotalFrete` float DEFAULT NULL,
  `ValorCupomDesconto` float DEFAULT NULL,
  `ValorCupomDescontoFrete` float DEFAULT NULL,
  `ValorTotalProdutos` decimal(9,2) DEFAULT NULL COMMENT 'Produtos + Garantia',
  `ValorDespesas` float DEFAULT NULL COMMENT 'Embalagem, etc',
  `ValorJuros` float DEFAULT NULL,
  `ValorFreteSemPromocao` float DEFAULT NULL,
  `ValorVale` float DEFAULT NULL,
  `ValorGarantiaEstendida` float DEFAULT NULL,
  `GrupoB2CId` int(11) DEFAULT NULL,
  `GrupoB2CNome` varchar(75) DEFAULT NULL,
  `OperadorB2CId` int(11) DEFAULT NULL,
  `OperadorB2CNome` char(100) DEFAULT NULL,
  `ListaCasamentoId` int(11) DEFAULT NULL,
  `CampanhaId` int(11) DEFAULT NULL,
  `CampanhaNome` varchar(35) DEFAULT NULL,
  `BandeiraId` mediumint(5) NOT NULL DEFAULT '0',
  `BandeiraNome` varchar(25) DEFAULT NULL,
  `MeioPagamentoId` mediumint(5) NOT NULL DEFAULT '0',
  `MeioPagamentoNome` varchar(25) DEFAULT NULL,
  `Parcelas` tinyint(4) NOT NULL,
  `PercentualJuros` float DEFAULT '0',
  `EntregaVip` tinyint(4) DEFAULT NULL,
  `DataPedido` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `DataPagamento` datetime DEFAULT NULL,
  `DataCancelamento` datetime DEFAULT NULL,
  `FatorReversao` float DEFAULT '0',
  `ParceiraId` smallint(5) unsigned DEFAULT NULL,
  `ParceiraNome` varchar(40) DEFAULT NULL,
  `SistemaOperacional` varchar(12) DEFAULT NULL COMMENT 'Sistema Operacional do dispositivo que fez o pedido [Android|iOS|Mobile|Desktop]',
  PRIMARY KEY (`PedidoId`),
  KEY `DataPedido` (`DataPedido`),
  KEY `DataPagamento` (`DataPagamento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PedidoStatus`
--

DROP TABLE IF EXISTS `PedidoStatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PedidoStatus` (
  `PedidoStatusId` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(45) NOT NULL DEFAULT '',
  PRIMARY KEY (`PedidoStatusId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pedido_CaminhaoSorte`
--

DROP TABLE IF EXISTS `Pedido_CaminhaoSorte`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pedido_CaminhaoSorte` (
  `PedidoCaminhaoSorteId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `PedidoId` int(11) unsigned NOT NULL,
  `Quantidade` int(3) unsigned NOT NULL,
  `ValorCusto` decimal(9,2) DEFAULT NULL,
  `ValorVenda` decimal(9,2) DEFAULT NULL,
  PRIMARY KEY (`PedidoCaminhaoSorteId`),
  UNIQUE KEY `Pedido_CaminhaoSorte_PedidoId` (`PedidoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pedido_CaminhaoSorte_Numero`
--

DROP TABLE IF EXISTS `Pedido_CaminhaoSorte_Numero`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pedido_CaminhaoSorte_Numero` (
  `PedidoCaminhaoSorteId` int(11) unsigned NOT NULL,
  `CaminhaoSorteNumeroId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`CaminhaoSorteNumeroId`,`PedidoCaminhaoSorteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pedido_Campanha`
--

DROP TABLE IF EXISTS `Pedido_Campanha`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pedido_Campanha` (
  `PedidoCampanhaId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PedidoId` int(10) unsigned NOT NULL,
  `CampanhaId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`PedidoCampanhaId`),
  UNIQUE KEY `PedidoCampanhaId` (`PedidoCampanhaId`),
  UNIQUE KEY `PedidoId` (`PedidoId`)
) ENGINE=InnoDB AUTO_INCREMENT=10592617 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pedido_EntregaAgendada`
--

DROP TABLE IF EXISTS `Pedido_EntregaAgendada`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pedido_EntregaAgendada` (
  `PedidoId` int(11) unsigned NOT NULL,
  `DataEntrega` date NOT NULL,
  `TurnoEntrega` tinyint(1) unsigned NOT NULL COMMENT '1 => Manha, 2 => Tarde, 3 => Noite',
  `Valor` float unsigned NOT NULL COMMENT 'Valor do frete da entrega agendada',
  PRIMARY KEY (`PedidoId`),
  KEY `EA_PedidoId` (`PedidoId`),
  KEY `EA_DataEntrega` (`DataEntrega`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pedido_Hora`
--

DROP TABLE IF EXISTS `Pedido_Hora`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pedido_Hora` (
  `DataPedido` date NOT NULL,
  `HoraPedido` tinyint(4) NOT NULL,
  `DiaSemana` varchar(15) NOT NULL,
  `Quantidade` smallint(6) NOT NULL,
  `Valor` int(11) NOT NULL DEFAULT '0',
  `SiteId` tinyint(3) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pedido_Masterpass`
--

DROP TABLE IF EXISTS `Pedido_Masterpass`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pedido_Masterpass` (
  `PedidoId` int(11) unsigned NOT NULL,
  `TransactionId` varchar(255) NOT NULL,
  `StatusEnvio` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`PedidoId`),
  UNIQUE KEY `Pedido_Masterpass_PedidoId` (`PedidoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pedido_Parceira`
--

DROP TABLE IF EXISTS `Pedido_Parceira`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pedido_Parceira` (
  `PedidoParceiraId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `PedidoId` int(10) unsigned NOT NULL,
  `ParceiraId` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`PedidoParceiraId`),
  UNIQUE KEY `PedidoParceiraId` (`PedidoParceiraId`)
) ENGINE=InnoDB AUTO_INCREMENT=6363 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pedido_PedidoB2BStatus`
--

DROP TABLE IF EXISTS `Pedido_PedidoB2BStatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pedido_PedidoB2BStatus` (
  `PedidoId` int(11) unsigned NOT NULL,
  `B2BStatusId` int(11) unsigned NOT NULL COMMENT '1 - Aguardando Análise, 2 - Aprovado, 7 - Reprovado',
  `Data` date NOT NULL DEFAULT '0000-00-00',
  `Observacao` varchar(100) DEFAULT NULL COMMENT 'Breve comentario',
  PRIMARY KEY (`B2BStatusId`,`PedidoId`),
  KEY `idxteste` (`PedidoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pedido_PedidoStatus`
--

DROP TABLE IF EXISTS `Pedido_PedidoStatus`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pedido_PedidoStatus` (
  `PedidoId` int(11) NOT NULL DEFAULT '0',
  `PedidoStatusId` int(11) NOT NULL DEFAULT '0',
  `Data` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Observacao` varchar(100) DEFAULT NULL COMMENT 'PrevisÃ£o de Entrega, Msg ao cliente no geral',
  PRIMARY KEY (`PedidoId`,`PedidoStatusId`),
  KEY `fk_Pedido_PedidoStatus_Pedido` (`PedidoId`),
  KEY `fk_Pedido_PedidoStatus_PedidoStatus` (`PedidoStatusId`),
  KEY `Pedido_PedidoStatus_Data` (`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pedido_Produto`
--

DROP TABLE IF EXISTS `Pedido_Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pedido_Produto` (
  `PedidoId` int(11) NOT NULL DEFAULT '0',
  `ProdutoId` int(11) NOT NULL DEFAULT '0',
  `EstabelecimentoId` int(11) DEFAULT NULL,
  `PedidoProdutoGarantiaId` int(11) DEFAULT '0',
  `PedidoProdutoSeguroId` int(11) unsigned DEFAULT '0',
  `ListaId` int(11) DEFAULT NULL,
  `ProdutoCodigo` varchar(45) DEFAULT NULL,
  `Quantidade` int(11) NOT NULL DEFAULT '0',
  `ValorUnitario` decimal(9,2) NOT NULL,
  `ValorTotal` decimal(12,2) NOT NULL,
  `ValorFrete` float DEFAULT NULL,
  `Tipo` char(1) DEFAULT NULL COMMENT 'Produto ou não produto',
  `ProdutoAgrupador` varchar(20) DEFAULT NULL COMMENT 'Para MetaSku, esse agrupador será o código do primeiro produto de um meta sku',
  `PrazoCD` int(11) DEFAULT NULL,
  `PrazoEntregaCombinado` int(11) DEFAULT NULL,
  `PrazoCrossdocking` int(11) DEFAULT NULL,
  `Sequencial` smallint(5) unsigned DEFAULT NULL,
  `MetaSKUId` int(11) DEFAULT '0',
  `ValorDesconto` float DEFAULT NULL,
  `ValorCustoEntrada` decimal(10,2) DEFAULT NULL,
  `EmbalagemPresenteId` int(11) unsigned DEFAULT NULL,
  `EmbalagemPresentePreco` decimal(7,2) DEFAULT NULL,
  `EmbalagemPresenteSeq` smallint(4) DEFAULT NULL,
  `EntregaVip` tinyint(3) unsigned DEFAULT '0',
  `OfertaGarantia` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `PromocaoId` int(11) unsigned DEFAULT '0' COMMENT 'Promocao aplicada ao produto sem cupom',
  `QuantidadeAvulso` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`PedidoId`,`ProdutoId`),
  KEY `fk_Pedido_Produto_Pedido` (`PedidoId`),
  KEY `fk_Pedido_Produto_Produto` (`ProdutoId`),
  KEY `fk_Lista` (`ListaId`),
  KEY `PedidoProdutoGarantiaId` (`PedidoProdutoGarantiaId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pedido_ProdutoBI`
--

DROP TABLE IF EXISTS `Pedido_ProdutoBI`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pedido_ProdutoBI` (
  `PedidoId` int(11) unsigned NOT NULL DEFAULT '0',
  `ProdutoId` int(11) unsigned NOT NULL DEFAULT '0',
  `PedidoProdutoGarantiaId` int(11) DEFAULT '0',
  `ListaId` int(11) DEFAULT NULL,
  `FabricanteId` int(11) NOT NULL DEFAULT '0',
  `ProdutoCodigo` varchar(45) DEFAULT NULL,
  `Quantidade` int(11) NOT NULL DEFAULT '0',
  `ValorUnitario` float NOT NULL DEFAULT '0',
  `ValorTotal` decimal(9,2) NOT NULL DEFAULT '0.00',
  `ValorFrete` float DEFAULT NULL COMMENT 'valor total',
  `ValorGarantia` float DEFAULT NULL COMMENT 'valor total ',
  `ValorJurosGarantia` float DEFAULT NULL COMMENT 'valor total',
  `ValorJuros` float DEFAULT NULL COMMENT 'valor total ',
  `ValorValeGarantia` float DEFAULT NULL,
  `ValorVale` float DEFAULT NULL COMMENT 'valor total ',
  `ValorDesconto` float DEFAULT NULL COMMENT 'valor unitario',
  `ValorCustoEntrada` decimal(10,2) DEFAULT NULL COMMENT 'valor unitario',
  `Margem` float DEFAULT '0',
  `MargemPa` float DEFAULT '0',
  `MargemRe` float DEFAULT '0',
  `MargemReCmvb` float DEFAULT '0',
  `MargemPaCmvb` float DEFAULT '0',
  `Custo` float DEFAULT '0',
  `Cmvb` float DEFAULT '0',
  `TipoProduto` tinyint(1) DEFAULT NULL COMMENT 'Produto simples ou combo',
  `ProdutoAgrupador` varchar(20) DEFAULT NULL COMMENT 'Para MetaSku, esse agrupador será o código do primeiro produto de um meta sku',
  `Sequencial` tinyint(4) DEFAULT NULL,
  `MetaSKUId` int(11) DEFAULT '0',
  `DataPedido` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `LojaId` mediumint(5) DEFAULT '0',
  `CategoriaId` mediumint(5) DEFAULT '0',
  `SubCategoriaId` mediumint(5) DEFAULT '0',
  `LojaNome` varchar(45) DEFAULT NULL,
  `CategoriaNome` varchar(45) DEFAULT NULL,
  `SubCategoriaNome` varchar(45) DEFAULT NULL,
  `PrazoGarantia` tinyint(4) unsigned DEFAULT NULL,
  `NomeProduto` varchar(255) DEFAULT NULL,
  `VendeAvulso` tinyint(1) unsigned DEFAULT NULL,
  `EmLinha` tinyint(3) unsigned DEFAULT NULL,
  `NomeFabricante` varchar(100) DEFAULT NULL,
  `DataPagamento` datetime DEFAULT NULL,
  `DataCancelamento` datetime DEFAULT NULL,
  `Ipi` decimal(10,2) DEFAULT NULL,
  `SubsTributaria` decimal(10,2) DEFAULT NULL,
  `PesoEmbalagem` float DEFAULT NULL,
  `SiteId` int(11) DEFAULT NULL,
  `ProdutoId_GarantiaPos` int(11) NOT NULL DEFAULT '0' COMMENT 'Produto Relacionado Pedido Antigo',
  `PedidoId_GarantiaPos` int(11) NOT NULL DEFAULT '0' COMMENT 'Pedido Antigo',
  `EstabelecimentoId` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`PedidoId`,`ProdutoId`),
  KEY `PedidoProdutoBI_Index` (`PedidoId`),
  KEY `PedidoProdutoBI_Data` (`DataPedido`),
  KEY `ProdutoMasterIndex` (`ProdutoId`,`DataPagamento`,`SiteId`),
  KEY `DataPagamento` (`DataPagamento`,`SiteId`),
  KEY `DataPagamento_2` (`DataPagamento`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pedido_Produto_Garantia`
--

DROP TABLE IF EXISTS `Pedido_Produto_Garantia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pedido_Produto_Garantia` (
  `PedidoProdutoGarantiaId` int(11) NOT NULL AUTO_INCREMENT,
  `GarantiaCodigo` varchar(45) NOT NULL,
  `ValorCusto` decimal(10,2) NOT NULL,
  `ValorVenda` decimal(10,2) NOT NULL,
  `Prazo` tinyint(4) NOT NULL,
  `Quantidade` tinyint(3) unsigned DEFAULT NULL,
  `ProdutoId_GarantiaPos` int(11) NOT NULL DEFAULT '0' COMMENT 'Produto Relacionado Pedido Antigo',
  `PedidoId_GarantiaPos` int(11) NOT NULL DEFAULT '0' COMMENT 'Pedido Antigo',
  PRIMARY KEY (`PedidoProdutoGarantiaId`),
  KEY `PedidoId_GarantiaPos` (`PedidoId_GarantiaPos`)
) ENGINE=InnoDB AUTO_INCREMENT=691658 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pedido_Produto_Seguro`
--

DROP TABLE IF EXISTS `Pedido_Produto_Seguro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pedido_Produto_Seguro` (
  `PedidoProdutoSeguroId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `SeguroCodigo` varchar(45) DEFAULT NULL,
  `ValorVenda` decimal(10,2) unsigned NOT NULL,
  `Quantidade` tinyint(3) unsigned NOT NULL,
  `Familia` int(10) unsigned NOT NULL,
  `StatusExportacao` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`PedidoProdutoSeguroId`),
  KEY `idx_gw_status_exportacao` (`StatusExportacao`)
) ENGINE=InnoDB AUTO_INCREMENT=4649 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pedido_Produto_Servico`
--

DROP TABLE IF EXISTS `Pedido_Produto_Servico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pedido_Produto_Servico` (
  `PedidoId` int(11) NOT NULL,
  `ProdutoId` int(11) NOT NULL,
  `ServicoId` int(11) NOT NULL,
  `ValorTotal` float NOT NULL,
  `Observacao` varchar(255) DEFAULT NULL COMMENT 'Se serviço for embrulho para presente, o campo observacao pode ser usado para guardar uma mensagem no cartao do embrulho.',
  `Quantidade` int(11) DEFAULT NULL COMMENT 'Quantos terão o serviço',
  `Codigo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`PedidoId`,`ProdutoId`,`ServicoId`),
  KEY `fk_Pedido_Produto_Servico_Pedido_Produto` (`PedidoId`,`ProdutoId`),
  KEY `fk_Pedido_Produto_Servico_Servico` (`ServicoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Pedido_Verificacao`
--

DROP TABLE IF EXISTS `Pedido_Verificacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Pedido_Verificacao` (
  `PedidoId` int(11) unsigned NOT NULL DEFAULT '0',
  `CodigoVerificacao` varchar(25) DEFAULT NULL,
  `SiteId` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`PedidoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Perfil_Categoria`
--

DROP TABLE IF EXISTS `Perfil_Categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Perfil_Categoria` (
  `PerfilId` int(11) unsigned NOT NULL COMMENT 'Categoria que se refere ao Perfil',
  `CategoriaId` int(11) unsigned NOT NULL COMMENT 'Categoria associada ao perfil',
  `MostraPerfil` tinyint(1) unsigned NOT NULL DEFAULT '0',
  `SiteId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`PerfilId`,`CategoriaId`,`SiteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Ponteiro`
--

DROP TABLE IF EXISTS `Ponteiro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Ponteiro` (
  `PedidoId` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PopUpPromocional`
--

DROP TABLE IF EXISTS `PopUpPromocional`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PopUpPromocional` (
  `PopUpPromocionalId` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `Titulo` char(200) DEFAULT NULL,
  `Selo` char(100) DEFAULT NULL,
  `Html` text NOT NULL,
  `Status` tinyint(3) unsigned NOT NULL,
  `SiteId` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`PopUpPromocionalId`),
  KEY `PopUpPromocionalSiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=35 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PopUpPromocionalCondicao`
--

DROP TABLE IF EXISTS `PopUpPromocionalCondicao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PopUpPromocionalCondicao` (
  `PopUpPromocionalCondicaoId` mediumint(8) unsigned NOT NULL AUTO_INCREMENT,
  `PopUpPromocionalId` smallint(5) unsigned NOT NULL,
  `TipoGatilho` smallint(5) unsigned NOT NULL,
  `GatilhoId` mediumint(8) unsigned NOT NULL,
  PRIMARY KEY (`PopUpPromocionalCondicaoId`),
  KEY `idx_GatilhoId` (`GatilhoId`)
) ENGINE=InnoDB AUTO_INCREMENT=4351 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PrecificacaoNokia`
--

DROP TABLE IF EXISTS `PrecificacaoNokia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PrecificacaoNokia` (
  `PrecificacaoNokiaId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ProdutoId` int(10) unsigned NOT NULL,
  `TituloId` tinyint(3) unsigned NOT NULL,
  `Texto` text NOT NULL,
  `Status` tinyint(3) unsigned NOT NULL,
  `Src` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`PrecificacaoNokiaId`),
  UNIQUE KEY `DescricaoNokiaId` (`PrecificacaoNokiaId`) USING BTREE,
  KEY `ProdutoId` (`ProdutoId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=3985 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Produto`
--

DROP TABLE IF EXISTS `Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Produto` (
  `ProdutoId` int(11) NOT NULL AUTO_INCREMENT,
  `FabricanteId` int(11) NOT NULL DEFAULT '0',
  `Codigo` varchar(45) NOT NULL DEFAULT '',
  `Nome` varchar(255) NOT NULL DEFAULT '',
  `Descricao` text,
  `Tipo` tinyint(4) NOT NULL,
  `Visualizacao` varchar(20) DEFAULT NULL COMMENT '1,2 = catalogo + upsell (exemplo)\n0 = invisivel\n1 = disponÃ­vel no catalogo\n2 = disponivel para upsell\n',
  `PrecoCusto` float DEFAULT NULL,
  `DataCadastroUniconsult` datetime DEFAULT NULL,
  `DataImportacao` datetime DEFAULT NULL,
  `DataCadastro` datetime DEFAULT NULL,
  `DataDisponivel` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' COMMENT 'Para prÃ©-venda',
  `DataLancamento` datetime DEFAULT NULL,
  `ParcelamentoSemJuros` tinyint(4) DEFAULT NULL,
  `NomeUniconsult` varchar(255) DEFAULT NULL,
  `VendeAvulso` tinyint(1) DEFAULT '1',
  `Vale` char(1) DEFAULT NULL,
  `Unidade` varchar(2) DEFAULT NULL,
  `Brinde` char(1) DEFAULT '0',
  `PrazoCrossDocking` int(11) DEFAULT NULL,
  `TipoTransporte` tinyint(1) DEFAULT NULL,
  `OkFiscal` char(1) DEFAULT NULL,
  `PrazoGarantia` int(4) DEFAULT NULL,
  `ModeloFabricante` varchar(50) DEFAULT NULL,
  `TipoABC` char(1) DEFAULT NULL,
  `Situacao` char(1) DEFAULT NULL,
  `UsuarioId` int(11) DEFAULT NULL,
  `Iframe` char(255) DEFAULT NULL,
  `AlturaIframe` mediumint(11) unsigned DEFAULT NULL,
  `EmLinha` tinyint(3) unsigned NOT NULL DEFAULT '1',
  PRIMARY KEY (`ProdutoId`),
  UNIQUE KEY `Produto_Codigo` (`Codigo`),
  KEY `fk_Produto_Fabricante` (`FabricanteId`),
  KEY `Produto_DataDisponivel` (`DataDisponivel`),
  KEY `UsuarioId` (`UsuarioId`)
) ENGINE=InnoDB AUTO_INCREMENT=556985 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ProdutoAvaliacao`
--

DROP TABLE IF EXISTS `ProdutoAvaliacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProdutoAvaliacao` (
  `ProdutoAvaliacaoId` int(11) NOT NULL AUTO_INCREMENT,
  `ProdutoId` int(11) NOT NULL DEFAULT '0',
  `ClienteId` int(11) NOT NULL DEFAULT '0',
  `Titulo` varchar(40) NOT NULL,
  `Pontuacao` float NOT NULL DEFAULT '0',
  `Comentario` text,
  `Data` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` tinyint(4) NOT NULL DEFAULT '0' COMMENT '0 = Aguardando liberaÃ§Ã£o\n1 = Liberado\n2 = Negado',
  `JaTenho` tinyint(1) DEFAULT NULL,
  `VouTer` tinyint(1) DEFAULT NULL,
  `Autorizacao` tinyint(1) DEFAULT NULL,
  `SiteId` int(11) DEFAULT NULL,
  PRIMARY KEY (`ProdutoAvaliacaoId`),
  KEY `fk_ProdutoAvaliacao_Produto` (`ProdutoId`),
  KEY `fk_ProdutoAvaliacao_Cliente` (`ClienteId`),
  KEY `ProdutoAvaliacao_Status` (`Status`),
  KEY `ProdutoAvaliacaoSiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=74635 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ProdutoCupom`
--

DROP TABLE IF EXISTS `ProdutoCupom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProdutoCupom` (
  `ProdutoCupomId` int(11) NOT NULL AUTO_INCREMENT,
  `CupomDescontoId` int(11) NOT NULL,
  `Nome` varchar(100) DEFAULT NULL,
  `Regras` text,
  `Imagem` varchar(40) DEFAULT NULL,
  `DataHoraInicio` datetime DEFAULT NULL,
  `DataHoraFim` datetime DEFAULT NULL,
  `PrecoPor` float DEFAULT NULL,
  `ParcelamentoMaximo` tinyint(3) DEFAULT NULL,
  `Status` tinyint(1) DEFAULT '0',
  `Ordem` int(10) DEFAULT NULL,
  PRIMARY KEY (`ProdutoCupomId`,`CupomDescontoId`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ProdutoCupom_Cliente`
--

DROP TABLE IF EXISTS `ProdutoCupom_Cliente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProdutoCupom_Cliente` (
  `ProdutoCupomId` int(11) NOT NULL,
  `Utilizado` tinyint(1) DEFAULT '0',
  `ClienteId` int(11) DEFAULT NULL,
  `Aprovado` tinyint(1) DEFAULT '0',
  `PedidoId` int(11) NOT NULL,
  `ProdutoId` int(11) NOT NULL,
  `Documento` varchar(14) NOT NULL,
  `CodigoSige` varchar(100) NOT NULL,
  PRIMARY KEY (`ProdutoCupomId`,`PedidoId`,`CodigoSige`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ProdutoCusto`
--

DROP TABLE IF EXISTS `ProdutoCusto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProdutoCusto` (
  `Codigo` int(11) NOT NULL,
  `EAN` char(20) DEFAULT NULL,
  `Fornecedor` char(200) DEFAULT NULL,
  `CodFornecedor` char(100) DEFAULT NULL,
  `PisSaida` decimal(10,2) DEFAULT NULL,
  `CofinsSaida` decimal(10,2) DEFAULT NULL,
  `CustoMedioProprio` decimal(10,2) DEFAULT NULL,
  `CustoMedioTerceiro` decimal(10,2) DEFAULT NULL,
  `QtdCompra` int(11) DEFAULT NULL,
  `ValorCompra` decimal(10,2) DEFAULT NULL,
  `QtdNaoDisponivel` int(11) DEFAULT NULL,
  `QtdConsignada` int(11) DEFAULT NULL,
  `Ipi` decimal(10,2) DEFAULT NULL,
  `IcmsEntrada` decimal(10,2) DEFAULT NULL,
  `PisEntrada` decimal(10,2) DEFAULT NULL,
  `CofinsEntrada` decimal(10,2) DEFAULT NULL,
  `ValorCustoEntrada` decimal(10,2) DEFAULT NULL,
  `SubsTributaria` decimal(10,2) DEFAULT NULL,
  `Nbm` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`Codigo`),
  KEY `EAN` (`EAN`),
  KEY `CodFornecedor` (`CodFornecedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ProdutoCusto_Estabelecimento`
--

DROP TABLE IF EXISTS `ProdutoCusto_Estabelecimento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProdutoCusto_Estabelecimento` (
  `Codigo` int(11) NOT NULL,
  `EstabelecimentoId` int(11) NOT NULL,
  `EAN` char(20) DEFAULT NULL,
  `Fornecedor` char(200) DEFAULT NULL,
  `CodFornecedor` char(100) DEFAULT NULL,
  `PisSaida` decimal(10,2) DEFAULT NULL,
  `CofinsSaida` decimal(10,2) DEFAULT NULL,
  `CustoMedioProprio` decimal(10,2) DEFAULT NULL,
  `CustoMedioTerceiro` decimal(10,2) DEFAULT NULL,
  `QtdCompra` int(11) DEFAULT NULL,
  `ValorCompra` decimal(10,2) DEFAULT NULL,
  `QtdNaoDisponivel` int(11) DEFAULT NULL,
  `QtdConsignada` int(11) DEFAULT NULL,
  `Ipi` decimal(10,2) DEFAULT NULL,
  `IcmsEntrada` decimal(10,2) DEFAULT NULL,
  `PisEntrada` decimal(10,2) DEFAULT NULL,
  `CofinsEntrada` decimal(10,2) DEFAULT NULL,
  `ValorCustoEntrada` decimal(10,2) DEFAULT NULL,
  `SubsTributaria` decimal(10,2) DEFAULT NULL,
  `Nbm` int(10) unsigned DEFAULT NULL,
  PRIMARY KEY (`Codigo`,`EstabelecimentoId`),
  KEY `EAN` (`EAN`),
  KEY `CodFornecedor` (`CodFornecedor`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`127.0.0.1`*/ /*!50003 TRIGGER `InserirProdutoCusto` AFTER INSERT ON `ProdutoCusto_Estabelecimento`
    FOR EACH ROW BEGIN
SELECT  PCE.Codigo AS Codigo,
PCE.EAN  AS EAN,
PCE.Fornecedor  AS Fornecedor,
PCE.CodFornecedor AS CodFornecedor,
PCE.PisSaida AS PisSaida,
PCE.CofinsSaida AS CofinsSaida,
PCE.CustoMedioProprio AS CustoMedioProprio,
PCE.CustoMedioTerceiro AS CustoMedioTerceiro,
PCE.QtdCompra AS QtdCompra,
PCE.ValorCompra AS ValorCompra,
PCE.QtdNaoDisponivel AS QtdNaoDisponivel,
PCE.QtdConsignada AS QtdConsignada,
PCE.Ipi AS Ipi,
PCE.IcmsEntrada AS IcmsEntrada,
PCE.PisEntrada AS PisEntrada,
PCE.CofinsEntrada AS CofinsEntrada,
PCE.ValorCustoEntrada AS ValorCustoEntrada,
PCE.SubsTributaria AS SubsTributaria,
PCE.Nbm AS Nbm

INTO    @Codigo   ,
@EAN       ,
@Fornecedor ,
@CodFornecedor,
@PisSaida     ,
@CofinsSaida  ,
@CustoMedioProprio,
@CustoMedioTerceiro,
@QtdCompra          ,
@ValorCompra        ,
@QtdNaoDisponivel   ,
@QtdConsignada      ,
@Ipi                ,
@IcmsEntrada        ,
@PisEntrada         ,
@CofinsEntrada      ,
@ValorCustoEntrada  ,
@SubsTributaria     ,
@Nbm
FROM ProdutoCusto_Estabelecimento PCE INNER JOIN Estabelecimento E ON( PCE.EstabelecimentoId = E.EstabelecimentoId )

WHERE Codigo = NEW.Codigo
ORDER BY E.Prioridade ASC
LIMIT 1;

INSERT INTO ProdutoCusto (
Codigo,
EAN,
Fornecedor,
CodFornecedor,
PisSaida,
CofinsSaida,
CustoMedioProprio,
CustoMedioTerceiro,
QtdCompra,
ValorCompra,
QtdNaoDisponivel,
QtdConsignada,
Ipi,
IcmsEntrada,
PisEntrada,
CofinsEntrada,
ValorCustoEntrada,
SubsTributaria,
Nbm
)
VALUES (
@Codigo       ,
@EAN          ,
@Fornecedor   ,
@CodFornecedor,
@PisSaida     ,
@CofinsSaida  ,
@CustoMedioProprio  ,
@CustoMedioTerceiro ,
@QtdCompra          ,
@ValorCompra        ,
@QtdNaoDisponivel   ,
@QtdConsignada      ,
@Ipi                ,
@IcmsEntrada        ,
@PisEntrada         ,
@CofinsEntrada      ,
@ValorCustoEntrada  ,
@SubsTributaria     ,
@Nbm
       )
ON DUPLICATE KEY UPDATE
Codigo             = @Codigo,
EAN                = @EAN,
Fornecedor         = @Fornecedor,
CodFornecedor      = @CodFornecedor,
PisSaida           = @PisSaida,
CofinsSaida        = @CofinsSaida,
CustoMedioProprio  = @CustoMedioProprio,
CustoMedioTerceiro = @CustoMedioTerceiro,
QtdCompra          = @QtdCompra,
ValorCompra        = @ValorCompra,
QtdNaoDisponivel   = @QtdNaoDisponivel,
QtdConsignada      = @QtdConsignada,
Ipi                = @Ipi,
IcmsEntrada        = @IcmsEntrada,
PisEntrada         = @PisEntrada,
CofinsEntrada      = @CofinsEntrada,
ValorCustoEntrada  = @ValorCustoEntrada,
SubsTributaria     = @SubsTributaria,
Nbm                = @Nbm;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`127.0.0.1`*/ /*!50003 TRIGGER `AtualizarProdutoCusto` AFTER UPDATE ON `ProdutoCusto_Estabelecimento`
    FOR EACH ROW BEGIN
SELECT  PCE.Codigo AS Codigo,
PCE.EAN  AS EAN,
PCE.Fornecedor  AS Fornecedor,
PCE.CodFornecedor AS CodFornecedor,
PCE.PisSaida AS PisSaida,
PCE.CofinsSaida AS CofinsSaida,
PCE.CustoMedioProprio AS CustoMedioProprio,
PCE.CustoMedioTerceiro AS CustoMedioTerceiro,
PCE.QtdCompra AS QtdCompra,
PCE.ValorCompra AS ValorCompra,
PCE.QtdNaoDisponivel AS QtdNaoDisponivel,
PCE.QtdConsignada AS QtdConsignada,
PCE.Ipi AS Ipi,
PCE.IcmsEntrada AS IcmsEntrada,
PCE.PisEntrada AS PisEntrada,
PCE.CofinsEntrada AS CofinsEntrada,
PCE.ValorCustoEntrada AS ValorCustoEntrada,
PCE.SubsTributaria AS SubsTributaria,
PCE.Nbm AS Nbm

INTO    @Codigo,
@EAN,
@Fornecedor,
@CodFornecedor,
@PisSaida     ,
@CofinsSaida  ,
@CustoMedioProprio  ,
@CustoMedioTerceiro ,
@QtdCompra          ,
@ValorCompra        ,
@QtdNaoDisponivel   ,
@QtdConsignada      ,
@Ipi                ,
@IcmsEntrada        ,
@PisEntrada         ,
@CofinsEntrada      ,
@ValorCustoEntrada  ,
@SubsTributaria     ,
@Nbm
FROM ProdutoCusto_Estabelecimento PCE INNER JOIN Estabelecimento E ON( PCE.EstabelecimentoId = E.EstabelecimentoId )
WHERE Codigo = NEW.Codigo
AND COALESCE(PCE.ValorCustoEntrada,0) > 0
ORDER BY E.Prioridade ASC
LIMIT 1;

UPDATE ProdutoCusto
SET
EAN                = @EAN,
Fornecedor         = @Fornecedor,
CodFornecedor      = @CodFornecedor,
PisSaida           = @PisSaida,
CofinsSaida        = @CofinsSaida,
CustoMedioProprio  = @CustoMedioProprio,
CustoMedioTerceiro = @CustoMedioTerceiro,
QtdCompra          = @QtdCompra,
ValorCompra        = @ValorCompra,
QtdNaoDisponivel   = @QtdNaoDisponivel,
QtdConsignada      = @QtdConsignada,
Ipi                = @Ipi,
IcmsEntrada        = @IcmsEntrada,
PisEntrada         = @PisEntrada,
CofinsEntrada      = @CofinsEntrada,
ValorCustoEntrada  = @ValorCustoEntrada,
SubsTributaria     = @SubsTributaria,
Nbm                = @Nbm

WHERE
    Codigo             = @Codigo;
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `ProdutoDimensao`
--

DROP TABLE IF EXISTS `ProdutoDimensao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProdutoDimensao` (
  `ProdutoDimensaoId` int(11) NOT NULL AUTO_INCREMENT,
  `ProdutoId` int(11) DEFAULT NULL,
  `Altura` float DEFAULT NULL,
  `Largura` float DEFAULT NULL,
  `Comprimento` float DEFAULT NULL,
  `Peso` float DEFAULT NULL,
  `AlturaEmbalagem` float DEFAULT NULL,
  `LarguraEmbalagem` float DEFAULT NULL,
  `ComprimentoEmbalagem` float DEFAULT NULL,
  `PesoEmbalagem` float DEFAULT NULL,
  PRIMARY KEY (`ProdutoDimensaoId`),
  UNIQUE KEY `fk_ProdutoDimensao_Produto` (`ProdutoId`)
) ENGINE=InnoDB AUTO_INCREMENT=586270 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ProdutoMedia`
--

DROP TABLE IF EXISTS `ProdutoMedia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProdutoMedia` (
  `ProdutoMediaId` int(11) NOT NULL AUTO_INCREMENT,
  `ProdutoId` int(11) NOT NULL DEFAULT '0',
  `Src` varchar(45) NOT NULL DEFAULT '',
  `Mime` varchar(20) DEFAULT NULL,
  `Ordem` tinyint(4) DEFAULT NULL,
  `ThumbNail` char(45) DEFAULT NULL,
  PRIMARY KEY (`ProdutoMediaId`),
  KEY `fk_ProdutoImagem_Produto` (`ProdutoId`)
) ENGINE=InnoDB AUTO_INCREMENT=4322615 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `ProdutoOnePage`
--

DROP TABLE IF EXISTS `ProdutoOnePage`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProdutoOnePage` (
  `ProdutoId` int(11) NOT NULL,
  `DescricaoThumbnail` char(36) DEFAULT NULL,
  `DescricaoPrincipal` varchar(360) DEFAULT NULL,
  `AtributosPrincipais` varchar(288) DEFAULT NULL,
  `CompativelCom` int(11) NOT NULL,
  `EspecificacaoTecnica` text,
  `ItensInclusos` text,
  `SiteId` int(10) unsigned NOT NULL,
  `Disponivel` tinyint(1) unsigned NOT NULL DEFAULT '1',
  `IncluirBrinde` int(11) unsigned NOT NULL DEFAULT '0',
  PRIMARY KEY (`ProdutoId`),
  KEY `SiteID` (`SiteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = latin1 */ ;
/*!50003 SET character_set_results = latin1 */ ;
/*!50003 SET collation_connection  = latin1_swedish_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`localhost`*/ /*!50003 TRIGGER InserirCarrocelNokia AFTER INSERT ON ProdutoOnePage
 FOR EACH ROW BEGIN
  INSERT INTO CarrocelNokia (ProdutoId ,Exibir, Ordem) VALUES(NEW.ProdutoId,0,NULL);
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `ProdutoVisto`
--

DROP TABLE IF EXISTS `ProdutoVisto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `ProdutoVisto` (
  `ProdutoVistoId` int(11) NOT NULL AUTO_INCREMENT,
  `ClienteId` int(11) DEFAULT NULL,
  `ProdutoId` int(11) DEFAULT NULL,
  `SessionId` varchar(50) NOT NULL DEFAULT '0',
  `Data` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` tinyint(4) DEFAULT '1',
  `SiteId` int(11) DEFAULT NULL,
  PRIMARY KEY (`ProdutoVistoId`),
  KEY `fk_ProdutoVisto_Cliente` (`ClienteId`),
  KEY `fk_ProdutoVisto_Produto` (`ProdutoId`),
  KEY `fk_ProdutoVisto_Session` (`SessionId`),
  KEY `ProdutoVistoSiteId` (`SiteId`),
  KEY `idx_data` (`Data`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Produto_Atributo`
--

DROP TABLE IF EXISTS `Produto_Atributo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Produto_Atributo` (
  `ProdutoId` int(11) unsigned NOT NULL,
  `AtributoCorId` int(11) unsigned NOT NULL,
  `AtributoTamanhoId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`ProdutoId`),
  KEY `ProdutoId` (`ProdutoId`),
  KEY `TamanhoIndex` (`AtributoTamanhoId`),
  KEY `CorIndex` (`AtributoCorId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Produto_AtributoValor`
--

DROP TABLE IF EXISTS `Produto_AtributoValor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Produto_AtributoValor` (
  `ProdutoId` int(11) NOT NULL DEFAULT '0',
  `AtributoValorId` int(11) NOT NULL DEFAULT '0',
  `Codigo` varchar(45) NOT NULL DEFAULT '',
  `Status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ProdutoId`,`AtributoValorId`),
  KEY `fk_Produto_AtributoValor_Produto` (`ProdutoId`),
  KEY `fk_Produto_AtributoValor_AtributoValor` (`AtributoValorId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Produto_Aviso`
--

DROP TABLE IF EXISTS `Produto_Aviso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Produto_Aviso` (
  `ProdutoId` int(11) NOT NULL COMMENT 'ID do produto',
  `Aviso` varchar(1000) DEFAULT NULL COMMENT 'Texto do aviso',
  PRIMARY KEY (`ProdutoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Produto_CaracteristicaValor`
--

DROP TABLE IF EXISTS `Produto_CaracteristicaValor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Produto_CaracteristicaValor` (
  `ProdutoId` int(11) NOT NULL DEFAULT '0',
  `CaracteristicaValorId` int(11) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ProdutoId`,`CaracteristicaValorId`),
  KEY `fk_Produto_CaracteristicaValor_Produto` (`ProdutoId`),
  KEY `fk_Produto_CaracteristicaValor_CaracteristicaValor` (`CaracteristicaValorId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Produto_Categoria`
--

DROP TABLE IF EXISTS `Produto_Categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Produto_Categoria` (
  `ProdutoId` int(11) NOT NULL DEFAULT '0',
  `CategoriaId` int(11) NOT NULL DEFAULT '0',
  `Principal` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ProdutoId`,`CategoriaId`),
  KEY `fk_Produto_Categoria_Produto` (`ProdutoId`),
  KEY `fk_Produto_Categoria_Categoria` (`CategoriaId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Produto_Complemento`
--

DROP TABLE IF EXISTS `Produto_Complemento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Produto_Complemento` (
  `ProdutoId` int(11) NOT NULL,
  `Nome` varchar(255) DEFAULT NULL,
  `Descricao` text,
  `SiteId` int(11) NOT NULL,
  PRIMARY KEY (`ProdutoId`,`SiteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Produto_DepartamentoSige`
--

DROP TABLE IF EXISTS `Produto_DepartamentoSige`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Produto_DepartamentoSige` (
  `Codigo` varchar(45) NOT NULL,
  `DepartamentoId` int(11) unsigned NOT NULL,
  `Nome` varchar(255) NOT NULL,
  PRIMARY KEY (`Codigo`),
  KEY `DepartamentoId_idx` (`DepartamentoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Produto_PDF`
--

DROP TABLE IF EXISTS `Produto_PDF`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Produto_PDF` (
  `ProdutoId` int(11) unsigned NOT NULL,
  `Tipo` tinyint(1) unsigned NOT NULL COMMENT '1 = Manual, 2 = Primeiro Capitulo do livro',
  `Src` varchar(80) NOT NULL,
  PRIMARY KEY (`ProdutoId`,`Tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Produto_ProntaEntrega`
--

DROP TABLE IF EXISTS `Produto_ProntaEntrega`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Produto_ProntaEntrega` (
  `ProdutoId` int(11) unsigned NOT NULL,
  `SiteId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`ProdutoId`,`SiteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Produto_Selo`
--

DROP TABLE IF EXISTS `Produto_Selo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Produto_Selo` (
  `ProdutoId` int(11) NOT NULL,
  `SeloId` int(11) NOT NULL,
  PRIMARY KEY (`ProdutoId`,`SeloId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Produto_Servico`
--

DROP TABLE IF EXISTS `Produto_Servico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Produto_Servico` (
  `ServicoId` int(11) NOT NULL,
  `ProdutoId` int(11) NOT NULL,
  `Valor` float NOT NULL,
  `Codigo` varchar(45) NOT NULL COMMENT 'Se for um item, qual codigo do item (produto)',
  `Status` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`ServicoId`,`Codigo`),
  UNIQUE KEY `Codigo` (`Codigo`),
  KEY `fk_Servico_Produto_Servico` (`ServicoId`),
  KEY `fk_Servico_Produto_Produto` (`ProdutoId`),
  KEY `Status` (`Status`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Produto_Site`
--

DROP TABLE IF EXISTS `Produto_Site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Produto_Site` (
  `ProdutoId` int(11) NOT NULL DEFAULT '0',
  `SiteId` int(11) NOT NULL DEFAULT '0',
  `PrecoDe` float DEFAULT NULL,
  `PrecoPor` float NOT NULL,
  `ParcelamentoMaximo` tinyint(4) NOT NULL,
  `Status` tinyint(4) NOT NULL,
  `DataAlteracao` datetime DEFAULT NULL,
  `UsuarioAlteracao` int(11) DEFAULT NULL,
  `DescontoAVista` float DEFAULT NULL,
  `UrlEspecialId` int(11) DEFAULT NULL,
  `Disponivel` tinyint(3) unsigned DEFAULT '1',
  `AtualizaDados` tinyint(3) unsigned DEFAULT '1',
  `ExcecaoLimpeza` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`ProdutoId`,`SiteId`),
  UNIQUE KEY `ProdutoComercialPk` (`ProdutoId`,`SiteId`),
  KEY `ProdutoComercialStatus` (`Status`),
  KEY `ProdutoParcelamentoMaximo` (`ParcelamentoMaximo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Produto_Site_Log`
--

DROP TABLE IF EXISTS `Produto_Site_Log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Produto_Site_Log` (
  `ProdutoLogId` int(11) NOT NULL AUTO_INCREMENT,
  `ProdutoId` int(11) NOT NULL DEFAULT '0',
  `SiteId` tinyint(4) unsigned NOT NULL DEFAULT '0',
  `PrecoDe` float DEFAULT NULL,
  `PrecoPor` float NOT NULL,
  `ParcelamentoMaximo` tinyint(4) DEFAULT NULL,
  `Status` tinyint(4) NOT NULL,
  `DataAlteracao` datetime DEFAULT NULL,
  `UsuarioAlteracao` int(11) NOT NULL,
  `DescontoAVista` float DEFAULT NULL,
  `UrlEspecialId` int(11) DEFAULT '0',
  `VendeAvulso` tinyint(4) DEFAULT NULL,
  `LocalAlteracao` tinyint(4) DEFAULT NULL COMMENT '1 - Produto Gerenciar, 2 - Alteracao Rapida, 3 - Importacao Lote',
  `UsuarioSige` varchar(50) DEFAULT NULL,
  PRIMARY KEY (`ProdutoLogId`),
  KEY `ProdutloSiteLog_Index1` (`ProdutoId`)
) ENGINE=InnoDB AUTO_INCREMENT=67752253 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Promocao`
--

DROP TABLE IF EXISTS `Promocao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Promocao` (
  `PromocaoId` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(45) DEFAULT NULL,
  `Descricao` text,
  `DataInicio` datetime DEFAULT NULL,
  `DataFim` datetime DEFAULT NULL,
  `DataCadastro` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` tinyint(4) NOT NULL DEFAULT '0',
  `Tipo` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1 = Normal	\n2 = Cupom de Desconto',
  `SeloUrl` varchar(100) DEFAULT NULL,
  `Prioridade` int(4) DEFAULT NULL,
  `SiteId` int(11) DEFAULT NULL,
  `Exclusiva` tinyint(4) NOT NULL DEFAULT '0',
  `NomeRegraPopup` varchar(45) DEFAULT NULL,
  `DescricaoRegraPopup` text,
  PRIMARY KEY (`PromocaoId`),
  UNIQUE KEY `PromocaoStatusData2` (`Status`,`SiteId`,`Tipo`,`PromocaoId`,`DataInicio`,`DataFim`),
  KEY `Promocao_Tipo` (`Tipo`),
  KEY `Promocao_Data` (`DataInicio`,`DataFim`),
  KEY `Promocao_Status` (`Status`),
  KEY `PromocaoSiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=94033 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`127.0.0.1`*/ /*!50003 TRIGGER `InserirPromocaoLog` AFTER INSERT ON `Promocao` FOR EACH ROW BEGIN INSERT INTO Promocao_Log(PromocaoId, Nome, Descricao, DataInicio, DataFim, Status, Tipo, SeloUrl, Prioridade, SiteId, Exclusiva, DataAtualizacao) VALUES(NEW.PromocaoId, NEW.Nome, NEW.Descricao, NEW.DataInicio, NEW.DataFim, NEW.Status, NEW.Tipo, NEW.SeloUrl, NEW.Prioridade, NEW.SiteId, NEW.Exclusiva,CURRENT_TIMESTAMP); END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = '' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`root`@`127.0.0.1`*/ /*!50003 TRIGGER `AlterarPromocaoLog` AFTER UPDATE ON `Promocao` FOR EACH ROW BEGIN INSERT INTO Promocao_Log(PromocaoId, Nome, Descricao, DataInicio, DataFim, Status, Tipo, SeloUrl, Prioridade, SiteId, Exclusiva, DataAtualizacao) VALUES(NEW.PromocaoId, NEW.Nome, NEW.Descricao, NEW.DataInicio, NEW.DataFim, NEW.Status, NEW.Tipo, NEW.SeloUrl, NEW.Prioridade, NEW.SiteId, NEW.Exclusiva,CURRENT_TIMESTAMP); END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;

--
-- Table structure for table `PromocaoAcao`
--

DROP TABLE IF EXISTS `PromocaoAcao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PromocaoAcao` (
  `PromocaoAcaoId` int(11) NOT NULL AUTO_INCREMENT,
  `PromocaoCondicaoId` int(11) NOT NULL DEFAULT '0',
  `Tipo` tinyint(4) NOT NULL DEFAULT '0',
  `Valor` float NOT NULL DEFAULT '0',
  `ProcOutrasRegras` tinyint(1) DEFAULT NULL,
  PRIMARY KEY (`PromocaoAcaoId`),
  KEY `fk_PromocaoAcao_PromocaoCondicao` (`PromocaoCondicaoId`)
) ENGINE=InnoDB AUTO_INCREMENT=16748 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PromocaoAutomatica_Produto`
--

DROP TABLE IF EXISTS `PromocaoAutomatica_Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PromocaoAutomatica_Produto` (
  `PromocaoId` int(11) unsigned NOT NULL DEFAULT '0' COMMENT 'Promoções do tipo 2 (Promoção automática)',
  `ProdutoId` int(11) unsigned NOT NULL DEFAULT '0',
  `PrecoDe` float DEFAULT NULL,
  `PrecoPor` float NOT NULL,
  `DescontoAVista` float unsigned DEFAULT NULL,
  `ParcelamentoMaximo` tinyint(4) unsigned DEFAULT NULL,
  `DataAlteracao` datetime DEFAULT NULL,
  `UsuarioAlteracao` int(11) DEFAULT NULL,
  `Status` tinyint(1) unsigned NOT NULL,
  `PrecoDe_Automatica` float DEFAULT NULL,
  `PrecoPor_Automatica` float NOT NULL,
  `DescontoAVista_Automatica` float unsigned DEFAULT NULL,
  `ParcelamentoMaximo_Automatica` tinyint(4) unsigned NOT NULL,
  `Status_Automatica` tinyint(1) unsigned NOT NULL,
  `StatusAlteracao` tinyint(1) DEFAULT '0' COMMENT '0 = Ainda não alterou no site / 1 = Está com o preço automático / 2 = Estava com o preço automático e já voltou para o padrão',
  PRIMARY KEY (`PromocaoId`,`ProdutoId`),
  UNIQUE KEY `ProdutoComercialPk` (`ProdutoId`,`PromocaoId`) USING BTREE,
  KEY `ProdutoComercialStatus` (`Status`),
  KEY `ProdutoParcelamentoMaximo` (`ParcelamentoMaximo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PromocaoCondicao`
--

DROP TABLE IF EXISTS `PromocaoCondicao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PromocaoCondicao` (
  `PromocaoCondicaoId` int(11) NOT NULL AUTO_INCREMENT,
  `PromocaoId` int(11) DEFAULT NULL,
  `ClienteId` int(11) DEFAULT NULL,
  `Valor` float DEFAULT NULL COMMENT '0 = Percentual\n1 = Valor Bruto',
  `Margem` float DEFAULT NULL,
  `Parcelas` tinyint(4) DEFAULT NULL COMMENT 'Valor ou Percentual',
  `FaixaCep` varchar(255) DEFAULT NULL,
  `QuantidadeMinima` tinyint(4) DEFAULT NULL COMMENT 'x de desconto a partir de y unidades',
  `QuantidadeAgrupada` tinyint(4) DEFAULT NULL COMMENT 'x de desconto a cada y unidades (leve 3 pague 2)',
  `LocalAplicacao` varchar(10) NOT NULL COMMENT '1 = Produto	\n2 = Carrinho\n1,2 = Produto e Carrinho',
  `ValorMaximo` float DEFAULT NULL,
  `ParcelaMinima` tinyint(4) unsigned DEFAULT NULL,
  PRIMARY KEY (`PromocaoCondicaoId`),
  UNIQUE KEY `PromocaoCondicao_PromoCli2` (`PromocaoId`,`ClienteId`,`PromocaoCondicaoId`,`Valor`,`ValorMaximo`,`QuantidadeMinima`,`Parcelas`),
  KEY `fk_PromocaoCondicao_Promocao` (`PromocaoId`),
  KEY `Local` (`LocalAplicacao`),
  KEY `fk_PromocaoCondicao_Cliente` (`ClienteId`)
) ENGINE=InnoDB AUTO_INCREMENT=11878 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PromocaoCondicao_Bandeira`
--

DROP TABLE IF EXISTS `PromocaoCondicao_Bandeira`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PromocaoCondicao_Bandeira` (
  `PromocaoCondicaoId` int(11) NOT NULL,
  `BandeiraId` int(11) NOT NULL,
  PRIMARY KEY (`PromocaoCondicaoId`,`BandeiraId`),
  KEY `fk_PromocaoCondicao_has_Bandeira_PromocaoCondicao1` (`PromocaoCondicaoId`),
  KEY `fk_PromocaoCondicao_has_Bandeira_Bandeira1` (`BandeiraId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PromocaoCondicao_BinCartao`
--

DROP TABLE IF EXISTS `PromocaoCondicao_BinCartao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PromocaoCondicao_BinCartao` (
  `PromocaoCondicaoId` int(10) unsigned NOT NULL,
  `BinCartao` int(10) unsigned NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PromocaoCondicao_Categoria`
--

DROP TABLE IF EXISTS `PromocaoCondicao_Categoria`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PromocaoCondicao_Categoria` (
  `PromocaoCondicaoId` int(11) NOT NULL,
  `CategoriaId` int(11) NOT NULL,
  PRIMARY KEY (`PromocaoCondicaoId`,`CategoriaId`),
  KEY `fk_PromocaoCondicao_has_Categoria_PromocaoCondicao1` (`PromocaoCondicaoId`),
  KEY `fk_PromocaoCondicao_has_Categoria_Categoria1` (`CategoriaId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PromocaoCondicao_Fabricante`
--

DROP TABLE IF EXISTS `PromocaoCondicao_Fabricante`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PromocaoCondicao_Fabricante` (
  `PromocaoCondicaoId` int(11) NOT NULL,
  `FabricanteId` int(11) NOT NULL,
  PRIMARY KEY (`PromocaoCondicaoId`,`FabricanteId`),
  KEY `fk_PromocaoCondicao_has_Fabricante_PromocaoCondicao1` (`PromocaoCondicaoId`),
  KEY `fk_PromocaoCondicao_has_Fabricante_Fabricante1` (`FabricanteId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PromocaoCondicao_Localidade`
--

DROP TABLE IF EXISTS `PromocaoCondicao_Localidade`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PromocaoCondicao_Localidade` (
  `PromocaoCondicaoId` int(11) NOT NULL DEFAULT '0',
  `LocalidadeSigla` char(3) NOT NULL DEFAULT '',
  PRIMARY KEY (`PromocaoCondicaoId`,`LocalidadeSigla`),
  KEY `fk_PromocaoCondicaoLocalidade1` (`PromocaoCondicaoId`),
  KEY `fk_PromocaoCondicaoLocalidade2` (`LocalidadeSigla`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PromocaoCondicao_MeioPagamento`
--

DROP TABLE IF EXISTS `PromocaoCondicao_MeioPagamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PromocaoCondicao_MeioPagamento` (
  `PromocaoCondicaoId` int(11) NOT NULL,
  `MeioPagamentoId` int(11) NOT NULL,
  PRIMARY KEY (`PromocaoCondicaoId`,`MeioPagamentoId`),
  KEY `fk_PromocaoCondicao_has_MeioPagamento_PromocaoCondicao1` (`PromocaoCondicaoId`),
  KEY `fk_PromocaoCondicao_has_MeioPagamento_MeioPagamento1` (`MeioPagamentoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PromocaoCondicao_Produto`
--

DROP TABLE IF EXISTS `PromocaoCondicao_Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PromocaoCondicao_Produto` (
  `PromocaoCondicaoId` int(11) NOT NULL,
  `ProdutoId` int(11) NOT NULL,
  PRIMARY KEY (`PromocaoCondicaoId`,`ProdutoId`),
  KEY `fk_PromocaoCondicao_has_Produto_PromocaoCondicao1` (`PromocaoCondicaoId`),
  KEY `fk_PromocaoCondicao_has_Produto_Produto1` (`ProdutoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PromocaoHotsite`
--

DROP TABLE IF EXISTS `PromocaoHotsite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PromocaoHotsite` (
  `PromocaoHotsiteId` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(100) DEFAULT NULL,
  `SiteId` int(11) NOT NULL,
  PRIMARY KEY (`PromocaoHotsiteId`,`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PromocaoHotsite_Cupom`
--

DROP TABLE IF EXISTS `PromocaoHotsite_Cupom`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PromocaoHotsite_Cupom` (
  `PromocaoHotsiteCupomId` int(11) NOT NULL AUTO_INCREMENT,
  `PromocaoHotsiteId` int(11) NOT NULL,
  `ClienteId` int(11) DEFAULT NULL,
  `PedidoId` int(11) DEFAULT NULL,
  `Cupom` text,
  PRIMARY KEY (`PromocaoHotsiteCupomId`,`PromocaoHotsiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=6445 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `PromocaoHotsite_Email`
--

DROP TABLE IF EXISTS `PromocaoHotsite_Email`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `PromocaoHotsite_Email` (
  `PromocaoHotsiteEmailId` int(11) NOT NULL AUTO_INCREMENT,
  `Email` varchar(80) DEFAULT NULL,
  `Enviado` tinyint(1) DEFAULT '0',
  `PromocaoHotsiteId` int(11) NOT NULL,
  `DataEnvio` datetime DEFAULT NULL,
  PRIMARY KEY (`PromocaoHotsiteEmailId`,`PromocaoHotsiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=209168 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Promocao_Log`
--

DROP TABLE IF EXISTS `Promocao_Log`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Promocao_Log` (
  `PromocaoLogId` int(11) NOT NULL AUTO_INCREMENT,
  `PromocaoId` int(11) NOT NULL,
  `Nome` varchar(45) DEFAULT NULL,
  `Descricao` text,
  `DataInicio` datetime DEFAULT NULL,
  `DataFim` datetime DEFAULT NULL,
  `DataAtualizacao` datetime NOT NULL DEFAULT '0000-00-00 00:00:00',
  `Status` tinyint(4) NOT NULL DEFAULT '0',
  `Tipo` tinyint(4) NOT NULL DEFAULT '0' COMMENT '1 = Normal\n2 = Cupom de Desconto',
  `SeloUrl` varchar(100) DEFAULT NULL,
  `Prioridade` int(4) DEFAULT NULL,
  `SiteId` int(11) DEFAULT NULL,
  `Exclusiva` tinyint(4) NOT NULL DEFAULT '0',
  PRIMARY KEY (`PromocaoLogId`),
  KEY `PromocaoId` (`PromocaoId`),
  KEY `Promocao_Tipo` (`Tipo`),
  KEY `Promocao_Data` (`DataInicio`,`DataFim`),
  KEY `Promocao_Status` (`Status`),
  KEY `PromocaoSiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=349540 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Temporary view structure for view `QuemViuComprou`
--

DROP TABLE IF EXISTS `QuemViuComprou`;
/*!50001 DROP VIEW IF EXISTS `QuemViuComprou`*/;
SET @saved_cs_client     = @@character_set_client;
SET character_set_client = utf8;
/*!50001 CREATE VIEW `QuemViuComprou` AS SELECT
 1 AS `ProdutoVisto`,
 1 AS `ProdutoComprado`,
 1 AS `QtdComprado`*/;
SET character_set_client = @saved_cs_client;

--
-- Table structure for table `Recuperacao_Venda`
--

DROP TABLE IF EXISTS `Recuperacao_Venda`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Recuperacao_Venda` (
  `RecuperacaoVendaId` int(11) NOT NULL AUTO_INCREMENT,
  `PedidoOriginal` int(11) DEFAULT NULL COMMENT 'Pedido Original',
  `PedidoRevenda` int(11) DEFAULT NULL COMMENT 'Pedido da revenda',
  PRIMARY KEY (`RecuperacaoVendaId`),
  UNIQUE KEY `fk_pedidorevendaid` (`PedidoRevenda`)
) ENGINE=InnoDB AUTO_INCREMENT=156501 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Redirect`
--

DROP TABLE IF EXISTS `Redirect`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Redirect` (
  `RedirectId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Origem` char(80) NOT NULL,
  `Destino` char(100) NOT NULL,
  `Analitycs` char(250) DEFAULT NULL,
  `SiteId` tinyint(3) unsigned NOT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT '0',
  `UsuarioId` int(11) DEFAULT NULL,
  `DataAtualizacao` datetime DEFAULT NULL,
  PRIMARY KEY (`RedirectId`),
  UNIQUE KEY `Redirect` (`RedirectId`),
  KEY `Origem` (`Origem`) USING BTREE,
  KEY `SiteId` (`SiteId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=1209 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Roteamento`
--

DROP TABLE IF EXISTS `Roteamento`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Roteamento` (
  `IP` varchar(21) DEFAULT NULL,
  `Contador` bigint(20) unsigned DEFAULT NULL,
  `Status` enum('DOWN','UP') DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SeguroTabela`
--

DROP TABLE IF EXISTS `SeguroTabela`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SeguroTabela` (
  `SeguroTabelaId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `CategoriaId` int(11) unsigned NOT NULL,
  `PrecoInicial` decimal(10,2) unsigned NOT NULL,
  `PrecoFinal` decimal(10,2) unsigned NOT NULL,
  `ValorVenda` decimal(10,2) unsigned NOT NULL,
  `Familia` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`SeguroTabelaId`)
) ENGINE=InnoDB AUTO_INCREMENT=1136 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Selo`
--

DROP TABLE IF EXISTS `Selo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Selo` (
  `SeloId` int(11) NOT NULL AUTO_INCREMENT,
  `Src` char(100) NOT NULL,
  `Mime` char(3) NOT NULL,
  `Ordem` tinyint(4) NOT NULL,
  `Texto` char(20) DEFAULT NULL,
  `SiteId` int(11) DEFAULT NULL,
  PRIMARY KEY (`SeloId`),
  KEY `Mime` (`Mime`)
) ENGINE=InnoDB AUTO_INCREMENT=302 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SeloProntaEntrega`
--

DROP TABLE IF EXISTS `SeloProntaEntrega`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SeloProntaEntrega` (
  `SeloProntaEntregaId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Selo` varchar(60) NOT NULL,
  `SiteId` int(11) unsigned NOT NULL,
  PRIMARY KEY (`SeloProntaEntregaId`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SeloProntaEntregaCondicao`
--

DROP TABLE IF EXISTS `SeloProntaEntregaCondicao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SeloProntaEntregaCondicao` (
  `CondicaoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `TipoCondicaoId` int(11) NOT NULL,
  `Condicao` varchar(15) NOT NULL,
  `Excecao` tinyint(1) unsigned DEFAULT NULL,
  `SeloProntaEntregaId` int(11) DEFAULT NULL,
  PRIMARY KEY (`CondicaoId`),
  KEY `fk_SeloProntaEntregaId` (`SeloProntaEntregaId`)
) ENGINE=InnoDB AUTO_INCREMENT=300 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SeloProntaEntregaTipoCondicao`
--

DROP TABLE IF EXISTS `SeloProntaEntregaTipoCondicao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SeloProntaEntregaTipoCondicao` (
  `TipoCondicaoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Descricao` char(20) NOT NULL,
  PRIMARY KEY (`TipoCondicaoId`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SeoRedirect`
--

DROP TABLE IF EXISTS `SeoRedirect`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SeoRedirect` (
  `SeoRedirectId` int(11) NOT NULL AUTO_INCREMENT,
  `Hash` char(32) NOT NULL,
  `Origem` text NOT NULL,
  `Destino` text NOT NULL,
  `Tipo` smallint(6) NOT NULL,
  `Status` tinyint(4) NOT NULL DEFAULT '1',
  `SiteId` int(11) NOT NULL,
  PRIMARY KEY (`SeoRedirectId`),
  UNIQUE KEY `un_SiteId_Hash` (`SiteId`,`Hash`)
) ENGINE=InnoDB AUTO_INCREMENT=711 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Servico`
--

DROP TABLE IF EXISTS `Servico`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Servico` (
  `ServicoId` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(45) NOT NULL DEFAULT '',
  `Status` tinyint(1) NOT NULL DEFAULT '0',
  `Tipo` tinyint(4) DEFAULT '1',
  PRIMARY KEY (`ServicoId`),
  KEY `Servico_Status` (`Status`),
  KEY `Tipo` (`Tipo`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Session`
--

DROP TABLE IF EXISTS `Session`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Session` (
  `SessionId` char(32) NOT NULL,
  `HttpUserAgent` char(32) NOT NULL,
  `SessionData` varchar(7500) NOT NULL,
  `SessionExpire` int(10) unsigned NOT NULL,
  `SiteId` enum('1','2') NOT NULL,
  UNIQUE KEY `idx_SessionId` (`SessionId`),
  KEY `idx_SessionExpire` (`SessionExpire`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SgwLog`
--

DROP TABLE IF EXISTS `SgwLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SgwLog` (
  `SgwLogId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Id` int(11) unsigned NOT NULL,
  `Tipo` tinyint(4) unsigned NOT NULL COMMENT '1 - Cupom Desconto, 2 - Atributo Grupo, 3 - Hotsite Html',
  `SiteId` int(11) unsigned NOT NULL,
  `UsuarioId` int(11) unsigned NOT NULL,
  `DataAlteracao` datetime DEFAULT NULL,
  `Chave` char(100) DEFAULT NULL COMMENT 'chave de pesquisa do log',
  `Detalhes` text,
  PRIMARY KEY (`SgwLogId`)
) ENGINE=InnoDB AUTO_INCREMENT=364562 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Similar`
--

DROP TABLE IF EXISTS `Similar`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Similar` (
  `SimilarId` int(11) NOT NULL,
  `ProdutoId` int(11) NOT NULL DEFAULT '0',
  `Quantidade` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`SimilarId`,`ProdutoId`),
  KEY `fk_Similar_Produto` (`ProdutoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Site`
--

DROP TABLE IF EXISTS `Site`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Site` (
  `SiteId` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` char(120) DEFAULT NULL,
  `Status` tinyint(1) DEFAULT NULL,
  `Url` char(255) DEFAULT NULL,
  `Site` char(255) DEFAULT NULL,
  `ImagemPath` char(20) DEFAULT NULL,
  `Slogan` char(255) DEFAULT NULL,
  `Email` char(255) DEFAULT NULL,
  `ListaCasamentoEmail` char(255) DEFAULT NULL,
  `SiteDomain` char(40) DEFAULT NULL,
  `Grupo` tinyint(2) DEFAULT NULL,
  `NomeFatura` char(13) DEFAULT NULL,
  `Tipo` tinyint(1) NOT NULL COMMENT '1 => Magazines, 2 => Nichos',
  PRIMARY KEY (`SiteId`),
  UNIQUE KEY `SiteId` (`SiteId`),
  UNIQUE KEY `SiteDomainIndex` (`SiteDomain`),
  KEY `Status` (`Status`)
) ENGINE=InnoDB AUTO_INCREMENT=52 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Site_Grupo`
--

DROP TABLE IF EXISTS `Site_Grupo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Site_Grupo` (
  `GrupoId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(45) DEFAULT NULL,
  `Status` tinyint(3) unsigned DEFAULT NULL,
  PRIMARY KEY (`GrupoId`)
) ENGINE=InnoDB AUTO_INCREMENT=10 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Sitemap`
--

DROP TABLE IF EXISTS `Sitemap`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Sitemap` (
  `SitemapId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `Slug` varchar(20) NOT NULL,
  `Nome` varchar(45) NOT NULL,
  `Descricao` varchar(255) DEFAULT NULL,
  `DataCadastro` datetime NOT NULL,
  `DataModificacao` datetime NOT NULL,
  `Status` tinyint(4) NOT NULL DEFAULT '1',
  `SiteId` int(10) unsigned NOT NULL,
  PRIMARY KEY (`SitemapId`),
  UNIQUE KEY `un_Slug` (`SiteId`,`Slug`)
) ENGINE=InnoDB AUTO_INCREMENT=17 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `SitemapUrl`
--

DROP TABLE IF EXISTS `SitemapUrl`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `SitemapUrl` (
  `SitemapId` int(10) unsigned NOT NULL,
  `Indice` int(10) unsigned NOT NULL,
  `Endereco` text NOT NULL,
  `Frequencia` enum('always','hourly','daily','weekly','monthly','yearly','never') DEFAULT NULL,
  `Prioridade` decimal(3,2) DEFAULT NULL,
  `Atualizacao` datetime DEFAULT NULL,
  PRIMARY KEY (`SitemapId`,`Indice`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TaxonomiaGoogle`
--

DROP TABLE IF EXISTS `TaxonomiaGoogle`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TaxonomiaGoogle` (
  `CatID` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` varchar(100) CHARACTER SET latin1 COLLATE latin1_general_ci DEFAULT NULL,
  `Nivel` tinyint(4) DEFAULT NULL,
  `CategoriaAsc` int(11) DEFAULT NULL,
  PRIMARY KEY (`CatID`)
) ENGINE=InnoDB AUTO_INCREMENT=4557 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TipoImpressora`
--

DROP TABLE IF EXISTS `TipoImpressora`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TipoImpressora` (
  `TipoImpressoraId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` char(50) DEFAULT NULL,
  PRIMARY KEY (`TipoImpressoraId`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `TipoLog`
--

DROP TABLE IF EXISTS `TipoLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `TipoLog` (
  `TipoLogId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Descricao` varchar(50) NOT NULL,
  PRIMARY KEY (`TipoLogId`)
) ENGINE=InnoDB AUTO_INCREMENT=100 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Upsell`
--

DROP TABLE IF EXISTS `Upsell`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Upsell` (
  `UpsellId` int(11) NOT NULL,
  `ProdutoId` int(11) NOT NULL,
  `Diferencial` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`UpsellId`,`ProdutoId`),
  KEY `Produto_has_Produto_Produto1` (`UpsellId`),
  KEY `Produto_has_Produto_Produto2` (`ProdutoId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UrlEspecial`
--

DROP TABLE IF EXISTS `UrlEspecial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UrlEspecial` (
  `UrlEspecialId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` char(100) DEFAULT NULL,
  `Url` char(255) DEFAULT NULL,
  `Status` tinyint(1) unsigned DEFAULT NULL,
  `SiteId` tinyint(2) unsigned DEFAULT NULL,
  PRIMARY KEY (`UrlEspecialId`)
) ENGINE=InnoDB AUTO_INCREMENT=14 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `UsuarioArquivo`
--

DROP TABLE IF EXISTS `UsuarioArquivo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `UsuarioArquivo` (
  `UsuarioArquivoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(50) NOT NULL,
  `Email` varchar(60) NOT NULL,
  `Senha` varchar(50) NOT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT '0',
  `Telefone` varchar(15) NOT NULL,
  `Sites` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`UsuarioArquivoId`)
) ENGINE=InnoDB AUTO_INCREMENT=7 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `V1`
--

DROP TABLE IF EXISTS `V1`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `V1` (
  `ProdutoId` int(11) NOT NULL DEFAULT '0',
  `VitrineBlocoId` int(11) NOT NULL DEFAULT '0',
  `Posicao` tinyint(4) NOT NULL DEFAULT '0',
  `Prioridade` tinyint(4) NOT NULL DEFAULT '0',
  KEY `fk_Vitrine_Produto_Produto` (`ProdutoId`),
  KEY `VitrineBloco_Produto_Posicao` (`Posicao`),
  KEY `fk_Vitrine_Produto_VitrineBloco` (`VitrineBlocoId`),
  KEY `VitrineBloco_Produto_Prioridade` (`Prioridade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `V2`
--

DROP TABLE IF EXISTS `V2`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `V2` (
  `ProdutoId` int(11) NOT NULL DEFAULT '0',
  `VitrineBlocoId` int(11) NOT NULL DEFAULT '0',
  `Posicao` tinyint(4) NOT NULL DEFAULT '0',
  `Prioridade` tinyint(4) NOT NULL DEFAULT '0',
  KEY `fk_Vitrine_Produto_Produto` (`ProdutoId`),
  KEY `VitrineBloco_Produto_Posicao` (`Posicao`),
  KEY `fk_Vitrine_Produto_VitrineBloco` (`VitrineBlocoId`),
  KEY `VitrineBloco_Produto_Prioridade` (`Prioridade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Vale`
--

DROP TABLE IF EXISTS `Vale`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Vale` (
  `ValeId` int(11) NOT NULL AUTO_INCREMENT,
  `Codigo` varchar(70) DEFAULT NULL,
  `Valor` float DEFAULT NULL,
  `ClienteId` int(11) DEFAULT NULL,
  `Status` tinyint(1) DEFAULT NULL,
  `Validade` date DEFAULT NULL,
  `SiteId` int(11) DEFAULT NULL,
  PRIMARY KEY (`ValeId`),
  UNIQUE KEY `Codigo_2` (`Codigo`),
  KEY `Codigo` (`Codigo`,`ClienteId`,`Status`,`Validade`),
  KEY `ValeSiteId` (`SiteId`),
  KEY `ValeIndex` (`ClienteId`,`Status`,`Validade`,`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=5181190 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Variavel`
--

DROP TABLE IF EXISTS `Variavel`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Variavel` (
  `VariavelId` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` char(30) NOT NULL,
  `Valor` text NOT NULL,
  `SiteId` int(11) NOT NULL,
  PRIMARY KEY (`VariavelId`),
  UNIQUE KEY `Nome_Site_uq` (`Nome`,`SiteId`),
  KEY `SiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=3178 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Variavel_copy`
--

DROP TABLE IF EXISTS `Variavel_copy`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Variavel_copy` (
  `VariavelId` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` char(30) NOT NULL,
  `Valor` varchar(255) NOT NULL,
  `SiteId` int(11) NOT NULL,
  PRIMARY KEY (`VariavelId`),
  KEY `SiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=280 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Vitrine`
--

DROP TABLE IF EXISTS `Vitrine`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Vitrine` (
  `VitrineId` int(11) NOT NULL AUTO_INCREMENT,
  `Status` int(1) DEFAULT '1',
  `SiteId` int(11) DEFAULT NULL,
  PRIMARY KEY (`VitrineId`),
  KEY `VitrineSiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=34073 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `VitrineBloco`
--

DROP TABLE IF EXISTS `VitrineBloco`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VitrineBloco` (
  `VitrineBlocoId` int(11) NOT NULL AUTO_INCREMENT,
  `VitrineId` int(11) DEFAULT NULL,
  `Nome` varchar(45) DEFAULT NULL,
  `Posicao` tinyint(4) DEFAULT NULL,
  `Status` tinyint(1) NOT NULL DEFAULT '0',
  PRIMARY KEY (`VitrineBlocoId`),
  KEY `fk_VitrineBloco_Vitrine` (`VitrineId`),
  KEY `VitrineBloco_Status` (`Status`)
) ENGINE=InnoDB AUTO_INCREMENT=26938 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `VitrineBloco_Produto`
--

DROP TABLE IF EXISTS `VitrineBloco_Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VitrineBloco_Produto` (
  `ProdutoId` int(11) NOT NULL DEFAULT '0',
  `VitrineBlocoId` int(11) NOT NULL DEFAULT '0',
  `Posicao` tinyint(4) NOT NULL DEFAULT '0',
  `Prioridade` tinyint(4) NOT NULL DEFAULT '0',
  KEY `fk_Vitrine_Produto_Produto` (`ProdutoId`),
  KEY `VitrineBloco_Produto_Posicao` (`Posicao`),
  KEY `fk_Vitrine_Produto_VitrineBloco` (`VitrineBlocoId`),
  KEY `VitrineBloco_Produto_Prioridade` (`Prioridade`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `VitrineReplicacao`
--

DROP TABLE IF EXISTS `VitrineReplicacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VitrineReplicacao` (
  `VitrineReplicacaoId` int(11) NOT NULL AUTO_INCREMENT,
  `SiteId` int(11) NOT NULL,
  `CategoriaId` int(11) NOT NULL,
  `SiteIdReplicar` int(11) NOT NULL,
  `CategoriaIdReplicar` int(11) NOT NULL,
  PRIMARY KEY (`VitrineReplicacaoId`)
) ENGINE=InnoDB AUTO_INCREMENT=3750 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `VitrineXml`
--

DROP TABLE IF EXISTS `VitrineXml`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VitrineXml` (
  `VitrineXmlId` int(11) NOT NULL AUTO_INCREMENT,
  `CategoriaId` int(11) DEFAULT NULL,
  `SiteId` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`VitrineXmlId`),
  KEY `fk_VitrineXml_CategoriaId` (`CategoriaId`),
  KEY `fk_VitrineXml_SiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=83 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `VitrineXml_Itens`
--

DROP TABLE IF EXISTS `VitrineXml_Itens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VitrineXml_Itens` (
  `VitrineXmlItensId` int(11) NOT NULL AUTO_INCREMENT,
  `VitrineXmlId` int(11) NOT NULL,
  `ProdutoId` int(11) NOT NULL DEFAULT '0',
  `Nome` char(255) DEFAULT NULL,
  `PrecoDe` char(40) DEFAULT NULL,
  `PrecoPor` char(40) DEFAULT NULL,
  `Frete` char(100) DEFAULT NULL,
  `ParcelamentoMaximo` tinyint(4) DEFAULT NULL,
  `Src` char(255) DEFAULT NULL,
  `Link` char(255) DEFAULT NULL,
  `ParcelamentoJuros` char(60) DEFAULT '',
  `Descricao1` text,
  `Descricao2` text,
  `Descricao3` text,
  `Ordem` tinyint(4) DEFAULT NULL,
  PRIMARY KEY (`VitrineXmlItensId`),
  KEY `fk_VitrineXmlItens_VitrineXmlId` (`VitrineXmlId`),
  KEY `fk_VitrineXmlItens_ProdutoId` (`ProdutoId`)
) ENGINE=InnoDB AUTO_INCREMENT=3090 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `VoxMidia_Produto`
--

DROP TABLE IF EXISTS `VoxMidia_Produto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VoxMidia_Produto` (
  `ProdutoId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Nome` varchar(200) NOT NULL,
  `Foto` varchar(200) NOT NULL,
  `Fase` int(11) unsigned NOT NULL,
  PRIMARY KEY (`ProdutoId`)
) ENGINE=InnoDB AUTO_INCREMENT=41 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `VoxMidia_Vencedor`
--

DROP TABLE IF EXISTS `VoxMidia_Vencedor`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VoxMidia_Vencedor` (
  `VencedorId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `TituloVencedor` varchar(120) DEFAULT NULL,
  `SubTituloVencedor` varchar(120) DEFAULT NULL,
  `FotoVencedor` varchar(120) DEFAULT NULL,
  `Porcentagem` varchar(4) DEFAULT NULL,
  `Fase` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`VencedorId`)
) ENGINE=InnoDB AUTO_INCREMENT=11 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `VoxMidia_Votacao`
--

DROP TABLE IF EXISTS `VoxMidia_Votacao`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `VoxMidia_Votacao` (
  `VotacaoId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `ProdutoId` int(10) unsigned NOT NULL,
  `Fase` int(10) unsigned NOT NULL,
  `Nome` char(80) DEFAULT NULL,
  `Email` char(60) DEFAULT NULL,
  `Optin` tinyint(1) unsigned DEFAULT '0',
  PRIMARY KEY (`VotacaoId`)
) ENGINE=InnoDB AUTO_INCREMENT=139421 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `Xml`
--

DROP TABLE IF EXISTS `Xml`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `Xml` (
  `XmlId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `Tipo` int(11) NOT NULL,
  `EstoqueDe` int(11) DEFAULT NULL,
  `EstoqueAte` int(11) DEFAULT NULL,
  `PrecoDe` float DEFAULT NULL,
  `PrecoAte` float DEFAULT NULL,
  `SiteId` int(11) DEFAULT NULL,
  `DataAtualizacao` datetime DEFAULT NULL,
  `Nome` varchar(45) NOT NULL,
  `TipoXml` tinyint(3) unsigned NOT NULL DEFAULT '1' COMMENT '1=>XML Antigo 2=> XML Avancado',
  `ParceiraId` int(10) unsigned DEFAULT NULL,
  `ParceiraIdSec` int(10) DEFAULT NULL COMMENT 'utilizado para google shopping',
  PRIMARY KEY (`XmlId`),
  KEY `XmlSiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=810 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `XmlCupomDesconto`
--

DROP TABLE IF EXISTS `XmlCupomDesconto`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `XmlCupomDesconto` (
  `XmlCupomDescontoId` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `XmlItensId` int(10) unsigned NOT NULL COMMENT 'Relacionamento com a tabela XmlItens',
  `CupomDescontoId` int(10) unsigned NOT NULL COMMENT 'Relacionamento com a tabela CupomDesconto',
  `SiteId` tinyint(3) unsigned NOT NULL,
  PRIMARY KEY (`XmlCupomDescontoId`),
  UNIQUE KEY `XmlCupomDescontoId` (`XmlCupomDescontoId`) USING BTREE,
  KEY `XmlItensId` (`XmlItensId`),
  KEY `SiteId` (`SiteId`)
) ENGINE=InnoDB AUTO_INCREMENT=332738 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `XmlItemRotulo`
--

DROP TABLE IF EXISTS `XmlItemRotulo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `XmlItemRotulo` (
  `ItemRotuloId` int(11) NOT NULL AUTO_INCREMENT,
  `RotuloId` int(11) NOT NULL,
  `XmlId` int(11) NOT NULL,
  `Posicao` smallint(6) NOT NULL,
  `Produtos` text NOT NULL,
  PRIMARY KEY (`ItemRotuloId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `XmlItens`
--

DROP TABLE IF EXISTS `XmlItens`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `XmlItens` (
  `XmlItensId` int(11) NOT NULL AUTO_INCREMENT,
  `XmlId` int(11) NOT NULL,
  `CategoriaId` int(11) NOT NULL,
  `ProdutosExcluidos` text,
  `ProdutosOcultos` text,
  `UrlAnalytics` text,
  `IdExclusaoXml` text,
  `IdExclusaoCupom` text,
  PRIMARY KEY (`XmlItensId`),
  KEY `XmlItens_Categoria` (`CategoriaId`),
  KEY `XmlIntes_Xml` (`XmlId`)
) ENGINE=InnoDB AUTO_INCREMENT=1328968 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `XmlLog`
--

DROP TABLE IF EXISTS `XmlLog`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `XmlLog` (
  `LogId` int(11) NOT NULL AUTO_INCREMENT COMMENT 'Chave Primária para a tabela de Log',
  `Time` datetime NOT NULL COMMENT 'Momento de gravação do log',
  `SiteId` int(11) NOT NULL,
  `XmlId` int(6) NOT NULL,
  `ParceiroId` int(11) NOT NULL,
  `ParceiroIp` char(16) CHARACTER SET latin1 COLLATE latin1_general_ci NOT NULL,
  `Tipo` tinyint(3) NOT NULL COMMENT '1 - Criado 2 - Visualizado',
  PRIMARY KEY (`LogId`)
) ENGINE=InnoDB AUTO_INCREMENT=1837108 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `XmlParceiro`
--

DROP TABLE IF EXISTS `XmlParceiro`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `XmlParceiro` (
  `XmlParceiroId` int(11) NOT NULL AUTO_INCREMENT,
  `Nome` char(60) DEFAULT NULL,
  `Status` tinyint(1) DEFAULT '1' COMMENT '1 = Ativado / 0 = Desativado',
  PRIMARY KEY (`XmlParceiroId`)
) ENGINE=InnoDB AUTO_INCREMENT=172 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `XmlRotulo`
--

DROP TABLE IF EXISTS `XmlRotulo`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `XmlRotulo` (
  `RotuloId` int(11) NOT NULL,
  `RotuloNome` varchar(60) DEFAULT NULL,
  PRIMARY KEY (`RotuloId`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `XmlSite`
--

DROP TABLE IF EXISTS `XmlSite`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `XmlSite` (
  `XmlId` int(11) NOT NULL AUTO_INCREMENT,
  `SiteId` int(11) NOT NULL,
  PRIMARY KEY (`XmlId`,`SiteId`) USING BTREE
) ENGINE=InnoDB AUTO_INCREMENT=810 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Table structure for table `XmlTime`
--

DROP TABLE IF EXISTS `XmlTime`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `XmlTime` (
  `XmlTimeId` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `XmlParceiroId` int(11) DEFAULT NULL,
  `Hora` int(2) DEFAULT NULL,
  `Status` int(11) DEFAULT NULL,
  PRIMARY KEY (`XmlTimeId`)
) ENGINE=InnoDB AUTO_INCREMENT=733 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Final view structure for view `QuemViuComprou`
--

/*!50001 DROP VIEW IF EXISTS `QuemViuComprou`*/;
/*!50001 SET @saved_cs_client          = @@character_set_client */;
/*!50001 SET @saved_cs_results         = @@character_set_results */;
/*!50001 SET @saved_col_connection     = @@collation_connection */;
/*!50001 SET character_set_client      = utf8 */;
/*!50001 SET character_set_results     = utf8 */;
/*!50001 SET collation_connection      = utf8_general_ci */;
/*!50001 CREATE ALGORITHM=UNDEFINED */
/*!50013 DEFINER=`root`@`localhost` SQL SECURITY DEFINER */
/*!50001 VIEW `QuemViuComprou` AS select `a`.`ProdutoId` AS `ProdutoVisto`,`c`.`ProdutoId` AS `ProdutoComprado`,count(`c`.`ProdutoId`) AS `QtdComprado` from ((`ProdutoVisto` `a` join `Pedido` `b`) join `Pedido_Produto` `c`) where ((`a`.`SessionId` = `b`.`SessionId`) and (`b`.`PedidoId` = `c`.`PedidoId`)) group by `c`.`ProdutoId` */;
/*!50001 SET character_set_client      = @saved_cs_client */;
/*!50001 SET character_set_results     = @saved_cs_results */;
/*!50001 SET collation_connection      = @saved_col_connection */;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2016-01-16 12:17:27
