use riel_integracao;

INSERT INTO `InterfacePermissao` (`InterfacePermissaoId`, `ParceiroSiteId`, `InterfaceTipoId`) VALUES
	(28, 2, 2),
	(30, 2, 1),
	(32, 2, 3);

INSERT INTO `InterfaceTracking` (`PedidoId`, `ParceiroId`, `PedidoParceiroId`, `Json`, `Hit`, `Versao`, `DataCriacao`, `DataAlteracao`, `DataUltimoConsumo`) VALUES
	(19629765, 11, 1025230352, '[{"pedido_parceiro":"1025230352","entregas":{"ENT_1962976501":{"itens":{"483103":{"qtd":"x"}},"trackings":{"PAP":{"d":"2014-11-26","link":"http:\\/\\/tracking.totalexpress.com.br\\/poupup_track.php?reid=1625&pedido=#PED_ENT_ORIG&nfiscal=#NF_REMESSA"},"ENT":{"d":"2014-11-26","link":"http:\\/\\/tracking.totalexpress.com.br\\/poupup_track.php?reid=1625&pedido=#PED_ENT_ORIG&nfiscal=#NF_REMESSA"},"ETR":{"d":"2014-11-26","link":"http:\\/\\/tracking.totalexpress.com.br\\/poupup_track.php?reid=1625&pedido=#PED_ENT_ORIG&nfiscal=#NF_REMESSA"},"NFS":{"d":"2014-11-26","link":"http:\\/\\/tracking.totalexpress.com.br\\/poupup_track.php?reid=1625&pedido=#PED_ENT_ORIG&nfiscal=#NF_REMESSA"},"WMS":{"d":"2014-11-26","link":"http:\\/\\/tracking.totalexpress.com.br\\/poupup_track.php?reid=1625&pedido=#PED_ENT_ORIG&nfiscal=#NF_REMESSA"}}}}}]', 0, 'V1', '2015-11-17 15:35:33', '2015-11-17 21:38:22', NULL),
	(19911905, 11, 906056691, '[{"pedido_parceiro":"906056691","entregas":{"ENT_1991190501":{"itens":{"365487":{"qtd":"x"}},"trackings":{"PAP":{"d":"2014-12-15","link":"http:\\/\\/ssw.inf.br\\/cgi-local\\/tracking\\/#CNPJ_FILIAL\\/#NF_REMESSA\\/#SR_NF_REMESSA"},"ENT":{"d":"2014-12-15","link":"http:\\/\\/ssw.inf.br\\/cgi-local\\/tracking\\/#CNPJ_FILIAL\\/#NF_REMESSA\\/#SR_NF_REMESSA"},"ETR":{"d":"2014-12-15","link":"http:\\/\\/ssw.inf.br\\/cgi-local\\/tracking\\/#CNPJ_FILIAL\\/#NF_REMESSA\\/#SR_NF_REMESSA"},"NFS":{"d":"2014-12-15","link":"http:\\/\\/ssw.inf.br\\/cgi-local\\/tracking\\/#CNPJ_FILIAL\\/#NF_REMESSA\\/#SR_NF_REMESSA"},"WMS":{"d":"2014-12-15","link":"http:\\/\\/ssw.inf.br\\/cgi-local\\/tracking\\/#CNPJ_FILIAL\\/#NF_REMESSA\\/#SR_NF_REMESSA"}}}}}]', 0, 'V1', '2015-11-17 15:35:33', '2015-11-17 21:42:48', NULL),
	(19918343, 11, 905719812, '[{"pedido_parceiro":"905719812","entregas":{"ENT_19918343":{"itens":{"365487":{"qtd":"x"}},"trackings":{"PAP":{"d":"2014-12-15","link":"http:\\/\\/ssw.inf.br\\/cgi-local\\/tracking\\/#CNPJ_FILIAL\\/#NF_REMESSA\\/#SR_NF_REMESSA"},"ENT":{"d":"2014-12-15","link":"http:\\/\\/ssw.inf.br\\/cgi-local\\/tracking\\/#CNPJ_FILIAL\\/#NF_REMESSA\\/#SR_NF_REMESSA"},"ETR":{"d":"2014-12-15","link":"http:\\/\\/ssw.inf.br\\/cgi-local\\/tracking\\/#CNPJ_FILIAL\\/#NF_REMESSA\\/#SR_NF_REMESSA"},"NFS":{"d":"2014-12-15","link":"http:\\/\\/ssw.inf.br\\/cgi-local\\/tracking\\/#CNPJ_FILIAL\\/#NF_REMESSA\\/#SR_NF_REMESSA"},"WMS":{"d":"2014-12-15","link":"http:\\/\\/ssw.inf.br\\/cgi-local\\/tracking\\/#CNPJ_FILIAL\\/#NF_REMESSA\\/#SR_NF_REMESSA"}}}}}]', 0, 'V1', '2015-11-17 15:35:33', '2015-11-17 21:42:48', NULL),
	(22336572, 2, 1148121, '[{"pedido_parceiro":"1148121","entregas":{"ENT_2233657201":{"itens":{"521903":{"qtd":"x"}},"trackings":{"WMS":{"d":"2015-07-23"},"NFS":{"d":"2015-07-23"},"ETR":{"d":"2015-07-23","link":"http:\\/\\/www.transfolha.com.br\\/outros\\/pesquisahttpentrega.asp?sCliente=000454&sChave=F7CCC5C0C8&sPesquisa=PEDIDO&sDado=2233657201"},"ENT":{"d":"2015-07-23"}}}}}]', 0, 'V1', '2015-10-23 15:20:58', '2015-11-09 17:46:16', NULL),
	(22360564, 2, 1148238, '[{"pedido_parceiro":"1148238","entregas":{"ENT_2236056401":{"itens":{"521899":{"qtd":"x"}},"trackings":{"WMS":{"d":"2015-07-28"},"NFS":{"d":"2015-07-28"},"ENT":{"d":"2015-07-28"}}}}}]', 15, 'V1', '2015-10-23 15:20:53', '2015-11-09 17:46:12', '2016-01-11 11:09:45');

INSERT INTO `Parceiro` (`ParceiroId`, `Nome`, `Status`) VALUES
	(2, 'Vtex', 1),
	(11, 'Mercado Livre RE', 1);

INSERT INTO `Parceiro_Site` (`ParceiroSiteId`, `ParceiroId`, `SiteId`, `SiteBase`, `Status`) VALUES
	(2, 2, 4, 1, 1),
	(11, 11, 32, 1, 1);

INSERT INTO `Parceiro_SiteIp` (`ParceiroSiteIpId`, `ParceiroSiteId`, `Ip`) VALUES
	(615, 2, '127.0.0.1'),
	(616, 2, '192.168.181.14');

INSERT INTO `Parceiro_Site_Permissao` (`ParceiroSitePermissaoId`, `ParceiroSiteId`, `PermissaoTipoId`, `Permissao`, `Excecao`) VALUES
	(6781, 2, 3, 0, 0),
	(6782, 2, 1, 631, 0),
	(6783, 2, 1, 407, 0),
	(6784, 2, 1, 1848, 0),
	(6785, 2, 1, 2205, 0),
	(6786, 2, 1, 701, 0),
	(6787, 2, 1, 407, 0),
	(6788, 2, 1, 407, 0),
	(6789, 2, 1, 407, 0),
	(6790, 2, 1, 701, 0),
	(6791, 2, 1, 1042, 0),
	(6792, 2, 1, 422, 0);

INSERT INTO riel_integracao.PermissaoTipo VALUES (1, 'loren'), (2, 'ipsum');

INSERT INTO riel_integracao.Configuracao (Chave, ValorDefault, TipoDefault) VALUES
	('URL_NOTIFICACAO', NULL, 'string'),
	('ACEITA_NOTIFICACAO', 0, 'bool');

INSERT INTO riel_integracao.Parceiro_Configuracao (ParceiroId, ConfiguracaoId, Valor, Tipo)
VALUES (2, 1, 'lorem', 'text'), (2, 2, '1', 'integer');


INSERT INTO riel_integracao.InterfaceTipo (InterfaceTipoId, Nome, Status) VALUES (1, 'Estoque', 1);
INSERT INTO riel_integracao.InterfaceTipo (InterfaceTipoId, Nome, Status) VALUES (2, 'Pedido', 1);
INSERT INTO riel_integracao.InterfaceTipo (InterfaceTipoId, Nome, Status) VALUES (3, 'Tracking', 1);

INSERT INTO riel_integracao.Interface (InterfaceId, Dados, DataCriacao, DataAprovacao, ParceiroSiteId, InterfaceTipoId, Enviada) VALUES (4, '[{"Codigo":"234373","Estoque":"107"}]', '2013-01-16 18:00:49', '2016-03-12 10:14:08', 2, 1, 2);
INSERT INTO riel_integracao.Interface (InterfaceId, Dados, DataCriacao, DataAprovacao, ParceiroSiteId, InterfaceTipoId, Enviada) VALUES (6, '[{"Codigo":"82469","Estoque":"697"},{"Codigo":"83337","Estoque":"143"},{"Codigo":"172575","Estoque":"1907"}]', '2013-01-16 18:03:12', '2016-03-08 18:04:07', 2, 1, 2);
INSERT INTO riel_integracao.Interface (InterfaceId, Dados, DataCriacao, DataAprovacao, ParceiroSiteId, InterfaceTipoId, Enviada) VALUES (8, '[{"Codigo":"83337","Estoque":"143"},{"Codigo":"214266","Estoque":"57"},{"Codigo":"234373","Estoque":"107"}]', '2013-01-16 18:04:23', '2016-03-08 18:04:07', 2, 1, 2);
INSERT INTO riel_integracao.Interface (InterfaceId, Dados, DataCriacao, DataAprovacao, ParceiroSiteId, InterfaceTipoId, Enviada) VALUES (10, '[{"Codigo":"172575","Estoque":"1907"}]', '2013-01-16 18:05:00', '2016-03-08 18:04:07', 2, 1, 2);
INSERT INTO riel_integracao.Interface (InterfaceId, Dados, DataCriacao, DataAprovacao, ParceiroSiteId, InterfaceTipoId, Enviada) VALUES (12, '[{"Codigo":"229681","Estoque":"262"}]', '2013-01-16 18:05:34', '2016-03-08 18:04:07', 2, 1, 2);
INSERT INTO riel_integracao.Interface (InterfaceId, Dados, DataCriacao, DataAprovacao, ParceiroSiteId, InterfaceTipoId, Enviada) VALUES (14, '[{"Codigo":"82469","Estoque":"696"},{"Codigo":"225675","Estoque":"16"},{"Codigo":"229681","Estoque":"262"}]', '2013-01-16 18:07:18', '2016-03-08 18:04:07', 2, 1, 0);
INSERT INTO riel_integracao.Interface (InterfaceId, Dados, DataCriacao, DataAprovacao, ParceiroSiteId, InterfaceTipoId, Enviada) VALUES (16, '[{"Codigo":"172575","Estoque":"1906"},{"Codigo":"214496","Estoque":"1102"},{"Codigo":"234373","Estoque":"106"}]', '2013-01-16 18:07:54', '2016-03-08 18:04:07', 2, 1, 2);
INSERT INTO riel_integracao.Interface (InterfaceId, Dados, DataCriacao, DataAprovacao, ParceiroSiteId, InterfaceTipoId, Enviada) VALUES (18, '[{"Codigo":"82469","Estoque":"696"},{"Codigo":"214496","Estoque":"1102"}]', '2013-01-16 18:08:28', '2016-03-08 18:04:07', 2, 1, 2);
INSERT INTO riel_integracao.Interface (InterfaceId, Dados, DataCriacao, DataAprovacao, ParceiroSiteId, InterfaceTipoId, Enviada) VALUES (20, '[{"Codigo":"83337","Estoque":"143"},{"Codigo":"172575","Estoque":"1906"},{"Codigo":"234373","Estoque":"107"},{"Codigo":"265979","Estoque":"55"}]', '2013-01-16 18:09:02', '2016-03-08 18:04:07', 2, 1, 2);
INSERT INTO riel_integracao.Interface (InterfaceId, Dados, DataCriacao, DataAprovacao, ParceiroSiteId, InterfaceTipoId, Enviada) VALUES (22, '[{"Codigo":"83337","Estoque":"143"}]', '2013-01-16 18:09:37', '2016-03-08 18:04:07', 2, 1, 2);

INSERT INTO riel_integracao.Parceiro_SiteIp (ParceiroSiteId, Ip) VALUES (11, '127.0.0.1');

INSERT INTO InterfacePermissao (ParceiroSiteId, InterfaceTipoId) VALUES (11, 1), (11, 2), (11, 3);