#!/bin/bash
DIR_PACOTE=$(cd "$(dirname "$0")"; pwd)
CHECKOUT_DIR=$(dirname $DIR_PACOTE)

PROD_DB_USER='root'
PROD_DB_PASS=$MYSQL_PASS
PROD_DB_HOST='localhost'

TEST_DB_USER='root'
TEST_DB_PASS=$MYSQL_PASS
TEST_DB_HOST='localhost'

#echo "ROOT_DIR: $CHECKOUT_DIR"

if [ ! -d "$CHECKOUT_DIR/src/vendor" ]; then
    echo "Não existe vendor"
    cd $CHECKOUT_DIR/src

    if [ ! -f "composer.phar" ]; then
        php -r "readfile('https://getcomposer.org/installer');" > composer-setup.php
        php composer-setup.php
        php -r "unlink('composer-setup.php');"
    fi

    if [ ! -f "composer.lock" ]; then
        php composer.phar install --ignore-platform-reqs
    else
        php composer.phar update
    fi

    if [ -f "composer.phar" ]; then
        rm -rf composer.phar
    fi
fi

#Conecta no banco e recria as estruturas
echo "Dropando schemas riel_producao e riel_integracao"
mysql -u $TEST_DB_USER -h $TEST_DB_HOST -p$TEST_DB_PASS -e 'DROP SCHEMA riel_producao;'
mysql -u $TEST_DB_USER -h $TEST_DB_HOST -p$TEST_DB_PASS -e 'DROP SCHEMA riel_integracao;'

echo "Recriando schemas riel_producao e riel_integracao"
mysql -u $TEST_DB_USER -h $TEST_DB_HOST -p$TEST_DB_PASS -e 'CREATE SCHEMA riel_producao;'
mysql -u $TEST_DB_USER -h $TEST_DB_HOST -p$TEST_DB_PASS -e 'CREATE SCHEMA riel_integracao;'

echo "Publicando estruturas riel_producao e riel_integracao"
mysql -u $TEST_DB_USER -h $TEST_DB_HOST -p$TEST_DB_PASS < $CHECKOUT_DIR/test/massas/dumpEstruturaIntegracao.sql
mysql -u $TEST_DB_USER -h $TEST_DB_HOST -p$TEST_DB_PASS < $CHECKOUT_DIR/test/massas/dumpEstruturaProducao.sql

echo "Publicando massa riel_producao e riel_integracao"
mysql -u $TEST_DB_USER -h $TEST_DB_HOST -p$TEST_DB_PASS < $CHECKOUT_DIR/test/massas/massaDados.sql
mysql -u $TEST_DB_USER -h $TEST_DB_HOST -p$TEST_DB_PASS < $CHECKOUT_DIR/test/massas/massaDadosIntegracao.sql

mkdir -p $CHECKOUT_DIR/test/reports

$CHECKOUT_DIR/src/vendor/bin/phpunit --coverage-clover=$CHECKOUT_DIR/test/reports/clover.xml --coverage-xml=$CHECKOUT_DIR/test/reports/coverage --configuration=$CHECKOUT_DIR/test/phpunit.xml --log-junit=$CHECKOUT_DIR/test/reports/TEST-integracao.xml $CHECKOUT_DIR/test 
