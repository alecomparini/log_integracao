<?php

use App\Core\Database;

/**
 * @covers \App\Core\Database
 */
class DatabaseTest extends PHPUnit_Framework_TestCase {

    /**
     * Instancia da classe \App\Core\Database
     * @var instance
     */
    public static $database;

    /**
     * Instancia o \App\Core\Database
     * @method setUpBeforeClass
     */
    public static function setUpBeforeClass() {
        self::$database = Database::getInstance();
    }

    /**
     * Verifica qualquer erro de sintaxe e excucao do construct
     * @covers \App\Core\Database::__construct
     * @test   testConstruct
     */
    public function testConstruct() {
        $result = Database::getInstance();
        $this->assertNotEmpty($result, "O __construct do Database nao executou ou falhou!");
    }

    /**
     * Verifica se o prepare executa e retorna o esperado (O SQL, caso seja success)
     * @covers \App\Core\Database::prepare
     * @test   testPrepare
     */
    public function testPrepare() {
        $sql = "SELECT * FROM InterfaceTracking LIMIT 2";
        $result = self::$database->prepare($sql);
        $this->assertNotEmpty($result, "O metodo prepare nao executou ou nao retornou nada");
        $this->assertContains($sql, $result, "O metodo prepare nao retornou o esperado");
    }

    /**
     * Verifica se o get executa e retorna o esperado (Objeto)
     * @covers \App\Core\Database::get
     * @test   testGet
     */
    public function testGet() {
        $sql = "SELECT * FROM InterfaceTracking LIMIT 2";
        $result = self::$database->get($sql);
        $this->assertNotEmpty($result, "O metodo get nao executou ou nao retornou nada");
        $this->assertEquals("object", gettype($result), "O metodo get nao esta trazendo um objeto!");

        // Teste pra fetch com query falsa
        try {
            $sql = "SELECT * FROM QUERYINVALIDA WHERE Invalido = 'TRUE'";
            self::$database->get($sql);
        } catch (Exception $e) {
            $this->assertRegexp('/(Table \'riel_integracao\.queryinvalida\' doesn\'t exist).*/i', $e->getMessage(), "A mensagem da exception de tipo de sql invalido em get esta invalida!");
        }
    }

    /**
     * Verifica se o getAll executa e retorna o esperado (Array de objetos)
     * @covers \App\Core\Database::getAll
     * @test   testGetAll
     */
    public function testGetAll() {
        $sql = "SELECT * FROM InterfaceTracking LIMIT 2";
        $result = self::$database->getAll($sql);
        $this->assertNotEmpty($result, "O metodo getAll nao executou ou nao retornou nada");
        $this->assertEquals("array", gettype($result), "O metodo getAll nao esta trazendo um array de objetos!");
        $this->assertEquals("object", gettype($result[0]), "O metodo getAll nao esta trazendo um array de objetos!");
        $this->assertEquals(2, count($result), "O metodo getAll nao esta trazendo a quantidade certa de dados!");
    }

    /**
     * Verifica se o Insert executa e realmente insere o dado no banco
     * @covers \App\Core\Database::insert
     * @test   testInsert
     */
    public function testInsert() {
        try {
            $sql = "INSERT INTO `InterfaceTracking` VALUES('9999999','11','9999999','[{json_exemplo : true}]','0','V1','2015-11-17 15:35:33','2015-11-17 21:42:48',NULL)";

            $qtdAntes = (int)self::$database->get("SELECT COUNT(*) AS count FROM InterfaceTracking")->count;
            self::$database->insert($sql);
            $qtdDepois = (int)self::$database->get("SELECT COUNT(*) AS count FROM InterfaceTracking")->count;

            $this->assertTrue(($qtdDepois > $qtdAntes), "O metodo insert nao inseriu ou nao aumentou o numero de dados da tabela");
        } catch (Exception $e) {
            $this->fail('Insert falhou com a seguinte mensagem: ' . $e->getMessage());
        }

        try {
            // Teste pra insert com query falsa
            $sql = "INSERT INTO QUERYINVALIDA VALUES('INVALIDO', 'FALSO')";
            self::$database->insert($sql);
        } catch (Exception $e) {
            $this->assertRegexp('/(Table \'riel_integracao\.queryinvalida\' doesn\'t exist).*/i', $e->getMessage(), "A mensagem da exception de tipo de sql invalido em insert esta invalida!");
        }
    }

    /**
     * Verifica se o execute executa e realiza update e delete no banco
     * @covers \App\Core\Database::execute
     * @test   testExecute
     */
    public function testExecute() {
        $sql = "insert into `InterfaceTracking` VALUES('1111111','11','1111111','[{json_exemplo : true}]','0','V1','2015-11-17 15:35:33','2015-11-17 21:42:48',NULL)";
        self::$database->insert($sql);
        $qtdAntes = (int)self::$database->get("SELECT COUNT(*) as count FROM InterfaceTracking")->count;

        $sql = "UPDATE InterfaceTracking SET PedidoId = '8888888' WHERE PedidoId = '1111111'";
        $returnUpdate = self::$database->execute($sql);
        $this->assertTrue($returnUpdate, "O metodo execute nao realizou o update com sucesso!");

        $sql = "DELETE FROM InterfaceTracking WHERE PedidoId = '8888888'";
        $returnDelete = self::$database->execute($sql);
        $this->assertTrue($returnDelete, "O metodo delete nao realizou o update com sucesso!");

        $qtdDepois = (int)self::$database->get("SELECT COUNT(*) as count FROM InterfaceTracking")->count;
        $this->assertTrue(($qtdDepois < $qtdAntes), "O metodo execute nao deletou ou nao diminuiu o numero de dados da tabela");

        // Teste pra execute com query falsa
        try {
            $sql = "UPDATE QUERYINVALIDA SET INVALIDO = 'TRUE' WHERE INVALIDO = 'FALSE')";
            self::$database->execute($sql);
        } catch (Exception $e) {
            $this->assertRegexp('/(You have an error in your SQL syntax).*/', $e->getMessage(), "A mensagem da exception de tipo de sql invalido em execute esta invalida!");
        }
    }

    /**
     * Verifica se o begin transaction tem algum erro de sintaxe
     * @covers  \App\Core\Database::beginTransaction
     * @test   testBeginTransaction
     */
    public function testBeginTransaction() {
        $result = self::$database->beginTransaction();
        $this->assertEquals(1, PHPUnit_Framework_Assert::readAttribute(self::$database, 'qtdStartTransaction'));

        $this->assertEquals(self::$database, $result, "O metodo begin transaction nao rodou ou teve retorno invalido");

        $result = self::$database->beginTransaction();
        $this->assertEquals(self::$database, $result, "O metodo begin transaction nao rodou ou teve retorno invalido");
        $this->assertEquals(2, PHPUnit_Framework_Assert::readAttribute(self::$database, 'qtdStartTransaction'));
    }

    /**
     * Verifica se o rollback tem algum erro de sintaxe
     * @covers  \App\Core\Database::rollback
     * @test   testRollback
     */
    public function testRollback() {
        $result = self::$database->rollback();
        $this->assertEquals(self::$database, $result, "O metodo rollback nao rodou ou teve retorno invalido");
    }

    /**
     * Verifica se o commit tem algum erro de sintaxe
     * e se está commitando apenas no último
     * @covers  \App\Core\Database::commit
     * @test   testCommit
     */
    public function testCommit() {
        self::$database->beginTransaction();
        $this->assertEquals(1, PHPUnit_Framework_Assert::readAttribute(self::$database, 'qtdStartTransaction'));

        self::$database->beginTransaction();
        $this->assertEquals(2, PHPUnit_Framework_Assert::readAttribute(self::$database, 'qtdStartTransaction'));

        $result = self::$database->commit();
        $this->assertEquals(self::$database, $result, "O metodo commit nao rodou ou teve retorno invalido");
        $this->assertEquals(1, PHPUnit_Framework_Assert::readAttribute(self::$database, 'qtdStartTransaction'));

        //Testa um segundo commit
        self::$database->commit();
        $this->assertEquals(0, PHPUnit_Framework_Assert::readAttribute(self::$database, 'qtdStartTransaction'));
        $this->assertEquals(false, PHPUnit_Framework_Assert::readAttribute(self::$database, 'hasOpenTransaction'));
    }

    /**
     * Checa se a mudanca de Path para execucao de queries versionadas
     * @covers \App\Core\Database::setSqlPath
     * @test   testSetSqlPath
     */
    public function testSetSqlPath() {
        $result = self::$database->setSqlPath(TEST_DIR . "Service" . DS . "Query" . DS);
        $this->assertEquals(self::$database, $result, "O metodo setSqlPath nao rodou ou teve retorno invalido");

        try {
            $path = "CaminhoInvalido/Invalido";
            $test = self::$database->setSqlPath($path);
        } catch (Exception $e) {
            $this->assertRegexp('/(Caminho informado nao e um diretorio).*/', $e->getMessage(), "A mensagem da exception de caminho invalido esta invalida!");
        }
    }

    /**
     * Verifica se o getSqlFile executa e realmente atualiza o caminho de arquivos SQL
     * @covers \App\Core\Database::getSqlFile
     * @test   testGetSqlFile
     */
    public function testGetSqlFile() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        self::$database->setSqlPath(TEST_DIR . "Service" . DS . "Query" . DS);

        $path = "ParceiroSite/QryGetParceiroSite";
        $return = self::$database->getSqlFile($path);
        $this->assertRegexp('/(SELECT).*/', $return, "O getSqlFile nao esta retornando um SQL valido");

        try {
            $path = "ParceiroSite/QryInexistente";
            $test = self::$database->getSqlFile($path);
        } catch (Exception $e) {
            $this->assertEquals(5, $e->getCode(), "O codigo da exception de query vazia esta invalida!");
            $this->assertRegexp('/(Arquivo SQL nao encontrado).*/', $e->getMessage(), "A mensagem da exception de query invalida esta invalida!");
        }

        try {
            $path = "Test/QryVazia";
            $test = self::$database->getSqlFile($path);
        } catch (Exception $e) {
            $this->assertEquals(6, $e->getCode(), "O codigo da exception de query vazia esta invalida!");
            $this->assertRegexp('/(Arquivo SQL retorna uma query vazia).*/', $e->getMessage(), "A mensagem da exception de query vazia esta invalida!");
        }

        try {
            $test = self::$database->getSqlFile("");
        } catch (Exception $e) {
            $this->assertRegexp('/(O arquivo SQL e obrigatorio).*/', $e->getMessage(), "A mensagem da exception de arquivo de query vazio esta invalida!");
        }
    }

    /**
     * Verifica se o getSql executa, atribui os parametros e traz a query formatada.
     * @covers \App\Core\Database::getSql
     * @test   testGetSql
     */
    public function testGetSql() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        self::$database->setSqlPath(TEST_DIR . "Service" . DS . "Query" . DS);

        $path = "ParceiroSite/QryGetParceiroSite";
        $return = self::$database->getSql($path, array("parceiro_id" => 3, "site_id" => 11));
        $this->assertRegexp('/.*(ParceiroId = 3).*/', $return, "O metodo getSql nao bindou corretamente o 1 parametro");
        $this->assertRegexp('/.*(SiteId = 11).*/', $return, "O metodo getSql nao bindou corretamente o 2 parametro");

        $path = "Test/QryTestRange";
        $return = self::$database->getSql($path, array("parceiro_id" => array(1, 5, 7, 3), "site_id" => array(10, 11, 8, 9, 2)));
        $this->assertRegexp('/.*(ParceiroId IN \(1).*/', $return, "O metodo getSql nao bindou corretamente o 1 parametro");
        $this->assertRegexp('/.*(SiteId IN \(10).*/', $return, "O metodo getSql nao bindou corretamente o 2 parametro");
    }

}
