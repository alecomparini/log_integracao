<?php

use App\Core\DAO;

/**
 * @covers \App\Core\DAO
 */
class DAOTest extends PHPUnit_Framework_TestCase {

    /**
     * Verifica se o toggle de mapeamento esta rodando e disparando as exception necessarias
     * @covers \App\Core\DAO::setMap
     * @test   testSetMap
     */
    public function testSetMap() {
        $stub = $this->getMockForAbstractClass('\App\Core\DAO');
        $result = $stub->setMap(true);
        $this->assertTrue($result, "O metodo setMap nao retornou o status atualizado ou nao atualizou o mapping");
        $result = $stub->setMap(false);
        $this->assertFalse($result, "O metodo setMap nao retornou o status atualizado ou nao atualizou o mapping");

        try {
            $stub->setMap(23323);
        } catch (Exception $e) {
            $this->assertEquals("InvalidArgumentException", get_class($e), "A classe de exception do tipo de map esta invalida!");
            $this->assertRegexp('/(Variavel map deve ser booleana).*/', $e->getMessage(), "A mensagem da exception de tipo de map esta invalida!");
        }
    }

    /**
     * Verifica se o toggle de debug esta rodando e disparando as exception necessarias
     * @covers \App\Core\DAO::setDebug
     * @test   testSetDebug
     */
    public function testSetDebug() {
        $stub = $this->getMockForAbstractClass('\App\Core\DAO');
        $result = $stub->setDebug(true);
        $this->assertEquals($stub, $result, "O metodo setDebug nao retornou a instancia esperada");
        $result = $stub->setDebug(false);
        $this->assertEquals($stub, $result, "O metodo setDebug nao retornou a instancia esperada");

        try {
            $stub->setDebug("NAO BOLLEANO");
        } catch (Exception $e) {
            $this->assertEquals("InvalidArgumentException", get_class($e), "A classe de exception do tipo de debugging esta invalida!");
            $this->assertRegexp('/(Variavel debugging deve ser booleana).*/', $e->getMessage(), "A mensagem da exception de tipo de debugging esta invalida!");
        }
    }


    /**
     * Verifica se o retorno de uma query traz dados e se mapeia certo
     * @covers \App\Core\DAO::get
     * @test   testGet
     */
    public function testGet() {
        $stub = $this->getMockForAbstractClass('\App\Core\DAO');
        $stub->model = "ParceiroSite";
        $sql = "ParceiroSite/QryGetParceiroSite";
        $params = array("parceiro_id" => 11, "site_id" => 32);

        // @TODO Testes sem parametros

        // Testa sem mapear a classe
        $stub->setMap(false);
        $results = $stub->get($sql, $params);
        $this->assertNotEmpty($results, "O metodo get nao trouxe dados");
        $this->assertEquals(count($results), 1, "O retorno de get nao esta trazendo a quantidade correta de dados!");

        // Testa mapeando a classe
        $stub->setMap(true);
        $results = $stub->get($sql, $params);
        $this->assertInstanceOf('\App\Model\ParceiroSite', $results, "O metodo de map nao trouxe a instancia correta");
        $this->assertNotEmpty($results, "O metodo get nao trouxe dados");
    }
    
    /**
     * Verifica se o retorno de uma query traz dados e se mapeia certo
     * @covers \App\Core\DAO::raw
     * @test   testRaw
     */
    public function testRaw() {
        $stub = $this->getMockForAbstractClass('\App\Core\DAO');
        $stub->model = "ParceiroSite";
        
        //Executa simplesmente o debug pra pegar php exceptions
        $stub->setDebug(true);
        $stub->raw("SELECT * FROM InterfaceTracking LIMIT 5");
        $this->expectOutputRegex('/select\s+\*\s+from/i');
        $stub->setDebug(false);
        
        // Testa sem mapear a classe
        $stub->setMap(false);
        $result = $stub->raw("SELECT * FROM InterfaceTracking LIMIT 5");
        $this->assertNotEmpty($result, "O metodo raw nao trouxe dados");
        $this->assertInstanceOf("stdClass", $result, "O metodo raw nao esta retornando uma stdClass quando deveria");
        
        // Testa mapeando a classe
        $stub->setMap(true);
        $result = $stub->raw("SELECT * FROM InterfaceTracking LIMIT 5");
        $this->assertNotEmpty($result, "O metodo raw nao trouxe dados");
        $this->assertInstanceOf('\App\Model\ParceiroSite', $result, "O metodo raw nao esta retornando uma instancia de uma classe quando deveria");
    }


    /**
     * Verifica se o retorno de uma query traz uma colecao de dados e se mapeia certo
     * @covers \App\Core\DAO::getAll
     * @test   testGetAll
     */
    public function testGetAll() {
        $stub = $this->getMockForAbstractClass('\App\Core\DAO');
        $stub->model = "Tracking";
        $sql = "Tracking/QryGetAll";
        $params = array("parceiroId" => 11, "siteId" => 32, "sqlIn" => array(1025230352, 906056691, 905719812));

        //Executa simplesmente o debug pra pegar php exceptions
        $stub->setDebug(true);
        $stub->getAll($sql, $params);
        $stub->setDebug(false);
        $this->expectOutputRegex('/select\s+\it.*\s+from/i');

        // Testa sem mapear a classe
        $stub->setMap(false);
        $unmapped = $stub->getAll($sql, $params);
        $this->assertNotEmpty($unmapped, "O metodo getAll nao trouxe dados");
        $this->assertEquals(count($unmapped), 3, "O retorno de getAll nao esta trazendo a quantidade correta de dados!");

        // Testa mapeando a classe
        $stub->setMap(true);
        $mapped = $stub->getAll($sql, $params);
        $this->assertNotEmpty($mapped, "O metodo getAll nao trouxe dados");
        $this->assertEquals(count($mapped), 3, "O retorno de getAll nao esta trazendo a quantidade correta de dados!");
        $this->assertTrue(is_array($mapped), "O metodo getAll nao esta trazendo uma colecao de dados!");

        foreach ($mapped as $result) {
            $this->assertInstanceOf('\App\Model\Tracking', $result, "O metodo de map nao trouxe a instancia correta!");
        }
    }
}
