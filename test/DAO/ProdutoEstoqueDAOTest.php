<?php
use App\DAO\ProdutoEstoqueDAO;

/**
 * @covers \App\DAO\ProdutoEstoqueDAO
 */
class ProdutoEstoqueDAOTest extends PHPUnit_Framework_TestCase {


    /**
     * Instancia da classe \App\DAO\ProdutoEstoqueDAO
     * @var ProdutoEstoqueDAO
     */
    public static $produtoEstoqueDAO;

    /**
     * Instancia o \App\DAO\ProdutoEstoqueDAO
     * @method setUpBeforeClass
     */
    public static function setUpBeforeClass() {
        self::$produtoEstoqueDAO = new ProdutoEstoqueDAO();
    }

    /**
     * @covers \App\DAO\ProdutoEstoqueDAO::getEstoques
     */
    public function testGetEstoques() {
        $parceiroId = 2;
        $siteId = 4;

        $produtoCodigo = array(8019, 13377);
        $info = self::$produtoEstoqueDAO->getEstoques($parceiroId, $siteId, $produtoCodigo);
        $this->assertEquals(500, $info[0]->EstoqueDisponivel);
        $this->assertEquals(500, $info[1]->EstoqueDisponivel);

        $produtoCodigo = array(8019);
        $info = self::$produtoEstoqueDAO->getEstoques($parceiroId, $siteId, $produtoCodigo);
        $this->assertEquals(500, $info[0]->EstoqueDisponivel);

        $produtoCodigo = 13377;
        $info = self::$produtoEstoqueDAO->getEstoques($parceiroId, $siteId, $produtoCodigo);
        $this->assertEquals(500, $info[0]->EstoqueDisponivel);



    }
}