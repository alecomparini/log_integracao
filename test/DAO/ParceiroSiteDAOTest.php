<?php

use App\DAO\ParceiroSiteDAO;
use App\Core\DAO;

/**
 * @covers \App\DAO\ParceiroSiteDAO
 */
class ParceiroSiteDAOTest extends PHPUnit_Framework_TestCase {

    /**
     * Instancia da classe \App\DAO\ParceiroSiteDAO
     * @var ParceiroSiteDAO
     */
    public static $parceiroSiteDAO;

    /**
     * Instancia o \App\DAO\ParceiroSiteDAO
     * @method setUpBeforeClass
     */
    public static function setUpBeforeClass() {
        self::$parceiroSiteDAO = new ParceiroSiteDAO();
    }

    /**
     * Testa se o método está retornando um parceiro site devidamente
     * @covers \App\DAO\ParceiroSiteDAO::getParceiroSite
     * @test   testGetParceiroSite
     */
    public function testGetParceiroSite() {
        $parceiroSite = self::$parceiroSiteDAO->getParceiroSite(2, 4);
        $this->assertEquals(2, $parceiroSite->parceiroSiteId);
        $this->assertEquals(1, $parceiroSite->siteBase);
        $this->assertEquals("Vtex", $parceiroSite->nome);
        $this->assertEquals(1, $parceiroSite->status);
    }

    /**
     * Testa se o método está retornando um parceiro site devidamente
     * @covers \App\DAO\ParceiroSiteDAO::getParceiroSiteById
     * @test   testGetParceiroSiteById
     */
    public function testGetParceiroSiteById() {
        $parceiroSite = self::$parceiroSiteDAO->getParceiroSiteById(2);
        $this->assertEquals(2, $parceiroSite->parceiroSiteId);
        $this->assertEquals(1, $parceiroSite->siteBase);
        $this->assertEquals("Vtex", $parceiroSite->nome);
        $this->assertEquals(1, $parceiroSite->status);

    }

}
