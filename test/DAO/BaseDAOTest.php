<?php

use \App\DAO\ProdutoDAO;

/**
 * @package tests\DAO
 * @covers \App\DAO\BaseDAO
 */
class BaseDAOTest extends PHPUnit_Framework_TestCase
{
    /**
     * @covers \App\DAO\BaseDAO
     */
    public function testConstruct() {
        $baseDAO = new ProdutoDAO();
        self::assertInstanceOf('\App\DAO\BaseDAO', $baseDAO);
    }
}