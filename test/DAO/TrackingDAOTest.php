<?php

use App\DAO\TrackingDAO;
use App\Core\DAO;

/**
 * @covers \App\DAO\TrackingDAO
 */
class TrackingDAOTest extends PHPUnit_Framework_TestCase {
    
    /**
     * Instancia da classe \App\DAO\TrackingDAO
     * @var TrackingDAO
     */
    public static $trackingDAO;
    
    /**
     * Instancia o \App\DAO\TrackingDAO
     * @method setUpBeforeClass
     */
    public static function setUpBeforeClass() {
        self::$trackingDAO = new TrackingDAO;
    }
    
    /**
     * Verifica o retorno de busca de tracking pelo ID de pedido
     * @covers \App\DAO\TrackingDAO::getById
     * @test   testGetById
     */
    public function testGetById() {
        $result = self::$trackingDAO->getById(19629765);
        $resultJson = json_decode($result->json);
        
        $atributos = array('pedidoId', 'parceiroId', 'pedidoParceiroId', 'json', 'hit', 'versao', 'dataCriacao', 'dataAlteracao', 'dataUltimoConsumo');
        $atributosJson = array('pedido_parceiro', 'entregas');
        
        $this->assertTrue(is_array($resultJson), 'O JSON retornado nao esta no padrao correto!');        
        
        // Verifica se o retorno veio com todos os atributos
        foreach ($atributos as $atributo) {
            $this->assertObjectHasAttribute($atributo, $result, 'Atributo $atributo nao encontrado!');
        }
        
        // Verifica se o retorno JSON veio com todos os atributos
        foreach ($resultJson as $jsonData) {
            foreach ($atributosJson as $atributo) {
                $this->assertObjectHasAttribute($atributo, $jsonData, 'Atributo $atributo nao encontrado!');
            }
        }
    }
    
    /**
     * Verifica o retorno de busca de tracking por um range de Pedidos / Parceiro / Site
     * @covers \App\DAO\TrackingDAO::find
     * @test   testFind
     */
    public function testFind() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $pedidosId = array(1025230352, 906056691, 905719812);
        $results = self::$trackingDAO->find($pedidosId, 11, 32);
        
        $atributos = array('pedidoId', 'parceiroId', 'pedidoParceiroId', 'json', 'hit', 'versao', 'dataCriacao', 'dataAlteracao', 'dataUltimoConsumo');
        
        $this->assertEquals(3, count($results), "O retorno nao veio com a quantidade desejada");
        
        $atributosJson = array('pedido_parceiro', 'entregas');
        
        foreach ($results as $result) {
            $resultJson = json_decode($result->json);
            $this->assertTrue(is_array($resultJson), 'O JSON retornado nao esta no padrao correto!');        

            // Verifica se o retorno veio com todos os atributos
            foreach ($atributos as $atributo) {
                $this->assertObjectHasAttribute($atributo, $result, 'Atributo $atributo nao encontrado!');
            }
            
            // Verifica se o retorno JSON veio com todos os atributos
            foreach ($resultJson as $jsonData) {
                foreach ($atributosJson as $atributo) {
                    $this->assertObjectHasAttribute($atributo, $jsonData, 'Atributo $atributo nao encontrado!');
                }
            }
        }
    }
}
