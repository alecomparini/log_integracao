<?php
use App\DAO\ProdutoDAO;

/**
 * @covers \App\DAO\ProdutoDAO
 */
class ProdutoDAOTest extends PHPUnit_Framework_TestCase {


    /**
     * Instancia da classe \App\DAO\ProdutoDAO
     * @var ProdutoDAO
     */
    public static $produtoDAO;

    /**
     * Instancia o \App\DAO\ProdutoDAO
     * @method setUpBeforeClass
     */
    public static function setUpBeforeClass() {
        self::$produtoDAO = new ProdutoDAO();
    }

    /**
     * @covers \App\DAO\ProdutoDAO::__construct
     */
    public function testConstrucao() {
        $dao = new ProdutoDAO();
        $this->assertInstanceOf('\App\DAO\ProdutoDAO', $dao, 'Não é instância de ProdutoDAO');
        $this->assertEquals('Produto', PHPUnit_Framework_Assert::readAttribute($dao, 'model'));
    }

    /**
     * @covers \App\DAO\ProdutoDAO::getCodProdutos
     */
    public function testGetCodProdutos() {
        $cod = self::$produtoDAO->getCodProdutos(array('C39574'));
        $this->assertEquals(39574, $cod[0]->produtoId, 'Código de produto retornado incorreto');
        $this->assertTrue(is_array($cod), 'Não é array');
        $this->assertInstanceOf('\App\Model\Produto', $cod[0], 'Código não retornou o tipo \App\Model\Produto');
    }

    /**
     * @covers \App\DAO\ProdutoDAO::getProdutosFull
     */
    public function testGetProdutosFull() {
        $parceiroId = 2; //VTEX
        $siteId = 4; //NOKIA
        $produtosId = array(11774, 39056);

        $produtos = self::$produtoDAO->getProdutosFull($siteId, $produtosId, null, $parceiroId);
        $this->assertTrue(is_array($produtos));
        $this->assertEquals(1, count($produtos));
        $this->assertInstanceOf('\App\Model\Produto', $produtos[0]);
    }

    /**
     * @covers \App\DAO\ProdutoDAO::getProdutosFullCombo
     */
    public function testGetProdutosFullCombo() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $siteId = 4;
        $produtosId = array(11774);
        $produtoPai = 39056;

        $produtos = self::$produtoDAO->getProdutosFullCombo($siteId, $produtosId, $produtoPai);

        $this->assertTrue(is_array($produtos));
        $this->assertEquals(1, count($produtos));
        $this->assertInstanceOf('\App\Model\Produto', $produtos[0]);
    }

    /**
     * @covers \App\DAO\ProdutoDAO::getComboFilhos
     */
    public function testGetComboFilhos() {
        $siteId = 4;
        $produtoPai = 40461;

        $produtos = self::$produtoDAO->getComboFilhos($produtoPai, $siteId);
        $this->assertTrue(is_array($produtos));
        $this->assertEquals(2, count($produtos));
        $this->assertInstanceOf('\App\Model\Produto', $produtos[0]);
    }

    /**
     * @covers \App\DAO\ProdutoDAO::getProdutosAll
     */
    public function testGetProdutosAll() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $siteId = 1;

        $produtos = self::$produtoDAO->getProdutosAll($siteId);

        $this->assertTrue(is_array($produtos));
        $this->assertEquals(1, count($produtos));
        $this->assertInstanceOf('\App\Model\Produto', $produtos[0]);
    }

    /**
    * @covers \App\DAO\ProdutoDAO::getProdutos
    */
    public function testGetProdutos() {
        $siteId = 1;
        $produtoId = 11774;

        $produtos = self::$produtoDAO->getProdutos($siteId, $produtoId);

        $this->assertTrue(is_array($produtos));
        $this->assertEquals(1, count($produtos));
        $this->assertInstanceOf('\App\Model\Produto', $produtos[0]);
    }

    /**
     * @covers \App\DAO\ProdutoDAO::getProdutosChanged
     */
    public function testGetProdutosChanged() {
        $siteId = 1;
        $dataAlteracao = '2012-10-19 14:45:34';

        $produtos = self::$produtoDAO->getProdutosChanged($siteId, $dataAlteracao);

        $this->assertTrue(is_array($produtos));
        $this->assertEquals(1, count($produtos));
        $this->assertInstanceOf('\App\Model\Produto', $produtos[0]);
    }

    /**
     * @covers \App\DAO\ProdutoDAO::buscaProdutoPermissao
     */
    public function testBuscaProdutoPermissao() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $siteId = 1;
        $produtoCodigo = '13377';
        $idsFabricantesPermissao = array(353);
        $idsFabricantesExcecao = array();
        $idsCategoriasPermissao = array(497);
        $idsCategoriasExcecao = array();


        $produtos = self::$produtoDAO->buscaProdutoPermissao($produtoCodigo, $siteId, $idsFabricantesPermissao, $idsFabricantesExcecao, $idsCategoriasPermissao, $idsCategoriasExcecao);

        $this->assertTrue(is_array($produtos));
        $this->assertEquals(1, count($produtos));
        $this->assertInstanceOf('\stdClass', $produtos[0]);
    }

    /**
     * @covers \App\DAO\ProdutoDAO::getProdutoEstoque
     */
    public function testGetProdutoEstoque() {
        $produtoCodigo = '13377';

        $estoque = self::$produtoDAO->getProdutoEstoque($produtoCodigo);

        $this->assertTrue(is_object($estoque));
        $this->assertEquals(500, $estoque->Disponivel);
        $this->assertEquals(16902, $estoque->ProdutoId);
        $this->assertInstanceOf('\stdClass', $estoque);
    }

    /**
     * @covers \App\DAO\ProdutoDAO::getProdutoEstoque
     */
    public function testGetProdutoCustoEntrada() {
        $produtoCodigo = '13377';

        $custoEntrada = self::$produtoDAO->getProdutoCustoEntrada($produtoCodigo);

        $this->assertTrue(is_float($custoEntrada));
        $this->assertEquals(270.5, $custoEntrada);

        $produtoCodigo = 'C40468';
        $custoEntrada = self::$produtoDAO->getProdutoCustoEntrada($produtoCodigo);
        $this->assertTrue(is_float($custoEntrada));
        $this->assertEquals(0.0, $custoEntrada);


    }

    /**
     * @covers  \App\DAO\ProdutoDAO::getProdutoInfo
     */
    public function testGetProdutoInfo() {
        $info = self::$produtoDAO->getProdutoInfo(11774, 4);
        $this->assertInstanceOf('\stdClass', $info, 'Info não foi retornado um stdClass');
        $this->assertEquals(22450, $info->PrecoPor, 'Preço por deveria ser 19');
        $this->assertEquals(10, $info->ParcelamentoMaximo, 'Parcelamento máximo deve ser 10');
    }
}