<?php

use App\DAO\VendedorDAO;
use App\Core\DAO;

/**
 * @covers \App\DAO\VendedorDAO
 */
class VendedorDAOTest extends PHPUnit_Framework_TestCase {
    
    /**
     * Instancia da classe \App\DAO\VendedorDAO
     * @var VendedorDAO
     */
    public static $vendedorDAO;
    
    /**
     * Instancia o \App\DAO\ParceiroSiteDAO
     * @method setUpBeforeClass
     */
    public static function setUpBeforeClass() {
        self::$vendedorDAO = new VendedorDAO();
    }
    
    /**
     * Testa se o método está retornando um operadorb2c devidamente
     * @covers \App\DAO\VendedorDAO::getVendedorByEmail
     * @test   testVendedorByEmail
     */
    public function testVendedorByEmail() {
        $vendedor = self::$vendedorDAO->getVendedorByEmail('jeann.reis@ricardoeletro.com');
        $this->assertEquals("eb4da621718f1c65ffe637897b954616", $vendedor->senha);
        $this->assertEquals(4, $vendedor->id);
        $this->assertEquals("JEANN RAFAEL PEREIRA REIS", $vendedor->nome);
        $this->assertEquals(0, $vendedor->tipo);
        $this->assertEquals("TELEVENDAS/MG", $vendedor->grupo);
        $this->assertEquals(3, $vendedor->grupoId);
    }


}
