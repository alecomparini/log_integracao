<?php
use App\DAO\CartaoDAO;

/**
 * @covers \App\DAO\CartaoDAO
 */
class CartaoDAOTest extends PHPUnit_Framework_TestCase {

    /**
     * @covers \App\DAO\CartaoDAO::__construct
     */
    public function testConstrucao() {
        $cartaoDAO = new CartaoDAO();

        $this->assertInstanceOf('\App\DAO\CartaoDAO', $cartaoDAO, 'Não instanciou um cartão DAO');

        return $cartaoDAO;
    }

    /**
     * @param \App\DAO\CartaoDAO $cartaoDAO
     * @covers \App\DAO\CartaoDAO::getFatoresCartao
     * @depends testConstrucao
     */
    public function testGetFatoresCartao($cartaoDAO) {
        $fatores = $cartaoDAO->getFatoresCartao();

        $this->assertTrue(is_array($fatores), 'Fatores não retorou um array');
        $this->assertEquals(12, count($fatores), 'Quantidade de fatores deveria ser 12');
        $this->assertEquals(1, $fatores[0]->Fator, 'O fator para 1 parcela deveria ser apenas 1');
    }

}