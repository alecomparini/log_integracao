<?php

/**
 * Class Utils
 *
 * Classe repositório de algumas ferramentas utilitárias
 */
class Utils {

    /**
     * Compara campos e valores de dois arrays recursivamente e retorna um array com as diferenças,
     * array vazio significa que os arrays são idênticos
     *
     * @param $amostra
     * @param $teste
     * @return array
     */
    public static function showDiffArray($amostra, $teste) {
        $resultado = array();
        $chaves = array_keys($teste);

        foreach ($chaves as $chave) {

            if (!array_key_exists($chave, $amostra)) {
                $resultado[] = "A chave [$chave] nao foi encontrado na amostra.";
            } else {
                if (is_array($teste[$chave])) {
                    if (!is_array($amostra[$chave])) {
                        $resultado[] = "A chave [$chave] da amostra deveria ser um array.";
                    } else {
                        $tmp_resultado = static::showDiffArray($amostra[$chave], $teste[$chave]);
                        if (is_array($tmp_resultado)) {
                            $resultado = array_merge($tmp_resultado, $resultado);
                        }
                    }
                } else {
                    if ($amostra[$chave] != $teste[$chave]) {
                        $resultado[] = "A chave [$chave] da amostra [$amostra[$chave]] nao bate com a informacao no teste [$teste[$chave]].";
                    }
                }
            }
        }

        return $resultado;
    }
}
