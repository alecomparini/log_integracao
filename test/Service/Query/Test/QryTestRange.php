<?php
/**
 * Query dummy pra validar bind de parametros de range IN()
 * @see DatabaseTest::testGetSql
 * @var string
 */
$sql = "SELECT *
        FROM Parceiro_Site ps
        WHERE p.ParceiroId IN :parceiro_id
        AND ps.SiteId IN :site_id";
        
return $sql;
