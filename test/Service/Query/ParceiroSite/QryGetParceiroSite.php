<?php
/**
 * Query vazia para validar execucao de queries validas, retorno e bind de parametros
 * @see DatabaseTest::testGetSqlFile
 * @see DatabaseTest::testGetSql
 * @var string
 */
$sql = "SELECT *
        FROM Parceiro_Site ps
        INNER JOIN Parceiro p ON p.ParceiroId = ps.ParceiroId
        WHERE p.ParceiroId = :parceiro_id
        AND ps.SiteId = :site_id";
        
return $sql;
