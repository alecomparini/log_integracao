<?php

require_once realpath(__DIR__ . '/../../../Utils/Utils.php');

/**
 * Class ServicosControllerTest
 *
 * @package tests\Controller\Rest\Servicos
 * @covers \App\Controller\Rest\Servicos\ServicosController
 */
class ServicosControllerTest extends PHPUnit_Framework_TestCase{

    /**
     * Testa criação de notificações com produtos válidos
     *
     * @covers  \App\Controller\Rest\Servicos\ServicosController::setNotificacao
     * @test    testNotificacaoProdutosValidos
     */
    public function testNotificacaoProdutosValidos() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $postData = array(
            'produtos' => array(4221,11774),
            'tipoNotificacao' => 1
        );

        //Preparando o teste
        $json = file_get_contents(MOCK_DIR . 'notificacaoProdutosValidos.json');
        $teste = json_decode($json, true);

        if (!$teste) {
            $this->fail('O arquivo de mock [notificacaoProdutosValidos.json] nao contem um json valido.');
        }

        //coletando amostra
        $curl = $this->createCURL('notificacao', null, $postData);
        $resultado = curl_exec($curl);
        $amostra = json_decode($resultado, true);

        //comparando o resultado com o teste
        $comparacao = Utils::showDiffArray($amostra, $teste);

        if(count($comparacao) > 0) {
            foreach ($comparacao as $msg) {
                $this->fail($msg);
            }
        }
    }

    /**
     * Testa criação de notificações com produtos notificacaoProdutosInvalidos
     *
     * @covers  \App\Controller\Rest\Servicos\ServicosController::setNotificacao
     * @test    testNotificacaoProdutosInvalidos
     * @group dev
     */
    public function testNotificacaoProdutosInvalidos() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $postData = array(
            'produtos' => array(117713232322),
            'tipoNotificacao' => 1
        );

        //Preparando o teste
        $json = file_get_contents(MOCK_DIR . 'notificacaoProdutosInvalidos.json');
        $teste = json_decode($json, true);

        if (!$teste) {
            $this->fail('O arquivo de mock [notificacaoProdutosInvalidos.json] nao contem um json valido.');
        }

        //coletando amostra
        $curl = $this->createCURL('notificacao', null, $postData);
        $resultado = curl_exec($curl);
        $amostra = json_decode($resultado, true);

        //comparando o resultado com o teste
        $comparacao = Utils::showDiffArray($amostra, $teste);

        if(count($comparacao) > 0) {
            foreach ($comparacao as $msg) {
                $this->fail($msg);
            }
        }
    }

    /**
     * Testa criação de notificações sem códigos de  produtos
     *
     * @covers  \App\Controller\Rest\Servicos\ServicosController::setNotificacao
     * @test    testNotificacaoProdutosAusentes
     */
    public function testNotificacaoProdutosAusentes() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $postData = array(
            'tipoNotificacao' => 1
        );

        //Preparando o teste
        $json = file_get_contents(MOCK_DIR . 'notificacaoSemProdutos.json');
        $teste = json_decode($json, true);

        if (!$teste) {
            $this->fail('O arquivo de mock [notificacaoSemProdutos.json] nao contem um json valido.');
        }

        //coletando amostra
        $curl = $this->createCURL('notificacao', null, $postData);
        $resultado = curl_exec($curl);
        $amostra = json_decode($resultado, true);

        //comparando o resultado com o teste
        $comparacao = Utils::showDiffArray($amostra, $teste);

        if(count($comparacao) > 0) {
            foreach ($comparacao as $msg) {
                $this->fail($msg);
            }
        }
    }

    /**
     * Testa criação de notificações com tipo de notificação inválida
     *
     * @covers  \App\Controller\Rest\Servicos\ServicosController::setNotificacao
     * @test    testNotificacaoTipoInvalido
     */
    public function testNotificacaoTipoInvalido() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $postData = array(
            'produtos' => array(11771),
            'tipoNotificacao' => 33333
        );

        //Preparando o teste
        $json = file_get_contents(MOCK_DIR . 'notificacaoTipoInvalido.json');
        $teste = json_decode($json, true);

        if (!$teste) {
            $this->fail('O arquivo de mock [notificacaoTipoInvalido.json] nao contem um json valido.');
        }

        //coletando amostra
        $curl = $this->createCURL('notificacao', null, $postData);
        $resultado = curl_exec($curl);
        $amostra = json_decode($resultado, true);

        //comparando o resultado com o teste
        $comparacao = Utils::showDiffArray($amostra, $teste);

        if(count($comparacao) > 0) {
            foreach ($comparacao as $msg) {
                $this->fail($msg);
            }
        }
    }

    /**
     * Testa criação de notificações sem tipo de notificação
     *
     * @covers  \App\Controller\Rest\Servicos\ServicosController::setNotificacao
     * @test    testNotificacaoSemTipoNotificacao
     * @group dev
     */
    public function testNotificacaoSemTipoNotificacao() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $postData = array(
            'produtos' => array(11771)
        );

        //Preparando o teste
        $json = file_get_contents(MOCK_DIR . 'notificacaoSemTipo.json');
        $teste = json_decode($json, true);

        if (!$teste) {
            $this->fail('O arquivo de mock [notificacaoSemTipo.json] nao contem um json valido.');
        }

        //coletando amostra
        $curl = $this->createCURL('notificacao', null, $postData);
        $resultado = curl_exec($curl);
        $amostra = json_decode($resultado, true);

        //comparando o resultado com o teste
        $comparacao = Utils::showDiffArray($amostra, $teste);

        if(count($comparacao) > 0) {
            foreach ($comparacao as $msg) {
                $this->fail($msg);
            }
        }
    }

    /**
     * Cria handler de cURL para testes de caixa preta
     *
     * @return resource
     */
    private function createCURL($servico, $queryString = null, $postData = null) {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $url = Config::get('base_url') . '/servicos/' . $servico;
        $ch = curl_init();

        if ($queryString != null) {
            $url .= "?" . $queryString;
        }

        if ($postData) {
            curl_setopt($ch, CURLOPT_POST, 1);
            curl_setopt($ch, CURLOPT_POSTFIELDS, json_encode($postData));
            curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        }

        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);

        return $ch;
    }

}
