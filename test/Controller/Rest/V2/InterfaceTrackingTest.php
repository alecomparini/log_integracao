<?php
//@TODO criar caso de teste para dar erro em + de 10 pedido_id passados no body

/**
 * Class InterfaceTrackingTest Teste Caixa preta para Tracking
*
* @package tests\Controller\Rest\V1
* @covers \App\Controller\Rest\V1\TrackingController
*/
class InterfaceTrackingTest extends PHPUnit_Framework_TestCase {

    const URL_SERVICO = '/InterfaceTracking/Consulta';

    /**
     * Testa requisições de consultas
     *
     * @covers \App\Controller\Rest\V1\TrackingController::consulta
     * @test   testConsulta
     */
    public function testConsulta() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $response = \Httpful\Request::post(Config::get('base_url') . self::URL_SERVICO)
            ->sends('application/json')
            ->body('{"parceiro_id" : 2,"site_id" : 4,"pedido_id" : [22336572, 1148238]}')
            ->send();        
        $this->assertEquals(200, $response->code, 'Erro na requisição do pedido. Msg: ' . $response->body->msg);
        
        $antigo = json_decode(file_get_contents(MOCK_DIR."InterfaceTracking_Consulta.json"));
        $jsonRetorno = $response->body;
        
        $this->assertEquals(200, $response->body->status, 'Erro retornado da aplicação');
        
        $this->assertEquals($antigo->dados, $jsonRetorno->dados, "");
        
        $this->assertEquals($antigo->msg, $jsonRetorno->msg, "");
        
        $this->assertEquals($antigo->status, $jsonRetorno->status, "");        

//         var_dump([$antigo, $jsonRetorno]);
//         $jsonRetorno->dados[0]->pedido_parceiro = 5555;

        $this->assertTrue($antigo == $jsonRetorno, "Objetos retornados sao diferentes");
    }

    /**
     * Testa requisições de consultas
     *
     * @covers \App\Controller\Rest\V1\TrackingController::consulta
     * @test   testConsultaMaisDeDezPedidos
     */
    public function testConsultaMaisDeDezPedidos() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $response = \Httpful\Request::post(Config::get('base_url') . self::URL_SERVICO)
        ->sends('application/json')
        ->body('{"parceiro_id" : 2,"site_id" : 4,"pedido_id" : [22336572, 1148238, 3, 4, 5, 6, 7, 8, 9, 10, 11]}')
        ->send();
        $this->assertEquals(400, $response->code, 'Erro na requisição do pedido. Msg: ' . $response->body->msg);
    
        $jsonRetorno = $response->body;

        $this->assertEquals(array(), $jsonRetorno->dados, "");

        $this->assertEquals("Erro: Erro de validação - Tamanho do campo [pedidoId] deve ser menor ou igual a 10. Tamanho informado foi 11.", $jsonRetorno->msg, "");

        $this->assertEquals(5, $jsonRetorno->status, 'Aplicação deveria ter retornado um codigo de erro');

    }

}