<?php

/**
 * Class PedidoControllerTest
 *
 * @package tests\Controller\Rest\V2
 * @covers \App\Controller\Rest\V2\PedidoController
 */
class PedidoControllerTest extends PHPUnit_Framework_TestCase {

    const URL_SERVICO = '/ws/Pedido';
    const URL_SERVICO_REST_PEDIDO = '/v2/pedido'; //?XDEBUG_SESSION_START=PHPStorm

    /**
     * @dataProvider vendaProvider
     * @param $venda
     */
    public function testVendaComSeguro($venda) {
        $this->markTestIncomplete('Este metodo não esta completo ainda');

        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        unset($venda['pedidoProduto'][0]['garantia']);
        $venda['pedidoProduto'][0]['valorTotal'] = 3100;
        $venda['valorTotalProdutos'] = 3100;
        $venda['valorTotal'] = 3110;
        $venda['pagamento'][0]['valorPago'] = 3110;

        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertEquals(200, $response->code, 'Erro na requisição do pedido. Msg: ' . $response->body->mensagem);
        $this->assertEquals($response->body->codigo, 0, 'Erro retornado da aplicação');
    }

    /**
     * @dataProvider vendaProvider
     * @param array $venda
     */
    public function testVendaComSeguroMensagensErro($venda) {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $seguro = $venda['pedidoProduto'][0]['seguro'];
        $venda['pedidoProduto'][0]['valorTotal'] = 3100;
        $venda['valorTotalProdutos'] = 3100;
        $venda['valorTotal'] = 3110;
        $venda['pagamento'][0]['valorPago'] = 3110;

        unset($venda['pedidoProduto'][0]['garantia']);
        // Cenários
        // -> Seguro inexistente
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['seguro'] = $seguro;
        $venda['pedidoProduto'][0]['seguro']['seguroTabelaId'] = 6664454;
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído -> Seguro inexistente: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        // -> Seguro de outro produto
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['seguro'] = $seguro;
        $venda['pedidoProduto'][0]['seguro']['seguroTabelaId'] = 206;
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído -> Seguro de outro produto: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        // -> Campos faltantes
        // ----> SeguroTabelaId
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['seguro'] = $seguro;
        unset($venda['pedidoProduto'][0]['seguro']['seguroTabelaId']);
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído ----> SeguroTabelaId: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        // ----> ValorVenda
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['seguro'] = $seguro;
        unset($venda['pedidoProduto'][0]['seguro']['valorVenda']);
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído ----> ValorVenda: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        // ----> Familia
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['seguro'] = $seguro;
        unset($venda['pedidoProduto'][0]['seguro']['familia']);
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído ----> Familia: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        // ----> Quantidade
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['seguro'] = $seguro;
        unset($venda['pedidoProduto'][0]['seguro']['quantidade']);
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();

        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído ----> Quantidade: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        // -> Testa tipos de campso do seguro
        // ----> SeguroTabelaId
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['seguro'] = $seguro;
        $venda['pedidoProduto'][0]['seguro']['seguroTabelaId'] = '2132s2ss';
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído ----> SeguroTabelaId: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        // ----> ValorVenda
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['seguro'] = $seguro;
        $venda['pedidoProduto'][0]['seguro']['valorVenda'] = 'asdasd';
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído ----> ValorVenda: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        // ----> Familia
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['seguro'] = $seguro;
        $venda['pedidoProduto'][0]['seguro']['familia'] = '1';
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído ----> Familia: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['seguro'] = $seguro;
        $venda['pedidoProduto'][0]['seguro']['familia'] = 'asdasd';
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído ----> Familia2: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['seguro'] = $seguro;
        $venda['pedidoProduto'][0]['seguro']['quantidade'] = 'A4';
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído ----> Quantidade1: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['seguro'] = $seguro;
        $venda['pedidoProduto'][0]['seguro']['quantidade'] = -1;
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído ----> Quantidade2: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');
    }

    /**
     * @dataProvider vendaProvider
     * @param $venda
     */
    public function testVendaComGarantia($venda) {
        $this->markTestIncomplete('Este metodo não esta completo ainda');

        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        unset($venda['pedidoProduto'][0]['seguro']);
        $venda['pedidoProduto'][0]['valorTotal'] = 3100;
        $venda['valorTotalProdutos'] = 3100;
        $venda['valorTotal'] = 3110;
        $venda['pagamento'][0]['valorPago'] = 3110;

        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertEquals(200, $response->code, 'Erro na requisição do pedido. Msg: ' . $response->body->mensagem);
        $this->assertEquals($response->body->codigo, 0, 'Erro retornado da aplicação');
    }

    /**
     * @dataProvider vendaProvider
     * @param $venda
     */
    public function testVendaComGarantiaMensagensErro($venda) {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $garantia = $venda['pedidoProduto'][0]['garantia'];
        unset($venda['pedidoProduto'][0]['seguro']);

        //Cenários

        // -> Seguro com código que não existe
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['garantia'] = $garantia;
        $venda['pedidoProduto'][0]['garantia']['codigo'] = 132321000;
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        // -> Campos faltantes
        // -> CF: Codigo
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['garantia'] = $garantia;
        unset($venda['pedidoProduto'][0]['garantia']['codigo']);
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        // -> CF: Prazo
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['garantia'] = $garantia;
        unset($venda['pedidoProduto'][0]['garantia']['prazo']);
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        // -> CF: ValorVenda
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['garantia'] = $garantia;
        unset($venda['pedidoProduto'][0]['garantia']['valorVenda']);
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        // -> CF: ValorParcelado
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['garantia'] = $garantia;
        unset($venda['pedidoProduto'][0]['garantia']['valorParcelado']);
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        // -> CF: Quantidade
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['garantia'] = $garantia;
        unset($venda['pedidoProduto'][0]['garantia']['quantidade']);
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        // -> Campos com tipos de dados incorretos
        // -> Codigo Int
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['garantia'] = $garantia;
        $venda['pedidoProduto'][0]['garantia']['codigo'] = 105106;
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        // -> Codigo string com letras
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['garantia'] = $garantia;
        $venda['pedidoProduto'][0]['garantia']['codigo'] = "BATATA";
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        // -> Prazo string numerica
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['garantia'] = $garantia;
        $venda['pedidoProduto'][0]['garantia']['prazo'] = "105106";
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        // -> Prazo string com letras
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['garantia'] = $garantia;
        $venda['pedidoProduto'][0]['garantia']['prazo'] = "105106AGS";
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        // -> Valor venda string float
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['garantia'] = $garantia;
        $venda['pedidoProduto'][0]['garantia']['valorVenda'] = "272.99";
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        // -> Valor venda string com letras
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['garantia'] = $garantia;
        $venda['pedidoProduto'][0]['garantia']['valorVenda'] = "C272.99";
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        // -> Quantidade string numérica
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['garantia'] = $garantia;
        $venda['pedidoProduto'][0]['garantia']['quantidade'] = "1";
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        // -> Quantidade string com letra
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['garantia'] = $garantia;
        $venda['pedidoProduto'][0]['garantia']['quantidade'] = "B12";
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        // -> Quantidade negativa
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['garantia'] = $garantia;
        $venda['pedidoProduto'][0]['garantia']['quantidade'] = -1;
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');

        // -> Seguro com código existente mas não pertence ao produto
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $venda['pedidoProduto'][0]['garantia'] = $garantia;
        $venda['pedidoProduto'][0]['garantia']['codigo'] = "608460";
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertTrue(200 != $response->code, 'O pedido não deveria ter sido concluído: ' . $response->body->mensagem);
        $this->assertTrue($response->body->codigo != 0, 'Erro retornado da aplicação');
    }

    /**
     * @dataProvider vendaProvider
     * @param $venda
     */
    public function testVendaComSeguroEGarantia($venda) {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertEquals(200, $response->code, 'O pedido deveria ter sido concluído: ' . $response->body->mensagem);
        $this->assertEquals($response->body->codigo, 0, 'Erro retornado da aplicação' . $response->body->mensagem);
    }

    /**
     * @dataProvider vendaProvider
     * @param $venda
     */
    public function testVendaSemSeguroESemGarantia($venda) {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $venda['parceiroPedidoId'] = "" . intval(microtime(true));
        unset($venda['pedidoProduto']['seguro']);
        unset($venda['pedidoProduto']['garantia']);
        $obj = json_encode($venda);
        $response = \Httpful\Request::post(Config::get('base_url') . PedidoControllerTest::URL_SERVICO_REST_PEDIDO)
            ->sends('application/json')
            ->body($obj)
            ->send();
        $this->assertEquals(200, $response->code, 'O pedido deveria ter sido concluído: ' . $response->body->mensagem);
        $this->assertEquals($response->body->codigo, 0, 'Erro retornado da aplicação' . $response->body->mensagem);
    }

//@TODO: Externalizar este teste
//    /**
//     * @dataProvider vendaProvider
//     * @param $venda
//     */
//    public function testVendaValoresIncorretos($venda) {
//        $this->assertTrue(class_exists('SoapClient'), 'Classe SoapClient não existe');
//        $soapClient = new SoapClient(PedidoControllerTest::URL_SERVICO . '/?wsdl&XDEBUG_SESSION_START=PHPStorm');
//
//        //Testa inserção que já existe
//        $response = $soapClient->setPedido($venda);
//        $this->assertEquals(17, intval($response->Codigo), print_r($response, true));
//
//        //Testa inserção que já existe
//        $venda['parceiroPedidoId'] = intval(microtime(true));
//        $response = $soapClient->setPedido($venda);
//        $this->assertEquals(0, intval($response->Codigo), print_r($response, true));
//
//        //Testa sem pagamento
//        $venda['parceiroPedidoId'] = intval(microtime(true));
//        $oldPagamento = $venda['pagamento'];
//        $venda['pagamento'] = array();
//        $response = $soapClient->setPedido($venda);
//        $this->assertEquals(2, intval($response->Codigo), print_r($response, true));
//        $venda['pagamento'] = $oldPagamento;
//
//        //Testa sem Produto
//        $venda['parceiroPedidoId'] = intval(microtime(true));
//        $oldPedidoProduto = $venda['pedidoProduto'];
//        $venda['pedidoProduto'] = array();
//        $response = $soapClient->setPedido($venda);
//        $this->assertEquals(2, intval($response->Codigo), print_r($response, true));
//        $venda['pedidoProduto'] = $oldPedidoProduto;
//
//        //Testa valor incorreto enviado
//        $venda['parceiroPedidoId'] = intval(microtime(true));
//        $oldValorTotalProdutos = $venda['valorTotalProdutos'];
//        $venda['valorTotalProdutos'] = 0.01;
//        $response = $soapClient->setPedido($venda);
//        $this->assertEquals(0, intval($response->Codigo), print_r($response, true));
//        $venda['valorTotalProdutos'] = $oldValorTotalProdutos;
//    }

    public function vendaProvider() {
        return array(array(
            array(
                'parceiroId' => 2,
                'clienteEmail' => "cmgomes.es@gmail.com",
                'clienteTipo' => "pf",

                'clientePfNome' => "William",
                'clientePfSobrenome' => "Okano",
                'clientePfCPF' => "64767738792",
                'clientePfDataNascimento' => "1989-03-15",
                'clientePfSexo' => "M",

                'clientePjCNPJ' => '',
                'clientePjInscEstadual' => '',
                'clientePjIsento' => '',
                'clientePjNomeFantasia' => '',
                'clientePjRamoAtividade' => '',
                'clientePjRazaoSocial' => '',
                'clientePjTelefone' => '',

                'endEntregaTipo' => "Residencial",
                'endEntregaDestinatario' => "William Okano",
                'endEntregaCep' => "38408102",
                'endEntregaEndereco' => "Rua Fco Vicente Ferreira",
                'endEntregaNumero' => 170,
                'endEntregaComplemento' => "Apto 106 R",
                'endEntregaBairro' => "Santa Mônica",
                'endEntregaCidade' => "Uberlândia",
                'endEntregaEstado' => "MG",
                'endEntregaTelefone1' => "(34)9999-9999",
                'endEntregaTelefone2' => "",
                'endEntregaCelular' => "",
                'endEntregaReferencia' => "UFU",
                'endCobrancaTipo' => "Residencial",
                'endCobrancaDestinatario' => "William Okano",
                'endCobrancaCep' => "38408102",
                'endCobrancaEndereco' => "Rua Fco Vic Ferr",
                'endCobrancaNumero' => 170,
                'endCobrancaComplemento' => "Apt 106 R",
                'endCobrancaBairro' => "Santa Mônica",
                'endCobrancaCidade' => "Uberlândia",
                'endCobrancaEstado' => "MG",
                'endCobrancaTelefone1' => "(33)3333-3333",
                'endCobrancaTelefone2' => "(44)4444-4444",
                'endCobrancaCelular' => "",
                'endCobrancaReferencia' => "UFU",
                'parceiroPedidoId' => "11111",
                'valorTotal' => 3111,
                'valorEntrega' => 1.00,
                'valorTotalFrete' => 10.00,
                'valorTotalProdutos' =>  3101,
                'valorJuros' => 0.00,
                'dataEntrega' => date("Y-m-d", strtotime("+25 days")),
                'turnoEntrega' => 1,
                'vendedorToken' => '',

                'pedidoProduto' => array(
                    array(
                        'sequencial' => 1,
                        'codigoProduto' => '597508',
                        'quantidade' => 1,
                        'valorUnitario' => 3099,
                        'valorTotal' => 3101,
                        'prazoEntregaCombinado' => 5,
                        'tipo' => 1,
                        'valorDesconto' => 0.00,
                        'valorFrete' => 10.00,

                        'garantia' => array(
                            'codigo' => '608460',
                            'prazo' => 12,
                            'valorVenda' => 1,
                            'quantidade' => 1,
                        ),

                        'seguro' => array(
                            'seguroTabelaId' => 323,
                            'valorVenda' => 1,
                            'familia' => 36,
                            'quantidade' => 1
                        ),
                    ),
                ),

                'pagamento' => array(
                    array(
                        'meioPagamentoId' => 1,
                        'sequencial' => 1,
                        'valorPago' => 3111,
                        'bandeiraId' => 1,
                        'parcelas' => 1,
                        'cartaoNumero' => "4444-4444-4444-4444",
                        'cartaoValidade' => "12/2016",
                        'cartaoCodSeguranca' => 176,
                        'cartaoNomePortador' => "José Maria João",
                        'percentualJuros' => 0.00,
                        'valorJuros' => 0.00,
                    ),
                ),
            )
        ));
    }

}