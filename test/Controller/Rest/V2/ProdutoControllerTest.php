<?php

/**
 * Teste Caixa preta para consumo de Consultas de Produtos
 *
 * @covers \App\Controller\Rest\V2\ProdutoController
 */
class ProdutoControllerTest extends PHPUnit_Framework_TestCase {

    /**
     * Testa requisições de consultas de produtos sem parametros
     *
     * @covers \App\Controller\Rest\V2\ProdutoController::consulta
     * @test   testConsultaSemParametros
     */
    public function testConsultaSemParametros() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $curl = $this->createCURL();
        $result = curl_exec($curl);

        $this->assertStringStartsWith('{', $result, 'Mal Formação do JSON de retorno.');
        $this->assertStringEndsWith('}', $result, 'Mal formação do JSON de retorno.');

        $response = json_decode($result);

        $this->assertObjectHasAttribute('success', $response, 'Ausência da propriedade success na resposta.');
        $this->assertObjectHasAttribute('data', $response, 'Ausência da propriedade data na resposta.');
        $this->assertObjectHasAttribute('mensagem', $response, 'Ausência da propriedade mensagem na resposta.');
        $this->assertObjectHasAttribute('codigo', $response, 'Ausência da propriedade codigo na resposta.');
        $this->assertTrue(is_array($response->data), 'A propriedade data não é um array válido.');
        $this->assertNotNull($response->mensagem, 'A propriedade mensagem veio vazia.');
        $this->assertEquals(
            'Erro de validação: Campo [parceiroId] nao deve ser vazio',
            $response->mensagem,
            'A mensagem de resposta não foi a esperada.'
        );
    }

    /**
     * Testa requisições de consultas de produtos sem códigos de produto
     *
     * @covers \App\Controller\Rest\V2\ProdutoController::consulta
     * @test   testConsultaSemCodigosProduto
     */
    public function testConsultaSemCodigosProduto() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $curl = $this->createCURL('parceiroId=2');
        $result = curl_exec($curl);

        $this->assertStringStartsWith('{', $result, 'Mal Formação do JSON de retorno.');
        $this->assertStringEndsWith('}', $result, 'Mal formação do JSON de retorno.');
        $response = json_decode($result);

        $this->assertInstanceOf('\stdClass', $response, 'Não é uma resposta válida.');
        $this->assertEquals(
            'Erro de validação: Campo [produtoCodigo] e obrigatorio.',
            $response->mensagem,
            'A mensagem de erro não foi a esperada.'
        );
    }

    /**
     * Testa requisições de consultas de produtos com códigos de produtos inválidos
     *
     * @covers \App\Controller\Rest\V2\ProdutoController::consulta
     * @test   testConsultaCodigoInvalido
     */
    public function testConsultaCodigoInvalido() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $curl = $this->createCURL('parceiroId=2&produtoCodigo[]=343242424');
        $result = curl_exec($curl);

        $this->assertStringStartsWith('{', $result, 'Mal Formação do JSON de retorno.');
        $this->assertStringEndsWith('}', $result, 'Mal formação do JSON de retorno.');

        $response = json_decode($result);

        $this->assertInstanceOf('\stdClass', $response, 'Não é uma resposta válida.');
        $this->assertEquals(
            'Erro de validação: Os códigos de produtos recebidos não são válidos',
            $response->mensagem,
            'A mensagem de erro não foi a esperada.'
        );
    }

    /**
     * Testa requisições de consultas de produtos com único produto simples sem garantia nem seguro
     * @covers       \App\Controller\Rest\V2\ProdutoController::consulta
     * @test   testConsultaProdutoSimples
     */
    public function testConsultaProdutoSimples() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $curl = $this->createCURL('parceiroId=2&produtoCodigo[]=630018');
        $result = curl_exec($curl);

        $this->assertStringStartsWith('{', $result, 'Mal Formação do JSON de retorno. ');
        $this->assertStringEndsWith('}', $result, 'Mal formação do JSON de retorno.');

        $response = json_decode($result);

        $this->assertInstanceOf('\stdClass', $response, 'Não é uma resposta válida.');
        $this->assertTrue(is_array($response->data), 'A propriedade data não é um array');
        $this->assertEquals(1, count($response->data), 'Não é o resultado esperado');

        $produto = $response->data[0];

        $this->assertInstanceOf('\stdClass', $produto, 'Produto não é uma instancia de \stdClass.');
        $this->assertObjectNotHasAttribute('Garantias', $produto, 'O produto possui garantia quando não deveria possuir.');
        $this->assertObjectNotHasAttribute('Seguro', $produto, 'O produto possui seguro quando não deveria possuir.');

        //Preparando o teste
        $json = file_get_contents(MOCK_DIR . 'dadosProdutoSimples.json');
        $teste = json_decode($json, true);

        $amostra = json_decode($result, true);

        //print_r($teste);die;
        $diferencas = $this->showDiffArray($amostra, $teste);

        if (count($diferencas) > 0) {
            foreach ($diferencas as $diff) {
                 $this->fail($diff);
            }
        }
    }

    /**
     * Testa requisições de consultas de produtos com único produto simples com garantia e seguro
     * @covers       \App\Controller\Rest\V2\ProdutoController::consulta
     * @test   testConsultaProdutoSeguroGarantias
     */
    public function testConsultaProdutoSeguroGarantias() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $curl = $this->createCURL('parceiroId=2&produtoCodigo[]=597508');
        $result = curl_exec($curl);

        $this->assertStringStartsWith('{', $result, 'Mal Formação do JSON de retorno.');
        $this->assertStringEndsWith('}', $result, 'Mal formação do JSON de retorno.');

        $response = json_decode($result);

        $this->assertInstanceOf('\stdClass', $response, 'Não é uma resposta válida.');
        $this->assertTrue(is_array($response->data), 'A propriedade data não é um array');
        $this->assertEquals(1, count($response->data), 'Não é o resultado esperado');

        $produto = $response->data[0];

        $this->assertInstanceOf('\stdClass', $produto, 'Produto não é uma instancia de \stdClass.');
        $this->assertObjectHasAttribute('garantias', $produto, 'O produto possui garantia quando não deveria possuir.');
        $this->assertObjectHasAttribute('seguro', $produto, 'O produto possui seguro quando não deveria possuir.');

        //Preparando o teste
        $file = realpath(MOCK_DIR . DS . 'dadosProdutoSimplesSeguradoGarantias.json');
        $json = file_get_contents($file);
        $teste = json_decode($json, true);

        $amostra = json_decode($result, true);
        $diferencas = $this->showDiffArray($amostra, $teste);

        if (count($diferencas) > 0) {
            foreach ($diferencas as $diff) {
                $this->fail($diff);
            }
        }
    }

    /**
     * Testa requisições de consultas de produtos com único combo com garantia e seguro
     *
     * @covers  \App\Controller\Rest\V2\ProdutoController::consulta
     * @test    testConsultaComboSeguroGarantias
     */
    public function testConsultaComboSeguroGarantias() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $curl = $this->createCURL('parceiroId=2&produtoCodigo[]=C47104');
        $result = curl_exec($curl);

        $this->assertStringStartsWith('{', $result, 'Mal Formacao do JSON de retorno. '. $result);
        $this->assertStringEndsWith('}', $result, 'Mal formacao do JSON de retorno.');

        $response = json_decode($result);

        $this->assertInstanceOf('\stdClass', $response, 'Nao e uma resposta valida.');
        $this->assertTrue(is_array($response->data), 'A propriedade data nao e um array');
        $this->assertEquals(1, count($response->data), 'Não é o resultado esperado');

        $produto = $response->data[0];

        $this->assertInstanceOf('\stdClass', $produto, 'Produto nao e uma instancia de \stdClass.');

        //Preparando o teste
        $file = realpath(MOCK_DIR . DS . 'dadosComboSeguroGarantias.json');
        $json = file_get_contents($file);
        $teste = json_decode($json, true);

        $amostra = json_decode($result, true);
        $diferencas = $this->showDiffArray($amostra, $teste);

        if (count($diferencas) > 0) {
            foreach ($diferencas as $diff) {
                $this->fail($diff);
            }
        }
    }

    /**
     * Testa requisições de consultas de produtos com varios produtos e combos com garantias e seguro
     *
     * @covers  \App\Controller\Rest\V2\ProdutoController::consulta
     * @test    testConsultaMultiProdutosECombosSeguroGarantias
     */
    public function testConsultaMultiProdutosECombosSeguroGarantias() {
        $this->markTestIncomplete('Este metodo não esta completo ainda');
        $curl = $this->createCURL('parceiroId=2&produtoCodigo[]=C55837&produtoCodigo[]=597508&produtoCodigo[]=C47104&produtoCodigo[]=605560');
        $result = curl_exec($curl);

        $this->assertStringStartsWith('{', $result, 'Mal Formacao do JSON de retorno.' . $result);
        $this->assertStringEndsWith('}', $result, 'Mal formacao do JSON de retorno.');

        $response = json_decode($result);

        $this->assertInstanceOf('\stdClass', $response, 'Nao e uma resposta valida.');
        $this->assertTrue(is_array($response->data), 'A propriedade data nao e um array');

        $produto = $response->data[0];

        $this->assertInstanceOf('\stdClass', $produto, 'Produto nao e uma instancia de \stdClass.');

        //Preparando o teste
        $file = realpath(MOCK_DIR . 'dadosMultiProdutosCombosSeguradosGarantias.json');
        $json = file_get_contents($file);
        $teste = json_decode($json, true);
        $amostra = json_decode($result, true);
        $diferencas = $this->showDiffArray($amostra, $teste);

        if (count($diferencas) > 0) {
            foreach ($diferencas as $diff) {
                $this->fail($diff);
            }
        }
    }

    private function showDiffArray($amostra, $teste) {
        $resultado = array();
        $chaves = array_keys($teste);

        foreach ($chaves as $chave) {

            if (!array_key_exists($chave, $amostra)) {
                $resultado[] = "A chave [$chave] nao foi encontrado na amostra.";
            } else {
                if (is_array($teste[$chave])) {
                    if (!is_array($amostra[$chave])) {
                        $resultado[] = "A chave [$chave] da amostra deveria ser um array.";
                    } else {
                        $tmp_resultado = $this->showDiffArray($amostra[$chave], $teste[$chave]);
                        if (is_array($tmp_resultado)) {
                            $resultado = array_merge($tmp_resultado, $resultado);
                        }
                    }
                } else {
                    if ($amostra[$chave] != $teste[$chave]) {
                        $resultado[] = "A chave [$chave] da amostra [$amostra[$chave]] nao bate com a informacao no teste [$teste[$chave]].";
                    }
                }
            }
        }

        return $resultado;
    }

    /**
     * Cria handler de cURL
     *
     * @return resource
     */
    private function createCURL($queryString = null) {
        $url = Config::get('base_url') . '/v2/produto';
        //curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
        if ($queryString != null) {
            $url .= "?" . $queryString;
        }

        $ch = curl_init();
        $timeout = 0;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        return $ch;
    }

}