<?php
/**
 *  Arquivo de inicialização do PHPUnit, carrega componentes e classes necessárias para execução de testes
 * @author Gustavo F. Viegas de Oliveira <gustavooliveira@softbox.com.br>
 */

require_once realpath(dirname(__FILE__)) . "/../src/bootstrap.php";
require_once realpath(dirname(__FILE__)) . "/TestSuiteCase.php";
require_once realpath(dirname(__FILE__)) . "/Soap/SoapBase.php";
require_once realpath(dirname(__FILE__)) . "/Soap/PDOIntegracao.php";
require_once realpath(dirname(__FILE__)) . "/Config.php";

$config = new Config();
$config->load('test');

## Reescrevendo em Mysqli

//$conn = new \Mysqli(
//    getenv("DB_MASTER_HOST"),
//    getenv("DB_MASTER_USER"),
//    getenv('DB_MASTER_PASS'),
//    getenv("DB_MASTER_BASE"),
//    getenv("DB_MASTER_PORT"));
//
//if ($conn->connect_errno) {
//    fwrite(STDOUT, "Falha na conexão com o banco de dados" . PHP_EOL);
//    exit;
//}
//
//fwrite(STDOUT, "Limpando tabelas riel_producao" . PHP_EOL);
//
//$conn->query("use riel_producao");
//$conn->query("SET FOREIGN_KEY_CHECKS=0");
//
//$tables = $conn->query("SHOW TABLES")->fetch_all();
//
//foreach ($tables as $table) {
//    $table = $table[0];
//    if ($conn->query("TRUNCATE TABLE $table")) {
//        fwrite(STDOUT, ".");
//    } else {
//        fwrite(STDOUT, "F[$table]");
//    }
//}
//
//fwrite(STDOUT, PHP_EOL . "Populando tabelas riel_producao" . PHP_EOL);
//$sql = file_get_contents(TEST_DIR . "massas/massaDados.sql");
//$conn->query($sql);
//$conn->commit();
//
//fwrite(STDOUT, "Limpando tabelas riel_integracao" . PHP_EOL);
//$conn->query("use riel_integracao");
//$conn->query("SET FOREIGN_KEY_CHECKS=0;");
//$tables = $conn->query("SHOW TABLES")->fetch_all();
//foreach ($tables as $table) {
//    $table = $table[0];
//    fwrite(STDOUT, ".");
//    $conn->query("TRUNCATE TABLE $table");
//}
//
//fwrite(STDOUT, PHP_EOL . "Populando tabelas riel_integracao" . PHP_EOL);
//$sql = file_get_contents(TEST_DIR . "massas/massaDadosIntegracao.sql");
//$conn->query($sql);
//$conn->query("SET FOREIGN_KEY_CHECKS=1;");

//fwrite(STDOUT, "Iniciando PHPUnit" . PHP_EOL);
