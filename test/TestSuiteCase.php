<?php 

use PHPUnit_Extensions_MultipleDatabase_TestCase as PHPUnit;
/**
 * Classe container de Testes do PHPUnit
 * Providencia simulacoes do framework e metodos auxiliares com o mecanismo de multiplas conexões
 */
abstract class TestSuiteCase extends PHPUnit {

    private $configConnections;

    /**
     * Retorna dataset referente a massa de dados da riel_producao
     *
     * @return PHPUnit_Extensions_Database_DataSet_IDataSet
     */
    public function getRielProdutoDataSet() {
        $caminho = sprintf("%stests/massas/%s.xml", ROOT_DIR, "massaRielProducao");
        return new PHPUnit_Extensions_Database_DataSet_MysqlXmlDataSet($caminho);
    }

    /**
     * retorna dataset referente a massa de dados da riel_integracao
     * @return PHPUnit_Extensions_Database_DataSet_IDataSet
     */
    public function getIntegracaoDataSet() {
        $caminho = sprintf("%stests/massas/%s.xml", ROOT_DIR, "massaRielIntegracao");
        return new PHPUnit_Extensions_Database_DataSet_MysqlXmlDataSet($caminho);
    }


    /**
     * Estabelece as múltiplas conexões necessárias para os testes agrupando-as em um array e retornando para
     * as classes internas do PHPUNIT
     *
     * @return array
     * @throws Exception
     */
    protected function getDatabaseConfigs() {
        $configs = array();

        $pdoParams = sprintf("mysql:host=%s;dbname=%s", Config::get('db.master.host'), Config::get('db.master.base'));
        $siteConnection = new PDO(
            $pdoParams,
            Config::get('db.master.user'),
            Config::get('db.master.pass')
        );

        $pdoParams = sprintf("mysql:host=%s;dbname=%s", Config::get('db.master.host'), Config::get('db.master.base'));
        $integracaoConnection = new PDO(
            $pdoParams,
            Config::get('db.master.user'),
            Config::get('db.master.pass')
        );

        $sConnection = new PHPUnit_Extensions_Database_DB_DefaultDatabaseConnection($siteConnection);
        $iConnection = new PHPUnit_Extensions_Database_DB_DefaultDatabaseConnection($integracaoConnection);

        $builderSite = new PHPUnit_Extensions_MultipleDatabase_DatabaseConfig_Builder();
        $builderSite->connection($iConnection);
        $builderSite->dataSet($this->getIntegracaoDataSet());
        $configs[] = $builderSite->build();

        $builderSite = new PHPUnit_Extensions_MultipleDatabase_DatabaseConfig_Builder();
        $builderSite->connection($sConnection);
        $builderSite->dataSet($this->getRielProdutoDataSet());
        $configs[] = $builderSite->build();

        $this->configConnections = $configs;
        return $configs;
    }

}
