# Integração MV.COM

Projeto do sistema de Integração (webservices) da MV.COM

## Instalação
### Requisitos
- PHP 5.3.28+
    - curl
    - mcrypt
    - soap
    - lib-xml
- Composer
- Apache 2.2+

### Passo-a-passo
- cd src
- composer install
- cd app
- cd configs
- cp default.php.exemplo dev.php
- cp default.php.exemplo test.php

> **Não esquecer**  
> Editar as entradas de banco de dados de dev  
> Editar as entradas para o banco que rodará os testes (**o banco será dropado em toda execução, então cuidado**)  
> Este projeto utiliza encoding UTF-8, então configure sua IDE.

