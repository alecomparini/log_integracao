#!/bin/sh

# Arquivo utilizado para realizar a instalacao do sistema.
# Este arquivo deve ser agnostico a ambiente, pois sera utilizado para o deploy de todos (prd, hml, tst, ic, etc.)
# Aqui devem ser executados comandos para instalar dependencias, remover testes, etc.
#
# Argumentos: ambiente nome_do_projeto versao dir_checkout dir_configs

echo "Removendo testes..."
rm -rf ./test
cd ./src && composer install --no-dev --ignore-platform-reqs
cd ./

echo "Criando links..."
rm -f ./src/app/configs/hom.php
rm -f ./src/app/environment.php
ln -sf /home/projetos/configs/integracao/hom.php /home/projetos/releases/integracao/current/src/app/configs/hom.php
ln -sf /home/projetos/configs/integracao/environment.php /home/projetos/releases/integracao/current/src/app/environment.php
ln -sf /home/projetos/configs/integracao/config.php /home/projetos/releases/integracao/current/src/app/config.php

echo "Criando logs"
mkdir -p /tmp/___logs
chmod 777 -R /tmp/___logs
ln -s /tmp/___logs /home/projetos/releases/integracao/current/src/___logs

#
